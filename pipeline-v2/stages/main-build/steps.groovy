/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */


JACOCO_MOJO = "org.jacoco:jacoco-maven-plugin:0.8.4"
JACOCO_FILE = "jacoco.exe"




def call(){

	container('maven') {

		// Base build (compile + test + deploy), with jacoco records enabled.
		echo "Compilation - unit tests - deploy in local repo"

		// cleaning the previous jacoco file if any
		sh "rm -f ${WORKSPACE}/${JACOCO_FILE}"

		// Note that the jacoco agent goal must be run first,
		// otherwise it will be executed at the end (ie after
		// the test phase). Also we want to append everything to the same file.

		// Also we add some tweaks to exclude tm-front from the build (it will be built
		// in a separate pod). The profile 'no-front' is defined in tm.web
		// and make tm-front not required as dependency. The argument " -pl '!tm/tm-front' "
		// will exclude it from the reactor

		// Last, we want to deploy the snapshots for later use by the slave node
		sh """mvn clean	${JACOCO_MOJO}:prepare-agent \
                             -Djacoco.destFile=${WORKSPACE}/${JACOCO_FILE} \
                             -Djacoco.append=true \
                             -Pci -B -ntp \
 						deploy
                 """

	}

	container('chrome') {

		dir('tm/tm-front'){

			//yarn test-core, export node to path before the execution of the tests
			sh returnStatus: true, script:  'PATH=$PATH:$(pwd)/node && node/yarn/dist/bin/yarn test-core --no-progress --reporters junit --browsers ChromeHeadlessNoSandbox'

			//yarn test-app, export node to path before the execution of the tests
			sh returnStatus: true, script:  'PATH=$PATH:$(pwd)/node && node/yarn/dist/bin/yarn test-app --no-progress --reporters junit --browsers ChromeHeadlessNoSandbox'

		}
	}

}


return this
