<?xml version="1.0" encoding="UTF-8"?>
<!--

        This file is part of the Squashtest platform.
        Copyright (C) Henix, henix.fr

        See the NOTICE file distributed with this work for additional
        information regarding copyright ownership.

        This is free software: you can redistribute it and/or modify
        it under the terms of the GNU Lesser General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        this software is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Lesser General Public License for more details.

        You should have received a copy of the GNU Lesser General Public License
        along with this software.  If not, see <http://www.gnu.org/licenses/>.

-->
<databaseChangeLog xmlns="http://www.liquibase.org/xml/ns/dbchangelog"
                   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                   xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-2.0.xsd">

  <changeSet id="tm-1.22.2" author="jprioux">
    <comment>Update TM database version number</comment>
    <update tableName="CORE_CONFIG">
      <column name="VALUE" value="1.22.2"/>
      <where>STR_KEY = 'squashtest.tm.database.version'</where>
    </update>
  </changeSet>

  <changeSet id="tm-1.22.2-SQUASH-2701-1-add-tc-scm-repository-id-column" author="jprioux">
    <comment>Add a new column in test case for link to scm repository</comment>
    <addColumn tableName="TEST_CASE">
      <column name="SCM_REPOSITORY_ID" type="BIGINT"
              remarks="foreign key to SCM_REPOSITORY, link to associated scm repository.">
        <constraints nullable="true"
                     references="SCM_REPOSITORY(SCM_REPOSITORY_ID)" foreignKeyName="fk_tc_scm_repository" />
      </column>
    </addColumn>

    <createIndex tableName="TEST_CASE" indexName="idx_fk_tc_scm_repository">
      <column name="SCM_REPOSITORY_ID" type="BIGINT"/>
    </createIndex>
  </changeSet>

  <changeSet id="tm-1.22.2-SQUASH-2701-2-h2-drop-old-column" author="jprioux" dbms="h2">
    <comment>Directly drop unnecessary old column, no migration for h2</comment>
    <dropColumn tableName="TEST_CASE" columnName="SOURCE_CODE_REPOSITORY_URL" />
  </changeSet>

  <changeSet id="tm-1.22.2-SQUASH-2701-2-scm-repository-url-temporary-table" author="jprioux" dbms="mysql, postgresql">
    <comment>Create temporary table for migration</comment>
    <createTable tableName="SCM_REPOSITORY_URL_MIGRATION_TEMP">
      <column name="TCLN_ID" type="BIGINT">
        <constraints primaryKey="true" nullable="false" />
      </column>
      <column name="SOURCE_CODE_REPOSITORY_URL" type="VARCHAR(255)" />
      <column name="SERVER_URL_FROM_TC" type="VARCHAR(255)" />
      <column name="REPOSITORY_NAME_FROM_TC" type="VARCHAR(255)" />
      <column name="SCM_SERVER_ID" type="BIGINT" />
      <column name="SCM_REPOSITORY_ID" type="BIGINT" />
    </createTable>
  </changeSet>

  <changeSet id="tm-1.22.2-SQUASH-2701-3-mysql-migration-for-scm-repo-url" author="jprioux" dbms="mysql">
    <comment>Migration part if scm repository url has been added in tc</comment>

    <!-- Insert data into temporary table -->
    <sql>
      INSERT INTO SCM_REPOSITORY_URL_MIGRATION_TEMP
      SELECT tc.TCLN_ID,
             TRIM(TRAILING '/' FROM TRIM(tc.SOURCE_CODE_REPOSITORY_URL)),
             REPLACE(TRIM(TRAILING '/' FROM TRIM(tc.SOURCE_CODE_REPOSITORY_URL)), CONCAT('/', REVERSE(SUBSTRING_INDEX(REVERSE(TRIM(TRAILING '/' FROM TRIM(tc.SOURCE_CODE_REPOSITORY_URL))), '/', 1))), ''),
             REVERSE(SUBSTRING_INDEX(REVERSE(TRIM(TRAILING '/' FROM TRIM(tc.SOURCE_CODE_REPOSITORY_URL))), '/', 1)),
             null,
             null
      FROM TEST_CASE tc
      WHERE tc.SOURCE_CODE_REPOSITORY_URL IS NOT NULL;
    </sql>

    <!-- Update with scm linked in project if found, else with other existing repo and server ids, then update only with existing server ids.
         All null are for non-existing repos and servers which will be created after -->
    <sql>
      UPDATE SCM_REPOSITORY_URL_MIGRATION_TEMP mig
      JOIN TEST_CASE_LIBRARY_NODE tcln ON mig.TCLN_ID = tcln.TCLN_ID
      JOIN PROJECT p ON tcln.PROJECT_ID = p.PROJECT_ID
      JOIN SCM_REPOSITORY repo ON p.SCM_REPOSITORY_ID = repo.SCM_REPOSITORY_ID AND mig.REPOSITORY_NAME_FROM_TC = repo.NAME
      JOIN THIRD_PARTY_SERVER server ON server.SERVER_ID = repo.SERVER_ID and (mig.SERVER_URL_FROM_TC = server.URL or CONCAT(mig.SERVER_URL_FROM_TC, '/') = server.URL)
      SET mig.SCM_REPOSITORY_ID = repo.SCM_REPOSITORY_ID, mig.SCM_SERVER_ID = repo.SERVER_ID;

      UPDATE SCM_REPOSITORY_URL_MIGRATION_TEMP mig
      JOIN TEST_CASE_LIBRARY_NODE tcln ON mig.TCLN_ID = tcln.TCLN_ID
      JOIN PROJECT p ON tcln.PROJECT_ID = p.PROJECT_ID
      JOIN SCM_REPOSITORY repo ON p.SCM_REPOSITORY_ID = repo.SCM_REPOSITORY_ID
      JOIN THIRD_PARTY_SERVER server ON server.SERVER_ID = repo.SERVER_ID and (mig.SERVER_URL_FROM_TC = server.URL or CONCAT(mig.SERVER_URL_FROM_TC, '/') = server.URL)
      SET mig.SCM_SERVER_ID = repo.SERVER_ID
      WHERE mig.SCM_SERVER_ID IS NULL;

      UPDATE SCM_REPOSITORY_URL_MIGRATION_TEMP mig
      JOIN SCM_REPOSITORY repo ON mig.REPOSITORY_NAME_FROM_TC = repo.NAME
      JOIN THIRD_PARTY_SERVER server ON server.SERVER_ID = repo.SERVER_ID and (mig.SERVER_URL_FROM_TC = server.URL or CONCAT(mig.SERVER_URL_FROM_TC, '/') = server.URL)
      SET mig.SCM_REPOSITORY_ID = repo.SCM_REPOSITORY_ID, mig.SCM_SERVER_ID = repo.SERVER_ID
      WHERE mig.SCM_REPOSITORY_ID IS NULL;

      UPDATE SCM_REPOSITORY_URL_MIGRATION_TEMP mig
      JOIN THIRD_PARTY_SERVER server ON mig.SERVER_URL_FROM_TC = server.URL or CONCAT(mig.SERVER_URL_FROM_TC, '/') = server.URL
      JOIN SCM_SERVER scmserver ON server.SERVER_ID = scmserver.SERVER_ID
      SET mig.SCM_SERVER_ID = scmserver.SERVER_ID
      WHERE mig.SCM_SERVER_ID IS NULL;
    </sql>

    <!-- If server does not exist, create a scm server with url from test case source code repository url -->
    <sql>
      INSERT INTO THIRD_PARTY_SERVER (NAME, URL, AUTH_POLICY, AUTH_PROTOCOL)
      SELECT CONCAT('mig_git_', mig.TCLN_ID),
              mig.SERVER_URL_FROM_TC,
              'APP_LEVEL',
              'BASIC_AUTH'
      FROM SCM_REPOSITORY_URL_MIGRATION_TEMP mig
      WHERE mig.SCM_SERVER_ID IS NULL
      GROUP BY mig.SERVER_URL_FROM_TC;

      INSERT INTO SCM_SERVER (SERVER_ID, KIND)
      SELECT server.SERVER_ID,
             'git'
      FROM THIRD_PARTY_SERVER server
      LEFT JOIN SCM_SERVER scm ON server.SERVER_ID = scm.SERVER_ID
      WHERE scm.SERVER_ID IS NULL and server.NAME LIKE 'mig_git_%';

      UPDATE SCM_REPOSITORY_URL_MIGRATION_TEMP mig
      JOIN THIRD_PARTY_SERVER server ON server.URL = mig.SERVER_URL_FROM_TC
      SET mig.SCM_SERVER_ID = server.SERVER_ID
      WHERE mig.SCM_SERVER_ID IS NULL;
    </sql>

    <!-- If repository does not exist, create a scm repository with name from test case source code repository url -->
    <sql>
      INSERT INTO SCM_REPOSITORY (SERVER_ID, NAME, REPOSITORY_PATH, WORKING_BRANCH)
      SELECT server.SERVER_ID,
             mig.REPOSITORY_NAME_FROM_TC,
             '',
             'master'
      FROM SCM_REPOSITORY_URL_MIGRATION_TEMP mig
      JOIN THIRD_PARTY_SERVER server ON mig.SCM_SERVER_ID = server.SERVER_ID
      WHERE mig.SCM_REPOSITORY_ID IS NULL
      GROUP BY mig.SOURCE_CODE_REPOSITORY_URL;

      UPDATE SCM_REPOSITORY_URL_MIGRATION_TEMP mig
      JOIN SCM_REPOSITORY repo ON mig.SCM_SERVER_ID = repo.SERVER_ID and mig.REPOSITORY_NAME_FROM_TC = repo.NAME
      SET mig.SCM_REPOSITORY_ID = repo.SCM_REPOSITORY_ID
      WHERE mig.SCM_REPOSITORY_ID IS NULL;
    </sql>

    <!-- At the end, update test case scm repository id  -->
    <sql>
      UPDATE TEST_CASE tc
      JOIN SCM_REPOSITORY_URL_MIGRATION_TEMP mig ON tc.tcln_id = mig.TCLN_ID
      SET tc.SCM_REPOSITORY_ID = mig.SCM_REPOSITORY_ID
      WHERE mig.SCM_REPOSITORY_ID IS NOT NULL;
    </sql>

  </changeSet>

  <changeSet id="tm-1.22.2-SQUASH-2701-3-psql-migration-for-scm-repo-url" author="jprioux" dbms="postgresql">
    <comment>Migration part if scm repository url has been added in tc</comment>

    <!-- Insert data into temporary table -->
    <sql>
      INSERT INTO SCM_REPOSITORY_URL_MIGRATION_TEMP
      SELECT tc.TCLN_ID,
             TRIM(TRAILING '/' FROM TRIM(tc.SOURCE_CODE_REPOSITORY_URL)),
             REGEXP_REPLACE(TRIM(TRAILING '/' FROM TRIM(tc.SOURCE_CODE_REPOSITORY_URL)), '\/[^\/]+$', ''),
             REGEXP_REPLACE(TRIM(TRAILING '/' FROM TRIM(tc.SOURCE_CODE_REPOSITORY_URL)), '.*[\/]', ''),
             null,
             null
      FROM TEST_CASE tc
      WHERE tc.SOURCE_CODE_REPOSITORY_URL IS NOT NULL;
    </sql>

    <!-- Update with scm linked in project if found, else with other existing repo and server ids, then update only with existing server ids.
    All null are for non-existing repos and servers which will be created after -->
    <sql>
      UPDATE SCM_REPOSITORY_URL_MIGRATION_TEMP mig
      SET SCM_REPOSITORY_ID = repo.SCM_REPOSITORY_ID, SCM_SERVER_ID = repo.SERVER_ID
      FROM TEST_CASE_LIBRARY_NODE tcln
      JOIN PROJECT p ON tcln.PROJECT_ID = p.PROJECT_ID
      JOIN SCM_REPOSITORY repo ON p.SCM_REPOSITORY_ID = repo.SCM_REPOSITORY_ID
      JOIN THIRD_PARTY_SERVER server ON server.SERVER_ID = repo.SERVER_ID
      WHERE mig.TCLN_ID = tcln.TCLN_ID
        AND mig.REPOSITORY_NAME_FROM_TC = repo.NAME
        AND (mig.SERVER_URL_FROM_TC = server.URL or CONCAT(mig.SERVER_URL_FROM_TC, '/') = server.URL);

      UPDATE SCM_REPOSITORY_URL_MIGRATION_TEMP mig
      SET SCM_SERVER_ID = repo.SERVER_ID
      FROM TEST_CASE_LIBRARY_NODE tcln
      JOIN PROJECT p ON tcln.PROJECT_ID = p.PROJECT_ID
      JOIN SCM_REPOSITORY repo ON p.SCM_REPOSITORY_ID = repo.SCM_REPOSITORY_ID
      JOIN THIRD_PARTY_SERVER server ON server.SERVER_ID = repo.SERVER_ID
      WHERE mig.SCM_SERVER_ID IS NULL
        AND mig.TCLN_ID = tcln.TCLN_ID
        AND (mig.SERVER_URL_FROM_TC = server.URL or CONCAT(mig.SERVER_URL_FROM_TC, '/') = server.URL);

      UPDATE SCM_REPOSITORY_URL_MIGRATION_TEMP mig
      SET SCM_REPOSITORY_ID = repo.SCM_REPOSITORY_ID, SCM_SERVER_ID = repo.SERVER_ID
      FROM SCM_REPOSITORY repo
      JOIN THIRD_PARTY_SERVER server ON server.SERVER_ID = repo.SERVER_ID
      WHERE mig.SCM_REPOSITORY_ID IS NULL
        AND mig.REPOSITORY_NAME_FROM_TC = repo.NAME
        AND (mig.SERVER_URL_FROM_TC = server.URL or CONCAT(mig.SERVER_URL_FROM_TC, '/') = server.URL);

      UPDATE SCM_REPOSITORY_URL_MIGRATION_TEMP mig
      SET SCM_SERVER_ID = scmserver.SERVER_ID
      FROM THIRD_PARTY_SERVER server
      JOIN SCM_SERVER scmserver ON server.SERVER_ID = scmserver.SERVER_ID
      WHERE mig.SCM_SERVER_ID IS NULL
        AND (mig.SERVER_URL_FROM_TC = server.URL or CONCAT(mig.SERVER_URL_FROM_TC, '/') = server.URL);
    </sql>

    <!-- If server does not exist, create a scm server with url from test case source code repository url -->
    <sql>
      INSERT INTO THIRD_PARTY_SERVER (NAME, URL, AUTH_POLICY, AUTH_PROTOCOL)
      SELECT distinct on (mig.SERVER_URL_FROM_TC) CONCAT('mig_git_', mig.TCLN_ID),
             mig.SERVER_URL_FROM_TC,
             'APP_LEVEL',
             'BASIC_AUTH'
      FROM SCM_REPOSITORY_URL_MIGRATION_TEMP mig
      WHERE mig.SCM_SERVER_ID IS NULL;

      INSERT INTO SCM_SERVER (SERVER_ID, KIND)
      SELECT server.SERVER_ID,
             'git'
      FROM THIRD_PARTY_SERVER server
      LEFT JOIN SCM_SERVER scm ON server.SERVER_ID = scm.SERVER_ID
      WHERE scm.SERVER_ID IS NULL
        and server.NAME LIKE 'mig_git_%';;

      UPDATE SCM_REPOSITORY_URL_MIGRATION_TEMP
      SET SCM_SERVER_ID = server.SERVER_ID
      FROM THIRD_PARTY_SERVER server
      WHERE server.URL = SERVER_URL_FROM_TC and SCM_SERVER_ID IS NULL;
    </sql>

    <!-- If repository does not exist, create a scm repository with name from test case source code repository url -->
    <sql>
      INSERT INTO SCM_REPOSITORY (SERVER_ID, NAME, REPOSITORY_PATH, WORKING_BRANCH)
      SELECT distinct on (mig.SOURCE_CODE_REPOSITORY_URL) server.SERVER_ID,
             mig.REPOSITORY_NAME_FROM_TC,
             '',
             'master'
      FROM SCM_REPOSITORY_URL_MIGRATION_TEMP mig
      JOIN THIRD_PARTY_SERVER server ON mig.SCM_SERVER_ID = server.SERVER_ID
      WHERE mig.SCM_REPOSITORY_ID IS NULL;

      UPDATE SCM_REPOSITORY_URL_MIGRATION_TEMP mig
      SET SCM_REPOSITORY_ID = repo.SCM_REPOSITORY_ID
      FROM SCM_REPOSITORY repo
      WHERE mig.SCM_SERVER_ID = repo.SERVER_ID
        and mig.REPOSITORY_NAME_FROM_TC = repo.NAME
        and mig.SCM_REPOSITORY_ID IS NULL;
    </sql>

    <!-- At the end, update test case scm repository id  -->
    <sql>
      UPDATE TEST_CASE tc
      SET SCM_REPOSITORY_ID = mig.SCM_REPOSITORY_ID
      FROM SCM_REPOSITORY_URL_MIGRATION_TEMP mig
      WHERE tc.TCLN_ID = mig.TCLN_ID
        and mig.SCM_REPOSITORY_ID IS NOT NULL;
    </sql>

  </changeSet>

  <changeSet id="tm-1.22.2-SQUASH-2701-4-drop-old-column-and-temp-table" author="jprioux" dbms="mysql, postgresql">
    <comment>Drop unnecessary old column and the temporary table</comment>
    <dropColumn tableName="TEST_CASE" columnName="SOURCE_CODE_REPOSITORY_URL" />
    <dropTable tableName="SCM_REPOSITORY_URL_MIGRATION_TEMP" />
  </changeSet>




  <changeSet id="tm-1.22.2-SQUASH-2834-modify-scm-repo-unique-constraint" author="jlor">
    <comment>Modify SCM_REPOSITORY unique constraint (name, server_id) to (name, working_branch, server_id)</comment>
    <!-- To do so, since the unique constraint name was not set at creation, we have to drop the whole table and recreate it with the right constraint -->

    <!-- Create temporary table holding SCM_REPOSITORY data -->
    <createTable tableName="SCM_REPOSITORY_DATA_TEMP">
      <column name="SCM_REPOSITORY_ID" type="BIGINT">
        <constraints primaryKey="true" nullable="false"/>
      </column>
      <column name="SERVER_ID" type="BIGINT">
        <constraints nullable="false" foreignKeyName="fk_scm_repository_data_temp_scm_server" references="SCM_SERVER(SERVER_ID)"/>
      </column>
      <column name="NAME" type="VARCHAR(255)">
        <constraints nullable="false"/>
      </column>
      <column name="WORKING_BRANCH" type="VARCHAR(255)">
        <constraints nullable="false"/>
      </column>
      <column name="REPOSITORY_PATH" type="VARCHAR(255)">
        <constraints nullable="false"/>
      </column>
      <column name="WORKING_FOLDER_PATH" type="VARCHAR(255)"/>
    </createTable>
    <!-- Copy SCM_REPOSITORY data into SCM_REPOSITORY_DATA_TEMP -->
    <sql>
      INSERT INTO SCM_REPOSITORY_DATA_TEMP (SCM_REPOSITORY_ID, SERVER_ID, NAME, WORKING_BRANCH, REPOSITORY_PATH, WORKING_FOLDER_PATH)
      SELECT SCM_REPOSITORY_ID, SERVER_ID, NAME, WORKING_BRANCH, REPOSITORY_PATH, WORKING_FOLDER_PATH
      FROM SCM_REPOSITORY;
    </sql>

    <dropForeignKeyConstraint baseTableName="PROJECT" constraintName="fk_project_scm_repository" />
    <dropForeignKeyConstraint baseTableName="TEST_CASE" constraintName="fk_tc_scm_repository" />
    <dropTable tableName="SCM_REPOSITORY" />

    <!-- Recreate original table -->
    <createTable tableName="SCM_REPOSITORY">

      <column name="SCM_REPOSITORY_ID" type="BIGINT" autoIncrement="true" remarks="The auto-generated id of the scm repository.">
        <constraints primaryKey="true" nullable="false"/>
      </column>

      <column name="SERVER_ID" type="BIGINT" remarks="The foreign key to the related SCM_SEVER">
        <constraints nullable="false" foreignKeyName="fk_scm_repository_scm_server" references="SCM_SERVER(SERVER_ID)"/>
      </column>

      <column name="NAME" type="VARCHAR(255)" remarks="The name of the repository.">
        <constraints nullable="false"/>
      </column>

      <column name="WORKING_BRANCH" type="VARCHAR(255)" remarks="The name of the working branch on which SquashTM will push.">
        <constraints nullable="false"/>
      </column>

      <column name="REPOSITORY_PATH" type="VARCHAR(255)" remarks="The absolute path to the local repository on the local server.">
        <constraints nullable="false"/>
      </column>

      <column name="WORKING_FOLDER_PATH" type="VARCHAR(255)" remarks="The relative path of the working folder in which SquashTM will push."/>

    </createTable>

    <addUniqueConstraint constraintName="uniq_scm_repo_name_branch_server_id"
                         tableName="SCM_REPOSITORY" columnNames="NAME, WORKING_BRANCH, SERVER_ID" />

    <createIndex tableName="SCM_REPOSITORY" indexName="idx_fk_scm_repository_scm_server">
      <column name="SERVER_ID" type="BIGINT"/>
    </createIndex>

    <!-- Copy SCM_REPOSITORY_DATA_TEMP into new SCM_REPOSITORY -->
    <sql>
      INSERT INTO SCM_REPOSITORY (SCM_REPOSITORY_ID, SERVER_ID, NAME, WORKING_BRANCH, REPOSITORY_PATH, WORKING_FOLDER_PATH)
      SELECT SCM_REPOSITORY_ID, SERVER_ID, NAME, WORKING_BRANCH, REPOSITORY_PATH, WORKING_FOLDER_PATH
      FROM SCM_REPOSITORY_DATA_TEMP;
    </sql>

    <addForeignKeyConstraint constraintName="fk_project_scm_repository"
                             baseTableName="PROJECT"  baseColumnNames="SCM_REPOSITORY_ID"
                             referencedTableName="SCM_REPOSITORY" referencedColumnNames="SCM_REPOSITORY_ID" />

    <addForeignKeyConstraint constraintName="fk_tc_scm_repository"
                             baseTableName="TEST_CASE" baseColumnNames="SCM_REPOSITORY_ID"
                             referencedTableName="SCM_REPOSITORY" referencedColumnNames="SCM_REPOSITORY_ID" />

    <!-- Drop temporary table -->
    <dropTable tableName="SCM_REPOSITORY_DATA_TEMP" />

  </changeSet>

  <changeSet id="tm-1.22.2-add-auto-test-tech" author="aguilhem">
    <comment>Add new compatible auto. test tech.</comment>
    <sql>
      insert into AUTOMATED_TEST_TECHNOLOGY(NAME, ACTION_PROVIDER_KEY)
      values ('SoapUI', 'soapui/execute@v1'),
      ('Agilitest', 'agilitest/execute@v1'),
      ('UFT', 'uft/execute@v1'),
      ('Ranorex', 'ranorex/execute@v1')
    </sql>
  </changeSet>
</databaseChangeLog>


