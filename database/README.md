Squash TM database schema management
====================================

This module contains liquibase changesets which define database schema and upgrade.
**Changesets of released versions SHOULD NEVER BE MODIFIED** ! It would break debian / redhat packages incremental upgrade.

Additionally the dbdoc is created as part of the build, in the directory 
`target/generated-resources/xml/xslt`.

Along with the Maven build, a pair of script are supplied to the developper for creating its own database (see Generate a developper database).

Creating a new database version
-------------------------------

When you need to create a DB upgrade for a new version, use the groovy script : 

    cd etc
    groovy createNewChangelogs.goovy -DnewVersion="1.2.3"

It should create a bunch of new changelog files and tell you which other files you need to manually modify.


Database properties
--------------------

The following properties configure the database parameters for Liquibase
to use (more at https://www.liquibase.org/documentation/maven/maven_update.html) : 

* `liquibase.url` : the jdbc url to the database schema
* `liquibase.username` : the username to log to the database
* `liquibase.password` : the password to log to the database  
* `liquibase.driver` : (optional) the jdbc driver classname. This one 
is automatically set according to the database profile (see below)
* `dev.liquibase.url` : (optional) URL of your development schema, if you use the database generation script (see below).


Database profiles
-----------------

The database profiles sets which database type should be used 
for the build. 

* `h2` (default) : The database will be H2. It will be created created on the fly with sensible defaults. Also enabled by setting `-Ddatabase=h2`.

* `mysql` : The database will be MySQL. It requires an external instance of MySQL, and the database properties must be provided accordingly (see above), for instance in your $HOME/.m2/settings.xml . Also enabled by setting `-Ddatabase=mysql`.

* `postgresql` : The database will be Postgresql. It requires an external instance of PostGreSQL, and the database properties must be provided accordingly (see above), for instance in your $HOME/.m2/settings.xml . 
Also enabled by setting `-Ddatabase=postgresql`.


Generate a developper database
------------------------------

The script `gen-db` (bat or sh) is a wrapper around the maven liquibase plugin. 

*Usage* : `gen-db <database-profile> [-empty]`

*Parameters* :

*`database-profile` : one of h2, mysql or postgresql. In particular, if that profile is h2 the property `liquibase.url` will be 
forced to point to the usual `../tm/data/squash-tm` (so H2 users wont 
need to change their habits much). Otherwise all the other `liquibase.*` 
will be picked-up according to your own `settings.xml`, for that given profile. 

* `-empty` : if set, the database will be generated empty instead of being populated with sample data

* (settings.xml) : you also need to configure the properties `liquibase.*` for the given database profile in your Maven settings.xml. **Note** : the jdbc url must be provided as `dev.liquibase.url`, not the usual `liquibase.url`.


Good practices for changesets
------------------------------

* If your changeset include data migration, please write a migration test. You can find them in tm-integration/db-migration-tests maven module.

* Try to have a logical changeset organisation. For table creation, one changeset for the whole table. For data transformation think 'transaction', try to make your changeset follow a logical transaction. Think like if you are coding a service and want to have proper rollback if the changeset failed...

* When making tables fusion or migrating pk/fk/uniques constrained columns it's often easier to perform the migration with the following plan :
	* Copy all the table data in one temporary table
	* Modify/ Add your data in this temporary table, by using tmp columns if needed
	* Clear the original table(s) if you must continue to use them
	* Insert back the modified data in the required table(s)
	* I **STRONGLY** recommend avoiding migrations with sql updates statement directly in final table when pk/fk constraints are in your scope. **With the update method, all your constraint must be valid at each stage of the migration (aka after EACH statement)**. It lead to violation constraint crashes that are often only visible on a real production dataset (aka when our customers run the migration script against their production database !). We had some very bad experience with updates statement on pk columns when 1.22.0 version was released, leading to multiple changeset overrides and a truly unstable migration for our customers. I hope to never see that again so please ear my call and do clean migrations !
	* Of course, try to NOT deactivate constraints during data migration, they are here to protect you.
	
* If you modify a PK column with postgresql don't forget to set the related sequence to max value.
