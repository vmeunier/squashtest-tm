/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.form.model;

import org.squashtest.tm.domain.customfield.CustomField;
import org.squashtest.tm.domain.customfield.CustomFieldOption;
import org.squashtest.tm.domain.customfield.InputType;
import org.squashtest.tm.domain.customfield.MultiSelectField;
import org.squashtest.tm.domain.customfield.NumericField;
import org.squashtest.tm.domain.customfield.RichTextField;
import org.squashtest.tm.domain.customfield.SingleSelectField;

public class CustomFieldFormModel {

	private String name;
	private String label;
	private String code;
	private InputType inputType;
	private boolean optional;
	private String[][] options;
	private String defaultValue;

	public CustomField getCustomField() {
		CustomField customField;

		switch (inputType) {
			case DROPDOWN_LIST:
				customField = createSingleSelectField();
				break;
			case RICH_TEXT:
				customField = createRichTextField();
				break;
			case TAG:
				customField = createTag();
				break;
			case NUMERIC:
				customField = createNumeric();
				break;
			default:
				customField = new CustomField(inputType);
		}

		customField.setCode(code);
		customField.setLabel(label);
		customField.setName(name);
		customField.setOptional(optional);
		customField.setDefaultValue(defaultValue);

		return customField;
	}

	private CustomField createNumeric() {
		return new NumericField();
	}

	private CustomField createSingleSelectField() {
		CustomField res;
		SingleSelectField ssf = new SingleSelectField();

		for (String[] option : options) {
			String label = option[0];
			String code = option[1];
			String colour = option[2];
			ssf.addOption(new CustomFieldOption(label, code, colour));
		}

		res = ssf;
		return res;
	}

	private CustomField createRichTextField() {
		return new RichTextField();
	}

	private CustomField createTag() {
		return new MultiSelectField();
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setInputType(InputType inputType) {
		this.inputType = inputType;
	}

	public void setOptional(boolean optional) {
		this.optional = optional;
	}

	public void setOptions(String[][] options) {
		this.options = options;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
}
