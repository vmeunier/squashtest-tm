/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.report;

import org.squashtest.tm.api.report.form.BasicInput;
import org.squashtest.tm.api.report.form.CheckboxInput;
import org.squashtest.tm.api.report.form.Input;
import org.squashtest.tm.api.report.form.InputType;
import org.squashtest.tm.api.report.form.InputVisitor;
import org.squashtest.tm.api.report.form.InputsGroup;
import org.squashtest.tm.api.report.form.LabelInput;
import org.squashtest.tm.api.report.form.OptionsGroup;
import org.squashtest.tm.api.report.form.TreePicker;

class BasicInputJson implements InputJson {
	private final String name;
	private final InputType type;
	private final String disabledBy;
	private final String label;
	private final boolean required;

	public BasicInputJson(BasicInput input) {
		this.name = input.getName();
		this.type = input.getType();
		this.disabledBy = input.getDisabledBy();
		this.label = input.getLabel();
		this.required = input.isRequired();
	}

	@Override
	public String getName() {
		return name;
	}

	public InputType getType() {
		return type;
	}

	public String getDisabledBy() {
		return disabledBy;
	}

	public static InputJson fromInput(Input input) {
		InputJson[] basicInputJsons = new InputJson[1];

		InputVisitor inputVisitor = new InputVisitor() {
			@Override
			public void visit(OptionsGroup optionsGroup) {
				basicInputJsons[0] = new OptionsGroupJson(optionsGroup);
			}

			@Override
			public void visit(InputsGroup inputsGroup) {
				basicInputJsons[0] = new InputsGroupJson(inputsGroup);
			}

			@Override
			public void visit(CheckboxInput checkboxInput) {
				basicInputJsons[0] = new OptionInputJson.CheckBoxInputJson(checkboxInput);
			}

			@Override
			public void visit(TreePicker treePicker) {
				basicInputJsons[0] = new TreePickerJson(treePicker);
			}

			@Override
			public void visit(LabelInput labelInput) {
				basicInputJsons[0] = new BasicInputJson(labelInput);
			}
		};

		input.accept(inputVisitor);

		return basicInputJsons[0];
	}

	public String getLabel() {
		return label;
	}

	public boolean isRequired() {
		return required;
	}
}
