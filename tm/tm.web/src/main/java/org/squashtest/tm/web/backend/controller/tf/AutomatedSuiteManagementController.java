/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.tf;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.testautomation.AutomatedSuite;
import org.squashtest.tm.service.testautomation.AutomatedSuiteManagerService;
import org.squashtest.tm.service.testautomation.model.AutomatedSuiteCreationSpecification;
import org.squashtest.tm.service.testautomation.model.AutomatedSuitePreview;
import org.squashtest.tm.service.testautomation.model.AutomatedSuiteWithSquashAutomAutomatedITPIs;

import javax.inject.Inject;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("backend/automated-suites")
public class AutomatedSuiteManagementController {

	private static final Logger LOGGER = LoggerFactory.getLogger(AutomatedSuiteManagementController.class);

	@Inject
	private AutomatedSuiteManagerService automatedSuiteService;

	@RequestMapping(value = "/preview", method = RequestMethod.POST)
	@ResponseBody
	public AutomatedSuitePreview generateSuitePreview(@RequestBody AutomatedSuiteCreationSpecification specification){
		return automatedSuiteService.preview(specification);
	}

	@ResponseBody
	@RequestMapping(value = "/preview/test-list", method = RequestMethod.POST, params = "auto-project-id")
	public List<String> findTestListPreview(
		@RequestBody AutomatedSuiteCreationSpecification specification,
		@RequestParam("auto-project-id") Long automatedProjectId){
		return automatedSuiteService.findTestListPreview(specification, automatedProjectId);
	}

	@ResponseBody
	@RequestMapping(value = "/create-and-execute", method = RequestMethod.POST)
	public AutomatedExecutionViewUtils.AutomatedSuiteOverview createAndExecute(@RequestBody AutomatedSuiteCreationSpecification specification){
		AutomatedSuiteWithSquashAutomAutomatedITPIs suite = automatedSuiteService.createAndExecute(specification);
		Date startDate = new Date();
		LOGGER.debug("START CREATING AUTOMATED SUITE OVERVIEW " + startDate);
		AutomatedExecutionViewUtils.AutomatedSuiteOverview automatedSuiteOverview = AutomatedExecutionViewUtils.buildExecInfo(suite);
		Date endDate = new Date();
		LOGGER.debug("END CREATING AUTOMATED SUITE OVERVIEW " + endDate);
		return automatedSuiteOverview;
	}

	@ResponseBody
	@RequestMapping(value = "/{suiteId}/executions", method = RequestMethod.GET)
	public AutomatedExecutionViewUtils.AutomatedSuiteOverview updateExecutionInfo(@PathVariable String suiteId) {
		AutomatedSuite suite = automatedSuiteService.findById(suiteId);
		return AutomatedExecutionViewUtils.buildExecInfo(suite);
	}

}
