/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.customreport;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.service.display.customreport.CustomReportDisplayService;
import org.squashtest.tm.service.internal.display.dto.LibraryDto;

@RestController
@RequestMapping("/backend/custom-report-library-view")
public class CustomReportLibraryViewController {

	private CustomReportDisplayService customReportDisplayService;

	public CustomReportLibraryViewController(CustomReportDisplayService customReportDisplayService) {
		this.customReportDisplayService = customReportDisplayService;
	}

	@RequestMapping(value = "/{customReportLibraryId}", method = RequestMethod.GET)
	public LibraryDto getCampaignView(@PathVariable long customReportLibraryId) {
		return customReportDisplayService.getCustomReportLibraryView(customReportLibraryId);
	}

}

