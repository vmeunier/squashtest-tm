/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.test.automation.server;

import org.springframework.web.bind.annotation.*;
import org.squashtest.tm.service.display.test.automation.server.TestAutomationServerDisplayService;
import org.squashtest.tm.service.internal.display.dto.CredentialsDto;
import org.squashtest.tm.service.internal.display.dto.TestAutomationServerAdminViewDto;
import org.squashtest.tm.service.servers.ManageableCredentials;
import org.squashtest.tm.service.servers.StoredCredentialsManager;

import javax.inject.Inject;

@RestController
@RequestMapping("/backend/test-automation-server-view")
public class TestAutomationServerViewController {

	private final TestAutomationServerDisplayService testAutomationServerDisplayService;

	@Inject
	TestAutomationServerViewController(TestAutomationServerDisplayService testAutomationServerDisplayService) {
		this.testAutomationServerDisplayService = testAutomationServerDisplayService;
	}

	@RequestMapping(value = "/{testAutomationServerId}", method = RequestMethod.GET)
	@ResponseBody
	public TestAutomationServerAdminViewDto getView(@PathVariable long testAutomationServerId) {
		return testAutomationServerDisplayService.getTestAutomationServerView(testAutomationServerId);
	}
}
