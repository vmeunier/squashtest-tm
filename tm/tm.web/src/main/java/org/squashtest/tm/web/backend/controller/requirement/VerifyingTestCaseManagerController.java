/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.requirement;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.squashtest.tm.exception.requirement.VerifiedRequirementException;
import org.squashtest.tm.service.display.requirements.RequirementDisplayService;
import org.squashtest.tm.service.internal.display.dto.requirement.VerifyingTestCaseDto;
import org.squashtest.tm.service.testcase.VerifyingTestCaseManagerService;
import org.squashtest.tm.web.backend.helper.VerifiedRequirementActionSummaryBuilder;

import java.util.Collection;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "backend/requirement-version/{requirementVersionId}")
public class VerifyingTestCaseManagerController {

	private VerifyingTestCaseManagerService verifyingTestCaseManager;
	private RequirementDisplayService requirementDisplayService;

	public VerifyingTestCaseManagerController(VerifyingTestCaseManagerService verifyingTestCaseManager, RequirementDisplayService requirementDisplayService) {
		this.verifyingTestCaseManager = verifyingTestCaseManager;
		this.requirementDisplayService = requirementDisplayService;
	}

	@ResponseBody
	@DeleteMapping(value = "/verifying-test-cases/{testCaseIds}")
	public ChangeVerifyingTestCasesOperationReport removeVerifyingTestCaseFromRequirement(@PathVariable("requirementVersionId") long requirementVersionId,
													   @PathVariable("testCaseIds") List<Long> testCaseIds) {
		verifyingTestCaseManager.removeVerifyingTestCasesFromRequirementVersion(testCaseIds, requirementVersionId);
		List<VerifyingTestCaseDto> verifyingTestCasesDto = requirementDisplayService.findVerifyingTestCasesByRequirementVersionId(requirementVersionId);
		return new ChangeVerifyingTestCasesOperationReport(verifyingTestCasesDto);
	}

	@ResponseBody
	@PostMapping(value = "/verifying-test-cases")
	public ChangeVerifyingTestCasesOperationReport addVerifyingTestCasesToRequirement(@RequestBody TestCaseIdsForm testCaseIdsForm, @PathVariable long requirementVersionId) {
		Map<String, Collection<?>> rejectionsAndIds =
			verifyingTestCaseManager.addVerifyingTestCasesToRequirementVersion(testCaseIdsForm.testCaseIds, requirementVersionId);
		Collection<VerifiedRequirementException> rejections = (Collection<VerifiedRequirementException>) rejectionsAndIds.get(VerifyingTestCaseManagerService.REJECTION_KEY);
		Map<String, Object> result = buildSummary(rejections);
		List<VerifyingTestCaseDto> verifyingTestCasesDto = requirementDisplayService.findVerifyingTestCasesByRequirementVersionId(requirementVersionId);
		ChangeVerifyingTestCasesOperationReport report = new ChangeVerifyingTestCasesOperationReport(verifyingTestCasesDto);
		report.setSummary(result);
		return report;
	}

	private Map<String, Object> buildSummary(Collection<VerifiedRequirementException> rejections) {
		return VerifiedRequirementActionSummaryBuilder.buildAddActionSummary(rejections);
	}

	static class TestCaseIdsForm {
		private List<Long> testCaseIds;

		public List<Long> getTestCaseIds() {
			return testCaseIds;
		}

		public void setTestCaseIds(List<Long> testCaseIds) {
			this.testCaseIds = testCaseIds;
		}
	}

	static class ChangeVerifyingTestCasesOperationReport {
		private List<VerifyingTestCaseDto> verifyingTestCases;
		private Map<String, Object> summary;
		private Map<Long, VerifiedRequirementException> exceptions;

		public ChangeVerifyingTestCasesOperationReport(List<VerifyingTestCaseDto> verifyingTestCases) {
			this.verifyingTestCases = verifyingTestCases;
		}

		public List<VerifyingTestCaseDto> getVerifyingTestCases() {
			return verifyingTestCases;
		}

		public void setVerifyingTestCases(List<VerifyingTestCaseDto> verifyingTestCases) {
			this.verifyingTestCases = verifyingTestCases;
		}

		public Map<String, Object> getSummary() {
			return summary;
		}

		public void setSummary(Map<String, Object> summary) {
			this.summary = summary;
		}

		public Map<Long, VerifiedRequirementException> getExceptions() {
			return exceptions;
		}

		public void setExceptions(Map<Long, VerifiedRequirementException> exceptions) {
			this.exceptions = exceptions;
		}
	}
}
