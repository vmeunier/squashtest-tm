/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.testcase.parameters;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.exception.DuplicateNameException;
import org.squashtest.tm.exception.NameAlreadyInUseException;
import org.squashtest.tm.service.internal.display.testcase.parameter.NewDatasetData;
import org.squashtest.tm.service.testcase.DatasetModificationService;

@Controller
@RequestMapping(value = "backend/datasets")
public class DatasetModificationController {

	private DatasetModificationService datasetModificationService;

	public DatasetModificationController(DatasetModificationService datasetModificationService) {
		this.datasetModificationService = datasetModificationService;
	}

	@RequestMapping(value = "/{dataSetId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteDataSet(@PathVariable Long dataSetId) {
		datasetModificationService.removeById(dataSetId);
	}

	@RequestMapping(value = "/rename", method = RequestMethod.POST)
	@ResponseBody
	public void rename(@RequestBody NewDatasetData dataset) {
		try {
			datasetModificationService.changeName(dataset.getId(), dataset.getName());
		} catch (DuplicateNameException ex) {
			throw new NameAlreadyInUseException("Dataset", dataset.getName());
		}
	}
}
