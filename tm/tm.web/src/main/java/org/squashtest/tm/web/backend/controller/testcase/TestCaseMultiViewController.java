/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.testcase;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.NodeReferences;
import org.squashtest.tm.domain.Workspace;
import org.squashtest.tm.domain.customreport.CustomReportDashboard;
import org.squashtest.tm.service.customreport.CustomReportLibraryNodeService;
import org.squashtest.tm.service.display.testcase.TestCaseDisplayService;
import org.squashtest.tm.service.internal.display.dto.testcase.TestCaseMultiSelectionDto;
import org.squashtest.tm.service.internal.dto.json.JsonCustomReportDashboard;
import org.squashtest.tm.service.testcase.TestCaseLibraryNavigationService;
import org.squashtest.tm.web.backend.controller.form.model.NodeList;
import org.squashtest.tm.web.backend.model.builder.JsonCustomReportDashboardBuilder;

import javax.inject.Named;
import javax.inject.Provider;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/backend/test-case-workspace-multi-view")
public class TestCaseMultiViewController {

	private final TestCaseDisplayService testCaseDisplayService;
	private final CustomReportLibraryNodeService customReportLibraryNodeService;
	@Named("customReport.dashboardBuilder")
	private final Provider<JsonCustomReportDashboardBuilder> builderProvider;
	private final TestCaseLibraryNavigationService testCaseLibraryNavigationService;


	public TestCaseMultiViewController(TestCaseDisplayService testCaseDisplayService,
									   CustomReportLibraryNodeService customReportLibraryNodeService,
									   Provider<JsonCustomReportDashboardBuilder> builderProvider,
									   TestCaseLibraryNavigationService testCaseLibraryNavigationService) {
		this.testCaseDisplayService = testCaseDisplayService;
		this.builderProvider = builderProvider;
		this.customReportLibraryNodeService = customReportLibraryNodeService;
		this.testCaseLibraryNavigationService = testCaseLibraryNavigationService;
	}

	@ResponseBody
	@PostMapping
	public TestCaseMultiSelectionDto getTestCaseMultiView(@RequestBody NodeList statisticRequest, Locale locale) {
		NodeReferences nodeReferences = statisticRequest.asNodeReferences();

		TestCaseMultiSelectionDto testCaseMultiSelectionDto = testCaseDisplayService.getTestCaseMultiView();
		if (testCaseMultiSelectionDto.isShouldShowFavoriteDashboard()) {
			if (testCaseMultiSelectionDto.isCanShowFavoriteDashboard()) {
				List<String> references = statisticRequest.getReferences();
				List<EntityReference> entityReferences = references.stream().map(EntityReference::fromNodeId).collect(Collectors.toList());
				CustomReportDashboard dashboard = customReportLibraryNodeService.findCustomReportDashboardById(testCaseMultiSelectionDto.getFavoriteDashboardId());
				JsonCustomReportDashboard jsonDashboard = builderProvider.get()
					.build(testCaseMultiSelectionDto.getFavoriteDashboardId(), dashboard, locale, entityReferences, false, Workspace.TEST_CASE);
				testCaseMultiSelectionDto.setDashboard(jsonDashboard);
			}
		} else {
			Set<Long> libraryIds = nodeReferences.extractLibraryIds();
			Set<Long> testCaseNodeIds = nodeReferences.extractNonLibraryIds();
			testCaseMultiSelectionDto.setStatistics(testCaseLibraryNavigationService
				.getStatisticsForSelection(libraryIds, testCaseNodeIds));
		}
		return testCaseMultiSelectionDto;
	}

}
