/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.project;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.api.plugin.EntityReference;
import org.squashtest.tm.api.plugin.EntityType;
import org.squashtest.tm.api.plugin.PluginValidationException;
import org.squashtest.tm.api.wizard.WorkspaceWizard;
import org.squashtest.tm.domain.users.Party;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.service.display.project.ProjectDisplayService;
import org.squashtest.tm.service.internal.display.dto.BindMilestoneToProjectDialogData;
import org.squashtest.tm.service.internal.display.dto.ProjectPluginDto;
import org.squashtest.tm.service.internal.display.dto.ProjectViewDto;
import org.squashtest.tm.service.internal.display.dto.TestAutomationProjectDto;
import org.squashtest.tm.service.internal.utils.HTMLCleanupUtils;
import org.squashtest.tm.service.project.GenericProjectManagerService;
import org.squashtest.tm.web.backend.manager.wizard.WorkspaceWizardManager;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/backend/project-view")
public class ProjectViewController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProjectViewController.class);
	private static final String STATUS_ERROR = "ERROR";
	private static final String STATUS_OK = "OK";

	private final ProjectDisplayService projectViewDisplayService;
	private final GenericProjectManagerService projectManager;
	private final WorkspaceWizardManager pluginManager;

	@Inject
	ProjectViewController(ProjectDisplayService projectViewDisplayService,
						  GenericProjectManagerService projectManager,
						  WorkspaceWizardManager pluginManager) {
		this.projectViewDisplayService = projectViewDisplayService;
		this.projectManager = projectManager;
		this.pluginManager = pluginManager;
	}

	@RequestMapping(value = "/{projectId}", method = RequestMethod.GET)
	public ProjectViewDto getProjectView(@PathVariable long projectId) {
		ProjectViewDto dto = projectViewDisplayService.getProjectView(projectId);
		dto.setAvailablePlugins(getAvailablePlugins(projectId, dto.isTemplate()));
		return dto;
	}

	@RequestMapping(value = "/{projectId}/available-milestones", method = RequestMethod.GET)
	public BindMilestoneToProjectDialogData getAvailableMilestones(@PathVariable long projectId) {
		return projectViewDisplayService.findAvailableMilestones(projectId);
	}

	@GetMapping(value = "/{projectId}/unbound-parties")
	public Map<String, Object> getUnboundParties(@PathVariable long projectId) {
		Map<String, Object> unboundParties = new HashMap<>();

		List<Party> partyList = projectManager.findPartyWithoutPermissionByProject(projectId);

		List<Map<String, Object>> usersModel = new ArrayList<>();
		List<Map<String, Object>> teamsModel = new ArrayList<>();


		for (Party p : partyList) {
			boolean isUser = User.class.isAssignableFrom(p.getClass());
			Map<String, Object> newModel = new HashMap<>();

			newModel.put("label", HTMLCleanupUtils.cleanAndUnescapeHTML(p.getName()));
			newModel.put("id", p.getId());

			if (isUser) {
				usersModel.add(newModel);
			} else {
				teamsModel.add(newModel);
			}
		}

		unboundParties.put("users", usersModel);
		unboundParties.put("teams", teamsModel);

		return unboundParties;
	}

	@GetMapping(value = "/{projectId}/available-ta-projects")
	@ResponseBody
	public Map<String, List<TestAutomationProjectDto>> getAvailableTAProjects(@PathVariable long projectId) {
		return Collections.singletonMap("taProjects", projectManager.findAllAvailableTaProjects(projectId).stream()
			.map(taProject -> {
				final TestAutomationProjectDto dto = new TestAutomationProjectDto();
				dto.setRemoteName(taProject.getJobName());
				dto.setLabel(taProject.getLabel());
				return dto;
			}).collect(Collectors.toList()));
	}

	private List<ProjectPluginDto> getAvailablePlugins(long projectId, boolean projectIsTemplate) {
		Collection<WorkspaceWizard> plugins = pluginManager.findAll();
		return toPluginDto(projectId, plugins, projectIsTemplate);
	}

	private List<ProjectPluginDto> toPluginDto(long projectId, Collection<WorkspaceWizard> plugins, boolean isTemplate) {

		List<ProjectPluginDto> output = new ArrayList<>(plugins.size());

		EntityReference context = new EntityReference(EntityType.PROJECT, projectId);

		int loop=1;
		for (WorkspaceWizard plugin : plugins) {

			boolean enabled = pluginManager.isActivePlugin(plugin, projectId);
			boolean hasConf = pluginManager.hasConfiguration(plugin, projectId);
			ProjectPluginDto projectPluginDto = new ProjectPluginDto(plugin);

			projectPluginDto.setIndex(loop++);
			projectPluginDto.setEnabled(enabled);
			projectPluginDto.setHasConf(hasConf);

			projectPluginDto.setPluginType(plugin.getPluginType());

			String url = plugin.getConfigurationPath(context);

			if(url != null && !isTemplate) {
				projectPluginDto.setConfigUrl(url);
			}else projectPluginDto.setConfigUrl("");

			// that should be refactored too once the API is updated
			try{
				plugin.validate(context);
				projectPluginDto.setStatus(STATUS_OK);
			}
			catch(PluginValidationException damnit){
				projectPluginDto.setStatus(STATUS_ERROR);
				LOGGER.debug("Plugin validation failed for Plugin " + plugin.getName(), damnit);
			}

			output.add(projectPluginDto);
		}

		return output;
	}
}
