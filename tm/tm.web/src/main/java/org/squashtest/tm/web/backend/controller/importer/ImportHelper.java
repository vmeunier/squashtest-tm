/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.importer;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.squashtest.tm.service.batchimport.excel.TemplateMismatchException;
import org.squashtest.tm.service.importer.ImportLog;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

@Component
public abstract class ImportHelper {
	private static final Logger LOGGER = LoggerFactory.getLogger(ImportHelper.class);
	public static final String TEST_CASES = "test-cases";
	public static final String REQUIREMENT = "requirement";
	public static final String SUMMARY = "summary";


	public interface Command<T, U> {
		U execute(T arg);
	}

	public File multipartToImportFile(MultipartFile uploadedFile, String prefix, String suffix) throws IOException {
		InputStream is = uploadedFile.getInputStream();
		File file = File.createTempFile(prefix, suffix);
		BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(file));
		IOUtils.copy(is, os);
		IOUtils.closeQuietly(os);
		return file;
	}

	public ImportExcelResponse importWorkbook(String element, MultipartFile uploadedFile, Command<File, ImportLog> callback) {
		ModelAndView mav = new ModelAndView("fragment/import/import-summary");

		File xls = null;
		ImportExcelResponse importExcelResponse = new ImportExcelResponse();
		try {
			String prefix = (TEST_CASES.equals(element)) ? "test-case-import-" : "requirement-import-";
			xls = multipartToImportFile(uploadedFile, prefix, ".xls");
			ImportLog summary = callback.execute(xls); // TODO parser may throw ex we should handle
			summary.recompute(); // TODO why is it here ? shouldnt it be in service ?
			generateImportLog(element, summary);
			mav.addObject(SUMMARY, summary);
			importExcelResponse.setTemplateOk(summary);
		} catch (IOException e) {
			LOGGER.error("An exception prevented processing of " + element + " import file", e);

		} catch (TemplateMismatchException tme) {
			ImportFormatFailure importFormatFailure = new ImportFormatFailure(tme);
			mav.addObject(SUMMARY, importFormatFailure);
			importExcelResponse.setImportFormatFailure(importFormatFailure);
		} finally {
			if (xls != null) {
				xls.deleteOnExit();
			}
		}
		String mavValue = (TEST_CASES.equals(element)) ? "test-case" : "requirement";
		mav.addObject("workspace", mavValue);

		return importExcelResponse;
	}

	/**
	 * Generates a downloadable xls import log file and stores it where it should.
	 *
	 * @param element
	 *            : either test-cases or requirement
	 * @param summary
	 *            : the {@link ImportLog} summary of the xls import/simulation
	 */
	private void generateImportLog(String element, ImportLog summary) {
		File xlsSummary = null;

		try {
			xlsSummary = importLogToLogFile(summary);

			String reportUrl = element+"/import-logs/" + xlsSummary.getName();
			summary.setReportUrl(reportUrl);

		} catch (IOException e) {
			LOGGER.warn("An error occurred during import log generation", e);

		} finally {
			if (xlsSummary != null) {
				xlsSummary.deleteOnExit();
			}
		}
	}

	protected abstract File importLogToLogFile(ImportLog summary) throws IOException;
}
