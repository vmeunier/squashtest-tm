/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.report;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.squashtest.tm.api.report.Report;
import org.squashtest.tm.api.report.criteria.Criteria;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.service.project.ProjectFinder;
import org.squashtest.tm.web.backend.helper.JsonHelper;
import org.squashtest.tm.web.backend.report.ReportsRegistry;
import org.squashtest.tm.web.backend.report.criteria.ConciseFormToCriteriaConverter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("backend/reports/{namespace}")
public class ReportGenerationController {

	private final ReportsRegistry reportsRegistry;

	private final ProjectFinder projectFinder;

	public ReportGenerationController(ReportsRegistry reportsRegistry, ProjectFinder projectFinder) {
		this.reportsRegistry = reportsRegistry;
		this.projectFinder = projectFinder;
	}

	@RequestMapping(value = "/views/{viewIndex}/formats/{format}", method = RequestMethod.GET, params = {"json"})
	public ModelAndView getReportView(@PathVariable String namespace,
									  @PathVariable int viewIndex, @PathVariable String format, @RequestParam("json") String parameters)
		throws IOException {
		Map<String, Object> form = JsonHelper.deserialize(parameters);
		Report report = reportsRegistry.findReport(namespace);
		List<Project> projects = projectFinder.findAllOrderedByName();
		Map<String, Criteria> crit = new ConciseFormToCriteriaConverter(report, projects).convert(form);
		return report.buildModelAndView(viewIndex, format, crit);
	}

	@RequestMapping(value = "/views/{viewIndex}/data/docx", method = RequestMethod.GET, params = {"json"})
	@ResponseBody
	public Map<String, Object> getReportView(@PathVariable String namespace,
											 @PathVariable int viewIndex, @RequestParam("json") String parameters)
		throws IOException {
		Map<String, Object> form = JsonHelper.deserialize(parameters);
		Report report = reportsRegistry.findReport(namespace);
		List<Project> projects = projectFinder.findAllOrderedByName();
		Map<String, Criteria> crit = new ConciseFormToCriteriaConverter(report, projects).convert(form);
		// the old report API is based on spring view so to avoid a complete redesign of the API
		// we simply extract the model as response body
		return report.buildModelAndView(viewIndex, "docx", crit).getModel();
	}


	@RequestMapping(value = "/views/{viewIndex}/docxtemplate", method = RequestMethod.GET)
	public void getTemplate(@PathVariable String namespace, @PathVariable int viewIndex, HttpServletRequest request, HttpServletResponse response) throws Exception {
		Report report = reportsRegistry.findReport(namespace);
		report.getViews()[viewIndex].getSpringView().render(null, request, response);
	}
}
