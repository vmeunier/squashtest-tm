/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.campaign

import org.squashtest.tm.api.security.acls.Roles
import org.squashtest.tm.domain.campaign.Iteration
import org.squashtest.tm.service.internal.display.grid.GridRequest
import org.squashtest.tm.service.internal.display.grid.filters.GridFilterOperation
import org.squashtest.tm.service.security.PermissionEvaluationService
import org.squashtest.tm.service.security.UserContextService
import spock.lang.Specification

import static org.squashtest.tm.service.internal.display.campaign.ReadUnassignedTestPlanHelper.READ_UNASSIGNED

class ReadUnassignedTestPlanHelperTest extends Specification {

	PermissionEvaluationService permissionEvaluationService = Mock(PermissionEvaluationService.class)
	UserContextService userContextService = Mock(UserContextService.class)
	ReadUnassignedTestPlanHelper helper;

	def setup() {
		helper = new ReadUnassignedTestPlanHelper(permissionEvaluationService, userContextService)
	}

	def "should append filter if current user has not read unassigned permission"() {
		given:
		permissionEvaluationService.hasRoleOrPermissionOnObject(Roles.ROLE_ADMIN, READ_UNASSIGNED, 1L, _) >> false
		userContextService.getUsername() >> "no_perm"

		and:
		GridRequest request = new GridRequest()

		when:
		helper.appendReadUnassignedFilter(request, 1L, Iteration.SIMPLE_CASS_NAME)

		then:
		request.filterValues.size() == 1
		def filterValue = request.filterValues.get(0)
		filterValue.id == "login"
		filterValue.operation == GridFilterOperation.EQUALS.name()
		filterValue.values == ["no_perm"]
	}

	def "should not append filter if current user has read unassigned permission"() {
		given:
		permissionEvaluationService.hasRoleOrPermissionOnObject(Roles.ROLE_ADMIN, READ_UNASSIGNED, 1L, _) >> true

		and:
		GridRequest request = new GridRequest()

		when:
		helper.appendReadUnassignedFilter(request, 1L, Iteration.SIMPLE_CASS_NAME)

		then:
		request.filterValues.size() == 0
	}
}
