/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.testcase.parameter;

import org.squashtest.tm.service.internal.display.dto.testcase.DataSetDto;
import org.squashtest.tm.service.internal.display.dto.testcase.DatasetParamValueDto;
import org.squashtest.tm.service.internal.display.dto.testcase.ParameterDto;

import java.util.ArrayList;
import java.util.List;

public class TestCaseParameterOperationReport {

	private List<ParameterDto> parameters = new ArrayList<>();
	private List<DataSetDto> dataSets = new ArrayList<>();
	private List<DatasetParamValueDto> paramValues = new ArrayList<>();

	public List<ParameterDto> getParameters() {
		return parameters;
	}

	public void setParameters(List<ParameterDto> parameters) {
		this.parameters = parameters;
	}

	public List<DataSetDto> getDataSets() {
		return dataSets;
	}

	public void setDataSets(List<DataSetDto> dataSets) {
		this.dataSets = dataSets;
	}

	public List<DatasetParamValueDto> getParamValues() {
		return paramValues;
	}

	public void setParamValues(List<DatasetParamValueDto> paramValues) {
		this.paramValues = paramValues;
	}
}
