/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.administration;

import org.jooq.Field;
import org.jooq.SortField;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.squashtest.tm.service.internal.display.grid.AbstractGrid;
import org.squashtest.tm.service.internal.display.grid.columns.GridColumn;

import java.util.Arrays;
import java.util.List;

import static org.squashtest.tm.jooq.domain.Tables.CUSTOM_FIELD;
import static org.squashtest.tm.jooq.domain.Tables.CUSTOM_FIELD_BINDING;

@Deprecated
public class ProjectCustomFieldGrid extends AbstractGrid {

	private final String CUSTOM_FIELD_ID = "CUSTOM_FIELD_ID";
	private final String CUSTOM_FIELD_BINDING_ID = "CUSTOM_FIELD_BINDING_ID";
	private final String PROJECT_ID = "PROJECT_ID";

	private long projectId;

	public ProjectCustomFieldGrid(long projectId) {
		this.projectId = projectId;
	}

	@Override
	protected List<GridColumn> getColumns() {
		return Arrays.asList(
			new GridColumn(DSL.field(PROJECT_ID)),
			new GridColumn(DSL.field(CUSTOM_FIELD_ID)),
			new GridColumn(DSL.field(CUSTOM_FIELD_BINDING_ID)),
			new GridColumn(DSL.field("NAME")),
			new GridColumn(DSL.field("LABEL")),
			new GridColumn(DSL.field("OPTIONAL")),
			new GridColumn(DSL.field("FIELD_TYPE")),
			new CustomFieldBoundEntityColumn(DSL.field("BOUND_ENTITY", String.class)),
			new GridColumn(DSL.field("INPUT_TYPE"))
		);
	}

	@Override
	protected Table<?> getTable() {
		return DSL
			.select(
				CUSTOM_FIELD_BINDING.BOUND_PROJECT_ID.as(PROJECT_ID),
				CUSTOM_FIELD.CF_ID.as(CUSTOM_FIELD_ID),
				CUSTOM_FIELD_BINDING.CFB_ID.as(CUSTOM_FIELD_BINDING_ID),
				CUSTOM_FIELD.NAME,
				CUSTOM_FIELD.LABEL,
				CUSTOM_FIELD.OPTIONAL,
				CUSTOM_FIELD.FIELD_TYPE,
				CUSTOM_FIELD.INPUT_TYPE,
				CUSTOM_FIELD_BINDING.BOUND_ENTITY
			)
			.from(CUSTOM_FIELD)
			.join(CUSTOM_FIELD_BINDING)
				.on(CUSTOM_FIELD_BINDING.CF_ID.eq(CUSTOM_FIELD.CF_ID)
					.and(CUSTOM_FIELD_BINDING.BOUND_PROJECT_ID.eq(projectId)))
			.asTable();
	}

	@Override
	protected Field<?> getIdentifier() {
		return DSL.field(CUSTOM_FIELD_BINDING_ID);
	}

	@Override
	protected Field<?> getProjectIdentifier() {
		return null;
	}

	@Override
	protected SortField<?> getDefaultOrder() {
		return DSL.field(CUSTOM_FIELD_BINDING_ID).asc();
	}
}
