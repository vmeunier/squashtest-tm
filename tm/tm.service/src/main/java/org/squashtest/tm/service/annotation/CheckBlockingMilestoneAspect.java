/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.annotation;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.milestone.MilestoneMember;
import org.squashtest.tm.exception.requirement.MilestoneForbidModificationException;
import org.squashtest.tm.service.internal.repository.MilestoneDao;

import javax.inject.Inject;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Collection;

/**
 * This aspect manages {@linkplain CheckBlockingMilestone} annotation.
 */
@Aspect
@Component
public class CheckBlockingMilestoneAspect {

	private static final String ADVISING_METHOD = "Advising method {}{}.";
	private static final String FOUND_ID_IN_METHOD = "Found required @{} on arg #{} of method {}.";
	private static final String LOCKED_MILESTONE_MESSAGE = "This element is bound to a locked milestone. It can't be modified";
	private static final String LOCKED_MILESTONES_MESSAGE = "At least one element is bound to a locked milestone. It can't be modified";
	private static final String MISSING_ID_PARAMETER = "Could not find any argument annotated @Id in @CheckBlockingMilestone method %s%s. This must be a structural programming error.";

	private static final Logger LOGGER = LoggerFactory.getLogger(CheckBlockingMilestone.class);

	@Inject
	private MilestoneDao milestoneDao;

	@Around(
		value = "execution(@org.squashtest.tm.service.annotation.CheckBlockingMilestone * * (..)) && @annotation(args)",
		argNames = "args")
	public Object checkBlockingMilestone(ProceedingJoinPoint pjp, CheckBlockingMilestone args) throws Throwable {
		long id = findEntityId(pjp);
		Class entityType = args.entityType();

		boolean isEntityBoundToBlockingMilestone;
		switch (entityType.getSimpleName()) {
			case "Requirement":
				isEntityBoundToBlockingMilestone = milestoneDao.isRequirementBoundToBlockingMilestone(id);
				break;
			case "RequirementVersion":
				isEntityBoundToBlockingMilestone = milestoneDao.isRequirementVersionBoundToBlockingMilestone(id);
				break;
			case "TestCase":
				isEntityBoundToBlockingMilestone = milestoneDao.isTestCaseMilestoneModifiable(id);
				break;
			case "TestStep":
				isEntityBoundToBlockingMilestone = milestoneDao.isTestStepBoundToBlockingMilestone(id);
				break;
			case "Parameter":
				isEntityBoundToBlockingMilestone = milestoneDao.isParameterBoundToBlockingMilestone(id);
				break;
			case "Dataset":
				isEntityBoundToBlockingMilestone = milestoneDao.isDatasetBoundToBlockingMilestone(id);
				break;
			case "DatasetParamValue":
				isEntityBoundToBlockingMilestone = milestoneDao.isDatasetParamValueBoundToBlockingMilestone(id);
				break;
			case "AttachmentList":
				isEntityBoundToBlockingMilestone = milestoneDao.isAttachmentListBoundToBlockingMilestone(id);
				break;
			case "Attachment":
				isEntityBoundToBlockingMilestone = milestoneDao.isAttachmentBoundToBlockingMilestone(id);
				break;
			case "Campaign":
				isEntityBoundToBlockingMilestone = milestoneDao.isCampaignBoundToBlockingMilestone(id);
				break;
			case "Iteration":
				isEntityBoundToBlockingMilestone = milestoneDao.isIterationBoundToBlockingMilestone(id);
				break;
			case "TestSuite":
				isEntityBoundToBlockingMilestone = milestoneDao.isTestSuiteBoundToBlockingMilestone(id);
				break;
			case "CampaignTestPlanItem":
				isEntityBoundToBlockingMilestone = milestoneDao.isCampaignTestPlanItemBoundToBlockingMilestone(id);
				break;
			case "Execution":
				isEntityBoundToBlockingMilestone = milestoneDao.isExecutionBoundToBlockingMilestone(id);
				break;
			case "ExecutionStep":
				isEntityBoundToBlockingMilestone = milestoneDao.isExecutionStepBoundToBlockingMilestone(id);
				break;
			case "IterationTestPlanItem":
				isEntityBoundToBlockingMilestone = milestoneDao.isIterationTestPlanItemBoundToBlockingMilestone(id);
				break;
			default:
				throw new UnsupportedOperationException("Cannot check locked milestones for entity type " + entityType.getSimpleName());
		}
		if (isEntityBoundToBlockingMilestone) {
			throw new MilestoneForbidModificationException(LOCKED_MILESTONE_MESSAGE);
		}
		return pjp.proceed();
	}

	private long findEntityId(ProceedingJoinPoint pjp) {
		return findAnnotatedParamValue(pjp, Id.class);
	}

	@Around(
		value = "execution(@org.squashtest.tm.service.annotation.CheckBlockingMilestones * * (..)) && @annotation(args)",
		argNames = "args")
	public Object checkBlockingMilestoneMultiple(ProceedingJoinPoint pjp, CheckBlockingMilestones args) throws Throwable {
		Collection<Long> ids = findEntitiesIds(pjp);
		Class<? extends MilestoneMember> entityType = args.entityType();

		boolean areEntitiesBoundToBlockingMilestone;
		switch (entityType.getSimpleName()) {
			case "Requirement":
				areEntitiesBoundToBlockingMilestone = milestoneDao.areRequirementsBoundToBlockingMilestone(ids);
				break;
			case "TestCase":
				areEntitiesBoundToBlockingMilestone = milestoneDao.areTestCasesBoundToBlockingMilestone(ids);
				break;
			case "Campaign":
				areEntitiesBoundToBlockingMilestone = milestoneDao.areCampaignsBoundToBlockingMilestone(ids);
				break;
			case "TestSuite":
				areEntitiesBoundToBlockingMilestone = milestoneDao.areTestSuitesBoundToBlockingMilestone(ids);
				break;
			default:
				throw new UnsupportedOperationException();
		}
		if (areEntitiesBoundToBlockingMilestone) {
			throw new MilestoneForbidModificationException(LOCKED_MILESTONES_MESSAGE);
		}
		return pjp.proceed();
	}

	private Collection<Long> findEntitiesIds(ProceedingJoinPoint pjp) {
		return findAnnotatedParamValue(pjp, Ids.class);
	}


	private <T> T findAnnotatedParamValue(ProceedingJoinPoint pjp, Class<? extends Annotation> idAnnotation) {
		MethodSignature signature = (MethodSignature) pjp.getSignature();
		Method method = signature.getMethod();
		LOGGER.trace(ADVISING_METHOD, signature.getDeclaringTypeName(), method.getName());

		T annotatedParameter = null;
		Annotation[][] annotations = method.getParameterAnnotations();

		argsLoop:
		for (int iArg = 0; iArg < annotations.length; iArg++) {
			Annotation[] currentArgAnnotations = annotations[iArg];
			for (int jAnn = 0; jAnn < currentArgAnnotations.length; jAnn++) {
				if (idAnnotation.equals(currentArgAnnotations[jAnn].annotationType())) {
					LOGGER.trace(FOUND_ID_IN_METHOD, idAnnotation.getSimpleName(), iArg, method.getName());
					annotatedParameter = (T) pjp.getArgs()[iArg];
					break argsLoop;
				}
			}
		}

		if (annotatedParameter == null) {
			throw new IllegalArgumentException(
				String.format(MISSING_ID_PARAMETER, signature.getDeclaringTypeName(), method.getName()));
		}

		return annotatedParameter;
	}

}
