/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto;

import java.util.List;

public class UserAccountDto {
	private Long id;
	private String firstName;
	private String lastName;
	private String login;
	private UsersGroupDto userGroup;
	private String email;
	private List<ProjectPermissionDto> projectPermissions;
	private List<BugTrackerCredentialsDto> bugTrackerCredentials;
	private String bugTrackerMode;
	private boolean canManageLocalPassword;
	private boolean hasLocalPassword;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public UsersGroupDto getUserGroup() {
		return userGroup;
	}

	public void setUserGroup(UsersGroupDto userGroup) {
		this.userGroup = userGroup;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<ProjectPermissionDto> getProjectPermissions() {
		return projectPermissions;
	}

	public void setProjectPermissions(List<ProjectPermissionDto> projectPermissions) {
		this.projectPermissions = projectPermissions;
	}

	public List<BugTrackerCredentialsDto> getBugTrackerCredentials() {
		return bugTrackerCredentials;
	}

	public void setBugTrackerCredentials(List<BugTrackerCredentialsDto> bugtrackerCredentials) {
		this.bugTrackerCredentials = bugtrackerCredentials;
	}

	public String getBugTrackerMode() {
		return bugTrackerMode;
	}

	public void setBugTrackerMode(String bugTrackerMode) {
		this.bugTrackerMode = bugTrackerMode;
	}

	public boolean isCanManageLocalPassword() {
		return canManageLocalPassword;
	}

	public void setCanManageLocalPassword(boolean canManageLocalPassword) {
		this.canManageLocalPassword = canManageLocalPassword;
	}

	public boolean isHasLocalPassword() {
		return hasLocalPassword;
	}

	public void setHasLocalPassword(boolean hasLocalPassword) {
		this.hasLocalPassword = hasLocalPassword;
	}
}
