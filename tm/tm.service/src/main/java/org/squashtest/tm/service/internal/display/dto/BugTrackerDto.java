/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto;

import org.squashtest.csp.core.bugtracker.domain.BugTracker;

public class BugTrackerDto {

	private Long id;
	private String name;
	private String url;
	private String kind;
	private String authPolicy;
	private String authProtocol;
	private boolean iframeFriendly;

	public static BugTrackerDto from(BugTracker bt) {
		BugTrackerDto dto = new BugTrackerDto();

		dto.setId(bt.getId());
		dto.setName(bt.getName());
		dto.setUrl(bt.getUrl());
		dto.setKind(bt.getKind());
		dto.setAuthPolicy(bt.getAuthenticationPolicy().name());
		dto.setAuthProtocol(bt.getAuthenticationProtocol().name());
		dto.setIframeFriendly(bt.isIframeFriendly());

		return dto;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public String getAuthPolicy() {
		return authPolicy;
	}

	public void setAuthPolicy(String authPolicy) {
		this.authPolicy = authPolicy;
	}

	public boolean isIframeFriendly() {
		return iframeFriendly;
	}

	public void setIframeFriendly(boolean iframeFriendly) {
		this.iframeFriendly = iframeFriendly;
	}

	public String getAuthProtocol() {
		return authProtocol;
	}

	public void setAuthProtocol(String authProtocol) {
		this.authProtocol = authProtocol;
	}
}
