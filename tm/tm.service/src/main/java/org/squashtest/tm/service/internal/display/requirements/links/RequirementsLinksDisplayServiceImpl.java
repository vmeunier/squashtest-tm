/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.requirements.links;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.service.display.requirements.links.RequirementsLinksDisplayService;
import org.squashtest.tm.service.internal.display.dto.RequirementVersionLinkTypeDto;
import org.squashtest.tm.service.internal.display.dto.RequirementsLinksTypeDto;
import org.squashtest.tm.service.internal.repository.display.RequirementVersionLinkTypeDisplayDao;

import java.util.List;

import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;

@Service
@Transactional(readOnly = true)
public class RequirementsLinksDisplayServiceImpl implements RequirementsLinksDisplayService {

	private final RequirementVersionLinkTypeDisplayDao requirementVersionLinkTypeDisplayDao;

	public RequirementsLinksDisplayServiceImpl(RequirementVersionLinkTypeDisplayDao requirementVersionLinkTypeDisplayDao) {
		this.requirementVersionLinkTypeDisplayDao = requirementVersionLinkTypeDisplayDao;
	}

	@PreAuthorize(HAS_ROLE_ADMIN)
	@Override
	public List<RequirementVersionLinkTypeDto> findAll() {
		return requirementVersionLinkTypeDisplayDao.findAll();
	}

	@PreAuthorize(HAS_ROLE_ADMIN)
	@Override
	public List<RequirementsLinksTypeDto> findAllRequirementsLinksType() {
		return requirementVersionLinkTypeDisplayDao.findAllRequirementsLinksType();
	}
}
