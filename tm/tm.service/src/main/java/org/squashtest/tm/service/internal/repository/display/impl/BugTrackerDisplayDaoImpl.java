/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.bugtracker.BugTrackerKindNotBindableToProject;
import org.squashtest.tm.service.internal.display.dto.BugTrackerBindingDto;
import org.squashtest.tm.service.internal.display.dto.BugTrackerDto;
import org.squashtest.tm.service.internal.display.dto.BugTrackerViewDto;
import org.squashtest.tm.service.internal.display.dto.ProjectDto;
import org.squashtest.tm.service.internal.repository.display.BugTrackerDisplayDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.squashtest.tm.jooq.domain.Tables.BUGTRACKER;
import static org.squashtest.tm.jooq.domain.Tables.THIRD_PARTY_SERVER;
import static org.squashtest.tm.jooq.domain.tables.BugtrackerBinding.BUGTRACKER_BINDING;

@Repository
public class BugTrackerDisplayDaoImpl implements BugTrackerDisplayDao {

	private DSLContext dsl;

	public BugTrackerDisplayDaoImpl(DSLContext dsl) {
		this.dsl = dsl;
	}

	@Override
	public void appendBugTrackerBindings(List<ProjectDto> projects) {

		Map<Long, ProjectDto> projectsMap = projects.stream().collect(Collectors.toMap(ProjectDto::getId, Function.identity()));
		Set<Long> projectIds = projects.stream().map(ProjectDto::getId).collect(Collectors.toSet());

		dsl.select(BUGTRACKER_BINDING.BUGTRACKER_BINDING_ID.as(RequestAliasesConstants.ID), BUGTRACKER_BINDING.BUGTRACKER_ID, BUGTRACKER_BINDING.PROJECT_ID)
			.from(BUGTRACKER_BINDING)
			.where(BUGTRACKER_BINDING.PROJECT_ID.in(projectIds))
			.forEach(record -> {
				ProjectDto projectDto = projectsMap.get(record.get(BUGTRACKER_BINDING.PROJECT_ID));
				BugTrackerBindingDto bugTrackerBindingDto = new BugTrackerBindingDto();
				bugTrackerBindingDto.setId(record.get(BUGTRACKER_BINDING.BUGTRACKER_BINDING_ID.as(RequestAliasesConstants.ID)));
				bugTrackerBindingDto.setBugTrackerId(record.get(BUGTRACKER_BINDING.BUGTRACKER_ID));
				bugTrackerBindingDto.setProjectId(record.get(BUGTRACKER_BINDING.PROJECT_ID));
				projectDto.setBugTrackerBinding(bugTrackerBindingDto);
			});
	}

	@Override
	public List<BugTrackerDto> findAll() {
		return dsl.select(BUGTRACKER.BUGTRACKER_ID.as(RequestAliasesConstants.ID), BUGTRACKER.IFRAME_FRIENDLY, BUGTRACKER.KIND, THIRD_PARTY_SERVER.AUTH_POLICY,
			THIRD_PARTY_SERVER.AUTH_PROTOCOL, THIRD_PARTY_SERVER.NAME, THIRD_PARTY_SERVER.URL)
			.from(BUGTRACKER).innerJoin(THIRD_PARTY_SERVER).on(BUGTRACKER.BUGTRACKER_ID.eq(THIRD_PARTY_SERVER.SERVER_ID))
			.fetchInto(BugTrackerDto.class);
	}

	@Override
	public List<BugTrackerDto> findAllBugtrackerBindableToProject() {
		List<String> allBugtrackerKindNotBindableToProject = Stream.of(BugTrackerKindNotBindableToProject.values())
																.map(BugTrackerKindNotBindableToProject::getBugTrackerKind)
																.collect(Collectors.toList());
		return dsl.select(BUGTRACKER.BUGTRACKER_ID.as(RequestAliasesConstants.ID), BUGTRACKER.IFRAME_FRIENDLY, BUGTRACKER.KIND, THIRD_PARTY_SERVER.AUTH_POLICY,
			THIRD_PARTY_SERVER.AUTH_PROTOCOL, THIRD_PARTY_SERVER.NAME, THIRD_PARTY_SERVER.URL)
			.from(BUGTRACKER).innerJoin(THIRD_PARTY_SERVER).on(BUGTRACKER.BUGTRACKER_ID.eq(THIRD_PARTY_SERVER.SERVER_ID))
			.where(BUGTRACKER.KIND.notIn(allBugtrackerKindNotBindableToProject))
			.fetchInto(BugTrackerDto.class);
	}

	@Override
	public BugTrackerViewDto getBugTrackerView(Long bugTrackerId) {
		return dsl.select(BUGTRACKER.BUGTRACKER_ID.as(RequestAliasesConstants.ID),
				BUGTRACKER.IFRAME_FRIENDLY,
				BUGTRACKER.KIND,
				THIRD_PARTY_SERVER.AUTH_POLICY,
				THIRD_PARTY_SERVER.AUTH_PROTOCOL,
				THIRD_PARTY_SERVER.NAME,
				THIRD_PARTY_SERVER.URL)
				.from(BUGTRACKER)
				.innerJoin(THIRD_PARTY_SERVER).on(BUGTRACKER.BUGTRACKER_ID.eq(THIRD_PARTY_SERVER.SERVER_ID))
				.where(BUGTRACKER.BUGTRACKER_ID.eq(bugTrackerId))
				.fetchOneInto(BugTrackerViewDto.class);
	}
}
