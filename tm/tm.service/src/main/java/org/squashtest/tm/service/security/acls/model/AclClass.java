/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.security.acls.model;

import org.jooq.Field;
import org.squashtest.tm.domain.actionword.ActionWordLibrary;
import org.squashtest.tm.domain.campaign.CampaignLibrary;
import org.squashtest.tm.domain.customreport.CustomReportLibrary;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.project.ProjectTemplate;
import org.squashtest.tm.domain.requirement.RequirementLibrary;
import org.squashtest.tm.domain.testcase.TestCaseLibrary;
import org.squashtest.tm.domain.tf.automationrequest.AutomationRequestLibrary;
import org.squashtest.tm.service.security.acls.jdbc.UnknownAclClassException;

import java.util.EnumSet;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public enum AclClass {

	PROJECT(Project.class, org.squashtest.tm.jooq.domain.tables.Project.PROJECT.PROJECT_ID),
	PROJECT_TEMPLATE(ProjectTemplate.class, org.squashtest.tm.jooq.domain.tables.Project.PROJECT.PROJECT_ID),
	REQUIREMENT_LIBRARY(RequirementLibrary.class, org.squashtest.tm.jooq.domain.tables.Project.PROJECT.RL_ID),
	TEST_CASE_LIBRARY(TestCaseLibrary.class, org.squashtest.tm.jooq.domain.tables.Project.PROJECT.TCL_ID),
	CAMPAIGN_LIBRARY(CampaignLibrary.class, org.squashtest.tm.jooq.domain.tables.Project.PROJECT.CL_ID),
	CUSTOM_REPORT_LIBRARY(CustomReportLibrary.class, org.squashtest.tm.jooq.domain.tables.Project.PROJECT.CRL_ID),
	AUTOMATION_REQUEST_LIBRARY(AutomationRequestLibrary.class, org.squashtest.tm.jooq.domain.tables.Project.PROJECT.ARL_ID),
	ACTION_WORD_LIBRARY(ActionWordLibrary.class, org.squashtest.tm.jooq.domain.tables.Project.PROJECT.AWL_ID);

	AclClass(Class entityClass, Field<Long> identityColumn) {
		this.className = entityClass.getName();
		this.simpleClassName = entityClass.getSimpleName();
		this.identityColumn = identityColumn;
	}

	private String className;
	private String simpleClassName;
	private Field<Long> identityColumn;

	public String getClassName() {
		return className;
	}

	public String getSimpleClassName() {
		return simpleClassName;
	}

	public Field<Long> getIdentityColumn() {
		return identityColumn;
	}

	public static AclClass findByClassName(String className) {
		Map<String, AclClass> aclClassByName = EnumSet.allOf(AclClass.class).stream().collect(Collectors.toMap(AclClass::getClassName, Function.identity()));
		if(aclClassByName.containsKey(className)){
			return aclClassByName.get(className);
		}
		throw new UnknownAclClassException(className);
	}
}
