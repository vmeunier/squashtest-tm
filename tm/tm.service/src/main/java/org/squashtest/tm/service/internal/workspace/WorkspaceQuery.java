/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.workspace;

public class WorkspaceQuery {
	private String[] openedNodes = new String[0];
	private String[] selectedNodes = new String[0];

	public String[] getOpenedNodes() {
		return openedNodes;
	}

	public void setOpenedNodes(String[] openedNodes) {
		this.openedNodes = openedNodes;
	}

	public String[] getSelectedNodes() {
		return selectedNodes;
	}

	public void setSelectedNodes(String[] selectedNodes) {
		this.selectedNodes = selectedNodes;
	}
}
