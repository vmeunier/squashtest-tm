/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto;

import org.squashtest.tm.domain.servers.AuthenticationStatus;

public class IssuesPanelModel {

	private String entityType;
	private AuthenticationStatus bugTrackerStatus;
	private String projectName;
	private Long projectId;
	private String delete;
	private boolean isOslc;
	private String bugTrackerMode;

	public String getEntityType() {
		return entityType;
	}

	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}


	public AuthenticationStatus getBugTrackerStatus() {
		return bugTrackerStatus;
	}

	public void setBugTrackerStatus(AuthenticationStatus bugTrackerStatus) {
		this.bugTrackerStatus = bugTrackerStatus;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public String getDelete() {
		return delete;
	}

	public void setDelete(String delete) {
		this.delete = delete;
	}

	public boolean isOslc() {
		return isOslc;
	}

	public void setOslc(boolean oslc) {
		isOslc = oslc;
	}

	public String getBugTrackerMode() {
		return bugTrackerMode;
	}

	public void setBugTrackerMode(String bugTrackerMode) {
		this.bugTrackerMode = bugTrackerMode;
	}
}
