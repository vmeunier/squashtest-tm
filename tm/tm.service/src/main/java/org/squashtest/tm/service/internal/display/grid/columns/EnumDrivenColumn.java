/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.columns;

import org.jooq.Field;
import org.jooq.SortField;
import org.squashtest.tm.service.internal.display.grid.GridSort;

import java.util.HashMap;
import java.util.Map;

public abstract class EnumDrivenColumn extends GridColumn {

	private Class<? extends Enum<?>> sourceEnum;

	public EnumDrivenColumn(Class<? extends Enum<?>> sourceEnum, Field<String> field) {
		super(field);
		this.sourceEnum = sourceEnum;
	}

	// This override won't work if the field is given an alias
	public SortField<?> generateOrderClause(GridSort.SortDirection direction) {
		Map<String, Integer> sortMap = getAscSortMap();

		// Invert ranks if direction is descending
		if (direction.equals(GridSort.SortDirection.DESC)) {
			sortMap.keySet().forEach(key -> sortMap.put(key, sortMap.get(key) * -1));
		}

		return this.aliasedField.sort((Map)sortMap);
	}

	/**
	 * Maps each enum value (as Strings) to a number for ordering. The default implementation use the
	 * ordinal order.
	 *
	 * @return a mapping between enum string values and the rank used for ordering
	 */
	protected Map<String, Integer> getAscSortMap() {
		Map<String, Integer> sortMap = new HashMap<>();

		for (Enum<?> e: sourceEnum.getEnumConstants()) {
			sortMap.put(e.toString(), e.ordinal());
		}

		return sortMap;
	}
}
