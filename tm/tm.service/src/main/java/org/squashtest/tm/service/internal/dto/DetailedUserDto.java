/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto;

public class DetailedUserDto extends UserDto {
	private String firstName;
	private String lastName;
	private boolean isProjectManager;
	private boolean isFunctionalTester;
	private boolean isAutomationProgrammer;

	public DetailedUserDto() {
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public boolean isProjectManager() {
		return isProjectManager;
	}

	public void setProjectManager(boolean projectManager) {
		isProjectManager = projectManager;
	}

	public boolean isFunctionalTester() {
		return isFunctionalTester;
	}

	public void setFunctionalTester(boolean functionalTester) {
		isFunctionalTester = functionalTester;
	}

	public boolean isAutomationProgrammer() {
		return isAutomationProgrammer;
	}

	public void setAutomationProgrammer(boolean automationProgrammer) {
		isAutomationProgrammer = automationProgrammer;
	}
}

