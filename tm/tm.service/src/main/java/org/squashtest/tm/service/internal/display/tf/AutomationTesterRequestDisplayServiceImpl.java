/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.tf;

import org.jooq.DSLContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.query.Operation;
import org.squashtest.tm.domain.tf.automationrequest.AutomationRequestStatus;
import org.squashtest.tm.service.display.tf.AutomationTesterRequestDisplayService;
import org.squashtest.tm.service.internal.display.grid.GridFilterValue;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.display.grid.tf.AutomationRequestGrid;
import org.squashtest.tm.service.internal.repository.display.AutomationTesterRequestDisplayDao;
import org.squashtest.tm.service.project.ProjectFinder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class AutomationTesterRequestDisplayServiceImpl implements AutomationTesterRequestDisplayService {

	private final ProjectFinder projectFinder;
	private final AutomationTesterRequestDisplayDao automationTesterRequestDisplayDao;
	private final DSLContext dslContext;

	public AutomationTesterRequestDisplayServiceImpl(ProjectFinder projectFinder,
													 AutomationTesterRequestDisplayDao automationTesterRequestDisplayDao,
													 DSLContext dslContext) {
		this.projectFinder = projectFinder;
		this.automationTesterRequestDisplayDao = automationTesterRequestDisplayDao;
		this.dslContext = dslContext;
	}

	@Override
	public GridResponse findGlobalView(GridRequest request) {
		List<Long> projectIds = projectFinder.findAllReadableIds();
		AutomationRequestGrid grid = new AutomationRequestGrid(projectIds);
		return grid.getRows(request, dslContext);
	}

	@Override
	public GridResponse findReadyForTransmissionRequests(GridRequest request) {
		List<Long> projectIds = projectFinder.findAllReadableIds();
		GridFilterValue filterValue = new GridFilterValue();
		filterValue.setId("requestStatus");
		filterValue.setValues(Collections.singletonList(AutomationRequestStatus.READY_TO_TRANSMIT.name()));
		filterValue.setOperation(Operation.IN.name());
		request.getFilterValues().add(filterValue);
		AutomationRequestGrid grid = new AutomationRequestGrid(projectIds);
		return grid.getRows(request, dslContext);
	}

	@Override
	public GridResponse findToBeValidatedRequests(GridRequest request) {
		List<Long> projectIds = projectFinder.findAllReadableIds();
		GridFilterValue filterValue = new GridFilterValue();
		filterValue.setId("requestStatus");
		filterValue.setValues(getToBeValidatedAutomRequestStatuses());
		filterValue.setOperation(Operation.IN.name());
		request.getFilterValues().add(filterValue);
		AutomationRequestGrid grid = new AutomationRequestGrid(projectIds);
		return grid.getRows(request, dslContext);
	}

	private List<String> getToBeValidatedAutomRequestStatuses() {
		List requestStatuses = new ArrayList();
		requestStatuses.add(AutomationRequestStatus.WORK_IN_PROGRESS.name());
		requestStatuses.add(AutomationRequestStatus.REJECTED.name());
		requestStatuses.add(AutomationRequestStatus.SUSPENDED.name());
		return requestStatuses;
	}
}
