/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.referential;

import org.squashtest.tm.api.wizard.WorkspacePlugin;
import org.squashtest.tm.api.wizard.WorkspacePluginIcon;

public class WorkspacePluginDto {
	private String id;
	private String workspaceId;
	private String name;
	private String iconName;
	private String tooltip;
	private String theme;
	private String url;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIconName() {
		return iconName;
	}

	public void setIconName(String iconName) {
		this.iconName = iconName;
	}

	public String getTooltip() {
		return tooltip;
	}

	public void setTooltip(String tooltip) {
		this.tooltip = tooltip;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public static WorkspacePluginDto fromWorkspacePlugin(WorkspacePlugin workspacePlugin){
		WorkspacePluginDto workspacePluginDto = new WorkspacePluginDto();
		workspacePluginDto.id = workspacePlugin.getId();
		WorkspacePluginIcon workspaceIcon = workspacePlugin.getWorkspaceIcon();
		workspacePluginDto.workspaceId = workspaceIcon.getWorkspaceId();
		workspacePluginDto.name = workspaceIcon.getWorkspaceName();
		workspacePluginDto.iconName = workspaceIcon.getIconName();
		workspacePluginDto.tooltip = workspaceIcon.getTooltip();
		workspacePluginDto.url = workspaceIcon.getUrl();
		workspacePluginDto.theme = workspaceIcon.getTheme();
		return workspacePluginDto;
	}

	public String getWorkspaceId() {
		return workspaceId;
	}

	public void setWorkspaceId(String workspaceId) {
		this.workspaceId = workspaceId;
	}
}
