/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto;

import org.squashtest.tm.service.internal.dto.PermissionWithMask;

import java.util.ArrayList;
import java.util.List;

public class ProjectPermissions {

	private List<PermissionWithMask> project = new ArrayList<>();
	private List<PermissionWithMask> requirement = new ArrayList<>();
	private List<PermissionWithMask> testCase = new ArrayList<>();
	private List<PermissionWithMask> campaign = new ArrayList<>();
	private List<PermissionWithMask> customReport = new ArrayList<>();
	private List<PermissionWithMask> automationRequest = new ArrayList<>();

	public ProjectPermissions() {
	}

	public List<PermissionWithMask> getProject() {
		return project;
	}

	public void addProjectPermission(PermissionWithMask permission) {
		project.add(permission);
	}

	public List<PermissionWithMask> getRequirement() {
		return requirement;
	}

	public void addRequirementPermission(PermissionWithMask permission) {
		requirement.add(permission);
	}

	public List<PermissionWithMask> getTestCase() {
		return testCase;
	}

	public void addTestCasePermission(PermissionWithMask permission) {
		testCase.add(permission);
	}

	public List<PermissionWithMask> getCampaign() {
		return campaign;
	}

	public void addCampaignPermission(PermissionWithMask permission) {
		campaign.add(permission);
	}

	public List<PermissionWithMask> getCustomReport() {
		return customReport;
	}

	public void addCustomReportPermission(PermissionWithMask permission) {
		customReport.add(permission);
	}

	public List<PermissionWithMask> getAutomationRequest() {
		return automationRequest;
	}

	public void addAutomationRequestPermission(PermissionWithMask permission) {
		automationRequest.add(permission);
	}
}
