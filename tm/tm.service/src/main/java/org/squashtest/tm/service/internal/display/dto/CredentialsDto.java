/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto;

import com.google.common.base.Strings;
import org.squashtest.tm.domain.servers.AuthenticationProtocol;
import org.squashtest.tm.domain.servers.BasicAuthenticationCredentials;
import org.squashtest.tm.domain.servers.OAuth1aCredentials;
import org.squashtest.tm.domain.servers.TokenAuthCredentials;
import org.squashtest.tm.service.internal.servers.UserOAuth1aToken;
import org.squashtest.tm.service.servers.ManageableCredentials;

/**
 * Used when sending stored third party server credentials from the server to the client.
 * Sensible data (password, token secret,..) is removed.
 */
public class CredentialsDto {
    private AuthenticationProtocol implementedProtocol;
    private String username;
    private String token;
    private AuthenticationProtocol type;
    private boolean registered;

    public static CredentialsDto from(ManageableCredentials credentials) {
        if (credentials == null) {
            return null;
        }

        switch(credentials.getImplementedProtocol()) {
            case OAUTH_1A:
                if (credentials instanceof OAuth1aCredentials) {
                    return fromOAuth1a((OAuth1aCredentials) credentials);
                } else if (credentials instanceof UserOAuth1aToken) {
                    return fromUserOAuth1aToken((UserOAuth1aToken) credentials);
                } else {
                    throw new IllegalArgumentException("Invalid type for OAuth1a credentials " + credentials.getClass().getName());
                }
            case BASIC_AUTH:
                return fromBasicAuth((BasicAuthenticationCredentials)credentials);
            case TOKEN_AUTH:
                return fromTokenAuth((TokenAuthCredentials)credentials);
            default:
                throw new IllegalArgumentException("Invalid authentication protocol " + credentials.getImplementedProtocol());
        }
    }

    private static CredentialsDto fromTokenAuth(TokenAuthCredentials credentials) {
        CredentialsDto dto = new CredentialsDto();
        dto.implementedProtocol = credentials.getImplementedProtocol();
        dto.registered = !Strings.isNullOrEmpty(credentials.getToken());
		dto.type = credentials.getImplementedProtocol();
        return dto;
    }

    private static CredentialsDto fromBasicAuth(BasicAuthenticationCredentials credentials) {
        CredentialsDto dto = new CredentialsDto();
        dto.implementedProtocol = credentials.getImplementedProtocol();
		dto.registered = !Strings.isNullOrEmpty(credentials.getUsername());
        dto.username = credentials.getUsername();
		dto.type = credentials.getImplementedProtocol();
        return dto;
    }

    public static CredentialsDto fromOAuth1a(OAuth1aCredentials credentials) {
        CredentialsDto dto = new CredentialsDto();
        dto.implementedProtocol = credentials.getImplementedProtocol();
		dto.registered = !Strings.isNullOrEmpty(credentials.getToken());
        dto.token = credentials.getToken();
        dto.type = credentials.getImplementedProtocol();
        return dto;
    }

    public static CredentialsDto fromUserOAuth1aToken(UserOAuth1aToken credentials) {
        CredentialsDto dto = new CredentialsDto();
        dto.implementedProtocol = credentials.getImplementedProtocol();
		dto.registered = !Strings.isNullOrEmpty(credentials.getToken());
        dto.token = credentials.getToken();
        dto.type = credentials.getImplementedProtocol();
        return dto;
    }

    public AuthenticationProtocol getImplementedProtocol() {
        return implementedProtocol;
    }

    public void setImplementedProtocol(AuthenticationProtocol implementedProtocol) {
        this.implementedProtocol = implementedProtocol;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

	public boolean isRegistered() {
		return registered;
	}

	public void setRegistered(boolean registered) {
		this.registered = registered;
	}

	public AuthenticationProtocol getType() {
		return type;
	}

	public void setType(AuthenticationProtocol type) {
		this.type = type;
	}
}
