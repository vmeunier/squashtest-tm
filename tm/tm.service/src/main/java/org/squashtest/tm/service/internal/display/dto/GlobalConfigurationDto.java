/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto;

import org.apache.commons.lang3.StringUtils;
import org.squashtest.tm.service.configuration.ConfigurationService;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GlobalConfigurationDto {
	private boolean milestoneFeatureEnabled;
	private List<String> uploadFileExtensionWhitelist;
	private Long uploadFileSizeLimit;

	private GlobalConfigurationDto() {
	}

	public boolean isMilestoneFeatureEnabled() {
		return milestoneFeatureEnabled;
	}

	private void setMilestoneFeatureEnabled(boolean milestoneFeatureEnabled) {
		this.milestoneFeatureEnabled = milestoneFeatureEnabled;
	}

	public List<String> getUploadFileExtensionWhitelist() {
		return uploadFileExtensionWhitelist;
	}

	private void setUploadFileExtensionWhitelist(String whiteList) {
		this.uploadFileExtensionWhitelist = Arrays.stream(whiteList.split(","))
			.filter(StringUtils::isNotBlank)
			.map(StringUtils::trim)
			.collect(Collectors.toList());
	}

	public Long getUploadFileSizeLimit() {
		return uploadFileSizeLimit;
	}

	private void setUploadFileSizeLimit(Long uploadFileSizeLimit) {
		this.uploadFileSizeLimit = uploadFileSizeLimit;
	}

	public static GlobalConfigurationDto create(Map<String, String> coreConfig) {
		GlobalConfigurationDto conf = new GlobalConfigurationDto();
		conf.setMilestoneFeatureEnabled(Boolean.parseBoolean(coreConfig.get(ConfigurationService.Properties.MILESTONE_FEATURE_ENABLED)));
		conf.setUploadFileExtensionWhitelist(coreConfig.get(ConfigurationService.Properties.UPLOAD_EXTENSIONS_WHITELIST));
		conf.setUploadFileSizeLimit(Long.parseLong(coreConfig.get(ConfigurationService.Properties.UPLOAD_SIZE_LIMIT)));
		return conf;
	}
}
