/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.requirements;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.Workspace;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.users.PartyPreference;
import org.squashtest.tm.domain.users.preferences.CorePartyPreference;
import org.squashtest.tm.service.customreport.CustomReportDashboardService;
import org.squashtest.tm.service.display.requirements.RequirementDisplayService;
import org.squashtest.tm.service.internal.display.dto.MilestoneDto;
import org.squashtest.tm.service.internal.display.dto.requirement.RequirementFolderDto;
import org.squashtest.tm.service.internal.display.dto.requirement.RequirementLibraryDto;
import org.squashtest.tm.service.internal.display.dto.requirement.RequirementMultiSelectionDto;
import org.squashtest.tm.service.internal.display.dto.requirement.RequirementVersionDto;
import org.squashtest.tm.service.internal.display.dto.requirement.RequirementVersionLinkDto;
import org.squashtest.tm.service.internal.display.dto.requirement.VerifyingTestCaseDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.repository.display.AttachmentDisplayDao;
import org.squashtest.tm.service.internal.repository.display.CustomFieldValueDisplayDao;
import org.squashtest.tm.service.internal.repository.display.IssueDisplayDao;
import org.squashtest.tm.service.internal.repository.display.MilestoneDisplayDao;
import org.squashtest.tm.service.internal.repository.display.ModificationHistoryDisplayDao;
import org.squashtest.tm.service.internal.repository.display.RequirementDisplayDao;
import org.squashtest.tm.service.internal.repository.display.RequirementVersionDisplayDao;
import org.squashtest.tm.service.internal.repository.display.RequirementVersionLinkDisplayDao;
import org.squashtest.tm.service.internal.repository.display.VerifyingTestCaseDisplayDao;
import org.squashtest.tm.service.requirement.RequirementLibraryNavigationService;
import org.squashtest.tm.service.requirement.RequirementStatisticsService;
import org.squashtest.tm.service.user.PartyPreferenceService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.squashtest.tm.service.security.Authorizations.OR_HAS_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.READ_REQUIREMENT_FOLDER_OR_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.READ_REQUIREMENT_OR_ROLE_ADMIN;

@Service
@Transactional(readOnly = true)
public class RequirementDisplayServiceImpl implements RequirementDisplayService {

	private final RequirementVersionDisplayDao requirementVersionDisplayDao;
	private final CustomFieldValueDisplayDao customFieldValueDisplayDao;
	private final AttachmentDisplayDao attachmentDisplayDao;
	private final MilestoneDisplayDao milestoneDisplayDao;
	private final RequirementDisplayDao requirementDisplayDao;
	private final VerifyingTestCaseDisplayDao verifyingTestCaseDisplayDao;
	private final RequirementVersionLinkDisplayDao requirementVersionLinkDisplayDao;
	private final RequirementStatisticsService requirementStatisticsService;
	private final ModificationHistoryDisplayDao modificationHistoryDisplayDao;
	private final IssueDisplayDao issueDisplayDao;
	private final RequirementLibraryNavigationService requirementLibraryNavigationService;
	private final CustomReportDashboardService customReportDashboardService;
	private final PartyPreferenceService partyPreferenceService;

	public RequirementDisplayServiceImpl(RequirementVersionDisplayDao requirementVersionDisplayDao,
										 CustomFieldValueDisplayDao customFieldValueDisplayDao,
										 AttachmentDisplayDao attachmentDisplayDao,
										 MilestoneDisplayDao milestoneDisplayDao,
										 RequirementDisplayDao requirementDisplayDao,
										 VerifyingTestCaseDisplayDao verifyingTestCaseDisplayDao,
										 RequirementVersionLinkDisplayDao requirementVersionLinkDisplayDao,
										 ModificationHistoryDisplayDao modificationHistoryDisplayDao,
										 RequirementStatisticsService requirementStatisticsService,
										 IssueDisplayDao issueDisplayDao,
										 RequirementLibraryNavigationService requirementLibraryNavigationService,
										 CustomReportDashboardService customReportDashboardService,
										 PartyPreferenceService partyPreferenceService) {
		this.requirementVersionDisplayDao = requirementVersionDisplayDao;
		this.customFieldValueDisplayDao = customFieldValueDisplayDao;
		this.attachmentDisplayDao = attachmentDisplayDao;
		this.milestoneDisplayDao = milestoneDisplayDao;
		this.requirementDisplayDao = requirementDisplayDao;
		this.verifyingTestCaseDisplayDao = verifyingTestCaseDisplayDao;
		this.requirementVersionLinkDisplayDao = requirementVersionLinkDisplayDao;
		this.requirementStatisticsService = requirementStatisticsService;
		this.modificationHistoryDisplayDao = modificationHistoryDisplayDao;
		this.issueDisplayDao = issueDisplayDao;
		this.requirementLibraryNavigationService = requirementLibraryNavigationService;
		this.customReportDashboardService = customReportDashboardService;
		this.partyPreferenceService = partyPreferenceService;
	}

	@PreAuthorize(READ_REQUIREMENT_OR_ROLE_ADMIN)
	@Override
	public RequirementVersionDto findCurrentVersionView(Long requirementId) {
		Long currentVersionId = this.requirementVersionDisplayDao.findCurrentRequirementVersions(requirementId);
		return createRequirementVersionDto(currentVersionId);
	}

	@PreAuthorize("hasPermission(#requirementVersionId, 'org.squashtest.tm.domain.requirement.RequirementVersion', 'READ')"
		+ OR_HAS_ROLE_ADMIN)
	@Override
	public RequirementVersionDto findVersionView(Long requirementVersionId) {
		return createRequirementVersionDto(requirementVersionId);

	}

	private RequirementVersionDto createRequirementVersionDto(Long requirementVersionId) {
		RequirementVersionDto requirementVersion = requirementVersionDisplayDao.findRequirementVersion(requirementVersionId);
		requirementVersion.setCustomFieldValues(customFieldValueDisplayDao.findCustomFieldValues(BindableEntity.REQUIREMENT_VERSION, requirementVersionId));
		requirementVersion.setAttachmentList(attachmentDisplayDao.findAttachmentListById(requirementVersion.getAttachmentListId()));
		requirementVersion.setMilestones(milestoneDisplayDao.getMilestonesByRequirementVersionId(requirementVersionId));
		requirementVersion.setBindableMilestones(milestoneDisplayDao.getMilestonesDtoAssociableToRequirementVersion(requirementVersionId));
		requirementVersion.setVerifyingTestCases(verifyingTestCaseDisplayDao.findByRequirementVersionId(requirementVersionId));
		requirementVersion.setRequirementVersionLinks(requirementVersionLinkDisplayDao.findLinksByRequirementVersionId(requirementVersionId));
		requirementVersion.setRequirementStats(requirementStatisticsService.findCoveragesStatsByRequirementVersionId(requirementVersionId));
		requirementVersion.setNbIssues(issueDisplayDao.countIssuesByRequirementVersionId(requirementVersionId));
		return requirementVersion;
	}

	@PreAuthorize("hasPermission(#requirementVersionId, 'org.squashtest.tm.domain.requirement.RequirementVersion', 'WRITE')"
		+ OR_HAS_ROLE_ADMIN)
	@Override
	public List<MilestoneDto> findBindableMilestones(Long requirementVersionId) {
		return milestoneDisplayDao.getMilestonesDtoAssociableToRequirementVersion(requirementVersionId);
	}

	@Override
	public RequirementLibraryDto findLibrary(Long libraryId) {
		RequirementLibraryDto libraryDto = requirementDisplayDao.findRequirementLibraryDtoById(libraryId);
		libraryDto.setAttachmentList(attachmentDisplayDao.findAttachmentListById(libraryDto.getAttachmentListId()));
		appendFavoriteDashboardInformation(libraryDto);
		return libraryDto;
	}

	private void appendFavoriteDashboardInformation(RequirementLibraryDto libraryDto) {
		libraryDto.setCanShowFavoriteDashboard(customReportDashboardService.canShowDashboardInWorkspace(Workspace.REQUIREMENT));
		libraryDto.setShouldShowFavoriteDashboard(customReportDashboardService.shouldShowFavoriteDashboardInWorkspace(Workspace.REQUIREMENT));
		PartyPreference preference = partyPreferenceService
			.findPreferenceForCurrentUser(CorePartyPreference.FAVORITE_DASHBOARD_REQUIREMENT.getPreferenceKey());
		if (preference != null) {
			Long dashboardId = Long.valueOf(preference.getPreferenceValue());
			libraryDto.setFavoriteDashboardId(dashboardId);
		}
	}

	@PreAuthorize(READ_REQUIREMENT_FOLDER_OR_ROLE_ADMIN)
	@Override
	public RequirementFolderDto getRequirementFolderView(long requirementFolderId) {
		RequirementFolderDto requirementFolder = requirementDisplayDao.getRequirementFolderDtoById(requirementFolderId);
		requirementFolder.setAttachmentList(attachmentDisplayDao.findAttachmentListById(requirementFolder.getAttachmentListId()));
		requirementFolder.setCustomFieldValues(customFieldValueDisplayDao.findCustomFieldValues(BindableEntity.REQUIREMENT_FOLDER, requirementFolderId));
		appendFavoriteDashboardInformation(requirementFolder);
		return requirementFolder;
	}

	private void appendFavoriteDashboardInformation(RequirementFolderDto requirementFolderDto) {
		requirementFolderDto.setCanShowFavoriteDashboard(customReportDashboardService.canShowDashboardInWorkspace(Workspace.REQUIREMENT));
		requirementFolderDto.setShouldShowFavoriteDashboard(customReportDashboardService.shouldShowFavoriteDashboardInWorkspace(Workspace.REQUIREMENT));
		PartyPreference preference = partyPreferenceService
			.findPreferenceForCurrentUser(CorePartyPreference.FAVORITE_DASHBOARD_REQUIREMENT.getPreferenceKey());
		if (preference != null) {
			Long dashboardId = Long.valueOf(preference.getPreferenceValue());
			requirementFolderDto.setFavoriteDashboardId(dashboardId);
		}
	}

	@PreAuthorize("hasPermission(#requirementVersionId, 'org.squashtest.tm.domain.requirement.RequirementVersion', 'READ')"
		+ OR_HAS_ROLE_ADMIN)
	@Override
	public List<VerifyingTestCaseDto> findVerifyingTestCasesByRequirementVersionId(Long requirementVersionId) {
		return verifyingTestCaseDisplayDao.findByRequirementVersionId(requirementVersionId);
	}

	@PreAuthorize("hasPermission(#requirementVersionId, 'org.squashtest.tm.domain.requirement.RequirementVersion', 'READ')"
		+ OR_HAS_ROLE_ADMIN)
	@Override
	public List<RequirementVersionLinkDto> findLinkedRequirementsByRequirementVersionId(Long requirementVersionId) {
		return requirementVersionLinkDisplayDao.findLinksByRequirementVersionId(requirementVersionId);
	}

	@Override
	public Map<String, String> findRequirementVersionNamesByRequirementIds(List<Long> requirementIds) {
		List<String> reqVersionNames = requirementDisplayDao.findRequirementVersionNamesByRequirementIds(requirementIds);
		Map<String, String> versionNames = new HashMap<>();
		versionNames.put("versionName", String.join(",", reqVersionNames));
		return versionNames;
	}

	@Override
	public GridResponse findCurrentVersionModificationHistoryByRequirementVersionId(Long requirementVersionId, GridRequest request) {
		return modificationHistoryDisplayDao.findGridByRequirementVersionId(requirementVersionId, request);
	}

	@Override
	public GridResponse findVersionsByRequirementId(Long requirementId, GridRequest request) {
		return requirementDisplayDao.findVersionsById(requirementId, request);
	}

	@Override
	public RequirementMultiSelectionDto getRequirementMultiView() {
		RequirementMultiSelectionDto requirementMultiSelectionDto = new RequirementMultiSelectionDto();
		requirementMultiSelectionDto.setCanShowFavoriteDashboard(customReportDashboardService.canShowDashboardInWorkspace(Workspace.REQUIREMENT));
		requirementMultiSelectionDto.setShouldShowFavoriteDashboard(customReportDashboardService.shouldShowFavoriteDashboardInWorkspace(Workspace.REQUIREMENT));
		PartyPreference preference = partyPreferenceService
			.findPreferenceForCurrentUser(CorePartyPreference.FAVORITE_DASHBOARD_REQUIREMENT.getPreferenceKey());
		if (preference != null) {
			Long dashboardId = Long.valueOf(preference.getPreferenceValue());
			requirementMultiSelectionDto.setFavoriteDashboardId(dashboardId);
		}
		return requirementMultiSelectionDto;
	}
}
