/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.referential;

import org.squashtest.tm.service.internal.display.referential.AdminReferentialData;
import org.squashtest.tm.service.internal.display.referential.ReferentialData;

/**
 * This service is responsible to provide all CURRENT USER referential data for SquashTM 2.0 pages.
 * Referential data contains :
 * - All inter-project data. Aka levels enums, infolists and their items. Some of these data must be filetred by user visible project ids to prevent
 * leak of data from projects that user can't read.
 * - The specific data of each project with it's configuration. Of course only projetcs visible to user must be provided...
 */
public interface ReferentialDataProvider {
	ReferentialData findReferentialData();

	AdminReferentialData findAdminReferentialData();
}
