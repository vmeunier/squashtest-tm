# TmFront

This project has two main modules :
1. sqtm-core: A module containing the common core for squash-tm main application and plugins.
2. sqtm-app: A module containing the main squash tm application.
3. cypress tests are in ./cypress folder

## Notes on dependencies
- To allow propre caching of npm dependencies, you must copy package.json and yarn.lock manually into pipeline-v1/docker/front-end each time you change the package.json deps. The folder is located at the whole maven project root.
- This must be done EACH TIME the package.json or yarn.lock is modified

## Releasing the core
As squashTM.IT.28, we use Angular 11. Publishing ivy libraries is not allowed, so you must rebuild sqtm-core without building sqtm-app before publishing. Building sqtm-app will make ngcc transform sqtm-core module into ivy code, and thus it will reject the publish
Release procedure:

- yarn build-core -> It will build sqtm-core and create the distribution of sqtm-core into dist/sqtm-core
- navigate to dist/sqtm-core folder. You must release the core distribution and not the root package or the project/sqtm-core package.
- open generated package.json and check the version.
- publish to your npm repository ``` yarn publish --no-git-tag-version ```
- the published version will have version number that you enter the line: question New version
- Increment versions number in the two SOURCE package.json (the root one, and the project/sqtm-core one)
- Note on versions :
  - npm semver do not accept a floating snapshot version. If you need to publish a snapshot, use a version number like "2.0.0-it.28.snapshot.1"
  - If you need to publish an iteration version or a release candidate, use a version number like "2.0.0-it.28" or "2.0.0-rc.1"
  - If you need to publish a release, use a version number like "2.0.0" or "2.0.1"
  - Take care of what REAL version your plugins yarn install will resolve to be sure you have the desired version. Try to avoid dependency to snapshot version once your dev is over.

All this release process should be automated in the future, and the CI should perform release itself. In the meantime we must handle it manually.

However, for local development, it could be necessary to handle snapshot release if a plugin require a modification in sqtm-core...

