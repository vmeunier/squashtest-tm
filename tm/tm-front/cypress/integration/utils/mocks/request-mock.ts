import {isBackEndMocked} from '../../../utils/testing-context';
import Chainable = Cypress.Chainable;

export function apiBaseUrl(): string {
  return Cypress.env('apiBaseUrl');
}

export class HttpMock<R extends Object> {

  public constructor(public url: string, public defaultStatus: number) {
  }

  private get composedUrl() {
    return `${apiBaseUrl()}/${this.url}`;
  }

  get alias() {
    return `@${this.composedUrl}`;
  }

  wait() {
    cy.wait(this.alias);
  }

  waitResponseBody(status?: HttpResponseStatus): Chainable<R> {
    return cy
      .wait(this.alias)
      .then((xhr) => {
        assert.equal(xhr.status, status || this.defaultStatus);
        return xhr.responseBody as R;
      });
  }
}

export class HttpMockBuilder<R extends Object> {

  private response: any;
  private httpResponseStatus: HttpResponseStatus = HTTP_RESPONSE_STATUS.SUCCESS;
  private _responseHeaders: HeaderParams = {};
  private method: 'GET' | 'POST' | 'DELETE' = 'GET';

  constructor(private url: string) {
  }

  status(status: HttpResponseStatus): HttpMockBuilder<R> {
    this.httpResponseStatus = status;
    return this;
  }

  responseBody(response: R): HttpMockBuilder<R> {
    this.response = response;
    return this;
  }

  responseHeaders(headers: HeaderParams): HttpMockBuilder<R> {
    this._responseHeaders = headers;
    return this;
  }

  get(): HttpMockBuilder<R> {
    this.method = 'GET';
    return this;
  }

  post(): HttpMockBuilder<R> {
    this.method = 'POST';
    return this;
  }

  delete(): HttpMockBuilder<R> {
    this.method = 'DELETE';
    return this;
  }

  build(): HttpMock<R> {
    console.log(`Building Mock ${this.url}`);
    if (isBackEndMocked()) {
      cy.route({
        method: this.method,
        url: this.getCompleteUrl(),
        response: this.response || '',
        status: this.httpResponseStatus,
        headers: this._responseHeaders,
      }).as(this.getCompleteUrl());
    } else {
      cy.route(this.method, this.getCompleteUrl()).as(this.getCompleteUrl());
    }
    return new HttpMock(this.url, this.httpResponseStatus);
  }

  private getCompleteUrl() {
    return `${apiBaseUrl()}/${this.url}`;
  }

}

export interface HeaderParams {
  [param: string]: string;
}

export type HttpResponseStatus = 200 | 201 | 412 | 500;

export enum HTTP_RESPONSE_STATUS {
  SUCCESS = 200,
  CREATED = 201,
  PRECONDITION_FAIL = 412,
  INTERNAL_SERVER_ERROR = 500,
}
