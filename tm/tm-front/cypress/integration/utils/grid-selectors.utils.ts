export function getGrid(id: string) {
  return `[data-test-grid-id="${id}"]`;
}

export function getViewport(name: string) {
  return `[data-test-viewport-name="${name}"]`;
}

export function getRow(id: string) {
  return `[data-test-row-id="${id}"]`;
}

export function getCell(id: string) {
  return `[data-test-cell-id="${id}"]`;
}
