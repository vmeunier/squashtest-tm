export class MenuElement {

  constructor(private menuId: string) {
  }

  assertExist() {
    cy.get(this.buildSelector()).should('exist');
  }


  buildSelector(): string {
    return `
    [data-test-menu-id="${this.menuId}"]
    `;
  }

  item(itemId: string): MenuItemElement {
    return new MenuItemElement(this.buildSelector(), itemId);
  }

  hide() {
    cy.get('body').type('{esc}').then(() => {
      cy.get(this.buildSelector())
        .should('not.exist');
    });
  }
}

export class MenuItemElement {
  constructor(private rootSelector: string, private itemId: string) {
  }

  buildSelector(): string {
    return `
    ${this.rootSelector}
    [data-test-menu-item-id="${this.itemId}"]
    `;
  }

  assertExist() {
    cy.get(this.buildSelector()).should('exist');
  }

  assertDisabled() {
    cy.get(this.buildSelector())
      .should('have.class', 'ant-dropdown-menu-item-disabled');
  }

  assertEnabled() {
    cy.get(this.buildSelector())
      .should('not.have.class', 'ant-dropdown-menu-item-disabled');
  }

  assertIsChecked() {
    cy.get(this.buildSelector()).find('span.ant-checkbox').should('have.class', 'ant-checkbox-checked');
  }

  assertIsNotChecked() {
    cy.get(this.buildSelector()).find('span.ant-checkbox').should('not.have.class', 'ant-checkbox-checked');
  }

  click() {
    cy.get(this.buildSelector()).click();
  }

}
