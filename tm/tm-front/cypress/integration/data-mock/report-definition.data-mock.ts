import {combineWithDefaultData} from './data-mocks.utils';
import {ReportDefinitionViewModel} from '../model/custom-report/report-definition.model';

export function mockReportDefinitionViewModel(customData: Partial<ReportDefinitionViewModel> = {}): ReportDefinitionViewModel {
  return combineWithDefaultData({
    id: 1,
    name: '',
    summary: '',
    description: '',
    pluginNamespace: '',
    parameters: [],
    missingPlugin: false,
    reportLabel: '',
    attributes: {},
    customReportLibraryNodeId: 1,
    projectId: 1
  }, customData);
}
