import {AttachmentListModel} from '../model/attachment/attachment-list.model';

export function mockEmptyAttachmentListModel(): AttachmentListModel {
  return {
    id: 1,
    attachments: [],
  };
}
