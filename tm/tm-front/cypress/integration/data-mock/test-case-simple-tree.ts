import {DataRow, DataRowOpenState, GridResponse} from '../model/grids/data-row.type';

export const initialTestCaseLibraries: GridResponse = {
  count: 1,
  dataRows: [{
    id: 'TestCaseLibrary-1',
    children: [],
    data: {'NAME': 'Project1'}
  } as unknown as DataRow]
};

export const projectOneChildren = [
  {
    id: 'TestCaseLibrary-1',
    children: ['TestCaseFolder-1', 'TestCase-3', 'TestCaseFolder-2'],
    data: {'NAME': 'Project1', 'CHILD_COUNT': 3},
    projectId: 1,
    state: DataRowOpenState.open
  } as unknown as DataRow,
  {
    id: 'TestCaseFolder-1',
    children: [],
    data: {'NAME': 'folder1'},
    parentRowId: 'TestCaseLibrary-1',
    projectId: 1,
  } as unknown as DataRow,
  {
    id: 'TestCase-3',
    children: [],
    data: {'NAME': 'a nice test', 'CHILD_COUNT': 0, 'TC_STATUS': 'APPROVED', 'TC_KIND': 'STANDARD', 'IMPORTANCE': 'HIGH'},
    parentRowId: 'TestCaseLibrary-1',
    projectId: 1,
  } as unknown as DataRow,
  {
    id: 'TestCaseFolder-2',
    children: [],
    data: {'NAME': 'folder2'},
    parentRowId: 'TestCaseLibrary-1',
    projectId: 1,
  } as unknown as DataRow
];
