import {NavBarElement} from '../../elements/nav-bar/nav-bar.element';
import {TestCaseWorkspacePage} from '../../pages/test-case-workspace/test-case-workspace.page';
import {TestCaseViewPage} from '../../pages/test-case-workspace/test-case/test-case-view.page';
import {TestCaseViewParametersPage} from '../../pages/test-case-workspace/test-case/test-case-view-parameters.page';
import Chainable = Cypress.Chainable;

export class TestCaseE2eCommands {
  public static createTestCase(name: string, reference: string, description: string, projectName: string) {
    let testCaseWorkspacePage: TestCaseWorkspacePage;
    testCaseWorkspacePage = NavBarElement.navigateToTestCaseWorkspace();
    testCaseWorkspacePage.selectTreeNodeByName(projectName, 'project', true);
    const createTestCaseDialog = testCaseWorkspacePage.treeMenu.openCreateTestCase();
    createTestCaseDialog.fillFieldsWithoutCuf(name, reference, description);
    createTestCaseDialog.clickOnAddButton();
  }

  public static addAParameterToTestCase(projectName: string, testCaseName: string, paramName: string) {
    const testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
    testCaseWorkspace.tree.findRowId('NAME', testCaseName).then(testCaseId => {
      const testCaseViewPage = testCaseWorkspace.tree.selectNode(testCaseId) as TestCaseViewPage;
      const testCaseViewParametersPage = testCaseViewPage.clickAnchorLink('parameters') as TestCaseViewParametersPage;
      const createParameterDialogElement = testCaseViewParametersPage.openAddParameterDialog();
      createParameterDialogElement.fillName(paramName);
      createParameterDialogElement.addParam();
      testCaseViewParametersPage.checkExistingParam(paramName);
    });
  }

  public static findCufLabelId(cufCode: string): Chainable {
    return cy.get('div')
      .contains('div', cufCode)
      .invoke('attr', 'data-test-cuf-label-id');
  }

  public static findCufId(cufCode: string): Chainable {
    return cy.get('sqtm-core-label')
      .contains('sqtm-core-label', cufCode)
      .next('div')
      .find('sqtm-core-custom-field-widget')
      .invoke('attr', 'data-test-customfield-name');
  }
}

