import {EntityViewPage, Page} from '../page';

export class CustomReportLibraryViewPage extends EntityViewPage {

  constructor(private customReportLibraryNodeId: number | '*') {
    super('sqtm-app-custom-report-library-view');
  }

  clickAnchorLink(linkId: string): Page {
    return undefined;
  }
}
