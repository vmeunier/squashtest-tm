import {PageFactory} from '../../page';
import {NavBarElement} from '../../../elements/nav-bar/nav-bar.element';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {GridElement} from '../../../elements/grid/grid.element';
import {AutomationProgrammerWorkspacePage} from './automation-programmer-workspace.page';
import {GridResponse} from '../../../../model/grids/data-row.type';
import {ReferentialData} from '../../../../model/referential-data.model';
import {ReferentialDataProviderBuilder} from '../../../../utils/referential/referential-data.provider';
import {AutomationWorkspaceDataModel, defaultAutomationWorkspaceData} from './utils/automation-programmer-utils';
import {selectByDataTestButtonId} from '../../../../utils/basic-selectors';
import {ToolbarElement} from '../../../elements/workspace-common/toolbar.element';


export class AutomationProgrammerAssigneePage extends AutomationProgrammerWorkspacePage {

  public readonly navBar = new NavBarElement();

  gridToolBarElement: ToolbarElement;

  protected constructor(public readonly grid: GridElement) {
    super(grid, 'sqtm-app-automation-workspace-assignee');
    this.gridToolBarElement = new ToolbarElement('assignee-toolbar');
  }

  public static initTestAtPage: PageFactory<AutomationProgrammerAssigneePage> =
    (initialNodes: GridResponse = {dataRows: []}, initialWorkspaceModel: AutomationWorkspaceDataModel = defaultAutomationWorkspaceData,
     referentialData?: ReferentialData) => {

      const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
      const mockBuilder = new HttpMockBuilder('automation-workspace/data').responseBody(initialWorkspaceModel).build();
      const gridElement = GridElement.createGridElement('automation-programmer-assigned',
        'automation-workspace/assignee-autom-req', initialNodes);
      const page = new AutomationProgrammerAssigneePage(gridElement);

      // visit page
      cy.visit(`automation-workspace/automation-programmer-workspace/assignee`);

      // wait for ref data request to fire
      referentialDataProvider.wait();

      mockBuilder.wait();

      // wait for initial grid data and additional requests to fire
      page.waitInitialDataFetch();
      // Check page initialisation
      page.assertExist();

      return page;
    }


  getPageUrl(): string {
    return 'assignee';
  }

  unAssigned() {


    return cy.get(selectByDataTestButtonId('unassign'));
  }

  automatedButton() {
    return cy.get(selectByDataTestButtonId('automated'));
  }
}
