import {User} from '../../../../../model/user/user.model';

export class AutomationFunctionalTesterWorkspaceDataModel {
  nbGlobalAutomReq: number;
  nbReadyForTransmissionAutomReq: number;
  nbToBeValidatedAutomReq: number;
  usersWhoModifiedTestCasesGlobalView: User[];
  usersWhoModifiedTestCasesReadyView: User[];
  usersWhoModifiedTestCasesValidateView: User[];
  loaded?: boolean;
}

export const defaultFunctionalTesterWorkspaceData: AutomationFunctionalTesterWorkspaceDataModel = {
  nbGlobalAutomReq: 7,
  nbReadyForTransmissionAutomReq: 3,
  nbToBeValidatedAutomReq: 5,
  usersWhoModifiedTestCasesGlobalView: [{
    id: 1,
    login: 'admin'
  } as User],
  usersWhoModifiedTestCasesReadyView: [{
    id: 1,
    login: 'admin'
  } as User],
  usersWhoModifiedTestCasesValidateView: [{
    id: 1,
    login: 'admin'
  } as User]
};
