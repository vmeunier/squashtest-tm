import {TreeElement} from '../elements/grid/grid.element';
import {DataRow} from '../../model/grids/data-row.type';
import {NavBarElement} from '../elements/nav-bar/nav-bar.element';
import {HttpMockBuilder} from '../../utils/mocks/request-mock';
import {Page} from './page';

export abstract class WorkspaceWithTreePage extends Page {

  protected constructor(public readonly tree: TreeElement, rootSelector: string) {
    super(rootSelector);
    cy.get(this.rootSelector).should('have.length', 1);
    this.tree.assertExist();
  }

  public activateMilestoneMode(milestone: string | number, refreshedRows?: DataRow[]) {
    const milestoneSelector = new NavBarElement().openMilestoneSelector();
    milestoneSelector.selectMilestone(milestone);
    const mock = new HttpMockBuilder(this.tree.url)
      .post()
      .responseBody({dataRows: refreshedRows})
      .build();
    milestoneSelector.confirm();
    mock.wait();
  }

  public disableMilestoneMode(refreshedRows?: DataRow[]) {
    const mock = new HttpMockBuilder(this.tree.url)
      .post()
      .responseBody({dataRows: refreshedRows})
      .build();
    new NavBarElement().disableMilestoneMode();
    mock.wait();
  }
}
