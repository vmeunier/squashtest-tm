import {EntityViewPage} from '../../page';
import {TestCaseFolderAnchorLinks} from '../test-case-folder/test-case-folder-view.page';
import {TestCaseStatisticPanelElement} from '../panels/test-case-statistic-panel.element';
import {selectByDataTestComponentId} from '../../../../utils/basic-selectors';

export class TestCaseMilestoneViewPage extends EntityViewPage {

  public constructor() {
    super('sqtm-app-test-case-milestone-view');
  }

  clickAnchorLink<T>(linkId: TestCaseMilestoneViewAnchorLinks, data?: any): T {
    let element: T;
    switch (linkId) {
      case 'information':
        element = this.showInformationPanel(linkId) as unknown as T;
        break;
      case 'dashboard':
        element = this.showDashboard(linkId) as unknown as T;
        break;
      default :
        throw Error(`Unknown linkId : ${linkId}`);
    }
    return element;
  }

  private showInformationPanel<T>(linkId: 'information'): MilestoneInformationPanel {
    this.clickOnAnchorLink(linkId);
    return new MilestoneInformationPanel();
  }

  private clickOnAnchorLink(linkId: TestCaseFolderAnchorLinks) {
    cy.get(`[anchor-link-id=${linkId}]`).click().trigger('mouseleave');
    cy.removeNzTooltip();
  }

  private showDashboard(linkId: 'dashboard') {
    this.clickOnAnchorLink(linkId);
    return new TestCaseStatisticPanelElement();
  }

  assertNameEquals(expectedName: string) {
    cy.get(this.rootSelector)
      .find(selectByDataTestComponentId('milestone-label'))
      .should('contain.text', expectedName);
  }

}

export type TestCaseMilestoneViewAnchorLinks = 'dashboard' | 'information';


export class MilestoneInformationPanel {
  assertStatusEquals(expectedStatus: string) {
    cy.get('span.label')
      .contains('Statut')
      .next()
      .should('contain.text', expectedStatus);
  }

  assertEndDateEquals(expectedDate: string) {
    cy.get('span.label')
      .contains('Échéance')
      .next()
      .should('contain.text', expectedDate);
  }
}
