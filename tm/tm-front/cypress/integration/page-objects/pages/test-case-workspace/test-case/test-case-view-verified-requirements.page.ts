import {TestCaseViewPage} from './test-case-view.page';
import {Page} from '../../page';

export class TestCaseViewVerifiedRequirementsPage extends Page {
  constructor(private parentPage: TestCaseViewPage) {
    super('sqtm-app-coverage-table');
  }

  get testCaseId(): number|string {
    return this.parentPage.testCaseId;
  }

  checkTableFieldsNames(names: Array<string>) {
    const checkedNames = Array<string>();
    names.push('');
    cy.get(`
    ${this.rootSelector}
    div.sqtm-grid-header-viewport
    `)
      .find('span')
      .each(($el, index, $lis) => {
        checkedNames.push($el.text());
        assert(checkedNames[index] === names[index]);
      });

  }


}
