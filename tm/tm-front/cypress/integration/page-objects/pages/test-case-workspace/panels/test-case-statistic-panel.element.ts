import {ChartElement} from '../../../elements/charts/chart.element';
import {selectByDataTestComponentId} from '../../../../utils/basic-selectors';
import {TestCaseStatistics} from '../../../../model/test-case/test-case-statistics.model';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';

export class TestCaseStatisticPanelElement {

  private statPanelTag = `sqtm-app-test-case-statistic-panel`;

  private customDashboardTag = `sqtm-app-custom-dashboard`;

  public coverageChart = new ChartElement('coverage-test-case');
  public statusChart = new ChartElement('status-test-case');
  public importanceChart = new ChartElement('importance-test-case');
  public sizeChart = new ChartElement('size-test-case');

  constructor() {
  }

  assertTitleExist(expectedTitle: string) {
    cy.get('.ant-collapse-header').should('contain.text', expectedTitle);
  }

  refreshStatistics(stats?: TestCaseStatistics) {
    const selector = `.ant-collapse-header ${selectByDataTestComponentId('refresh-button')}`;
    const mock = new HttpMockBuilder('test-case/statistics').post().responseBody(stats).build();
    cy.get(selector).click();
    mock.wait();
  }

  assertFooterContains(expected: string) {
    const selector = `${this.statPanelTag} ${selectByDataTestComponentId('footer')}`;
    cy.get(selector).should('contain.text', expected);
  }

  assertCustomDashboardExist() {
    cy.get(this.customDashboardTag).should('exist');
  }
}
