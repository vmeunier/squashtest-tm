import {DeleteConfirmDialogElement} from '../../../elements/dialog/delete-confirm-dialog.element';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';

export class RemoveCoveragesDialogElement extends DeleteConfirmDialogElement {

  testCaseId: number;
  requirementVersions: number[];

  constructor(testCaseId: number, requirementVersions: number[]) {
    super('confirm-delete');
    this.testCaseId = testCaseId;
    this.requirementVersions = requirementVersions;
  }

  deleteForFailure(response: any) {
  }

  deleteForSuccess(response?: any) {
    const removeMock = new HttpMockBuilder<any>(`test-cases/${this.testCaseId}/verified-requirement-versions/${[this.requirementVersions]}`)
      .delete().responseBody(response).build();
    this.clickOnConfirmButton();
    removeMock.wait();
  }
}
