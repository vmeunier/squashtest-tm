import {AlertDialogElement} from '../../../../elements/dialog/alert-dialog.element';

export class NoWritingRightsDialogElement extends AlertDialogElement {

  constructor() {
    super('alert');
  }

  assertMessage() {
    this.assertHasMessage('Certains éléments ne seront pas modifiés car : soit le statut de l\'un de leurs jalons' +
      ' associés ne le permettent pas soit vous ne disposez pas de droits suffisants.');
  }
}
