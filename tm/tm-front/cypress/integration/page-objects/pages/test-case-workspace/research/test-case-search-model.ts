import {UserView} from '../../../../model/user/user-view';

export class TestCaseSearchModel {
  usersWhoCreatedTestCases: UserView[];
  usersWhoModifiedTestCases: UserView[];
}
