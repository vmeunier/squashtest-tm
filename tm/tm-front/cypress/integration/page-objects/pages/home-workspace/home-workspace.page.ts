import {Page} from '../page';
import {NavBarElement} from '../../elements/nav-bar/nav-bar.element';
import {ReferentialData} from '../../../model/referential-data.model';
import {ReferentialDataProviderBuilder} from '../../../utils/referential/referential-data.provider';
import {HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {HomeWorkspaceModel} from '../../../model/home/home-workspace.model';

export class HomeWorkspacePage extends Page {
  public readonly navBar: NavBarElement = new NavBarElement();

  public constructor() {
    super('sqtm-app-home-workspace-page');
  }

  public static initTestAtPage(referentialData?: ReferentialData) {
    return HomeWorkspacePage.initTestAtPageWithModel(null, referentialData);
  }

  public static initTestAtPageWithModel(homeWorkspaceModel?: HomeWorkspaceModel, referentialData?: ReferentialData): HomeWorkspacePage {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const homePageModelMock = new HttpMockBuilder('home-workspace').responseBody(homeWorkspaceModel).build();
    cy.visit('home-workspace');
    referentialDataProvider.wait();
    homePageModelMock.wait();
    return new HomeWorkspacePage();
  }

  public showDashboard(): void {
    const userPrefsMock = new HttpMockBuilder('user-prefs/update').post().build();
    cy.get(this.selectByComponentId('show-dashboard-button')).click();
    userPrefsMock.wait();
  }

  public showWelcomeMessage(): void {
    cy.get(this.selectByComponentId('show-welcome-message-button')).click();
  }

  public assertMessageIsVisible(): void {
    cy.get(this.selectByComponentId('welcome-message-container')).should('be.visible');
  }

  public assertDashboardIsVisible(): void {
    cy.get(this.selectByComponentId('dashboard-container')).should('be.visible');
  }

  public assertDefaultMessageIsVisible(): void {
    cy.get(this.selectByComponentId('default-welcome-message')).should('be.visible');
  }

  assertCustomMessageIsVisible() {
    cy.get(this.selectByComponentId('custom-welcome-message')).should('be.visible');
  }

  assertCustomMessageContains(expectedMessage: string) {
    cy.get(this.selectByComponentId('custom-welcome-message')).should('contain.html', expectedMessage);
  }

  assertLicenseMessageDoesNotExist(): void {
    cy.get(this.selectByComponentId('license-message-container')).should('not.exist');
  }

  assertLicenseMessageIsVisible(): void {
    cy.get(this.selectByComponentId('license-message-container')).should('be.visible');
  }
}
