import {NavBarElement} from '../../elements/nav-bar/nav-bar.element';
import {GridElement} from '../../elements/grid/grid.element';
import {AdministrationWorkspacePage} from './administration-workspace.page';
import {PageFactory} from '../page';
import {ReferentialData} from '../../../model/referential-data.model';
import {CreateMilestoneDialog} from './dialogs/create-milestone-dialog.element';
import {AdminReferentialDataProviderBuilder} from '../../../utils/referential/admin-referential-data.provider';
import {MilestoneViewPage} from './milestone-view/milestone-view.page';
import {HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {User} from '../../../model/user/user.model';
import {MilestoneAdminView} from '../../../model/milestone/milestone.model';
import {GridResponse} from '../../../model/grids/data-row.type';

export class AdminWorkspaceMilestonesPage extends AdministrationWorkspacePage {

  public readonly navBar = new NavBarElement();

  constructor(public readonly grid: GridElement) {
    super(grid, 'sqtm-app-milestone-workspace');
  }

  public static initTestAtPageMilestones: PageFactory<AdminWorkspaceMilestonesPage> =
    (milestones: GridResponse, referentialData?: ReferentialData) => {
      return AdminWorkspaceMilestonesPage.initTestAtPage(milestones, 'milestones', 'milestones', referentialData);
    }

  public static initTestAtPage: PageFactory<AdminWorkspaceMilestonesPage> =
    (milestones: GridResponse, gridId: string, pageUrl: string,
     referentialData?: ReferentialData) => {
      const adminReferentialDataProvider = new AdminReferentialDataProviderBuilder(referentialData).build();
      const milestonesMock = new HttpMockBuilder('milestones').responseBody(milestones).post().build();
      const gridElement = GridElement.createGridElement(gridId);
      // visit page
      cy.visit(`administration-workspace/${pageUrl}`);
      // wait for ref data request to fire
      adminReferentialDataProvider.wait();
      // wait for initial milestones data request to fire
      milestonesMock.waitResponseBody();
      return new AdminWorkspaceMilestonesPage(gridElement);
    }

  openCreateMilestone(): CreateMilestoneDialog {
    this.clickCreateButton();
    return new CreateMilestoneDialog();
  }

  protected getPageUrl(): string {
    return 'milestones';
  }

  protected getDeleteUrl(): string {
    return 'milestones';
  }

  selectMilestoneByLabel(label: string, viewResponse: MilestoneAdminView, possibleOwners: User[]): MilestoneViewPage {
    const view = new MilestoneViewPage(possibleOwners);
    const mock = new HttpMockBuilder('milestone-view/*')
      .responseBody(viewResponse)
      .build();

    this.selectRowWithMatchingCellContent('label', label);

    mock.waitResponseBody().then(() => {
      view.waitInitialDataFetch();
      view.assertExist();
    });

    return view;
  }
}
