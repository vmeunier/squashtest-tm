import {WorkspaceWithGridPage} from '../page';
import {NavBarElement} from '../../elements/nav-bar/nav-bar.element';
import {GridElement} from '../../elements/grid/grid.element';
import {GridResponse} from '../../../model/grids/data-row.type';
import {HttpMock, HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {selectByDataTestToolbarButtonId} from '../../../utils/basic-selectors';

export abstract class AdministrationWorkspacePage extends WorkspaceWithGridPage {

  public readonly navBar = new NavBarElement();

  protected deleteMock: HttpMock<any>;

  protected constructor(public readonly grid: GridElement, rootSelector: string) {
    super(grid, rootSelector);
  }

  selectRowsWithMatchingCellContent(cellId: string, contents: string[]) {
    this.grid.selectRowsWithMatchingCellContent(cellId, contents);
  }

  selectRowWithMatchingCellContent(cellId: string, content: string) {
    this.grid.selectRowWithMatchingCellContent(cellId, content);
  }

  deleteRowWithMatchingCellContent(
    cellId: string, content: string, confirm: boolean, gridResponse?: GridResponse, error?: boolean, errorMessage?: string) {
    // Prepare mocks
    this.prepareDeleteMock();

    if (confirm) {
      // We need to provide a response for the refreshData mock (even in e2e tests) so the following line makes sure a mock response is
      // indeed provided
      gridResponse = gridResponse || {
        count: 0,
        dataRows: [],
      };

      this.grid.declareRefreshData(gridResponse);
    }

    // Do click
    const button = cy.get(`sqtm-core-grid-cell[data-test-cell-id="${cellId}"]`)
      .contains('span', content)
      .closest('sqtm-core-grid-cell')
      .siblings('sqtm-core-grid-cell[data-test-cell-id="delete"]')
      .find('i');

    button.should('exist');
    button.click();

    if (error) {
      cy.log('ERRRROR ' + error );
      cy.get('sqtm-core-alert-dialog')
        .find('span[data-test-dialog-message]')
        .should('have.text', errorMessage);

      cy.get('button[data-test-dialog-button-id="close"]').click().as('button');

      cy.get('@button').should('not.exist');
    } else {
      if (confirm) {
        this.confirmDelete();

        // Wait for requests to fulfill
        this.waitForDelete();
        this.grid.waitForRefresh();
      } else {
        this.cancelDelete();
      }
    }
  }

  clickOnMultipleDeleteButton(confirm: boolean, gridResponse?: GridResponse) {
    this.prepareDeleteMock();

    if (confirm) {
      // We need to provide a response for the refreshData mock (even in e2e tests) so the following line makes sure a mock response is
      // indeed provided
      gridResponse = gridResponse || {
        count: 0,
        dataRows: [],
      };

      this.grid.declareRefreshData(gridResponse);
    }

    cy.get(selectByDataTestToolbarButtonId('delete-button')).click();

    if (confirm) {
      this.confirmDelete();
      this.waitForDelete();
      this.grid.waitForRefresh();
    } else {
      this.cancelDelete();
    }
  }

  protected confirmDelete() {
    cy.get('sqtm-core-confirm-delete-dialog ' +
      'button[data-test-dialog-button-id="confirm"]')
      .should('exist')
      .click();

    cy.get('#sqtm-core-confirm-delete-dialog')
      .should('not.exist');
  }

  protected cancelDelete() {
    cy.get('sqtm-core-confirm-delete-dialog ' +
      'button[data-test-dialog-button-id="cancel"]')
      .should('exist')
      .click()
      .should('not.exist');
  }

  protected clickCreateButton() {
    cy.get(selectByDataTestToolbarButtonId('create-button')).click();
  }

  protected prepareDeleteMock() {
    if (this.deleteMock == null) {
      this.deleteMock = new HttpMockBuilder(this.getDeleteUrl() + '/*').delete().build();
    }
  }

  protected waitForDelete() {
    if (this.deleteMock == null) {
      throw new Error('You need to call "prepareDeleteMock" before calling "waitForDelete" !');
    }

    this.deleteMock.wait();
  }

  protected abstract getPageUrl(): string;

  /**
   * Returns the server's endpoint for multi delete
   */
  protected getDeleteUrl(): string {
    throw new Error('If you wan\'t to use the "delete" buttons, you need to override this method to provide the actual ' +
      'delete endpoint for this page !');
  }
}
