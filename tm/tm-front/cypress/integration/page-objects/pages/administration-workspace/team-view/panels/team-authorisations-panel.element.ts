import {GridElement} from '../../../../elements/grid/grid.element';
import {HttpMockBuilder} from '../../../../../utils/mocks/request-mock';
import {ProjectWithoutPermission} from '../../user-view/dialogs/add-user-authorisations.dialog';
import {AddTeamAuthorisationsDialog} from '../dialogs/add-team-authorisations.dialog';
import {selectByDataTestToolbarButtonId} from '../../../../../utils/basic-selectors';

export class TeamAuthorisationsPanelElement {
  public readonly grid: GridElement;

  constructor() {
    this.grid = GridElement.createGridElement('team-authorisations');
  }

  waitInitialDataFetch() {
  }

  deleteOne(projectName: string) {
    this.grid.findRowId('projectName', projectName).then((id) => {
      this.grid
        .getRow(id)
        .cell('delete')
        .iconRenderer()
        .click();

      const deleteMock = new HttpMockBuilder('teams/*/permissions/*')
        .delete()
        .build();

      this.clickConfirmDeleteButton();

      deleteMock.wait();
    });
  }

  deleteMultiple(projectNames: string[]) {
    this.grid.selectRowsWithMatchingCellContent('projectName', projectNames);

    const deleteMock = new HttpMockBuilder('teams/*/permissions/*')
      .delete()
      .build();

    this.clickOnDeleteButton();
    this.clickConfirmDeleteButton();

    deleteMock.wait();
  }

  clickOnAddPermissionButton(projectsWithoutPermission?: ProjectWithoutPermission[]): AddTeamAuthorisationsDialog {
    const dialog = new AddTeamAuthorisationsDialog(this.grid, projectsWithoutPermission);

    cy.get('[data-test-button-id="add-authorisation"]')
      .should('exist')
      .click();


    dialog.waitInitialDataFetch();
    dialog.assertExist();

    return dialog;
  }



  private clickOnDeleteButton() {
    cy.get(selectByDataTestToolbarButtonId('remove-authorisations'))
      .should('exist')
      .click();
  }

  private clickConfirmDeleteButton() {
    cy.get('sqtm-core-confirm-delete-dialog')
      .find('[data-test-dialog-button-id="confirm"]')
      .click()
      // Then
      .get('sqtm-core-confirm-delete-dialog')
      .should('not.exist');
  }
}
