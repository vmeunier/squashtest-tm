import {HttpMock, HttpMockBuilder} from '../../../../../utils/mocks/request-mock';
import {GroupedMultiListElement} from '../../../../elements/filters/grouped-multi-list.element';
import {GridElement} from '../../../../elements/grid/grid.element';


export interface TeamAssociation {
  partyId: string;
  name: string;
}


export class AssociateTeamToUserDialog {

  private readonly dialogId = 'associate-team-to-user-dialog';

  private unassociatedTeamMock: HttpMock<TeamAssociation[]>;

  constructor(private readonly permissionsGrid: GridElement, unassociatedTeams: TeamAssociation[]) {
    this.unassociatedTeamMock = new HttpMockBuilder<TeamAssociation[]>('user-view/*/unassociated-teams')
      .responseBody(unassociatedTeams)
      .build();
  }

  waitInitialDataFetch() {
    this.unassociatedTeamMock.wait();
  }

  assertExist() {
    cy.get(this.buildSelector()).should('exist');
  }

  confirm(updatedTeams?: TeamAssociation[]) {
    const mock = new HttpMockBuilder('users/*/teams/*')
      .responseBody({teams: updatedTeams})
      .post()
      .build();

    this.clickOnConfirmButton();

    mock.wait();
  }

  cancel() {
    const buttonSelector = this.buildButtonSelector('cancel');
    cy.get(buttonSelector).should('exist');
    cy.get(buttonSelector).click();
  }

  buildSelector(): string {
    return `[data-test-dialog-id=${this.dialogId}]`;
  }

  clickOnConfirmButton() {
    const buttonSelector = this.buildButtonSelector('confirm');
    cy.get(buttonSelector).should('exist');
    cy.get(buttonSelector).click();
  }

  private buildButtonSelector(buttonId: string) {
    return `${this.buildSelector()} [data-test-dialog-button-id=${buttonId}]`;
  }

  selectTeams(...teamNames: string[]) {
    cy.get('[data-test-component-id="grouped-multi-list-display-value"]').click();
    const multiList = new GroupedMultiListElement();
    teamNames.forEach(name => multiList.toggleOneItem(name));
    cy.clickVoid();
    multiList.assertNotExist();
  }
}
