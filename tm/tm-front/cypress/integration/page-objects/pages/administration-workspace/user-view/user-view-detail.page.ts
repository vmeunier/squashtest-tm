import {UserViewPage} from './user-view.page';

export class UserViewDetailPage extends UserViewPage {
  constructor() {
    super('sqtm-app-user-view-detail > div');
  }

  clickBackButton(): void {
    cy.get(this.rootSelector)
      .find('[data-test-button-id="back"]')
      .click();
  }
}
