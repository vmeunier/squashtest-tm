import {ConfirmDialogElement} from '../../../../elements/dialog/confirm-dialog.element';
import {HttpMockBuilder} from '../../../../../utils/mocks/request-mock';


export class ConfirmInfoListBindingAlert extends ConfirmDialogElement {
  constructor() {
    super('confirm-info-list-binding-dialog');
  }

  assertMessage() {
    this.assertHasMessage(`La liste sera modifiée. Cette action ne peut être annulée.
Le champ correspondant sera valorisé par la valeur par défaut de la nouvelle liste sélectionnée.
Confirmez-vous la modification de la liste ?`);
  }

  confirm(modifiedAttributeName: 'type' | 'nature' | 'category') {
    const mock = new HttpMockBuilder('info-list-binding/project/*/' + modifiedAttributeName).post().build();
    this.clickOnConfirmButton();
    mock.wait();
  }
}
