import {GridElement} from '../../../../elements/grid/grid.element';
import {HttpMockBuilder} from '../../../../../utils/mocks/request-mock';
import {CreateProjectPermissionsDialog, UnboundPartiesResponse} from '../dialogs/create-project-permissions.dialog';
import {PartyProjectPermission} from '../../../../../model/project/project.model';
import {selectByDataTestButtonId, selectByDataTestMenuItemId} from '../../../../../utils/basic-selectors';

export class PermissionsPanelElement {
  public readonly grid: GridElement;

  constructor() {
    this.grid = GridElement.createGridElement('project-permissions');
  }

  waitInitialDataFetch() {
  }

  deleteOne(partyName: string, updatedPermissions: PartyProjectPermission[] = []) {
    this.grid.findRowId('partyName', partyName).then((id) => {
      this.grid
        .getRow(id)
        .cell('delete')
        .iconRenderer()
        .click();

      const deleteMock = new HttpMockBuilder('generic-projects/*/permissions/*')
        .delete()
        .responseBody({partyProjectPermissions: updatedPermissions})
        .build();

      this.clickConfirmDeleteButton();

      deleteMock.wait();
    });
  }

  deleteMultiple(partyNames: string[], updatedPermissions: PartyProjectPermission[] = []) {
    this.grid.selectRowsWithMatchingCellContent('partyName', partyNames);

    const deleteMock = new HttpMockBuilder('generic-projects/*/permissions/*')
      .delete()
      .responseBody({partyProjectPermissions: updatedPermissions})
      .build();

    this.clickOnDeleteButton();
    this.clickConfirmDeleteButton();

    deleteMock.wait();
  }

  clickOnAddUserPermissionButton(unboundParties?: UnboundPartiesResponse): CreateProjectPermissionsDialog {
    const dialog = new CreateProjectPermissionsDialog(this.grid, unboundParties);

    cy.get(selectByDataTestButtonId('add-permission'))
      .should('exist')
      .click();

    cy.get('[data-test-menu-id="permission-menu"]')
      .should('exist');

    cy.get(selectByDataTestMenuItemId('add-user-permission'))
      .should('exist')
      .click();

    dialog.waitInitialDataFetch();
    dialog.assertExist();

    return dialog;
  }

  clickOnAddTeamPermissionButton(unboundParties?: UnboundPartiesResponse): CreateProjectPermissionsDialog {
    const dialog = new CreateProjectPermissionsDialog(this.grid, unboundParties);

    cy.get('[data-test-button-id="add-permission"]')
      .should('exist')
      .click();

    cy.get('[data-test-menu-id="permission-menu"]')
      .should('exist');

    cy.get('[data-test-menu-item-id="add-team-permission"]')
      .should('exist')
      .click();

    dialog.waitInitialDataFetch();
    dialog.assertExist();

    return dialog;
  }

  private clickOnDeleteButton() {
    cy.get('[data-test-button-id="remove-permissions"]')
      .should('exist')
      .click();
  }

  private clickConfirmDeleteButton() {
    cy.get('sqtm-core-confirm-delete-dialog')
      .find('[data-test-dialog-button-id="confirm"]')
      .click()
      // Then
      .get('sqtm-core-confirm-delete-dialog')
      .should('not.exist');
  }
}
