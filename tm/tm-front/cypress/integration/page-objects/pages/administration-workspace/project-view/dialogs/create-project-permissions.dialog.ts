import {HttpMock, HttpMockBuilder} from '../../../../../utils/mocks/request-mock';
import {GroupedMultiListElement} from '../../../../elements/filters/grouped-multi-list.element';
import {SelectFieldElement} from '../../../../elements/forms/select-field.element';
import {GridElement} from '../../../../elements/grid/grid.element';
import {PartyProjectPermission} from '../../../../../model/project/project.model';
import {CommonSelectors} from '../../../../elements/forms/abstract-form-field.element';

interface Party {
  id: string;
  label: string;
  groupId: string;
}

export interface UnboundPartiesResponse {
  teams: Party[];
  users: Party[];
}

export class CreateProjectPermissionsDialog {

  public readonly profileSelectField: SelectFieldElement;

  private readonly dialogId = 'add-permission-dialog';

  private unboundPartiesMock: HttpMock<UnboundPartiesResponse>;

  constructor(private readonly permissionsGrid: GridElement, unboundParties: UnboundPartiesResponse) {
    this.unboundPartiesMock = new HttpMockBuilder<UnboundPartiesResponse>('project-view/*/unbound-parties')
      .responseBody(unboundParties)
      .build();

    this.profileSelectField = new SelectFieldElement(CommonSelectors.fieldName('permission-profile'));
  }

  waitInitialDataFetch() {
    this.unboundPartiesMock.wait();
  }

  assertExist() {
    cy.get(this.buildSelector()).should('exist');
  }

  confirm(updatedPermissions?: PartyProjectPermission[]) {
    const mock = new HttpMockBuilder('generic-projects/*/permissions/*/group')
      .responseBody({partyProjectPermissions: updatedPermissions})
      .post()
      .build();

    this.clickOnConfirmButton();

    mock.wait();
  }

  cancel() {
    const buttonSelector = this.buildButtonSelector('cancel');
    cy.get(buttonSelector).should('exist');
    cy.get(buttonSelector).click();
  }

  buildSelector(): string {
    return `[data-test-dialog-id=${this.dialogId}]`;
  }

  clickOnConfirmButton() {
    const buttonSelector = this.buildButtonSelector('confirm');
    cy.get(buttonSelector).should('exist');
    cy.get(buttonSelector).click();
  }

  private buildButtonSelector(buttonId: string) {
    return `${this.buildSelector()} [data-test-dialog-button-id=${buttonId}]`;
  }

  selectParties(...partyNames: string[]) {
   // cy.get('[data-test-component-id="parties-permissions-field"]').click();
    cy.get('[data-test-component-id="grouped-multi-list-display-value"]').click();
    const multiList = new GroupedMultiListElement();
    partyNames.forEach(name => multiList.toggleOneItem(name));
    cy.clickVoid();
    multiList.assertNotExist();
  }

  selectProfile(profile: string) {
    this.profileSelectField.selectValue(profile);
  }
}
