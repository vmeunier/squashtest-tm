import {AdminReferentialData} from '../../../../model/admin-referential-data.model';
import {SystemViewModel} from '../../../../model/system/system-view.model';
import {EditableRichTextFieldElement} from '../../../elements/forms/editable-rich-text-field.element';
import {SystemViewInformationPage} from './system-view-information.page';
import {Page} from '../../page';

export class SystemViewMessagesPage extends Page {
  public readonly loginMessageField: EditableRichTextFieldElement;
  public readonly welcomeMessageField: EditableRichTextFieldElement;

  constructor() {
    super('sqtm-app-system-view-messages > div');

    const changeUrlBase = 'system/messages/';

    this.loginMessageField = new EditableRichTextFieldElement('loginMessage', changeUrlBase + 'login-message');
    this.welcomeMessageField = new EditableRichTextFieldElement('welcomeMessage', changeUrlBase + 'welcome-message');
  }

  static navigateToPage(viewData: SystemViewModel, referentialData?: AdminReferentialData): SystemViewMessagesPage {
    const page = SystemViewInformationPage.navigateToPage(viewData, referentialData);
    return page.clickMessagesAnchor();
  }
}
