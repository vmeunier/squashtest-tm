import {Page} from '../../page';
import {AdminReferentialData} from '../../../../model/admin-referential-data.model';
import {SystemViewModel} from '../../../../model/system/system-view.model';
import {SystemViewInformationPage} from './system-view-information.page';
import Chainable = Cypress.Chainable;

export class SystemViewDownloadsPage extends Page {
  constructor() {
    super('sqtm-app-system-view-downloads > div');
  }

  static navigateToPage(viewData: SystemViewModel, referentialData?: AdminReferentialData): SystemViewDownloadsPage {
    const page = SystemViewInformationPage.navigateToPage(viewData, referentialData);
    return page.clickDownloadsAnchor();
  }

  assertLatestLogLinkExists() {
    this.assertLinkExists('latest-log');
  }

  assertLinkExists(linkId: string) {
    this.getLink(linkId).should('exist');
  }

  assertLinkDoesNotExist(linkId: string): void {
    this.getLink(linkId).should('not.exist');
  }

  private getLink(linkId: string): Chainable {
    return cy.get(`${this.rootSelector} a[data-test-link-id="${linkId}"]`);
  }

  assertPreviousLinksAreNotVisible(): void {
    cy.get(`${this.rootSelector} [data-test-component-id="previous-log-files"]`).should('not.exist');
  }
}
