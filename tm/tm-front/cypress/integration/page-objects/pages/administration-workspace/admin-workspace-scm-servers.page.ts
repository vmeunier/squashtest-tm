import {NavBarElement} from '../../elements/nav-bar/nav-bar.element';
import {GridElement} from '../../elements/grid/grid.element';
import {AdministrationWorkspacePage} from './administration-workspace.page';
import {PageFactory} from '../page';
import {GridResponse} from '../../../model/grids/data-row.type';
import {ReferentialData} from '../../../model/referential-data.model';
import {HttpMock, HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {CreateScmServerDialog} from './dialogs/create-scm-server-dialog.element';
import {AdminReferentialDataProviderBuilder} from '../../../utils/referential/admin-referential-data.provider';
import {AdminScmServer} from '../../../model/scm-server/scm-server.model';
import {ScmServerViewPage} from './scm-server-view/scm-server-view.page';

export class AdminWorkspaceScmServersPage extends AdministrationWorkspacePage {

  public readonly navBar = new NavBarElement();

  private scmServersKindMock: HttpMock<any>;

  constructor(public readonly grid: GridElement, scmPluginInstalled: boolean) {
    super(grid, 'sqtm-app-main-server-workspace');
    if (scmPluginInstalled) {
      this.scmServersKindMock = new HttpMockBuilder<any>('scm-servers/get-scm-server-kinds')
        .responseBody({scmServerKinds: ['Github']})
        .build();
    } else {
      this.scmServersKindMock = new HttpMockBuilder<any>('scm-servers/get-scm-server-kinds')
        .responseBody({scmServerKinds: []})
        .build();
    }
  }

  public static initTestAtPageScmServers: PageFactory<AdminWorkspaceScmServersPage> =
    (initialNodes: GridResponse = {dataRows: []}, referentialData?: ReferentialData) => {
      return AdminWorkspaceScmServersPage.initTestAtPage(initialNodes, 'scm-servers', 'scm-servers',
        'servers/scm-servers', true, referentialData);
    };

  public static initTestAtPageScmServersWithoutScmPluginInstalled: PageFactory<AdminWorkspaceScmServersPage> =
    (initialNodes: GridResponse = {dataRows: []}, referentialData?: ReferentialData) => {
      return AdminWorkspaceScmServersPage.initTestAtPage(initialNodes, 'scm-servers', 'scm-servers',
        'servers/scm-servers', false, referentialData);
    };

  public static initTestAtPage: PageFactory<AdminWorkspaceScmServersPage> =
    (initialNodes: GridResponse = {dataRows: []}, gridId: string, gridUrl: string, pageUrl: string, scmPluginInstalled: boolean,
     referentialData?: ReferentialData) => {
      const adminReferentialDataProvider = new AdminReferentialDataProviderBuilder(referentialData).build();
      const gridElement = GridElement.createGridElement(gridId, gridUrl, initialNodes);
      const page = new AdminWorkspaceScmServersPage(gridElement, scmPluginInstalled);

      // visit page
      cy.visit(`administration-workspace/${pageUrl}`);
      page.assertExist();

      // wait for ref data request to fire
      adminReferentialDataProvider.wait();

      // wait for initial data request to fire
      page.waitInitialDataFetch();

      return page;
    };

  assertExist() {
    super.assertExist();
  }

  waitInitialDataFetch() {
    super.waitInitialDataFetch();
    this.scmServersKindMock.wait();
  }

  openCreateScmServer(): CreateScmServerDialog {
    this.clickCreateButton();
    return new CreateScmServerDialog;
  }

  protected getPageUrl(): string {
    return 'scm-servers';
  }

  selectScmServerByName(names: string[],
                         viewResponse?: AdminScmServer): ScmServerViewPage {

    const view = new ScmServerViewPage();
    const mock = new HttpMockBuilder('scm-server-view/*')
      .responseBody(viewResponse)
      .build();

    this.selectRowsWithMatchingCellContent('name', names);

    mock.waitResponseBody().then(() => {
      view.waitInitialDataFetch();
    });

    return view;
  }

}
