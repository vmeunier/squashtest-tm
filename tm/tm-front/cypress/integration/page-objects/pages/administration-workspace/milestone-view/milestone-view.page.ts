import {Page} from '../../page';
import {EditableTextFieldElement} from '../../../elements/forms/editable-text-field.element';
import {MilestoneInformationPanelElement} from './panels/milestone-information-panel.element';
import {HttpMock, HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {User} from '../../../../model/user/user.model';
import {MilestoneProjectsPanelElement} from './panels/milestone-projects-panel.element';

export class MilestoneViewPage extends Page {

  public readonly informationPanel = new MilestoneInformationPanelElement();
  public readonly projectsPanel = new MilestoneProjectsPanelElement();
  public readonly entityNameField: EditableTextFieldElement;

  private possibleOwnersMock: HttpMock<{ users: User[] }>;

  constructor(possibleOwners: User[] = []) {
    super('sqtm-app-milestone-view');

    const possibleOwnersResponse = {users: possibleOwners};
    this.possibleOwnersMock = new HttpMockBuilder<{ users: User[] }>('milestones/possible-owners')
      .responseBody(possibleOwnersResponse).build();
    this.entityNameField = new EditableTextFieldElement('entity-name', 'milestones/*/label');
  }

  waitInitialDataFetch() {
    this.possibleOwnersMock.wait();
  }

  foldGrid() {
    cy.get(`[data-test-component-id="fold-tree-button"]`).click();
  }
}
