import {MilestoneViewPage} from './milestone-view.page';
import {User} from '../../../../model/user/user.model';

export class MilestoneViewDetailPage extends MilestoneViewPage {
  constructor(possibleOwners?: User[]) {
    super(possibleOwners);
  }

  assertExist() {
    cy.get('sqtm-app-milestone-view-detail').should('have.length', 1);
    super.assertExist();
  }

  clickBackButton(): void {
    cy.get(this.rootSelector)
      .find('[data-test-button-id="back"]')
      .click();
  }
}
