import {Page} from '../../../page';
import {EditableTextFieldElement} from '../../../../elements/forms/editable-text-field.element';

export class ScmServerCommitPolicyPanelElement extends Page {
  committerMailField: EditableTextFieldElement;

  constructor() {
    super('sqtm-app-scm-server-commit-policy-panel');

    this.committerMailField = new EditableTextFieldElement('committer-email', 'scm-servers/*/committer-mail');
  }
}
