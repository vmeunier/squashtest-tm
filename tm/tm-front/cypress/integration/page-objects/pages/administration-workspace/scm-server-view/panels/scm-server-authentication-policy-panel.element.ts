import {Page} from '../../../page';
import {TextFieldElement} from '../../../../elements/forms/TextFieldElement';
import {HttpMockBuilder} from '../../../../../utils/mocks/request-mock';

export class ScmServerAuthenticationPolicyPanelElement extends Page {
  usernameField: TextFieldElement;
  passwordField: TextFieldElement;

  constructor() {
    super('sqtm-app-scm-server-authentication-policy-panel');

    this.usernameField = new TextFieldElement('username');
    this.passwordField = new TextFieldElement('password');
  }

  get sendButtonSelector(): string {
    return this.rootSelector + ' [data-test-component-id="save-credentials-button"]';
  }

  get statusMessageSelector(): string {
    return this.rootSelector + ' [data-test-component-id="status-message"]';
  }


  assertSendButtonEnabled(): void {
    cy.get(this.sendButtonSelector).should('not.be.disabled');
  }

  assertSendButtonDisabled(): void {
    cy.get(this.sendButtonSelector).should('be.disabled');
  }

  sendCredentialsForm(): void {
    const mock = new HttpMockBuilder('scm-servers/*/credentials').post().build();
    cy.get(this.sendButtonSelector).click();
    mock.wait();
  }

  assertStatusMessageNotVisible(): void {
    cy.get(this.statusMessageSelector).should('not.be.visible');
  }

  assertSaveSuccessMessageVisible(): void {
    cy.get(this.statusMessageSelector).should('contain.text', 'Enregistré');
  }
}
