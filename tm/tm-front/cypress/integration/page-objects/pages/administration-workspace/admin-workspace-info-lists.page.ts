import {NavBarElement} from '../../elements/nav-bar/nav-bar.element';
import {GridElement} from '../../elements/grid/grid.element';
import {AdministrationWorkspacePage} from './administration-workspace.page';
import {PageFactory} from '../page';
import {GridResponse} from '../../../model/grids/data-row.type';
import {ReferentialData} from '../../../model/referential-data.model';
import {AdminReferentialDataProviderBuilder} from '../../../utils/referential/admin-referential-data.provider';
import {HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {InfoListViewPage} from './info-list-view/info-list-view.page';
import {AdminInfoList} from '../../../model/infolist/adminInfoList.model';
import {CreateInfoListDialog} from './dialogs/create-info-list-dialog';

export class AdminWorkspaceInfoListsPage extends AdministrationWorkspacePage {

  public readonly navBar = new NavBarElement();

  constructor(public readonly grid: GridElement) {
    super(grid, 'sqtm-app-main-custom-workspace');
  }

  public static initTestAtPageInfoLists: PageFactory<AdminWorkspaceInfoListsPage> =
    (initialNodes: GridResponse = {dataRows: []}, referentialData?: ReferentialData) => {
      return AdminWorkspaceInfoListsPage.initTestAtPage(initialNodes, 'infoLists', 'info-lists',
        'entities-customization/info-lists', referentialData);
    }

  public static initTestAtPage: PageFactory<AdminWorkspaceInfoListsPage> =
    (initialNodes: GridResponse = {dataRows: []}, gridId: string, gridUrl: string, pageUrl: string,
     referentialData?: ReferentialData) => {
      const adminReferentialDataProvider = new AdminReferentialDataProviderBuilder(referentialData).build();
      const gridElement = GridElement.createGridElement(gridId, gridUrl, initialNodes);
      // visit page
      cy.visit(`administration-workspace/${pageUrl}`);
      // wait for ref data request to fire
      adminReferentialDataProvider.wait();
      // wait for initial tree data request to fire
      gridElement.waitInitialDataFetch();
      return new AdminWorkspaceInfoListsPage(gridElement);
    }

  openCreateInfoList(): CreateInfoListDialog {
    this.clickCreateButton();
    return new CreateInfoListDialog();
  }

  protected getPageUrl(): string {
    return 'info-lists';
  }

  protected getDeleteUrl(): string {
    return 'info-lists';
  }

  selectInfoListsByLabel(labels: string[],
                         viewResponse?: AdminInfoList): InfoListViewPage {

    const view = new InfoListViewPage();
    const mock = new HttpMockBuilder('info-list-view/*')
      .responseBody(viewResponse)
      .build();

    this.selectRowsWithMatchingCellContent('label', labels);

    mock.waitResponseBody().then(() => {
      view.waitInitialDataFetch();
    });

    return view;
  }
}
