import {Page} from '../../../page';
import {EditableSelectFieldElement} from '../../../../elements/forms/editable-select-field.element';
import {EditableTextFieldElement} from '../../../../elements/forms/editable-text-field.element';

export class BugTrackerInformationPanelElement extends Page {
  kindField: EditableSelectFieldElement;
  urlField: EditableTextFieldElement;

  constructor() {
    super('sqtm-app-bug-tracker-information-panel');

    const urlPrefix = 'bugtracker/*/';
    this.kindField = new EditableSelectFieldElement('kind', urlPrefix + 'kind');
    this.urlField = new EditableTextFieldElement('url', urlPrefix + 'url');
  }
}
