import {Page} from '../../../page';
import {EditableSelectFieldElement} from '../../../../elements/forms/editable-select-field.element';
import {TextFieldElement} from '../../../../elements/forms/TextFieldElement';
import {HttpMockBuilder} from '../../../../../utils/mocks/request-mock';
import {TextAreaFieldElement} from '../../../../elements/forms/text-area-field.element';

export class BugTrackerAuthenticationProtocolPanelElement extends Page {
  protocolField: EditableSelectFieldElement;
  consumerKeyField: TextFieldElement;
  secretField: TextFieldElement;

  private authFormSelector = '[data-test-component-id="oauth-form"]';

  constructor() {
    super('sqtm-app-bug-tracker-authentication-protocol-panel');

    const urlPrefix = 'bugtracker/*/';
    this.protocolField = new EditableSelectFieldElement('authProtocol', urlPrefix + 'auth-protocol');
    this.consumerKeyField = new TextFieldElement('consumerKey');
    this.secretField = new TextAreaFieldElement('clientSecret');
  }

  get saveButtonSelector(): string {
    return this.rootSelector + ' [data-test-component-id="save-oauth-button"]';
  }

  get statusMessageSelector(): string {
    return this.rootSelector + ' [data-test-component-id="status-message"]';
  }

  assertOAuthFormVisible(): void {
    cy.get(this.authFormSelector).should('be.visible');
  }

  assertOAuthFormNotVisible(): void {
    cy.get(this.authFormSelector).should('not.exist');
  }

  assertSaveButtonEnabled(): void {
    cy.get(this.saveButtonSelector).should('not.be.disabled');
  }

  assertSaveButtonDisabled(): void {
    cy.get(this.saveButtonSelector).should('be.disabled');
  }

  sendOAuthForm(): void {
    const mock = new HttpMockBuilder('bugtracker/*/auth-protocol/configuration').post().build();
    cy.get(this.saveButtonSelector).click();
    mock.wait();
  }

  assertStatusMessageNotVisible(): void {
    cy.get(this.statusMessageSelector).should('not.be.visible');
  }

  assertUnsavedChangesMessageVisible(): void {
    cy.get(this.statusMessageSelector).should('have.text', 'Changements non enregistrés.');
  }

  assertSaveSuccessMessageVisible(): void {
    cy.get(this.statusMessageSelector).should('have.text', 'Enregistré !');
  }

  sendOAuthFormWithServerError(): void {
    const response = {
      status: 412,
      error: {
        squashTMError: {
          kind: 'ACTION_ERROR',
        }
      }
    };

    const mock = new HttpMockBuilder('bugtracker/*/auth-protocol/configuration')
      .post()
      .status(412)
      .responseBody(response)
      .build();

    cy.get(this.saveButtonSelector).click();
    mock.wait();
  }

  assertServerErrorMessageVisible(): void {
    cy.get(this.statusMessageSelector).should('have.text', 'Une erreur est survenue lors du traitement.');
  }
}
