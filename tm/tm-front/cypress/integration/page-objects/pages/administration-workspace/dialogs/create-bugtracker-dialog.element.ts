import {TextFieldElement} from '../../../elements/forms/TextFieldElement';
import {CreateAdministrationEntityDialog} from '../create-administration-entity-dialog';
import {SelectFieldElement} from '../../../elements/forms/select-field.element';
import {CommonSelectors} from '../../../elements/forms/abstract-form-field.element';

export class CreateBugtrackerDialog extends CreateAdministrationEntityDialog {


  private readonly nameField: TextFieldElement;
  private readonly kindField: SelectFieldElement;
  private readonly urlField: TextFieldElement;

  constructor() {
    super('new-bugtracker', 'bugtracker/new', 'bugtrackers');

    this.nameField = new TextFieldElement('name');
    this.kindField = new SelectFieldElement(CommonSelectors.fieldName('kind'));
    this.urlField = new TextFieldElement('url');
  }

  fillName(name: string) {
    this.nameField.fill(name);
  }

  selectKind(group: KindOptions) {
    this.kindField.selectValue(group);
  }

  fillUrl(url: string) {
    this.urlField.fill(url);
  }

  // Used in E2E, not tested in ITs.
  createBugtracker(name: string, kind: KindOptions, url: string) {
    this.fillName(name);
    this.selectKind(kind);
    this.fillUrl(url);
    this.addForSuccess(false);
  }

  checkIfFormIsEmpty() {
    this.nameField.checkContent('');
    this.kindField.selectValue('Mantis');
    this.urlField.checkContent('');
  }

  clearForm(): void {
    this.nameField.clearContent();
    this.urlField.clearContent();
  }
}

type KindOptions = 'Mantis';
