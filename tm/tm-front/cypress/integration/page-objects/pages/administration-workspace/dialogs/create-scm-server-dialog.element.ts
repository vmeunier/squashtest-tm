import {TextFieldElement} from '../../../elements/forms/TextFieldElement';
import {CreateAdministrationEntityDialog} from '../create-administration-entity-dialog';
import {SelectFieldElement} from '../../../elements/forms/select-field.element';
import {CommonSelectors} from '../../../elements/forms/abstract-form-field.element';

export class CreateScmServerDialog extends CreateAdministrationEntityDialog {


  private readonly nameField: TextFieldElement;
  private readonly kindField: SelectFieldElement;
  private readonly urlField: TextFieldElement;

  constructor() {
    super('new-scm-server', 'scm-servers/new', 'scm-servers');

    this.nameField = new TextFieldElement('name');
    this.kindField = new SelectFieldElement(CommonSelectors.fieldName('kind'));
    this.urlField = new TextFieldElement('url');
  }

  fillName(name: string) {
    this.nameField.fill(name);
  }

  fillKindField(group: string) {
    this.kindField.setValue(group);
  }

  fillUrl(url: string) {
    this.urlField.fill(url);
  }

  openCreateScmServer(name: string, kind: string, url: string) {
    this.fillName(name);
    this.fillKindField(kind);
    this.fillUrl(url);
    this.addForSuccess(false);
  }

  checkIfFormIsEmpty() {
    this.nameField.checkContent('');
    this.kindField.setValue('Github');
    this.urlField.checkContent('');
  }

}
