import {AlertDialogElement} from '../../../elements/dialog/alert-dialog.element';

export class CannotRemoveBugtrackerAlert extends AlertDialogElement {
  constructor() {
    super('alert');
  }

  assertMessage() {
    this.assertHasMessage('Le serveur ne peut pas être supprimé car il est associé à au moins une ' +
      'synchronisation. Veuillez supprimer toutes les synchronisations avant de supprimer le serveur.');
  }
}
