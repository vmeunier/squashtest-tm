import {AlertDialogElement} from '../../../elements/dialog/alert-dialog.element';

export class RemoveDefaultRequirementsLinksDialogElement extends AlertDialogElement {
  constructor() {
    super('alert');
  }

  assertMessageSimpleDelete() {
    this.assertHasMessage('Le type sélectionné est le type par défaut. Il ne peut donc pas être supprimé.');
  }

  assertMessageMultipleDelete() {
    this.assertHasMessage('La suppression est impossible car la sélection contient le type par défaut.');
  }
}
