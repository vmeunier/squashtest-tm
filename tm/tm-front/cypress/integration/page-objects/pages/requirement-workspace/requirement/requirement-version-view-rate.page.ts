import {Page} from '../../page';
import {selectByDataTestComponentId} from '../../../../utils/basic-selectors';
import {RequirementVersionViewPage} from './requirement-version-view.page';

export class RequirementVersionViewRatePage extends Page {

  constructor(private parentPage: RequirementVersionViewPage) {
    super('sqtm-app-requirement-version-view-rate-panel');
  }

  getTotalStatsElement(): RequirementVersionStatsElement {
    return new RequirementVersionStatsElement('total-stats');
  }

  getCurrentVersionStatsElement(): RequirementVersionStatsElement {
    return new RequirementVersionStatsElement('current-version-stats');
  }

  getChildStatsElement(): RequirementVersionStatsElement {
    return new RequirementVersionStatsElement('child-stats');
  }
}

export class RequirementVersionStatsElement {

  constructor(private componentId: string) {
  }

  getRedactionRateElement() {
    return new RequirementRateElement(this.componentId, 'redaction-rate');
  }

  getValidationRateElement() {
    return new RequirementRateElement(this.componentId, 'validation-rate');
  }

  getVerificationRateElement() {
    return new RequirementRateElement(this.componentId, 'verification-rate');
  }

  assertExist() {
    cy.get(this.buildSelector()).should('exist');
  }

  buildSelector() {
    return `${selectByDataTestComponentId(this.componentId)}`;
  }
}


export class RequirementRateElement {
  constructor(private requirementStatsElementId: string, private componentId: string) {
  }

  assertElementHaveTitle(title: string) {
    cy.get(`${this.buildSelector()} ${selectByDataTestComponentId('title')}`).should('contain.text', title);
  }

  assertPercentageValue(percentage: string) {
    cy.get(`${this.buildSelector()} ${selectByDataTestComponentId('percentage')}`).find('span').should('contain.text', `${percentage}`);
  }

  assertRateValue(rate: string) {
    cy.get(`${this.buildSelector()} ${selectByDataTestComponentId('rate')}`).should('contain.text', rate);
  }

  assertExist() {
    cy.get(this.buildSelector()).should('exist');
  }

  assertHaveValidColor() {
    cy.get(`${this.buildSelector()} ${selectByDataTestComponentId('percentage')}`).should('have.class', 'percent-valid');
  }

  assertHaveWarningColor() {
    cy.get(`${this.buildSelector()} ${selectByDataTestComponentId('percentage')}`).should('have.class', 'percent-warning');
  }

  assertHaveDangerColor() {
    cy.get(`${this.buildSelector()} ${selectByDataTestComponentId('percentage')}`).should('have.class', 'percent-danger');
  }

  assertHaveUndefinedColor() {
    cy.get(`${this.buildSelector()} ${selectByDataTestComponentId('percentage')}`).should('have.class', 'percent-undefined');
  }

  buildSelector() {
    return `${selectByDataTestComponentId(this.requirementStatsElementId)} ${selectByDataTestComponentId(this.componentId)}`;
  }
}
