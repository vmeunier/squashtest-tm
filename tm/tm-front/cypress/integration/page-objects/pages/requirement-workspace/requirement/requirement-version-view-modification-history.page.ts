import {Page} from '../../page';
import {GridElement} from '../../../elements/grid/grid.element';
import {GridResponse} from '../../../../model/grids/data-row.type';
import {LinkRendererSelectorBuilder} from '../../../../utils/grid-selectors.builder';
import {ShowDescriptionChangeDialogElement} from '../dialogs/show-description-change-dialog.element';

export class RequirementVersionViewModificationHistoryPage extends Page {

  constructor(public readonly grid: GridElement) {
    super('sqtm-app-requirement-version-modification-history-panel');
  }

  get modificationHistoryTable() {
    return new GridElement('req-version-modification-history');
  }

  static navigateTo(requirementVersionId: number | string, data: GridResponse): RequirementVersionViewModificationHistoryPage {
    const url = `requirement-version/${requirementVersionId}/history`;
    const gridElement = GridElement.createGridElement('req-version-modification-history', url, data);
    return new RequirementVersionViewModificationHistoryPage(gridElement);
  }

  showDescriptionChangeDialog(linkCellRenderer: LinkRendererSelectorBuilder) {
    linkCellRenderer.findCellLink().click();
    return new ShowDescriptionChangeDialogElement('alert-requirement-version-description-changes');
  }
}
