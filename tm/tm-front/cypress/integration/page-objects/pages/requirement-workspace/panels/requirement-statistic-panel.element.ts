import {ChartElement} from '../../../elements/charts/chart.element';
import {selectByDataTestComponentId} from '../../../../utils/basic-selectors';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {RequirementStatistics} from '../../../../model/requirements/requirement-statistics.model';

export class RequirementStatisticPanelElement {

  private statPanelTag = `sqtm-app-requirement-statistics-panel`;

  private customDashboardTag = `sqtm-app-custom-dashboard`;

  public orphanRequirementChart = new ChartElement('coverage-requirement');
  public statusChart = new ChartElement('status-requirement');
  public criticalityChart = new ChartElement('criticality-requirement');
  public descriptionChart = new ChartElement('description-requirement');
  public coverageByCriticalityChart = new ChartElement('coverage-by-criticality');
  public validationByCriticalityChart = new ChartElement('validation-by-criticality');

  constructor() {
  }

  assertTitleExist(expectedTitle: string) {
    cy.get('.ant-collapse-header').should('contain.text', expectedTitle);
  }

  refreshStatistics(stats?: RequirementStatistics) {
    const selector = `.ant-collapse-header ${selectByDataTestComponentId('refresh-button')}`;
    const mock = new HttpMockBuilder('requirement/statistics').post().responseBody(stats).build();
    cy.get(selector).click();
    mock.wait();
  }

  assertFooterContains(expected: string) {
    const selector = `${this.statPanelTag} ${selectByDataTestComponentId('footer')}`;
    cy.get(selector).should('contain.text', expected);
  }

  assertCustomDashboardExist() {
    cy.get(this.customDashboardTag).should('exist');
  }
}
