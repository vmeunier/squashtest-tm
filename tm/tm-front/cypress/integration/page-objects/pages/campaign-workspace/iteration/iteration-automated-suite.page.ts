import {Page} from '../../page';
import {GridElement} from '../../../elements/grid/grid.element';
import {GridResponse} from '../../../../model/grids/data-row.type';


export class IterationAutomatedSuitePage extends Page {

  private constructor(rootSelector: string, public readonly testPlan: GridElement, private iterationId: number | string) {
    super(rootSelector);
  }

  static navigateTo(iterationId: number | string, automatedSuites: GridResponse): IterationAutomatedSuitePage {
    const url = `iteration/${iterationId}/automated-suite`;
    const gridElement = GridElement.createGridElement('automated-suite-grid', url, automatedSuites);
    return new IterationAutomatedSuitePage('sqtm-app-iteration-automated-suite', gridElement, iterationId);
  }

}

