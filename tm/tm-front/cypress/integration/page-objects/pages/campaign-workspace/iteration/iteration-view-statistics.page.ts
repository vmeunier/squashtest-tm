import {Page} from '../../page';
import {IterationViewPage} from './iteration-view.page';


export class IterationViewStatisticsPage extends Page {
  constructor(private parentPage: IterationViewPage) {
    super('sqtm-app-campaign-statistics-panel');
  }

  get iterationId() {
    return this.parentPage.iterationId;
  }
}
