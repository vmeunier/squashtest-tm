import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {GridResponse} from '../../../../model/grids/data-row.type';
import {GridElement} from '../../../elements/grid/grid.element';
import {ItpiSearchModel} from './itpi-search-model';

export function buildItpiSearchPageMock
(itpiSearchModel: ItpiSearchModel = {usersExecutedItpi: [], usersAssignedTo: []}) {
  return new HttpMockBuilder<ItpiSearchModel>('search/campaign').responseBody(itpiSearchModel).build();
}

export function buildItpiSearchGrid(initialRows: GridResponse) {
  return GridElement.createGridElement('itpi-search', 'search/campaign', initialRows);
}
