import {UserView} from '../../../../model/user/user-view';

export class ItpiSearchModel {
  usersAssignedTo: UserView[];
  usersExecutedItpi: UserView[];
}

