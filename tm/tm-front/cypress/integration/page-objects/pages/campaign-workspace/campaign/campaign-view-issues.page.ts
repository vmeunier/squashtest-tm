import {GridElement} from '../../../elements/grid/grid.element';
import {IssuePage} from '../../../elements/issues/issue.page';

export class CampaignViewIssuesPage extends IssuePage {
  constructor() {
    super('sqtm-app-campaign-issues');
  }

  get issueGrid() {
    return new GridElement('campaign-view-issue');
  }

}
