import {ChartElement} from '../../../elements/charts/chart.element';
import {CampaignStatisticsBundle} from '../../../../model/campaign/campaign-model';
import {selectByDataTestComponentId} from '../../../../utils/basic-selectors';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';

export class CampaignStatisticsPanelElement {

  private statPanelTag = 'sqtm-app-campaign-statistics-charts-panel';

  public statusChart = new ChartElement('status-chart');
  public conclusivenessChart = new ChartElement('conclusiveness-chart');
  public importanceChart = new ChartElement('importance-chart');

  constructor() {}

  assertTitleExist(expectedTitle: string) {
    cy.get('.ant-collapse-header').should('contain.text', expectedTitle);
  }

  refreshStatistics(stats?: CampaignStatisticsBundle) {
    const selector = `.ant-collapse-header ${selectByDataTestComponentId('refresh-button')}`;
    const mock = new HttpMockBuilder('campaign/statistics').post().responseBody(stats).build();
    cy.get(selector).click();
    mock.wait();
  }
}
