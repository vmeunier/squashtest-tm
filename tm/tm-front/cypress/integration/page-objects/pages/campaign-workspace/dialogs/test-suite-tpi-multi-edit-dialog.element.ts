import {SelectFieldElement} from '../../../elements/forms/select-field.element';
import {DataRow} from '../../../../model/grids/data-row.type';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {CommonSelectors} from '../../../elements/forms/abstract-form-field.element';


export class TestSuiteTpiMultiEditDialogElement {
  public readonly dialogId = 'itpi-multi-edit';
  public readonly assigneeSelect: SelectFieldElement;
  public readonly statusSelect: SelectFieldElement;

  constructor() {
    this.assigneeSelect = new SelectFieldElement(CommonSelectors.fieldName('assignee'));
    this.statusSelect = new SelectFieldElement(CommonSelectors.fieldName('status'));
  }

  assertNotExist() {
    cy.get(this.buildSelector()).should('not.exist');
  }

  assertExist() {
    cy.get(this.buildSelector()).should('exist');
  }

  toggleAssigneeEdition(): void {
    this.clickLabel('Assignation');
  }

  selectAssignee(assignee: string): void {
    this.assigneeSelect.selectValue(assignee);
  }

  toggleStatusEdition(): void {
    this.clickLabel('Statut');
  }

  selectStatus(status: string): void {
    this.statusSelect.selectValue(status);
  }

  confirm(gridRefreshResponse: DataRow[]): void {
    const updateMock = new HttpMockBuilder('test-suite/test-plan/*/mass-update').post().build();
    const testPlanMock = new HttpMockBuilder('test-suite/*/test-plan')
      .post().responseBody({dataRows: gridRefreshResponse})
      .build();

    const refreshTreeMock = new HttpMockBuilder('campaign-tree/refresh')
      .post().responseBody({dataRows: []})
      .build();

    this.clickOnConfirmButton();

    updateMock.wait();
    testPlanMock.wait();
    refreshTreeMock.wait();
  }

  private clickLabel(labelContent: string): void {
    cy.get(this.buildSelector())
      .contains('span', labelContent)
      .click();
  }

  private clickOnConfirmButton() {
    const buttonSelector = this.buildButtonSelector('confirm');
    cy.get(buttonSelector).should('exist');
    cy.get(buttonSelector).click();
    cy.get(this.buildSelector()).should('not.exist');
  }

  private buildSelector(): string {
    return `[data-test-dialog-id=${this.dialogId}]`;
  }

  private buildButtonSelector(buttonId: string) {
    return `${this.buildSelector()} [data-test-dialog-button-id=${buttonId}]`;
  }
}
