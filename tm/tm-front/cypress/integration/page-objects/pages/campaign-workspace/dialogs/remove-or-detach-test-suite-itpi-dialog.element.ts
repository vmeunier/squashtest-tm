import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {GridResponse} from '../../../../model/grids/data-row.type';


export class RemoveOrDetachTestSuiteItpiDialogElement {
  testSuiteId: number;
  itemTestPlanIds: number[];
  refreshedTestPlan: GridResponse;
  public readonly dialogId = 'delete-test-suite-tpi';

  constructor(testSuiteId: number, itemTestPlanIds: number[], refreshedTestPlan: GridResponse) {
    this.testSuiteId = testSuiteId;
    this.itemTestPlanIds = itemTestPlanIds;
    this.refreshedTestPlan = refreshedTestPlan;
  }

  detachFromTestSuite() {
    const detachMock = new HttpMockBuilder('test-suite/*/test-plan-items/detach/*').delete().build();
    const refreshGridMock = new HttpMockBuilder('test-suite/*/test-plan').post().responseBody(this.refreshedTestPlan).build();

    cy.get('[data-test-dialog-button-id="detach"]').click();
    detachMock.wait();
    refreshGridMock.waitResponseBody();
  }

  removeFromTestSuiteAndIteration() {
    const removeMock = new HttpMockBuilder('test-suite/*/test-plan-items/remove/*').delete().build();
    const refreshGridMock = new HttpMockBuilder('test-suite/*/test-plan').post().responseBody(this.refreshedTestPlan).build();

    cy.get('[data-test-dialog-button-id="remove"]').click();
    removeMock.wait();
    refreshGridMock.waitResponseBody();
  }

  assertItemHasExecutions() {
    const message = this.itemTestPlanIds.length > 1 ?
      'Ces cas de test ne seront plus associés au plan ' +
      'd\'exécution de la suite. Vous pouvez également les supprimer de l\'itération. Dans ce dernier cas, les ' +
      'exécutions seront également supprimées. Confirmez-vous ces actions ?'
      :
      'Ce cas de test ne sera plus associé au plan ' +
      'd\'exécution de la suite. Vous pouvez également le supprimer de l\'itération. Dans ce dernier cas, les ' +
      'exécutions seront également supprimées. Confirmez-vous ces actions ?';

    cy.get('[data-test-dialog-message="true"]').should('contain.text', message);
  }

  assertItemHasNoExecutions() {
    const message = this.itemTestPlanIds.length > 1 ?
      'Ces cas de test ne seront plus associés au plan d\'exécution de la suite. Vous pouvez également les supprimer ' +
      'de l\'itération.  Confirmez-vous la suppression de l\'association ?'
      :
      'Ce cas de test ne sera plus associé au plan d\'exécution de la suite. Vous pouvez également le supprimer de ' +
      'l\'itération.  Confirmez-vous la suppression de l\'association ?';

      cy.get('[data-test-dialog-message="true"]').should('contain.text', message);
  }

  assertNotExist() {
    cy.get(this.buildSelector()).should('not.exist');
  }

  assertExist() {
    cy.get(this.buildSelector()).should('exist');
  }

  private buildSelector(): string {
    return `[data-test-dialog-id=${this.dialogId}]`;
  }
}
