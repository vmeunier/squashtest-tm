import {DeleteConfirmDialogElement} from '../../../elements/dialog/delete-confirm-dialog.element';
import {GridResponse} from '../../../../model/grids/data-row.type';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';

export class MassDeleteCtpiDialogElement extends DeleteConfirmDialogElement {
  campaignId: number;
  ctpiIds: number[];

  constructor(campaignId: number, ctpiIds: number[]) {
    super('confirm-delete');
    this.campaignId = campaignId;
    this.ctpiIds = ctpiIds;
  }

  deleteForFailure(response: any) {
  }

  deleteForSuccess(response?: GridResponse) {
    const removeMock = new HttpMockBuilder<any>(`campaign/${this.campaignId}/test-plan/${[this.ctpiIds]}`)
      .delete().responseBody('').build();
    const mock = new HttpMockBuilder<GridResponse>(`campaign/${this.campaignId}/test-plan`).post().responseBody(response).build();
    this.clickOnConfirmButton();
    removeMock.wait();
    return mock.waitResponseBody();
  }

}
