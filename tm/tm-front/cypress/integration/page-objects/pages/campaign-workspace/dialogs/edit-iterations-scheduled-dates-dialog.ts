import {DatePickerElement} from '../../../elements/forms/date-picker.element';
import {IterationPlanning} from '../../../../model/campaign/campaign-model';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {CommonSelectors} from '../../../elements/forms/abstract-form-field.element';

export class EditIterationsScheduledDatesDialog {

  private readonly rootSelector = 'sqtm-app-iteration-planning-dialog';

  constructor(private campaignId: string | number = '*') {
  }

  assertIsOpened() {
    cy.get(this.rootSelector)
      .should('be.visible');
  }

  setScheduledStartDateToNow(iterationId: number) {
    const datePickerElement = new DatePickerElement(CommonSelectors.componentId(`iteration-${iterationId}-scheduled-start-date`));
    datePickerElement.selectTodayDate();
  }

  confirm(serverResponse: IterationPlanning[] = []) {
    const httpMock = new HttpMockBuilder(`campaign/${this.campaignId}/iterations/planning`)
      .post()
      .responseBody(serverResponse)
      .build();
    this.clickConfirmButton();
    httpMock.wait();
  }

  clickConfirmButton(): void {
    cy.get('[data-test-dialog-button-id="confirm"]').click();
  }
}
