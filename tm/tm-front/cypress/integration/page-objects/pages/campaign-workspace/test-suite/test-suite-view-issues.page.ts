import {GridElement} from '../../../elements/grid/grid.element';
import {IssuePage} from '../../../elements/issues/issue.page';

export class TestSuiteViewIssuesPage extends IssuePage {
  constructor() {
    super('sqtm-app-test-suite-issues');
  }

  get issueGrid() {
    return new GridElement('test-suite-view-issue');
  }
}
