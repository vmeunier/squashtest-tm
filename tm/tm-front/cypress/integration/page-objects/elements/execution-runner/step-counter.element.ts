import Chainable = Cypress.Chainable;
import {selectByDataTestComponentId} from '../../../utils/basic-selectors';
import {CompactEditableNumericFieldElement} from '../forms/compact-editable-numeric-field.element';

export class StepCounterElement {

  private readonly componentTag = 'sqtm-core-step-navigator';
  private editableStepField: CompactEditableNumericFieldElement;

  constructor() {
    this.editableStepField = new CompactEditableNumericFieldElement('step-index');
  }

  checkExecutionStepper(currentStep: number, maxStep: number) {
    this.getStepCount().then(counter => {
      let text = counter.text();
      text = text.replace(' ', '');
      cy.wrap(text).should('equal', `${currentStep} /${maxStep}`);
    });
  }

  navigateForward() {
    this.getForwardButton().click();
  }

  navigateBackward() {
    this.getBackwardButton().click();
  }

  navigateToArbitraryStep(stepIndex: number) {
    this.editableStepField.setValue(stepIndex);
    this.editableStepField.confirm();
  }

  private getStepCount(): Chainable<any> {
    return this.getComponent().find(selectByDataTestComponentId('step-counter'));
  }

  private getComponent(): Chainable<any> {
    return cy.get(this.componentTag);
  }

  assertForwardButtonIsActive() {
    this.getForwardButton()
      .should('exist')
      .should('not.be.disabled');
  }

  assertForwardButtonIsDisabled() {
    this.getForwardButton()
      .should('exist')
      .should('be.disabled');
  }

  private getForwardButton(): Chainable<any> {
    return cy.get('[data-test-component-id="step-forward"]');
  }

  assertBackwardButtonIsActive() {
    this.getBackwardButton()
      .should('exist')
      .should('not.be.disabled');
  }

  assertBackwardButtonIsDisabled() {
    this.getBackwardButton()
      .should('exist')
      .should('be.disabled');
  }

  private getBackwardButton(): Chainable<any> {
    return cy.get('[data-test-component-id="step-backward"]');
  }

}
