import {selectByDataTestComponentId} from '../../../utils/basic-selectors';
import Chainable = Cypress.Chainable;

export class NgZorroTable {

  private readonly tableSelector: TableSelector;

  constructor(table: string | TableSelector) {
    if (typeof table === 'string') {
      this.tableSelector = buildSelectTableById(table);
    } else {
      this.tableSelector = table;
    }
  }

  assertExist() {
    this.getTable()
      .should('exist')
      .find('.ant-table')
      .should('exist');
  }

  getTable() {
    return this.tableSelector();
  }

  static getTableWrapper(id: string): Chainable<JQuery<any>> {
    return cy.get(`nz-table${selectByDataTestComponentId(id)}`);
  }

  findRow(index: number): NgZorroTableRow {
    return new NgZorroTableRow(buildSelectRowByIndex(this.tableSelector, index));
  }

}

type TableSelector = () => Chainable<JQuery<any>>;

export function buildSelectTableById(tableId: string): TableSelector {
  return () => NgZorroTable.getTableWrapper(tableId);
}

export class NgZorroTableRow {
  constructor(private readonly rowSelector: RowSelector) {
  }

  assertExist(): NgZorroTableRow {
    this.getRow().should('exist');
    return this;
  }

  getCell(cellId: number | string): NgZorroTableCell {
    return new NgZorroTableCell(buildSelectCell(this.rowSelector, cellId));
  }

  private getRow() {
    return this.rowSelector();
  }
}

type RowSelector = () => Chainable<JQuery<HTMLTableRowElement>>;

export function buildSelectRowByIndex(tableSelector: TableSelector, rowIndex: number): RowSelector {
  return () => tableSelector()
    .find('tbody')
    .find('tr')
    .eq(rowIndex);
}

export class NgZorroTableCell {
  constructor(private readonly cellSelector: CellSelector) {
  }

  assertExist(): NgZorroTableCell {
    this.getCell().should('exist');
    return this;
  }

  assertContainsText(expectedText: string): NgZorroTableCell {
    this.getCell().should('contain.text', expectedText);
    return this;
  }

  private getCell(): Chainable<JQuery<HTMLTableCellElement>> {
    return this.cellSelector();
  }
}

type CellSelector = () => Chainable<JQuery<HTMLTableCellElement>>;

export function buildSelectCell(rowSelector: RowSelector, cellId: number | string): CellSelector {
  if (typeof cellId === 'number') {
    return buildSelectCellByIndex(rowSelector, cellId);
  } else {
    return buildSelectCellByComponentId(rowSelector, cellId);
  }
}

export function buildSelectCellByIndex(rowSelector: RowSelector, cellIndex: number): CellSelector {
  return () => rowSelector().find('td').eq(cellIndex);
}

export function buildSelectCellByComponentId(rowSelector: RowSelector, componentId: string): CellSelector {
  return () => rowSelector().find<HTMLTableCellElement>(`td${selectByDataTestComponentId(componentId)}`)
    .first();
}
