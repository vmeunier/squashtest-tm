import {MenuElement} from '../../../utils/menu.element';
import {selectByDataTestToolbarButtonId} from '../../../utils/basic-selectors';

export class ToolbarElement {

  constructor(protected readonly toolbarId: string) {
  }

  shouldExist() {
    cy.get(this.buildSelector()).should('exist');
  }


  buildSelector(): string {
    return `
    [data-test-toolbar-id="${this.toolbarId}"]
    `;
  }

  buttonMenu(buttonId: string, menuId: string): ToolbarMenuButtonElement {
    return new ToolbarMenuButtonElement(this.buildSelector(), buttonId, menuId);
  }

  button(buttonId: string): ToolbarButtonElement {
    return new ToolbarButtonElement(this.buildSelector(), buttonId);
  }
}

export class ToolbarMenuButtonElement {
  constructor(private readonly rootSelector: string, private readonly buttonId: string, private readonly menuId: string) {
  }

  buildSelector(): string {
    return `
    ${this.rootSelector}
    ${selectByDataTestToolbarButtonId(this.buttonId)}
    `;
  }

  assertExist() {
    cy.get(this.buildSelector()).should('exist');
  }

  assertIsDisabled() {
    cy.get(this.buildSelector()).should('have.class', 'label-color');
  }

  showMenu(force = false): MenuElement {
    cy.get(this.buildSelector()).trigger('mouseenter', {force});
    const menuElement = new MenuElement(this.menuId);
    menuElement.assertExist();
    return menuElement;
  }

  hideMenu() {
    const menuElement = new MenuElement(this.menuId);
    menuElement.hide();
  }

  assertNotExist() {
    cy.get(this.buildSelector()).should('not.exist');
  }
}

export class ToolbarButtonElement {
  constructor(private readonly rootSelector: string, private readonly buttonId: string) {
  }

  buildSelector(): string {
    return `
    ${this.rootSelector}
    ${selectByDataTestToolbarButtonId(this.buttonId)}
    `;
  }

  assertExist() {
    cy.get(this.buildSelector()).should('exist');
  }

  click() {
    cy.get(this.buildSelector())
      .find('span')
      .click();
  }

  clickWithoutSpan() {
    cy.get(this.buildSelector())
      .click();
  }

  assertIsActive() {
    cy.get(this.buildSelector())
      .find('span')
      .should('exist')
      .should('not.have.class', 'label-color');
  }

  assertIsDisabled() {
    cy.get(this.buildSelector())
      .find('span')
      .should('exist')
      .should('have.class', 'label-color');
  }

}
