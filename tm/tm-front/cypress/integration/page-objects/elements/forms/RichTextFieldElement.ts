import Chainable = Cypress.Chainable;

export class RichTextFieldElement {

  constructor(private readonly fieldName: string) {
  }

  get selector(): string {
    return `
    sqtm-core-rich-text-field
    [data-test-field-name="${this.fieldName}"]
    `;
  }

  assertIsReady() {
    this.extractCKEditorId().then((id) => {
      cy.window()
        .its('CKEDITOR')
        .then(CKEDITOR => {
          return cy.wrap(CKEDITOR.instances[id]);
        });
    })
      .its('status')
      .should('equal', 'ready');
  }

  fillWithString(value: string) {
    this.fillWithHtml(`<p>${value}</p>`);
  }

  fillWithHtml(htmlValue: string) {
    this.assertIsReady();
    this.extractCKEditorId()
      .then(id => {
        cy.window()
          .its('CKEDITOR')
          .then(CKEDITOR => {
            CKEDITOR.instances[id].setData(htmlValue);
          });
      });
  }

  private extractCKEditorId(): Chainable<string> {
    return cy.get(`sqtm-core-rich-text-field
    [data-test-field-name="${this.fieldName}"]`)
      .find('div.cke')
      .should('be.visible')
      .then(element => {
        const id = element.attr('id').split('_')[1];
        return cy.wrap(id);
      });
  }

  fill(value: string) {
    this.fillWithString(value);
  }

  clear(): void {
    this.fill('');
  }

  checkContent(expectedContent: string) {
    cy.get(this.selector).should('have.value', expectedContent);
  }

  checkHtmlTextContent(expectedContent: string) {
    cy.get(this.selector)
      .should('exist')
      .find('.cke_wysiwyg_div')
      .should('have.html', expectedContent);
  }
}
