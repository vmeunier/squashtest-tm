import Chainable = Cypress.Chainable;
import {HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {AbstractFormFieldElement, ElementSelectorFactory} from './abstract-form-field.element';

export class EditableRichTextFieldElement extends AbstractFormFieldElement {

  constructor(selectorOrFieldId: ElementSelectorFactory | string,
              private readonly url?: string) {
    super(selectorOrFieldId);
  }

  // A selector for the embedded ckeditor. It won't work if the edit mode is not enabled
  get editorSelector(): Chainable<any> {
    return this.selector.find(`ckeditor`);
  }

  // Enable the edit mode
  enableEditMode() {
    this.selector.find('>div').click();
  }

  // Add some text to the editor (when visible). By default, the content is cleared before.
  setValue(value: string) {
    if (value === '') {
      return;
    }

    this.clear().then(() => this.editorSelector.type(value));
  }

  // Replace the content of the editor (when visible) with the default one.
  clear(): Chainable<any> {
    return this.editorSelector.find(`div.cke_wysiwyg_div`)
      .type('{selectall}')
      .type('{backspace}')
      .should('have.text', '');
  }

  // Click the confirm button (when visible). The result check is done on the content of the non-editable
  confirm(expectedContent?: string): void {
    // Click on button
    if (this.url != null) {
      const mock = new HttpMockBuilder(this.url).post().build();
      this.selector.find('button[data-test-button-id="confirm"]').click();
      mock.wait();
    } else {
      this.selector.find('button[data-test-button-id="confirm"]').click();
    }

    // Optionally check displayed value
    if (expectedContent != null) {
      this.checkTextContent(expectedContent);
    }
  }

  confirmValue(response?: any) {
    if (this.url != null) {
      const mock = new HttpMockBuilder(this.url).post().responseBody(response).build();
      this.clickButton('confirm');
      mock.wait();
    } else {
      this.clickButton('confirm');
    }
  }

  confirmOnError(error?: any) {
    if (this.url != null) {
      const mock = new HttpMockBuilder(this.url).post().status(412).responseBody(error).build();
      this.selector.find('button[data-test-button-id="confirm"]').click();
      mock.wait();
    } else {
      this.selector.find('button[data-test-button-id="confirm"]').click();
    }
  }

  cancel(): void {
    this.selector.find('button[data-test-button-id="cancel"]').click();
    this.selector.find('button[data-test-button-id="cancel"]').should('not.exist');
  }

  checkDoesNotContainText(value: string) {
    return this.doCheckContent(value, false, false);
  }

  checkTextContent(value: string) {
    this.doCheckContent(value, true, false);
  }

  checkHtmlContent(value: string) {
    this.doCheckContent(value, true, true);
  }

  private doCheckContent(value: string, checkIfPresent: boolean, asHtml: boolean) {
    const baseChainer = asHtml ? 'contain.html' : 'contain.text';
    if (checkIfPresent) {
      this.selector.find('>div>div').should(baseChainer, value);
    } else {
      this.selector.find('>div>div').should('not.' + baseChainer, value);
    }
  }

  setAndConfirmValue(newValue: string) {
    this.enableEditMode();
    this.setValue(newValue);
    this.confirm(newValue);
  }

  clickButton(buttonId: string) {
    this.selector.find(`[data-test-button-id="${buttonId}"]`).click();
  }

  assertIsEditable() {
    this.getDiwWrapper().should('have.class', 'editable');
  }

  assertIsNotEditable() {
    this.getDiwWrapper().should('not.have.class', 'editable');
  }

  private getDiwWrapper() {
    return this.selector.find('>div');
  }
}
