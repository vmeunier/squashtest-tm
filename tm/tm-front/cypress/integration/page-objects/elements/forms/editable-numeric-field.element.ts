import {HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {AbstractFormFieldElement, ElementSelectorFactory} from './abstract-form-field.element';

export class EditableNumericFieldElement extends AbstractFormFieldElement {

  constructor(selectorOrFieldId: ElementSelectorFactory | string, private readonly url?: string) {
    super(selectorOrFieldId);
  }

  setAndConfirmValue(newValue: number) {
    this.setValue(newValue);
    this.confirm(newValue);
  }

  setValue(newValue: number) {
    this.selector.find('span').click();
    this.selector.find('input').clear();
    this.selector.find('input').type(newValue.toString());
  }

  confirm(expectedContent?: number): void {
    // Click on button
    if (this.url != null) {
      const mock = new HttpMockBuilder(this.url).post().build();
      this.selector.find('button').first().click();
      mock.wait();
    } else {
      this.selector.find('button').first().click();
    }

    if (expectedContent != null) {
      this.checkContent(expectedContent);
    }
    this.checkEditMode(false);
  }

  cancel(): void {
    this.selector.find('button').eq(1).click();
    this.checkEditMode(false);
  }

  checkContent(value: number | string) {
    this.selector.find('span').should('contain.text', value);
  }

  checkEditMode(shouldBeInEditMode: boolean) {
    const chainer = shouldBeInEditMode ? 'exist' : 'not.exist';
    this.selector.find('button').should(chainer);
  }

  checkPlaceholder() {
    this.checkContent('(Cliquer pour éditer...)');
  }

  assertIsEditable() {
    this.findDivWrapper()
      .should('have.class', 'editable');
  }

  assertIsNotEditable() {
    this.findDivWrapper()
      .should('not.have.class', 'editable');
  }

  private findDivWrapper() {
    return this.selector
      .find('div')
      .should('exist');
  }
}
