import {AbstractFormFieldElement, ElementSelectorFactory} from './abstract-form-field.element';

export class GenericTextFieldElement extends AbstractFormFieldElement {

  constructor(selectorOrFieldId: ElementSelectorFactory | string) {
    super(selectorOrFieldId);
  }

  checkContent(expectedContent: string) {
    this.selector.should('contain.text', expectedContent);
  }
}
