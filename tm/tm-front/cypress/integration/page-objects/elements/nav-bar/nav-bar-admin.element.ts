import {selectByDataTestComponentId} from '../../../utils/basic-selectors';

export class NavBarAdminElement {


  constructor() {
  }

  private readonly rootSelector = 'sqtm-core-nav-bar-admin';

  public toggle() {
    cy.get(this.rootSelector).find(selectByDataTestComponentId('sqtm-main-nav-bar-toggle-button')).click();
  }

  assertExist() {
    cy.get(this.rootSelector).should('have.length', 1);
  }

}
