import {GridResponse} from '../../../model/grids/data-row.type';
import {HttpMock, HttpMockBuilder} from '../../../utils/mocks/request-mock';

export class DateFilterWidgetElement {
  private selectBaseComponent() {
    return 'sqtm-core-date-filter';
  }

  private selectByComponentId(id: string) {
    return `
    sqtm-core-date-filter
    [data-test-component-id="${id}"]
    `;
  }

  private selectOperationSelector() {
    return `
    sqtm-core-date-filter
    sqtm-core-operation-selector
    `;
  }

  constructor(public url: string) {
  }

  assertExist() {
    cy.get(this.selectBaseComponent()).should('exist');
    cy.get(this.selectByComponentId('update-button')).should('contain.text', 'Mettre à jour');
    cy.get(this.selectByComponentId('cancel-button')).should('exist');
    cy.get(this.selectByComponentId('cancel-button')).should('contain.text', 'Annuler');
  }

  assertStartComponentExist() {
    cy.get(this.selectByComponentId('input-start')).should('exist');
  }

  assertRangeComponentExist() {
    cy.get(this.selectByComponentId('input-range')).should('exist');
  }

  assertOperationChosen(operation: string) {
    cy.get(this.selectOperationSelector()).should('contain.text', operation);
  }

  fillTodayDate() {
    const datePicker = cy.get(this.selectByComponentId('input-start'));
    datePicker.click();
    cy.get('.ant-picker-today-btn').click();
  }

  fillRange(input: string) {
    cy.get(this.selectByComponentId('input-range')).type(input);
  }

  assertNotExist() {
    cy.get(this.selectBaseComponent()).should('not.exist');
  }

  update(response: GridResponse = {dataRows: []}) {
    let mock: HttpMock<GridResponse>;
    if (this.url) {
      mock = new HttpMockBuilder<GridResponse>(this.url).post().responseBody(response).build();
    }
    cy.get(this.selectByComponentId('update-button')).click();
  }

  cancel() {
    cy.get(this.selectByComponentId('cancel-button')).click();
  }

  changeOperation(label: string) {
    const selector = `
    ${this.selectOperationSelector()}
    nz-select
    `;

    // open the select menu
    cy.get(selector).click();

    // click on item
    cy.get('nz-option-item')
      .contains(label)
      .click();
  }
}
