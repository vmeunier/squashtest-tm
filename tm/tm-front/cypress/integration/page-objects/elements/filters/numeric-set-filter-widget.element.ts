import {GridResponse} from '../../../model/grids/data-row.type';
import {HttpMock, HttpMockBuilder} from '../../../utils/mocks/request-mock';

export class NumericSetFilterWidgetElement {

  private selectBaseComponent() {
    return `
    sqtm-core-numeric-set-filter
    `;
  }

  private selectByComponentId(id: string) {
    return `
    sqtm-core-numeric-set-filter
    [data-test-component-id="${id}"]
    `;
  }

  constructor(public url: string) {
  }

  assertExist() {
    this.assertExistWithOutOperationSelector();
  }

  assertExistWithOutOperationSelector() {
    cy.get(this.selectBaseComponent()).should('exist');
    cy.get(this.selectByComponentId('input')).should('exist');
    cy.get(this.selectByComponentId('update-button')).should('contain.text', 'Mettre à jour');
    cy.get(this.selectByComponentId('cancel-button')).should('exist');
    cy.get(this.selectByComponentId('cancel-button')).should('contain.text', 'Annuler');
  }

  fillInput(input: string) {
    cy.get(this.selectByComponentId('input')).clear().type(input);
  }

  assertNotExist() {
    cy.get(this.selectBaseComponent()).should('not.exist');
  }

  update(response: GridResponse = {dataRows: []}) {
    let mock: HttpMock<GridResponse>;
    if (this.url) {
      mock = new HttpMockBuilder<GridResponse>(this.url).post().responseBody(response).build();
    }
    cy.get(this.selectByComponentId('update-button')).click();
  }

  cancel() {
    cy.get(this.selectByComponentId('cancel-button')).click();
  }
}
