import {DataRow, GridResponse} from '../../../../model/grids/data-row.type';
import {RemoveTestAutomationServerDialogElement} from '../../../../page-objects/pages/administration-workspace/dialogs/remove-test-automation-server-dialog.element';
import {AdminWorkspaceAutomationServersPage} from '../../../../page-objects/pages/administration-workspace/admin-workspace-automation-servers.page';
import {mockFieldValidationError} from '../../../../data-mock/http-errors.data-mock';
import {TestAutomationServerViewPage} from '../../../../page-objects/pages/administration-workspace/test-automation-server-view/test-automation-server-view.page';
import {AdminTestAutomationServer, TestAutomationServerKind} from '../../../../model/test-automation/test-automation-server.model';
import {makeTAServerViewData} from '../../../../data-mock/administration-views.data-mock';
import {AdminReferentialDataMockBuilder} from '../../../../utils/referential/admin-referential-data-builder';

describe('Administration workspace - Test Automation Servers', function () {
  function initialisePage(): AdminWorkspaceAutomationServersPage {
    const refData = new AdminReferentialDataMockBuilder()
      .withAvailableTestAutomationServerKinds([TestAutomationServerKind.jenkins, TestAutomationServerKind.squashAutom])
      .build();
    return AdminWorkspaceAutomationServersPage.initTestAtPageTestAutomationServers(initialNodes, refData);
  }


  it('should display  the grid of test automation servers', () => {
    const page = initialisePage();
    const grid = page.grid;

    grid.assertRowExist(1);
    const row = grid.getRow(1);
    row.cell('name').textRenderer().assertContainText('TestAutomationServer1');
    row.cell('baseUrl').linkRenderer().assertContainText('http:192.168.0.03:9090/testServer1');
  });

  it('should add test automation servers', () => {
    const page = initialisePage();

    const dialog = page.openCreateTestAutomationServer();
    dialog.assertExist();
    dialog.fillName('t');
    dialog.fillUrl('u');
    dialog.fillDescription('d');

    dialog.addWithOptions({
      addAnother: false,
      createResponse: {id: 4},
      gridResponse: addTestAutomationServerResponse
    });

    const grid = page.grid;

    grid.assertRowExist('1');
    grid.assertRowExist('2');
    grid.assertRowExist('3');
    grid.assertRowExist('4');

    // should add another test automation server
    const dialogAnother = page.openCreateTestAutomationServer();
    dialogAnother.assertExist();
    dialogAnother.fillName('t');
    dialogAnother.fillUrl('u');
    dialogAnother.fillDescription('d');

    dialogAnother.addWithOptions({
      addAnother: true,
      createResponse: {id: 4},
      gridResponse: addTestAutomationServerResponse
    });

    dialogAnother.assertExist();
    dialogAnother.checkIfFormIsEmpty();
    dialogAnother.cancel();
    dialogAnother.assertNotExist();

  });

  it('should validate creation form', () => {
    const page = initialisePage();

    const dialog = page.openCreateTestAutomationServer();
    dialog.assertExist();
    dialog.fillName('t');
    dialog.fillUrl('u');
    dialog.fillDescription('d');

    // should forbid to add a test automation server if name already exists
    const nameHttpError = mockFieldValidationError('name', 'sqtm-core.error.generic.name-already-in-use');
    dialog.addWithServerSideFailure(nameHttpError);
    dialog.assertExist();
    dialog.checkIfNameAlreadyInUseErrorMessageIsDisplayed();

    // should forbid to add a test automation server with an empty name
    dialog.fillName('');
    dialog.clickOnAddButton();
    dialog.checkIfRequiredErrorMessageIsDisplayed();

    // should forbid to add a test automation server with an empty url
    dialog.fillName('n');
    dialog.fillUrl('');
    dialog.clickOnAddButton();
    dialog.checkIfRequiredErrorMessageIsDisplayed();

    // should forbid to add a test automation server with a malformed url
    const urlHttpError = mockFieldValidationError('baseUrl', 'sqtm-core.exception.wrong-url');
    dialog.fillUrl('http');
    dialog.addWithServerSideFailure(urlHttpError);
    dialog.assertExist();
    dialog.checkIfMalformedUrlErrorMessageIsDisplayed();
  });

  it('should allow test automation removal without bound projects', () => {
    const page = initialisePage();

    // Show dialog
    page.grid.getRow(3).cell('delete').iconRenderer().click();

    // Confirm
    const dialog = new RemoveTestAutomationServerDialogElement([3]);
    dialog.deleteForSuccess({
      count: 1,
      dataRows: [initialNodes.dataRows[0]]
    });
  });

  it('should allow test automation removal with bound projects', () => {
    const page = initialisePage();

    // Show dialog
    page.grid.getRow(2).cell('delete').iconRenderer().click();

    // Confirm
    const dialog = new RemoveTestAutomationServerDialogElement([2]);
    dialog.deleteForSuccess({
      count: 1,
      dataRows: [initialNodes.dataRows[0]]
    });
  });

  it('should allow multiple test automation servers removal', () => {
    const page = initialisePage();

    // Select elements to delete
    page.grid.selectRows([1, 2, 3], '#', 'leftViewport');

    // Show dialog
    page.clickOnMultipleDeleteButton(true,  {
      count: 0,
      dataRows: [],
      idAttribute: '',
    });
  });

  const initialNodes: GridResponse = {
    count: 3,
    dataRows: [
      {
        id: '1',
        children: [],
        data: {
          serverId: 1,
          name: 'TestAutomationServer1',
          baseUrl: 'http:192.168.0.03:9090/testServer1',
          executionCount: 0,
          createdBy: '',
          createdOn: '',
          lastModifiedBy: null,
          lastModifiedOn: null,
          kind: TestAutomationServerKind.jenkins,
        }
      } as unknown as DataRow,
      {
        id: '2',
        children: [],
        data: {
          serverId: 2,
          name: 'TestAutomationServer2',
          baseUrl: 'http:192.168.0.03:9090/testServer2',
          executionCount: 2,
          createdBy: '',
          createdOn: '',
          lastModifiedBy: null,
          lastModifiedOn: null,
          kind: TestAutomationServerKind.jenkins,
        }
      } as unknown as DataRow,
      {
        id: '3',
        children: [],
        data: {
          serverId: 3,
          name: 'TestAutomationServer3',
          baseUrl: 'http:192.168.0.03:9090/testServer3',
          executionCount: 0,
          createdBy: '',
          createdOn: '',
          lastModifiedBy: null,
          lastModifiedOn: null,
          kind: TestAutomationServerKind.jenkins,
        }
      } as unknown as DataRow,
    ]
  };

  const addTestAutomationServerResponse: GridResponse = {
    count: 4,
    dataRows: [
      ...initialNodes.dataRows,
      {
        id: '4',
        children: [],
        data: {'name': 'TestAutomationServer4'}
      } as unknown as DataRow,
    ]
  };
});


describe('Administration Workspace - Test automation server - Authentication policy', function () {
  it('should send credential form', () => {
    const page = showTestAutomationServerView();

    page.authPolicyPanel.assertSendButtonDisabled();
    page.authPolicyPanel.usernameField.fill('username');
    page.authPolicyPanel.passwordField.fill('password');
    page.authPolicyPanel.assertSendButtonEnabled();

    page.authPolicyPanel.sendCredentialsForm();
    page.authPolicyPanel.assertSaveSuccessMessageVisible();
  });
});


describe('Administration Workspace - Test automation server - Authentication protocol panel', function () {
  it('should display auth protocol panel', () => {
    const page = showTestAutomationServerView();
    page.authProtocolPanel.protocolField.checkSelectedOption('basic authentication');
  });
});


describe('Administration Workspace - Test automation server - Information panel', function () {
  it('should set test automation server name', () => {
    const page = showTestAutomationServerView();
    page.entityNameField.setAndConfirmValue('NEW');
    page.entityNameField.checkContent('NEW');
  });

  it('should set test automation server url', () => {
    const page = showTestAutomationServerView();
    page.informationPanel.urlField.setAndConfirmValue('http://192.168.0.1:8080/jenkins');
    page.informationPanel.urlField.checkContent('http://192.168.0.1:8080/jenkins');
  });

  it('should set test automation server description', () => {
    const page = showTestAutomationServerView();
    page.informationPanel.descriptionField.setAndConfirmValue('A description');
    page.informationPanel.descriptionField.checkTextContent('A description');
  });

  it('should check test automation server manual slave selection', () => {
    const page = showTestAutomationServerView();
    page.informationPanel.manualSlaveSelection.toggleState();
    page.informationPanel.manualSlaveSelection.checkState(true);
  });
});

function showTestAutomationServerView(): TestAutomationServerViewPage {
  const initialNodes = getInitialNodes();
  const page = AdminWorkspaceAutomationServersPage.initTestAtPageTestAutomationServers(initialNodes);
  return page.selectAutomationServerByName([initialNodes.dataRows[0].data.name],
    initialNodes.dataRows[0].data as AdminTestAutomationServer);
}

function getInitialNodes(): GridResponse {
  const automationServer = getInitialModel();

  return {
    count: 1,
    dataRows: [
      {
        id: automationServer.id.toString(), children: [], data: automationServer, allowMoves: true, allowedChildren: [], type: 'Generic'
      } as unknown as DataRow,
    ],
  };
}

function getInitialModel(): AdminTestAutomationServer {
  return makeTAServerViewData({
    id: 1,
    name: 'auto1',
    baseUrl: 'http://test',
    createdBy: 'admin',
    createdOn: new Date(),
    description: '',
    lastModifiedBy: 'admin',
    lastModifiedOn: new Date(),
  });
}
