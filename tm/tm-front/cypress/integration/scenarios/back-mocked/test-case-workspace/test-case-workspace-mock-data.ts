import {
  ChartColumnType,
  ChartDataType,
  ChartOperation,
  ChartScopeType,
  ChartType
} from '../../../model/custom-report/chart-definition.model';
import {CustomDashboardModel} from '../../../model/custom-report/custom-dashboard.model';
import {TestCaseStatistics} from '../../../model/test-case/test-case-statistics.model';

export function getStatistics(): TestCaseStatistics {
  return {
    selectedIds: [4, 5, 7, 9, 12, 78],
    sizeStatistics: {zeroSteps: 1, between0And10Steps: 2, between11And20Steps: 2, above20Steps: 1},
    importanceStatistics: {low: 1, medium: 2, high: 1, veryHigh: 2},
    statusesStatistics: {workInProgress: 2, underReview: 1, approved: 2, toBeUpdated: 1, obsolete: 0},
    boundRequirementsStatistics: {zeroRequirements: 1, oneRequirement: 4, manyRequirements: 1}
  };
}

export function getFavoriteDashboard(): CustomDashboardModel {
  return {
    id: 2,
    projectId: 1,
    customReportLibraryNodeId: 18,
    name: 'Favorite Dashboard',
    createdBy: 'cypress',
    chartBindings: [
      {
        id: 2,
        chartDefinitionId: 2,
        dashboardId: 2,
        chartInstance: {
          id: 2,
          customReportLibraryNodeId: null,
          projectId: 1,
          name: 'New Chart',
          type: ChartType.PIE,
          measures: [{
            cufId: null,
            label: '',
            column: {
              columnType: ChartColumnType.ATTRIBUTE,
              label: 'TEST_CASE_ID',
              specializedEntityType: {entityType: 'TEST_CASE', entityRole: null},
              dataType: ChartDataType.NUMERIC
            },
            operation: ChartOperation.COUNT
          }],
          axis: [{
            cufId: null,
            label: '',
            column: {
              columnType: ChartColumnType.ATTRIBUTE,
              label: 'TEST_CASE_REFERENCE',
              specializedEntityType: {entityType: 'TEST_CASE', entityRole: null},
              dataType: ChartDataType.STRING
            },
            operation: ChartOperation.NONE
          }],
          filters: [],
          abscissa: [['']],
          series: {'': [2]},
          projectScope: [],
          scope: [],
          scopeType: ChartScopeType.DEFAULT
        },
        row: 1,
        col: 1,
        sizeX: 2,
        sizeY: 2
      }
    ],
    reportBindings: [],
  };
}
