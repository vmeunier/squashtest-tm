import {TestCaseSearchPage} from '../../../../page-objects/pages/test-case-workspace/research/test-case-search-page';
import {
  ALL_PROJECT_PERMISSIONS,
  ReferentialDataMockBuilder
} from '../../../../utils/referential/referential-data-builder';
import {
  GroupedMultiListElement,
  SearchOnTagElement
} from '../../../../page-objects/elements/filters/grouped-multi-list.element';
import {InputType} from '../../../../model/customfield/customfield.model';
import {BindableEntity} from '../../../../model/bindable-entity.model';
import {UserView} from '../../../../model/user/user-view';
import {EditableDateFieldElement} from '../../../../page-objects/elements/forms/editable-date-field.element';
import {FilterOperation, SimpleFilter} from '../../../../model/filters/filter.model';

const HISTORICAL_GROUP_LABEL = 'Historique';
const CREATED_BY_LABEL = 'Créé par';
const CREATED_ON_LABEL = 'Créé le';
const CUSTOM_FIELD_GROUP_LABEL = 'Champs personnalisés';
const CUSTOM_FIELD_TAG_OR_OPERATION = 'Contient au moins une valeur';
const CUSTOM_FIELD_TAG_AND_OPERATION = 'Contient toutes les valeurs';
const ATTACH_COUNT = 'Nombre de pièces jointes';
const STEP_COUNT = 'Nombre de pas de test';
const PARAM_COUNT = 'Nombre de paramètres';
const DATASET_COUNT = 'Nombre de jeux de données';
const CALLSTEP_COUNT = 'Nombre d\'appels de cas de test';
const MODIFIED_BY_LABEL = 'Modifié par';
const MODIFIED_ON_LABEL = 'Modifié le';
const MILESTONE_NAME = 'Nom du jalon';
const MILESTONE_STATUS = 'Statut du jalon';
const MILESTONE_END_DATE = 'Date d\'échéance';

const OPERATION_LABELS = {
  EQUALS: 'Égal',
  GREATER: 'Supérieur',
  GREATER_EQUALS: 'Supérieur ou égal',
  BETWEEN: 'Entre'
};

function referentialDataWithCustomInfoLists() {
  return new ReferentialDataMockBuilder()
    .withProjects({
        name: 'Project 1',
        permissions: ALL_PROJECT_PERMISSIONS,
      },
      {
        name: 'Project 2',
        permissions: ALL_PROJECT_PERMISSIONS,
      },
      {
        name: 'Project 3',
        permissions: ALL_PROJECT_PERMISSIONS,
      })
    .withInfoLists(
      {
        label: 'Space Shuttles',
        items: [
          {label: 'Atlantys'},
          {label: 'Endeavour'},
          {label: 'Discovery'},
          {label: 'Columbia'},
          {label: 'Challenger'},
        ],
        boundToProject: [
          {projectIndex: 0, role: 'testCaseNature'}
        ]
      },
      {
        label: 'Rockets',
        items: [
          {label: 'Saturn 5'},
          {label: 'Proton'},
          {label: 'Ariane 6'},
        ],
        boundToProject: [
          {projectIndex: 1, role: 'testCaseNature'}
        ]
      },
      {
        label: 'Satellites',
        items: [
          {label: 'Diapason'},
          {label: 'Pollux'},
          {label: 'Polaire'},
        ],
        boundToProject: [
          {projectIndex: 0, role: 'testCaseType'}
        ]
      }
    )
    .withCustomFields(
      {
        code: 'MISSION_TAG',
        inputType: InputType.TAG,
        label: 'Objectif de mission',
        name: 'Objectif de mission',
        optional: true,
        options: [
          {
            label: 'ISS',
          },
          {
            label: 'Hubble',
          },
          {
            label: 'Scientific experiences',
          }
        ],
        bindings: [{bindableEntities: [BindableEntity.TEST_CASE], projectIndexes: [0, 2]}]
      },
      {
        code: 'ENGINE_CODE',
        inputType: InputType.PLAIN_TEXT,
        label: 'Moteur du lanceur',
        name: 'Moteur principal du lanceur',
        optional: true,
        bindings: [{bindableEntities: [BindableEntity.TEST_CASE], projectIndexes: [2]}]
      },
      {
        code: 'Numéro de mission',
        inputType: InputType.NUMERIC,
        label: 'Numéro de mission',
        name: 'Numéro de mission',
        optional: true,
        bindings: [{bindableEntities: [BindableEntity.TEST_CASE], projectIndexes: [0]}]
      },
      {
        code: 'Date de mission',
        inputType: InputType.DATE_PICKER,
        label: 'Date de mission',
        name: 'Date de mission',
        optional: true,
        bindings: [{bindableEntities: [BindableEntity.TEST_CASE], projectIndexes: [0]}]
      },
      {
        code: 'Astronautes',
        inputType: InputType.DROPDOWN_LIST,
        label: 'Astronautes',
        name: 'Astronautes',
        optional: true,
        bindings: [{bindableEntities: [BindableEntity.TEST_CASE], projectIndexes: [0]}],
        options: [{
          label: 'Gaspard'
        }, {
          label: 'Jacob'
        }]
      },
      {
        code: 'Révision',
        inputType: InputType.CHECKBOX,
        label: 'Révision',
        name: 'Révision',
        optional: true,
        bindings: [{bindableEntities: [BindableEntity.TEST_CASE], projectIndexes: [0]}],
      },
    ).withMilestones({
        label: 'Milestone 1',
        status: 'IN_PROGRESS',
        endDate: new Date('2020-05-19'),
        boundProjectIndexes: [0, 1]
      },
      {
        label: 'Milestone 2',
        status: 'IN_PROGRESS',
        endDate: new Date('2020-05-19'),
        boundProjectIndexes: [0, 1]
      },
      {
        label: 'Milestone 3',
        status: 'PLANNED',
        endDate: new Date('2020-05-19'),
        boundProjectIndexes: [0]
      },
      {
        label: 'Milestone 4',
        status: 'FINISHED',
        endDate: new Date('2020-05-19'),
        boundProjectIndexes: [0]
      }
    )
    .build();
}

function checkSpaceShuttles(multiListCriteria: GroupedMultiListElement) {
  multiListCriteria.assertGroupExist('Space Shuttles');
  multiListCriteria.assertGroupContain('Space Shuttles', [
    'Atlantys',
    'Endeavour',
    'Discovery',
    'Columbia',
    'Challenger',
  ]);
}

function checkRockets(multiListCriteria: GroupedMultiListElement) {
  multiListCriteria.assertGroupExist('Rockets');
  multiListCriteria.assertGroupContain('Rockets', [
    'Saturn 5',
    'Proton',
    'Ariane 6',
  ]);
}


function checkDefaultInfoList(multiListCriteria: GroupedMultiListElement) {
  multiListCriteria.assertGroupExist('Liste par défaut');
  multiListCriteria.assertGroupContain('Liste par défaut', [
    'Non définie',
    'Fonctionnel',
    'Métier',
    'Utilisateur',
    'Non fonctionnel',
    'Performance',
    'Sécurité',
    'ATDD',
  ]);
}

function checkNotFilteredList(multiListCriteria: GroupedMultiListElement) {
  checkSpaceShuttles(multiListCriteria);
  checkRockets(multiListCriteria);
  checkDefaultInfoList(multiListCriteria);
}


describe('Test Case Search', function () {

  beforeEach(() => {
    cy.viewport(1200, 720);
  });

  it('should display test case research page', () => {
    const testCaseSearchPage = TestCaseSearchPage.initTestAtPage();
    testCaseSearchPage.grid.filterPanel.assertPerimeterIsActive();
    testCaseSearchPage.grid.filterPanel.assertPerimeterHasValue('project 1');
    testCaseSearchPage.grid.filterPanel.assertAddCriteriaLinkIsPresent();
  });

  it('should show criteria list', () => {
    const referentialData = referentialDataWithCustomInfoLists();
    const testCaseSearchPage = TestCaseSearchPage.initTestAtPage(referentialData);
    const criteriaList = testCaseSearchPage.grid.filterPanel.openCriteriaList();
    criteriaList.assertGroupExist('Informations');
    criteriaList.assertGroupContain('Informations', [
      'Nom',
      'Référence',
      'ID',
      'Format',
      'Description',
      'Prérequis'
    ]);
    criteriaList.assertGroupExist('Attributs');
    criteriaList.assertGroupContain('Attributs', [
      'Nature',
      'Importance',
      'Statut',
      'Type'
    ]);
    criteriaList.assertGroupExist('Automatisation');
    criteriaList.assertGroupContain('Automatisation', [
      'Éligibilité à l\'automatisation',
      'Statut d\'automatisation'
    ]);
    criteriaList.assertGroupExist('Jalons');
    criteriaList.assertGroupContain('Jalons', [
      MILESTONE_NAME,
      MILESTONE_STATUS,
      MILESTONE_END_DATE
    ]);
    criteriaList.assertGroupExist('Contenu');
    criteriaList.assertGroupContain('Contenu', [
      STEP_COUNT,
      PARAM_COUNT,
      DATASET_COUNT,
      CALLSTEP_COUNT,
      ATTACH_COUNT,
    ]);

    criteriaList.assertGroupExist('Associations');
    criteriaList.assertGroupContain('Associations', [
      'Nombre d\'exigences',
      'Nombre d\'itérations',
      'Nombre d\'exécutions',
      'Nombre d\'anomalies',
    ]);

    criteriaList.assertGroupExist(HISTORICAL_GROUP_LABEL);
    criteriaList.assertGroupContain(HISTORICAL_GROUP_LABEL, [
      CREATED_ON_LABEL,
      CREATED_BY_LABEL,
      MODIFIED_ON_LABEL,
      MODIFIED_BY_LABEL
    ]);


    criteriaList.assertGroupExist(CUSTOM_FIELD_GROUP_LABEL);
    criteriaList.assertGroupContain(CUSTOM_FIELD_GROUP_LABEL, [
      'Objectif de mission',
      'Moteur du lanceur',
      'Numéro de mission',
    ]);
    criteriaList.assertNoItemIsSelected();
  });

  it('should add name criteria and search', () => {
    const testCaseSearchPage = TestCaseSearchPage.initTestAtPage();
    const textFilterWidgetElement = testCaseSearchPage.grid.filterPanel.selectTextCriteria('Nom');
    textFilterWidgetElement.cancel();
    textFilterWidgetElement.assertNotExist();
    testCaseSearchPage.grid.filterPanel.inactivateCriteria('Nom');
    testCaseSearchPage.grid.filterPanel.fillTextCriteria('Nom', 'STS');
    testCaseSearchPage.grid.filterPanel.assertCriteriaIsActive('Nom');
    // for now research do a toLowerCase on values
    testCaseSearchPage.grid.filterPanel.assertCriteriaHasValue('Nom', 'sts');
    testCaseSearchPage.grid.filterPanel.openExistingCriteria('Nom');
  });

  it('should add text cuf criteria and search', () => {
    const referentialData = referentialDataWithCustomInfoLists();
    const testCaseSearchPage = TestCaseSearchPage.initTestAtPage(referentialData);
    testCaseSearchPage.grid.filterPanel.fillTextCriteria('Moteur du lanceur', 'J5');
    testCaseSearchPage.grid.filterPanel.assertCriteriaIsActive('Moteur du lanceur');
    // for now research do a toLowerCase on values
    testCaseSearchPage.grid.filterPanel.assertCriteriaHasValue('Moteur du lanceur', 'j5');
    const criteriaList = testCaseSearchPage.grid.filterPanel.openCriteriaList();
    criteriaList.assertItemIsSelected('Moteur du lanceur');
  });

  it('should add date cuf criteria and search', () => {
    const referentialData = referentialDataWithCustomInfoLists();
    const testCaseSearchPage = TestCaseSearchPage.initTestAtPage(referentialData);
    const cufDate = testCaseSearchPage.grid.filterPanel.selectDateCriteria('Date de mission');
    cufDate.fillTodayDate();
    testCaseSearchPage.grid.filterPanel.assertCriteriaIsActive('Date de mission');
    cufDate.update();
    const localeToday = EditableDateFieldElement.dateToDisplayString(new Date(Date.now()));
    testCaseSearchPage.grid.filterPanel.assertCriteriaHasValue('Date de mission', `${OPERATION_LABELS.EQUALS} ${localeToday}`);
  });

  it('should add checkbox cuf criteria and research', () => {
    const referentialData = referentialDataWithCustomInfoLists();
    const testCaseSearchPage = TestCaseSearchPage.initTestAtPage(referentialData);
    const cufCheckbox = testCaseSearchPage.grid.filterPanel.selectMultiListCriteria('Révision');
    const researchOnTagElement = new SearchOnTagElement(cufCheckbox.url);
    researchOnTagElement.toggleOneItem('Vrai');
    researchOnTagElement.close();
    testCaseSearchPage.grid.filterPanel.assertCriteriaIsActive('Révision');
    testCaseSearchPage.grid.filterPanel.assertCriteriaHasValue('Révision', 'Vrai');
    testCaseSearchPage.grid.filterPanel.openExistingCriteria('Révision');
    researchOnTagElement.toggleOneItem('Faux');
    researchOnTagElement.close();
    testCaseSearchPage.grid.filterPanel.assertCriteriaHasValue('Révision', 'Faux, Vrai');
  });

  it('should add list cuf criteria and search', () => {
    const referentialData = referentialDataWithCustomInfoLists();
    const testCaseSearchPage = TestCaseSearchPage.initTestAtPage(referentialData);
    const cufListCriteria = testCaseSearchPage.grid.filterPanel.selectMultiListCriteria('Astronautes');
    const researchOnTagElement = new SearchOnTagElement(cufListCriteria.url);
    researchOnTagElement.toggleOneItem('Gaspard');
    researchOnTagElement.close();
    testCaseSearchPage.grid.filterPanel.assertCriteriaIsActive('Astronautes');
    testCaseSearchPage.grid.filterPanel.assertCriteriaHasValue('Astronautes', 'Gaspard');
    testCaseSearchPage.grid.filterPanel.openExistingCriteria('Astronautes');
    researchOnTagElement.toggleOneItem('Gaspard');
    researchOnTagElement.toggleOneItem('Jacob');
    researchOnTagElement.close();
    testCaseSearchPage.grid.filterPanel.assertCriteriaHasValue('Astronautes', 'Jacob');
  });

  it('should add created by criteria and search', () => {
    const users: UserView[] = [
      {
        id: 1,
        login: 'shepard',
        firstName: 'Alan',
        lastName: 'Shepard'
      },
      {
        id: 2,
        login: 'aldrin',
        firstName: 'Buzz',
        lastName: 'Aldrin'
      },
      {
        id: 3,
        login: 'armstrong',
        firstName: 'Neil',
        lastName: 'Armstrong'
      },
    ];
    const referentialData = referentialDataWithCustomInfoLists();
    const testCaseSearchPage = TestCaseSearchPage
      .initTestAtPage(referentialData, undefined, {usersWhoCreatedTestCases: users, usersWhoModifiedTestCases: []});
    const multiListCriteria = testCaseSearchPage.grid.filterPanel.selectMultiListCriteria(CREATED_BY_LABEL);
    const researchOnTagElement = new SearchOnTagElement(multiListCriteria.url);
    researchOnTagElement.toggleOneItem('aldrin');
    researchOnTagElement.close();
    testCaseSearchPage.grid.filterPanel.assertCriteriaIsActive(CREATED_BY_LABEL);
    testCaseSearchPage.grid.filterPanel.assertCriteriaHasValue(CREATED_BY_LABEL, 'aldrin');
    testCaseSearchPage.grid.filterPanel.openExistingCriteria(CREATED_BY_LABEL);
    researchOnTagElement.toggleOneItem('aldrin');
    researchOnTagElement.toggleOneItem('shepard');
    researchOnTagElement.toggleOneItem('armstrong');
    researchOnTagElement.close();
    testCaseSearchPage.grid.filterPanel.assertCriteriaHasValue(CREATED_BY_LABEL, 'armstrong, shepard');
  });

  it('should add modified by criteria and search', () => {
    const users: UserView[] = [
      {
        id: 1,
        login: 'shepard',
        firstName: 'Alan',
        lastName: 'Shepard'
      },
      {
        id: 2,
        login: 'aldrin',
        firstName: 'Buzz',
        lastName: 'Aldrin'
      },
      {
        id: 3,
        login: 'armstrong',
        firstName: 'Neil',
        lastName: 'Armstrong'
      },
    ];
    const referentialData = referentialDataWithCustomInfoLists();
    const testCaseSearchPage = TestCaseSearchPage
      .initTestAtPage(referentialData, undefined, {usersWhoCreatedTestCases: [], usersWhoModifiedTestCases: users});
    const multiListCriteria = testCaseSearchPage.grid.filterPanel.selectMultiListCriteria(MODIFIED_BY_LABEL);
    const researchOnModifiedByElement = new SearchOnTagElement(multiListCriteria.url);
    researchOnModifiedByElement.toggleOneItem('aldrin');
    researchOnModifiedByElement.close();
    testCaseSearchPage.grid.filterPanel.assertCriteriaIsActive(MODIFIED_BY_LABEL);
    testCaseSearchPage.grid.filterPanel.assertCriteriaHasValue(MODIFIED_BY_LABEL, 'aldrin');
    testCaseSearchPage.grid.filterPanel.openExistingCriteria(MODIFIED_BY_LABEL);
    researchOnModifiedByElement.toggleOneItem('aldrin');
    researchOnModifiedByElement.toggleOneItem('shepard');
    researchOnModifiedByElement.toggleOneItem('armstrong');
    researchOnModifiedByElement.close();
    testCaseSearchPage.grid.filterPanel.assertCriteriaHasValue(MODIFIED_BY_LABEL, 'armstrong, shepard');
  });

  it('should add cuf tag criteria and search', () => {
    const referentialData = referentialDataWithCustomInfoLists();
    const testCaseSearchPage = TestCaseSearchPage.initTestAtPage(referentialData);
    const multiListCriteria = testCaseSearchPage.grid.filterPanel.selectMultiListCriteria('Objectif de mission');
    const researchOnTagElement = new SearchOnTagElement(multiListCriteria.url);
    researchOnTagElement.toggleOneItem('Hubble');
    researchOnTagElement.close();
    testCaseSearchPage.grid.filterPanel.assertCriteriaIsActive('Objectif de mission');
    testCaseSearchPage.grid.filterPanel.assertCriteriaHasValue('Objectif de mission', 'Hubble');
    testCaseSearchPage.grid.filterPanel.openExistingCriteria('Objectif de mission');
    researchOnTagElement.toggleOneItem('ISS');
    researchOnTagElement.close();
    testCaseSearchPage.grid.filterPanel.assertCriteriaIsActive('Objectif de mission');
    testCaseSearchPage.grid.filterPanel.assertCriteriaHasValue('Objectif de mission', 'Hubble, ISS');
    testCaseSearchPage.grid.filterPanel.openExistingCriteria('Objectif de mission');
    researchOnTagElement.changeOperation(CUSTOM_FIELD_TAG_AND_OPERATION);
    researchOnTagElement.assertOperationChosen(CUSTOM_FIELD_TAG_AND_OPERATION);
    researchOnTagElement.changeOperation(CUSTOM_FIELD_TAG_OR_OPERATION);
    researchOnTagElement.assertOperationChosen(CUSTOM_FIELD_TAG_OR_OPERATION);
    researchOnTagElement.changeOperation(CUSTOM_FIELD_TAG_AND_OPERATION);
    researchOnTagElement.assertOperationChosen(CUSTOM_FIELD_TAG_AND_OPERATION);
    researchOnTagElement.close();
    testCaseSearchPage.grid.filterPanel.openExistingCriteria('Objectif de mission');
    researchOnTagElement.assertOperationChosen(CUSTOM_FIELD_TAG_AND_OPERATION);
  });

  it('should add attachment count criteria and search', () => {
    const referentialData = referentialDataWithCustomInfoLists();
    const testCaseSearchPage = TestCaseSearchPage.initTestAtPage(referentialData);
    const numericCriteria = testCaseSearchPage.grid.filterPanel.selectNumericCriteria(ATTACH_COUNT);
    numericCriteria.assertOperationChosen(OPERATION_LABELS.EQUALS);
    numericCriteria.changeOperation(OPERATION_LABELS.GREATER);
    numericCriteria.fillInputMin('1');
    numericCriteria.update();
    testCaseSearchPage.grid.filterPanel.assertCriteriaHasValue(ATTACH_COUNT, `${OPERATION_LABELS.GREATER} 1`);
    testCaseSearchPage.grid.filterPanel.openExistingCriteria(ATTACH_COUNT);
    numericCriteria.changeOperation(OPERATION_LABELS.BETWEEN);
    numericCriteria.fillInputMin('1');
    numericCriteria.fillInputMax('3');
    numericCriteria.update();
    testCaseSearchPage.grid.filterPanel.assertCriteriaHasValue(ATTACH_COUNT, `${OPERATION_LABELS.BETWEEN} 1 et 3`);
  });

  it('should add cuf numeric criteria and search', () => {
    const referentialData = referentialDataWithCustomInfoLists();
    const testCaseSearchPage = TestCaseSearchPage.initTestAtPage(referentialData);
    const numericCriteria = testCaseSearchPage.grid.filterPanel.selectNumericCriteria('Numéro de mission');
    numericCriteria.assertOperationChosen(OPERATION_LABELS.EQUALS);
    numericCriteria.fillInputMin('12');
    numericCriteria.update();
    testCaseSearchPage.grid.filterPanel.assertCriteriaHasValue('Numéro de mission', `${OPERATION_LABELS.EQUALS} 12`);
    testCaseSearchPage.grid.filterPanel.openExistingCriteria('Numéro de mission');
    numericCriteria.changeOperation(OPERATION_LABELS.BETWEEN);
    numericCriteria.fillInputMin('4');
    numericCriteria.fillInputMax('12');
    numericCriteria.update();
    testCaseSearchPage.grid.filterPanel.assertCriteriaHasValue('Numéro de mission', `${OPERATION_LABELS.BETWEEN} 4 et 12`);
  });

  it('should add id criteria and search', () => {
    const referentialData = referentialDataWithCustomInfoLists();
    const testCaseSearchPage = TestCaseSearchPage.initTestAtPage(referentialData);
    const numericCriteria = testCaseSearchPage.grid.filterPanel.selectNumericCriteria('ID', true);
    numericCriteria.fillInputMin('4');
    numericCriteria.update();
    testCaseSearchPage.grid.filterPanel.assertCriteriaHasValue('ID', `4`);
  });

  it('should research options in multi list', () => {
    const referentialData = referentialDataWithCustomInfoLists();
    const testCaseSearchPage = TestCaseSearchPage.initTestAtPage(referentialData);
    const multiListCriteria = testCaseSearchPage.grid.filterPanel.selectMultiListCriteria('Nature');
    checkNotFilteredList(multiListCriteria);
    multiListCriteria.filterValues('sa');
    multiListCriteria.assertGroupExist('Rockets');
    multiListCriteria.assertGroupContain('Rockets', [
      'Saturn 5',
    ]);
    multiListCriteria.assertGroupExist('Liste par défaut');
    multiListCriteria.assertGroupContain('Liste par défaut', [
      'Utilisateur',
    ]);
    multiListCriteria.assertGroupNotExist('Space Shuttles');
    multiListCriteria.clearFilter();
    checkNotFilteredList(multiListCriteria);
    multiListCriteria.filterValues('c');
    multiListCriteria.assertGroupExist('Space Shuttles');
    multiListCriteria.assertGroupContain('Space Shuttles', [
      'Discovery',
      'Columbia',
      'Challenger',
    ]);
    multiListCriteria.assertGroupExist('Liste par défaut');
    multiListCriteria.assertGroupContain('Liste par défaut', [
      'Fonctionnel',
      'Non fonctionnel',
      'Performance',
      'Sécurité',
    ]);
  });

  it('should add createdOn criteria and search', () => {
    const referentialData = referentialDataWithCustomInfoLists();
    const testCaseSearchPage = TestCaseSearchPage.initTestAtPage(referentialData);
    const dateCriteria = testCaseSearchPage.grid.filterPanel.selectDateCriteria(CREATED_ON_LABEL);
    dateCriteria.assertOperationChosen(OPERATION_LABELS.EQUALS);
    dateCriteria.changeOperation(OPERATION_LABELS.GREATER);
    dateCriteria.fillTodayDate();
    dateCriteria.update();
    const localeToday = EditableDateFieldElement.dateToDisplayString(new Date(Date.now()));
    testCaseSearchPage.grid.filterPanel.assertCriteriaHasValue(CREATED_ON_LABEL, `${OPERATION_LABELS.GREATER} ${localeToday}`);
    testCaseSearchPage.grid.filterPanel.openExistingCriteria(CREATED_ON_LABEL);
    dateCriteria.changeOperation(OPERATION_LABELS.BETWEEN);
    dateCriteria.assertRangeComponentExist();
  });

  it('should add fulltext criteria and search', () => {
    const testCaseSearchPage = TestCaseSearchPage.initTestAtPage();
    const descriptionFilterWidgetElement = testCaseSearchPage.grid.filterPanel.selectTextCriteria('Description');
    descriptionFilterWidgetElement.cancel();
    descriptionFilterWidgetElement.assertNotExist();
    testCaseSearchPage.grid.filterPanel.inactivateCriteria('Description');
    testCaseSearchPage.grid.filterPanel.fillTextCriteria('Description', 'Descr');
    testCaseSearchPage.grid.filterPanel.assertCriteriaIsActive('Description');
    testCaseSearchPage.grid.filterPanel.assertCriteriaHasValue('Description', 'Descr');
    const criteriaList = testCaseSearchPage.grid.filterPanel.openCriteriaList();
    criteriaList.assertItemIsSelected('Description');
    criteriaList.close();
    const prerequisiteFilterWidgetElement = testCaseSearchPage.grid.filterPanel.selectTextCriteria('Prérequis');
    prerequisiteFilterWidgetElement.typeAndConfirm('Test');
    testCaseSearchPage.grid.filterPanel.assertCriteriaIsActive('Prérequis');
    testCaseSearchPage.grid.filterPanel.assertCriteriaHasValue('Prérequis', 'Test');
  });

  it('should restore filters from url query param', () => {
    const referentialData = referentialDataWithCustomInfoLists();
    const filters: SimpleFilter[] = [
      {
        id: 'status',
        operation: FilterOperation.IN,
        value: {
          kind: 'multiple-discrete-value',
          value: [{
            id: 'WORK_IN_PROGRESS',
            label: 'En cours de rédaction'
          }]
        }
      },
      {
        id: 'name',
        operation: FilterOperation.LIKE,
        value: {
          kind: 'single-string-value',
          value: 'Atlantys'
        }
      }
    ];
    const queryString = `filters=${JSON.stringify(filters)}`;
    const testCaseSearchPage = TestCaseSearchPage.initTestAtPage(referentialData, undefined, undefined, queryString);
    testCaseSearchPage.grid.filterPanel.assertCriteriaIsActive('Statut');
    testCaseSearchPage.grid.filterPanel.assertCriteriaHasValue('Statut', 'En cours de rédaction');
    testCaseSearchPage.grid.filterPanel.assertCriteriaIsActive('Nom');
    testCaseSearchPage.grid.filterPanel.assertCriteriaHasValue('Nom', 'Atlantys');
  });

  it('should show milestone criteria', () => {
    const referentialData = referentialDataWithCustomInfoLists();
    const testCaseSearchPage = TestCaseSearchPage.initTestAtPage(referentialData);
    const multiList = testCaseSearchPage.grid.filterPanel.selectMultiListCriteria(MILESTONE_NAME);
    testCaseSearchPage.grid.filterPanel.assertCriteriaIsActive(MILESTONE_NAME);
    multiList.assertGroupContain('ungrouped-items', ['Milestone 1', 'Milestone 2', 'Milestone 3', 'Milestone 4']);
    multiList.close();
    const projectScope = testCaseSearchPage.grid.filterPanel.openProjectScopeSelector();
    projectScope.toggleOneItem('Project 1');
    projectScope.close();
    testCaseSearchPage.grid.filterPanel.openExistingCriteria(MILESTONE_NAME);
    multiList.assertGroupContain('ungrouped-items', ['Milestone 1', 'Milestone 2']);
  });

});

