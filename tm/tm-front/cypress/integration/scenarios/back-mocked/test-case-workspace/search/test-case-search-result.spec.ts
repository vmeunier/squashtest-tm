import {TestCaseSearchPage} from '../../../../page-objects/pages/test-case-workspace/research/test-case-search-page';
import {DataRow, GridResponse} from '../../../../model/grids/data-row.type';
import {createEntityReferentialData} from '../../../../utils/referential/create-entity-referential.const';
import {MilestoneMassEdit} from '../../../../model/milestone/milestone.model';

function getInitialNodes() {
  const initialNodes: GridResponse = {
    count: 1,
    dataRows: [{
      id: '1',
      type: 'TestCase',
      projectId: 1,
      data: {
        'name': 'Test 1',
        'id': 1,
        'reference': 'ref1',
        'projectName': 'project 1',
        'attachments': 0,
        'items': 0,
        'steps': 0,
        'nature': 12,
        'type': 16,
        'automatable': 'Y',
        'status': 'WORK_IN_PROGRESS',
        'importance': 'LOW',
        'createdBy': 'admin',
        'lastModifiedBy': 'admin',
        'tcMilestoneLocked': 0,
        'reqMilestoneLocked': 0
      }
    } as unknown as DataRow,
      {
        id: '2',
        type: 'TestCase',
        projectId: 1,
        data: {
          'name': 'Test 2',
          'id': 2,
          'reference': 'ref2',
          'projectName': 'project 1',
          'attachments': 0,
          'items': 0,
          'steps': 0,
          'nature': 12,
          'type': 16,
          'automatable': 'Y',
          'status': 'WORK_IN_PROGRESS',
          'importance': 'LOW',
          'createdBy': 'admin',
          'lastModifiedBy': 'admin',
          'tcMilestoneLocked': 1,
          'reqMilestoneLocked': 0
        }
      } as unknown as DataRow]
  };
  return initialNodes;
}

describe('Test Case Search Results', function () {

  beforeEach(() => {
    cy.viewport(1200, 720);
  });

  describe('Test Case search table', () => {
    const initialNodes: GridResponse = {
      count: 1,
      dataRows: [{
        id: '1',
        type: 'TestCase',
        projectId: 1,
        data: {
          'name': 'Test 1',
          'id': 1,
          'reference': 'ref1',
          'projectName': 'project 1',
          'attachments': 0,
          'items': 0,
          'steps': 0,
          'nature': 12,
          'type': 16,
          'automatable': 'Y',
          'status': 'WORK_IN_PROGRESS',
          'importance': 'LOW',
          'createdBy': 'admin',
          'lastModifiedBy': 'admin',
          'tcMilestoneLocked': 0,
          'reqMilestoneLocked': 0
        }
      } as unknown as DataRow]
    };

    it('should display row in table', () => {
      const testCaseSearchPage = TestCaseSearchPage.initTestAtPage(createEntityReferentialData, initialNodes);
      const gridElement = testCaseSearchPage.grid;

      gridElement.assertRowExist(1);
      const row = gridElement.getRow(1);
      row.cell('reference').textRenderer().assertContainText('ref1');
      row.cell('name').textRenderer().assertContainText('Test 1');

    });

    it('should edit cell in table', () => {

      const testCaseSearchPage = TestCaseSearchPage.initTestAtPage(createEntityReferentialData, initialNodes);
      const gridElement = testCaseSearchPage.grid;

      gridElement.assertRowExist(1);
      const row = gridElement.getRow(1);
      const refCell = row.cell('reference').textRenderer();
      refCell.editText('ref02', 'test-case/*/reference');
      refCell.assertContainText('ref02');

      const natureCell = row.cell('nature').selectRenderer();
      natureCell.changeValue('item-16', 'test-case/*/nature');
      natureCell.assertContainText('Non fonctionnel');

      const httpError = {
        squashTMError: {
          kind: 'FIELD_VALIDATION_ERROR',
          fieldValidationErrors: [
            {
              fieldName: 'name',
              i18nKey: 'sqtm-core.error.generic.duplicate-name'
            }
          ]
        },
      };
      const nameCell = row.cell('name').textRenderer();
      nameCell.editTextError('TestCase-42', 'test-case/*/name', httpError);
      nameCell.assertErrorDialogContains('Un élément avec ce nom existe déjà à cet emplacement');
    });
  });

  describe('Test Case search mass edit', () => {

    const initialNodes = getInitialNodes();

    const editResponse: GridResponse = {
      count: 1,
      dataRows: [{
        id: '1',
        type: 'TestCase',
        projectId: 1,
        data: {
          'name': 'Test 1',
          'id': 1,
          'reference': 'ref1',
          'projectName': 'project 1',
          'attachments': 0,
          'items': 0,
          'steps': 0,
          'nature': 14,
          'type': 16,
          'automatable': 'Y',
          'status': 'WORK_IN_PROGRESS',
          'importance': 'LOW',
          'createdBy': 'admin',
          'lastModifiedBy': 'admin',
          'tcMilestoneLocked': 0,
          'reqMilestoneLocked': 0
        }
      } as unknown as DataRow,
        {
          id: '2',
          type: 'TestCase',
          projectId: 1,
          data: {
            'name': 'Test 2',
            'id': 2,
            'reference': 'ref2',
            'projectName': 'project 1',
            'attachments': 0,
            'items': 0,
            'steps': 0,
            'nature': 12,
            'type': 16,
            'automatable': 'Y',
            'status': 'WORK_IN_PROGRESS',
            'importance': 'LOW',
            'createdBy': 'admin',
            'lastModifiedBy': 'admin',
            'tcMilestoneLocked': 1,
            'reqMilestoneLocked': 0
          }
        } as unknown as DataRow]
    };


    it('should mass edit rows', () => {

      const testCaseSearchPage = TestCaseSearchPage.initTestAtPage(createEntityReferentialData, initialNodes);
      const gridElement = testCaseSearchPage.grid;
      gridElement.selectRow(1, '#', 'leftViewport');
      const massEditTestCaseDialog = testCaseSearchPage.showMassEditDialog();
      massEditTestCaseDialog.assertExist();
      const natureField = massEditTestCaseDialog.getOptionalField('nature');
      natureField.check();
      natureField.selectValue('Métier');

      massEditTestCaseDialog.confirm(editResponse);
      const row = gridElement.getRow(1);
      const natureCell = row.cell('nature').selectRenderer();
      natureCell.assertContainText('Métier');
    });

    it('should mass edit only available rows', () => {
      const testCaseSearchPage = TestCaseSearchPage.initTestAtPage(createEntityReferentialData, initialNodes);
      const gridElement = testCaseSearchPage.grid;
      gridElement.selectRows([1, 2], '#', 'leftViewport');
      const massEditTestCaseDialog = testCaseSearchPage.showMassEditDialog();
      massEditTestCaseDialog.assertExist();
      massEditTestCaseDialog.assertExistNoWritingRightsMessage();
      const natureField = massEditTestCaseDialog.getOptionalField('nature');
      natureField.check();
      natureField.selectValue('Métier');
      massEditTestCaseDialog.confirm(editResponse);
      const row = gridElement.getRow(1);
      const natureCell = row.cell('nature').selectRenderer();
      natureCell.assertContainText('Métier');
    });

    it('should not display multi edit dialog if not writing rights', () => {
      const testCaseSearchPage = TestCaseSearchPage.initTestAtPage(createEntityReferentialData, initialNodes);
      const gridElement = testCaseSearchPage.grid;
      gridElement.selectRow(2, '#', 'leftViewport');
      const massEditTestCaseDialog = testCaseSearchPage.showMassEditDialog();
      massEditTestCaseDialog.assertNotExist();
      const noLineWritingRightsDialogElement = testCaseSearchPage.getNoLineWritingRightsDialog();
      noLineWritingRightsDialogElement.assertExist();
      noLineWritingRightsDialogElement.assertMessage();
      noLineWritingRightsDialogElement.close();
      noLineWritingRightsDialogElement.assertNotExist();
    });
  });

  describe('Test Case search edit milestones', () => {

    const initialNodes = getInitialNodes();

    it('should edit milestones on selected rows', () => {
      const testCaseSearchPage = TestCaseSearchPage.initTestAtPage(createEntityReferentialData, initialNodes);
      const gridElement = testCaseSearchPage.grid;
      gridElement.selectRow(1, '#', 'leftViewport');
      const milestoneMassEdit: MilestoneMassEdit = {
        milestoneIds: [1, 2],
        checkedIds: [1],
        samePerimeter: true,
        boundObjectIds: []
      };
      const milestoneDialog = testCaseSearchPage.showEditMilestonesDialog(milestoneMassEdit);
      milestoneDialog.assertExist();

      const milestoneGrid = milestoneDialog.getMilestoneGrid();
      milestoneGrid.assertExist();
      milestoneGrid.assertRowExist(1);
      milestoneGrid.assertRowExist(2);
      const row2 = milestoneGrid.getRow(2);
      const checkBoxCell = row2.cell('select-row-column').checkBoxRender();
      checkBoxCell.toggleState();
      checkBoxCell.assertIsCheck();

      milestoneDialog.confirm();
    });

    it('should display no same perimeter message', () => {
      const testCaseSearchPage = TestCaseSearchPage.initTestAtPage(createEntityReferentialData, initialNodes);
      const gridElement = testCaseSearchPage.grid;
      gridElement.selectRows([1, 2], '#', 'leftViewport');
      const milestoneMassEdit: MilestoneMassEdit = {
        milestoneIds: [1, 2],
        checkedIds: [1],
        samePerimeter: false,
        boundObjectIds: []
      };

      const milestoneDialog = testCaseSearchPage.showEditMilestonesDialog(milestoneMassEdit);
      milestoneDialog.assertExist();

      milestoneDialog.assertNoSamePerimeterMessage();
    });
  });
});
