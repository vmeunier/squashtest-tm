import {DataRow, GridResponse} from '../../../../model/grids/data-row.type';
import {TestCaseWorkspacePage} from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import {ReferentialDataMockBuilder} from '../../../../utils/referential/referential-data-builder';
import {XlsReport} from '../../../../model/test-case/import-test-case.model';

function buildReferentialData() {
  return new ReferentialDataMockBuilder()
    .withUser({userId: 1, admin: true, username: 'admin', projectManager: false, functionalTester: false, automationProgrammer: false})
    .withProjects(
      {name: 'Apollo', label: 'Apollo'}, {name: 'Gemini', label: 'Gemini'}
    ).build();
}

describe('Test Case Import', function () {
  it('should import test case with xls file', () => {
    const testCaseWorkspacePage: TestCaseWorkspacePage = navigateToTestCaseWorkspace();
    testCaseWorkspacePage.navBar.toggle();
    const importDialog = testCaseWorkspacePage.treeMenu.openImportTestCaseDialog();
    importDialog.chooseImportFile('test_import.xls', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    importDialog.clickImport();
    importDialog.checkConfirmationMessage('test_import.xls');

    const xlsReport: XlsReport = {
      templateOk: {
        reportUrl: 'test-cases/import/test_case_200000.xls',
        coverageFailures: 0,
        coverageSuccesses: 0,
        coverageWarnings: 0,
        datasetFailures: 0,
        datasetSuccesses: 0,
        datasetWarnings: 0,
        parameterFailures: 0,
        parameterSuccesses: 0,
        parameterWarnings: 0,
        testCaseFailures: 0,
        testCaseSuccesses: 1,
        testCaseWarnings: 0,
        testStepFailures: 0,
        testStepSuccesses: 0,
        testStepWarnings: 0
      },
      importFormatFailure: null
    };

    importDialog.confirmXlsImport({dataRows: []} as GridResponse, xlsReport);
    importDialog.assertReportExist('xls-report-ok');
    importDialog.assertButtonExist('close');
  });

  it('should display Error Import report', () => {
    const testCaseWorkspacePage: TestCaseWorkspacePage = navigateToTestCaseWorkspace();
    testCaseWorkspacePage.navBar.toggle();
    const importDialog = testCaseWorkspacePage.treeMenu.openImportTestCaseDialog();
    importDialog.chooseImportFile('test_import.xls', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    importDialog.clickImport();
    importDialog.checkConfirmationMessage('test_import.xls');

    const xlsReport: XlsReport = {
      templateOk: null,
      importFormatFailure: {
        duplicateColumns: ['TEST_CASE_NAME'],
        actionValidationError: null,
        missingMandatoryColumns: ['PROJECT_NAME']
      }
    };

    importDialog.confirmXlsImport({dataRows: []} as GridResponse, xlsReport);
    importDialog.assertReportExist('xls-report-ko');
    importDialog.assertButtonExist('cancel');
  });

  function navigateToTestCaseWorkspace(): TestCaseWorkspacePage {
    const referentialDataMock = buildReferentialData();

    const initialNodes: GridResponse = {
      count: 1,
      dataRows: [{
        id: 'TestCaseLibrary-1',
        children: [],
        data: {'NAME': 'Project 1', 'MILESTONES': [1, 2]}
      } as unknown as DataRow]
    };
    return TestCaseWorkspacePage.initTestAtPage(initialNodes, referentialDataMock);
  }
});
