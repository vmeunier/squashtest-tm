import {HomeWorkspacePage} from '../../../page-objects/pages/home-workspace/home-workspace.page';
import {NavBarElement} from '../../../page-objects/elements/nav-bar/nav-bar.element';
import {UserAccount} from '../../../model/user/user-account.model';
import {AuthenticationPolicy} from '../../../model/bugtracker/bug-tracker.model';
import {AuthenticationProtocol} from '../../../model/third-party-server/authentication.model';

describe('UserAccount', function () {
  it(`should navigate to user account`, () => {
    HomeWorkspacePage.initTestAtPage();
    const userAccountPage = NavBarElement.openUserAccount(getInitialData());

    userAccountPage.assertExist();

    userAccountPage.assertName('admin admin');
    userAccountPage.assertEmail('admin@squash.fr');
    userAccountPage.assertGroup('Administrateur');

    userAccountPage.permissionsGrid.assertRowCount(2);
  });

  it(`should load appropriate credentials form`, () => {
    HomeWorkspacePage.initTestAtPage();
    const userAccountPage = NavBarElement.openUserAccount(getInitialData());

    userAccountPage.assertBugTrackerLogin('username');

    userAccountPage.selectEditedBugTracker('BT2 (url2)');

    userAccountPage.assertBugTrackerRevokeButtonVisible();
  });
});

function getInitialData(): UserAccount {
  return {
    id: 1,
    login: 'admin',
    email: 'admin@squash.fr',
    firstName: 'admin',
    lastName: 'admin',
    bugTrackerCredentials: [
      {
        bugTracker: {
          id: 1,
          url: 'url',
          name: 'BT1',
          kind: 'jira.cloud',
          authProtocol: AuthenticationProtocol.BASIC_AUTH,
          authPolicy: AuthenticationPolicy.USER,
          iframeFriendly: true,
        },
        credentials: {
          implementedProtocol: AuthenticationProtocol.BASIC_AUTH,
          type: AuthenticationProtocol.BASIC_AUTH,
          username: 'username',
          password: 'password',
        }
      },
      {
        bugTracker: {
          id: 2,
          url: 'url2',
          name: 'BT2',
          kind: 'jira.server',
          authProtocol: AuthenticationProtocol.OAUTH_1A,
          authPolicy: AuthenticationPolicy.USER,
          iframeFriendly: true,
        },
        credentials: {
          implementedProtocol: AuthenticationProtocol.OAUTH_1A,
          type: AuthenticationProtocol.OAUTH_1A,
          token: 'token',
          tokenSecret: 'tokenSecret',
        }
      }
    ],
    projectPermissions: [
      {
        projectName: 'P1',
        projectId: 1,
        permissionGroup: {
          simpleName: 'ProjectManager',
          qualifiedName: 'ProjectManager',
          id: 1,
        }
      },
      {
        projectName: 'P2',
        projectId: 2,
        permissionGroup: {
          simpleName: 'AdvanceTester',
          qualifiedName: 'AdvanceTester',
          id: 2,
        }
      }
    ],
    bugTrackerMode: 'Automatic',
    userGroup: {
      id: 1,
      qualifiedName: 'squashtest.authz.group.core.Admin'
    },
    canManageLocalPassword: true,
    hasLocalPassword: true,
  };
}
