import {IterationViewPage} from '../../../../page-objects/pages/campaign-workspace/iteration/iteration-view.page';
import {DataRow, DataRowOpenState, GridResponse} from '../../../../model/grids/data-row.type';
import {CampaignWorkspacePage} from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import {IterationModel, IterationStatisticsBundle} from '../../../../model/campaign/iteration-model';
import {
  getDefaultIterationStatisticsBundle,
  getEmptyIterationStatisticsBundle,
  mockIterationModel
} from '../../../../data-mock/iteration.data-mock';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';

describe('Iteration - Statistics', () => {
  it('should display iteration dashboard stats charts', () => {
    const iterationViewPage = navigateToIteration();
    const dashboard = iterationViewPage.showDashboardPanel();
    dashboard.assertExist();
    dashboard.assertStatsChartAreRendered();
    dashboard.assertAdvancementChartIsRendered();
  });

  it('should display empty chart warning', () => {
    const iterationViewPage: IterationViewPage = navigateToIteration(getEmptyIterationStatisticsBundle());
    const dashboard = iterationViewPage.showDashboardPanel();
    dashboard.assertExist();
    dashboard.assertErrorMessageOnDateIsVisible();
    dashboard.assertErrorMessageOnEmptyTestPlanIsVisible();
  });

  it('should display test suite inventory', () => {
    const iterationViewPage = navigateToIteration();
    const dashboard = iterationViewPage.showDashboardPanel();
    dashboard.assertExist();
    dashboard.assertInventoryTableExist();
    dashboard.assertTestSuiteRowHasName(0, 'testSuite 1');
    dashboard.assertTestSuiteRowHasName(1, 'Sans suite');
    dashboard.assertTestSuiteRowHasName(2, 'Total');
    dashboard.checkStatusCount(2, {
      READY: 24,
      RUNNING: 3,
      SUCCESS: 5,
      SETTLED: 4,
      FAILURE: 2,
      BLOCKED: 9,
      UNTESTABLE: 2
    });
    dashboard.checkImportanceCount(2, {
      VERY_HIGH: 6,
      HIGH: 7,
      MEDIUM: 8,
      LOW: 6,
    });
    dashboard.checkCell(2, 'nb-total', '34');
    dashboard.checkCell(2, 'nb-to-execute', '14');
    dashboard.checkCell(2, 'nb-executed', '20');

  });

  function navigateToIteration(
    iterationStatisticsBundle: IterationStatisticsBundle = getDefaultIterationStatisticsBundle()): IterationViewPage {
    const initialNodes: GridResponse = {
      count: 1,
      dataRows: [{
        id: 'CampaignLibrary-1',
        children: [],
        data: {'NAME': 'Project1'},
      } as unknown as DataRow]
    };
    const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(initialNodes);
    const libraryChildren = [
      {
        id: 'CampaignLibrary-1',
        children: ['Campaign-3'],
        data: {'NAME': 'Project1', 'CHILD_COUNT': 1},
        state: DataRowOpenState.open
      } as unknown as DataRow,
      {
        id: 'Campaign-3',
        children: [],
        projectId: 1,
        parentRowId: 'CampaignLibrary-1',
        data: {'NAME': 'campaign3', 'CHILD_COUNT': 1}
      } as unknown as DataRow];

    const campaignChildren = [
      {
        id: 'Campaign-3',
        children: ['Iteration-1'],
        projectId: 1,
        parentRowId: 'CampaignLibrary-1',
        state: DataRowOpenState.open,
        data: {'NAME': 'campaign3', 'CHILD_COUNT': 1}
      } as unknown as DataRow,
      {
        id: 'Iteration-1',
        children: [],
        projectId: 1,
        parentRowId: 'Campaign-3',
        data: {'NAME': 'iteration-1', 'CHILD_COUNT': 0}
      } as unknown as DataRow];
    campaignWorkspacePage.tree.openNode('CampaignLibrary-1', libraryChildren);
    campaignWorkspacePage.tree.openNode('Campaign-3', campaignChildren);
    const model: IterationModel = mockIterationModel({
      testPlanStatistics: {
        status: 'DONE',
        progression: 100,
        nbTestCases: 3,
        nbDone: 3,
        nbReady: 0,
        nbRunning: 0,
        nbUntestable: 0,
        nbBlocked: 0,
        nbFailure: 0,
        nbSettled: 0,
        nbSuccess: 0
      },
    });
    new HttpMockBuilder(`iteration-view/${model.id}/statistics`)
      .responseBody(iterationStatisticsBundle)
      .build();
    return campaignWorkspacePage.tree.selectNode<IterationViewPage>('Iteration-1', model);
  }
});
