import {TestSuiteViewPage} from '../../../../page-objects/pages/campaign-workspace/test-suite/test-suite-view.page';
import {DataRow, DataRowOpenState, GridResponse} from '../../../../model/grids/data-row.type';
import {CampaignWorkspacePage} from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import {createEntityReferentialData} from '../../../../utils/referential/create-entity-referential.const';
import {TestSuiteModel} from '../../../../model/campaign/test-suite.model';


describe('Test Suite - Information', () => {
  it ('should display test-suite informations', () => {
    const testSuiteViewPage = navigateToTestSuite();
    testSuiteViewPage.checkData('test-suite-id', '5');
    testSuiteViewPage.checkData('test-suite-created', '09/03/2020 10:30 (Bébert)');
    testSuiteViewPage.checkData('test-suite-lastModified', '09/03/2020 11:30 (Riton)');
    testSuiteViewPage.checkData('execution-status', 'En cours');
    testSuiteViewPage.checkData('test-suite-progress-state', 'A exécuter');
    testSuiteViewPage.descriptionRichField.checkTextContent('this is a wonderful test suite');
  });

  it ('should allow modifications on test-suite informations', () => {
    const testSuiteViewPage = navigateToTestSuite();
    const initialTestSuiteRow: DataRow = {
      id: 'TestSuite-5',
        children: [],
        projectId: 1,
        parentRowId: 'Iteration-4',
        state: DataRowOpenState.leaf,
        data: {'NAME': 'testSuite-5', 'CHILD_COUNT': 0, 'MILESTONE_STATUS': 'IN_PROGRESS', 'EXECUTION_STATUS': 'RUNNING'}
    } as unknown as DataRow;

    testSuiteViewPage.rename('CHOUBIDOU BIDOU WA', {
      dataRows: [{
        ...initialTestSuiteRow,
        data: {...initialTestSuiteRow.data, 'NAME': 'CHOUBIDOU BIDOU WA'}
      }]
    });

    const descriptionElement = testSuiteViewPage.descriptionRichField;
    descriptionElement.enableEditMode();
    descriptionElement.setValue('Hello, World!');
    descriptionElement.confirm('Hello, World!');

  });

  function navigateToTestSuite(): TestSuiteViewPage {

    const initialNodes: GridResponse = {
      count: 1,
      dataRows: [{
        id: 'CampaignLibrary-1',
        children: [],
        data: {'NAME': 'Project1'},
      } as unknown as DataRow]
    };

    const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(initialNodes, createEntityReferentialData);
    const libraryChildren = [
      {
        id: 'CampaignLibrary-1',
        children: ['Campaign-3'],
        data: {'NAME': 'Project1', 'CHILD_COUNT': 1},
        state: DataRowOpenState.open
      } as unknown as DataRow,
      {
        id: 'Campaign-3',
        children: [],
        projectId: 1,
        parentRowId: 'CampaignLibrary-1',
        data: {'NAME': 'campaign3', 'CHILD_COUNT': 1}
      } as unknown as DataRow];

    const campaignChildren = [
      {
        id: 'Campaign-3',
        children: ['Iteration-4'],
        projectId: 1,
        parentRowId: 'CampaignLibrary-1',
        state: DataRowOpenState.open,
        data: {'NAME': 'campaign3', 'CHILD_COUNT': 1, 'MILESTONE_STATUS': 'IN_PROGRESS'}
      } as unknown as DataRow,
      {
        id: 'Iteration-4',
        children: [],
        projectId: 1,
        parentRowId: 'Campaign-3',
        data: {'NAME': 'iteration-4', 'CHILD_COUNT': 0, 'MILESTONE_STATUS': 'IN_PROGRESS'}
      } as unknown as DataRow];

    const iterationChildren = [
      {
        id: 'Campaign-3',
        children: ['Iteration-4'],
        projectId: 1,
        parentRowId: 'CampaignLibrary-1',
        state: DataRowOpenState.open,
        data: {'NAME': 'campaign3', 'CHILD_COUNT': 1, 'MILESTONE_STATUS': 'IN_PROGRESS'}
      } as unknown as DataRow,
      {
        id: 'Iteration-4',
        children: ['TestSuite-5'],
        projectId: 1,
        parentRowId: 'Campaign-3',
        state: DataRowOpenState.open,
        data: {'NAME': 'iteration-4', 'CHILD_COUNT': 1, 'MILESTONE_STATUS': 'IN_PROGRESS'}
      } as unknown as DataRow,
      {
        id: 'TestSuite-5',
        children: [],
        projectId: 1,
        parentRowId: 'Iteration-4',
        state: DataRowOpenState.leaf,
        data: {'NAME': 'testSuite-5', 'CHILD_COUNT': 0, 'MILESTONE_STATUS': 'IN_PROGRESS', 'EXECUTION_STATUS': 'RUNNING'}
      } as unknown as DataRow
    ];

    campaignWorkspacePage.tree.openNode('CampaignLibrary-1', libraryChildren);
    campaignWorkspacePage.tree.openNode('Campaign-3', campaignChildren);
    campaignWorkspacePage.tree.openNode('Iteration-4', iterationChildren);

    const executionStatusMap = new Map<number, string>();
    executionStatusMap.set(1, 'READY');
    executionStatusMap.set(2, 'SUCCESS');

    const model: TestSuiteModel = {
      id: 5,
      projectId: 1,
      name: 'testSuite-5',
      description: 'this is a wonderful test suite',
      uuid: '3e448aca-436a-11eb-a12b-5c80b64fb103',
      executionStatus: 'RUNNING',
      createdOn: new Date('2020-03-09 10:30'),
      createdBy: 'Bébert',
      lastModifiedOn: new Date('2020-03-09 11:30'),
      lastModifiedBy: 'Riton',
      testPlanStatistics: {
        status: 'RUNNING',
        progression: 50,
        nbTestCases: 2,
        nbDone: 1,
        nbReady: 1,
        nbRunning: 0,
        nbUntestable: 0,
        nbBlocked: 0,
        nbFailure: 0,
        nbSettled: 0,
        nbSuccess: 1
      },
      customFieldValues: [],
      attachmentList: {
        id: 1,
        attachments: []
      },
      hasDatasets: true,
      users: [
        {id: 1, login: 'raowl', firstName: 'Ra', lastName: 'Oul'},
        {id: 2, login: 'jawny', firstName: 'Joe', lastName: 'Ni'}
      ],
      executionStatusMap: executionStatusMap
    };

    return campaignWorkspacePage.tree.selectNode<TestSuiteViewPage>('TestSuite-5', model);
  }
});
