import {HomeWorkspacePage} from '../../../page-objects/pages/home-workspace/home-workspace.page';
import {ReferentialDataMockBuilder} from '../../../utils/referential/referential-data-builder';

describe('NavBar', function () {
  it(`should don't display bugtrackers menu if no bugtrackers in referential data`, () => {
    const homeWorkspacePage = HomeWorkspacePage.initTestAtPage();
    const navBar = homeWorkspacePage.navBar;
    navBar.assertBugTrackerMenuNotExist();
  });

  it(`should display bugtrackers menu`, () => {
    const referentialDataMockBuilder = new ReferentialDataMockBuilder();
    referentialDataMockBuilder.withProjects(
      {bugTrackerBinding: {id: 1, bugTrackerId: 1, projectId: 1}},
      {bugTrackerBinding: {id: 2, bugTrackerId: 2, projectId: 2}}
    )
      .withBugTrackers({name: 'Jira'}, {name: 'Mantis'})
      .withUser({functionalTester: true});
    const homeWorkspacePage = HomeWorkspacePage.initTestAtPage(referentialDataMockBuilder.build());
    const navBar = homeWorkspacePage.navBar;
    navBar.assertBugTrackerMenuExist();
    const bugTrackerMenu = navBar.showSubMenu('bugtrackers', 'bugtrackers-menu');
    bugTrackerMenu.assertExist();
    bugTrackerMenu.item('Jira').assertExist();
    bugTrackerMenu.item('Mantis').assertExist();
    bugTrackerMenu.hide();
  });

  it('should display initials in avatar', () => {
    const referentialDataMockBuilder = new ReferentialDataMockBuilder();
    referentialDataMockBuilder.withUser({
      username: 'admin',
      userId: 1,
      admin: true,
      firstName: 'Squash',
      lastName: 'administrator',
      projectManager: false,
      functionalTester: false,
      automationProgrammer: false,
    });

    const homeWorkspace = HomeWorkspacePage.initTestAtPage(referentialDataMockBuilder.build());
    const navBar = homeWorkspace.navBar;
    navBar.assertAvatarHaveText('SA');
  });

  it('should display workspace plugin', () => {
    const referentialDataMockBuilder = new ReferentialDataMockBuilder();
    referentialDataMockBuilder.withUser({
      username: 'admin',
      userId: 1,
      admin: true,
      firstName: 'Squash',
      lastName: 'administrator',
      projectManager: false,
      functionalTester: false,
      automationProgrammer: false,
    });

    let referentialData = referentialDataMockBuilder.build();
    referentialData = {
      ...referentialData, workspacePlugins: [
        {
          id: 'plugin.workspace.actionword',
          name: 'Action',
          iconName: 'sqtm-core-nav:automation-workspace',
          tooltip: 'Bibliothèque d\'actions',
          theme: 'test-case',
          url: 'plugin/actionword/action-word-workspace',
          workspaceId: 'action-word-workspace'
        }
      ]
    };
    const homeWorkspace = HomeWorkspacePage.initTestAtPage(referentialData);
    const navBar = homeWorkspace.navBar;
    navBar.assertWorkspacePluginLinkExist('plugin.workspace.actionword', 'Action');
    navBar.assertFakeActionWordNotExist();
  });

  it('should display advertising', () => {
    const referentialDataMockBuilder = new ReferentialDataMockBuilder();
    referentialDataMockBuilder.withUser({
      username: 'admin',
      userId: 1,
      admin: true,
      firstName: 'Squash',
      lastName: 'administrator',
      projectManager: false,
      functionalTester: false,
      automationProgrammer: false,
    });
    const homeWorkspace = HomeWorkspacePage.initTestAtPage(referentialDataMockBuilder.build());
    const navBar = homeWorkspace.navBar;
    navBar.assertFakeActionWordExist();
    navBar.assertWorkspacePluginLinkNotExist('plugin.workspace.actionword');
  });

  it('should display workspace buttons as links', () => {
    const referentialDataMockBuilder = new ReferentialDataMockBuilder();
    referentialDataMockBuilder.withUser({
      username: 'admin',
      userId: 1,
      admin: true,
      firstName: 'Squash',
      lastName: 'administrator',
      projectManager: false,
      functionalTester: false,
      automationProgrammer: false,
    });
    const homeWorkspace = HomeWorkspacePage.initTestAtPage(referentialDataMockBuilder.build());
    const navBar = homeWorkspace.navBar;
    navBar.assertWorkspaceLinkExist('requirement-link', 'Exigences', '/requirement-workspace');
    navBar.assertWorkspaceLinkExist('test-case-link', 'Cas de test', '/test-case-workspace');
    navBar.assertWorkspaceLinkExist('campaign-link', 'Campagnes', '/campaign-workspace');
    navBar.assertWorkspaceLinkExist('custom-report-link', 'Pilotage', '/custom-report-workspace');
  });


  it('should persist navbar folding in localstorage', () => {
    const referentialDataMockBuilder = new ReferentialDataMockBuilder();
    referentialDataMockBuilder.withUser({
      username: 'admin',
      userId: 1,
      admin: true,
      firstName: 'Squash',
      lastName: 'administrator',
      projectManager: false,
      functionalTester: false,
      automationProgrammer: false,
    });
    const homeWorkspace = HomeWorkspacePage.initTestAtPage(referentialDataMockBuilder.build());
    const navBar = homeWorkspace.navBar;
    navBar.toggle();
    navBar.assertNavBarIsFold();
    HomeWorkspacePage.initTestAtPage(referentialDataMockBuilder.build());
    navBar.assertNavBarIsFold();
  });
});
