import {ExecutionRunnerProloguePage} from '../../../page-objects/pages/execution/execution-runner-prologue-page';
import {ExecutionModel} from '../../../model/execution/execution.model';
import {mockExecutionModel} from '../../../data-mock/execution.data-mock';
import {TestPlanResumeModel} from '../../../model/campaign/test-plan-resume.model';


describe('Test Plan Execution runner', function () {
  it('it should navigate to next execution', () => {
    const executionModel: ExecutionModel = mockExecutionModel({});
    const prologuePage = ExecutionRunnerProloguePage.initTestInIterations(1, 1, 1, true, executionModel);
    const executionRunnerStepPage = prologuePage.startExecution();
    executionRunnerStepPage.assertFastForwardButtonIsActive();
    const nextExecutionModel: ExecutionModel = mockExecutionModel({id: 2});
    const testPlanResume: TestPlanResumeModel = {
      hasNextTestCase: false,
      testPlanItemId: 2,
      iterationId: 1,
      executionId: 2
    };
    executionRunnerStepPage.successButton.changeStatus(1, 1);
    executionRunnerStepPage.successButton.changeStatus(2, 1);
    executionRunnerStepPage.successButton.changeStatusAndForward(3, 1, '1', '1', '2', testPlanResume, nextExecutionModel);
    // as the runner is mainly based on navigation manipulation, checking the url is important
    cy.location('pathname').should('contain', '/execution-runner/iteration/1/test-plan/2/execution/2/prologue');
    prologuePage.assertExist();
  });

  it('it should navigate to prologue of first exec and fast forward', () => {
    const executionModel: ExecutionModel = mockExecutionModel({});
    const prologuePage = ExecutionRunnerProloguePage.initTestInIterations(1, 1, 1, true, executionModel);
    const executionRunnerStepPage = prologuePage.startExecution();
    executionRunnerStepPage.assertFastForwardButtonIsActive();
    let nextExecutionModel: ExecutionModel = mockExecutionModel({id: 2});
    let testPlanResume: TestPlanResumeModel = {
      hasNextTestCase: true,
      testPlanItemId: 2,
      iterationId: 1,
      executionId: 2
    };
    executionRunnerStepPage.fastForward('1', '1', '2', testPlanResume, nextExecutionModel);
    cy.location('pathname').should('contain', '/execution-runner/iteration/1/test-plan/2/execution/2/prologue');
    prologuePage.assertExist();
    prologuePage.startExecution();
    testPlanResume = {
      hasNextTestCase: false,
      testPlanItemId: 3,
      iterationId: 1,
      executionId: 3,
      initialStepIndex: 1
    };
    nextExecutionModel = mockExecutionModel({id: 3});
    executionRunnerStepPage.fastForward('1', '2', '3', testPlanResume, nextExecutionModel);
    cy.location('pathname').should('contain', '/execution-runner/iteration/1/test-plan/3/execution/3/step/2');
    executionRunnerStepPage.assertFastForwardButtonIsInactive();
  });
});
