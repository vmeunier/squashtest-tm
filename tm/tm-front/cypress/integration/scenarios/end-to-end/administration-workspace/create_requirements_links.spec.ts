import {NavBarElement} from '../../../page-objects/elements/nav-bar/nav-bar.element';
import {AdminWorkspaceCustomFieldsPage} from '../../../page-objects/pages/administration-workspace/admin-workspace-custom-fields.page';
// tslint:disable-next-line:max-line-length
import {AdminWorkspaceRequirementsLinksPage} from '../../../page-objects/pages/administration-workspace/admin-workspace-requirements-links.page';

describe('Create requirement links', function () {

  beforeEach(function () {
    Cypress.Cookies.debug(true);
    //cy.server();
  });


  it('should check error messages', () => {
    cy.task('cleanDatabase');
    cy.logInAs('admin', 'admin');

    const adminWorkspaceCUFPage = NavBarElement.navigateToAdministration<AdminWorkspaceCustomFieldsPage>('entities-customization');

    const adminWorkspaceRequirementsLinksPage =
      adminWorkspaceCUFPage.clickAnchor('requirements-links', 'requirements-links')as AdminWorkspaceRequirementsLinksPage;

    const createRequirementsLinkDialog = adminWorkspaceRequirementsLinksPage.openCreateRequirementsLink();

    createRequirementsLinkDialog.clickOnAddButton();
    createRequirementsLinkDialog.checkIfRequiredErrorMessageIsDisplayed();
    createRequirementsLinkDialog.fillRole1('role1');
    createRequirementsLinkDialog.clickOnAddButton();
    createRequirementsLinkDialog.checkIfRequiredErrorMessageIsDisplayed();
    createRequirementsLinkDialog.fillRole2('role2');
    createRequirementsLinkDialog.clickOnAddButton();
    createRequirementsLinkDialog.checkIfRequiredErrorMessageIsDisplayed();
    createRequirementsLinkDialog.fillRole1Code('CR1');
    createRequirementsLinkDialog.clickOnAddButton();
    createRequirementsLinkDialog.checkIfRequiredErrorMessageIsDisplayed();
    createRequirementsLinkDialog.fillRole2Code('CR2');
  });

  it('should create links', () => {
    cy.task('cleanDatabase');
    cy.logInAs('admin', 'admin');

    const adminWorkspaceCUFPage = NavBarElement.navigateToAdministration<AdminWorkspaceCustomFieldsPage>('entities-customization');

    const adminWorkspaceRequirementsLinksPage =
      adminWorkspaceCUFPage.clickAnchor('requirements-links', 'requirements-links')as AdminWorkspaceRequirementsLinksPage;

    let createRequirementsLinkDialog = adminWorkspaceRequirementsLinksPage.openCreateRequirementsLink();
    createRequirementsLinkDialog.createLink('role1', 'role1Code', 'role2', 'role2Code');

    createRequirementsLinkDialog = adminWorkspaceRequirementsLinksPage.openCreateRequirementsLink();
    createRequirementsLinkDialog.createLink('role12', 'role12Code', 'role22', 'role22Code');

    createRequirementsLinkDialog = adminWorkspaceRequirementsLinksPage.openCreateRequirementsLink();
    createRequirementsLinkDialog.createLink('role13', 'role13Code', 'role23', 'role23Code');

  });

  it('should delete links', () => {
    cy.logInAs('admin', 'admin');

    const adminWorkspaceCUFPage = NavBarElement.navigateToAdministration<AdminWorkspaceCustomFieldsPage>('entities-customization');

    const adminWorkspaceRequirementsLinksPage =
      adminWorkspaceCUFPage.clickAnchor('requirements-links', 'requirements-links')as AdminWorkspaceRequirementsLinksPage;

    adminWorkspaceRequirementsLinksPage.makeLinkDefault('role1', 'role1');
    adminWorkspaceRequirementsLinksPage.deleteLinkByName(
      'role1', 'role1', false, true, 'Le type sélectionné est le type par défaut. Il ne peut donc pas être supprimé.'
    );

    adminWorkspaceRequirementsLinksPage.makeLinkDefault('role1Code', 'RELATED');

    adminWorkspaceRequirementsLinksPage.selectLinksByContent('role1', ['role1', 'role12']);
    adminWorkspaceRequirementsLinksPage.clickOnMultipleDeleteButton(false);

    adminWorkspaceRequirementsLinksPage.deleteLinkByName('role1', 'role13', true);

     adminWorkspaceRequirementsLinksPage.selectLinksByContent('role1', ['role1', 'role12']);
     adminWorkspaceRequirementsLinksPage.clickOnMultipleDeleteButton(true);

  });

});
