import {NavBarElement} from '../../../page-objects/elements/nav-bar/nav-bar.element';
import {AdminWorkspaceProjectsPage} from '../../../page-objects/pages/administration-workspace/admin-workspace-projects.page';
import {ProjectE2eCommands} from '../../../page-objects/scenarios-parts/project/project_e2e_commands';

describe('Create Project Model', function () {

  beforeEach(function () {
    Cypress.Cookies.debug(true);
    //cy.server();
    cy.task('cleanDatabase');
    ProjectE2eCommands.createProject('project-1');
  });

  it('should check disabled option', () => {
    cy.logInAs('admin', 'admin');
    const adminWorkspaceProjectsPage =
      NavBarElement.navigateToAdministration('projects') as AdminWorkspaceProjectsPage;
    adminWorkspaceProjectsPage.checkIfCreateFromProjectDisabled(true);
  });

  it('should create a model', () => {
    cy.logInAs('admin', 'admin');
    let adminWorkspaceProjectsPage = NavBarElement.navigateToAdministration('projects') as AdminWorkspaceProjectsPage;
    let  createTemplateDialog = adminWorkspaceProjectsPage.openCreateTemplate();
    createTemplateDialog.createTemplate('my template', 'template label', 'template description');
    adminWorkspaceProjectsPage.grid.findRowId('name', 'my template').then(templateId => {
      adminWorkspaceProjectsPage.grid.selectRows([templateId]);
    });

    adminWorkspaceProjectsPage.checkIfEntryIsModel('my template', true);

    createTemplateDialog = adminWorkspaceProjectsPage.openCreateTemplate();
    createTemplateDialog.fillDescription('template description');
    createTemplateDialog.fillLabel('template label');
    createTemplateDialog.clickOnAddButton();
    createTemplateDialog.checkIfRequiredErrorMessageIsDisplayed();
    createTemplateDialog.fillName('my template');
    createTemplateDialog.clickOnAddButton();
    createTemplateDialog.checkIfNameAlreadyInUseErrorMessageIsDisplayed();

    createTemplateDialog.cancel();

    // should create model from project'
    cy.goHome();
    adminWorkspaceProjectsPage = NavBarElement.navigateToAdministration('projects') as AdminWorkspaceProjectsPage;
    adminWorkspaceProjectsPage.grid.findRowId('name', 'project-1').then(projectId => {
      adminWorkspaceProjectsPage.grid.selectRows([projectId]);
    });
    const createTemplateFromProjectDialog = adminWorkspaceProjectsPage.openCreateTemplateFromProject();

    createTemplateFromProjectDialog.fillDescription('template description');
    createTemplateFromProjectDialog.fillLabel('template label');
    createTemplateFromProjectDialog.clickOnAddButton();
    createTemplateFromProjectDialog.checkIfRequiredErrorMessageIsDisplayed();
    createTemplateFromProjectDialog.fillName('my template');
    createTemplateFromProjectDialog.clickOnAddButton();
    createTemplateFromProjectDialog.checkIfNameAlreadyInUseErrorMessageIsDisplayed();
   createTemplateFromProjectDialog.cancel();

   cy.goHome();
    adminWorkspaceProjectsPage = NavBarElement.navigateToAdministration('projects') as AdminWorkspaceProjectsPage;
    adminWorkspaceProjectsPage.checkIfEntryIsModel('project-1', false);
    adminWorkspaceProjectsPage.checkIfEntryIsModel('my template', true);

  });
});
