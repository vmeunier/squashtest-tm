import {InfoListItem} from './infolistitem.model';

export class InfoList {
  id: number;
  uri: string;
  code: string;
  label: string;
  description: string;
  items: InfoListItem[];
}
