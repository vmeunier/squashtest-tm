export interface MilestoneBinding {
  id: number;
  projectId: number;
  milestoneId: number;
}
