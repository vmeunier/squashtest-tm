import {MilestoneStatusKeys} from '../level-enums/level-enum';
import {Identifier} from '../grids/data-row.type';


export interface Milestone {
  id: number;
  label: string;
  description: string;
  endDate: Date;
  status: MilestoneStatusKeys;
  range: string;
  ownerFistName: string;
  ownerLastName: string;
  ownerLogin: string;
  createdBy: string;
  createdOn: Date;
  lastModifiedBy: string;
  lastModifiedOn: Date;
}

export interface MilestoneView extends Milestone {
  boundToObject: boolean;
}

export interface MilestoneAdminView extends Milestone {
  canEdit: boolean;
  boundProjectsInformation: ProjectInfoForMilestoneAdminView[];
}

export interface MilestoneMassEdit {
  milestoneIds: number[];
  checkedIds: number[];
  samePerimeter: boolean;
  boundObjectIds: Identifier[];
}

export interface ProjectInfoForMilestoneAdminView {
  projectId: number;
  projectName: string;
  template: boolean;
  boundToMilestone: boolean;
  milestoneBoundToOneObjectOfProject: boolean;
}
