import {Permissions} from '../permissions/permissions.model';

export interface WorkspaceWizard {
  id: string;
  wizardMenu?: WizardMenu;
}

export interface WizardMenu {
  tooltip: string;
  url: string;
  label: string;
  accessRule: AccessRule;
}

// tslint:disable-next-line:no-empty-interface
export interface AccessRule {}

interface NodeSelection extends AccessRule {
  selectionMode: SelectionMode;
  rules: AccessRule[];
}

export enum SelectionMode {
  SINGLE_SELECTION = 'SINGLE_SELECTION',
  MULTIPLE_SELECTION = 'MULTIPLE_SELECTION',
}

export interface SelectedNodePermission extends AccessRule {
  nodeType: TreeNodeType;
  permission: Permissions;
}

export enum TreeNodeType {
  LIBRARY= 'LIBRARY',
  FOLDER = 'FOLDER',
  CAMPAIGN = 'CAMPAIGN',
  TEST_CASE = 'TEST_CASE',
  REQUIREMENT = 'REQUIREMENT',
  ITERATION = 'ITERATION',
}

export function isNodeSelectionAccessRule(accessRule: AccessRule): accessRule is NodeSelection {
  return accessRule.hasOwnProperty('selectionMode') && Object.values(SelectionMode).includes(accessRule['selectionMode'])
    && accessRule.hasOwnProperty('rules') && Array.isArray(accessRule['rules']);
}

export function isSelectedNodePermissionAccessRule(accessRule: AccessRule): accessRule is SelectedNodePermission {
  return accessRule.hasOwnProperty('nodeType') && typeof accessRule['nodeType'] === 'string'
    && accessRule.hasOwnProperty('permission') && Object.values(Permissions).includes(accessRule['permission']);
}
