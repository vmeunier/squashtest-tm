export interface RequirementVersionStats {
  plannedTestCase: number;
  allTestCaseCount: number;
  validatedTestCases: number;
  redactedTestCase: number;
  verifiedTestCase: number;
  executedTestCase: number;
}
