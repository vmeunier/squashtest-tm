export class XlsReport {
  templateOk: ImportLog;
  importFormatFailure: ImportFormatFailure;
}

export class ImportLog {
  requirementVersionSuccesses: number;
  requirementVersionWarnings: number;
  requirementVersionFailures: number;
  coverageSuccesses: number;
  coverageWarnings: number;
  coverageFailures: number;
  reqlinksSuccesses: number;
  reqlinksWarnings: number;
  reqlinksFailures: number;
  reportUrl: string;
}

export interface ImportFormatFailure {
  missingMandatoryColumns: string[];
  duplicateColumns: string[];
  actionValidationError: any;
}
