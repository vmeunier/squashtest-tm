export interface RequirementVersionLinkType {
  id: number;
  role: string;
  role1Code: string;
  role2: string;
  role2Code: string;
  default: boolean;
  linkCount: number;
}
