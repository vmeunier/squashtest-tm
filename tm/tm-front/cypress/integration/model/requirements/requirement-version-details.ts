import {RequirementCriticalityKeys, RequirementStatusKeys} from '../level-enums/level-enum';

export interface RequirementVersionDetails {
  criticality: RequirementCriticalityKeys;
  status: RequirementStatusKeys;
  category: number;
  description: string;
  versionNumber: number;
}
