import {AttachmentListModel} from './attachment/attachment-list.model';
import {CustomFieldValueModel} from './customfield/custom-field-value.model';

export interface EntityModel {
  id: number;
  projectId: number;
  attachmentList: AttachmentListModel;
  customFieldValues: CustomFieldValueModel[];
}

export enum EntityType {
  PROJECT = 'PROJECT',
  TEST_CASE_LIBRARY = 'TEST_CASE_LIBRARY',
  TEST_CASE_FOLDER = 'TEST_CASE_FOLDER',
  TEST_CASE = 'TEST_CASE',
  TEST_CASE_STEP = 'TEST_CASE_STEP',
  REQUIREMENT_LIBRARY = 'REQUIREMENT_LIBRARY',
  REQUIREMENT_FOLDER = 'REQUIREMENT_FOLDER',
  REQUIREMENT = 'REQUIREMENT',
  REQUIREMENT_VERSION = 'REQUIREMENT_VERSION',
  CAMPAIGN_LIBRARY = 'CAMPAIGN_LIBRARY',
  CAMPAIGN_FOLDER = 'CAMPAIGN_FOLDER',
  CAMPAIGN = 'CAMPAIGN',
  ITERATION = 'ITERATION',
  EXECUTION = 'EXECUTION',
  TEST_SUITE = 'TEST_SUITE',
  EXECUTION_STEP = 'EXECUTION_STEP',
  TEST_STEP = 'TEST_STEP',
  ISSUE = 'ISSUE',
  ITEM_TEST_PLAN = 'ITEM_TEST_PLAN',
  INFO_LIST_ITEM = 'INFO_LIST_ITEM',
  USER = 'USER',
  MILESTONE = 'MILESTONE',
  AUTOMATED_TEST = 'AUTOMATED_TEST',
  AUTOMATED_EXECUTION_EXTENDER = 'AUTOMATED_EXECUTION_EXTENDER',
  ATTACHMENT = 'ATTACHMENT',
  ATTACHMENT_LIST = 'ATTACHMENT_LIST',
  AUTOMATION_REQUEST = 'AUTOMATION_REQUEST',
  DATASET = 'DATASET',
  PARAMETER = 'PARAMETER'
}

export class EntityReference {

  constructor(public id: number, public type: EntityType) {
  }

  static toEntityReference(candidate: string): EntityReference {
    if (typeof candidate === 'string') {
      const splitNode = candidate.split('-');
      const candidateType = splitNode[0];
      const typeElement = EntityType[candidateType];
      if (!typeElement) {
        throw Error(`Unable to parse id ${candidate}`);
      }
      return new EntityReference(Number.parseInt(splitNode[1], 10), typeElement);
    }
    throw Error(`Unable to parse id ${candidate}`);
  }

  asString(): string {
    return `${this.type}-${this.id}`;
  }
}
