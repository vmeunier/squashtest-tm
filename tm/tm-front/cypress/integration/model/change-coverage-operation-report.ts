import {RequirementVersionCoverage} from './test-case/requirement-version-coverage-model';
import {VerifyingTestCase} from './requirements/verifying-test-case';
import {RequirementVersionLink} from './requirements/requirement-version.link';

export interface ChangeOperationReport {
  summary: SummaryExceptions;
}

export interface ChangeCoverageOperationReport extends ChangeOperationReport {
  coverages: RequirementVersionCoverage[];
}

export interface ChangeVerifyingTestCaseOperationReport extends ChangeOperationReport {
  verifyingTestCases: VerifyingTestCase[];
}

export interface ChangeItemTestPlanIdsReport extends ChangeOperationReport {
  itemTestPlanIds: number[];
}

export class SummaryExceptions {
  alreadyVerifiedRejections?: any;
  notLinkableRejections?: any;
  noVerifiableVersionRejections?: any;
}

export interface ChangeLinkedRequirementOperationReport {
  summary: RequirementLinksSummaryExceptions;
  requirementVersionLinks: RequirementVersionLink[];
}

export class RequirementLinksSummaryExceptions {
  notLinkableRejections: boolean;
  alreadyLinkedRejections: boolean;
  sameRequirementRejections: boolean;
}
