import {EntityModel} from '../../entity.model';

export interface CustomReportFolderModel extends EntityModel {
  name: string;
  description: string;
}

