import {EntityModel} from '../../entity.model';

export interface CustomReportLibraryModel extends EntityModel {
  name: string;
  description: string;
}

