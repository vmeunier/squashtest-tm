export type AclClass =
  'PROJECT' |
  'PROJECT_TEMPLATE' |
  'REQUIREMENT_LIBRARY' |
  'TEST_CASE_LIBRARY' |
  'CAMPAIGN_LIBRARY' |
  'CUSTOM_REPORT_LIBRARY' |
  'AUTOMATION_REQUEST_LIBRARY' |
  'ACTION_WORD_LIBRARY';

export enum Permissions {
  READ = 'READ',
  WRITE = 'WRITE',
  CREATE = 'CREATE',
  DELETE = 'DELETE',
  ADMIN = 'ADMIN',
  MANAGEMENT = 'MANAGEMENT',
  EXPORT = 'EXPORT',
  EXECUTE = 'EXECUTE',
  LINK = 'LINK',
  IMPORT = 'IMPORT',
  ATTACH = 'ATTACH',
  EXTENDED_DELETE = 'EXTENDED_DELETE',
  READ_UNASSIGNED = 'READ_UNASSIGNED',
  WRITE_AS_FUNCTIONAL = 'WRITE_AS_FUNCTIONAL',
  WRITE_AS_AUTOMATION = 'WRITE_AS_AUTOMATION',
}


export interface ProjectPermissions {
  PROJECT: (Permissions.READ | Permissions.WRITE | Permissions.DELETE | Permissions.ADMIN | Permissions.MANAGEMENT | Permissions.ATTACH)[];
  PROJECT_TEMPLATE: (Permissions.READ | Permissions.WRITE | Permissions.DELETE | Permissions.ADMIN |
    Permissions.MANAGEMENT | Permissions.ATTACH)[];
  REQUIREMENT_LIBRARY: (Permissions.READ | Permissions.WRITE | Permissions.CREATE | Permissions.DELETE | Permissions.ATTACH
    | Permissions.LINK)[];
  TEST_CASE_LIBRARY: (Permissions.READ | Permissions.WRITE | Permissions.CREATE | Permissions.DELETE | Permissions.ATTACH
    | Permissions.EXPORT | Permissions.LINK)[];
  CAMPAIGN_LIBRARY: (Permissions.READ | Permissions.WRITE | Permissions.CREATE | Permissions.DELETE | Permissions.ATTACH
    | Permissions.EXPORT | Permissions.EXECUTE | Permissions.LINK | Permissions.EXTENDED_DELETE | Permissions.READ_UNASSIGNED)[];
  CUSTOM_REPORT_LIBRARY: (Permissions.READ | Permissions.WRITE | Permissions.CREATE | Permissions.DELETE | Permissions.ATTACH)[];
  AUTOMATION_REQUEST_LIBRARY: (Permissions.READ | Permissions.WRITE | Permissions.CREATE | Permissions.DELETE | Permissions.ATTACH)[];
  ACTION_WORD_LIBRARY: (Permissions.READ | Permissions.WRITE | Permissions.CREATE | Permissions.DELETE | Permissions.ATTACH)[];
}

export const NO_PERMISSIONS: Readonly<ProjectPermissions> = {
  PROJECT: [],
  PROJECT_TEMPLATE: [],
  REQUIREMENT_LIBRARY: [],
  TEST_CASE_LIBRARY: [],
  CAMPAIGN_LIBRARY: [],
  CUSTOM_REPORT_LIBRARY: [],
  AUTOMATION_REQUEST_LIBRARY: [],
  ACTION_WORD_LIBRARY: []
};

// All permissions by business domain.
// Mainly used to give permissions to admin and avoid the usual isAdmin everywhere that can cause weirds behavior
// as whe always develop in admin... aka an admin shouldn't have rights that doesn't exist for a given entity...
// Even a mighty Administrator cannot READ_UNASSIGNED on a Requirement. Sad panda...
export const ADMIN_PERMISSIONS: Readonly<ProjectPermissions> = {
  PROJECT: [Permissions.READ, Permissions.WRITE, Permissions.DELETE, Permissions.ADMIN, Permissions.MANAGEMENT, Permissions.ATTACH],
  PROJECT_TEMPLATE: [Permissions.READ, Permissions.WRITE, Permissions.DELETE, Permissions.ADMIN,
    Permissions.MANAGEMENT, Permissions.ATTACH],
  REQUIREMENT_LIBRARY: [Permissions.READ, Permissions.WRITE, Permissions.CREATE, Permissions.DELETE, Permissions.ATTACH, Permissions.LINK],
  TEST_CASE_LIBRARY: [Permissions.READ, Permissions.WRITE, Permissions.CREATE, Permissions.DELETE, Permissions.ATTACH, Permissions.LINK],
  CAMPAIGN_LIBRARY: [Permissions.READ, Permissions.WRITE, Permissions.CREATE, Permissions.DELETE, Permissions.ATTACH,
    Permissions.EXPORT, Permissions.EXECUTE, Permissions.LINK, Permissions.EXTENDED_DELETE, Permissions.READ_UNASSIGNED],
  CUSTOM_REPORT_LIBRARY: [Permissions.READ, Permissions.WRITE, Permissions.CREATE, Permissions.DELETE, Permissions.ATTACH],
  AUTOMATION_REQUEST_LIBRARY: [Permissions.READ, Permissions.WRITE, Permissions.CREATE, Permissions.DELETE, Permissions.ATTACH],
  ACTION_WORD_LIBRARY: [Permissions.READ, Permissions.WRITE, Permissions.CREATE, Permissions.DELETE, Permissions.ATTACH]
};

export enum AclGroup {
  ADVANCE_TESTER = 'squashtest.acl.group.tm.AdvanceTester',
  AUTOMATED_TEST_WRITER = 'squashtest.acl.group.tm.AutomatedTestWriter',
  PROJECT_MANAGER = 'squashtest.acl.group.tm.ProjectManager',
  PROJECT_VIEWER = 'squashtest.acl.group.tm.ProjectViewer',
  TEST_DESIGNER = 'squashtest.acl.group.tm.TestDesigner',
  TEST_EDITOR = 'squashtest.acl.group.tm.TestEditor',
  TEST_RUNNER = 'squashtest.acl.group.tm.TestRunner',
  VALIDATOR = 'squashtest.acl.group.tm.Validator',
}
