import {Credentials} from '../third-party-server/credentials.model';
import {AuthenticationProtocol} from '../third-party-server/authentication.model';

export class TestAutomationServer {
  id: number;
  baseUrl: string;
  kind: TestAutomationServerKind;
  name: string;
  description: string;
  createdBy: string;
  createdOn: Date;
  lastModifiedBy: string;
  lastModifiedOn: Date;
  manualSlaveSelection: boolean;
  authProtocol: AuthenticationProtocol;
}

export class AdminTestAutomationServer extends TestAutomationServer {
  credentials: Credentials;
  supportedAuthenticationProtocols: AuthenticationProtocol[];
}

export enum TestAutomationServerKind {
  jenkins = 'jenkins',
  squashAutom = 'squashAutom',
}

