import {CustomFieldBinding} from './customfield/custom-field-binding.model';

export enum BindableEntity {
  REQUIREMENT_FOLDER = 'REQUIREMENT_FOLDER',
  CAMPAIGN_FOLDER = 'CAMPAIGN_FOLDER',
  TESTCASE_FOLDER = 'TESTCASE_FOLDER',
  CUSTOM_REPORT_FOLDER = 'CUSTOM_REPORT_FOLDER',
  TEST_CASE = 'TEST_CASE',
  TEST_STEP = 'TEST_STEP',
  CAMPAIGN = 'CAMPAIGN',
  ITERATION = 'ITERATION',
  TEST_SUITE = 'TEST_SUITE',
  REQUIREMENT_VERSION = 'REQUIREMENT_VERSION',
  EXECUTION = 'EXECUTION',
  EXECUTION_STEP = 'EXECUTION_STEP'
}

export type Bindings = { [id in BindableEntity]: CustomFieldBinding[] };
