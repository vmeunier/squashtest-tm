import {AuthenticationProtocol} from './authentication.model';

export interface BasicAuthCredentials {
  implementedProtocol: AuthenticationProtocol.BASIC_AUTH;
  type: AuthenticationProtocol.BASIC_AUTH;
  username: string;
  password: string;
}

export interface OAuthCredentials {
  implementedProtocol: AuthenticationProtocol.OAUTH_1A;
  type: AuthenticationProtocol.OAUTH_1A;
  token: string;
  tokenSecret: string;
}

export type Credentials = BasicAuthCredentials | OAuthCredentials;

export function isOAuth1aCredentials(credentials: any): credentials is OAuthCredentials {
  return credentials?.type === AuthenticationProtocol.OAUTH_1A;
}

export function isBasicAuthCredentials(credentials: any): credentials is BasicAuthCredentials {
  return credentials?.type === AuthenticationProtocol.BASIC_AUTH;
}
