export interface TestPlanStatistics {
  nbTestCases: number;
  progression: number;
  status: any;
  nbDone: number;
  nbBlocked: number;
  nbReady: number;
  nbSettled: number;
  nbUntestable: number;
  nbSuccess: number;
  nbRunning: number;
  nbFailure: number;
}
