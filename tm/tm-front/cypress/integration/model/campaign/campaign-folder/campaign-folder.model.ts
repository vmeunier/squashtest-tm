import {EntityModel} from '../../entity.model';
import {CampaignProgressionStatistics, ConclusivenessStatusCount, ExecutionStatusCount} from '../campaign-model';
import {TestCaseImportanceKeys} from '../../level-enums/level-enum';

export interface CampaignFolderModel extends EntityModel {
  name: string;
  description: string;
}

export interface CampaignTestInventoryStatistics {
  campaignName: string;
  statistics: Partial<ExecutionStatusCount>;

}

export interface CampaignFolderStatisticsBundle {
  campaignProgressionStatistics: CampaignProgressionStatistics;
  campaignTestCaseStatusStatistics: Partial<ExecutionStatusCount>;
  campaignTestCaseSuccessRateStatistics: { conclusiveness: { [K in TestCaseImportanceKeys]: ConclusivenessStatusCount } };
  campaignNonExecutedTestCaseImportanceStatistics: { [K in TestCaseImportanceKeys]: number };
  campaignTestInventoryStatisticsList: CampaignTestInventoryStatistics[];
}

