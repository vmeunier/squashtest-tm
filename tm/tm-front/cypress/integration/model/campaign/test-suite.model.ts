import {EntityModel} from '../entity.model';
import {TestPlanStatistics} from './test-plan-statistics';
import {SimpleUser} from '../user/user.model';

export interface TestSuiteModel extends EntityModel {
  name: string;
  description: string;
  uuid: string;
  executionStatus: string;
  createdOn: Date;
  createdBy: string;
  lastModifiedOn: Date;
  lastModifiedBy: string;
  testPlanStatistics: TestPlanStatistics;
  hasDatasets: boolean;
  // TODO: check expected type
  executionStatusMap: Map<number, string>;
  users: SimpleUser[];
}
