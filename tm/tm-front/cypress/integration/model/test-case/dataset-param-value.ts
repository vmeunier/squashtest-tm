export class DatasetParamValue {
  id: number;
  value: string;
  parameterId: number;
  datasetId: number;
}
