import {Parameter} from './parameter.model';
import {Dataset} from './dataset.model';
import {DatasetParamValue} from './dataset-param-value';

export interface TestCaseParameterOperationReport {
  parameters: Parameter[];
  dataSets: Dataset[];
  paramValues: DatasetParamValue[];
}
