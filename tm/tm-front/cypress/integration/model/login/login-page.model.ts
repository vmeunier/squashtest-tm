export interface LoginPageModel {
  loginMessage: string;
  squashVersion: string;
  isH2: boolean;
}
