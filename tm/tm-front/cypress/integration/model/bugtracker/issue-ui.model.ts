import {IssueBindableEntity} from './issue-bindable-entity.model';
import {BugTrackerMode} from './bug-tracker.model';

export class IssueUiModel {
  bugTrackerStatus: string;
  delete: string;
  entityType: IssueBindableEntity;
  oslc: boolean;
  panelStyle: string;
  projectId: number;
  projectName: string;
  hasError: boolean;
  error: any;
  modelLoaded: boolean;
  bugTrackerMode: BugTrackerMode;
}
