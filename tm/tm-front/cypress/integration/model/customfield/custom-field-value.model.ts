// custom field values from server
// with a nice untyped string even for multivalue field...
export interface CustomFieldValueModel {
  id: number;
  value: string;
  cufId: number;
  fieldType: 'CF' | 'RTF' | 'TAG' | 'NUM';
}

// custom field values in client app
// with some typing at least on TAGS...
export interface CustomFieldValue {
  id: number;
  value: string | string[];
  cufId: number;
  fieldType: 'CF' | 'RTF' | 'TAG' | 'NUM';
}
