const {getDatabaseInfos} = require("./database-info");
const {Client} = require('pg');

function executeQuery(query, cypressConfig) {
  const client = new Client(getDatabaseInfos(cypressConfig));
  client.connect();

  return client.query(query).finally(() => {
    client.end();
  });
}

module.exports = {
  executeQuery: executeQuery
};
