import {LoggerService} from './logger.service';
import {
  DebugLoggingLevel,
  ErrorLoggingLevel,
  InfoLoggingLevel,
  LoggingLevel,
  TraceLoggingLevel,
  WarningLoggingLevel
} from './logger.configuration';

export interface Logger {

  readonly namespace: string;

  readonly loggerService: LoggerService;

  trace(message: string, objects?: Object[]): void;

  debug(message: string, objects?: Object[]): void;

  info(message: string, objects?: Object[]): void;

  warning(message: string, objects?: Object[]): void;

  error(message: string, objects?: Object[]): void;

  isTraceEnabled(): boolean;

  isDebugEnabled(): boolean;

  isInfoEnabled(): boolean;

  isWarnEnabled(): boolean;

  isErrorEnabled(): boolean;

  compose(namespace: string): Logger;
}

export class SqtmLogger implements Logger {


  constructor(public loggerService: LoggerService, public readonly namespace: string) {
    // console.log('Instantiate Logger with ' + namespace + ' ' + loggerService);
  }

  trace(message: string, objects?: Object[]): void {
    this.loggerService.log(message, this.namespace, new TraceLoggingLevel(), objects);
  }

  debug(message: string, objects?: Object[]): void {
    this.loggerService.log(message, this.namespace, new DebugLoggingLevel(), objects);
  }

  info(message: string, objects?: Object[]): void {
    this.loggerService.log(message, this.namespace, new InfoLoggingLevel(), objects);
  }

  warning(message: string, objects?: Object[]): void {
    this.loggerService.log(message, this.namespace, new WarningLoggingLevel(), objects);
  }

  error(message: string, objects?: Object[]): void {
    this.loggerService.log(message, this.namespace, new ErrorLoggingLevel(), objects);
  }

  compose(namespace: string): Logger {
    const composedNamespace = `${this.namespace}.${namespace}`;
    return new SqtmLogger(this.loggerService, composedNamespace);
  }

  isTraceEnabled(): boolean {
    return this.loggerService.isEnabled(this.namespace, new TraceLoggingLevel());
  }


  isDebugEnabled(): boolean {
    return this.loggerService.isEnabled(this.namespace, new DebugLoggingLevel());
  }

  isInfoEnabled(): boolean {
    return this.loggerService.isEnabled(this.namespace, new InfoLoggingLevel());
  }

  isWarnEnabled(): boolean {
    return this.loggerService.isEnabled(this.namespace, new WarningLoggingLevel());
  }

  isErrorEnabled(): boolean {
    return this.loggerService.isEnabled(this.namespace, new ErrorLoggingLevel());
  }

  private isLevelEnabled(loggingLevel: LoggingLevel) {
    return this.loggerService.isEnabled(this.namespace, loggingLevel);
  }
}

export class NoopLogger implements Logger {

  public loggerService: LoggerService;

  constructor(public readonly namespace: string) {
  }

  debug(message: string): void {
  }

  error(message: string): void {
  }

  info(message: string): void {
  }

  trace(message: string): void {
  }

  warning(message: string): void {
  }

  compose(namespace: string): Logger {
    return new NoopLogger(namespace);
  }

  isTraceEnabled(): boolean {
    return false;
  }

  isDebugEnabled(): boolean {
    return false;
  }

  isErrorEnabled(): boolean {
    return false;
  }

  isInfoEnabled(): boolean {
    return false;
  }

  isWarnEnabled(): boolean {
    return false;
  }

}
