export class LoggingConfiguration {
  loggers: LoggerConfiguration;
  alwaysShowTrace?: boolean;
}

export class LoggerConfiguration {
  default?: LoggingLevelKey;
  namespaces?: { [K: string]: LoggingLevelKey };
}

export class NamespaceConfiguration {
  level: LoggingLevel;
}

export type LoggingLevelKey = 'none' | 'error' | 'warn' | 'info' | 'debug' | 'trace';

export type InternalLoggingLevelKey = 'unset' | LoggingLevelKey;

export interface LoggingLevel {
  level: number;
  name: InternalLoggingLevelKey;
}

export class UnsetLoggingLevel implements LoggingLevel {
  readonly level = -1;
  readonly name = 'unset';
}

export class NoneLoggingLevel implements LoggingLevel {
  readonly level = 1;
  readonly name = 'none';
}

export class ErrorLoggingLevel implements LoggingLevel {
  readonly level = 2;
  readonly name = 'error';
}

export class WarningLoggingLevel implements LoggingLevel {
  readonly level = 3;
  readonly name = 'warn';
}

export class InfoLoggingLevel implements LoggingLevel {
  readonly level = 4;
  readonly name = 'info';
}

export class DebugLoggingLevel implements LoggingLevel {
  readonly level = 5;
  readonly name = 'debug';
}

export class TraceLoggingLevel implements LoggingLevel {
  readonly level = 6;
  readonly name = 'trace';
}

export function getLoggingLevel(key: LoggingLevelKey): LoggingLevel {
  switch (key) {
    case 'none':
      return new NoneLoggingLevel();
    case 'error':
      return new ErrorLoggingLevel();
    case 'warn':
      return new WarningLoggingLevel();
    case 'info':
      return new InfoLoggingLevel();
    case 'debug':
      return new DebugLoggingLevel();
    case 'trace':
      return new TraceLoggingLevel();
    default:
      throw Error(`Unknown logging level key : ${key}`);
  }
}

export function isUnset(loggingLevel: LoggingLevel): loggingLevel is UnsetLoggingLevel {
  return loggingLevel.name === 'unset';
}
