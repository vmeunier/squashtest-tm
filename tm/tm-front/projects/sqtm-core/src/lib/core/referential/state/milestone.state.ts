import {createEntityAdapter, EntityState} from '@ngrx/entity';
import {Milestone} from '../../../model/milestone/milestone.model';

export interface MilestoneState extends EntityState<Milestone> {

}

export type MilestoneStateReadOnly = Readonly<MilestoneState>;

export function initialMilestoneState(): MilestoneStateReadOnly {
  return {
    ids: [],
    entities: {}
  };
}

export const milestoneEntityAdapter = createEntityAdapter<Milestone>();

export const selectAllMilestone = milestoneEntityAdapter.getSelectors().selectAll;
