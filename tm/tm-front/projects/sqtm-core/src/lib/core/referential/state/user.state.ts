import {AuthenticatedUser} from '../../../model/user/authenticated-user.model';

export type UserState = AuthenticatedUser;

export type UserStateReadOnly = Readonly<UserState>;

export function initialUserState(): UserStateReadOnly {
  return {
    userId: null,
    username: null,
    admin: false,
    firstName: null,
    lastName: null,
    automationProgrammer: null,
    functionalTester: null,
    projectManager: null,
  };
}
