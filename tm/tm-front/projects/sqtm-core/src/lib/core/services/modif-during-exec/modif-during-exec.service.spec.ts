import {TestBed} from '@angular/core/testing';

import {ModifDuringExecService} from './modif-during-exec.service';
import {TestingUtilsModule} from '../../../ui/testing-utils/testing-utils.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('ModifDuringExecService', () => {
  let service: ModifDuringExecService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestingUtilsModule, HttpClientTestingModule]
    });
    service = TestBed.inject(ModifDuringExecService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
