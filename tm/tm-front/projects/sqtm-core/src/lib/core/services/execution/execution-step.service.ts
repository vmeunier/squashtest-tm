import {Injectable} from '@angular/core';
import {RestService} from '../rest.service';
import {Observable} from 'rxjs';
import {ExecutionStatusKeys} from '../../../model/level-enums/level-enum';

@Injectable({
  providedIn: 'root'
})
export class ExecutionStepService {

  constructor(private restService: RestService) {
  }

  changeStatus(executionStepId: number, executionStatus: ExecutionStatusKeys, executionId: number): Observable<UpdateStatusResponse> {
    return this.restService.post<UpdateStatusResponse>(['execution-step', executionStepId.toString(), 'status'],
                                       {executionStatus},
                                      {params: {'executionId': executionId.toString()}});
  }

  changeComment(executionStepId: number, comment: string): Observable<void> {
    return this.restService.post<void>(['execution-step', executionStepId.toString(), 'comment'], {comment});
  }
}

export interface UpdateStatusResponse {
  executionStatus: ExecutionStatusKeys;
  lastExecutedOn: Date;
  lastExecutedBy: string;
}
