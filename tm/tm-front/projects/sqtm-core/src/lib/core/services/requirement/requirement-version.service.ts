import {Injectable} from '@angular/core';
import {RestService} from '../rest.service';
import {RequirementVersionModel} from '../../../model/requirement/requirement-version.model';
import {Observable} from 'rxjs';
import {Milestone} from '../../../model/milestone/milestone.model';
import {
  ChangeLinkedRequirementOperationReport,
  ChangeVerifyingTestCaseOperationReport
} from '../../../model/change-coverage-operation-report';
import {RequirementVersionLinkPatch} from '../../../model/requirement/requirement-version-link-patch.model';
import {RequirementVersionLink} from '../../../model/requirement/requirement-version.link';
import {NewReqVersionParams} from '../../../model/requirement/new-req-version-params.model';

@Injectable({
  providedIn: 'root'
})
export class RequirementVersionService {

  constructor(private restService: RestService) {

  }

  loadCurrentVersion(requirementId: number): Observable<RequirementVersionModel> {
    return this.restService.get<RequirementVersionModel>(['requirement-view', 'current-version', requirementId.toString()]);
  }

  loadVersion(requirementVersionId: number): Observable<RequirementVersionModel> {
    return this.restService.get<RequirementVersionModel>(['requirement-view', requirementVersionId.toString()]);
  }

  findAssociableMilestones(requirementVersionId: number): Observable<number[]> {
    return this.restService.get<number[]>(['requirement-version', requirementVersionId.toString(), 'associable-milestones']);
  }

  persistVerifyingTestCases(requirementVersionId: number, testCaseIds: number[]): Observable<ChangeVerifyingTestCaseOperationReport> {
    return this.restService.post(['requirement-version', requirementVersionId.toString(),
      'verifying-test-cases'], {testCaseIds});
  }

  removeVerifyingTestCases(requirementVersionId: number,
                           verifyingTestCaseIds: number[]): Observable<ChangeVerifyingTestCaseOperationReport> {
    return this.restService.delete(['requirement-version', requirementVersionId.toString(),
      'verifying-test-cases', verifyingTestCaseIds.toString()]);
  }

  removeRequirementLinks(requirementVersionId: number, requirementLinkIds: number[]): Observable<ChangeLinkedRequirementOperationReport> {
    return this.restService.delete(['requirement-version', requirementVersionId.toString(),
      'linked-requirement-versions', requirementLinkIds.toString()]);
  }

  bindMilestonesToRequirementVersion(requirementVersionId: number, milestoneIds: number[]): Observable<Milestone[]> {
    return this.restService.post<Milestone[]>(['requirement-version', requirementVersionId.toString(),
      'milestones', milestoneIds.toString()]);
  }

  updateStatus(requirementVersionId: number, status: string): Observable<any> {
    return this.restService.post(['requirement-version', requirementVersionId.toString(), 'status'], {status: status});
  }

  unbindMilestoneToRequirementVersion(requirementVersionId: number, milestoneId: number): Observable<Milestone[]> {
    return this.restService.delete<Milestone[]>(['requirement-version', requirementVersionId.toString(),
      'milestones', milestoneId.toString()]);
  }

  persistRequirementVersionLinks(requirementVersionId: number, requirementVersionLinkPatch: RequirementVersionLinkPatch):
    Observable<ChangeLinkedRequirementOperationReport> {
    return this.restService.post(['requirement-version', requirementVersionId.toString(),
      'linked-requirement-versions'], {...requirementVersionLinkPatch});
  }

  updateRequirementVersionLinkType(requirementVersionId: number, requirementVersionLinkPatch: RequirementVersionLinkPatch):
    Observable<RequirementVersionLink[]> {
    return this.restService.post(['requirement-version', requirementVersionId.toString(),
      'linked-requirement-versions', 'update'], {...requirementVersionLinkPatch});
  }

  findRequirementVersionLinkNames(requirementVersionId: number, relatedIds: number[]): Observable<any> {
    return this.restService.get(['requirement-version', requirementVersionId.toString(),
      'linked-requirement-versions', relatedIds.toString()]);
  }

  createNewRequirementVersion(requirementId: number, params: NewReqVersionParams): Observable<RequirementVersionModel> {
    return this.restService.post(['requirement-view', requirementId.toString(), 'new'], params);
  }
}
