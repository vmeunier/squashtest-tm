import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {RestService} from './rest.service';
import {RawValueMap} from '../../model/customfield/customfield.model';

export interface EntityFormModel {
  name: string;
  reference: string;
  description: string;
  parentEntityReference: string;
  customFields: RawValueMap;
}

@Injectable({
  providedIn: 'root'
})
export class EntityCreationService {

  constructor(private restService: RestService) {
  }

  persist(entityModel: Partial<EntityFormModel>, postUrl: string): Observable<any> {
    return this.restService.post([postUrl], entityModel);
  }
}
