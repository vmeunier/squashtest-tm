import {Injectable} from '@angular/core';
import {AttachmentService} from '../attachment.service';
import {Observable} from 'rxjs';
import {ReferentialDataService} from '../../referential/services/referential-data.service';
import {GlobalConfiguration} from '../../referential/state/global-configuration.state';
import {TranslateService} from '@ngx-translate/core';
import {AdminReferentialDataService} from '../../referential/services/admin-referential-data.service';
import {EntityViewAttachmentHelperService} from '../entity-view/entity-view-attachment-helper.service';

@Injectable({
  providedIn: 'root'
})
export class AdminViewAttachmentHelperService extends EntityViewAttachmentHelperService {

  constructor(attachmentService: AttachmentService,
              referentialDataService: ReferentialDataService,
              private adminReferentialDataService: AdminReferentialDataService,
              translateService: TranslateService) {
    super(attachmentService, referentialDataService, translateService);
  }

  protected getGlobalConfiguration(): Observable<GlobalConfiguration> {
    return this.adminReferentialDataService.globalConfiguration$;
  }
}
