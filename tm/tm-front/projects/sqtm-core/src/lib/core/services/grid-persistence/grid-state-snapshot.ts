import {createEntityAdapter, Dictionary, EntityState} from '@ngrx/entity';
import {createFeatureSelector, createSelector} from '@ngrx/store';
import {Identifier} from '../../../model/entity.model';
import {SortedColumn} from '../../../ui/grid/model/column-definition.model';
import {FilterValue} from '../../../ui/filters/state/filter.state';

export type GridPersistenceLevel = 'localStorage' | 'currentJsContext';

export interface GridFilterSnapshot {
  id: Identifier;
  value: FilterValue;
}

export interface GridStateSnapshot {
  id: string;
  selectedRowIds: Identifier[];
  openedRows: Identifier[];
  sortedColumns: SortedColumn[];
  pagination?: {
    page?: number;
    size?: number;
  };
  filters: GridFilterSnapshot[];
  // this boolean is used to mark grid not existing in local storage.
  // subscribers to state can handle it as they need, aka with default values
  noSnapshotExisting?: boolean;
  persistenceLevel: GridPersistenceLevel;
}

export const gridStateSnapshotAdapter = createEntityAdapter<GridStateSnapshot>();

export interface GridPersistenceState {
  grids: EntityState<GridStateSnapshot>;
}

export function getEmptyState(): Readonly<GridPersistenceState> {
  return {grids: gridStateSnapshotAdapter.getInitialState()};
}

const selectGridsState = createFeatureSelector<GridPersistenceState, EntityState<GridStateSnapshot>>('grids');

const selectGridsEntities = createSelector(
  selectGridsState,
  gridStateSnapshotAdapter.getSelectors().selectEntities);

function extractGridPersistenceSnapshot(snapshots: Dictionary<GridStateSnapshot>, persistenceKey: string) {
  return (snapshots[persistenceKey]
    || {
      id: persistenceKey,
      selectedRowIds: [],
      openedRows: [],
      sortedColumns: [],
      noSnapshotExisting: true,
      filters: [],
      persistenceLevel: 'currentJsContext' as GridPersistenceLevel
    });
}

export function selectGridSnapshot(persistenceKey: string) {
  return createSelector(
    selectGridsEntities, (snapshots) => extractGridPersistenceSnapshot(snapshots, persistenceKey));
}
