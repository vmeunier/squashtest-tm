import {Injectable} from '@angular/core';
import {RestService} from '../rest.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PartyPreferencesService {

  constructor(private restService: RestService) {
  }

  changeRequirementWorkspaceFavoriteDashboard(preferenceValue: FavoriteDashboardValue): Observable<void> {
    return this.changeFavoriteDashboard('squash.core.dashboard.content.requirement', preferenceValue);
  }

  changeTestCaseWorkspaceFavoriteDashboard(preferenceValue: FavoriteDashboardValue): Observable<void> {
    return this.changeFavoriteDashboard('squash.core.dashboard.content.tc', preferenceValue);
  }

  changeCampaignWorkspaceFavoriteDashboard(preferenceValue: FavoriteDashboardValue): Observable<void> {
    return this.changeFavoriteDashboard('squash.core.dashboard.content.campaign', preferenceValue);
  }

  changeHomeWorkspaceFavoriteDashboard(preferenceValue: FavoriteDashboardValue): Observable<void> {
    return this.changeFavoriteDashboard('squash.core.dashboard.content.home', preferenceValue);
  }

  private changeFavoriteDashboard(key: string, preferenceValue: FavoriteDashboardValue): Observable<void> {
    return this.restService.post(['user-prefs', 'update'], {
      'key': key,
      'value': preferenceValue
    });
  }
}

export type FavoriteDashboardValue = 'dashboard' | 'default';
