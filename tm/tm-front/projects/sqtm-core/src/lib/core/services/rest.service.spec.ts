import {TestBed} from '@angular/core/testing';

import {RestService} from './rest.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {BACKEND_CONTEXT_PATH, CORE_MODULE_CONFIGURATION} from '../sqtm-core.tokens';
import {defaultSqtmConfiguration} from '../sqtm-core.module';
import {APP_BASE_HREF} from '@angular/common';

describe('RestService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [
      {
        provide: CORE_MODULE_CONFIGURATION,
        useValue: defaultSqtmConfiguration
      },
      {
        provide: APP_BASE_HREF,
        useValue: ''
      },
      {
        provide: BACKEND_CONTEXT_PATH,
        useValue: ''
      }
    ]
  }));

  it('should be created', () => {
    const service: RestService = TestBed.inject(RestService);
    expect(service).toBeTruthy();
  });
});
