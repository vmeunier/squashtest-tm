import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {LocalPersistenceService} from './local-persistence.service';
import {CookieService} from 'ngx-cookie-service';
import {tap} from 'rxjs/operators';
import {MilestoneStateSnapshot} from '../referential/state/milestone-filter.state';
import {coreLogger} from '../core.logger';

const logger = coreLogger.compose('MilestoneLocalPersistenceService');

@Injectable({
  providedIn: 'root'
})
export class MilestoneLocalPersistenceService {

  private readonly milestoneCookieName = 'milestones';
  private readonly milestoneCookiePath = '/';
  private readonly localStorageKey = 'milestone-mode';

  constructor(private localPersistenceService: LocalPersistenceService, private cookieService: CookieService) {
  }

  persistMilestoneMode(stateSnapshot: MilestoneStateSnapshot): Observable<any> {
    return this.localPersistenceService.set(this.localStorageKey, stateSnapshot).pipe(
      tap(() => {
        this.setCookie(stateSnapshot);
      })
    );
  }

  private setCookie(stateSnapshot: MilestoneStateSnapshot) {
    if (stateSnapshot.milestoneModeEnabled) {
      this.cookieService.set(this.milestoneCookieName, stateSnapshot.selectedMilestoneId.toString(), 365, this.milestoneCookiePath);
    } else {
      this.deleteCookie();
    }
  }

  private deleteCookie() {
    this.cookieService.delete(this.milestoneCookieName, this.milestoneCookiePath);
  }

  restoreMilestoneMode(): Observable<MilestoneStateSnapshot> {
    return this.localPersistenceService.get<MilestoneStateSnapshot>(this.localStorageKey).pipe(
      tap(snapshot => {
        logger.debug('snapshot');
        logger.debug(JSON.stringify(snapshot));
        if (Boolean(snapshot)) {
          this.setCookie(snapshot);
        } else {
          this.deleteCookie();
        }
      })
    );
  }


}
