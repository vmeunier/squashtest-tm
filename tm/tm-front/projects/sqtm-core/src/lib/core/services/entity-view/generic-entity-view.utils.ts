import {GenericEntityViewState, SqtmGenericEntityState} from '../genric-entity-view/generic-entity-view-state';

export function updateGenericEntityInViewState<S extends SqtmGenericEntityState, T extends string>
(entity: S, state: GenericEntityViewState<S, T>)
  : GenericEntityViewState<S, T> {
  const nextState: GenericEntityViewState<S, T> = {...state};
  const update: any = {};
  update[state.type] = {...entity};
  Object.assign(nextState, update);
  return nextState;
}
