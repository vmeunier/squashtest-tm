export interface ProjectPlugin {
  index: number;
  id: string;
  enabled: boolean;
  type: string;
  name: string;
  status: string;
  configUrl: string;
  hasConf: boolean;
}

export enum PluginType {
  AUTOMATION = 'AUTOMATION'
}

export enum PluginId {
  WORKFLOW_AUTOMATION_JIRA = 'henix.plugin.automation.workflow.automjira',
  CAMPAIGN_ASSISTANT = 'squash.tm.wizard.campaignassistant',
  RESULT_PUBLISHER = 'squash.tm.plugin.resultpublisher',
}

export enum AutomationWorkflowTypes {
  NONE = 'NONE',
  NATIVE = 'NATIVE',
  REMOTE_WORKFLOW = 'REMOTE_WORKFLOW'
}
