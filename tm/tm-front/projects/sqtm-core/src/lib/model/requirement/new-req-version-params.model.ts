export interface NewReqVersionParams {
  inheritReqLinks: boolean;
  inheritTestCases: boolean;
}
