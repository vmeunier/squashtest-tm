import {AuthenticationPolicy, AuthenticationProtocol} from '../third-party-server/authentication.model';

export interface BugTracker {
  id: number;
  name: string;
  url: string;
  kind: string;
  authPolicy: AuthenticationPolicy;
  authProtocol: AuthenticationProtocol;
  iframeFriendly: boolean;
}

export type BugTrackerMode = 'Automatic' | 'Manual';
