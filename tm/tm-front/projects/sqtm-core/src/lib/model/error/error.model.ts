import {HttpErrorResponse} from '@angular/common/http';

export type ErrorKind =
  'ACTION_ERROR' |
  'FIELD_VALIDATION_ERROR';

export interface SquashError {
  kind: ErrorKind;
}

export interface SquashActionError extends SquashError {
  kind: 'ACTION_ERROR';
  actionValidationError: ActionValidationError;
}

export interface SquashFieldError extends SquashError {
  kind: 'FIELD_VALIDATION_ERROR';
  fieldValidationErrors: FieldValidationError[];
}

export interface ActionValidationError {
  exception: string;
  i18nKey: string;
  i18nParams: string[];
}

export interface FieldValidationError {
  objectName: string;
  fieldName: string;
  fieldValue: string;
  i18nKey: string;
  messageArgs: string[];
}

export function extractSquashActionError(err: any): SquashActionError {
  return (err as HttpErrorResponse).error.squashTMError as SquashActionError;
}

export function extractSquashFirstFieldError(err: any): SquashFieldError {
  return (err as HttpErrorResponse).error.squashTMError as SquashFieldError;
}


export function doesHttpErrorContainsSquashActionError(err: any): boolean {
  const isSquashError = doesHttpErrorContainsSquashError(err);
  if (isSquashError) {
    const squashError: SquashError = err.error.squashTMError;
    return squashError.kind === 'ACTION_ERROR';
  } else {
    return false;
  }
}

export function doesHttpErrorContainsSquashFieldError(err: any): boolean {
  const isSquashError = doesHttpErrorContainsSquashError(err);
  if (isSquashError) {
    const squashError: SquashError = err.error.squashTMError;
    return squashError.kind === 'FIELD_VALIDATION_ERROR';
  } else {
    return false;
  }
}


function doesHttpErrorContainsSquashError(err: any): boolean {
  return err instanceof HttpErrorResponse && err.status === 412 && Boolean(err.error.squashTMError);
}
