import {IssueBindableEntity} from './issue-bindable-entity.model';
import {HttpErrorResponse} from '@angular/common/http';
import {BugTrackerMode} from '../bugtracker/bug-tracker.model';

export class IssueUIModel {
  bugTrackerStatus: string;
  delete: string;
  entityType: IssueBindableEntity;
  oslc: boolean;
  panelStyle: string;
  projectId: number;
  projectName: string;
  hasError: boolean;
  error: HttpErrorResponse;
  modelLoaded: boolean;
  bugTrackerMode: BugTrackerMode;
  activated: boolean;
}
