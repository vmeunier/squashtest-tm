import {EntityModel} from '../../../core/services/entity-view/entity-view.state';

export interface CustomReportFolderModel extends EntityModel {
  name: string;
  description: string;
}

