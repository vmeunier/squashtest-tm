import {EntityModel} from '../../../core/services/entity-view/entity-view.state';

export interface CustomReportLibraryModel extends EntityModel {
  name: string;
  description: string;
}

