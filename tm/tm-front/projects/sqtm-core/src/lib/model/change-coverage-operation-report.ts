import {RequirementVersionCoverage} from './test-case/requirement-version-coverage-model';
import {VerifyingTestCase} from './requirement/verifying-test-case';
import {RequirementVersionLink} from './requirement/requirement-version.link';

export interface ChangeOperationReport {
  summary: SummaryExceptions;
}

export interface ChangeCoverageOperationReport extends ChangeOperationReport {
  coverages: RequirementVersionCoverage[];
}

export interface ChangeVerifyingTestCaseOperationReport extends ChangeOperationReport {
  verifyingTestCases: VerifyingTestCase[];
}

export interface ChangeLinkedRequirementOperationReport {
  summary: RequirementLinksSummaryExceptions;
  requirementVersionLinks: RequirementVersionLink[];
}

export class SummaryExceptions {
  alreadyVerifiedRejections: any;
  notLinkableRejections: any;
  noVerifiableVersionRejections: any;
}

export class RequirementLinksSummaryExceptions {
  notLinkableRejections: boolean;
  alreadyLinkedRejections: boolean;
  sameRequirementRejections: boolean;
}
