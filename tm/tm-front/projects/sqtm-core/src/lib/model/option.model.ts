export class Option {
  label: string;
  value: string;
  icon?: string;
  hide?: boolean;
}
