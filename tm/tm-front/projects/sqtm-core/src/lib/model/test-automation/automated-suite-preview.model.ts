import { EntityReference } from '../entity.model';

export interface AutomatedSuitePreview {
  isManualSlaveSelection: boolean;
  specification: AutomatedSuiteCreationSpecification;
  projects: TestAutomationProjectPreview[];
}

export interface AutomatedSuiteCreationSpecification {
  context: EntityReference;
  testPlanSubsetIds: number[];
  executionConfigurations?: AutomatedSuiteExecutionConfiguration[];
}

export interface TestAutomationProjectPreview {
  projectId: number;
  label: string;
  server: string;
  nodes: string[];
  testCount: number;
}

export interface AutomatedSuiteExecutionConfiguration {
  projectId: number;
  node: string;
}
