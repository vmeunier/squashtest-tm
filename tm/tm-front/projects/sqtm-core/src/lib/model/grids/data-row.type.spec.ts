import {DataRow} from '../../ui/grid/model/data-row.model';
import {convertSqtmLiterals, SquashTmDataRowType} from './data-row.type';
import {ProjectDataMap} from '../project/project-data.model';
import {Permissions} from '../permissions/permissions.model';

describe('DataRowType', () => {
  const projectDataMap: ProjectDataMap = {
    1: {
      id: 1,
      name: 'Project 1',
      uri: '',
      label: '',
      testCaseNature: null,
      testCaseType: null,
      requirementCategory: null,
      allowAutomationWorkflow: false,
      customFieldBinding: null,
      permissions: {
        TEST_CASE_LIBRARY: [Permissions.READ, Permissions.WRITE, Permissions.CREATE, Permissions.ATTACH],
        CAMPAIGN_LIBRARY: [],
        REQUIREMENT_LIBRARY: [],
        PROJECT: [],
        PROJECT_TEMPLATE: [],
        CUSTOM_REPORT_LIBRARY: [],
        AUTOMATION_REQUEST_LIBRARY: [],
        ACTION_WORD_LIBRARY: []
      },
      bugTracker: null,
      milestones: [],
      taServer: null,
      automationWorkflowType: 'NATIVE',
      disabledExecutionStatus: [],
      keywords: [],
      bddScriptLanguage: 'ENGLISH',
      allowTcModifDuringExec: false,
      activatedPlugins: null
    }
  };

  const partialDataRows: Partial<DataRow>[] = [
    {
      id: 'TestCaseLibrary-1',
      type: 'TestCaseLibrary',
      projectId: 1,
      data: []
    },
    {
      id: 'TestCaseFolder-3',
      type: 'TestCaseFolder',
      projectId: 1,
      data: []
    },
    {
      id: 'TestCase-2',
      type: 'TestCase',
      projectId: 1,
      data: []
    }
  ];

  it('should convert data row literals to SquashTmDataRow', () => {
    const dataRows = convertSqtmLiterals(partialDataRows, projectDataMap);

    const libraryRow = dataRows[0];
    expect(libraryRow.type).toBe(SquashTmDataRowType.TestCaseLibrary);
    expect(libraryRow.simplePermissions.canWrite).toBeTruthy();
    expect(libraryRow.simplePermissions.canCreate).toBeTruthy();
    expect(libraryRow.simplePermissions.canRead).toBeTruthy();
    expect(libraryRow.simplePermissions.canDelete).toBeFalsy();

    const folderRow = dataRows[1];
    expect(folderRow.type).toBe(SquashTmDataRowType.TestCaseFolder);
    expect(folderRow.simplePermissions.canWrite).toBeTruthy();
    expect(folderRow.simplePermissions.canCreate).toBeTruthy();
    expect(folderRow.simplePermissions.canRead).toBeTruthy();
    expect(folderRow.simplePermissions.canDelete).toBeFalsy();

    const testCase = dataRows[2];
    expect(testCase.type).toBe(SquashTmDataRowType.TestCase);
    expect(testCase.simplePermissions.canWrite).toBeTruthy();
    expect(testCase.simplePermissions.canCreate).toBeTruthy();
    expect(testCase.simplePermissions.canRead).toBeTruthy();
    expect(testCase.simplePermissions.canDelete).toBeFalsy();
  });
});
