import {EntityModel} from '../../../core/services/entity-view/entity-view.state';

export interface CampaignLibraryModel extends EntityModel {
  name: string;
  description: string;
}
