import {EntityModel} from '../../../core/services/entity-view/entity-view.state';
import {CustomDashboardModel} from '../../custom-report/custom-dashboard.model';
import {CampaignProgressionStatistics, ConclusivenessStatusCount, ExecutionStatusCount} from '../campaign-model';
import {TestCaseImportanceKeys} from '../../level-enums/level-enum';

export interface CampaignFolderModel extends EntityModel {
  name: string;
  description: string;
  nbIssues: number;
  shouldShowFavoriteDashboard: boolean;
  canShowFavoriteDashboard: boolean;
  favoriteDashboardId: number;
  dashboard?: CustomDashboardModel;
}

export interface CampaignTestInventoryStatistics {
  campaignName: string;
  statistics: ExecutionStatusCount;

}

export interface CampaignFolderStatisticsBundle {
  campaignProgressionStatistics: CampaignProgressionStatistics;
  campaignTestCaseStatusStatistics: Partial<ExecutionStatusCount>;
  campaignTestCaseSuccessRateStatistics: { conclusiveness: { [K in TestCaseImportanceKeys]: ConclusivenessStatusCount } };
  campaignNonExecutedTestCaseImportanceStatistics: { [K in TestCaseImportanceKeys]: number };
  campaignTestInventoryStatisticsList: CampaignTestInventoryStatistics[];
}
