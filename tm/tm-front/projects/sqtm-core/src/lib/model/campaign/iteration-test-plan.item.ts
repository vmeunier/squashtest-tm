export interface IterationTestPlanItem {
  id: number;
  label: string;
}
