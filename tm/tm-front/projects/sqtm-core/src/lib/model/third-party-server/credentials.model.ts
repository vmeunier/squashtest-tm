import {AuthenticationProtocol} from './authentication.model';

export interface BasicAuthCredentials {
  implementedProtocol: AuthenticationProtocol.BASIC_AUTH;
  type: AuthenticationProtocol.BASIC_AUTH;
  username: string;
  password?: string;
  registered?: boolean;
}

export interface OAuthCredentials {
  implementedProtocol: AuthenticationProtocol.OAUTH_1A;
  type: AuthenticationProtocol.OAUTH_1A;
  token: string;
  tokenSecret?: string;
  registered?: boolean;
}

export interface TokenAuthCredentials {
  implementedProtocol: AuthenticationProtocol.TOKEN_AUTH;
  type: AuthenticationProtocol.TOKEN_AUTH;
  token: string;
  registered?: boolean;
}

export type Credentials = BasicAuthCredentials | OAuthCredentials | TokenAuthCredentials;

export function isOAuth1aCredentials(credentials: any): credentials is OAuthCredentials {
  return credentials?.implementedProtocol === AuthenticationProtocol.OAUTH_1A;
}

export function isBasicAuthCredentials(credentials: any): credentials is BasicAuthCredentials {
  return credentials?.implementedProtocol === AuthenticationProtocol.BASIC_AUTH;
}

export function isTokenAuthCredentials(credentials: any): credentials is TokenAuthCredentials {
  return credentials?.implementedProtocol === AuthenticationProtocol.TOKEN_AUTH;
}
