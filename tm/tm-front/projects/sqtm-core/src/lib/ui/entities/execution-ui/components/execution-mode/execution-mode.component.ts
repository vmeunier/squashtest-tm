import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {LevelEnumItem, TestCaseExecutionModeKeys} from '../../../../../model/level-enums/level-enum';

@Component({
  selector: 'sqtm-core-execution-mode',
  template: `
    <div *ngIf="executionMode; else noData"
         nz-tooltip [nzTooltipTitle]="executionMode.i18nKey | translate"
         [ngSwitch]="executionMode.id">
      <i nz-icon *ngSwitchCase="'AUTOMATED'" nzType="sqtm-core-nav:automation-workspace" nzTheme="outline"></i>
      <i nz-icon *ngSwitchCase="'MANUAL'" nzType="sqtm-core-administration:user" nzTheme="outline"></i>
    </div>
    <ng-template #noData>
      <span>-</span>
    </ng-template>
  `,
  styleUrls: ['./execution-mode.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExecutionModeComponent implements OnInit {

  @Input()
  executionMode: LevelEnumItem<TestCaseExecutionModeKeys>;

  constructor() {
  }

  ngOnInit() {
  }
}
