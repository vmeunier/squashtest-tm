
export class DragAndDropData {
  constructor(public readonly origin: string, public data?: any) {
  }
}
