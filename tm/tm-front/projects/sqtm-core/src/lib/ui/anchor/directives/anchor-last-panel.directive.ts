import {AfterViewInit, Directive, ElementRef, Host, Input, OnInit, Renderer2} from '@angular/core';
import {AnchorResizeDirective} from './anchor-resize.directive';

@Directive({
  selector: '[sqtmCoreAnchorLastPanel]'
})
export class AnchorLastPanelDirective implements OnInit, AfterViewInit {

  @Input()
  isEffectiveLastPanel = true;

  constructor(@Host() private elementRef: ElementRef, private anchorResizeDirective: AnchorResizeDirective, private render: Renderer2) {
  }

  ngOnInit(): void {
    if (this.isEffectiveLastPanel) {
      this.render.setStyle(this.elementRef.nativeElement, 'margin-bottom', '0');
    }

  }

  ngAfterViewInit(): void {
    if (this.isEffectiveLastPanel) {
      this.anchorResizeDirective.registerLastElement(this.elementRef);
    }
  }
}
