import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnDestroy,
  OnInit
} from '@angular/core';
import {AnchorService} from '../../service/anchor.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {ViewIdDirective} from '../../directives/view-id.directive';

@Component({
  selector: 'sqtm-core-anchor-group',
  templateUrl: './anchor-group.component.html',
  styleUrls: ['./anchor-group.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AnchorGroupComponent implements OnInit, AfterViewInit, OnDestroy {

  private unsub$ = new Subject<void>();

  @Input()
  id: string;

  selected = false;

  constructor(private cdr: ChangeDetectorRef, private anchorService: AnchorService, private viewIdDirective: ViewIdDirective) {
  }

  ngOnInit() {

  }

  ngAfterViewInit(): void {
    this.anchorService.getActiveGroup(this.viewIdDirective.id).pipe(takeUntil(this.unsub$)).subscribe(activeGroup => {
      this.selected = this.id === activeGroup;
      this.cdr.detectChanges();
    });
  }


  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

}
