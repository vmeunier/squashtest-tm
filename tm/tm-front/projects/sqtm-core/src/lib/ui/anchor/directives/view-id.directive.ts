import {Directive, Input} from '@angular/core';

@Directive({
  selector: '[sqtmCoreViewId]'
})
export class ViewIdDirective {

  @Input('sqtmCoreViewId')
  id: string;

  constructor() {
  }

}
