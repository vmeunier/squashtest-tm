import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AnchorService} from '../../service/anchor.service';
import {AnchorGroupComponent} from '../anchor-group/anchor-group.component';
import {filter, take, takeUntil} from 'rxjs/operators';
import {AnchorsState} from '../../service/anchors.state';
import {ActivatedRoute, Router} from '@angular/router';
import {anchorLogger} from '../../anchor.logger';
import {ViewIdDirective} from '../../directives/view-id.directive';
import {Subject} from 'rxjs';

const logger = anchorLogger.compose('AnchorLinkComponent');

@Component({
  selector: 'sqtm-core-anchor-link',
  templateUrl: './anchor-link.component.html',
  styleUrls: ['./anchor-link.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AnchorLinkComponent implements OnInit, OnDestroy {

  private unsub$ = new Subject<void>();

  @Input()
  linkHref;

  @Input()
  iconName;

  @Input()
  id: string;

  active = false;

  constructor(public cdr: ChangeDetectorRef, private router: Router, private activeRoute: ActivatedRoute,
              private anchorService: AnchorService, private viewIdDirective: ViewIdDirective, private anchorGroup: AnchorGroupComponent) {
  }

  ngOnInit() {
    this.anchorService.registerLink(this.viewIdDirective.id, {group: this.anchorGroup.id, id: this.id, active: true});
    this.anchorService.state$.pipe(
      take(1),
      filter((state: AnchorsState) => {
        const anchorState = state.entities[this.viewIdDirective.id];
        let filterAnchor = false;
        if (Boolean(anchorState)) {
          filterAnchor = this.id === anchorState.selectedLinkId;
        }
        return filterAnchor;
      })).subscribe(() => this.restoreActiveLink());

    this.anchorService.getActiveLink(this.viewIdDirective.id).pipe(takeUntil(this.unsub$)).subscribe(activeLinkId => {
      this.active = activeLinkId === this.id;
      this.cdr.detectChanges();
    });
  }

  makeActive() {
    if (logger.isDebugEnabled()) {
      logger.debug(`Activate Link: ViewId:${this.viewIdDirective.id} GroupId:${this.anchorGroup.id} LinkId:${this.id}`);
    }
    this.anchorService.notifyAndNavigateToActiveLink(this.viewIdDirective.id, this.id);
    this.navigateToLink();
  }

  restoreActiveLink() {
    if (logger.isDebugEnabled()) {
      logger.debug(`Restore Active Link: ViewId:${this.viewIdDirective.id} GroupId:${this.anchorGroup.id} LinkId:${this.id}`);
    }
    this.anchorService.notifyRestoredLink(this.viewIdDirective.id, this.id);
    this.navigateToLink();
  }

  navigateToLink() {
    if (this.linkHref) {
      this.router.navigate([this.linkHref], {relativeTo: this.activeRoute, skipLocationChange: true});
    }

  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

}
