import {TestBed} from '@angular/core/testing';

import {AnchorService, registerLinkState} from './anchor.service';
import {AnchorsState, initialAnchorsState, LinkState} from './anchors.state';

const informationLinkState: LinkState = {
  id: 'information',
  group: 'content',
  active: false
};

const stepsLinkState: LinkState = {
  id: 'steps',
  group: 'steps',
  active: true
};

const parametersLinkState: LinkState = {
  id: 'parameters',
  group: 'content',
  active: false
};

const anchorsState: AnchorsState = initialAnchorsState;

describe('AnchorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AnchorService = TestBed.inject(AnchorService);
    expect(service).toBeTruthy();
  });


  it('register new view in state', () => {

    expect(anchorsState.ids.length).toBe(0);
    const nextState = registerLinkState('test-case-view', informationLinkState, anchorsState);
    expect(nextState.ids.length).toBe(1);
    expect(nextState.ids[0]).toBe('test-case-view');
    expect(nextState.entities['test-case-view'].ids[0]).toBe('information');
  });

  it('update specific view in state', () => {
    let nextState = registerLinkState('test-case-view', informationLinkState, anchorsState);
    const firstView = nextState.ids[0];
    expect(nextState.ids.length).toBe(1);
    expect(firstView).toBe('test-case-view');
    let viewState = getViewState(nextState, firstView);
    expect(viewState.ids.length).toBe(1);
    nextState = registerLinkState('test-case-view', parametersLinkState, nextState);
    expect(nextState.ids.length).toBe(1);
    viewState = getViewState(nextState, firstView);
    expect(viewState.ids.length).toBe(2);
    nextState = registerLinkState('test-case-view', stepsLinkState, nextState);
    expect(nextState.ids.length).toBe(1);
    viewState = getViewState(nextState, firstView);
    expect(viewState.ids.length).toBe(3);
  });

  function getViewState(state, viewId) {
    return state.entities[viewId];
  }
});
