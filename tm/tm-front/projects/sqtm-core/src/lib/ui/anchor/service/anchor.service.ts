import {Injectable} from '@angular/core';
import {
  activeGroupSelector,
  activeLinkSelector,
  anchorEntityAdapter,
  anchorsEntityAdapter,
  AnchorsState,
  AnchorState,
  initialAnchorsState,
  initialAnchorState,
  LinkState,
  panelIsActiveSelector
} from './anchors.state';
import {createStore, Store} from '../../../core/store/store';
import {map, take, tap} from 'rxjs/operators';
import {select} from '@ngrx/store';
import {BehaviorSubject, Observable} from 'rxjs';
import {anchorLogger} from '../anchor.logger';

const logger = anchorLogger.compose('AnchorService');

@Injectable({
  providedIn: 'root'
})
export class AnchorService {

  private store: Store<AnchorsState> = createStore(initialAnchorsState, {
    id: 'AnchorStore',
    logDiff: 'detailed'
  });

  public state$ = this.store.state$;

  private _requestScrollSubject = new BehaviorSubject<Pick<AnchorState, 'viewId' | 'selectedLinkId'>>({
    viewId: '',
    selectedLinkId: ''
  });

  public requestScroll$ = this._requestScrollSubject.asObservable();

  constructor() {
  }

  registerLink(viewId: string, link: LinkState) {
    this.store.state$.pipe(
      take(1),
      map(state => {
        return registerLinkState(viewId, link, state);
      })
    ).subscribe(state => this.store.commit(state));
  }

  beginLoadPhase() {
    this.store.state$.pipe(
      take(1),
      map(state => {
        const nextState = {...state};
        nextState.loaded = false;
        return nextState;
      })
    ).subscribe(state => this.store.commit(state));
  }

  notifyRestoredLink(viewId: string, linkId: string) {
    this.store.state$.pipe(
      take(1),
      map(state => {
        const nextState = {...state};
        nextState.loaded = true;
        return nextState;
      }),
      map(state => anchorsEntityAdapter.updateOne({id: viewId, changes: {selectedLinkId: linkId}}, state)),
      tap(() => this._requestScrollSubject.next({viewId: viewId, selectedLinkId: linkId}))
    ).subscribe(state => this.store.commit(state));
  }

  notifyActiveLink(viewId: string, linkId: string) {
    this.store.state$.pipe(
      take(1),
      map(state => anchorsEntityAdapter.updateOne({id: viewId, changes: {selectedLinkId: linkId}}, state)),
    ).subscribe(state => this.store.commit(state));
  }

  notifyAndNavigateToActiveLink(viewId: string, linkId: string) {
    this.store.state$.pipe(
      take(1),
      map(state => anchorsEntityAdapter.updateOne({id: viewId, changes: {selectedLinkId: linkId}}, state)),
      // performing side effect by emitting into dedicated subject.
      tap(() => this._requestScrollSubject.next({viewId: viewId, selectedLinkId: linkId}))
    ).subscribe(state => this.store.commit(state));
  }

  notifyCollapsePanel(viewId: string, linkId: string, active: boolean) {
    this.store.state$.pipe(
      take(1),
      map(state => {
        let nextState = {...state};
        let anchorState = nextState.entities[viewId];
        anchorState = anchorEntityAdapter.updateOne({id: linkId, changes: {active: active}}, anchorState);
        nextState = anchorsEntityAdapter.upsertOne(anchorState, nextState);
        return nextState;
      })
    ).subscribe(state => this.store.commit(state));
  }

  getActiveLink(viewId: string): Observable<string> {
    return this.state$.pipe(select(activeLinkSelector(viewId)));
  }

  getActiveGroup(viewId: string): Observable<string> {
    return this.state$.pipe(select(activeGroupSelector(viewId)));
  }

  isActive(viewId: string, linkId: string): Observable<boolean> {
    return this.state$.pipe(select(panelIsActiveSelector(viewId, linkId)));
  }

}

export function registerLinkState(viewId: string, link: LinkState, state: AnchorsState): AnchorsState {
  let nextState = {...state};
  let anchorState = state.entities[viewId];
  if (!Boolean(anchorState)) {
    anchorState = {...initialAnchorState};
    anchorState.viewId = viewId;
    anchorState.selectedLinkId = link.id;
  }
  anchorState = anchorEntityAdapter.addOne(link, anchorState);
  nextState = anchorsEntityAdapter.upsertOne(anchorState, nextState);
  return nextState;
}
