import { async, ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {AnchorComponent} from './anchor.component';
import {ViewIdDirective} from '../directives/view-id.directive';
import {DefaultAnchorLinkDirective} from '../directives/default-anchor-link.directive';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AnchorService} from '../service/anchor.service';
import {Observable, of} from 'rxjs';
import {initialAnchorsState} from '../service/anchors.state';

describe('AnchorComponent', () => {
  let component: AnchorComponent;
  let fixture: ComponentFixture<AnchorComponent>;

  let anchorService = jasmine.createSpyObj('AnchorService', ['instant', 'beginLoadPhase']);

  anchorService = {
    ...anchorService,
    state$: of(initialAnchorsState)
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AnchorComponent, DefaultAnchorLinkDirective, ViewIdDirective],
      imports: [],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {provide: ViewIdDirective, useValue: {}},
        {provide: AnchorService, useValue: anchorService}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnchorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', waitForAsync(() => {
    expect(component).toBeTruthy();
  }));
});
