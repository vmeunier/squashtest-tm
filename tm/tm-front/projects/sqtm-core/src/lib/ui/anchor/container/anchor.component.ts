import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChild,
  Inject,
  OnDestroy,
  OnInit
} from '@angular/core';
import {Subject} from 'rxjs';
import {filter, take} from 'rxjs/operators';
import {DOCUMENT} from '@angular/common';
import {AnchorLinkComponent} from '../components/anchor-link/anchor-link.component';
import {AnchorService} from '../service/anchor.service';
import {DefaultAnchorLinkDirective} from '../directives/default-anchor-link.directive';
import {ViewIdDirective} from '../directives/view-id.directive';


@Component({
  selector: 'sqtm-core-anchor',
  templateUrl: './anchor.component.html',
  styleUrls: ['./anchor.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AnchorComponent implements OnInit, AfterViewInit, OnDestroy {

  unsub$ = new Subject<void>();

  @ContentChild(DefaultAnchorLinkDirective, { read: AnchorLinkComponent })
  defaultLink: AnchorLinkComponent;

  constructor(public cdr: ChangeDetectorRef,
              @Inject(DOCUMENT) private doc: any,
              private anchorService: AnchorService,
              private viewDirective: ViewIdDirective) {
  }

  ngOnInit(): void {
    this.anchorService.beginLoadPhase();
  }

  ngAfterViewInit(): void {
    this.anchorService.state$.pipe(
      take(1),
      filter((state) => !state.loaded)
    ).subscribe(() => {
      if (this.defaultLink) {
        this.defaultLink.restoreActiveLink();
      }
    });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}
