import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {WorkspaceWithTreeComponent} from './workspace-with-tree.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {RouterTestingModule} from '@angular/router/testing';
import {GridService} from '../../../grid/services/grid.service';
import {EMPTY} from 'rxjs';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('WorkspaceWithTreeComponent', () => {
  let component: WorkspaceWithTreeComponent;
  let fixture: ComponentFixture<WorkspaceWithTreeComponent>;

  const gridService = jasmine.createSpyObj(['load']);
  gridService.selectedRows$ = EMPTY;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule
      ],
      providers: [
        {
          provide: GridService,
          useValue: gridService
        }
      ],
      declarations: [WorkspaceWithTreeComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkspaceWithTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // this test make randomly crash the karma runner.
  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
