import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Input,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild
} from '@angular/core';
import {Subject} from 'rxjs';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {distinctUntilChanged, filter, map, pluck, take, takeUntil, withLatestFrom} from 'rxjs/operators';
import {ResizableDirective} from '../../../workspace-common/directives/resizable.directive';
import {GridService} from '../../../grid/services/grid.service';
import {UiManagerService} from '../../../ui-manager/ui-manager.service';
import {GlobalEventService} from '../../../../core/services/global-event.service';
import {LicenseInformationState} from '../../../../core/referential/state/license-information.state';
import {ResizableWorkspaceLayoutDelegate, WorkspaceLikeComponent} from '../../resizable-workspace-layout-delegate';
import {LicenseMessagePlacement} from '../../../workspace-common/components/license-information-banner/license-information.helpers';
import {LocalPersistenceService} from '../../../../core/services/local-persistence.service';
import {wsLayoutLogger} from '../../workspace.layout.logger';
import {GridState} from '../../../grid/model/state/grid.state';
import {Identifier} from '../../../../model/entity.model';

const logger = wsLayoutLogger.compose('WorkspaceWithGridComponent');

@Component({
  selector: 'sqtm-core-workspace-with-grid',
  templateUrl: './workspace-with-grid.component.html',
  styleUrls: ['./workspace-with-grid.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorkspaceWithGridComponent implements WorkspaceLikeComponent, OnInit, AfterViewInit, OnDestroy {

  // Position of the ID param in the URL. Must be set for workspaces with navigation to a contextual view.
  // If your URL is '/administration-workspace/projects/11/content', then the entity ID is at index 2.
  entityIdPositionInUrl: number = undefined;

  // We store the current viewed entities to avoid useless navigations and circular callback chains
  private currentViewedEntities: Identifier[] = [];

  @Input()
  name: string;

  @Input()
  rootUrl: string[];

  @Input()
  workspaceTheme = 'administration-workspace';

  @Input()
  disableNavigationToContextualContent = false;

  // NB: providing licenseInformation alone is NOT enough. You also has to give a placement and tell if
  // current user is an admin.
  @Input()
  licenseInformation: LicenseInformationState;

  // Context for the license banner. Defaults to ADMIN_WORKSPACE as this is where this component is mostly used.
  // We're passing a string only to avoid typing error in templates... The other way around would be to declare
  // a local reference to the enum in each component using the WorkspaceWithGrid (and they are a lot).
  @Input()
  set licenseBannerPlacement(placement: string) {
    if (Object.values(LicenseMessagePlacement).includes(placement as LicenseMessagePlacement)) {
      this._licenseBannerPlacement = placement as LicenseMessagePlacement;
    } else {
      throw new Error('Received invalid license message placement : ' + placement);
    }
  }

  get placement(): LicenseMessagePlacement {
    return this._licenseBannerPlacement;
  }

  private _licenseBannerPlacement = LicenseMessagePlacement.ADMIN_WORKSPACE;

  @Input()
  isLoggedAsAdmin: boolean;

  @ViewChild('grid', {read: ElementRef, static: true})
  gridReference: ElementRef;

  @ViewChild(ResizableDirective, {read: ElementRef, static: true})
  resizeHandle: ElementRef;

  @ViewChild('contextualContent', {read: ElementRef, static: true})
  contextualContent: ElementRef;

  @ViewChild('workspace', {read: ElementRef, static: true})
  workspace: ElementRef;

  public readonly unsub$ = new Subject<void>();

  private readonly layoutDelegate: ResizableWorkspaceLayoutDelegate;

  constructor(private router: Router,
              private activeRoute: ActivatedRoute,
              public gridService: GridService,
              public readonly renderer: Renderer2,
              private uiManager: UiManagerService,
              private globalEventService: GlobalEventService,
              private persistenceService: LocalPersistenceService) {
    this.layoutDelegate = new ResizableWorkspaceLayoutDelegate(this, this.persistenceService);
  }

  ngOnInit(): void {
    if (!this.disableNavigationToContextualContent) {
      this.layoutDelegate.applyMinimalSizes();
      this.layoutDelegate.reactToWindowResized();
      this.layoutDelegate.initializeWidthBoundary();
      this.initializeNavigationFromGridSelection();
      this.initializeFolding();
      this.initializeClosingContextualContent();
      this.layoutDelegate.hideContextualContent();
      this.reactToRouteChanges();
    }
  }

  ngAfterViewInit(): void {
    if (!this.disableNavigationToContextualContent) {
      this.initGridSelectionFromInitialUrl();
    }
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  // Triggers navigation just as if a single row was selected (without affecting selection)
  forceNavigation(id: string): void {
    this.handleSingleSelection(id);
  }

  handleNoSelection(): void {
    if (this.currentViewedEntities.length > 0) {
      this.currentViewedEntities = [];
      this.layoutDelegate.hideContextualContent();
      this.router.navigate(this.rootUrl);
    }
  }

  handleSingleSelection(id: string): void {
    if (this.currentViewedEntities.length !== 1
      || this.currentViewedEntities[0] !== id) {
      this.currentViewedEntities = [id];
      this.layoutDelegate.showContextualContent();
      console.log('handle single selection');
      this.router.navigate([id], {relativeTo: this.activeRoute});
    } else {
      console.log('no need to handle single selection');
    }
  }

  handleResize(widthDelta: number): void {
    this.layoutDelegate.handleResize(widthDelta);
  }

  switchToOneRowSelectedLayout(): void {
    this.layoutDelegate.showContextualContent();
  }

  switchToNoRowLayout(): void {
    this.layoutDelegate.hideContextualContent();
    this.uiManager.unfoldTree(this.name);
  }

  private initializeFolding(): void {
    this.uiManager.registerWorkspace(this.name);
    this.uiManager.createWorkspaceLayoutObservable(this.name).pipe(
      takeUntil(this.unsub$),
      filter(workspaceLayoutState => Boolean(workspaceLayoutState)),
      pluck('treeFolded'),
      distinctUntilChanged()
    ).subscribe((treeFolded: boolean) => {
      if (treeFolded) {
        this.layoutDelegate.foldTree();
      } else {
        this.layoutDelegate.unfoldTree();
      }
    });
  }

  /**
   * Handles navigation when selections are made in the grid. When only one row gets selected
   * (with no modifier key pressed), a navigation to the contextual view is fired.
   */
  private initializeNavigationFromGridSelection(): void {
    this.gridService.selectedRowIds$.pipe(
      takeUntil(this.unsub$),
      withLatestFrom(this.globalEventService.controlKey$, this.globalEventService.shiftKey$),
    ).subscribe(([ids, ctrl, shift]: [string[], boolean, boolean]) => {
      if (ids.length === 1 && !ctrl && !shift) {
        this.handleSingleSelection(ids[0]);
      } else {
        this.handleNoSelection();
      }
    });
  }

  // Used by 'close' buttons in contextual views
  private initializeClosingContextualContent(): void {
    this.uiManager.requireClosingContextualContent$.pipe(
      takeUntil(this.unsub$)
    ).subscribe(() => this.gridService.unselectAllRows());
  }

  private initGridSelectionFromInitialUrl(): void {
    this.parseUrlAndNavigate(this.router.url);
  }

  private reactToRouteChanges(): void {
    this.router.events.pipe(
      takeUntil(this.unsub$),
      filter(e => e instanceof NavigationEnd)
    ).subscribe((e: NavigationEnd) => {
      this.parseUrlAndNavigate(e.urlAfterRedirects);
    });
  }

  private parseUrlAndNavigate(url: string): void {
    if (this.entityIdPositionInUrl == null) {
      throw new Error('Cannot parse URL because entity ID position in URL was not provided.');
    }

    if (url.startsWith('/')) {
      url = url.slice(1);
    }

    const urlParts = url.split('/');
    const entityId = urlParts[this.entityIdPositionInUrl];

    logger.debug(`found entity ID : ${entityId}`);

    if (entityIdSeemsLegit(entityId)) {
      this.gridService.loaded$.pipe(
        filter(loaded => Boolean(loaded)),
        take(1),
        withLatestFrom(this.gridService.gridState$),
        map(([, state]) => isEntityInGrid(entityId, state))
      ).subscribe((inGrid: boolean) => {
        if (inGrid) {
          logger.debug(`select single row : ${entityId}`);
          this.gridService.selectSingleRow(entityId);
          this.switchToOneRowSelectedLayout();
        } else {
          logger.debug(`force navigation : ${entityId}`);
          this.forceNavigation(entityId);
        }
      });
    } else {
      logger.debug(`No entity ID found on navigation, closing contextual view.`);
      this.switchToNoRowLayout();
    }
  }
}

function isEntityInGrid(entityId: string, state: GridState): boolean {
  return (state.dataRowState.ids as string[]).includes(entityId);
}

function entityIdSeemsLegit(entityId: string): boolean {
  const isNotEmptyOrNull = Boolean(entityId);
  const isInvalidInteger = isNaN(Number.parseInt(entityId, 10));
  return isNotEmptyOrNull && !isInvalidInteger;
}

