import {ChangeDetectionStrategy, Component} from '@angular/core';
import {TreePickerDialogConfiguration} from '../../picker.dialog.configuration';
import {DialogReference} from '../../../dialog/model/dialog-reference';
import {DataRow} from '../../../grid/model/data-row.model';
import {AbstractTreeDialogPicker} from '../abstract-tree-dialog-picker';

@Component({
  selector: 'sqtm-core-test-case-tree-dialog-picker',
  templateUrl: './test-case-tree-dialog-picker.component.html',
  styleUrls: ['./test-case-tree-dialog-picker.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TestCaseTreeDialogPickerComponent extends AbstractTreeDialogPicker {

  constructor(dialogReference: DialogReference<TreePickerDialogConfiguration, DataRow[]>) {
    super(dialogReference);
  }

}
