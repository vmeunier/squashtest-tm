import {Directive, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {TreePickerDialogConfiguration} from '../picker.dialog.configuration';
import {DialogReference} from '../../dialog/model/dialog-reference';
import {DataRow} from '../../grid/model/data-row.model';

@Directive()
// tslint:disable-next-line:directive-class-suffix
export abstract class AbstractTreeDialogPicker implements OnInit, OnDestroy {

  private unsub$ = new Subject<void>();

  configuration: TreePickerDialogConfiguration;

  protected constructor(protected dialogReference: DialogReference<TreePickerDialogConfiguration, DataRow[]>) {
    this.configuration = this.dialogReference.data;
  }

  ngOnInit() {
  }

  confirm() {
    this.dialogReference.close();
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  changeTreeSelection(rows: DataRow[]) {
    this.dialogReference.result = rows;
  }
}
