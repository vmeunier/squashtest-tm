import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {DefaultValueRendererComponent} from './default-value-renderer.component';
import {TestingUtilsModule} from '../../../../testing-utils/testing-utils.module';
import {TranslateModule} from '@ngx-translate/core';

describe('DefaultValueRendererComponent', () => {
  let component: DefaultValueRendererComponent;
  let fixture: ComponentFixture<DefaultValueRendererComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TestingUtilsModule, TranslateModule.forRoot()],
      declarations: [DefaultValueRendererComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefaultValueRendererComponent);
    component = fixture.componentInstance;
    component.filter = {
      id: 'filter',
      label: 'filter',
      value: {
        kind: 'single-string-value',
        value: 'v'
      }
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
