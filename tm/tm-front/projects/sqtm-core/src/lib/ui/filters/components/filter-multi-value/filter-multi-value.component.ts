import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {AbstractFilterWidget} from '../abstract-filter-widget';
import {DiscreteFilterValue, Filter, MultiDiscreteFilterValue, Scope} from '../../state/filter.state';
import {take} from 'rxjs/operators';
import {ItemListSearchProvider} from '../../item-list-search-provider.service';
import {
  ListGroup,
  ListItem
} from '../../../workspace-common/components/forms/grouped-multi-list/grouped-multi-list.component';

@Component({
  selector: 'sqtm-core-filter-multi-value',
  templateUrl: './filter-multi-value.component.html',
  styleUrls: ['./filter-multi-value.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FilterMultiValueComponent extends AbstractFilterWidget implements OnInit {

  items: ListItem[];
  groups: ListGroup[];
  private filterValue: DiscreteFilterValue[];

  constructor(protected cdRef: ChangeDetectorRef, private listProvider: ItemListSearchProvider) {
    super(cdRef);
  }

  ngOnInit(): void {
  }

  setFilter(filter: Filter, scope: Scope) {
    this.listProvider.provideList(filter.id.toString()).pipe(
      take(1)
    ).subscribe(providedItems => {
      const selectedItemIds = (filter.value as MultiDiscreteFilterValue).value.map(v => v.id);
      this.items = providedItems.map(item => ({
        ...item, selected: selectedItemIds.includes(item.id)
      }));
      this.filterValue = (filter.value as MultiDiscreteFilterValue).value;
      this.cdRef.detectChanges();
    });
    // this.referentialDataService.connectToCustomField(filter.cufId).pipe(
    //   take(1)
    // ).subscribe(cuf => {
    //   const filterValue = filter.value;
    //   if (isDiscreteValue(filterValue)) {
    //     const selectedIds = filterValue.value.map(v => v.id);
    //     this.items = cuf.options
    //       .filter(option => Boolean(option.label))
    //       .map(option => ({
    //         id: option.label,
    //         label: option.label,
    //         selected: selectedIds.includes(option.label)
    //       }));
    //     this.isTag = cuf.inputType === InputType.TAG;
    //     this._filter = filter;
    //     this.filterValue = filterValue.value;
    //     this.operation = filter.operation;
    //     this.workspace = filter.uiOptions?.workspace;
    //     this.cdRef.detectChanges();
    //   } else {
    //     throw Error('Not handled kind ' + filter.value.kind);
    //   }
    // });
    // this._filter = filter;
  }

  changeSelection($event: ListItem) {
    let nextValue: DiscreteFilterValue[] = [...this.filterValue];
    if ($event.selected) {
      nextValue = this.filterValue.concat({id: $event.id, label: $event.label});
    } else {
      const index = this.filterValue.findIndex(v => v.id === $event.id);
      if (index >= 0) {
        nextValue.splice(index, 1);
      }
    }
    this.filteringChanged.next({
      filterValue: {
        kind: 'multiple-discrete-value',
        value: nextValue
      }
    });
  }
}
