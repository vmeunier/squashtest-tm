import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';

export interface ListPanelItem {
  id: string | number;
  label: string;
  icon?: string;
  color?: string;
}

@Component({
  selector: 'sqtm-core-list-panel',
  templateUrl: './list-panel.component.html',
  styleUrls: ['./list-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListPanelComponent {
  _items: ListPanelItem[] = [];

  _selectedItem: string | number;

  @Input()
  set selectedItem(value: string | number) {
    this._selectedItem = value;
  }

  @Input()
  set items(items: ListPanelItem[]) {
    this._items = items;
  }

  @Output()
  itemSelectionChanged = new EventEmitter<string | number>();

  change(id) {
    this.selectedItem = id;
    this.itemSelectionChanged.emit(id);
  }
}
