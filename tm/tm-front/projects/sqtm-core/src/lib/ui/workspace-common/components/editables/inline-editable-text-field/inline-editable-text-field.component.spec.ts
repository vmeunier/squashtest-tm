import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {InlineEditableTextFieldComponent} from './inline-editable-text-field.component';
import {TranslateModule} from '@ngx-translate/core';

describe('InlineEditableTextFieldComponent', () => {
  let component: InlineEditableTextFieldComponent;
  let fixture: ComponentFixture<InlineEditableTextFieldComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [InlineEditableTextFieldComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InlineEditableTextFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
