import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';
import {Option} from '../../../../../model/option.model';
import {EditableCustomField} from '../../../../custom-field/editable-custom-field';
import {ValidatorFn} from '@angular/forms';
import {AbstractEditableField, EditableField, EditableLayout} from '../abstract-editable-field';
import {wsCommonLogger} from '../../../workspace.common.logger';
import {TranslateService} from '@ngx-translate/core';

const LOGGER = wsCommonLogger.compose('EditableSelectFieldComponent');

@Component({
  selector: 'sqtm-core-editable-select-field',
  template: `
    <div [ngClass]="getComponentClasses()" *ngIf="!edit">
          <span *ngIf="value" (click)="enableEditMode()" class="field-value-color"
                [class.editable]="editable">
        {{getOptionLabel(value)}}
        </span>
      <ng-container *ngIf="mustShowPlaceHolder(value, allowEmptyValue)">
            <span class="sqtm-placeholder" (click)="enableEditMode()" [class.editable]="editable">
             {{getActualPlaceHolder()}}
            </span>
      </ng-container>
      <i *ngIf="editable" class="caret-icon flex-fixed-size" nz-icon nzType="caret-down" nzTheme="outline"
         (click)="enableEditMode()"></i>
    </div>
    <div *ngIf="edit" class="flex-column">
      <div class="flex-row full-width">
        <nz-select class="full-width" [ngModel]="transientValue" [nzSize]="size" (ngModelChange)="change($event)"
                   [nzPlaceHolder]="placeHolder" [nzAllowClear]="allowEmptyValue">
          <nz-option *ngFor="let option of getVisibleOptions(allOptions, excludedKeys); trackBy:trackByOptionValue "
                     nzValue="{{option.value}}"
                     nzLabel="{{option.label}}"
                     nzCustomContent
                     [nzHide]="option.hide">
            {{option.label}}
          </nz-option>
        </nz-select>
        <div class="flex-fixed-size" *ngIf="mustShowButtons()">
          <button nz-button nzType="primary"
                  [attr.data-test-button-id]="'confirm'"
                  [ngClass]="getConfirmButtonCss()"
                  [nzSize]="size"
                  [nzLoading]="pending"
                  (click)="confirm()">
            <i nz-icon [nzType]="'check'"></i>
            <ng-container *ngIf="mustShowButtonLabels()">
              {{'sqtm-core.generic.label.confirm' | translate | capitalize}}
            </ng-container>
          </button>
        </div>
        <div class="flex-fixed-size" *ngIf="mustShowButtons()">
          <button nz-button nzType="default"
                  [attr.data-test-button-id]="'cancel'"
                  [nzSize]="size"
                  [ngClass]="getCancelButtonCss()"
                  [nzLoading]="pending"
                  (click)="cancel()">
            <i nz-icon [nzType]="'close'"></i>
            <ng-container *ngIf="mustShowButtonLabels()">
              {{'sqtm-core.generic.label.cancel' | translate | capitalize}}
            </ng-container>
          </button>
        </div>

      </div>
    </div>
  `,
  styleUrls: ['./editable-select-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditableSelectFieldComponent extends AbstractEditableField implements OnInit, EditableField, EditableCustomField {

  @Input()
  layout: EditableLayout = 'default';

  private _edit = false;

  @Input()
  set edit(isEdit: boolean) {
    this._edit = isEdit;
    this.toggleEditEvent.emit(isEdit);
    this.cdRef.detectChanges();
  }

  get edit(): boolean {
    return this._edit;
  }

  @Input()
  allowEmptyValue = false;

  @Input()
  showsPlaceholderAsFirstOption = true;

  @Input()
  pending = false;

  @Input()
  size: 'small' | 'default' | 'large' = 'default';

  @Input()
  set validators(validators: ValidatorFn[]) {
    throw Error('Validators are not handled in Select Field');
  }

  /**
   * Temp value set to user choice in NzSelect, before the confirm or cancel action is fired.
   */
  transientValue: string;

  @Input()
  options: Option[];

  @Input()
  excludedKeys: any[] = [];

  @Input()
  placeHolder: string;

  private _value: string;

  /**
   * Set the value, end all async operations and close editable mode
   * @param value the new value
   */
  @Input()
  set value(value: string) {
    this.updateAndClose(value);
  }

  get value(): string {
    return this._value;
  }

  @Output()
  readonly confirmEvent = new EventEmitter<Option>();

  @Output()
  readonly toggleEditEvent = new EventEmitter<boolean>();

  constructor(cdRef: ChangeDetectorRef, public readonly translateService: TranslateService) {
    super(cdRef);
  }

  get allOptions(): Option[] {
    if (this.showsPlaceholderAsFirstOption && this.allowEmptyValue) {
      return [{
        value: null,
        label: this.placeHolder,
      }, ...this.options];
    }

    return this.options;
  }

  ngOnInit() {
  }

  enableEditMode() {
    if (!this.edit && this.editable) {
      if (this.excludedKeys.includes(this._value)) {
        const visibleOptions = this.getVisibleOptions(this.options, this.excludedKeys);
        this.transientValue = visibleOptions[0].value;
      } else {
        this.transientValue = this.value;
      }
      this.edit = true;
    }
    LOGGER.debug(`Transient Value = ${this.transientValue}`);
    return false;
  }

  cancel() {
    this.transientValue = this.value;
    this.disableEditMode();
  }

  /**
   * Emit transient value.
   * Do NOT set the value neither change the edit mode.
   * As all our editable are used to modify state server side, the listener of this event must :
   * - Use beginAsync() to pass the editable to async mode.
   * - Proceed the request.
   * - If success set the value so the editable will close and the value will be set.
   * - If failure take according measures.
   */
  confirm() {
    if (this.transientValue !== this.value) {
      this.executeAutoAsync();
      this.confirmEvent.emit(this.getOptionFromValue(this.transientValue));
    } else {
      this.cancel();
    }
  }

  change($event: string) {
    this.transientValue = $event;
  }

  beginAsync() {
    this.pending = true;
  }

  endAsync() {
    this.pending = false;
  }

  private updateAndClose(value: string) {
    this.endAsync();
    this.disableEditMode();
    this._value = value;
    this.transientValue = value;
    this.cdRef.detectChanges();
  }

  private getOptionFromValue(value: string | number): Option {
    const optionFound = this.options.find(option => option.value === value);
    if (Boolean(optionFound)) {
      return optionFound;
    } else {
      if (!Boolean(value) && this.allowEmptyValue) {
        return null;
      }
    }
    throw Error(`Unable to find option ${value} inside available options : ${JSON.stringify(this.options)}`);
  }

  disableEditMode() {
    this.edit = false;
    this.pending = false;
  }

  getOptionLabel(key: string | number) {
    return this.getOptionFromValue(key).label;
  }

  getVisibleOptions(options: Option[], excludedKeys: string[]): Option[] {
    return options.filter(opt => !excludedKeys.includes(opt.value));
  }

  getComponentClasses() {
    const cssClass = [];
    if (this.edit) {
      cssClass.push('edit');
    } else {
      cssClass.push('read');
      if (this.editable) {
        cssClass.push('editable');
      }
    }
    return cssClass;
  }

  trackByOptionValue(index: number, option: Option): any {
    return option.value;
  }

  // avoid flash of placeholder in native entity field if no value and no-optional field
  // cuf have no problems because the cuf widget will only instantiate after entity has been fetched and loaded in store. So no flash.
  mustShowPlaceHolder(value: string, allowEmptyValue: boolean) {
    return allowEmptyValue && !Boolean(value);
  }

  getActualPlaceHolder() {
    return this.placeHolder || this.translateService.instant('sqtm-core.generic.editable.placeholder');
  }
}
