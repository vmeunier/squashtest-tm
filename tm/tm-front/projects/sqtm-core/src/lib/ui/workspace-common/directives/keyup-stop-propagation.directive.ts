import {Directive, HostListener, Input} from '@angular/core';

@Directive({
  selector: '[sqtmCoreKeyupStopPropagation]'
})
export class KeyupStopPropagationDirective {
  // tslint:disable-next-line:no-input-rename
  @Input('propagatesKeyUp')
  propagatesKeyUp = false;

  constructor() { }

  @HostListener('keyup', ['$event'])
  public onEvent($event: MouseEvent): void {
    if (! Boolean(this.propagatesKeyUp)) {
      $event.stopPropagation();
    }
  }
}
