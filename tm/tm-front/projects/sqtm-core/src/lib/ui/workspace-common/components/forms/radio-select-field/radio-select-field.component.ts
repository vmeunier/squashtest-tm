import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input} from '@angular/core';
import {AbstractFormField} from '../abstract-form-field';
import {FormGroup} from '@angular/forms';
import {DisplayOption} from '../display-option';
import {FieldValidationError} from '../../../../../model/error/error.model';

@Component({
  selector: 'sqtm-core-radio-select-field',
  template: `
    <div class="full-width" [formGroup]="formGroup" [attr.data-test-field-name]="fieldName"
         sqtmCoreKeyupStopPropagation>
      <nz-select class="full-width"
                 [nzDisabled]="disabled"
                 [formControlName]="fieldName"
                 [nzPlaceHolder]="placeHolder">
        <nz-option
            *ngFor="let option of options"
            [nzValue]="option.id"
            [nzLabel]="option.label | translate"
            [nzCustomContent]="true">
          <i nz-icon nzType="check" nzTheme="outline"
             class="check-icon" [class.checked]="isChecked(option)">
          </i> {{option.label | translate}}
        </nz-option>
      </nz-select>
      <div class="has-error" *ngFor="let error of errors">
        <span [attr.data-test-error-key]="error.provideI18nKey()"
              class="sqtm-core-error-message">
          {{error.provideI18nKey()| translate}}
        </span>
      </div>
    </div>`,
  styleUrls: ['./radio-select-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RadioSelectFieldComponent extends AbstractFormField {

  @Input()
  formGroup: FormGroup;

  @Input()
  fieldName: string;

  @Input()
  options: DisplayOption[] = [];

  @Input()
  placeHolder: string;

  @Input()
  set serverSideFieldValidationError(fieldsValidationErrors: FieldValidationError[]) {
    this.showServerSideError(fieldsValidationErrors);
  }

  @Input()
  set disabled(isDisabled: boolean) {
    this._disabled = isDisabled;
  }

  get disabled(): boolean {
    return this._disabled;
  }

  private _disabled: boolean;

  constructor(cdr: ChangeDetectorRef) {
    super(cdr);
  }

  isChecked(option: DisplayOption): boolean {
    return this.formControl?.value === option.id;
  }
}
