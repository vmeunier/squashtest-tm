import {AfterViewInit, Directive, Host, HostListener, Input, OnDestroy} from '@angular/core';
import {SqtmEntityState} from '../../../core/services/entity-view/entity-view.state';
import {Subject} from 'rxjs';
import {EntityViewService} from '../../../core/services/entity-view/entity-view.service';
import {distinctUntilChanged, map, takeUntil} from 'rxjs/operators';
import {EditableSelectLevelEnumFieldComponent} from '../components/editables/editable-select-level-enum-field/editable-select-level-enum-field.component';
import {SimplePermissions} from '../../../model/permissions/simple-permissions';
import {LevelEnumItem} from '../../../model/level-enums/level-enum';
import {wsCommonLogger} from '../workspace.common.logger';

const logger = wsCommonLogger.compose('EntityViewEditableSelectLevelEnumFieldDirective');

@Directive({
  selector: '[sqtmCoreEntityViewEditableSelectLevelEnumField]'
})
export class EntityViewEditableSelectLevelEnumFieldDirective<E extends SqtmEntityState, T extends string, P extends SimplePermissions>
  implements AfterViewInit, OnDestroy {

  @Input('sqtmCoreEntityViewEditableSelectLevelEnumField')
  fieldName: keyof E;

  unsub$ = new Subject<void>();

  constructor(private entityViewService: EntityViewService<E, T, P>, @Host() private editable: EditableSelectLevelEnumFieldComponent) {
  }

  @HostListener('confirmEvent', ['$event'])
  onConfirm(levelEnumItem: LevelEnumItem<any>) {
    this.editable.beginAsync();
    this.entityViewService.update(this.fieldName, levelEnumItem.id as any); // cannot use type system on dynamic inputs...
  }

  ngAfterViewInit(): void {
    logger.debug('enum field : ', [this.editable]);
    logger.debug('enum field name : ' + this.fieldName);
    logger.debug('enum field is editable : ' + this.editable.editable);

    this.entityViewService.componentData$.pipe(
      takeUntil(this.unsub$),
      map(componentData => componentData[componentData.type]),
      // If value has changed in store, the update is a success.
      // If there is any error server side, the error stream will emit, and the value will not be changed.
      distinctUntilChanged()
    ).subscribe((entity) => {
      this.editable.selectedEnumItem = entity[this.fieldName as string];
    });

    this.entityViewService.componentData$.pipe(
      takeUntil(this.unsub$),
    ).subscribe((componentData) => {
      this.editable.editable = componentData.permissions.canWrite
        && componentData.milestonesAllowModification
        && this.editable.editable;
    });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}
