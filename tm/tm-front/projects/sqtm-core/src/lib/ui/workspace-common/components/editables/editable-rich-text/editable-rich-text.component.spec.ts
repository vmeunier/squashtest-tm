import {TestBed, waitForAsync} from '@angular/core/testing';

import {EditableRichTextComponent} from './editable-rich-text.component';
import {NO_ERRORS_SCHEMA, Pipe, PipeTransform} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {FormsModule} from '@angular/forms';
import {TestElement} from 'ngx-speculoos';
import {By} from '@angular/platform-browser';
import {CKEditorModule} from 'ckeditor4-angular';
import {OnPushComponentTester} from '../../../../testing-utils/on-push-component-tester';
import {TestingUtilsModule} from '../../../../testing-utils/testing-utils.module';
import {SessionPingService} from '../../../../../core/services/session-ping/session-ping.service';
import {mockSessionPingService} from '../../../../testing-utils/mocks.service';

class EditableRichTextComponentTester extends OnPushComponentTester<EditableRichTextComponent> {
  constructor() {
    super(EditableRichTextComponent);
  }

  get span(): TestElement<any> {
    return this.element('span');
  }

  get errorMessages(): HTMLElement[] {
    return this.fixture.debugElement.queryAll(By.css('span.sqtm-core-error-message')).map(
      (dbgElem) => dbgElem.nativeElement);
  }

  get confirmButton(): HTMLButtonElement {
    const htmlButtonElement = this.getButtons();
    if (htmlButtonElement && htmlButtonElement.length > 0) {
      return htmlButtonElement[0];
    }
    return null;
  }

  get cancelButton(): HTMLButtonElement {
    const htmlButtonElement = this.getButtons();
    if (htmlButtonElement && htmlButtonElement.length > 0) {
      return htmlButtonElement[1];
    }
    return null;
  }

  private getButtons(): HTMLButtonElement[] {
    return this.fixture.nativeElement.querySelectorAll('button');
  }
}

@Pipe({name: 'safeRichContent'})
class SafeRichContentMock implements PipeTransform {
  transform(value: string): string {
    return value;
  }
}

describe('EditableRichTextComponent', () => {
  let tester: EditableRichTextComponentTester;
  const translateService = jasmine.createSpyObj('TranslateService', ['instant', 'getBrowserLang']);

  const data = '<p>Iamque non umbratis fallaciis res agebatur.</p>';
  const dataInnerHTML = 'Iamque non umbratis fallaciis res agebatur.';

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TestingUtilsModule, FormsModule, CKEditorModule],
      declarations: [SafeRichContentMock, EditableRichTextComponent],
      providers: [
        {
          provide: TranslateService,
          useValue: translateService
        },
        {
          provide: SessionPingService,
          useValue: mockSessionPingService(),
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents()
      .then(() => {
        tester = new EditableRichTextComponentTester();
        tester.componentInstance.ngOnInit();
        tester.componentInstance.value = data;
        tester.detectChanges();
      });
  }));

  it('should create', ((done) => {
    expect(tester).toBeTruthy();
    expect(tester.componentInstance).toBeTruthy();
    done();
  }));

  it('should display data in no-editing mode', ((done) => {
    const initialSpan = tester.fixture.nativeElement.querySelector('div');
    expect(initialSpan.textContent).toContain(dataInnerHTML);
    done();
  }));

  it(`should show error when confirming blank value while being required`, (done) => {
    tester.componentInstance.required = true;
    tester.detectChanges();

    activateEditMode();

    tester.componentInstance.editorModel.editorData = '';
    tester.confirmButton.click();
    tester.detectChanges();

    expect(tester.confirmButton).toBeTruthy();
    expect(tester.cancelButton).toBeTruthy();
    expect(tester.errorMessages.length).toEqual(1);

    done();
  });

  function activateEditMode() {
    const span = tester.fixture.debugElement.query(By.css('.rich-text-border'));
    span.triggerEventHandler('click', null);
    tester.detectChanges();
    expect(tester.cancelButton).toBeTruthy();
    expect(tester.confirmButton).toBeTruthy();
  }
});
