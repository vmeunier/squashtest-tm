///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {ResizableDirective} from './resizable.directive';
import {TestBed} from '@angular/core/testing';
import {Component} from '@angular/core';
import {By} from '@angular/platform-browser';
import {GridTestingModule} from '../../grid/grid-testing/grid-testing.module';

@Component({
  selector: 'sqtm-core-split-drag-directive-component',
  template: '<div sqtmCoreResizable></div>'
})
class TestComponent {

}


describe('ResizableDirective', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        TestComponent,
        ResizableDirective
      ],
      imports: [GridTestingModule]
    });
  });


  it('should create an instance', () => {
    TestBed.compileComponents().then(() => {
      const fixture = TestBed.createComponent(TestComponent);
      const directiveEl = fixture.debugElement.query(By.directive(ResizableDirective));
      expect(directiveEl).not.toBeNull();

      const directiveInstance = directiveEl.injector.get(ResizableDirective);
      expect(directiveInstance).not.toBe(null);
    });
  });
});
