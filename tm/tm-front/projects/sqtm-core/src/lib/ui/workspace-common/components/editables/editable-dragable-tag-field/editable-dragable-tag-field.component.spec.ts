import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {EditableDragableTagFieldComponent} from './editable-dragable-tag-field.component';

describe('EditableDragableTagFieldComponent', () => {
  let component: EditableDragableTagFieldComponent;
  let fixture: ComponentFixture<EditableDragableTagFieldComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [EditableDragableTagFieldComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditableDragableTagFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
