export interface DisplayOption {
  id: string | number;
  label: string;
}
