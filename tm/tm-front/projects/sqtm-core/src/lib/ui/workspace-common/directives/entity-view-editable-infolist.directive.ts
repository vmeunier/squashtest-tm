import {AfterViewInit, Directive, Host, HostListener, Input, OnDestroy} from '@angular/core';
import {SqtmEntityState} from '../../../core/services/entity-view/entity-view.state';
import {Subject} from 'rxjs';
import {EntityViewService} from '../../../core/services/entity-view/entity-view.service';
import {distinctUntilChanged, map, pluck, takeUntil} from 'rxjs/operators';
import {SimplePermissions} from '../../../model/permissions/simple-permissions';
import {InfoListItem} from '../../../model/infolist/infolistitem.model';
import {EditableSelectInfolistFieldComponent} from '../components/editables/editable-select-infolist-field/editable-select-infolist-field.component';

@Directive({
  selector: '[sqtmCoreEntityViewEditableInfolistDirective]'
})
export class EntityViewEditableInfolistDirective<E extends SqtmEntityState, T extends string, P extends SimplePermissions>
  implements AfterViewInit, OnDestroy {

  @Input('sqtmCoreEntityViewEditableInfolistDirective')
  fieldName: keyof E;

  unsub$ = new Subject<void>();

  constructor(private entityViewService: EntityViewService<E, T, P>, @Host() private editable: EditableSelectInfolistFieldComponent) {

  }

  @HostListener('confirmEvent', ['$event'])
  onConfirm(infoListItem: InfoListItem) {
    this.editable.beginAsync();
    this.entityViewService.update(this.fieldName, infoListItem.id as any); // cannot use type system on dynamic inputs...
  }

  ngAfterViewInit(): void {
    this.entityViewService.componentData$.pipe(
      takeUntil(this.unsub$),
      map(componentData => componentData[componentData.type]),
      pluck(this.fieldName as string),
      distinctUntilChanged()
    ).subscribe((value: number) => {
      this.editable.selectedItem = value;
    });

    this.entityViewService.componentData$.pipe(
      takeUntil(this.unsub$),
    ).subscribe((componentData) => {
      this.editable.editable = componentData.permissions.canWrite
        && componentData.milestonesAllowModification
        && this.editable.editable;
      this.editable.child.cdRef.detectChanges();
    });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

}
