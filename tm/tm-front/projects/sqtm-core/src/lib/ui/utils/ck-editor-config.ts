import {CKEditor4} from 'ckeditor4-angular';
import EditorType = CKEditor4.EditorType;

export function createCkEditorConfig(browserLang: string): CKEditor4.Config {
  return {
    language: browserLang,

    toolbar: 'Squash',
    skin: 'moono-lisa',
    toolbarCanCollapse: true,
    toolbar_Squash:
      [
        ['Bold', 'Italic', 'Underline', 'Strike', 'NumberedList', 'BulletedList'],
        ['Link'],
        ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
        ['TextColor'], ['Font'], ['FontSize'],
        ['Scayt'],
        ['Table', 'Image'],
        ['Maximize']
      ],
    height: 200,
    resize_minHeight: 175,
    resize_minWidth: 2001,
    removePlugins: 'elementspath',
    extraPlugins: 'onchange'
  };
}
