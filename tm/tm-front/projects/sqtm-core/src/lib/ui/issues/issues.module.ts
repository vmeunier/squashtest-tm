import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IssuesPanelComponent} from './container/issues-panel/issues-panel.component';
import {IssuesConnectComponent} from './components/issues-connect/issues-connect.component';
import {BugtrackerConnectDialogComponent} from './components/bugtracker-connect-dialog/bugtracker-connect-dialog.component';
import {TranslateModule} from '@ngx-translate/core';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import {ReactiveFormsModule} from '@angular/forms';
import {WorkspaceCommonModule} from '../workspace-common/workspace-common.module';
import {DialogModule} from '../dialog/dialog.module';
import {GridModule} from '../grid/grid.module';

@NgModule({
  declarations: [
    IssuesPanelComponent,
    IssuesConnectComponent,
    BugtrackerConnectDialogComponent
  ],
  imports: [
    CommonModule,
    GridModule,
    WorkspaceCommonModule,
    TranslateModule.forChild(),
    NzDividerModule,
    ReactiveFormsModule,
    NzFormModule,
    NzButtonModule,
    DialogModule,
    NzInputModule
  ],
  exports: [IssuesPanelComponent],
  entryComponents: [BugtrackerConnectDialogComponent]
})
export class IssuesModule {
}
