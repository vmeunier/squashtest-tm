import {createEntityAdapter, EntityState} from '@ngrx/entity';
import {IssueUIModel} from '../../../model/issue/issue-ui.model';

export interface BugTrackerUIModelState extends EntityState<IssueUIModel> {
}

export const bugTrackerUIModelEntityAdapter = createEntityAdapter<IssueUIModel>();
