import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {catchError, concatMap, map, take, tap} from 'rxjs/operators';
import {HttpErrorResponse} from '@angular/common/http';
import {createStore, Store} from '../../../core/store/store';
import {RestService} from '../../../core/services/rest.service';
import {IssueBindableEntity} from '../../../model/issue/issue-bindable-entity.model';
import {IssueUIModel} from '../../../model/issue/issue-ui.model';
import {Identifier} from '../../../model/entity.model';

@Injectable()
export class IssuesService {

  readonly store: Store<IssueUIModel>;

  readonly state$: Observable<IssueUIModel>;

  constructor(private restService: RestService) {
    const initialState = this.getInitialState();
    this.store = createStore<IssueUIModel>(initialState);

    this.state$ = this.store.state$;
  }

  loadModel(entityId: number, entityType: IssueBindableEntity): Observable<IssueUIModel> {
    return this.restService.getWithoutErrorHandling<IssueUIModel>([buildBugTrackerModelURL(entityId, entityType)])
      .pipe(
        map((model: IssueUIModel) => {
          return {...model, entityType};
        }),
        catchError(error => this.setError(error)),
        map(state => ({...state, modelLoaded: true})),
        tap(state => this.store.commit(state))
      );
  }

  private setError(error: HttpErrorResponse): Observable<IssueUIModel> {
    return this.store.state$.pipe(
      take(1),
      map((state: IssueUIModel) => {
        return {...state, hasError: true, error: error};
      })
    );
  }

  activateBugTracker(): Observable<IssueUIModel> {
    return this.store.state$.pipe(
      take(1),
      map((state) => ({...state, activated: true})),
      tap((newState) => this.store.commit(newState))
    );
  }

  /** Tries to connect server-side. Used in basic auth workflow. */
  connect(serverId: number, authentication: BugTrackerAuthentication): Observable<any> {
    return this.store.state$.pipe(
      take(1),
      concatMap((state: IssueUIModel) => this.restService.post([`servers/${serverId}/authentication`], authentication).pipe(
        map(() => {
          return {...state, bugTrackerStatus: 'AUTHENTICATED', modelLoaded: true};
        })
      )),
      tap(state => this.store.commit(state))
    );
  }

  /** Marks authentication success client-side only. Used in OAuth workflow. */
  setAuthenticated(): Observable<IssueUIModel> {
    return this.store.state$.pipe(
      take(1),
      map((state) => ({...state, bugTrackerStatus: 'AUTHENTICATED'})),
      tap((newState) => this.store.commit(newState))
    );
  }

  openOAuthDialog(bugTrackerId: Identifier): void {
    const oauthUrl = this.restService.backendRootUrl + `servers/${bugTrackerId}/authentication/oauth1a`;
    const dialogConf = 'height=690, width=810, resizable, scrollbars, dialog, alwaysRaised';
    window.open(oauthUrl, 'OAuth authorizations', dialogConf);
  }

  getInitialState(): IssueUIModel {
    return {
      bugTrackerStatus: '',
      delete: '',
      entityType: null,
      oslc: false,
      panelStyle: '',
      projectId: null,
      projectName: '',
      bugTrackerMode: null,
      activated: false,
      error: null,
      hasError: false,
      modelLoaded: false
    };
  }
}

const urlByEntity = {
  TEST_CASE_TYPE: 'test-case',
  REQUIREMENT_VERSION_TYPE: 'requirement-version',
  CAMPAIGN_TYPE: 'campaign',
  CAMPAIGN_FOLDER_TYPE: 'campaign-folder',
  ITERATION_TYPE: 'iteration',
  TEST_SUITE_TYPE: 'test-suite',
  EXECUTION_TYPE: 'execution',
  EXECUTION_STEP_TYPE: 'execution-step',
};

function buildBugTrackerModelURL(id, entityType: IssueBindableEntity): string {
  if (urlByEntity[entityType] == null) {
    throw Error('Unknown entityType');
  }

  return ['issues', urlByEntity[entityType], id].join('/');
}

export interface BugTrackerAuthentication {
  username: string;
  password: string;
  type: string;
}
