import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {BugTrackerAuthentication, IssuesService} from '../../service/issues.service';
import {BugtrackerConnectConfiguration} from './bugtracker-connect-configuration';
import {Subject} from 'rxjs';
import {take} from 'rxjs/operators';
import {DialogReference} from '../../../dialog/model/dialog-reference';

@Component({
  selector: 'sqtm-core-bugtracker-connect-dialog',
  templateUrl: './bugtracker-connect-dialog.component.html',
  styleUrls: ['./bugtracker-connect-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BugtrackerConnectDialogComponent implements OnInit, OnDestroy {

  private unsub$ = new Subject<void>();

  formGroup: FormGroup;

  configuration: BugtrackerConnectConfiguration;

  connectionError: boolean;

  constructor(private dialogReference: DialogReference<BugtrackerConnectConfiguration, boolean>,
              private issuesService: IssuesService,
              private cdRef: ChangeDetectorRef) {
    this.formGroup = new FormGroup({
      username: new FormControl(''),
      password: new FormControl('')
    });

    this.configuration = this.dialogReference.data;
  }

  ngOnInit() {
  }

  connect() {
    const authentication: BugTrackerAuthentication = {
      ...this.formGroup.value,
      type: this.configuration.bugTracker.authProtocol
    };

    this.issuesService.connect(this.configuration.bugTracker.id, authentication)
      .pipe(take(1))
      .subscribe(() => this.handleConnectionSuccess(),
        (error) => this.handleConnectionError(error));
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  private handleConnectionSuccess(): void {
    this.dialogReference.result = true;
    this.dialogReference.close();
  }

  private handleConnectionError(error: any): void {
    console.error(error);
    this.connectionError = true;
    this.cdRef.detectChanges();
  }
}
