import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';
import {ProjectFilterComponent} from './project-filter.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {CORE_MODULE_CONFIGURATION} from '../../../../core/sqtm-core.tokens';
import {defaultSqtmConfiguration} from '../../../../core/sqtm-core.module';
import {DialogReference} from '../../../dialog/model/dialog-reference';
import {TestingUtilsModule} from '../../../testing-utils/testing-utils.module';

describe('ProjectFilterComponent', () => {
  let component: ProjectFilterComponent;
  let fixture: ComponentFixture<ProjectFilterComponent>;
  const dialogReference = jasmine.createSpyObj('DialogReference', ['close']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, TestingUtilsModule],
      providers: [
        {provide: CORE_MODULE_CONFIGURATION, useValue: defaultSqtmConfiguration},
        {provide: DialogReference, useValue: dialogReference}
      ],
      declarations: [ProjectFilterComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents().then(() => {
      fixture = TestBed.createComponent(ProjectFilterComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
