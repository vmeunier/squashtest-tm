import {SessionPingService} from '../../core/services/session-ping/session-ping.service';
import SpyObj = jasmine.SpyObj;
import createSpyObj = jasmine.createSpyObj;

export function mockSessionPingService(): SpyObj<SessionPingService> {
  return createSpyObj<SessionPingService>([
    'initialize',
    'beginLongTermEdition',
    'endLongTermEdition',
  ]);
}
