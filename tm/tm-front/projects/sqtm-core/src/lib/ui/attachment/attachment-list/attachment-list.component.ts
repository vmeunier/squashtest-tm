import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {AttachmentService} from '../../../core/services/attachment.service';
import {AbstractAttachmentList} from './abstract-attachment-list';

@Component({
  selector: 'sqtm-core-attachment-list',
  templateUrl: './attachment-list.component.html',
  styleUrls: ['./attachment-list.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AttachmentListComponent extends AbstractAttachmentList implements OnInit {

  constructor(attachmentService: AttachmentService) {
    super(attachmentService);
  }

  ngOnInit() {
  }

}
