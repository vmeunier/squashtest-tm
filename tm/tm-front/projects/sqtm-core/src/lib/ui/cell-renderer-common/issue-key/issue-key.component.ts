import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {AbstractCellRendererComponent} from '../../grid/components/cell-renderers/abstract-cell-renderer/abstract-cell-renderer.component';
import {GridService} from '../../grid/services/grid.service';
import {ColumnDefinitionBuilder} from '../../grid/model/column-definition.builder';

@Component({
  selector: 'sqtm-core-issue-key',
  template: `
    <ng-container *ngIf="row">
      <div class="full-width full-height flex-column">
        <a class="sqtm-grid-cell-txt-renderer" style="margin-top: auto;margin-bottom: auto" [href]="row.data['url']"
           target="_blank">{{row.data[columnDisplay.id]}}</a>
      </div>
    </ng-container>`,
  styleUrls: ['./issue-key.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IssueKeyComponent extends AbstractCellRendererComponent implements OnInit {

  constructor(public girdService: GridService, public cdRef: ChangeDetectorRef) {
    super(girdService, cdRef);
  }

  ngOnInit() {
  }

}

export function issueKeyColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(IssueKeyComponent);
}
