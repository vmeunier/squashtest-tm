import {HttpErrorResponse} from '@angular/common/http';

export class HttpErrorDialogConfiguration {
  id: string;
  error: HttpErrorResponse;
}
