import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'sqtm-core-dialog-header',
  template: `
    <div>
      <ng-content></ng-content>
    </div>
  `,
  styleUrls: ['./dialog-header.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DialogHeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
