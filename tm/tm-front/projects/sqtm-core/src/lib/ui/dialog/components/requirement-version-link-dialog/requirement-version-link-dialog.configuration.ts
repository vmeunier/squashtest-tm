import {RequirementVersionLinkType} from '../../../../model/requirement/requirement-version-link-type.model';


export interface RequirementVersionLinkDialogConfiguration {
  titleKey: string;
  requirementVersionName: string;
  requirementVersionNodesName: string;
  requirementVersionLinkTypes: RequirementVersionLinkType[];
}

export interface RequirementVersionLinkDialogResult {
  linkTypeId: number;
  linkDirection: boolean;
}
