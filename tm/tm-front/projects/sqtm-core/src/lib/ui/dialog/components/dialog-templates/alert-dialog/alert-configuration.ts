interface TranslateParamMap {
  [paramName: string]: [string];
}

export class AlertConfiguration {
  id: string;
  titleKey: string;
  level: 'WARNING' | 'DANGER' | 'INFO' = 'DANGER';
  messageKey?: string;
  translateParameters?: TranslateParamMap | string[];
}

export const defaultAlertConfiguration: Readonly<AlertConfiguration> = {
  id: 'alert',
  titleKey: 'sqtm-core.generic.label.error',
  level: 'DANGER',
  messageKey: 'sqtm-core.error.generic.label'
};
