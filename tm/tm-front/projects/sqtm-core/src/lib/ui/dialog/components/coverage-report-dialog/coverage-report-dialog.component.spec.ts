import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {CoverageReportDialogComponent} from './coverage-report-dialog.component';
import {DialogReference} from '../../model/dialog-reference';
import {TranslateModule} from '@ngx-translate/core';
import createSpyObj = jasmine.createSpyObj;

describe('CoverageReportDialogComponent', () => {
  let component: CoverageReportDialogComponent;
  let fixture: ComponentFixture<CoverageReportDialogComponent>;
  const dialogReference = createSpyObj(['close']);
  dialogReference.data = {
    messageKeys: []
  };
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [CoverageReportDialogComponent],
      providers: [
        {
          provide: DialogReference,
          useValue: dialogReference
        }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoverageReportDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
