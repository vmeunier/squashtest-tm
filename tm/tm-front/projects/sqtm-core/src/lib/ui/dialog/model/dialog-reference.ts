import {Observable, Subject} from 'rxjs';
import {ComponentRef} from '@angular/core';
import {DialogComponent} from '../components/dialog/dialog.component';
import {OverlayRef} from '@angular/cdk/overlay';
import {share, take} from 'rxjs/operators';

export class DialogReference<D = any, R = any> {
  dialogClosed$: Observable<R | undefined>;
  dialogResultChanged$: Observable<R>;
  dialogOpened$: Observable<void>;
  dialogReset$: Observable<void>;

  private _dialogClosed = new Subject<R | undefined>();
  private _dialogResultChanged = new Subject<R | undefined>();
  private _dialogOpened = new Subject<void>();
  private _dialogReset = new Subject<void>();
  private _result: R;

  constructor(
    public readonly id: string,
    public readonly containerReference: ComponentRef<DialogComponent>,
    public readonly overlayReference: OverlayRef,
    public readonly data?: D) {

    this.dialogOpened$ = this._dialogOpened.asObservable().pipe(
      take(1),
      share());

    this.dialogResultChanged$ = this._dialogResultChanged.asObservable().pipe(
      share());

    this.dialogClosed$ = this._dialogClosed.asObservable().pipe(
      take(1),
      share());

    this.dialogReset$ = this._dialogReset.asObservable().pipe(
      share());

    this.overlayReference.attachments().subscribe(() => {
      this._dialogOpened.next();
      this._dialogOpened.complete();
    });
  }

  set result(result: R) {
    this._result = result;
    this._dialogResultChanged.next(this._result);
  }

  // Call me when you reset the form to notify anyone who need additional resetting behaviour
  // e.g. when adding multiple iterations, the reference should be updated each time the form is reset
  wasReset() {
    this._dialogReset.next();
  }

  close() {
    this.overlayReference.dispose();
    this._dialogClosed.next(this._result);
    this.completeAllSubjects();
  }

  cancel() {
    this.overlayReference.dispose();
    this._dialogClosed.next(undefined);
    this.completeAllSubjects();
  }

  private completeAllSubjects() {
    this._dialogOpened.complete();
    this._dialogResultChanged.complete();
    this._dialogClosed.complete();
    this._dialogReset.complete();
  }

}
