import {EventEmitter} from '@angular/core';
import {EditableField} from '../workspace-common/components/editables/abstract-editable-field';

export interface EditableCustomField extends EditableField {
  confirmEvent: EventEmitter<any>;
  value: string | string[] | number | Date | boolean;
}
