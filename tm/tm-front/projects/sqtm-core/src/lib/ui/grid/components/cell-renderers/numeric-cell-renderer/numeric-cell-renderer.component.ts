import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {AbstractCellRendererComponent} from '../abstract-cell-renderer/abstract-cell-renderer.component';
import {GridService} from '../../../services/grid.service';

@Component({
  selector: 'sqtm-core-numeric-cell-renderer',
  template: `
    <ng-container *ngIf="columnDisplay">
      <div class="full-width full-height flex-column">
        <span style="margin: auto">{{getValue()}}</span>
      </div>
    </ng-container>`,
  styleUrls: ['./numeric-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NumericCellRendererComponent extends AbstractCellRendererComponent implements OnInit {

  constructor(grid: GridService, cdr: ChangeDetectorRef) {
    super(grid, cdr);
  }

  ngOnInit() {
  }

  getValue() {
    const value = this.row.data[this.columnDisplay.id];
    if (value != null) {
      return value;
    } else {
      return 0;
    }
  }

}

