import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {GridHeaderRowComponent} from './grid-header-row.component';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {grid} from '../../../../model/grids/grid-builders';
import {GridDefinition} from '../../model/grid-definition.model';
import {RestService} from '../../../../core/services/rest.service';
import {GridService} from '../../services/grid.service';
import {gridServiceFactory} from '../../grid.service.provider';
import {CORE_MODULE_CONFIGURATION} from '../../../../core/sqtm-core.tokens';
import {defaultSqtmConfiguration} from '../../../../core/sqtm-core.module';
import {GridHeaderDirective} from '../../directives/grid-header.directive';
import {ReferentialDataService} from '../../../../core/referential/services/referential-data.service';
import {GridViewportService} from '../../services/grid-viewport.service';
import {TestingUtilsModule} from '../../../testing-utils/testing-utils.module';

describe('GridHeaderRowComponent', () => {
  let component: GridHeaderRowComponent;
  let fixture: ComponentFixture<GridHeaderRowComponent>;

  const gridConfig = grid('grid-test').build();
  const restService = {};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TestingUtilsModule],
      declarations: [GridHeaderRowComponent, GridHeaderDirective],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        {provide: CORE_MODULE_CONFIGURATION, useValue: defaultSqtmConfiguration},
        {
          provide: GridDefinition,
          useValue: gridConfig
        }, {
          provide: RestService,
          useValue: restService
        }, {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        },
        GridViewportService
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GridHeaderRowComponent);
    component = fixture.componentInstance;
    component.viewportName = 'mainViewport';
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
