export interface PaginationDisplay {
  active: boolean;
  page: number;
  size: number;
  count: number;
  maxPage: number;
  allowedPageSizes: number[];
  showPaginationFooter: boolean;
}
