import {Overlay, OverlayConfig, OverlayRef} from '@angular/cdk/overlay';
import {ChangeDetectorRef, Directive, ElementRef, OnDestroy, TemplateRef, ViewContainerRef} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {TemplatePortal} from '@angular/cdk/portal';
import {AbstractCellRendererComponent} from '../abstract-cell-renderer/abstract-cell-renderer.component';
import {GridService} from '../../../services/grid.service';
import {RestService} from '../../../../../core/services/rest.service';
import {ConnectedPosition} from '@angular/cdk/overlay/position/flexible-connected-position-strategy';
import {Subject} from 'rxjs';
import {TableValueChange} from '../../../model/actions/table-value-change';
import {catchError, finalize} from 'rxjs/operators';
import {ActionErrorDisplayService} from '../../../../../core/services/errors-handling/action-error-display.service';

@Directive()
// tslint:disable-next-line:directive-class-suffix
export abstract class AbstractListCellRendererComponent extends AbstractCellRendererComponent implements OnDestroy {

  overlayRef: OverlayRef;

  asyncOperationRunning = false;

  unsub$ = new Subject<void>();

  protected constructor(public grid: GridService,
                        public cdRef: ChangeDetectorRef,
                        public overlay: Overlay,
                        public vcr: ViewContainerRef,
                        public translateService: TranslateService,
                        public restService: RestService,
                        public errorDisplayService: ActionErrorDisplayService) {
    super(grid, cdRef);
  }

  showList(elementRef: ElementRef, templateRef: TemplateRef<any>, positions?: ConnectedPosition[]) {
    // Default positions
    positions = positions || [
      {originX: 'center', overlayX: 'start', originY: 'bottom', overlayY: 'top', offsetY: 10},
      {originX: 'center', overlayX: 'start', originY: 'top', overlayY: 'bottom', offsetY: -10},
    ];

    const positionStrategy = this.overlay.position().flexibleConnectedTo(elementRef)
      .withPositions(positions);
    const overlayConfig: OverlayConfig = {
      positionStrategy,
      hasBackdrop: true,
      disposeOnNavigation: true,
      backdropClass: 'transparent-overlay-backdrop',
      width: 200
    };
    this.overlayRef = this.overlay.create(overlayConfig);
    const templatePortal = new TemplatePortal(templateRef, this.vcr);
    this.overlayRef.attach(templatePortal);
    this.overlayRef.backdropClick().subscribe(() => this.close());
  }

  change($event) {
    this.beginAsyncOperation();
    const tableValueChange: TableValueChange = {columnId: this.columnDisplay.id, value: $event};
    this.grid.updateCellValue(this.row, this.columnDisplay, $event).pipe(
      catchError(err => this.errorDisplayService.handleActionError(err)),
      finalize(() => {
        this.close();
        this.endAsyncOperation();
      })
    ).subscribe(() => {
      this.grid.editRows([this.row.id], [tableValueChange]);
    });
  }

  ngOnDestroy(): void {
    this.close();
    this.unsub$.next();
    this.unsub$.complete();
  }

  close() {
    if (this.overlayRef) {
      this.overlayRef.dispose();
    }
  }

  protected beginAsyncOperation() {
    this.asyncOperationRunning = true;
    this.cdRef.markForCheck();
  }

  protected endAsyncOperation() {
    this.asyncOperationRunning = false;
    this.cdRef.markForCheck();
  }

  isEditable(): boolean {
    if (typeof this.columnDisplay.editable === 'function') {
      return this.columnDisplay.editable(this.columnDisplay, this.row);
    } else {
      return this.columnDisplay.editable;
    }
  }
}
