import {TestBed} from '@angular/core/testing';

import {TreeNodeServerOperationHandler} from './tree-node-server-operation-handler';
import {TestingUtilsModule} from '../../../testing-utils/testing-utils.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {DialogService} from '../../../dialog/services/dialog.service';
import {OverlayModule} from '@angular/cdk/overlay';
import {GridState, initialGridState} from '../../model/state/grid.state';
import {createEntityAdapter} from '@ngrx/entity';
import {DataRow} from '../../model/data-row.model';
import {getDataRowDataset} from '../data-row-loaders/data-row-dataset.spec';
import {extractRootIds} from '../data-row-loaders/data-row-utils';
import {ReadOnlyPermissions} from '../../../../model/permissions/simple-permissions';


class WritePerm extends ReadOnlyPermissions {

  get canWrite(): boolean {
    return true;
  }
}


function getStateWithRowsLoaded() {
  const gridState: GridState = initialGridState();
  gridState.dataRowState = createEntityAdapter<DataRow>().setAll(getDataRowDataset(), gridState.dataRowState);
  Object.values(gridState.dataRowState.entities).forEach(row => {
    row.simplePermissions = new WritePerm();
  });
  gridState.dataRowState.rootRowIds = extractRootIds(getDataRowDataset());
  return gridState;
}

describe('TreeNodeServerOperationHandler', () => {
  let service: TreeNodeServerOperationHandler;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestingUtilsModule, HttpClientTestingModule, OverlayModule],
      providers: [
        TreeNodeServerOperationHandler, DialogService
      ]
    });
    service = TestBed.inject(TreeNodeServerOperationHandler);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });



  describe('Dropping as sibling', () => {
    interface DataType {
      targetId: string;
      draggedRowsIds: string[];
      expectedResult: boolean;
    }

    const dataset: DataType[] = [
      {targetId: 'TestCaseLibrary-1', draggedRowsIds: ['TestCase-10'], expectedResult: false},
      {targetId: 'TestCaseFolder-1', draggedRowsIds: ['TestCase-10'], expectedResult: true},
      {targetId: 'TestCase-12', draggedRowsIds: ['TestCase-10'], expectedResult: true},
      {targetId: 'TestCaseFolder-1', draggedRowsIds: ['TestCaseFolder-1'], expectedResult: false},
      {targetId: 'TestCaseFolder-7', draggedRowsIds: ['TestCaseFolder-1'], expectedResult: false},
    ];

    dataset.forEach((data, index) => runTest(data, index));

    function runTest(data: DataType, index: number) {
      it(`DataSet ${index} - Allow drop sibling for row ${data.targetId} should be ${data.expectedResult}`, function () {
        const gridState = getStateWithRowsLoaded();
        const treeNodeServerOperationHandler: TreeNodeServerOperationHandler = TestBed.inject(TreeNodeServerOperationHandler);
        gridState.uiState.dragState.draggedRowIds = data.draggedRowsIds;
        const allowDropSibling = treeNodeServerOperationHandler.allowDropSibling(data.targetId, gridState);
        expect(allowDropSibling).toEqual(data.expectedResult);
      });
    }
  });

  describe('Dropping into various rows', () => {
    interface DataType {
      targetId: string;
      draggedRowIds: string[];
      expectedResult: boolean;
    }

    const dataset: DataType[] = [
      // checking type constraints
      {targetId: 'TestCaseLibrary-2', draggedRowIds: ['TestCase-12'], expectedResult: true},
      {targetId: 'TestCaseFolder-1', draggedRowIds: ['TestCase-12'], expectedResult: true},
      {targetId: 'TestCase-4', draggedRowIds: ['TestCase-12'], expectedResult: false},
      // checking drop into itself will fail
      {targetId: 'TestCaseFolder-1', draggedRowIds: ['TestCaseFolder-1'], expectedResult: false},
      {
        targetId: 'TestCaseFolder-1',
        draggedRowIds: ['TestCaseFolder-1', 'TestCaseFolder-11', 'TestCase-10'],
        expectedResult: false
      },
      // checking drop into a descendant of any selected row will fail
      {
        targetId: 'TestCaseFolder-7',
        draggedRowIds: ['TestCaseFolder-1', 'TestCaseFolder-11', 'TestCase-10'],
        expectedResult: false
      },
    ];

    dataset.forEach((data, index) => runSiblingTest(data, index));

    function runSiblingTest(data: DataType, index: number) {
      it(`DataSet ${index} -
       Allow drop into row ${data.targetId} with selection ${JSON.stringify(data.draggedRowIds)} should be ${data.expectedResult}`,
        function () {
          const gridState = getStateWithRowsLoaded();
          const treeNodeServerOperationHandler: TreeNodeServerOperationHandler = TestBed.inject(TreeNodeServerOperationHandler);
          gridState.uiState.dragState.draggedRowIds = data.draggedRowIds;
          const allowDropInto = treeNodeServerOperationHandler.allowDropInto(data.targetId, gridState);
          expect(allowDropInto).toEqual(data.expectedResult);
        });
    }
  });
});
