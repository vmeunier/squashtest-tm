import {Injectable} from '@angular/core';
import {RestService} from '../../../../core/services/rest.service';
import {DialogService} from '../../../dialog/services/dialog.service';
import {ReferentialDataService} from '../../../../core/referential/services/referential-data.service';
import {ActionErrorDisplayService} from '../../../../core/services/errors-handling/action-error-display.service';
import {TreeNodeServerOperationHandler} from './tree-node-server-operation-handler';


@Injectable()
export class CustomReportTreeNodeServerOperationHandler extends TreeNodeServerOperationHandler {

  constructor(protected restService: RestService,
              protected dialogService: DialogService,
              protected referentialDataService: ReferentialDataService,
              protected actionErrorDisplayService: ActionErrorDisplayService) {
    super(restService, dialogService, referentialDataService, actionErrorDisplayService);
  }

  protected shouldCheckInterProjectMoves(): boolean {
    return false;
  }
}

