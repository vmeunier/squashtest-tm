import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {GridDefinition} from '../../../model/grid-definition.model';
import {RestService} from '../../../../../core/services/rest.service';
import {GridService} from '../../../services/grid.service';
import {gridServiceFactory} from '../../../grid.service.provider';
import {TestCaseLibrary} from '../../../../../model/grids/data-row.type';
import {LevelEnumCellRendererComponent} from './level-enum-cell-renderer.component';
import {getBasicGridDisplay} from '../../../grid-testing/grid-testing-utils';
import {grid} from '../../../../../model/grids/grid-builders';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {ReferentialDataService} from '../../../../../core/referential/services/referential-data.service';
import {Limited} from '../../../model/column-definition.model';

describe('LevelEnumCellRendererComponent', () => {
  let component: LevelEnumCellRendererComponent;
  let fixture: ComponentFixture<LevelEnumCellRendererComponent>;

  const translateService = jasmine.createSpyObj('TranslateService', ['instant']);
  const gridConfig = grid('grid-test').build();
  const restService = {};
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule],
      declarations: [LevelEnumCellRendererComponent],
      providers: [
        {
          provide: TranslateService,
          useValue: translateService
        }, {
          provide: GridDefinition,
          useValue: gridConfig
        }, {
          provide: RestService,
          useValue: restService
        }, {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(LevelEnumCellRendererComponent);
        component = fixture.componentInstance;
        component.gridDisplay = getBasicGridDisplay();
        component.columnDisplay = {
          id: 'column-1', show: true,
          widthCalculationStrategy: new Limited(200),
          headerPosition: 'left', contentPosition: 'left', showHeader: true,
          viewportName: 'mainViewport'
        };
        component.row = {
          ...new TestCaseLibrary(),
          id: 'tcln-1',
          data: {id: 'tcln-1'},
        };
        fixture.detectChanges();
      });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
