import {GridState, initialGridState} from '../model/state/grid.state';
import {checkAndSetInitialConfiguration} from './grid-service-functions';
import {Sort} from '../model/column-definition.model';
import {createColumn} from '../grid-testing/grid-testing-utils';

describe('GridServiceFunctions', () => {
  it('should add initial pagination', () => {
    const state: GridState = initialGridState();
    const nextState = checkAndSetInitialConfiguration({pagination: {page: 2, size: 10}}, state);
    expect(nextState.paginationState.page).toEqual(2);
    expect(nextState.paginationState.size).toEqual(10);
  });

  it('should not restore invalid initial pagination', () => {
    const state: GridState = initialGridState();
    const nextState = checkAndSetInitialConfiguration({pagination: {page: 2, size: 12}}, state);
    expect(nextState.paginationState.page).toEqual(0);
    expect(nextState.paginationState.size).toEqual(25);
  });

  it('should add initial sorted columns and override existing', () => {
    const state: GridState = initialGridState();
    state.columnState = {
      ...state.columnState,
      ids: ['name', 'reference'],
      entities: {name: createColumn('name'), reference: createColumn('reference')},
      sortedColumns: [{id: 'reference', sort: Sort.ASC}]
    };
    const nextState = checkAndSetInitialConfiguration({
      sortedColumns: [
        {
          id: 'name',
          sort: Sort.DESC
        }, {
          id: 'not-exist',
          sort: Sort.DESC
        }]
    }, state);
    expect(nextState.columnState.sortedColumns).toEqual([{id: 'name', sort: Sort.DESC}]);
  });
});

