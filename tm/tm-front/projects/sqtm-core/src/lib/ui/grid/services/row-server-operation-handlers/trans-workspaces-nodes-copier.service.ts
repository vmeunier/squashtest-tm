import {Injectable} from '@angular/core';
import {DataRow} from '../../model/data-row.model';

@Injectable({
  providedIn: 'root'
})
export class TransWorkspacesNodesCopierService {
  copiedRequirementNodes: DataRow[] = [];

  addCopiedNodes(rows: DataRow[]) {
    this.copiedRequirementNodes = rows;
  }
}
