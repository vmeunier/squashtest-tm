import {
  ColumnDefinition,
  ColumnDefinitionOption,
  ContentDriven,
  Fixed,
  I18nEnumOptions,
  Limited,
  WidthCalculationStrategy
} from './column-definition.model';
import {TextCellRendererComponent} from '../components/cell-renderers/text-cell-renderer/text-cell-renderer.component';
import {CheckBoxCellRendererComponent} from '../components/cell-renderers/check-box-cell-renderer/check-box-cell-renderer.component';
import {
  buildLevelEnumKeySort,
  I18nEnum,
  LevelEnum, TestCaseAutomatable,
  TestCaseStatus,
  TestCaseWeight
} from '../../../model/level-enums/level-enum';
import {Type} from '@angular/core';
// tslint:disable-next-line:max-line-length
import {EditableRichTextRendererComponent} from '../components/cell-renderers/editable-rich-text-renderer/editable-rich-text-renderer.component';
import {GridHeaderRendererComponent} from '../components/header-renderers/grid-header-renderer/grid-header-renderer.component';
import {IndexCellRendererComponent} from '../components/cell-renderers/index-cell-renderer/index-cell-renderer.component';
// tslint:disable-next-line:max-line-length
import {EditableDateCellRendererComponent} from '../components/cell-renderers/editable-date-cell-renderer/editable-date-cell-renderer.component';
// tslint:disable-next-line:max-line-length
import {EditableNumericCellRendererComponent} from '../components/cell-renderers/editable-numeric-cell-renderer/editable-numeric-cell-renderer.component';
import {Identifier} from '../../../model/entity.model';
// tslint:disable-next-line:max-line-length
import {EditableTextCellRendererComponent} from '../components/cell-renderers/editable-text-cell-renderer/editable-text-cell-renderer.component';
import {
  DateCellRendererComponent,
  DateTimeCellRendererComponent
} from '../components/cell-renderers/date-cell-renderer/date-cell-renderer.component';
import {DataRow, DataRowSortFunction} from './data-row.model';
import {RichTextRendererComponent} from '../components/cell-renderers/rich-text-renderer/rich-text-renderer.component';
import {NumericCellRendererComponent} from '../components/cell-renderers/numeric-cell-renderer/numeric-cell-renderer.component';
import {BooleanCellRendererComponent} from '../components/cell-renderers/boolean-cell-renderer/boolean-cell-renderer.component';
import {ResearchColumnPrototype} from '../../filters/state/filter.state';
import {GridViewportName} from './state/column.state';
import {RadioCellRendererComponent} from '../components/cell-renderers/radio-cell-renderer/radio-cell-renderer.component';
import {EnumEditableCellComponent} from '../../cell-renderer-common/enum-editable-cell/enum-editable-cell.component';
import {ColumnDisplay} from './column-display.model';
import {isTestCaseEditable, isTestCaseImportanceEditable} from '../../cell-renderer-common/editable-functions';
// tslint:disable-next-line:max-line-length
import {SelectableNumericCellRendererComponent} from '../components/cell-renderers/selectable-numeric-cell-renderer/selectable-numeric-cell-renderer.component';

export function column(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id);
}

export function textColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(TextCellRendererComponent);
}

export function centredTextColumn(id: string): ColumnDefinitionBuilder {
  return textColumn(id)
    .withHeaderPosition('center')
    .withContentPosition('center');
}

export function editableTextColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(EditableTextCellRendererComponent);
}

export function checkboxColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(CheckBoxCellRendererComponent);
}

export function booleanColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(BooleanCellRendererComponent);
}

export function editableDateColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(EditableDateCellRendererComponent);
}

export function dateColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(DateCellRendererComponent);
}

export function dateTimeColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(DateTimeCellRendererComponent);
}

export function editableNumericColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(EditableNumericCellRendererComponent);
}

export function numericColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(NumericCellRendererComponent).withHeaderPosition('center');
}

export function indexColumn(): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder('#')
    .withHeaderPosition('center')
    .withRenderer(IndexCellRendererComponent)
    .changeWidthCalculationStrategy(new Fixed(50))
    .disableSort();
}

export function editableRichTextColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(EditableRichTextRendererComponent);
}

export function richTextColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(RichTextRendererComponent);
}

export function selectRowColumn(): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder('select-row-column')
    .changeWidthCalculationStrategy(new Fixed(40))
    .disableSort()
    .enableForceRenderOnNoValue()
    .withRenderer(CheckBoxCellRendererComponent);
}

export function singleSelectRowColumn(): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder('single-select-row-column')
    .changeWidthCalculationStrategy(new Fixed(40))
    .disableSort()
    .enableForceRenderOnNoValue()
    .disableHeader()
    .withRenderer(RadioCellRendererComponent);
}

export function deleteColumn(renderer: Type<any>, id = 'delete', label = ''): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(renderer)
    .withLabel(label)
    .disableSort()
    .changeWidthCalculationStrategy(new Fixed(50));
}

export function testCaseImportanceColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withEnumRenderer(TestCaseWeight)
    .isEditable(isTestCaseImportanceEditable)
    .withHeaderPosition('center');
}

export function testCaseStatusColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withEnumRenderer(TestCaseStatus)
    .isEditable(isTestCaseEditable)
    .withHeaderPosition('center');
}

export function testCaseAutomatableColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withEnumRenderer(TestCaseAutomatable)
    .isEditable(isTestCaseEditable)
    .withHeaderPosition('center');
}

export function i18nEnumColumn(id: string, i18nEnum: I18nEnum<any>): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withEnumRenderer(i18nEnum)
    .withHeaderPosition('center');
}

export function levelEnumColumn(id: string, levelEnum: LevelEnum<any>): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withEnumRenderer(levelEnum)
    .withHeaderPosition('center')
    .withSortFunction(buildLevelEnumKeySort(levelEnum));
}

export function selectableNumericColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(SelectableNumericCellRendererComponent);
}

export class ColumnDefinitionBuilder {
  private widthCalculationStrategy: WidthCalculationStrategy = new Limited(100);
  private i18nKey: string;
  private label: string;
  private sortable = true;
  private draggable = true;
  private associateToFilter: Identifier;
  private resizable = true;
  private show = true;
  private sortIndex = 0;
  private cellRenderer: Type<any> = TextCellRendererComponent;
  private forceRenderOnNoValue = false;
  private headerRenderer: Type<any> = GridHeaderRendererComponent;
  private iconName: string;
  private iconTheme = 'outline';
  private cufId: Identifier;
  private titleI18nKey: string;
  private sortFunction: DataRowSortFunction;
  private headerPosition: POSITION = 'left';
  private contentPosition: POSITION = 'left';
  private editable: boolean | ((columnDisplay: ColumnDisplay, row: DataRow) => boolean);
  private columnPrototype: ResearchColumnPrototype;
  private showHeader = true;
  private viewportName: GridViewportName = 'mainViewport';
  private options?: ColumnDefinitionOption;

  constructor(private id: string) {
  }

  static column(id: string) {
    return new ColumnDefinitionBuilder(id);
  }

  changeWidthCalculationStrategy(strategy: WidthCalculationStrategy): ColumnDefinitionBuilder {
    this.widthCalculationStrategy = strategy;
    return this;
  }

  withI18nKey(i18nKey: string): ColumnDefinitionBuilder {
    this.i18nKey = i18nKey;
    return this;
  }

  disableSort() {
    this.sortable = false;
    return this;
  }

  withHeaderRenderer(renderer: Type<any>) {
    this.headerRenderer = renderer;
    return this;
  }

  withIconName(iconName: string) {
    this.iconName = iconName;
    return this;
  }

  withIconTheme(iconTheme: string) {
    this.iconTheme = iconTheme;
    return this;
  }

  enableDnd() {
    this.draggable = true;
    return this;
  }

  enableForceRenderOnNoValue() {
    this.forceRenderOnNoValue = true;
    return this;
  }

  withRenderer(cellRendererType: Type<any>) {
    this.cellRenderer = cellRendererType;
    return this;
  }

  withEnumRenderer(i18nEnum: I18nEnum<any>, showIcon = true, showLabel = false) {
    this.cellRenderer = EnumEditableCellComponent;
    const options: I18nEnumOptions = {kind: 'i18nEnum', i18nEnum, showIcon, showLabel};
    this.options = options;
    return this;
  }

  withLabel(label: string) {
    this.label = label;
    return this;
  }

  defaultAutoResize() {
    this.widthCalculationStrategy = new ContentDriven();
    this.resizable = false;
    return this;
  }

  withCufId(cufId: Identifier) {
    this.cufId = cufId;
    return this;
  }

  withTitleI18nKey(i18nKey: string) {
    this.titleI18nKey = i18nKey;
    return this;
  }

  withSortFunction(sortFunction: DataRowSortFunction) {
    this.sortFunction = sortFunction;
    return this;
  }

  withHeaderPosition(headerPosition: POSITION) {
    this.headerPosition = headerPosition;
    return this;
  }

  withContentPosition(contentPosition: POSITION) {
    this.contentPosition = contentPosition;
    return this;
  }

  withVisibility(visible: boolean) {
    this.show = visible;
    return this;
  }

  withColumnPrototype(columnPrototype: ResearchColumnPrototype) {
    this.columnPrototype = columnPrototype;
    return this;
  }

  withAssociatedFilter(filterId?: Identifier) {
    this.associateToFilter = filterId || this.id;
    return this;
  }

  isEditable(editable: boolean | ((columnDisplay: ColumnDisplay, row: DataRow) => boolean)) {
    this.editable = editable;
    return this;
  }

  disableHeader() {
    this.showHeader = false;
    return this;
  }

  withViewport(viewportName: GridViewportName) {
    this.viewportName = viewportName;
    return this;
  }

  withOptions(options: ColumnDefinitionOption) {
    this.options = options;
    return this;
  }

  build(): ColumnDefinition {
    return {...this as object as ColumnDefinition};
  }
}

export type POSITION = 'left' | 'center' | 'right';
