import {InjectionToken} from '@angular/core';
import {GridNodeGenerator} from './services/node-generators/grid-node-generator';
import {DataRowLoader} from './services/data-row-loaders/data-row-loader';
import {Identifier} from '../../model/entity.model';

export const GRID_NODE_GENERATOR = new InjectionToken<GridNodeGenerator>(
  'A service used by the grid to transform the internal row data in grid node objects, ready to be drawn by the component');

export const DATA_ROW_LOADER = new InjectionToken<DataRowLoader>(
  'A service used by the grid to load rows, add rows, updates them...');

export const COLUMN_ID = new InjectionToken<Identifier>('Token used to inject column id in Portal Component');
