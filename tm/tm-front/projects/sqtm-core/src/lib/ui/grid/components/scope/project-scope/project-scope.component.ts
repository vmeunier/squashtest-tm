import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {EntityScope, Scope} from '../../../../filters/state/filter.state';
import {Subject} from 'rxjs';
import {map, take} from 'rxjs/operators';
import {ReferentialDataService} from '../../../../../core/referential/services/referential-data.service';
import {DataRow} from '../../../model/data-row.model';
import {Identifier} from '../../../../../model/entity.model';
import {ProjectReference} from '../../../../../model/grids/data-row.type';
import {ListItem} from '../../../../workspace-common/components/forms/grouped-multi-list/grouped-multi-list.component';
import {CustomScopeKind} from '../../../model/state/definition.state';

@Component({
  selector: 'sqtm-core-project-scope',
  templateUrl: './project-scope.component.html',
  styleUrls: ['./project-scope.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectScopeComponent implements OnInit {

  valueChanged = new Subject<Scope>();

  close = new Subject<void>();

  items: ListItem[];

  private scope: Scope;

  customScopeKind: CustomScopeKind;

  customScopeEntities: EntityScope[] = [];

  get nzTabSelected(): number {
    if (this.scope && this.scope.kind === 'custom') {
      return 1;
    }
    return 0;
  }

  get initialSelectedEntities(): Identifier[] {
    if (this.scope && this.scope.kind === 'custom') {
      return this.scope.value.map(e => e.id);
    }
    return [];
  }

  constructor(protected cdRef: ChangeDetectorRef, private referentialDataService: ReferentialDataService) {
  }

  ngOnInit() {
  }

  changeSelection(item: ListItem) {
    let nextValue: EntityScope[] = this.scope.kind === 'project' ? [...this.scope.value] : [];
    if (item.selected) {
      const projectId = ProjectReference.fromString(item.id.toString()).id;
      nextValue = nextValue.concat({id: item.id.toString(), label: item.label, projectId});
    } else {
      const index = nextValue.findIndex(v => v.id === item.id);
      if (index >= 0) {
        nextValue.splice(index, 1);
      }
    }
    this.valueChanged.next({...this.scope, kind: 'project', value: nextValue});
  }

  setScope(scope: Scope) {
    this.referentialDataService.projectDatas$.pipe(
      take(1),
      map(projectDataMap => Object.values(projectDataMap)),
    ).subscribe(projectDatas => {
      const scopeValue = scope.value;
      const ids = scopeValue.map(value => value.id);
      this.items = projectDatas
        .sort((pa, pb) => {
          return pa.name.localeCompare(pb.name);
        }).map(projectData => {
          const ref = new ProjectReference(projectData.id).asString();
          return {
            id: ref,
            selected: ids.includes(ref),
            label: projectData.name
          };
        });
      this.scope = scope;
      this.cdRef.detectChanges();
    });
  }

  handleConfirm() {
    this.valueChanged.next({...this.scope, kind: 'custom', value: this.customScopeEntities});
    this.close.next();
  }

  changeTreeSelection(dataRows: DataRow[]) {
    this.customScopeEntities = dataRows.map(row => ({
      id: row.id as string,
      label: row.data['NAME'],
      projectId: row.projectId
    }));
  }

  handleCancel() {
    this.close.next();
  }
}
