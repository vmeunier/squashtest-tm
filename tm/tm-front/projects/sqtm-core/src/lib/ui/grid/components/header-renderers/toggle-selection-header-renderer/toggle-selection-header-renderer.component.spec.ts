import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';
import {ToggleSelectionHeaderRendererComponent} from './toggle-selection-header-renderer.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {GridTestingModule} from '../../../grid-testing/grid-testing.module';
import {TestingUtilsModule} from '../../../../testing-utils/testing-utils.module';

describe('ToggleSelectionHeaderRendererComponent', () => {
  let component: ToggleSelectionHeaderRendererComponent;
  let fixture: ComponentFixture<ToggleSelectionHeaderRendererComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ToggleSelectionHeaderRendererComponent],
      imports: [GridTestingModule, TestingUtilsModule],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToggleSelectionHeaderRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
