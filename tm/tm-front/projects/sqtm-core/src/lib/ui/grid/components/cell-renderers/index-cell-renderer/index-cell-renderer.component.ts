import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {AbstractCellRendererComponent} from '../abstract-cell-renderer/abstract-cell-renderer.component';
import {GridService} from '../../../services/grid.service';
import {PaginationDisplay} from '../../../model/pagination-display.model';

@Component({
  selector: 'sqtm-core-index-cell-renderer',
  template: `
    <div class="full-height full-width flex-column" style="text-align: center" [sqtmCoreSelectable]="row.id" [sqtmCoreSelectableDisabled]="!row.selectable">
        <span *ngIf="grid.paginationDisplay$ | async as pagination">
         {{getIndex(pagination)}}
        </span>
    </div>
  `,
  styleUrls: ['./index-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IndexCellRendererComponent extends AbstractCellRendererComponent implements OnInit {

  constructor(public grid: GridService, public cdRef: ChangeDetectorRef) {
    super(grid, cdRef);
  }

  ngOnInit() {
  }

  getIndex(pagination: PaginationDisplay) {
    if (pagination.active && pagination.size !== -1) {
      return this.index + 1 + pagination.page * pagination.size;
    }
    return this.index + 1;
  }
}
