import {Identifier} from '../../../../model/entity.model';
import {Type} from '@angular/core';

export interface OpenContextualMenuAction {
  id: Identifier;
  x: number;
  y: number;
  component?: Type<any>;
}
