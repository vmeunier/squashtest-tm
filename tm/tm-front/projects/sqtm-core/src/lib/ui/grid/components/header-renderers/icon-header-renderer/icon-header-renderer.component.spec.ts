import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {IconHeaderRendererComponent} from './icon-header-renderer.component';
import {GridTestingModule} from '../../../grid-testing/grid-testing.module';
import {TranslateService} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TestingUtilsModule} from '../../../../testing-utils/testing-utils.module';

describe('IconHeaderRendererComponent', () => {
  let component: IconHeaderRendererComponent;
  let fixture: ComponentFixture<IconHeaderRendererComponent>;
  const translateService = jasmine.createSpyObj('TranslateService', ['instant']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [IconHeaderRendererComponent],
      imports: [GridTestingModule, TestingUtilsModule],
      providers: [{
        provide: TranslateService,
        useValue: translateService
      }],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IconHeaderRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
