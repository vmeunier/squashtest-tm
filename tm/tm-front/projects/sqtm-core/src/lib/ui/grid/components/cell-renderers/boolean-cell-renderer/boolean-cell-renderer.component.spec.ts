import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {BooleanCellRendererComponent} from './boolean-cell-renderer.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TestingUtilsModule} from '../../../../testing-utils/testing-utils.module';
import {GridDefinition} from '../../../model/grid-definition.model';
import {RestService} from '../../../../../core/services/rest.service';
import {GridService} from '../../../services/grid.service';
import {gridServiceFactory} from '../../../grid.service.provider';
import {ReferentialDataService} from '../../../../../core/referential/services/referential-data.service';
import {grid} from '../../../../../model/grids/grid-builders';

describe('BooleanCellRendererComponent', () => {
  let component: BooleanCellRendererComponent;
  let fixture: ComponentFixture<BooleanCellRendererComponent>;

  const gridConfig = grid('grid-test').build();
  const restService = {};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [BooleanCellRendererComponent],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig
        }, {
          provide: RestService,
          useValue: restService
        }, {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        },
      ],
      imports: [TestingUtilsModule],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BooleanCellRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
