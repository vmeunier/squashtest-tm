import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {PaginationSizeSelectorComponent} from './pagination-size-selector.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

describe('PaginationSizeSelectorComponent', () => {
  let component: PaginationSizeSelectorComponent;
  let fixture: ComponentFixture<PaginationSizeSelectorComponent>;
  const translateService = jasmine.createSpyObj('TranslateService', ['instant']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: [{
        provide: TranslateService,
        useValue: translateService
      }],
      declarations: [PaginationSizeSelectorComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(PaginationSizeSelectorComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
      });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
