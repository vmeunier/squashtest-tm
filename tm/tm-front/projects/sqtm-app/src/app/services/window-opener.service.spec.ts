import {TestBed} from '@angular/core/testing';

import {WindowOpenerService} from './window-opener.service';
import {RouterTestingModule} from '@angular/router/testing';
import {AppTestingUtilsModule} from '../utils/testing-utils/app-testing-utils.module';

describe('WindowOpenerService', () => {
  let service: WindowOpenerService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, AppTestingUtilsModule],
      providers: [WindowOpenerService]
    });
    service = TestBed.inject(WindowOpenerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
