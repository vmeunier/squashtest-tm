import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OpenCloseCellRendererComponent } from './open-close-cell-renderer.component';
import {GridTestingModule} from 'sqtm-core';
import {TranslateModule} from '@ngx-translate/core';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('OpenCloseCellRendererComponent', () => {
  let component: OpenCloseCellRendererComponent;
  let fixture: ComponentFixture<OpenCloseCellRendererComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [GridTestingModule, AppTestingUtilsModule, TranslateModule.forRoot()],
      declarations: [ OpenCloseCellRendererComponent ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenCloseCellRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
