import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RequirementFoldableRowComponent } from './requirement-foldable-row.component';
import {GridTestingModule, GridViewportService} from 'sqtm-core';
import {TranslateModule} from '@ngx-translate/core';
import {AppTestingUtilsModule} from '../../../../utils/testing-utils/app-testing-utils.module';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('RequirementFoldableRowComponent', () => {
  let component: RequirementFoldableRowComponent;
  let fixture: ComponentFixture<RequirementFoldableRowComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [GridTestingModule, AppTestingUtilsModule, TranslateModule.forRoot()],
      declarations: [ RequirementFoldableRowComponent ],
      providers: [GridViewportService],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequirementFoldableRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
