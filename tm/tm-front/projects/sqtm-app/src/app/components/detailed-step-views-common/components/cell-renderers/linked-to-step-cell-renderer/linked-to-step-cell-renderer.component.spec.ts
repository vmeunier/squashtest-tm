import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { LinkedToStepCellRendererComponent } from './linked-to-step-cell-renderer.component';
import {GridTestingModule} from 'sqtm-core';
import {TranslateModule} from '@ngx-translate/core';
import {DetailedTestStepViewService} from '../../../../../pages/detailed-views/detailed-test-step-view/services/detailed-test-step-view.service';
import {of} from 'rxjs';
import {DetailedTestStepViewComponentData} from '../../../../../pages/detailed-views/detailed-test-step-view/containers/detailed-test-step-view/detailed-test-step-view.component';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {STEP_VIEW_REQUIREMENT_LINK_SERVICE} from '../../../detailed-step-view.constant.ts.constant';

describe('LinkedToStepCellRendererComponent', () => {
  let component: LinkedToStepCellRendererComponent;
  let fixture: ComponentFixture<LinkedToStepCellRendererComponent>;

  const viewService = jasmine.createSpyObj<DetailedTestStepViewService>(['load']);
  viewService.componentData$ = of({permissions: {canWrite: true}} as DetailedTestStepViewComponentData);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [GridTestingModule, AppTestingUtilsModule, TranslateModule.forRoot()],
      declarations: [ LinkedToStepCellRendererComponent ],
      providers: [
        {
          provide: STEP_VIEW_REQUIREMENT_LINK_SERVICE,
          useValue: viewService
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkedToStepCellRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
