import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DetailedStepCoverageTableComponent } from './detailed-step-coverage-table.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('DetailedStepCoverageTableComponent', () => {
  let component: DetailedStepCoverageTableComponent;
  let fixture: ComponentFixture<DetailedStepCoverageTableComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailedStepCoverageTableComponent ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailedStepCoverageTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
