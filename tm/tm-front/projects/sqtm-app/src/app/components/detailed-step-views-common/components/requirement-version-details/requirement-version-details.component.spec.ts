import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {RequirementVersionDetailsComponent} from './requirement-version-details.component';
import {TranslateModule} from '@ngx-translate/core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../utils/testing-utils/app-testing-utils.module';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('RequirementVersionDetailsComponent', () => {
  let component: RequirementVersionDetailsComponent;
  let fixture: ComponentFixture<RequirementVersionDetailsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule, TranslateModule.forRoot()],
      declarations: [RequirementVersionDetailsComponent],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequirementVersionDetailsComponent);
    component = fixture.componentInstance;
    component.requirementVersionId = 42;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
