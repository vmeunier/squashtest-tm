import {ComponentFixture, TestBed} from '@angular/core/testing';

import {CustomExportScopeSelectorOverlayComponent} from './custom-export-scope-selector-overlay.component';
import {TranslatePipe, TranslateService} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {mockPassThroughTranslateService} from '../../../../utils/testing-utils/mocks.service';
import {TranslateMockPipe} from '../../../../utils/testing-utils/mocks.pipe';
import {AppTestingUtilsModule} from '../../../../utils/testing-utils/app-testing-utils.module';

describe('CustomExportScopeSelectorOverlayComponent', () => {
  let component: CustomExportScopeSelectorOverlayComponent;
  let fixture: ComponentFixture<CustomExportScopeSelectorOverlayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule],
      declarations: [CustomExportScopeSelectorOverlayComponent],
      providers: [
        {provide: TranslateService, useValue: mockPassThroughTranslateService()},
        {provide: TranslatePipe, useClass: TranslateMockPipe},
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomExportScopeSelectorOverlayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
