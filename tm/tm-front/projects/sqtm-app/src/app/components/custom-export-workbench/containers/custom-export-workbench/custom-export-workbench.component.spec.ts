import {ComponentFixture, TestBed} from '@angular/core/testing';

import {CustomExportWorkbenchComponent} from './custom-export-workbench.component';
import {AppTestingUtilsModule} from '../../../../utils/testing-utils/app-testing-utils.module';
import {ReactiveFormsModule} from '@angular/forms';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {RouterTestingModule} from '@angular/router/testing';
import {ReferentialDataService} from 'sqtm-core';
import {mockReferentialDataService} from '../../../../utils/testing-utils/mocks.service';
import {CreateCustomExportViewService} from '../../../../pages/custom-report-workspace/create-custom-export-view/services/create-custom-export-view.service';

describe('CustomExportWorkbenchComponent', () => {
  let component: CustomExportWorkbenchComponent;
  let fixture: ComponentFixture<CustomExportWorkbenchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, ReactiveFormsModule, RouterTestingModule],
      declarations: [CustomExportWorkbenchComponent],
      providers: [
        {provide: CreateCustomExportViewService, useValue: jasmine.createSpyObj(['fetchWorkbenchData'])},
        // Transitive dependency for CustomExportWorkbenchService
        {provide: ReferentialDataService, useValue: mockReferentialDataService()},
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomExportWorkbenchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
