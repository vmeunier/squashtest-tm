import {Injectable} from '@angular/core';
import {
  BindableEntity,
  createStore,
  CustomExportColumn,
  CustomExportEntityType,
  CustomExportModel,
  CustomExportWorkbenchData,
  CustomFieldBindingData,
  GlobalConfigurationState,
  ProjectDataMap,
  ReferentialDataService
} from 'sqtm-core';
import {
  CraftedExport,
  CustomExportIdentifiedColumnsByEntityType,
  CustomExportScope,
  CustomExportWorkbenchState,
  getIdentifiedColumnsByEntityType,
  IdentifiedExportColumn,
  initialCustomExportWorkbenchState
} from '../state/custom-export-workbench.state';
import {map, switchMap, take, tap} from 'rxjs/operators';
import {combineLatest, Observable} from 'rxjs';
import * as _ from 'lodash';

@Injectable()
export class CustomExportWorkbenchService {

  constructor(private readonly referentialDataService: ReferentialDataService) {
  }

  private store = createStore<CustomExportWorkbenchState>(initialCustomExportWorkbenchState, {
    id: 'CustomExportWorkbench',
    logDiff: 'simple'
  });

  public state$: Observable<CustomExportWorkbenchState> = this.store.state$;

  load(workbenchData: CustomExportWorkbenchData): void {
    this.state$.pipe(
      take(1),
      map(state => ({...state, containerId: workbenchData.containerId})),
    ).subscribe((state: CustomExportWorkbenchState) => this.loadFromModel(workbenchData.customExport, state));
  }

  changeName(name: string): Observable<any> {
    return this.state$.pipe(
      take(1),
      tap((state) => this.store.commit(withUpdatedCraftedExport({name}, state)))
    );
  }

  changeScope(scope: CustomExportScope): Observable<any> {
    return this.state$.pipe(
      take(1),
      switchMap((state) => this.updateScopeAndAvailableColumns(scope, state)),
      tap((newState) => this.store.commit(newState))
    );
  }

  addSelectedColumns(ids: string[]): void {
    this.state$.pipe(
      take(1),
      map((state: CustomExportWorkbenchState) => this.withAddedSelectedColumns(ids, state)),
      tap(state => this.store.commit(state)),
    ).subscribe();
  }

  removeSelectedColumn(columnToRemoveId: string): void {
    this.state$.pipe(
      take(1),
      map((state: CustomExportWorkbenchState) => {
        const columns = state.craftedExport.columns.filter(col => col.id !== columnToRemoveId);
        return {...state, craftedExport: {...state.craftedExport, columns}};
      }),
      tap(state => this.store.commit(state)),
    ).subscribe();
  }

  complete(): void {
    this.store.complete();
  }

  private loadFromModel(customExportModel: CustomExportModel | undefined, state: CustomExportWorkbenchState): void {
    if (customExportModel == null) {
      this.withUpdatedAvailableColumns(state).subscribe(updatedState => this.store.commit(updatedState));
    } else {
      const scope: CustomExportScope = {
        name: customExportModel.scopeNodeName,
        id: customExportModel.scopeNodeId,
        projectId: customExportModel.scopeProjectId,
      };

      this.updateScopeAndAvailableColumns(scope, state).pipe(
        map(newState => {
          const columnIds = customExportModel.columns.map(col => buildColumnTemporaryId(col));
          const updatedState: CustomExportWorkbenchState = {
            ...newState,
            craftedExport: {
              ...newState.craftedExport,
              existingNodeId: customExportModel.customReportLibraryNodeId,
              name: customExportModel.name,
            },
          };

          return this.withAddedSelectedColumns(columnIds, updatedState);
        })
      ).subscribe(finalState => this.store.commit(finalState));
    }
  }

  private updateScopeAndAvailableColumns(scope: CustomExportScope,
                                         state: CustomExportWorkbenchState): Observable<CustomExportWorkbenchState> {
    if (!scopeNodeWasDeleted(scope)) {
      const updatedState = withUpdatedCraftedExport({scope}, state);
      return this.withUpdatedAvailableColumns(updatedState);
    } else {
      return this.withUpdatedAvailableColumns(state);
    }
  }

  private withAddedSelectedColumns(ids: string[], state: CustomExportWorkbenchState): CustomExportWorkbenchState {
    const addedColumns = ids.map(id => findIdentifiedColumnById(id, state.availableColumns)).filter(Boolean);
    const columns = sortColumns(
      _.uniq([...state.craftedExport.columns, ...addedColumns]),
      Object.values(state.availableColumns).flat(1));
    return {...state, craftedExport: {...state.craftedExport, columns}};
  }

  private withUpdatedAvailableColumns(currentState: CustomExportWorkbenchState): Observable<CustomExportWorkbenchState> {
    return combineLatest([
      this.referentialDataService.projectDatas$,
      this.referentialDataService.globalConfiguration$,
    ]).pipe(
      take(1),
      map(([projectDataMap, globalConf]) => this.withAvailableColumns(currentState, projectDataMap, globalConf)),
      map((updatedState: CustomExportWorkbenchState) => withUnavailableCustomFieldAttributesRemoved(updatedState)));
  }

  // Build the available column list from the standard column and the available custom fields in the current scope's project.
  private withAvailableColumns(state: CustomExportWorkbenchState,
                               projectDataMap: ProjectDataMap,
                               globalConfiguration: GlobalConfigurationState): CustomExportWorkbenchState {
    const showMilestoneColumns = globalConfiguration.milestoneFeatureEnabled;
    const availableColumns = getIdentifiedColumnsByEntityType(showMilestoneColumns);

    const currentScope = state.craftedExport.scope;
    const projectData = projectDataMap[currentScope?.projectId];

    if (projectData) {
      Object.keys(availableColumns).forEach(entity => {
        const availableCustomFields: CustomFieldBindingData[] = projectData.customFieldBinding[entity];

        if (availableCustomFields) {
          const availableColumnsForEntity: IdentifiedExportColumn[] = [ // Not inlined to preserve type safety
            ...availableColumns[entity],
            ...availableCustomFields.map((cfbd: CustomFieldBindingData) => ({
              id: makeTemporaryCufColumnId(cfbd.bindableEntity, cfbd.customField.id),
              label: cfbd.customField.label,
              cufId: cfbd.customField.id,
              entityType: cfbd.bindableEntity,
              // CUFs do have column names server-side such as 'CAMPAIGN_CUF' but we don't need it (and don't know it)
              columnName: null,
            })),
          ];
          availableColumns[entity] = availableColumnsForEntity;
        }
      });
    }

    return {...state, availableColumns};
  }
}

function withUnavailableCustomFieldAttributesRemoved(state: CustomExportWorkbenchState): CustomExportWorkbenchState {
  const columns = state.craftedExport.columns.filter(col => findIdentifiedColumnById(col.id, state.availableColumns) != null);
  return withUpdatedCraftedExport({columns}, state);
}

function withUpdatedCraftedExport(changes: Partial<CraftedExport>, state: CustomExportWorkbenchState): CustomExportWorkbenchState {
  return {...state, craftedExport: {...state.craftedExport, ...changes}};
}

function findIdentifiedColumnById(id: string, availableColumns: CustomExportIdentifiedColumnsByEntityType): IdentifiedExportColumn {
  return Object.values(availableColumns)
    .flat(1)
    .find((available: IdentifiedExportColumn) => available.id === id);
}

function sortColumns(columns: Readonly<IdentifiedExportColumn[]>, flatColumnArray: IdentifiedExportColumn[]): IdentifiedExportColumn[] {
  const copy = [...columns];
  copy.sort((a, b) => {
    const indexA = flatColumnArray.indexOf(flatColumnArray.find(col => col.id === a.id));
    const indexB = flatColumnArray.indexOf(flatColumnArray.find(col => col.id === b.id));
    return indexA - indexB;
  });
  return copy;
}

function makeTemporaryCufColumnId(bindableEntity: BindableEntity, customFieldId: number): any {
  return `${bindableEntity}-${customFieldId}`;
}

function buildColumnTemporaryId(col: CustomExportColumn): string {
  return col.cufId ? makeTemporaryCufColumnId(asBindableEntity(col.entityType), col.cufId) : col.columnName;
}

function asBindableEntity(entityType: CustomExportEntityType): BindableEntity {
  switch (entityType) {
    case CustomExportEntityType.CAMPAIGN:
      return BindableEntity.CAMPAIGN;
    case CustomExportEntityType.EXECUTION_STEP:
      return BindableEntity.EXECUTION_STEP;
    case CustomExportEntityType.EXECUTION:
      return BindableEntity.EXECUTION;
    case CustomExportEntityType.ITERATION:
      return BindableEntity.ITERATION;
    case CustomExportEntityType.TEST_CASE:
      return BindableEntity.TEST_CASE;
    case CustomExportEntityType.TEST_SUITE:
      return BindableEntity.TEST_SUITE;
    default:
      throw new Error('Invalid custom export entity type for CUF binding : ' + entityType);
  }
}

function scopeNodeWasDeleted(customExportScope: CustomExportScope): Boolean {
  // We check if we have an empty scope node name, which means the scope node was deleted.
  return !customExportScope.name;
}
