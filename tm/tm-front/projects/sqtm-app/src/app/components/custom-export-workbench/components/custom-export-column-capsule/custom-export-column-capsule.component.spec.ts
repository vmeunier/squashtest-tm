import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomExportColumnCapsuleComponent } from './custom-export-column-capsule.component';
import {TranslateService} from '@ngx-translate/core';
import {mockPassThroughTranslateService} from '../../../../utils/testing-utils/mocks.service';

describe('CustomExportColumnCapsuleComponent', () => {
  let component: CustomExportColumnCapsuleComponent;
  let fixture: ComponentFixture<CustomExportColumnCapsuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomExportColumnCapsuleComponent ],
      providers: [
        { provide: TranslateService, useValue: mockPassThroughTranslateService() },
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomExportColumnCapsuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
