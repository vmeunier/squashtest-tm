import {ComponentFixture, TestBed} from '@angular/core/testing';

import {CustomExportColumnSelectorComponent} from './custom-export-column-selector.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {RestService} from 'sqtm-core';
import {mockPassThroughTranslateService, mockRestService} from '../../../../utils/testing-utils/mocks.service';
import {TranslateService} from '@ngx-translate/core';
import {CustomExportWorkbenchService} from '../../services/custom-export-workbench.service';


describe('CustomExportColumnSelectorComponent', () => {
  let component: CustomExportColumnSelectorComponent;
  let fixture: ComponentFixture<CustomExportColumnSelectorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CustomExportColumnSelectorComponent],
      providers: [
        {provide: RestService, useValue: mockRestService()},
        {provide: TranslateService, useValue: mockPassThroughTranslateService()},
        {provide: CustomExportWorkbenchService, useClass: CustomExportWorkbenchService}
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomExportColumnSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
