import {AXIS_CONVERTERS} from './charts.constants';
import {LevelEnumAxisConverter} from './converters/level-enum-axis-converter.service';
import {DateByDayToStringAxisConverter} from './converters/date-by-day-to-string-axis-converter.service';
import {InfoListItemConverter} from './converters/info-list-item-converter';
import {PassTroughAxisConverter} from './converters/pass-trough-axis-converter';
import {ChartsTranslateService} from './services/charts-translate.service';
import {BooleanAxisConverter} from './converters/boolean-axis-converter.service';
import {DateByWeekToStringAxisConverter} from './converters/date-by-week-to-string-axis-converter.service';
import {DateByMonthToStringAxisConverter} from './converters/date-by-month-to-string-axis-converter.service';
import {DateByYearToStringAxisConverter} from './converters/date-by-year-to-string-axis-converter.service';

export const chartServiceProviders =  [
  {
    provide: AXIS_CONVERTERS,
    useClass: LevelEnumAxisConverter,
    multi: true
  },
  {
    provide: AXIS_CONVERTERS,
    useClass: DateByDayToStringAxisConverter,
    multi: true
  },
  {
    provide: AXIS_CONVERTERS,
    useClass: DateByWeekToStringAxisConverter,
    multi: true
  },
  {
    provide: AXIS_CONVERTERS,
    useClass: DateByMonthToStringAxisConverter,
    multi: true
  },
  {
    provide: AXIS_CONVERTERS,
    useClass: DateByYearToStringAxisConverter,
    multi: true
  },
  {
    provide: AXIS_CONVERTERS,
    useClass: InfoListItemConverter,
    multi: true
  },
  {
    provide: AXIS_CONVERTERS,
    useClass: BooleanAxisConverter,
    multi: true
  },
  {
    provide: PassTroughAxisConverter,
    useClass: PassTroughAxisConverter
  },
  {
    provide: ChartsTranslateService,
    useClass: ChartsTranslateService
  }
];
