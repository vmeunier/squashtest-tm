import {ChangeDetectionStrategy, Component} from '@angular/core';
import {ChartDefinitionService, ChartType, ReferentialDataService} from 'sqtm-core';
import {Config, Layout, newPlot} from 'plotly.js';
import {AbstractCustomReportChart} from '../abstract-custom-report-chart';
import {ChartsTranslateService} from '../../../services/charts-translate.service';
import {chartsLogger} from '../../../charts.logger';
import {TranslateService} from '@ngx-translate/core';
import * as _ from 'lodash';

const logger = chartsLogger.compose('CustomReportCumulativeChartComponent');

@Component({
  selector: 'sqtm-app-custom-report-cumulative-chart',
  templateUrl: './custom-report-cumulative-chart.component.html',
  styleUrls: ['./custom-report-cumulative-chart.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CustomReportCumulativeChartComponent extends AbstractCustomReportChart {

  constructor(protected chartsTranslateService: ChartsTranslateService,
              protected chartDefinitionService: ChartDefinitionService,
              protected referentialDataService: ReferentialDataService,
              protected translateService: TranslateService) {
    super(chartsTranslateService, chartDefinitionService, referentialDataService, translateService);
  }

  protected getHandledType(): ChartType {
    return ChartType.CUMULATIVE;
  }

  protected generateChart() {
    if (this.isDateChart()) {
      this.generateDateChart();
    } else {
      this.generateDefaultChart();
    }
  }

  private generateDateChart() {
    const rawLabels = this.chartDefinition.abscissa.flat(2);
    if (this.hasNeverDateValue(rawLabels)) {
      logger.debug(`Generate Chart with never value ${this.chartDefinition.name}`);
      this.generateDateChartWithNeverValue(rawLabels);
    } else {
      this.generateDateChartWithoutNeverValue(rawLabels);
    }
  }

  private generateDateChartWithoutNeverValue(rawLabels: number[]) {
    const yValues = Object.values<number[]>(this.chartDefinition.series)[0].flat();
    const accumulation = this.accumulate(yValues);

    const rawDates = this.convertDates(this.chartDefinition.axis[0], rawLabels);
    const data: any = [{
      x: rawDates,
      y: accumulation.values,
      type: 'scatter',
      fill: 'tozeroy',
      line: {
        shape: 'hv',
        color: this.defaultColours[0]
      },
      hovertemplate: '%{x}<br>%{y}<extra></extra>',
      marker: {
        color: this.defaultColours[0]
      },
    }];

    logger.debug('Final plotly data', [data]);
    const layout: Partial<Layout> = {
      title: this.chartTitle,
      showlegend: false,
      yaxis: {
        title: {
          text: this.getMeasureText(),
          font: this.axisTitleFont
        },
        tickfont: this.legendFont,
        fixedrange: true,
      },
      xaxis: {
        type: 'date',
        title: {
          text: this.getAxisText(),
          font: this.axisTitleFont
        },
        tickfont: this.legendFont,
        fixedrange: true,
      }
    };

    if (this.isCompactLayout()) {
      layout.margin = {
        t: 25
      };
    }

    newPlot(this.chartContainer.nativeElement, data, layout, this.getDefaultConfig());
  }

  private accumulate(yValues: any[]) {
    return yValues.reduce((acc, value) => {
      acc.currentSum = acc.currentSum + value;
      acc.values.push(acc.currentSum);
      return acc;
    }, {currentSum: 0, values: []});
  }

  private generateDefaultChart() {
    const rawLabels = this.chartDefinition.abscissa.flat(2);
    const yValues = Object.values<number[]>(this.chartDefinition.series)[0].flat();

    const accumulation = this.accumulate(yValues);

    const convertedLabels = this.chartsTranslateService.convertAxisLabels(this.chartDefinition.axis[0], rawLabels, this.chartDefinition);

    const data: any = [{
      x: convertedLabels,
      y: accumulation.values,
      type: 'scatter',
      fill: 'tozeroy',
      hovertemplate: '%{x}<br>%{y}<extra></extra>',
      marker: {
        color: this.defaultColours[0]
      },
      line: {
        color: this.defaultColours[0]
      },
    }];

    const layout: Partial<Layout> = {
      title: this.chartTitle,
      showlegend: false,
      yaxis: {
        title: {
          text: this.getMeasureText(),
          font: this.axisTitleFont
        },
        fixedrange: true,
        tickfont: this.legendFont,
      },
      xaxis: {
        type: 'category',
        title: {
          text: this.getAxisText(),
          font: this.axisTitleFont
        },
        fixedrange: true,
        tickfont: this.legendFont,
      }
    };

    if (this.isCompactLayout()) {
      layout.margin = {
        t: 25
      };
    }

    const config: Partial<Config> = this.getDefaultConfig();

    newPlot(this.chartContainer.nativeElement, data, layout, config);
  }

  private generateDateChartWithNeverValue(rawLabels: any[]) {
    const yValues = Object.values<number[]>(this.chartDefinition.series)[0].flat();
    const neverLabel = _.capitalize(this.translateService.instant('sqtm-core.generic.label.never'));
    const neverValue = this.extractNeverDateValue(rawLabels, yValues);
    const rawDates = this.convertDates(this.chartDefinition.axis[0], this.filterNeverDateValue(rawLabels));
    const accumulation = this.accumulate(yValues);
    const data: any = [
      {
        x: [neverLabel],
        y: [neverValue],
        type: 'bar',
        hovertemplate: '%{x}<br>%{y}<extra></extra>',
        marker: {
          color: this.defaultColours[1]
        },
      },
      {
        x: rawDates,
        y: accumulation.values,
        xaxis: 'x2',
        type: 'scatter',
        fill: 'tozeroy',
        hovertemplate: '%{x}<br>%{y}<extra></extra>',
        marker: {
          color: this.defaultColours[0]
        },
        line: {
          color: this.defaultColours[0],
          shape: 'hv'
        },
      }
    ];

    logger.debug('Final plotly data', [data]);

    const layout: Partial<Layout> = {
      title: this.chartTitle,
      showlegend: false,
      yaxis: {
        title: {
          text: this.getMeasureText(),
          font: this.axisTitleFont
        },
        fixedrange: true,
        tickfont: this.legendFont,
      },
      xaxis: {
        domain: [0, 0.1],
        fixedrange: true,
        tickfont: this.legendFont,
      },
      xaxis2: {
        type: 'date',
        fixedrange: true,
        domain: [0.1, 1],
        tickfont: this.legendFont,
        title: {
          text: this.getAxisText(),
          font: this.axisTitleFont
        },
      }
    };

    if (this.isCompactLayout()) {
      layout.margin = {
        t: 25
      };
    }

    const config: Partial<Config> = this.getDefaultConfig();

    newPlot(this.chartContainer.nativeElement, data, layout, config);
  }
}
