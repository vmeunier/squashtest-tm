import * as locale_fr from 'plotly.js/lib/locales/fr';
import * as locale_de from 'plotly.js/lib/locales/de';
import * as locale_es from 'plotly.js/lib/locales/es';
import * as Plotly from 'plotly.js';

export function registerPlotlyLocale(locale: string){
  switch (locale) {
    case 'fr' :
      // register method is not exposed in types ? Have to hack...
      Plotly['register'](locale_fr);
      break;
    case 'de' :
      Plotly['register'](locale_de);
      break;
    case 'es' :
      Plotly['register'](locale_es);
      break;
  }
}
