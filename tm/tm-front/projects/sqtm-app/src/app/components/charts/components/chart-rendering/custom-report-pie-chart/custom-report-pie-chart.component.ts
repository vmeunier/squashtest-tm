import {ChangeDetectionStrategy, Component} from '@angular/core';
import {ChartDefinitionService, ChartType, ReferentialDataService} from 'sqtm-core';
import * as Plotly from 'plotly.js';
import {Layout} from 'plotly.js';
import {chartsLogger} from '../../../charts.logger';
import {ChartsTranslateService} from '../../../services/charts-translate.service';
import {AbstractCustomReportChart} from '../abstract-custom-report-chart';
import {TranslateService} from '@ngx-translate/core';

const logger = chartsLogger.compose('CustomReportPieChartComponent');

@Component({
  selector: 'sqtm-app-custom-report-pie-chart',
  templateUrl: './custom-report-pie-chart.component.html',
  styleUrls: ['./custom-report-pie-chart.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CustomReportPieChartComponent extends AbstractCustomReportChart {

  constructor(protected chartsTranslateService: ChartsTranslateService,
              protected chartDefinitionService: ChartDefinitionService,
              protected referentialDataService: ReferentialDataService,
              protected translateService: TranslateService) {
    super(chartsTranslateService, chartDefinitionService, referentialDataService, translateService);
  }

  protected generateChart() {
    logger.debug(`Generating chart for chart definition ${this.chartDefinition.name}`, [this.chartDefinition]);
    const rawLabels = this.chartDefinition.abscissa.flat(2);
    const fullLabels = this.chartsTranslateService.convertAxisLabels(this.chartDefinition.axis[0], rawLabels, this.chartDefinition);

    let characterLength = 25;
    if (this.isCompactLayout()) {
      characterLength = 17;
    }

    const shortLabels = fullLabels.map(l => l.slice(0, characterLength));
    const values = Object.values(this.chartDefinition.series)[0];

    const colors = this.getColors(rawLabels, this.chartDefinition.axis[0]);

    const data: any = [{
      values: values,
      labels: shortLabels,
      type: 'pie',
      textfont: this.legendFont,
      texttemplate: '%{percent} (%{value})',
      hovertemplate: '%{customdata[0]}<br> %{percent}<br>(%{value})<extra></extra>',
      customdata: fullLabels,
      sort: false,
      textposition: 'inside',
      marker: {
        colors
      }
    }];

    const layout: Partial<Layout> = {
      ...this.baseLayout,
      showlegend: true,
      xaxis: {}
    };

    const config: Partial<Plotly.Config> = {staticPlot: true};

    Plotly.newPlot(this.chartContainer.nativeElement, data, layout, config);

    this.chartContainer.nativeElement.on('plotly_click', function (event) {
      logger.debug(event);
    });
  }

  protected getHandledType(): ChartType {
    return ChartType.PIE;
  }

}
