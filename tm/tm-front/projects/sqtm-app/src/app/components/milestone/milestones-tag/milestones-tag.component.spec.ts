import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {MilestonesTagComponent} from './milestones-tag.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {DialogService} from 'sqtm-core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TranslateModule} from '@ngx-translate/core';
import {OverlayModule} from '@angular/cdk/overlay';
import {AppTestingUtilsModule} from '../../../utils/testing-utils/app-testing-utils.module';

describe('MilestonesTagComponent', () => {
  let component: MilestonesTagComponent;
  let fixture: ComponentFixture<MilestonesTagComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, HttpClientTestingModule, TranslateModule.forRoot(), OverlayModule],
      declarations: [MilestonesTagComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [DialogService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MilestonesTagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
