import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {MilestoneDialogComponent} from './milestone-dialog.component';
import {DialogReference} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {MilestoneDialogConfiguration} from './milestone.dialog.configuration';
import {EMPTY} from 'rxjs';
import {AppTestingUtilsModule} from '../../../utils/testing-utils/app-testing-utils.module';

describe('MilestoneDialogComponent', () => {
  let component: MilestoneDialogComponent;
  let fixture: ComponentFixture<MilestoneDialogComponent>;
  const overlayReference = jasmine.createSpyObj(['attachments']);

  overlayReference.attachments.and.returnValue(EMPTY);
  const dialogReference: DialogReference<MilestoneDialogConfiguration, number[]> =
    new DialogReference<MilestoneDialogConfiguration, number[]>(
      'bind-milestone',
      null,
      overlayReference,
      {titleKey: 'titleKey', milestoneViews: [], samePerimeter: true, id: '', checkedIds: []}
    );

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [MilestoneDialogComponent],
      imports: [AppTestingUtilsModule, HttpClientTestingModule],
      providers: [
        {provide: DialogReference, useValue: dialogReference}
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MilestoneDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
