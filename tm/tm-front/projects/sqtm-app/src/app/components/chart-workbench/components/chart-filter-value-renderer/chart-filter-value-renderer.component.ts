import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {
  AbstractFilterValueRenderer,
  ChartOperation,
  DateFilterValue,
  EntityType,
  Filter,
  FilterOperation, MultipleStringFilterValue
} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {ChartWorkbenchUtils} from '../chart-workbench.utils';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'sqtm-app-chart-filter-value-renderer',
  templateUrl: './chart-filter-value-renderer.component.html',
  styleUrls: ['./chart-filter-value-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChartFilterValueRendererComponent extends AbstractFilterValueRenderer implements OnInit {

  get filter(): ChartWorkbenchFilter {
    return this._filter as ChartWorkbenchFilter;
  }

  @Input()
  set filter(value: ChartWorkbenchFilter) {
    this._filter = value;
  }

  get entityNameI18nKey(): string {
    return ChartWorkbenchUtils.getEntityNameI18nKey(this.filter.entityType);
  }

  get operationI18nKey(): string {
    return ChartWorkbenchUtils.getOperationI18nKey(ChartOperation[this.filter.operation]);
  }

  constructor(protected cdRef: ChangeDetectorRef, protected translateService: TranslateService, protected datePipe: DatePipe) {
    super(cdRef, translateService);
  }

  ngOnInit(): void {
  }

  getWorkspace() {
    return ChartWorkbenchUtils.getWorkspace(this.filter.entityType);
  }

  isCuf() {
    return this.filter.cufId;
  }

  removeFilter($event: MouseEvent) {
    $event.stopPropagation();
    this.inactivateFilter();
  }


  protected buildDateValue(filterValue: DateFilterValue, operation: FilterOperation): string {
    if (filterValue.value.length === 0) {
      return '';
    }
    return this.datePipe.transform(filterValue.value, 'shortDate', '', this.translateService.getBrowserLang());
  }

  protected buildMultiDateValue(filterValue: MultipleStringFilterValue, operation: FilterOperation): string {
    if (filterValue.value.length === 0) {
      return '';
    }
    const values = filterValue.value.map(value => this.datePipe.transform(value, 'shortDate', '', this.translateService.getBrowserLang()));
    return values.join(', ');
  }
}

export type ChartWorkbenchFilter = Filter & { entityType: EntityType, preventOpening: boolean };
