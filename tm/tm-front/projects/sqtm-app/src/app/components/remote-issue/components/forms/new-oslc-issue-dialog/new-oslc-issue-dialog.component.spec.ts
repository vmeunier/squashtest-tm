import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewOslcIssueDialogComponent } from './new-oslc-issue-dialog.component';
import {DialogService, OslcIssue} from 'sqtm-core';
import {RemoteIssueDialogState} from '../../../states/remote-issue-dialog.state';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {EMPTY} from 'rxjs';
import {RemoteIssueService} from '../../../services/remote-issue.service';
import SpyObj = jasmine.SpyObj;
import createSpyObj = jasmine.createSpyObj;
import {mockDialogService} from '../../../../../utils/testing-utils/mocks.service';
import {ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';

describe('NewOslcIssueDialogComponent', () => {
  let component: NewOslcIssueDialogComponent;
  let fixture: ComponentFixture<NewOslcIssueDialogComponent>;
  let remoteIssueService: SpyObj<RemoteIssueService>;

  beforeEach(async () => {
    remoteIssueService = createSpyObj(['loadInitialData']);
    (remoteIssueService as any).state$ = EMPTY;

    await TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, TranslateModule.forRoot()],
      declarations: [ NewOslcIssueDialogComponent ],
      providers: [
        {provide: RemoteIssueService, useValue: remoteIssueService},
        {provide: DialogService, useValue: mockDialogService()},
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewOslcIssueDialogComponent);
    component = fixture.componentInstance;

    component.remoteIssueDialogState = mockDialogState();

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

function makeOslcIssue(): OslcIssue {
  return {
    assignee: undefined,
    btName: '',
    bugtracker: '',
    category: undefined,
    comment: '',
    createDialog: '',
    currentScheme: '',
    description: '',
    fieldValues: {},
    hasBlankId: false,
    id: '',
    priority: undefined,
    project: undefined,
    selectDialog: '',
    state: undefined,
    summary: '',
    url: '',
    version: undefined,
    getNewKey(): string {
      return '';
    }
  };
}

function mockDialogState(): RemoteIssueDialogState {
  return {
    bugTrackerId: 1,
    attachMode: false,
    remoteIssue: makeOslcIssue(),
    remoteProjectNames: ['P12', 'P13'],
    remoteProjectName: 'P12',
    boundEntity: {
      boundEntityId: 12,
      bindableEntity: 'EXECUTION_TYPE',
    }
  };
}
