import {RemoteIssueService} from '../../services/remote-issue.service';
import {
  DatePickerComponent,
  RemoteIssue,
  RichTextFieldComponent,
  SelectFieldComponent,
  TextAreaFieldComponent,
  TextFieldComponent
} from 'sqtm-core';
import {RemoteIssueDialogState} from '../../states/remote-issue-dialog.state';
import {ChangeDetectorRef, Directive, QueryList, ViewChildren} from '@angular/core';
import {finalize, switchMap} from 'rxjs/operators';
import {Observable, of, throwError} from 'rxjs';
import {FormBuilder, FormGroup} from '@angular/forms';
import {RemoteAutocompleteTextFieldComponent} from '../widgets/remote-autocomplete-text-field/remote-autocomplete-text-field.component';
import {RemoteAttachmentFieldComponent} from '../widgets/remote-attachment-field/remote-attachment-field.component';

@Directive()
// tslint:disable-next-line:directive-class-suffix
export abstract class AbstractRemoteIssueForm {

  @ViewChildren(TextFieldComponent)
  textFields: QueryList<TextFieldComponent>;

  @ViewChildren(RemoteAutocompleteTextFieldComponent)
  remoteAutocompleteTextFields: QueryList<RemoteAutocompleteTextFieldComponent>;

  @ViewChildren(SelectFieldComponent)
  selectFields: QueryList<SelectFieldComponent>;

  @ViewChildren(DatePickerComponent)
  dateFields: QueryList<DatePickerComponent>;

  @ViewChildren(RichTextFieldComponent)
  richTextFields: QueryList<RichTextFieldComponent>;

  @ViewChildren(TextAreaFieldComponent)
  textAreaFields: QueryList<TextAreaFieldComponent>;

  @ViewChildren(RemoteAttachmentFieldComponent)
  attachmentFields: QueryList<RemoteAttachmentFieldComponent>;

  abstract remoteIssueDialogState: Readonly<RemoteIssueDialogState>;

  abstract formGroup: FormGroup;

  asyncMode = false;

  protected constructor(public readonly remoteIssueService: RemoteIssueService,
                        public readonly cdRef: ChangeDetectorRef,
                        public readonly fb: FormBuilder) {
  }

  clearForm(): void {
    this.formGroup.reset();
  }

  submitForm(): Observable<any> {
    if (this.checkFormValidity()) {
      this.beginAsync();

      return this.getNewRemoteIssue().pipe(
        switchMap(remoteIssue => this.remoteIssueService.submitIssue(remoteIssue)),
        switchMap((submitResponse) => this.submitAttachments(submitResponse.issueId)),
        finalize(() => this.endAsync()));
    } else {
      // This error is only propagated so that the dialog doesn't get closed.
      return throwError('Invalid form.');
    }
  }

  protected checkFormValidity(): boolean {
    if (this.formGroup.valid) {
      return true;
    } else {
      this.showClientSideErrors();
      return false;
    }
  }

  protected showClientSideErrors() {
    [
      this.textFields,
      this.selectFields,
      this.dateFields,
      this.richTextFields,
      this.textAreaFields,
      this.remoteAutocompleteTextFields,
    ].forEach(fields => fields
      .filter(field => this.formGroup.contains(field.fieldName))
      .forEach(field => field.showClientSideError()));
  }

  protected abstract getNewRemoteIssue(): Observable<RemoteIssue>;

  protected beginAsync(): void {
    this.asyncMode = true;
    this.cdRef.detectChanges();
  }

  protected endAsync(): void {
    this.asyncMode = false;
    this.cdRef.detectChanges();
  }

  protected resetErrorMessage(): void {
    this.remoteIssueService.clearError();
  }

  private submitAttachments(issueId: string): Observable<any> {
    if (this.attachmentFields.length > 0) {
      const files = this.attachmentFields.reduce((acc, curr) => [...acc, ...curr.addedFiles], []);
      return this.remoteIssueService.submitAttachments(files, issueId);
    }

    return of(null);
  }
}

