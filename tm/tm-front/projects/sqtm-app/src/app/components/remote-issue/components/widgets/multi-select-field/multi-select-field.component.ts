import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input} from '@angular/core';
import {AbstractFormField, DisplayOption, FieldValidationError} from 'sqtm-core';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'sqtm-app-multi-select-field',
  templateUrl: './multi-select-field.component.html',
  styleUrls: ['./multi-select-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MultiSelectFieldComponent extends AbstractFormField {

  @Input() options: DisplayOption[] = [];
  @Input() fieldName: string;
  @Input() formGroup: FormGroup;
  @Input() serverSideFieldValidationError: FieldValidationError[];

  constructor(cdRef: ChangeDetectorRef) {
    super(cdRef);
  }

}
