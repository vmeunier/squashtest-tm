import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {NewRemoteIssueFormComponent} from './new-remote-issue-form.component';
import {ReactiveFormsModule} from '@angular/forms';
import {RemoteIssueService} from '../../../services/remote-issue.service';
import {TranslateModule} from '@ngx-translate/core';
import SpyObj = jasmine.SpyObj;
import createSpyObj = jasmine.createSpyObj;
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {BTIssue} from 'sqtm-core';
import {RemoteIssueDialogState} from '../../../states/remote-issue-dialog.state';
import {EMPTY} from 'rxjs';

describe('NewRemoteIssueFormComponent', () => {
  let component: NewRemoteIssueFormComponent;
  let fixture: ComponentFixture<NewRemoteIssueFormComponent>;
  let remoteIssueService: SpyObj<RemoteIssueService>;

  beforeEach(waitForAsync(() => {
    remoteIssueService = createSpyObj(['loadInitialData']);
    (remoteIssueService as any).state$ = EMPTY;

    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, TranslateModule.forRoot()],
      declarations: [NewRemoteIssueFormComponent],
      providers: [
        {provide: RemoteIssueService, useValue: remoteIssueService}
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewRemoteIssueFormComponent);
    component = fixture.componentInstance;
    component.remoteIssueDialogState = mockDialogState();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});


function makeBTIssue(): BTIssue {
  return {
    assignee: undefined,
    bugtracker: '',
    category: undefined,
    comment: '',
    createdOn: undefined,
    description: '',
    hasBlankId: false,
    id: '',
    priority: undefined,
    project: {
      categories: [],
      versions: [],
      users: [],
      priorities: [],
      defaultIssuePriority: {
        id: 'low', name: 'low',
      },
      id: 1,
      name: 'prj',
    },
    reporter: undefined,
    state: undefined,
    status: undefined,
    summary: '',
    version: undefined,
    getNewKey(): string {
      return '';
    }
  };
}

function mockDialogState(): RemoteIssueDialogState {
  return {
    bugTrackerId: 1,
    attachMode: false,
    remoteIssue: makeBTIssue(),
    remoteProjectNames: ['P12', 'P13'],
    remoteProjectName: 'P12',
    boundEntity: {
      boundEntityId: 12,
      bindableEntity: 'EXECUTION_TYPE',
    }
  };
}
