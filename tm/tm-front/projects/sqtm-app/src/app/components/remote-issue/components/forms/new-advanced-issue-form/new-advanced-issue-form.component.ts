import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, NgZone} from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {
  ActionErrorDisplayService,
  AdvancedIssue,
  CascadingSelectOption,
  DialogReference,
  DisplayOption,
  Field,
  isAdvancedIssue,
  ListItem,
  ValueRenderingInputType
} from 'sqtm-core';
import {RemoteIssueDialogState} from '../../../states/remote-issue-dialog.state';
import {RemoteIssueService} from '../../../services/remote-issue.service';
import {AbstractRemoteIssueForm} from '../abstract-remote-issue-form';
import {Observable, Subscription} from 'rxjs';
import {remoteIssueLogger} from '../../../remote-issue.logger';
import {distinctUntilChanged, filter, map, switchMap, take} from 'rxjs/operators';
import {
  findFieldValueAmongPossibleValues,
  getPossibleValuesForField,
  isCompositeField,
  isDropdownListField,
  isFileUpload,
  isIssueFieldTypeSupported,
  SUPPORTED_INPUT_TYPES,
  SUPPORTED_UNKNOWN_INPUT_DATA_TYPES,
  toFieldValue
} from './new-advanced-issue-form.model';
import {TranslateService} from '@ngx-translate/core';
import * as _ from 'lodash';


const logger = remoteIssueLogger.compose('NewAdvancedIssueFormComponent');

/**
 * Remote issue reporting form for advanced issues (e.g. Jira issues).
 * The emphasis is made on display logic whereas the data handling is mostly delegated to pure functions
 * defined in new-advance-issue-form.model.ts.
 * Remote project selection and error handling is done in parent class.
 */
@Component({
  selector: 'sqtm-app-new-advanced-issue-form',
  templateUrl: './new-advanced-issue-form.component.html',
  styleUrls: [
    './new-advanced-issue-form.component.less',
    '../../../styles/remote-issue-dialog-commons.less',
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NewAdvancedIssueFormComponent extends AbstractRemoteIssueForm {

  formGroup: FormGroup;

  readonly currentScheme$: Observable<Field[]>;

  private _schemeSelectorSubscriptions: Subscription[] = [];

  @Input()
  set remoteIssueDialogState(state: RemoteIssueDialogState) {
    this._remoteIssueDialogState = state;

    if (!isAdvancedIssue(this.remoteIssueDialogState.remoteIssue)) {
      throw new Error('Bad remote issue type. Expected AdvancedIssue. This component may not work as intended.');
    }

    this.buildFormGroup();
  }

  get remoteIssueDialogState(): RemoteIssueDialogState {
    return this._remoteIssueDialogState;
  }

  private _remoteIssueDialogState: RemoteIssueDialogState;

  get defaultRadioLabel(): string {
    const label = this.translateService.instant('sqtm-core.generic.label.none.masculine');
    return `(${label})`;
  }

  constructor(public readonly remoteIssueService: RemoteIssueService,
              public readonly fb: FormBuilder,
              public readonly cdRef: ChangeDetectorRef,
              public readonly actionErrorDisplayService: ActionErrorDisplayService,
              public readonly dialogReference: DialogReference,
              public readonly ngZone: NgZone,
              public readonly translateService: TranslateService) {
    super(remoteIssueService, cdRef, fb);

    this.currentScheme$ = remoteIssueService.state$.pipe(
      filter(state => state.remoteIssue != null),
      map(state => {
        const advancedIssue = state.remoteIssue as AdvancedIssue;
        return advancedIssue.project.schemes[advancedIssue.currentScheme];
      }),
      filter(scheme => Array.isArray(scheme)));
  }

  // This is NOT the transient state ! This is just the form data obtained on initial fetch.
  // It is still useful for things that won't change : remote project infos, schemes...
  get advancedIssue(): Readonly<AdvancedIssue> {
    // Type check is done once for all in remoteIssueDialogState setter
    return this._remoteIssueDialogState.remoteIssue as AdvancedIssue;
  }

  get isEditable(): boolean {
    return !this._remoteIssueDialogState.attachMode;
  }

  getFormControl(field: Field): FormControl {
    return this.formGroup.controls[field.id] as FormControl;
  }

  isFieldVisible(field: Field): boolean {
    const isSupported = this.isFieldTypeSupported(field.rendering.inputType);

    if (this.remoteIssueDialogState.attachMode) {
      return Object.keys(this.advancedIssue.fieldValues).includes(field.id);
    } else {
      return isSupported;
    }
  }

  isFieldTypeSupported(inputType: ValueRenderingInputType): boolean {
    const isSupported = isIssueFieldTypeSupported(inputType.name, inputType.dataType);

    if (!isSupported) {
      logger.debug('Unsupported field type received : ' + inputType);
    }

    return isSupported;
  }

  getLabelForField(field: Field): string {
    return field.rendering.required ? `${field.label}*` : field.label;
  }

  // Temporary display for debugging, do NOT use it for internal logic
  getValueForField(field: Field): any {
    return this.advancedIssue.fieldValues[field.id]?.scalar ?? '-';
  }

  getOptionsForField(field: Field): DisplayOption[] {
    return getPossibleValuesForField(field)?.map(value => ({id: value.id, label: value.name})) ?? [];
  }

  // We need a different method for checkbox_list type because we're expecting a scalar with a
  // comma-separated list of labels...
  getCheckboxOptionsForField(field: Field): DisplayOption[] {
    return getPossibleValuesForField(field)?.map(value => ({id: value.name, label: value.name})) ?? [];
  }

  // Here again, server is expecting labels (not ids)
  getCascadeOptionsForField(field: Field): CascadingSelectOption[] {
    // We know we won't have more than 2 levels so we don't need to make it recursive
    return getPossibleValuesForField(field)?.map(value => ({
      id: value.name,
      label: value.name,
      children: value.composite
        .map(child => ({id: child.name, label: child.name, children: []}))
    }));
  }

  getListItemsForField(field: Field): ListItem[] {
    return getPossibleValuesForField(field)?.map(value => ({
      id: value.id,
      label: value.name,
    })) ?? [];
  }

  protected getNewRemoteIssue(): Observable<AdvancedIssue> {
    return this.currentScheme$.pipe(
      take(1),
      map(scheme => this.makeIssueFromFormValues(scheme)));
  }

  private buildFormGroup(): void {
    this.currentScheme$.pipe(
      take(1),
    ).subscribe((currentScheme) => {
      this.resetSchemeSelectorObservers();
      this.formGroup = this.doBuildFormGroup(currentScheme);
      this.cdRef.detectChanges();
      this.observeSchemeSelectorsValueChanges();
    });
  }

  private doBuildFormGroup(currentScheme: Field[]): FormGroup {
    const controls = {};

    currentScheme.forEach((field) => {
      let initialValue = this.determineCurrentFieldValue(field);
      const validator = field.rendering.required ? Validators.required : null;

      if (this.remoteIssueDialogState.attachMode) {
        initialValue = {value: initialValue, disabled: true};
      }

      controls[field.id] = this.fb.control(initialValue, validator);
    });

    return this.fb.group(controls);
  }

  private determineCurrentFieldValue(field: Field): any {
    const fieldId = field.id;
    const currentFormValue = this.formGroup?.controls[fieldId]?.value;
    const scalarFieldValue = this.advancedIssue.fieldValues[fieldId]?.scalar;
    let initialValue = currentFormValue ?? scalarFieldValue;

    if (isDropdownListField(field) && initialValue == null) {
      // Select first available option
      initialValue = this.getOptionsForField(field)[0]?.id;
    }

    if (isCompositeField(field) && initialValue != null) {
      initialValue = [initialValue];
    }

    return initialValue;
  }

  private makeIssueFromFormValues(scheme: Field[]): AdvancedIssue {
    const issue = {...this.advancedIssue, fieldValues: {}};
    const strippedIssue: Partial<AdvancedIssue> = stripUndesiredIssueProperties(issue);

    scheme.forEach((schemeField) => {
      const isNotSupportedInputTypeName = !SUPPORTED_INPUT_TYPES.includes(schemeField.rendering.inputType.name as any);
      const isNotSupportedUnknownDataType = schemeField.rendering.inputType.name === 'unknown' &&
        !SUPPORTED_UNKNOWN_INPUT_DATA_TYPES.includes(schemeField.rendering.inputType.dataType);

      if (isNotSupportedInputTypeName || isNotSupportedUnknownDataType) {
        logger.trace(`The type of field ${schemeField.id} is not supported`);
      } else if (isFileUpload(schemeField)) {
        strippedIssue.fieldValues = _.omit(strippedIssue.fieldValues, [schemeField.id]);
      } else {
        const formValue = this.formGroup.controls[schemeField.id]?.value;
        strippedIssue.fieldValues[schemeField.id] = toFieldValue(schemeField, formValue);
      }
    });
    return strippedIssue as AdvancedIssue; // Yeah... WTF ? See stripUndesiredIssueProperties()
  }

  private observeSchemeSelectorsValueChanges(): void {
    this.currentScheme$
      .pipe(take(1))
      .subscribe((fields) => this.observeSchemeSelectors(fields));
  }

  private resetSchemeSelectorObservers(): void {
    this._schemeSelectorSubscriptions.forEach((sub) => sub.unsubscribe());
    this._schemeSelectorSubscriptions = [];
  }

  private observeSchemeSelectors(fields: Field[]): void {
    this._schemeSelectorSubscriptions = fields
      .filter((field) => isFieldSchemeSelector(field))
      .map(field => ({field, formControl: this.formGroup.controls[field.id]}))
      .filter(({formControl}) => formControl != null)
      .map(({field, formControl}) => this.subscribeToValueChange(field, formControl));
  }

  private subscribeToValueChange(field: Field, formControl: AbstractControl): Subscription {
    return formControl.valueChanges.pipe(
      distinctUntilChanged(),
      map(() => findFieldValueAmongPossibleValues(field, formControl.value)),
      filter(value => value != null),
      switchMap(value => this.remoteIssueService.changeScheme(field, value.scalar))
    ).subscribe(() => this.buildFormGroup());
  }
}

function isFieldSchemeSelector(field: Field): boolean {
  return field?.rendering.inputType.fieldSchemeSelector;
}

/**
 * Dirty hack inherited from legacy.
 * For some reason, when fetching the RemoteIssue, we receive an object with properties that
 * we need to remove before sending the object back to the server. If we don't remove them, the
 * bugtracker runs into troubles. This issue was detected with Redmine (SQUASH-3544).
 * I think the client shouldn't do such dirty things to please the server. Why is the server sending
 * these properties in the first place ?
 * Anyway, things would be easier and cleaner if the server wasn't using the same object for the
 * form configuration and the actual form values.
 *
 * Original comment :
 *  //this remove the properties that Jackson shouldn't bother with - thus preventing crashes
 *  //everything it needs is in the fieldValues
 */
function stripUndesiredIssueProperties(issue: AdvancedIssue): Partial<AdvancedIssue> {
  const propertiesToOmit = [
    'priority',
    'comment',
    'version',
    'status',
    'description',
    'category',
    'summary',
    'assignee',
  ];

  return _.omit(issue, propertiesToOmit);
}
