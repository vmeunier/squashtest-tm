import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {RemoteIssueDialogComponent, RemoteIssueDialogData} from './remote-issue-dialog.component';
import {ActionErrorDisplayService, DialogReference, RestService} from 'sqtm-core';
import {RemoteIssueService} from '../../services/remote-issue.service';
import {of} from 'rxjs';
import {mockRestService} from '../../../../utils/testing-utils/mocks.service';
import {TranslateModule} from '@ngx-translate/core';
import {AppTestingUtilsModule} from '../../../../utils/testing-utils/app-testing-utils.module';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import SpyObj = jasmine.SpyObj;

describe('RemoteIssueDialogComponent', () => {
  let component: RemoteIssueDialogComponent;
  let fixture: ComponentFixture<RemoteIssueDialogComponent>;
  const data: RemoteIssueDialogData = {
    attachMode: false,
    bugTrackerId: 1,
    squashProjectId: 1,
    bindableEntity: 'EXECUTION_TYPE',
    boundEntityId: 1,
  };
  const dialogReference = {data};
  const remoteIssueService = jasmine.createSpyObj(['loadInitialData']);
  let restService: SpyObj<RestService>;

  beforeEach(waitForAsync(() => {
    restService = mockRestService();
    restService.get.and.returnValue(of({projectNames: []}));

    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot(), AppTestingUtilsModule],
      declarations: [RemoteIssueDialogComponent],
      providers: [
        {provide: RestService, useValue: restService}, // transient dep for non overridable RemoteIssueService
        {provide: DialogReference, useValue: dialogReference},
        {provide: RemoteIssueService, useValue: remoteIssueService},
        {provide: ActionErrorDisplayService, useValue: jasmine.createSpyObj(['showActionError'])}
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoteIssueDialogComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
