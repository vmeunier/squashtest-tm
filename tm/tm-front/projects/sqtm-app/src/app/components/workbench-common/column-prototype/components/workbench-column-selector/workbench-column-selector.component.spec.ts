import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkbenchColumnSelectorComponent } from './workbench-column-selector.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {GridService} from 'sqtm-core';
import {mockGridService} from '../../../../../utils/testing-utils/mocks.service';

describe('WorkbenchColumnSelectorComponent', () => {
  let component: WorkbenchColumnSelectorComponent;
  let fixture: ComponentFixture<WorkbenchColumnSelectorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkbenchColumnSelectorComponent ],
      providers: [
        { provide: GridService, useValue: mockGridService() },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkbenchColumnSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
