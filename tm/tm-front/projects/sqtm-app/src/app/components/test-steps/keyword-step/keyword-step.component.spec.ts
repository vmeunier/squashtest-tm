import { OverlayModule } from '@angular/cdk/overlay';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateService } from '@ngx-translate/core';
import { DialogService, DraggableListDirective, SqtmDragAndDropModule } from 'sqtm-core';
import { TestCaseViewService } from '../../../pages/test-case-workspace/test-case-view/service/test-case-view.service';
import { KeywordStepView } from '../../../pages/test-case-workspace/test-case-view/state/test-step.state';
import { AppTestingUtilsModule } from '../../../utils/testing-utils/app-testing-utils.module';

import { KeywordStepComponent } from './keyword-step.component';

describe('KeywordStepComponent', () => {
  let component: KeywordStepComponent;
  let fixture: ComponentFixture<KeywordStepComponent>;
  const testCaseViewService = jasmine.createSpyObj('TestCaseViewService', ['load']);
  const dialogService = jasmine.createSpyObj('DialogService', ['load']);
  const translateService = jasmine.createSpyObj('TranslateService', ['load']);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ RouterTestingModule, AppTestingUtilsModule, SqtmDragAndDropModule, OverlayModule ],
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [ KeywordStepComponent ],
      providers: [
        {
        provide: TestCaseViewService,
        useValue: testCaseViewService
        },
        {
          provide: DialogService,
          useValue: dialogService
        },
        {
          provide: TranslateService,
          useValue: translateService
        },
        {
          provide: DraggableListDirective,
          useValue: {}
        }]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KeywordStepComponent);
    component = fixture.componentInstance;
    component.keywordStep = {
      selected: false,
      draggingStep: false,
      keyword: 'AND',
      action: 'I eat one apple.',
      styledAction: `I eat <span style='color: blue;'>one</span> apple.'`,
      datatable: '| product | price |\n| Espresso | 0.40 |',
      docstring: 'Hello\nWorld\n!',
      comment: 'This is a\ncomment.'
    } as KeywordStepView;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
