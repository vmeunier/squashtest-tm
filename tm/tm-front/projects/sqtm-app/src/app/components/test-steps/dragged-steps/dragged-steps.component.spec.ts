import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {DraggedStepsComponent} from './dragged-steps.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {DRAG_AND_DROP_DATA} from 'sqtm-core';
import {AppTestingUtilsModule} from '../../../utils/testing-utils/app-testing-utils.module';

describe('DraggedStepsComponent', () => {
  let component: DraggedStepsComponent;
  let fixture: ComponentFixture<DraggedStepsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule],
      declarations: [DraggedStepsComponent],
      providers: [
        {provide: DRAG_AND_DROP_DATA, useValue: {data: {}}}
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DraggedStepsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
