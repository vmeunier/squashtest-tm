import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {CallStepComponent} from './call-step.component';
import {RestService, WorkspaceCommonModule} from 'sqtm-core';
import {RouterTestingModule} from '@angular/router/testing';
import {TestCaseViewService} from '../../../pages/test-case-workspace/test-case-view/service/test-case-view.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {EMPTY} from 'rxjs';
import {AppTestingUtilsModule} from '../../../utils/testing-utils/app-testing-utils.module';

describe('CallStepComponent', () => {
  let component: CallStepComponent;
  let fixture: ComponentFixture<CallStepComponent>;

  let testCaseViewService = jasmine.createSpyObj('testCaseViewService', ['load']);
  testCaseViewService = {...testCaseViewService, componentData$: EMPTY};
  const restService = {};
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, RouterTestingModule, WorkspaceCommonModule, TranslateModule.forRoot()],
      providers: [
        {provide: TestCaseViewService, useValue: testCaseViewService},
        {provide: RestService, useValue: restService}
      ],
      declarations: [CallStepComponent],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CallStepComponent);
    component = fixture.componentInstance;
    component.callTestStep = {
      id: 1,
      testCaseId: 1,
      stepOrder: 0,
      calledTcId: 125,
      kind: 'call-step',
      projectId: 1,
      calledTcName: 'test-case-125',
      calledTestCaseSteps: [],
      selected: false,
      activeSelection: false,
      draggingStep: false,
      showPlaceHolder: false,
      extended: false,
      calledDatasetId: 1,
      delegateParam: false,
      calledDatasetName: 'JDD'
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
