import { OverlayModule } from '@angular/cdk/overlay';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { ActionErrorDisplayService } from 'sqtm-core';
import { TestCaseViewService } from '../../../pages/test-case-workspace/test-case-view/service/test-case-view.service';
import { AppTestingUtilsModule } from '../../../utils/testing-utils/app-testing-utils.module';

import { KeywordStepListComponent } from './keyword-step-list.component';

describe('KeywordStepListComponent', () => {
  let component: KeywordStepListComponent;
  let fixture: ComponentFixture<KeywordStepListComponent>;
  const testCaseViewService = jasmine.createSpyObj('testCaseViewService', ['load']);
  const actionErrorDisplayService = jasmine.createSpyObj('actionErrorDisplayService', ['handleActionError']);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KeywordStepListComponent ],
      imports: [ AppTestingUtilsModule, ReactiveFormsModule, OverlayModule ],
      providers: [
        {
          provide: TestCaseViewService,
          useValue: testCaseViewService
        },
        {
          provide: ActionErrorDisplayService,
          useValue: actionErrorDisplayService
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KeywordStepListComponent);
    component = fixture.componentInstance;
    component.steps = [];
    component.keywords = [];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
