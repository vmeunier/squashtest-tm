import {TestStepState} from '../../pages/test-case-workspace/test-case-view/state/test-step.state';

export const TEST_STEP_VIEW_ORIGIN = 'TEST_STEP_VIEW_ORIGIN';

export class TestStepDndData {
  constructor(public readonly draggedSteps: TestStepState[]) {
  }
}
