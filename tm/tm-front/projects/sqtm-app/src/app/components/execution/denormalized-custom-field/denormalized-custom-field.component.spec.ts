import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {DenormalizedCustomFieldComponent} from './denormalized-custom-field.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {InputType} from 'sqtm-core';

describe('DenormalizedCustomFieldComponent', () => {
  let component: DenormalizedCustomFieldComponent;
  let fixture: ComponentFixture<DenormalizedCustomFieldComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [DenormalizedCustomFieldComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DenormalizedCustomFieldComponent);
    component = fixture.componentInstance;
    component.denormalizedFieldValue = {
      id: 1,
      value: 'hello',
      fieldType: 'CF',
      denormalizedFieldHolderId: 1,
      inputType: InputType.PLAIN_TEXT,
      label: 'cuf-1'
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
