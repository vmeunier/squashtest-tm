import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {TestCaseStatus, TestCaseStatusKeys, TestCaseStatusLevelEnumItem} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-denormalized-status',
  templateUrl: './denormalized-status.component.html',
  styleUrls: ['./denormalized-status.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DenormalizedStatusComponent {

  _status: TestCaseStatusLevelEnumItem<TestCaseStatusKeys>;

  constructor() {
  }

  @Input()
  set status(key: TestCaseStatusKeys) {
    this._status = TestCaseStatus[key];
  }

  get statusKey() {
    return this._status.i18nKey;
  }

  get statusColor(): string {
    return this._status.color;
  }

  get icon(): string {
    return this._status.icon;
  }
}
