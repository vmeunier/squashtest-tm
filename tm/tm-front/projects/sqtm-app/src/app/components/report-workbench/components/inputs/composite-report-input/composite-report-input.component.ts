import {ChangeDetectionStrategy, Component, Input, OnInit, ViewChild} from '@angular/core';
import {ReportInputType, ReportOption, ReportOptionGroup} from 'sqtm-core';
import {FormGroup, ValidationErrors} from '@angular/forms';
import {FormProjectPickerComponent} from '../form-project-picker/form-project-picker.component';

@Component({
  selector: 'sqtm-app-composite-report-input',
  templateUrl: './composite-report-input.component.html',
  styleUrls: ['./composite-report-input.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CompositeReportInputComponent implements OnInit {

  @Input()
  reportInput: ReportOptionGroup;

  @Input()
  subFormGroup: FormGroup;

  @Input()
  errors: ValidationErrors;

  @ViewChild(FormProjectPickerComponent)
  projectPicker: FormProjectPickerComponent;

  ReportInputType = ReportInputType;

  constructor() {
  }

  ngOnInit(): void {
  }

  handleOpen() {

  }

  getDebug() {
    return Object.keys(this.subFormGroup.controls);
  }

  getErrors() {
    return Object.keys(this.subFormGroup.errors || {});
  }

  showControl($event: any) {
    console.log($event);
  }

  optionIsDisabled(option: ReportOption) {
    if (option.disabledBy != null) {
      const disabledBy = option.disabledBy;
      const control = this.subFormGroup.controls[disabledBy];
      const value = control.value as boolean;
      return !value;
    }
    return false;
  }
}
