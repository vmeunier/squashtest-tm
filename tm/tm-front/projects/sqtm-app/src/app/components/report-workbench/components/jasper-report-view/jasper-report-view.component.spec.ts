import {ComponentFixture, TestBed} from '@angular/core/testing';

import {JasperReportViewComponent} from './jasper-report-view.component';
import {JasperReportService} from '../../services/jasper-report.service';
import {EMPTY} from 'rxjs';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('JasperReportViewComponent', () => {
  let component: JasperReportViewComponent;
  let fixture: ComponentFixture<JasperReportViewComponent>;
  const jasperMock = jasmine.createSpyObj<JasperReportService>(['downloadReport', 'getHtmlPreview']);


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [JasperReportViewComponent],
      providers: [{
        provide: JasperReportService,
        useValue: jasperMock
      }],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  });

  beforeEach(() => {
    jasperMock.getHtmlPreview.and.returnValue(EMPTY);
    fixture = TestBed.createComponent(JasperReportViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
