import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormProjectPickerComponent } from './form-project-picker.component';
import {OverlayModule} from '@angular/cdk/overlay';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';

describe('FormProjectPickerComponent', () => {
  let component: FormProjectPickerComponent;
  let fixture: ComponentFixture<FormProjectPickerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [OverlayModule, HttpClientTestingModule, AppTestingUtilsModule],
      declarations: [ FormProjectPickerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormProjectPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
