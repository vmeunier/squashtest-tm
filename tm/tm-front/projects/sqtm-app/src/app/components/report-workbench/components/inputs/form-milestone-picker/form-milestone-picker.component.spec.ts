import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormMilestonePickerComponent } from './form-milestone-picker.component';
import {OverlayModule} from '@angular/cdk/overlay';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('FormMilestonePickerComponent', () => {
  let component: FormMilestonePickerComponent;
  let fixture: ComponentFixture<FormMilestonePickerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        OverlayModule,
        AppTestingUtilsModule,
        HttpClientTestingModule
      ],
      declarations: [
        FormMilestonePickerComponent
      ],
      schemas:[NO_ERRORS_SCHEMA]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormMilestonePickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
