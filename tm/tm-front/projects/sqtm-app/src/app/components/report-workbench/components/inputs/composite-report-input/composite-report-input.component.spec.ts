import {ComponentFixture, TestBed} from '@angular/core/testing';

import {CompositeReportInputComponent} from './composite-report-input.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {ReportInputType} from 'sqtm-core';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {TranslateModule} from '@ngx-translate/core';

describe('CompositeReportInputComponent', () => {
  let component: CompositeReportInputComponent;
  let fixture: ComponentFixture<CompositeReportInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, TranslateModule.forRoot()],
      declarations: [CompositeReportInputComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompositeReportInputComponent);
    component = fixture.componentInstance;
    component.reportInput = {
      name: 'report',
      options: [],
      type: ReportInputType.RADIO_BUTTONS_GROUP,
      required: false,
      label: 'report'
    };
    component.errors = [];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
