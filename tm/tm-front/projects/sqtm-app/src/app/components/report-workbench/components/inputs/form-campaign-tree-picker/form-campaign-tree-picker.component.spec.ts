import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormCampaignTreePickerComponent } from './form-campaign-tree-picker.component';
import {OverlayModule} from '@angular/cdk/overlay';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {TranslateModule} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('FormCampaignTreePickerComponent', () => {
  let component: FormCampaignTreePickerComponent;
  let fixture: ComponentFixture<FormCampaignTreePickerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormCampaignTreePickerComponent ],
      imports: [OverlayModule, HttpClientTestingModule, AppTestingUtilsModule, TranslateModule.forRoot()],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormCampaignTreePickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
