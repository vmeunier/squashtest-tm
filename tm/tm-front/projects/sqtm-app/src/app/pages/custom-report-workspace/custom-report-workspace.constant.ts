import {InjectionToken} from '@angular/core';
import {GridDefinition, GridService} from 'sqtm-core';

export const CR_WS_TREE_CONFIG = new InjectionToken<GridDefinition>('Grid config instance for the main custom report workspace tree');
export const CR_WS_TREE = new InjectionToken<GridService>('Grid service instance for the main custom report workspace tree');

export const customReportWorkspaceTreeId = 'custom-report-workspace-main-tree';
