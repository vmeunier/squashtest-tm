import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardViewContentComponent } from './dashboard-view-content.component';
import {DashboardViewService} from '../../services/dashboard-view.service';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TranslateModule} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {NzDropDownModule} from 'ng-zorro-antd/dropdown';

describe('DashboardViewContentComponent', () => {
  let component: DashboardViewContentComponent;
  let fixture: ComponentFixture<DashboardViewContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, HttpClientTestingModule, TranslateModule.forRoot(), NzDropDownModule],
      declarations: [ DashboardViewContentComponent ],
      providers: [DashboardViewService],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardViewContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
