import {ComponentFixture, TestBed} from '@angular/core/testing';

import {CreateReportViewComponent} from './create-report-view.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../utils/testing-utils/app-testing-utils.module';
import {RouterTestingModule} from '@angular/router/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('CreateReportViewComponent', () => {
  let component: CreateReportViewComponent;
  let fixture: ComponentFixture<CreateReportViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule, RouterTestingModule],
      declarations: [CreateReportViewComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateReportViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
