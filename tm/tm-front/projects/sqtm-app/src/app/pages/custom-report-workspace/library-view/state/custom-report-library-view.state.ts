import {EntityViewState, provideInitialViewState} from 'sqtm-core';
import {CustomReportLibraryState} from './custom-report-library.state';

export interface CustomReportLibraryViewState extends EntityViewState<CustomReportLibraryState, 'customReportLibrary'> {
  customReportLibrary: CustomReportLibraryState;
}

export function provideInitialCustomReportLibraryView(): Readonly<CustomReportLibraryViewState> {
  return provideInitialViewState<CustomReportLibraryState, 'customReportLibrary'>('customReportLibrary');
}
