import {EntityViewState, provideInitialViewState} from 'sqtm-core';
import {CustomExportState} from './custom-export.state';

export interface CustomExportViewState extends EntityViewState<CustomExportState, 'customExport'> {
  customExport: CustomExportState;
}

export function provideInitialCustomExportView(): Readonly<CustomExportViewState> {
  return provideInitialViewState<CustomExportState, 'customExport'>('customExport');
}
