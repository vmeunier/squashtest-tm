import {ComponentFixture, TestBed} from '@angular/core/testing';

import {CreateCustomExportViewComponent} from './create-custom-export-view.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {ReferentialDataService, RestService} from 'sqtm-core';
import {mockReferentialDataService, mockRestService} from '../../../../../utils/testing-utils/mocks.service';
import {ActivatedRoute} from '@angular/router';
import {EMPTY} from 'rxjs';

describe('CreateCustomExportViewComponent', () => {
  let component: CreateCustomExportViewComponent;
  let fixture: ComponentFixture<CreateCustomExportViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CreateCustomExportViewComponent],
      providers: [
        {provide: ReferentialDataService, useValue: mockReferentialDataService()},
        {provide: ActivatedRoute, useValue: {params: EMPTY}},
        {provide: RestService, useValue: mockRestService()},
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateCustomExportViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
