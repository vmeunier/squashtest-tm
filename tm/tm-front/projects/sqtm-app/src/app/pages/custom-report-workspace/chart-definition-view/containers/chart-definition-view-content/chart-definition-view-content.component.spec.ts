import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ChartDefinitionViewContentComponent } from './chart-definition-view-content.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {TranslateModule} from '@ngx-translate/core';
import {DatePipe} from '@angular/common';
import {EMPTY} from 'rxjs';
import {ChartDefinitionViewService} from '../../services/chart-definition-view.service';
import {RouterTestingModule} from '@angular/router/testing';

describe('ChartDefinitionViewContentComponent', () => {
  let component: ChartDefinitionViewContentComponent;
  let fixture: ComponentFixture<ChartDefinitionViewContentComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule, TranslateModule.forRoot(), RouterTestingModule],
      declarations: [ ChartDefinitionViewContentComponent ],
      providers: [
        {
          provide: ChartDefinitionViewService,
          useValue: {
            componentData$: EMPTY
          }
        },
        DatePipe
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartDefinitionViewContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
