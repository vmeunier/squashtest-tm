import {ComponentFixture, TestBed} from '@angular/core/testing';

import {DashboardViewComponent} from './dashboard-view.component';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {DashboardViewService} from '../../services/dashboard-view.service';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {NzDropDownModule} from 'ng-zorro-antd/dropdown';

describe('DashboardViewComponent', () => {
  let component: DashboardViewComponent;
  let fixture: ComponentFixture<DashboardViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, HttpClientTestingModule, TranslateModule.forRoot(), RouterTestingModule, NzDropDownModule],
      declarations: [DashboardViewComponent],
      providers: [DashboardViewService],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
