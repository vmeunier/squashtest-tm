import {TestBed} from '@angular/core/testing';

import {ReportDefinitionViewService} from './report-definition-view.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../utils/testing-utils/app-testing-utils.module';
import {TranslateModule} from '@ngx-translate/core';

describe('ReportDefinitionViewService', () => {
  let service: ReportDefinitionViewService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule, TranslateModule.forRoot()],
      providers: [ReportDefinitionViewService]
    });
    service = TestBed.inject(ReportDefinitionViewService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
