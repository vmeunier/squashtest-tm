import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomExportInformationPanelComponent } from './custom-export-information-panel.component';
import {ReferentialDataService} from 'sqtm-core';
import {mockReferentialDataService} from '../../../../../utils/testing-utils/mocks.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';

describe('CustomExportInformationPanelComponent', () => {
  let component: CustomExportInformationPanelComponent;
  let fixture: ComponentFixture<CustomExportInformationPanelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [ CustomExportInformationPanelComponent ],
      providers: [
        {provide: ReferentialDataService, useValue: mockReferentialDataService()}
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomExportInformationPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
