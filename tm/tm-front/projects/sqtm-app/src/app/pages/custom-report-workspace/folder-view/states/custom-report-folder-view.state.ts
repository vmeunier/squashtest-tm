import {EntityViewState, provideInitialViewState} from 'sqtm-core';
import {CustomReportFolderState} from './custom-report-folder.state';

export interface CustomReportFolderViewState extends EntityViewState<CustomReportFolderState, 'customReportFolder'> {
  customReportFolder: CustomReportFolderState;
}

export function provideInitialCustomReportFolderView(): Readonly<CustomReportFolderViewState> {
  return provideInitialViewState<CustomReportFolderState, 'customReportFolder'>('customReportFolder');
}
