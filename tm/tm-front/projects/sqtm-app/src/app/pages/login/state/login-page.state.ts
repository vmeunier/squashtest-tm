import {LicenseInformationState} from 'sqtm-core';

export interface LoginPageState {
  loginMessage: string;
  squashVersion: string;
  isH2: boolean;
  showLoginError: boolean;
  licenseInformation: LicenseInformationState;
}

export function initialLoginPageState(): Readonly<LoginPageState> {
  return {
    squashVersion: null,
    loginMessage: null,
    isH2: false,
    showLoginError: false,
    licenseInformation: null,
  };
}
