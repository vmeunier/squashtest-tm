import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { DialogReference } from 'sqtm-core';
import { AppTestingUtilsModule } from '../../../../../../utils/testing-utils/app-testing-utils.module';

import { BddScriptExportDialogComponent } from './bdd-script-export-dialog.component';

describe('BddScriptExportDialogComponent', () => {
  let component: BddScriptExportDialogComponent;
  let fixture: ComponentFixture<BddScriptExportDialogComponent>;
  const translateService = jasmine.createSpyObj('TranslateService', ['instant']);
  const dialogReference = {
    data: {
      id: 'export-bdd-script',
      libraries: [],
      nodes: []
    }
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BddScriptExportDialogComponent ],
      imports: [ AppTestingUtilsModule, ReactiveFormsModule, HttpClientTestingModule ],
      providers: [{
        provide: DialogReference, useValue: dialogReference
      }, {
        provide: TranslateService, useValue: translateService
      }]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BddScriptExportDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
