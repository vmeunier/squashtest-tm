import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {ImportLog, ImportTestCaseComponentData} from '../../state/import-test-case.state';
import {ImportTestCaseService} from '../../services/import-test-case.service';

@Component({
  selector: 'sqtm-app-import-report',
  templateUrl: './import-report.component.html',
  styleUrls: ['./import-report.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImportReportComponent implements OnInit {

  @Input()
  componentData: ImportTestCaseComponentData;

  constructor(private importTestCaseService: ImportTestCaseService) {
  }

  ngOnInit(): void {
  }

  getFileName(templateOk: ImportLog) {
    return this.importTestCaseService.getDownLoadReportUrl(templateOk.reportUrl);
  }

}
