import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {TestCaseMilestoneViewComponent} from './test-case-milestone-view.component';
import {TestCaseMilestoneViewService} from '../../services/test-case-milestone-view.service';
import {TranslateModule} from '@ngx-translate/core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';

describe('TestCaseMilestoneViewComponent', () => {
  let component: TestCaseMilestoneViewComponent;
  let fixture: ComponentFixture<TestCaseMilestoneViewComponent>;

  const serviceMock = jasmine.createSpyObj<TestCaseMilestoneViewService>(['init']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot(), HttpClientTestingModule, AppTestingUtilsModule],
      declarations: [TestCaseMilestoneViewComponent],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestCaseMilestoneViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
