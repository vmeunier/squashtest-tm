import {CustomDashboardModel, SqtmEntityState, TestCaseStatistics} from 'sqtm-core';

// tslint:disable-next-line:no-empty-interface
export interface TestCaseLibraryState extends SqtmEntityState {
  name: string;
  statistics: TestCaseStatistics;
  dashboard: CustomDashboardModel;
  generatedDashboardOn: Date;
  shouldShowFavoriteDashboard: boolean;
  canShowFavoriteDashboard: boolean;
  favoriteDashboardId: number;
}
