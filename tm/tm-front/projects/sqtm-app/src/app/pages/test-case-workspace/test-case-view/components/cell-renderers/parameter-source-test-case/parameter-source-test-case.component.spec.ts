import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {ParameterSourceTestCaseComponent} from './parameter-source-test-case.component';
import {GridTestingModule} from 'sqtm-core';
import {TranslateModule} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';

describe('ParameterSourceTestCaseComponent', () => {
  let component: ParameterSourceTestCaseComponent;
  let fixture: ComponentFixture<ParameterSourceTestCaseComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, GridTestingModule, TranslateModule.forRoot()],
      declarations: [ParameterSourceTestCaseComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParameterSourceTestCaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
