import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {TestCaseViewContentComponent} from './test-case-view-content.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {WorkspaceCommonModule} from 'sqtm-core';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {EMPTY} from 'rxjs';
import {TestCaseViewService} from '../../service/test-case-view.service';
import {TCW_COVERAGE_TABLE} from '../../test-case-view.constant';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';

describe('TestCaseViewContentComponent', () => {
  let component: TestCaseViewContentComponent;
  let fixture: ComponentFixture<TestCaseViewContentComponent>;
  let testCaseViewService = jasmine.createSpyObj('testCaseViewService', ['load']);
  testCaseViewService = {...testCaseViewService, componentData$: EMPTY};
  const tableMock = jasmine.createSpyObj('tableMock', ['load']);
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [TestCaseViewContentComponent],
      imports: [AppTestingUtilsModule, TranslateModule.forRoot(), RouterTestingModule, WorkspaceCommonModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {provide: TCW_COVERAGE_TABLE, useValue: tableMock},
        {
          provide: TestCaseViewService,
          useValue: testCaseViewService
        }
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestCaseViewContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
