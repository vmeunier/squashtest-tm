import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { NewVersionDialogComponent } from './new-version-dialog.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {DialogModule, DialogReference} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';

describe('NewVersionDialogComponent', () => {
  let component: NewVersionDialogComponent;
  let fixture: ComponentFixture<NewVersionDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule, DialogModule, TranslateModule.forRoot()],
      providers: [
        {
          provide: DialogReference,
          useValue: jasmine.createSpyObj(['close'])
        },
        {
          provide: TranslateService,
          useValue: jasmine.createSpyObj('TranslateService', ['instant']),
        },
        TranslateService
      ],
      declarations: [ NewVersionDialogComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewVersionDialogComponent);
    component = fixture.componentInstance;
    component.data = {
      id: '-1',
      titleKey: 'key',
      projectId: -1,
      originalTestCaseId: -1,
      tcKind: 'STANDARD'
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
