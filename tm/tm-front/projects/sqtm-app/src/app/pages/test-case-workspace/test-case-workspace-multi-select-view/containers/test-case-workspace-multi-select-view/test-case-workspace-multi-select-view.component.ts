import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {TestCaseMultiSelectionService} from '../../services/test-case-multi-selection.service';
import {PartyPreferencesService, RestService} from 'sqtm-core';
import {Observable} from 'rxjs';
import {TestCaseMultiViewState} from '../../state/test-case-multi-view.state';

@Component({
  selector: 'sqtm-app-test-case-workspace-multi-select-view',
  templateUrl: './test-case-workspace-multi-select-view.component.html',
  styleUrls: ['./test-case-workspace-multi-select-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: TestCaseMultiSelectionService,
      useClass: TestCaseMultiSelectionService,
      deps: [RestService, PartyPreferencesService]
    }
  ]
})
export class TestCaseWorkspaceMultiSelectViewComponent implements OnInit, OnDestroy {

  public componentData$: Observable<TestCaseMultiViewState>;

  constructor(private viewService: TestCaseMultiSelectionService) {

  }

  ngOnInit(): void {
    this.componentData$ = this.viewService.componentData$;
  }

  ngOnDestroy(): void {
    this.viewService.complete();
  }

}
