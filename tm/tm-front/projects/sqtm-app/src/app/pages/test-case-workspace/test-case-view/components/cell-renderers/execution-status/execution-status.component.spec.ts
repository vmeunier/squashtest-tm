import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import {ExecutionStatusComponent} from './execution-status.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {GridTestingModule} from 'sqtm-core';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';

describe('ExecutionStatusComponent', () => {
  let component: ExecutionStatusComponent;
  let fixture: ComponentFixture<ExecutionStatusComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [GridTestingModule, AppTestingUtilsModule],
      declarations: [ExecutionStatusComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutionStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
