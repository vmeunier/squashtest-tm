import {ProjectData} from 'sqtm-core';

export class ImportTestCaseState {
  currentStep: ImportTestCaseSteps;
  format: TestCaseImportFileFormatKeys;
  file: File;
  encoding: FileEncodingKeys;
  projects: ProjectData[];
  selectedProject: number;
  simulationReport: XlsReport;
  zipReport: ImportSummary;
  xlsReport: XlsReport;
  loadingData: boolean;
  importFailed: boolean;
}

export class XlsReport {
  templateOk: ImportLog;
  importFormatFailure: ImportFormatFailure;
}

export interface ImportFormatFailure {
  missingMandatoryColumns: string[];
  duplicateColumns: string[];
  actionValidationError: any;
}

export class ImportLog {
  testCaseSuccesses: number;
  testCaseWarnings: number;
  testCaseFailures: number;
  testStepSuccesses: number;
  testStepWarnings: number;
  testStepFailures: number;
  parameterSuccesses: number;
  parameterWarnings: number;
  parameterFailures: number;
  datasetSuccesses: number;
  datasetWarnings: number;
  datasetFailures: number;
  coverageSuccesses: number;
  coverageWarnings: number;
  coverageFailures: number;
  reportUrl: string;
}

export class ImportSummary {
  total: number;
  success: number;
  renamed: number;
  modified: number;
  failures: number;
  rejected: number;
  milestoneFailures: number;
  milestoneNotActivatedFailures: number;
}

export function initialImportTestCaseState(): Readonly<ImportTestCaseState> {
  return {
    currentStep: 'CONFIGURATION',
    format: 'XLS',
    file: null,
    encoding: 'WINDOWS',
    projects: [],
    selectedProject: null,
    simulationReport: null,
    xlsReport: null,
    zipReport: null,
    loadingData: false,
    importFailed: false
  };
}


export class ImportTestCaseComponentData extends ImportTestCaseState {

}

export type FileFormat<K extends string> = { [id in K]: FileFormatItem<K> };

export interface FileFormatItem<K> {
  id: string;
  i18nKey: string;
  value: string;
}

export type ImportTestCaseSteps = 'CONFIGURATION' | 'SIMULATION_REPORT' | 'CONFIRMATION' | 'CONFIRMATION_REPORT';

export type TestCaseImportFileFormatKeys = 'XLS' | 'ZIP';

export const TestCaseFileFormat: FileFormat<TestCaseImportFileFormatKeys> = {
  XLS: {id: 'XLS', i18nKey: 'sqtm-core.generic.label.file.format.xls', value: 'xls'},
  ZIP: {id: 'ZIP', i18nKey: 'sqtm-core.generic.label.file.format.zip', value: 'zip'}
};

export type FileEncodingKeys = 'WINDOWS' | 'ZIP';

export const FileEncoding = {
  WINDOWS: {id: 'WINDOWS', value: 'Cp858', i18nKey: 'sqtm-core.test-case-workspace.dialog.field.encoding.windows'},
  UTF_8: {id: 'UTF8', value: 'UTF8', i18nKey: 'sqtm-core.test-case-workspace.dialog.field.encoding.utf-8'}
};

// tslint:disable-next-line:max-line-length
export const XLS_TYPE = 'application/vnd.ms-excel,application/vnd.ms-excel.sheet.macroEnabled.12, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
export const ZIP_TYPE = 'application/zip';
