import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {
  CustomFieldData,
  InfoList,
  Milestone,
  MilestoneStatus,
  ReferentialDataService,
  TestCaseKind,
  TestCaseKindKeys
} from 'sqtm-core';
import {TestCaseViewService} from '../../service/test-case-view.service';
import {TestCaseViewComponentData} from '../../containers/test-case-view/test-case-view.component';
import {TranslateService} from '@ngx-translate/core';
import {DatePipe} from '@angular/common';
import {Observable} from 'rxjs';
import {pluck} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-test-case-information-panel',
  templateUrl: './test-case-information-panel.component.html',
  styleUrls: ['./test-case-information-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TestCaseInformationPanelComponent {

  readonly SQTM_CORE_INFOLIST_ITEM_ICON_PREFIX = 'sqtm-core-infolist-item';

  isMilestoneFeatureEnabled$: Observable<boolean>;

  constructor(
    public testCaseViewService: TestCaseViewService,
    public translateService: TranslateService,
    private datePipe: DatePipe,
    private referentialDataService: ReferentialDataService
  ) {
    this.isMilestoneFeatureEnabled$ = referentialDataService.globalConfiguration$.pipe(
      pluck('milestoneFeatureEnabled'));
  }

  @Input()
  testCaseViewComponentData: TestCaseViewComponentData;

  @Input()
  customFieldData: CustomFieldData[];

  getIcon(infoList: InfoList, itemId: number) {
    const item = infoList.items.find(infoListItem => infoListItem.id === itemId);
    if (item.iconName !== '' && item.iconName !== 'noicon') {
      return `${this.SQTM_CORE_INFOLIST_ITEM_ICON_PREFIX}:${item.iconName}`;
    } else {
      return '';
    }

  }

  deleteMilestone(testCaseId: number, milestoneId: number) {
    this.testCaseViewService.unbindMilestone(testCaseId, milestoneId);
  }

  bindMilestones(testCaseId: number, milestoneIds: number[]) {
    this.testCaseViewService.bindMilestones(testCaseId, milestoneIds);
  }

  trackCfd(cfd: CustomFieldData) {
    return cfd.id;
  }

  getFormatTranslation(format: TestCaseKindKeys) {
    const key = TestCaseKind[format].i18nKey;
    return this.translateService.instant(key);
  }

  getPossibleMilestones(componentData: TestCaseViewComponentData): Milestone[] {
    const projectMilestoneViews = componentData.projectData.milestones;
    const testCaseMilestones = componentData.testCase.milestones;
    const tcMilestonesIds = testCaseMilestones.map(milestone => milestone.id);
    return projectMilestoneViews.filter(milestone => !tcMilestonesIds.find(id => id === milestone.id))
      .filter(milestone => milestone.status === MilestoneStatus.IN_PROGRESS.id || milestone.status === MilestoneStatus.FINISHED.id);
  }
}
