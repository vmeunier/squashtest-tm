import {TestBed} from '@angular/core/testing';

import {TestCaseLibraryViewService} from './test-case-library-view.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TranslateModule} from '@ngx-translate/core';
import {AppTestingUtilsModule} from '../../../../utils/testing-utils/app-testing-utils.module';

describe('TestCaseLibraryService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [AppTestingUtilsModule, HttpClientTestingModule, TranslateModule.forRoot()],
    providers: [TestCaseLibraryViewService]
  }));

  it('should be created', () => {
    const service: TestCaseLibraryViewService = TestBed.inject(TestCaseLibraryViewService);
    expect(service).toBeTruthy();
  });
});
