import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {ScriptComponent} from './script.component';
import {TestCaseViewService} from '../../service/test-case-view.service';
import {DialogService} from 'sqtm-core';
import {EMPTY} from 'rxjs';

describe('ScriptComponent', () => {
  let component: ScriptComponent;
  let fixture: ComponentFixture<ScriptComponent>;
  const dialogService = jasmine.createSpyObj('dialogService', ['create']);
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ScriptComponent],
      providers: [
        {
          provide: TestCaseViewService,
          useValue: {...jasmine.createSpyObj(['load']), componentData$: EMPTY}
        },
        {
          provide: DialogService,
          useValue: dialogService
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScriptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
