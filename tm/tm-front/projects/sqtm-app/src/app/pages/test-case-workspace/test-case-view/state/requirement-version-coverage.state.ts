import {createEntityAdapter, EntityState} from '@ngrx/entity';
import {RequirementVersionCoverage} from 'sqtm-core';

export interface CoverageState extends EntityState<RequirementVersionCoverage> {

}

export const coverageEntityAdapter = createEntityAdapter<RequirementVersionCoverage>({
  selectId: (rvc) => rvc.requirementVersionId
});

export const coverageEntitySelectors = coverageEntityAdapter.getSelectors();
