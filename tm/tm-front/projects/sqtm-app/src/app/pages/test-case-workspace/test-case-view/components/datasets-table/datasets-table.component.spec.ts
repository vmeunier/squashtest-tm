import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {DatasetsTableComponent, tcwDatasetsTableDefinition} from './datasets-table.component';
import {
  DialogService,
  gridServiceFactory,
  GridTestingModule,
  ReferentialDataService,
  RestService,
} from 'sqtm-core';
import {TranslateModule} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TCW_DATASET_TABLE, TCW_DATASET_TABLE_CONF} from '../../test-case-view.constant';
import { NzModalModule } from 'ng-zorro-antd/modal';
import {TestCaseViewService} from '../../service/test-case-view.service';
import {EMPTY} from 'rxjs';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';

describe('DatasetsTableComponent', () => {
  let component: DatasetsTableComponent;
  let fixture: ComponentFixture<DatasetsTableComponent>;
  const testCaseViewService = jasmine.createSpyObj('testCaseViewService', ['load']);
  testCaseViewService.componentData$ = EMPTY;
  const dialogService = jasmine.createSpyObj('dialogService', ['create']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, GridTestingModule, TranslateModule.forRoot(), NzModalModule],
      declarations: [DatasetsTableComponent],
      providers: [
        {
          provide: TCW_DATASET_TABLE_CONF,
          useFactory: tcwDatasetsTableDefinition
        },
        {
          provide: TCW_DATASET_TABLE,
          useFactory: gridServiceFactory,
          deps: [RestService, TCW_DATASET_TABLE_CONF, ReferentialDataService]
        },
        {
          provide: TestCaseViewService,
          useValue: testCaseViewService
        },
        {
          provide: DialogService,
          useValue: dialogService
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatasetsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
