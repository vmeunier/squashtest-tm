import {CustomDashboardModel, EntityScope, Milestone, TestCaseStatistics} from 'sqtm-core';

export interface TestCaseMilestoneViewState {
  milestone: Milestone;
  statistics: TestCaseStatistics;
  scope: EntityScope[];
  dashboard: CustomDashboardModel;
  generatedDashboardOn: Date;
  shouldShowFavoriteDashboard: boolean;
  canShowFavoriteDashboard: boolean;
  favoriteDashboardId: number;
}

export function initialTestCaseMilestoneViewState(): Readonly<TestCaseMilestoneViewState> {
  return {
    milestone: null,
    statistics: null,
    scope: [],
    dashboard: null,
    generatedDashboardOn: null,
    shouldShowFavoriteDashboard: false,
    canShowFavoriteDashboard: false,
    favoriteDashboardId: null
  };
}
