import {ChangeDetectionStrategy, Component, OnDestroy} from '@angular/core';
import {
  coverageRequirementColumn,
  coverageVerifiedByColumn,
  DataRow,
  deleteColumn,
  Extendable,
  Fixed,
  GenericDataRow,
  GridDefinition,
  GridService,
  indexColumn,
  levelEnumColumn,
  milestoneLabelColumn,
  ReferentialDataService,
  RequirementCriticality,
  RequirementStatus,
  smallGrid,
  Sort,
  StyleDefinitionBuilder,
  textColumn
} from 'sqtm-core';
import {TCW_COVERAGE_TABLE} from '../../test-case-view.constant';
import {DeleteCoverageComponent} from '../cell-renderers/delete-coverage/delete-coverage.component';
import {pluck, takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';

export function tcwCoverageTableDefinition(): GridDefinition {
  return smallGrid('test-case-view-coverages')
    .withColumns([
      indexColumn()
        .withViewport('leftViewport'),
      textColumn('projectName')
        .changeWidthCalculationStrategy(new Extendable(100, 1))
        .withI18nKey('sqtm-core.entity.project.label.singular'),
      textColumn('reference')
        .withI18nKey('sqtm-core.entity.generic.reference.label'),
      coverageRequirementColumn('name')
        .changeWidthCalculationStrategy(new Extendable(100, 1.5))
        .withI18nKey('sqtm-core.entity.requirement.label.singular'),
      milestoneLabelColumn('milestoneLabels'),
      levelEnumColumn('criticality', RequirementCriticality)
        .withTitleI18nKey('sqtm-core.entity.generic.criticality.label')
        .withI18nKey('sqtm-core.entity.generic.criticality.short')
        .isEditable(false)
        .changeWidthCalculationStrategy(new Fixed(40)),
      levelEnumColumn('status', RequirementStatus)
        .withTitleI18nKey('sqtm-core.entity.generic.status.label')
        .withI18nKey('sqtm-core.entity.generic.status.short')
        .isEditable(false)
        .changeWidthCalculationStrategy(new Fixed(40)),
      coverageVerifiedByColumn('verifiedBy')
        .changeWidthCalculationStrategy(new Fixed(150))
        .withI18nKey('sqtm-core.entity.generic.verified-by.label'),
      deleteColumn(DeleteCoverageComponent)
    ])
    .withStyle(new StyleDefinitionBuilder().showLines())
    .withRowHeight(35)
    .withRowConverter(coverageLiteralConverter)
    .withInitialSortedColumns([{id: 'criticality', sort: Sort.DESC}, {
      id: 'projectName',
      sort: Sort.DESC
    }, {id: 'reference', sort: Sort.DESC}, {id: 'name', sort: Sort.DESC}])
    .build();
}

@Component({
  selector: 'sqtm-app-coverage-table',
  template: `
    <sqtm-core-grid></sqtm-core-grid>
  `,
  styleUrls: ['./coverage-table.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: GridService,
      useExisting: TCW_COVERAGE_TABLE
    }
  ]
})
export class CoverageTableComponent implements OnDestroy {
  unsub$ = new Subject<void>();

  constructor(public grid: GridService, public referentialDataService: ReferentialDataService) {
    this.referentialDataService.globalConfiguration$.pipe(
      takeUntil(this.unsub$),
      pluck('milestoneFeatureEnabled')
    ).subscribe((milestoneFeatureEnabled) => {
      this.grid.setColumnVisibility('milestoneLabels', milestoneFeatureEnabled);
    });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}

export function coverageLiteralConverter(literals: Partial<DataRow>[]): DataRow[] {
  return literals.reduce((datarows, literal) => {
    const dataRow = new GenericDataRow();
    Object.assign(dataRow, literal);
    const directlyVerified = dataRow.data['directlyVerified'];
    const unDirectlyVerified = dataRow.data['unDirectlyVerified'];
    dataRow.disabled = !directlyVerified && unDirectlyVerified;
    datarows.push(dataRow);
    return datarows;
  }, []);
}
