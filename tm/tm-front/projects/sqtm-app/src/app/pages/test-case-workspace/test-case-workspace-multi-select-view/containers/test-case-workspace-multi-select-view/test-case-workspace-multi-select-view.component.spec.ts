import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {TestCaseWorkspaceMultiSelectViewComponent} from './test-case-workspace-multi-select-view.component';
import {GridService} from 'sqtm-core';
import {of} from 'rxjs';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';

describe('TestCaseWorkspaceMultiSelectViewComponent', () => {
  let component: TestCaseWorkspaceMultiSelectViewComponent;
  let fixture: ComponentFixture<TestCaseWorkspaceMultiSelectViewComponent>;
  const gridServiceMock = jasmine.createSpyObj(['load']);
  gridServiceMock.selectedRows$ = of([]);


  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, HttpClientTestingModule, RouterTestingModule],
      declarations: [TestCaseWorkspaceMultiSelectViewComponent],
      providers: [
        {
          provide: GridService,
          useValue: gridServiceMock
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestCaseWorkspaceMultiSelectViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
