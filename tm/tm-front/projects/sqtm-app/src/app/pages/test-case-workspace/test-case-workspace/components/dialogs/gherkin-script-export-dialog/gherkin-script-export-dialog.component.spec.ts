import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { DialogReference } from 'sqtm-core';
import { AppTestingUtilsModule } from '../../../../../../utils/testing-utils/app-testing-utils.module';

import { GherkinScriptExportDialogComponent } from './gherkin-script-export-dialog.component';

describe('GherkinScriptExportDialogComponent', () => {
  let component: GherkinScriptExportDialogComponent;
  let fixture: ComponentFixture<GherkinScriptExportDialogComponent>;
  const translateService = jasmine.createSpyObj('TranslateService', ['instant']);
  const dialogReference = {
    data: {
      id: 'export-gherkin-script',
      libraries: [],
      nodes: []
    }
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GherkinScriptExportDialogComponent ],
      imports: [ AppTestingUtilsModule, ReactiveFormsModule, HttpClientTestingModule ],
      providers: [{
        provide: DialogReference, useValue: dialogReference
      }, {
        provide: TranslateService, useValue: translateService
      }]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GherkinScriptExportDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
