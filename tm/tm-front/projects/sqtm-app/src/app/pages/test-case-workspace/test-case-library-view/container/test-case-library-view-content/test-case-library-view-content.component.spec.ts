import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {TestCaseLibraryViewContentComponent} from './test-case-library-view-content.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {EMPTY} from 'rxjs';
import {TestCaseLibraryViewService} from '../../service/test-case-library-view.service';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import SpyObj = jasmine.SpyObj;

describe('TestCaseFolderViewContentComponent', () => {
  let component: TestCaseLibraryViewContentComponent;
  let fixture: ComponentFixture<TestCaseLibraryViewContentComponent>;

  const testCaseLibraryViewService: SpyObj<TestCaseLibraryViewService> = jasmine.createSpyObj('testCaseLibraryViewService', ['load']);
  testCaseLibraryViewService.componentData$ = EMPTY;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule],
      declarations: [TestCaseLibraryViewContentComponent],
      providers: [
        {
          provide: TestCaseLibraryViewService,
          useValue: testCaseLibraryViewService
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestCaseLibraryViewContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
