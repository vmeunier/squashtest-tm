import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild, } from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  EditableTextFieldComponent,
  GridService,
  RestService,
  TableValueChange
} from 'sqtm-core';
import {FormControl, Validators} from '@angular/forms';
import {TestCaseViewService} from '../../../service/test-case-view.service';

@Component({
  selector: 'sqtm-app-dataset-dataset-param-value',
  template: `

    <ng-container *ngIf="columnDisplay && row">
      <div class="full-width full-height flex-column p-l-5">
        <sqtm-core-editable-text-field style="margin: auto 0 auto 0" [layout]="'no-buttons'"
                                       [size]="'small'"
                                       [formControl]="formControl"
                                       [value]="getValue()"
                                       [editable]="editable"
                                       [showPlaceHolder]="false"
                                       [displayInGrid]="true"
                                       (confirmEvent)="updateAndClose($event)"
                                       #editableTextField ngDefaultControl></sqtm-core-editable-text-field>
      </div>
    </ng-container>
  `,
  styleUrls: ['./dataset-dataset-param-value.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatasetDatasetParamValueComponent extends AbstractCellRendererComponent implements OnInit {

  formControl: FormControl;

  @ViewChild('editableTextField')
  editableTextField: EditableTextFieldComponent;

  get editable() {
    return this.gridDisplay.allowModifications;
  }

  constructor(public grid: GridService,
              public cdRef: ChangeDetectorRef,
              private restService: RestService,
              private testCaseViewService: TestCaseViewService
  ) {
    super(grid, cdRef);
    this.formControl = new FormControl('', [Validators.maxLength(255)]);
  }

  ngOnInit() {
  }

  updateAndClose(currentValue: string) {

    const datasetParamValuePatch = {
      id: this.row.data[this.columnDisplay.id].id,
      value: currentValue
    };

    this.restService.post(['dataset-param-values'], datasetParamValuePatch)
      .subscribe(() => {
        this.testCaseViewService.updateParamValueData(datasetParamValuePatch);
        const changedDatasetParamValue = {...this.row.data[this.columnDisplay.id], value: currentValue};
        const changedValue: TableValueChange = {columnId: this.columnDisplay.id, value: changedDatasetParamValue};
        this.grid.editRows([this.row.id], [changedValue]);
      });
  }

  getValue() {
    return this.row.data[this.columnDisplay.id].value;
  }

}

export function datasetDatasetParamValueColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(DatasetDatasetParamValueComponent);
}
