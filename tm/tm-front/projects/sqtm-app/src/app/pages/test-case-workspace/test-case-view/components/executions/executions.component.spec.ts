import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {ExecutionsComponent, tcwExecutionsTableDefinition} from './executions.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TestCaseViewService} from '../../service/test-case-view.service';
import {EMPTY} from 'rxjs';
import {TCW_EXECUTION_TABLE, TCW_EXECUTION_TABLE_CONF} from '../../test-case-view.constant';
import {
  gridServiceFactory,
  GridTestingModule,
  ReferentialDataService,
  RestService,
} from 'sqtm-core';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';

describe('ExecutionsComponent', () => {
  let component: ExecutionsComponent;
  let fixture: ComponentFixture<ExecutionsComponent>;
  let testCaseViewService = jasmine.createSpyObj('testCaseViewService', ['load']);
  testCaseViewService = {...testCaseViewService, componentData$: EMPTY};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ExecutionsComponent],
      imports: [GridTestingModule, AppTestingUtilsModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: TCW_EXECUTION_TABLE_CONF,
          useFactory: tcwExecutionsTableDefinition
        },
        {
          provide: TCW_EXECUTION_TABLE,
          useFactory: gridServiceFactory,
          deps: [RestService, TCW_EXECUTION_TABLE_CONF, ReferentialDataService]
        },
        {provide: TestCaseViewService, useValue: testCaseViewService}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
