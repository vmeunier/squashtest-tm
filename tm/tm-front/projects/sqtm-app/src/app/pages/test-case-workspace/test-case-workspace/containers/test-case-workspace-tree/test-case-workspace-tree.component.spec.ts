import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {TestCaseWorkspaceTreeComponent} from './test-case-workspace-tree.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TC_WS_TREE} from '../../../test-case-workspace.constant';
import {EMPTY, of} from 'rxjs';
import {GridService, DialogService} from 'sqtm-core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import {RouterTestingModule} from '@angular/router/testing';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';

describe('TestCaseWorkspaceTreeComponent', () => {
  let component: TestCaseWorkspaceTreeComponent;
  let fixture: ComponentFixture<TestCaseWorkspaceTreeComponent>;
  const tableMock = jasmine.createSpyObj('tableMock', ['load', 'setColumnSorts', 'addFilters', 'inactivateFilter', 'activateFilter']);
  tableMock.selectedRows$ = EMPTY;
  tableMock.openedRowIds$ = EMPTY;
  tableMock.sortedColumns$ = EMPTY;
  tableMock.loaded$ = of(false);
  tableMock.canCreate$ = of(true);

  const dialogService = jasmine.createSpyObj('dialogService', ['create']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, HttpClientTestingModule, NzDropDownModule, RouterTestingModule],
      declarations: [TestCaseWorkspaceTreeComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [{
        provide: DialogService,
        useValue: dialogService
      }]
    })
      .compileComponents().then(() => {
      TestBed.overrideProvider(TC_WS_TREE, {useValue: tableMock});
      TestBed.overrideProvider(GridService, {useValue: tableMock});
      fixture = TestBed.createComponent(TestCaseWorkspaceTreeComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
