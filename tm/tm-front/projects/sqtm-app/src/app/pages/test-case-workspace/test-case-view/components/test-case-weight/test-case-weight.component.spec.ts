import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TestCaseWeightComponent } from './test-case-weight.component';
import {EMPTY} from 'rxjs';
import {TranslateModule} from '@ngx-translate/core';
import {TestCaseViewService} from '../../service/test-case-view.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';

describe('TestCaseWeightComponent', () => {
  let component: TestCaseWeightComponent;
  let fixture: ComponentFixture<TestCaseWeightComponent>;
  let testCaseViewService = jasmine.createSpyObj('testCaseViewService', ['load']);
  testCaseViewService = {...testCaseViewService, componentData$: EMPTY};
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, TranslateModule.forRoot()],
      declarations: [ TestCaseWeightComponent ],
      providers: [
        {
          provide: TestCaseViewService,
          useValue: testCaseViewService
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestCaseWeightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
