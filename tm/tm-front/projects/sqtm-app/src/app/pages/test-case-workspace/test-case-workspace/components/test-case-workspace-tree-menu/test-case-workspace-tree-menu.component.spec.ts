import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {TestCaseWorkspaceTreeMenuComponent} from './test-case-workspace-tree-menu.component';
import {GridTestingModule} from 'sqtm-core';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('TestCaseWorkspaceTreeMenuComponent', () => {
  let component: TestCaseWorkspaceTreeMenuComponent;
  let fixture: ComponentFixture<TestCaseWorkspaceTreeMenuComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [GridTestingModule, AppTestingUtilsModule],
      declarations: [TestCaseWorkspaceTreeMenuComponent],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestCaseWorkspaceTreeMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
