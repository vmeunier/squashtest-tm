export class TestCaseExportDialogConfiguration {
  id: string;
  nodes: number[];
  libraries: number[];
}
