import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {CalledTestCaseComponent, tcwCalledTCTableDefiniton} from './called-test-case.component';
import {gridServiceFactory, GridTestingModule, ReferentialDataService, RestService} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TCW_CALLED_TC_TABLE, TCW_CALLED_TC_TABLE_CONF} from '../../test-case-view.constant';

describe('CalledTestCaseComponent', () => {
  let component: CalledTestCaseComponent;
  let fixture: ComponentFixture<CalledTestCaseComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [GridTestingModule],
      declarations: [CalledTestCaseComponent],
      providers: [
        {
          provide: TCW_CALLED_TC_TABLE_CONF,
          useFactory: tcwCalledTCTableDefiniton
        },
        {
          provide: TCW_CALLED_TC_TABLE,
          useFactory: gridServiceFactory,
          deps: [RestService, TCW_CALLED_TC_TABLE_CONF, ReferentialDataService]
        }],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalledTestCaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
