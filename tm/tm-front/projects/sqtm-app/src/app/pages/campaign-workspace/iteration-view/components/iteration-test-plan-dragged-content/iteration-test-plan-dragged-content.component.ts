import {ChangeDetectionStrategy, Component, Inject} from '@angular/core';
import {DataRow, DefaultGridDraggedContentComponent, DRAG_AND_DROP_DATA, DragAndDropData, GridService} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';

const DELETED_TC_KEY = 'sqtm-core.campaign-workspace.test-plan.label.deleted-test-case';

@Component({
  selector: 'sqtm-app-iteration-test-plan-dragged-content',
  templateUrl: './iteration-test-plan-dragged-content.component.html',
  styleUrls: ['./iteration-test-plan-dragged-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IterationTestPlanDraggedContentComponent extends DefaultGridDraggedContentComponent {
  constructor(@Inject(DRAG_AND_DROP_DATA) public dragAnDropData: DragAndDropData,
              public readonly grid: GridService,
              public readonly translateService: TranslateService) {
    super(dragAnDropData, grid);
  }

  getText(dataRow: Readonly<DataRow>): string {
    return dataRow.data['testCaseName'] ?? this.translateService.instant(DELETED_TC_KEY);
  }
}
