import {
  AbstractListCellRendererComponent,
  ActionErrorDisplayService,
  AuthenticatedUser,
  ExecutionStatus,
  ExecutionStatusKeys,
  formatFullUserName,
  GridService,
  ListPanelItem,
  ProjectData,
  ReferentialDataService,
  RestService
} from 'sqtm-core';
import {ChangeDetectorRef, Directive, ElementRef, TemplateRef, ViewChild, ViewContainerRef} from '@angular/core';
import {Overlay, OverlayRef} from '@angular/cdk/overlay';
import {TranslateService} from '@ngx-translate/core';
import {Observable} from 'rxjs';
import {filter, take, takeUntil} from 'rxjs/operators';

@Directive()
// tslint:disable-next-line:directive-class-suffix
export abstract class AbstractExecutionStatusCell extends AbstractListCellRendererComponent {
  @ViewChild('templatePortalContent', {read: TemplateRef})
  templatePortalContent: TemplateRef<any>;

  @ViewChild('executionStatus', {read: ElementRef})
  executionStatus: ElementRef;

  overlayRef: OverlayRef;

  readonly authenticatedUser$: Observable<AuthenticatedUser>;

  abstract canEdit$: Observable<boolean>;

  abstract panelItems$: Observable<ListPanelItem[]>;

  protected constructor(public grid: GridService,
                        public cdRef: ChangeDetectorRef,
                        public readonly overlay: Overlay,
                        public readonly vcr: ViewContainerRef,
                        public readonly translateService: TranslateService,
                        public readonly restService: RestService,
                        public readonly actionErrorDisplayService: ActionErrorDisplayService,
                        public referentialDataService: ReferentialDataService) {
    super(grid, cdRef, overlay, vcr, translateService, restService, actionErrorDisplayService);

    this.authenticatedUser$ = this.referentialDataService.authenticatedUser$
      .pipe(takeUntil(this.unsub$));
  }

  get statusKey(): ExecutionStatusKeys {
    return this.row.data[this.columnDisplay.id];
  }

  get color(): string {
    return ExecutionStatus[this.statusKey].color;
  }

  get i18nExecutionStatusKey(): string {
    return ExecutionStatus[this.statusKey].i18nKey;
  }

  protected getFilteredExecutionStatusKeys(projectData: ProjectData): string[] {
    const filteredExecutionStatusKeys = ['READY', 'RUNNING', 'SUCCESS', 'FAILURE', 'BLOCKED'];

    if (this.isExecutionStatusEnabled('SETTLED', projectData)) {
      filteredExecutionStatusKeys.push('SETTLED');
    }

    if (this.isExecutionStatusEnabled('UNTESTABLE', projectData)) {
      filteredExecutionStatusKeys.push('UNTESTABLE');
    }

    return filteredExecutionStatusKeys;
  }

  public showExecutionStatusList() {
    this.canEdit$.pipe(
      take(1),
      filter(canEdit => canEdit)
    ).subscribe(() => {
      this.showList(
        this.executionStatus,
        this.templatePortalContent,
        [
          {originX: 'start', overlayX: 'start', originY: 'bottom', overlayY: 'top', offsetX: -10, offsetY: 6},
          {originX: 'start', overlayX: 'start', originY: 'top', overlayY: 'bottom', offsetX: -10, offsetY: -6},
        ]
      );
    });
  }

  protected isExecutionStatusEnabled(statusKey: string, projectData: ProjectData): boolean {
    return ! projectData.disabledExecutionStatus.includes(statusKey as any);
  }

  protected getUser(user: AuthenticatedUser): string {
    return formatFullUserName(user);
  }

  protected asListItemOptions(statuses: string[]): ListPanelItem[] {
    return statuses.map(key => (
      {
        id: ExecutionStatus[key].id,
        label: this.translateService.instant(ExecutionStatus[key].i18nKey),
        icon: ExecutionStatus[key].icon,
        color: ExecutionStatus[key].color
      }));
  }
}
