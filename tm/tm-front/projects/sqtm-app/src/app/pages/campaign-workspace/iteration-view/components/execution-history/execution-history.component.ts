import {AfterViewInit, ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {
  centredTextColumn,
  convertSqtmLiterals,
  DataRow,
  dateTimeColumn,
  DialogConfiguration,
  DialogReference,
  DialogService,
  executionModeColumn,
  executionStatusColumn,
  Extendable,
  Fixed,
  grid,
  GridDefinition,
  GridService,
  gridServiceFactory,
  indexColumn,
  numericColumn,
  ProjectDataMap,
  ReferentialDataService,
  RestService,
  sortDate,
  SquashTmDataRowType,
  StyleDefinitionBuilder,
  testCaseImportanceColumn,
  textColumn,
  withLinkColumn
} from 'sqtm-core';
import {ITV_ITPE_HISTORY_TABLE, ITV_ITPE_HISTORY_TABLE_CONF} from '../../iteration-view.constant';
import {SuccessRateComponent} from '../cell-renderers/success-rate/success-rate.component';
import {deleteExecutionHistoryColumn} from '../cell-renderers/delete-execution-history/delete-execution-history.component';
import {Observable, Subject} from 'rxjs';
import {DatePipe} from '@angular/common';
import {NavigationStart, Router} from '@angular/router';
import {concatMap, filter, finalize, map, take, takeUntil, tap} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-execution-history',
  templateUrl: './execution-history.component.html',
  styleUrls: ['./execution-history.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    DatePipe,
    {
      provide: ITV_ITPE_HISTORY_TABLE_CONF,
      useFactory: itvItpeHistoryTableDefinition
    },
    {
      provide: ITV_ITPE_HISTORY_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, ITV_ITPE_HISTORY_TABLE_CONF, ReferentialDataService]
    },
    {
      provide: GridService,
      useExisting: ITV_ITPE_HISTORY_TABLE
    }
  ]
})
export class ExecutionHistoryComponent implements OnInit, AfterViewInit, OnDestroy {

  unsub$ = new Subject<void>();

  canDelete$: Observable<boolean>;
  private iterationId: number;
  private testPlanItemId: number;

  conf: DialogConfiguration;

  constructor(private dialogRef: DialogReference<DialogConfiguration>,
              private gridService: GridService,
              private dialogService: DialogService,
              private restService: RestService,
              private router: Router) {
    this.conf = this.dialogRef.data;
  }

  ngOnInit(): void {
    this.iterationId = this.conf['iterationId'];
    this.testPlanItemId = this.conf['testPlanItemId'];
    this.canDelete$ = this.gridService.selectedRows$.pipe(
      takeUntil(this.unsub$),
      map(rows => this.canDelete(rows)));
    this.router.events
      .pipe(
        takeUntil(this.unsub$),
        filter(event => event instanceof NavigationStart)
      )
      .subscribe((event: NavigationStart) => {
        if (event.url.includes('/execution/')) {
          this.dialogRef.close();
        }
      });
  }

  private canDelete(rows: DataRow[]) {
    return rows.length > 0
      && !rows.some(row => row.data.boundToBlockingMilestone)
      && !rows.some(row => !row.simplePermissions.canDelete);
  }

  ngAfterViewInit(): void {
    this.fetchExecutionHistory();
  }

  private fetchExecutionHistory() {
    this.gridService.setServerUrl([`iteration/${this.iterationId}/test-plan/${this.testPlanItemId}/executions`]);
  }

  showMassDeleteExecutionDialog() {
    this.gridService.selectedRows$.pipe(
      take(1),
      filter((rows: DataRow[]) => this.canDelete(rows)),
      concatMap((rows: DataRow[]) => this.openDeleteExecutionDialog(rows)),
      filter(({confirmDelete}) => confirmDelete),
      tap(() => this.gridService.beginAsyncOperation()),
      concatMap(({rows}) => this.removeExecutionsServerSide(rows)),
      finalize(() => this.gridService.completeAsyncOperation())
    ).subscribe(() => this.gridService.refreshData());
  }

  private openDeleteExecutionDialog(rows): Observable<{ confirmDelete: boolean, rows: string[] }> {
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.campaign-workspace.dialog.title.mass-remove-execution',
      messageKey: 'sqtm-core.campaign-workspace.dialog.message.mass-remove-execution',
      level: 'DANGER'
    });
    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map(confirmDelete => ({confirmDelete, rows}))
    );
  }

  private removeExecutionsServerSide(rows): Observable<void> {
    const rowIds = rows.map(row => row.data['executionId']).join(',');
    return this.restService.delete(['iteration', this.iterationId.toString(), 'test-plan/execution', rowIds]);
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}

function executionConverter(literals: Partial<DataRow>[], projectDataMap: ProjectDataMap): DataRow[] {
  const executions = literals.map(li => ({...li, type: SquashTmDataRowType.Execution}));
  return convertSqtmLiterals(executions, projectDataMap);
}

export function itvItpeHistoryTableDefinition(): GridDefinition {
  return grid('iteration-test-plan-execution-history')
    .withColumns([
      indexColumn()
        .withViewport('leftViewport'),
      withLinkColumn('executionOrder', {
        kind: 'link',
        baseUrl: '/execution',
        columnParamId: 'executionId',
        saveGridStateBeforeNavigate: true
      })
        .withI18nKey('sqtm-core.entity.execution-plan.execution-number.short')
        .withTitleI18nKey('sqtm-core.entity.execution-plan.execution-number.long')
        .changeWidthCalculationStrategy(new Fixed(60)),
      executionModeColumn('executionMode')
        .withI18nKey('sqtm-core.entity.execution.mode.label')
        .withTitleI18nKey('sqtm-core.entity.execution.mode.label')
        .changeWidthCalculationStrategy(new Fixed(60)),
      textColumn('executionName')
        .withI18nKey('sqtm-core.entity.name')
        .changeWidthCalculationStrategy(new Extendable(150, 0.2)),
      testCaseImportanceColumn('importance')
        .withI18nKey('sqtm-core.entity.test-case.importance.label-short-dot')
        .withTitleI18nKey('sqtm-core.entity.test-case.importance.label')
        .isEditable(false)
        .changeWidthCalculationStrategy(new Fixed(80)),
      textColumn('datasetName')
        .withI18nKey('sqtm-core.entity.dataset.label.short')
        .withTitleI18nKey('sqtm-core.entity.dataset.label.singular')
        .changeWidthCalculationStrategy(new Fixed(100)),
      centredTextColumn('successRate')
        .withRenderer(SuccessRateComponent)
        .withI18nKey('sqtm-core.entity.execution-plan.success-rate.label.short')
        .withTitleI18nKey('sqtm-core.entity.execution-plan.success-rate.label.long')
        .changeWidthCalculationStrategy(new Fixed(60)),
      executionStatusColumn('executionStatus')
        .withI18nKey('sqtm-core.entity.execution.status.label')
        .withTitleI18nKey('sqtm-core.entity.execution.status.long-label')
        .changeWidthCalculationStrategy(new Fixed(60)),
      numericColumn('issueCount')
        .withI18nKey('sqtm-core.entity.execution-plan.ano-number.short')
        .withTitleI18nKey('sqtm-core.entity.execution-plan.ano-number.long')
        .changeWidthCalculationStrategy(new Fixed(60)),
      textColumn('user')
        .withI18nKey('sqtm-core.generic.label.user')
        .changeWidthCalculationStrategy(new Extendable(100, 0.2)),
      dateTimeColumn('lastExecutedOn')
        .withSortFunction(sortDate)
        .withI18nKey('sqtm-core.entity.execution-plan.last-execution.label.short-dot')
        .withTitleI18nKey('sqtm-core.entity.execution-plan.last-execution.label.long')
        .changeWidthCalculationStrategy(new Extendable(130, 0.2)),
      deleteExecutionHistoryColumn('delete').withLabel('').disableSort().changeWidthCalculationStrategy(new Fixed(30))
    ]).server()
    .withRowConverter(executionConverter)
    .disableRightToolBar()
    .withStyle(new StyleDefinitionBuilder()
      .enableInitialLoadAnimation()
      .showLines())
    .withRowHeight(35)
    .build();
}
