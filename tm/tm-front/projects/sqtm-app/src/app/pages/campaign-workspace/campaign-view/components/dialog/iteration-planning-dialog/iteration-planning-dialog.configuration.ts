import {DialogConfiguration, IterationPlanning} from 'sqtm-core';
import {IterationPlanningDialogComponent} from './iteration-planning-dialog.component';

export interface IterationPlanningDialogConfiguration {
  iterationPlannings: IterationPlanning[];
}

export const baseIterationPlanningDialogConfiguration: DialogConfiguration = {
  id: 'iteration-planning-dialog',
  width: 700,
  component: IterationPlanningDialogComponent,
  viewContainerReference: null,
  data: null
};
