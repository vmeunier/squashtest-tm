import {InjectionToken} from '@angular/core';
import {GridDefinition, GridService} from 'sqtm-core';

export const CAMPAIGN_WS_TREE_CONFIG = new InjectionToken<GridDefinition>('Grid config instance for the main workspace tree');
export const CAMPAIGN_WS_TREE = new InjectionToken<GridService>('Grid service instance for the main workspace tree');


export const CAMPAIGN_ISSUE_TABLE_CONF = new InjectionToken<GridDefinition>('Grid config for the issue table of campaign view');
export const CAMPAIGN_ISSUE_TABLE = new InjectionToken<GridService>('Grid service instance for the issue table of campaign view');
