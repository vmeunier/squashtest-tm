import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {
  AbstractDeleteCellRenderer,
  ColumnDefinitionBuilder,
  ConfirmDeleteLevel,
  DataRow,
  DialogService,
  GridService,
  RestService
} from 'sqtm-core';
import {finalize} from 'rxjs/operators';
import {CampaignViewService} from '../../../service/campaign-view.service';

@Component({
  selector: 'sqtm-app-delete-campaign-test-plan-item',
  template: `
    <ng-container *ngIf="campaignViewService.componentData$ | async as componentData">
      <ng-container *ngIf="row && canDelete(row) && componentData.milestonesAllowModification">
        <sqtm-core-delete-icon
            [iconName]="getIcon()"
            (delete)="showDeleteConfirm()"></sqtm-core-delete-icon>
      </ng-container>
    </ng-container>
  `,
  styleUrls: ['./delete-campaign-test-plan-item.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DeleteCampaignTestPlanItemComponent extends AbstractDeleteCellRenderer {

  constructor(public grid: GridService,
              cdr: ChangeDetectorRef,
              dialogService: DialogService,
              private restService: RestService,
              public readonly campaignViewService: CampaignViewService) {
    super(grid, cdr, dialogService);
  }

  getIcon(): string {
    return 'sqtm-core-generic:unlink';
  }

  canDelete(row: DataRow): boolean {
    return row.simplePermissions && row.simplePermissions.canDelete;
  }

  protected doDelete(): any {
    this.grid.beginAsyncOperation();
    const campaignId = this.row.data['campaignId'];
    const itemTestPlanId = this.row.data['ctpiId'];
    this.restService.delete(['campaign', campaignId, 'test-plan', itemTestPlanId])
      .pipe(
        finalize(() => this.grid.completeAsyncOperation())
      )
      .subscribe(() => this.grid.refreshData());
  }

  protected getLevel(): ConfirmDeleteLevel {
    return 'WARNING';
  }

  protected getMessageKey(): string {
    return 'sqtm-core.campaign-workspace.dialog.message.remove-association.unbind';
  }

  protected getTitleKey(): string {
    return 'sqtm-core.campaign-workspace.dialog.title.remove-association';
  }
}

export function deleteCampaignTestPlanItemColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(DeleteCampaignTestPlanItemComponent);
}
