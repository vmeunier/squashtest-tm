import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {CampaignLibraryViewComponent} from './campaign-library-view.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {TranslateModule} from '@ngx-translate/core';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';

describe('TestCaseLibraryViewComponent', () => {
  let component: CampaignLibraryViewComponent;
  let fixture: ComponentFixture<CampaignLibraryViewComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, HttpClientTestingModule, RouterTestingModule, TranslateModule.forRoot()],
      declarations: [CampaignLibraryViewComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignLibraryViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
