import {EntityViewState, provideInitialViewState} from 'sqtm-core';
import {TestSuiteState} from './test-suite.state';


export interface TestSuiteViewState extends EntityViewState<TestSuiteState, 'testSuite'> {
  testSuite: TestSuiteState;
}

export function provideInitialTestSuiteView(): Readonly<TestSuiteViewState> {
  return provideInitialViewState<TestSuiteState, 'testSuite'>('testSuite');
}
