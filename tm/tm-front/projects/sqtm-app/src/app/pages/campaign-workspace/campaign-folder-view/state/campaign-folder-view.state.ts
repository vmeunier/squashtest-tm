import {EntityViewState, provideInitialViewState} from 'sqtm-core';
import {CampaignFolderState} from './campaign-folder.state';

export interface CampaignFolderViewState extends EntityViewState<CampaignFolderState, 'campaignFolder'> {
  campaignFolder: CampaignFolderState;
}

export function provideInitialCampaignFolderView(): Readonly<CampaignFolderViewState> {
  return provideInitialViewState<CampaignFolderState, 'campaignFolder'>('campaignFolder');
}
