import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {IterationViewComponentData} from '../iteration-view/iteration-view.component';
import {IterationViewService} from '../../services/iteration-view.service';
import {iterationViewLogger} from '../../iteration-view.logger';
import {BindableEntity, createCustomFieldValueDataSelector, CustomFieldData} from 'sqtm-core';
import {takeUntil} from 'rxjs/operators';
import {select} from '@ngrx/store';

const logger = iterationViewLogger.compose('IterationViewContentComponent');

@Component({
  selector: 'sqtm-app-iteration-view-content',
  templateUrl: './iteration-view-content.component.html',
  styleUrls: ['./iteration-view-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IterationViewContentComponent implements OnInit {

  private unsub$ = new Subject<void>();
  componentData$: Observable<IterationViewComponentData>;
  customFieldData: CustomFieldData[];

  constructor(private iterationViewService: IterationViewService) {
    this.componentData$ = this.iterationViewService.componentData$;
  }

  ngOnInit() {
    this.componentData$.pipe(
      takeUntil(this.unsub$),
      select(createCustomFieldValueDataSelector(BindableEntity.ITERATION))
    ).subscribe((customFieldData: CustomFieldData[]) => {
      this.customFieldData = customFieldData;
    });
  }
}
