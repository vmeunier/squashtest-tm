import {
  CampaignStatisticsBundle,
  CustomDashboardModel,
  Milestone,
  NamedReference,
  SimpleUser,
  SqtmEntityState
} from 'sqtm-core';


export interface CampaignState extends SqtmEntityState {

  name: string;
  reference: string;
  description: string;
  campaignStatus: string;
  lastModifiedOn: string;
  lastModifiedBy: string;
  createdOn: string;
  createdBy: string;
  milestones: Milestone[];
  testPlanStatistics: TestPlanStatistics;
  actualStartDate: Date;
  actualEndDate: Date;
  actualStartAuto: boolean;
  actualEndAuto: boolean;
  scheduledStartDate: Date;
  scheduledEndDate: Date;
  nbIssues: number;
  hasDatasets: boolean;
  users: SimpleUser[];
  testSuites: NamedReference[];
  uiState: {
    openTestCaseTreePicker: boolean
  };
  shouldShowFavoriteDashboard: boolean;
  canShowFavoriteDashboard: boolean;
  favoriteDashboardId: number;
  campaignStatisticsBundle?: CampaignStatisticsBundle;
  dashboard?: CustomDashboardModel;
  nbTestPlanItems: number;
}

export interface TestPlanStatistics {
  nbTestCases: number;
  progression: number;
  status: string;
  nbDone: number;
  nbBlocked: number;
  nbReady: number;
  nbSettled: number;
  nbUntestable: number;
  nbSuccess: number;
  nbRunning: number;
  nbFailure: number;
}
