import {EntityViewState, provideInitialViewState} from 'sqtm-core';
import {IterationState} from './iteration.state';

export interface IterationViewState extends EntityViewState<IterationState, 'iteration'> {
  iteration: IterationState;
}

export function provideInitialIterationView(): Readonly<IterationViewState> {
  return provideInitialViewState<IterationState, 'iteration'>('iteration');
}
