import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { RestService } from 'sqtm-core';
import { AppTestingUtilsModule } from '../../../../../utils/testing-utils/app-testing-utils.module';
import { mockPassThroughTranslateService } from '../../../../../utils/testing-utils/mocks.service';

import { AutomationProjectMenuComponent } from './automation-project-menu.component';

describe('AutomationProjectMenuComponent', () => {
  let component: AutomationProjectMenuComponent;
  let fixture: ComponentFixture<AutomationProjectMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AutomationProjectMenuComponent ],
      imports: [AppTestingUtilsModule, HttpClientTestingModule],
      providers: [{
        provide: TranslateService, useValue: mockPassThroughTranslateService()
      },
      RestService],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutomationProjectMenuComponent);
    component = fixture.componentInstance;
    component.formGroup = new FormGroup({});
    component.automationProject = {
      projectId: 1,
      label: 'Job1',
      server: 'Jenkins',
      nodes: ['Agent1, Agent2', 'Agent3'],
      testCount: 4
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
