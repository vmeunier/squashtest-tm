import {AfterViewInit, ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {IterationComponentData} from '../iteration-test-plan-execution/iteration-test-plan-execution.component';
import {IterationViewService} from '../../services/iteration-view.service';
import {GridService, gridServiceFactory, ReferentialDataService, RestService} from 'sqtm-core';
import {DatePipe} from '@angular/common';
import {ITERATION_AUTOMATED_SUITE_TABLE, ITERATION_AUTOMATED_SUITE_TABLE_CONF} from '../../iteration-view.constant';
import {filter, take, takeUntil} from 'rxjs/operators';
import {IterationViewComponentData} from '../../container/iteration-view/iteration-view.component';
import {automatedSuiteTableDefinition} from '../../../automated-suite-grid-conf';

@Component({
  selector: 'sqtm-app-iteration-automated-suite',
  templateUrl: './iteration-automated-suite.component.html',
  styleUrls: ['./iteration-automated-suite.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    DatePipe,
    {
      provide: ITERATION_AUTOMATED_SUITE_TABLE_CONF,
      useFactory: automatedSuiteTableDefinition
    },
    {
      provide: ITERATION_AUTOMATED_SUITE_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, ITERATION_AUTOMATED_SUITE_TABLE_CONF, ReferentialDataService]
    },
    {provide: GridService, useExisting: ITERATION_AUTOMATED_SUITE_TABLE},
  ]
})
export class IterationAutomatedSuiteComponent implements OnInit, AfterViewInit, OnDestroy {

  componentData$: Observable<IterationComponentData>;
  unsub$ = new Subject<void>();

  constructor(private iterationViewService: IterationViewService, private gridService: GridService) {
  }

  ngOnInit(): void {
    this.componentData$ = this.iterationViewService.componentData$;
  }

  ngAfterViewInit(): void {
    this.fetchTestPlan();
  }

  ngOnDestroy(): void {
    this.gridService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  private fetchTestPlan() {
    this.iterationViewService.componentData$
      .pipe(
        takeUntil(this.unsub$),
        filter((componentData: IterationViewComponentData) => Boolean(componentData.iteration.id)),
        take(1)
      )
      .subscribe(componentData => {
        this.gridService.setServerUrl([`iteration/${componentData.iteration.id}/automated-suite`]);
      });
  }

}
