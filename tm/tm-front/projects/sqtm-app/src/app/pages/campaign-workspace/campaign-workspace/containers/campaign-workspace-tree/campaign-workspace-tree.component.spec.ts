import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {CampaignWorkspaceTreeComponent} from './campaign-workspace-tree.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {DialogService, GridService} from 'sqtm-core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {EMPTY, of} from 'rxjs';
import {CAMPAIGN_WS_TREE} from '../../../campaign-workspace.constant';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import {RouterTestingModule} from '@angular/router/testing';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';

describe('CampaignWorkspaceTreeComponent', () => {
  let component: CampaignWorkspaceTreeComponent;
  let fixture: ComponentFixture<CampaignWorkspaceTreeComponent>;

  const dialogService = jasmine.createSpyObj('dialogService', ['create']);
  const tableMock = jasmine.createSpyObj('tableMock', ['load', 'setColumnSorts']);
  tableMock.selectedRows$ = EMPTY;
  tableMock.selectedRowIds$ = EMPTY;
  tableMock.openedRowIds$ = EMPTY;
  tableMock.sortedColumns$ = EMPTY;
  tableMock.loaded$ = of(false);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, HttpClientTestingModule, NzDropDownModule, RouterTestingModule],
      declarations: [CampaignWorkspaceTreeComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [{
        provide: DialogService,
        useValue: dialogService
      }]
    })
      .compileComponents().then(() => {
      TestBed.overrideProvider(CAMPAIGN_WS_TREE, {useValue: tableMock});
      TestBed.overrideProvider(GridService, {useValue: tableMock});
      fixture = TestBed.createComponent(CampaignWorkspaceTreeComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
