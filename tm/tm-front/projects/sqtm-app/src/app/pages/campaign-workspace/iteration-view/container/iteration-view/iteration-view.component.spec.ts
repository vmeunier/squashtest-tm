import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {IterationViewComponent} from './iteration-view.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TranslateModule} from '@ngx-translate/core';
import {EMPTY} from 'rxjs';
import {IterationViewService} from '../../services/iteration-view.service';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import {WorkspaceWithTreeComponent} from 'sqtm-core';

describe('IterationViewComponent', () => {
  let component: IterationViewComponent;
  let fixture: ComponentFixture<IterationViewComponent>;

  let iterationViewService = jasmine.createSpyObj('iterationViewService', ['load']);
  iterationViewService = {...iterationViewService, componentData$: EMPTY};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [IterationViewComponent],
      imports: [
        AppTestingUtilsModule,
        RouterTestingModule,
        HttpClientTestingModule,
        TranslateModule.forRoot(),
        NzDropDownModule,
      ],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {provide: IterationViewService, useValue: iterationViewService},
        {provide: WorkspaceWithTreeComponent, useValue: WorkspaceWithTreeComponent}
      ]

    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IterationViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
