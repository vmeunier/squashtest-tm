import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnInit,
  TemplateRef,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {
  AbstractListCellRendererComponent,
  ActionErrorDisplayService,
  ColumnDefinitionBuilder,
  formatFullUserName,
  GridService,
  ListPanelItem,
  RestService,
  SimpleUser,
  TableValueChange
} from 'sqtm-core';
import {Overlay, OverlayRef} from '@angular/cdk/overlay';
import {TestSuiteViewComponentData} from '../../../containers/test-suite-view/test-suite-view.component';
import {TranslateService} from '@ngx-translate/core';
import {TestSuiteViewService} from '../../../services/test-suite-view.service';
import {filter, map, take, takeUntil} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Component({
  selector: 'sqtm-app-test-suite-assigned-user-cell',
  templateUrl: './test-suite-assigned-user-cell.component.html',
  styleUrls: ['./test-suite-assigned-user-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TestSuiteAssignedUserCellComponent extends AbstractListCellRendererComponent implements OnInit {

  @ViewChild('templatePortalContent', {read: TemplateRef})
  templatePortalContent: TemplateRef<any>;

  @ViewChild('availableUsers', {read: ElementRef})
  availableUsers: ElementRef;

  overlayRef: OverlayRef;

  readonly rowHeight = 29;
  numberOfItemsToDisplay = 8;
  scrollbar: Boolean = false;

  panelItems: ListPanelItem[] = [];

  componentData$: Observable<TestSuiteViewComponentData>;
  canEdit$: Observable<boolean>;

  constructor(public grid: GridService,
              public cdRef: ChangeDetectorRef,
              public readonly translateService: TranslateService,
              public readonly overlay: Overlay,
              public readonly vcr: ViewContainerRef,
              public readonly restService: RestService,
              public readonly actionErrorDisplayService: ActionErrorDisplayService,
              public testSuiteViewService: TestSuiteViewService) {
    super(grid, cdRef, overlay, vcr, translateService, restService, actionErrorDisplayService);
  }

  get assignedUser(): string {
    return this.row.data[this.columnDisplay.id] || '-';
  }

  ngOnInit(): void {
    this.componentData$ = this.testSuiteViewService.componentData$.pipe(
      takeUntil(this.unsub$));

    this.canEdit$ = this.componentData$.pipe(
      map(componentData => componentData.permissions.canWrite && componentData.milestonesAllowModification));

    this.componentData$.pipe(
      take(1)
    ).subscribe(componentData => {
      this.panelItems.push({id: 0, label: this.translateService.instant('sqtm-core.campaign-workspace.test-plan.label.user.unassigned')});
      componentData.testSuite.users.map(user => this.panelItems.push({id: user.id, label: this.getFullUsername(user)}));
      this.scrollbar = this.panelItems.length > this.numberOfItemsToDisplay;
    });
  }

  showAvailableUsersList() {
    this.canEdit$.pipe(
      take(1),
      filter(canEdit => canEdit)
    ).subscribe(() => this.showList(this.availableUsers, this.templatePortalContent,
      [{originX: 'start', overlayX: 'start', originY: 'bottom', overlayY: 'top', offsetX: -10, offsetY: 6},
        {originX: 'start', overlayX: 'start', originY: 'top', overlayY: 'bottom', offsetX: -10, offsetY: -6}]));
  }

  get minAvailableItemsHeight(): number {
    const nbItemMin = Math.min(this.panelItems.length, this.numberOfItemsToDisplay);
    return nbItemMin * this.rowHeight;
  }

  getFullUsername(user: SimpleUser): string {
    return formatFullUserName(user) || '-';
  }

  change(userId: number) {
    const userItem = this.panelItems.filter(item => item.id === userId);
    const newUserName = userId === 0 ? '-' : userItem[0].label;
    this.testSuiteViewService.updateAssignedUser([this.row.data['itemTestPlanId']], userId).subscribe();
    const changes: TableValueChange[] = [
      {columnId: 'user', value: newUserName},
      {columnId: 'userId', value: userId === 0 ? null : userId},
    ];
    this.grid.editRows([this.row.id], changes);
    this.close();
  }
}

export function testSuiteAssignedUserColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(TestSuiteAssignedUserCellComponent)
    .withHeaderPosition('left');
}
