import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {concatMap, filter, map, take, takeUntil} from 'rxjs/operators';
import {IterationViewService} from '../../services/iteration-view.service';
import {Observable, Subject} from 'rxjs';
import {IterationViewComponentData} from '../iteration-view/iteration-view.component';
import {
  CampaignProgressionStatistics,
  CustomDashboardModel,
  EntityRowReference,
  EntityScope,
  ExecutionStatusCount,
  SquashTmDataRowType
} from 'sqtm-core';
import * as _ from 'lodash';

@Component({
  selector: 'sqtm-app-iteration-view-dashboard',
  templateUrl: './iteration-view-dashboard.component.html',
  styleUrls: ['./iteration-view-dashboard.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IterationViewDashboardComponent implements OnInit, OnDestroy {
  componentData$: Observable<IterationViewComponentData>;
  iterationProgressionData$: Observable<CampaignProgressionStatistics>;
  private unsub$: Subject<void> = new Subject<void>();

  constructor(private readonly iterationViewService: IterationViewService) {
    this.componentData$ = iterationViewService.componentData$;

    // mapping IterationProgressionStatistics into CampaignProgressionStatistics to allow reuse of campaign progression chart.
    this.iterationProgressionData$ = this.componentData$.pipe(
      takeUntil(this.unsub$),
      filter(componentData => Boolean(componentData.iteration.iterationStatisticsBundle)),
      filter(componentData => {
        const errors = componentData.iteration.iterationStatisticsBundle.iterationProgressionStatistics.errors;
        return !errors || errors.length === 0;
      }),
      map(componentData => {
        const progressionStatistics = componentData.iteration.iterationStatisticsBundle.iterationProgressionStatistics;
        return {
          cumulativeExecutionsPerDate: progressionStatistics.cumulativeExecutionsPerDate || [],
          errors: progressionStatistics.errors,
          scheduledIterations: [progressionStatistics.scheduledIteration]
        };
      })
    );
  }

  ngOnInit(): void {
    this.componentData$.pipe(
      take(1),
      concatMap(() => this.iterationViewService.refreshStats())
    ).subscribe();
  }

  campaignProgressionChartHasErrors(componentData: IterationViewComponentData) {
    const i18nErrors = componentData.iteration.iterationStatisticsBundle.iterationProgressionStatistics.errors;
    return i18nErrors && i18nErrors.length > 0;
  }

  hasTestPlanItems(executionStatusCount: ExecutionStatusCount) {
    return Object.values(executionStatusCount).reduce(_.add, 0) > 0;
  }

  getScope(componentData: IterationViewComponentData): EntityScope[] {
    const id = new EntityRowReference(componentData.iteration.id, SquashTmDataRowType.Iteration).asString();
    return [{id, label: componentData.iteration.name, projectId: componentData.projectData.id}];
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  getChartBindings(dashboard: CustomDashboardModel) {
    return [...dashboard.chartBindings, ...dashboard.reportBindings];
  }

  displayFavoriteDashboard($event) {
    $event.stopPropagation();
    this.iterationViewService.changeDashboardToDisplay('dashboard');
  }

  displayDefaultDashboard($event) {
    $event.stopPropagation();
    this.iterationViewService.changeDashboardToDisplay('default');
  }
}
