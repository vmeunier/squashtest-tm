import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {CampaignWorkspaceComponent} from './campaign-workspace.component';
import {TranslateService} from '@ngx-translate/core';
import {EventEmitter, NO_ERRORS_SCHEMA} from '@angular/core';
import {RouterTestingModule} from '@angular/router/testing';
import {of} from 'rxjs';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {DialogModule} from 'sqtm-core';

describe('CampaignWorkspaceComponent', () => {
  let component: CampaignWorkspaceComponent;
  let fixture: ComponentFixture<CampaignWorkspaceComponent>;

  let fakeTranslateService = jasmine.createSpyObj<TranslateService>('TranslateService', ['get']);

  fakeTranslateService = {
    ...fakeTranslateService,
    onLangChange: new EventEmitter(),
    onTranslationChange: new EventEmitter(),
    onDefaultLangChange: new EventEmitter()
  } as jasmine.SpyObj<TranslateService>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule, AppTestingUtilsModule, DialogModule],
      declarations: [CampaignWorkspaceComponent],
      providers: [
        {provide: TranslateService, useValue: fakeTranslateService}
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents()
      .then(() => {
        fakeTranslateService.get.and.returnValue(of());
        fixture = TestBed.createComponent(CampaignWorkspaceComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
      });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
