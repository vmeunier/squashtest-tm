import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppTestingUtilsModule } from '../../../../../../utils/testing-utils/app-testing-utils.module';

import { SquashAutomExecutionTableComponent } from './squash-autom-execution-table.component';

describe('SquashAutomExecutionTableComponent', () => {
  let component: SquashAutomExecutionTableComponent;
  let fixture: ComponentFixture<SquashAutomExecutionTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SquashAutomExecutionTableComponent ],
      imports: [ AppTestingUtilsModule, HttpClientTestingModule ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SquashAutomExecutionTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
