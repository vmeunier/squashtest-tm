import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {CampaignLibraryViewContentComponent} from './campaign-library-view-content.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {EMPTY} from 'rxjs';
import SpyObj = jasmine.SpyObj;
import {CampaignLibraryViewService} from '../../service/campaign-library-view.service';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';

describe('CampaignLibraryViewContentComponent', () => {
  let component: CampaignLibraryViewContentComponent;
  let fixture: ComponentFixture<CampaignLibraryViewContentComponent>;

  const campaignLibraryViewService: SpyObj<CampaignLibraryViewService> = jasmine.createSpyObj('campaignLibraryViewService', ['load']);
  campaignLibraryViewService.componentData$ = EMPTY;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule],
      declarations: [CampaignLibraryViewContentComponent],
      providers: [
        {
          provide: CampaignLibraryViewService,
          useValue: campaignLibraryViewService
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignLibraryViewContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
