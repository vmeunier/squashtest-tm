import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {NO_ERRORS_SCHEMA} from '@angular/core';
import {EMPTY} from 'rxjs';
import {CampaignFolderViewService} from '../../services/campaign-folder-view.service';
import SpyObj = jasmine.SpyObj;
import {CampaignFolderViewContentComponent} from './campaign-folder-view-content.component';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';

describe('CampaignFolderViewContentComponent', () => {
  let component: CampaignFolderViewContentComponent;
  let fixture: ComponentFixture<CampaignFolderViewContentComponent>;

  const campaignFolderViewService: SpyObj<CampaignFolderViewService> = jasmine.createSpyObj('campaignFolderViewService', ['load']);
  campaignFolderViewService.componentData$ = EMPTY;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule],
      declarations: [CampaignFolderViewContentComponent],
      providers: [
        {
          provide: CampaignFolderViewService,
          useValue: campaignFolderViewService
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignFolderViewContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
