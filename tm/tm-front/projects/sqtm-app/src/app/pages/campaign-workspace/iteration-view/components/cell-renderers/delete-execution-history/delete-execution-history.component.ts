import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {
  AbstractDeleteCellRenderer,
  ColumnDefinitionBuilder,
  ConfirmDeleteLevel,
  DataRow,
  DialogService,
  GridService,
  RestService
} from 'sqtm-core';
import {finalize} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-delete-execution-history',
  template: `
    <ng-container *ngIf="row && canDelete(row)">
      <sqtm-core-delete-icon
        [iconName]="getIcon()"
        (delete)="showDeleteConfirm()"></sqtm-core-delete-icon>
    </ng-container>
  `,
  styleUrls: ['./delete-execution-history.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DeleteExecutionHistoryComponent extends AbstractDeleteCellRenderer {

  constructor(public grid: GridService,
              cdr: ChangeDetectorRef,
              dialogService: DialogService,
              private restService: RestService) {
    super(grid, cdr, dialogService);
  }

  getIcon(): string {
    return 'sqtm-core-generic:delete';
  }

  canDelete(row: DataRow): boolean {
    return row.simplePermissions && row.simplePermissions.canDelete
      && !row.data.boundToBlockingMilestone;
  }

  protected doDelete(): any {
    this.grid.beginAsyncOperation();
    const iterationId = this.row.data['iterationId'];
    const executionId = this.row.data['executionId'];
    this.restService.delete(['iteration', iterationId, 'test-plan/execution', executionId])
      .pipe(
        finalize(() => this.grid.completeAsyncOperation())
      )
      .subscribe(() => this.grid.refreshData());
  }


  protected getLevel(): ConfirmDeleteLevel {
    return 'DANGER';
  }

  protected getMessageKey(): string {
    return 'sqtm-core.campaign-workspace.dialog.message.remove-execution';
  }

  protected getTitleKey(): string {
    return 'sqtm-core.campaign-workspace.dialog.title.remove-execution';
  }
}

export function deleteExecutionHistoryColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(DeleteExecutionHistoryComponent);
}
