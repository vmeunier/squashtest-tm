import {ChangeDetectionStrategy, Component, Inject} from '@angular/core';
import {DataRow, DefaultGridDraggedContentComponent, DRAG_AND_DROP_DATA, DragAndDropData, GridService} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-campaign-test-plan-dragged-content',
  templateUrl: './campaign-test-plan-dragged-content.component.html',
  styleUrls: ['./campaign-test-plan-dragged-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CampaignTestPlanDraggedContentComponent extends DefaultGridDraggedContentComponent {

  constructor(@Inject(DRAG_AND_DROP_DATA) public dragAnDropData: DragAndDropData,
              public readonly grid: GridService,
              public readonly translateService: TranslateService) {
    super(dragAnDropData, grid);
  }

  getText(dataRow: Readonly<DataRow>): string {
    return dataRow.data['testCaseName'] ?? this.translateService.instant(DELETED_TC_KEY);
  }
}

const DELETED_TC_KEY = 'sqtm-core.campaign-workspace.test-plan.label.deleted-test-case';
