import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import { DeleteTestSuiteTestPlanItemComponent } from './delete-test-suite-test-plan-item.component';
import {
  DataRow, DialogService, GenericDataRow,
  getBasicGridDisplay,
  grid,
  GridDefinition,
  GridService,
  gridServiceFactory,
  Limited, ReferentialDataService,
  RestService
} from 'sqtm-core';
import {EMPTY} from 'rxjs';
import {TestSuiteViewService} from '../../../services/test-suite-view.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('DeleteTestSuiteTestPlanItemComponent', () => {
  let component: DeleteTestSuiteTestPlanItemComponent;
  let fixture: ComponentFixture<DeleteTestSuiteTestPlanItemComponent>;

  const gridConfig = grid('grid-test').build();
  const restService = {};
  const dialogService = jasmine.createSpyObj('dialogService', ['create']);
  let testSuiteViewService = jasmine.createSpyObj('testSuiteViewService', ['load']);
  testSuiteViewService = {...testSuiteViewService, componentData$: EMPTY};


  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [DeleteTestSuiteTestPlanItemComponent],
      providers: [
        {provide: GridDefinition, useValue: gridConfig},
        {provide: RestService, useValue: restService},
        {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        },
        {provide: DialogService, useValue: dialogService},
        {provide: TestSuiteViewService, useValue: testSuiteViewService}
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  it('should create', waitForAsync(() => {
    prepareComponent({
      ...new GenericDataRow(),
      id: 'itpi-1',
      data: {itemTestPlanId: 'itpi-1', lastExecutedOn: '2020-02-05', testCaseName: 'TC1'}
    });
    expect(component).toBeTruthy();
  }));

  function prepareComponent(row: DataRow) {
    fixture = TestBed.createComponent(DeleteTestSuiteTestPlanItemComponent);
    component = fixture.componentInstance;
    component.gridDisplay = getBasicGridDisplay();
    component.columnDisplay = {
      id: 'NAME', show: true,
      widthCalculationStrategy: new Limited(200),
      headerPosition: 'left', contentPosition: 'left', showHeader: true,
      viewportName: 'mainViewport'
    };
    component.row = row;
    fixture.detectChanges();
  }
});
