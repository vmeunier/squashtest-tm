import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SuccessRateComponent } from './success-rate.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {
  DialogModule,
  getBasicGridDisplay,
  grid,
  GridDefinition,
  GridService, gridServiceFactory, Limited, ReferentialDataService,
  RestService, TestCaseLibrary,
  TextCellRendererComponent
} from 'sqtm-core';

describe('SuccessRateComponent', () => {
  let component: TextCellRendererComponent;
  let fixture: ComponentFixture<TextCellRendererComponent>;

  const gridConfig = grid('grid-test').build();
  const restService = {};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [DialogModule],
      declarations: [TextCellRendererComponent],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig
        }, {
          provide: RestService,
          useValue: restService
        }, {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(TextCellRendererComponent);
        component = fixture.componentInstance;
        component.gridDisplay = getBasicGridDisplay();
        component.columnDisplay = {
          id: 'column-1', show: true,
          widthCalculationStrategy: new Limited(200),
          headerPosition: 'left', contentPosition: 'left', showHeader: true,
          viewportName: 'mainViewport'
        };
        component.row = {
          ...new TestCaseLibrary(),
          id: 'tcln-1',
          data: {id: 'tcln-1'},
        };
        fixture.detectChanges();
      });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
