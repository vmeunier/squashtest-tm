import {TestBed} from '@angular/core/testing';
import {AppTestingUtilsModule} from '../../../../utils/testing-utils/app-testing-utils.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TranslateModule} from '@ngx-translate/core';
import {TestSuiteViewService} from './test-suite-view.service';
import {TestSuiteTestPlanOperationHandler} from './test-suite-test-plan-operation-handler';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {mockGridService} from '../../../../utils/testing-utils/mocks.service';
import {of} from 'rxjs';
import {GridState} from 'sqtm-core';


describe('Test suite Test Plan Operation Handler', () => {
  const testSuiteViewService = jasmine.createSpyObj(['changeItemsPosition']);

  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      AppTestingUtilsModule,
      HttpClientTestingModule,
      TranslateModule.forRoot()
    ],
    providers: [
      {provide: TestSuiteViewService, useValue: testSuiteViewService},
      TestSuiteTestPlanOperationHandler
    ],
    schemas: [NO_ERRORS_SCHEMA],
  }));

  it('should be created', () => {
    const service: TestSuiteTestPlanOperationHandler = TestBed.inject(TestSuiteTestPlanOperationHandler);
    expect(service).toBeTruthy();
  });

  it('should handle internal reordering', () => {
    const service: TestSuiteTestPlanOperationHandler = TestBed.inject(TestSuiteTestPlanOperationHandler);
    service.grid = mockGridService();
    expect(service).toBeTruthy();
    service.grid.isSortedOrFiltered$ = of(false);

    testSuiteViewService.changeItemsPosition.and.returnValue(of({}));

    service.grid.gridState$ = of({
      dataRowState: {
        ids: [2, 5, 1, 3],
      },
      uiState: {
        dragState: {
          draggedRowIds: [3, 5],
          dragging: true,
          currentDndTarget: {
            id: 1,
            zone: 'bellow',
          }
        }
      }
    } as GridState);

    service.notifyInternalDrop();

    expect(testSuiteViewService.changeItemsPosition).toHaveBeenCalledWith([3, 5], 2);
  });
});
