import {TestBed} from '@angular/core/testing';

import {IterationViewService} from './iteration-view.service';
import {
  AttachmentService,
  CustomFieldValueService,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  IterationModel,
  ReferentialDataService,
  RestService,
} from 'sqtm-core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {of} from 'rxjs';
import {AppTestingUtilsModule} from '../../../../utils/testing-utils/app-testing-utils.module';
import {mockIterationModel} from '../../../../utils/testing-utils/mocks.data';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import SpyObj = jasmine.SpyObj;

describe('IterationViewService', () => {
  const attachmentHelperMock = jasmine.createSpyObj('attachmentHelper', ['initializeAttachmentState', 'mapUploadEventToState']);
  const customFieldHelper = jasmine.createSpyObj('customFieldHelper', ['initializeCustomFieldValueState', 'updateCustomFieldValue']);
  const referentialDataService: SpyObj<ReferentialDataService> = jasmine.createSpyObj('referentialDataService', ['connectToProjectData']);
  referentialDataService.globalConfiguration$ = of({
    milestoneFeatureEnabled: false,
    uploadFileExtensionWhitelist: ['txt'],
    uploadFileSizeLimit: 1000,
    squashVersion: '2.0'
  });

  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      AppTestingUtilsModule,
      HttpClientTestingModule,
      TranslateModule.forRoot()
    ],
    providers: [
      {
        provide: ReferentialDataService,
        useValue: referentialDataService
      },
      {
        provide: IterationViewService,
        useClass: IterationViewService,
        deps: [
          RestService,
          ReferentialDataService,
          AttachmentService,
          CustomFieldValueService,
          TranslateService,
          EntityViewAttachmentHelperService,
          EntityViewCustomFieldHelperService,
        ]
      }
    ],
    schemas: [NO_ERRORS_SCHEMA],
  }));

  it('should be created', () => {
    const service: IterationViewService = TestBed.inject(IterationViewService);
    expect(service).toBeTruthy();
  });

  it('should toggle test case drawer', (done) => {
    const service: IterationViewService = TestBed.inject(IterationViewService);
    attachmentHelperMock.initializeAttachmentState.and.returnValue({ids: [], entities: {}});
    customFieldHelper.initializeCustomFieldValueState.and.returnValue({ids: [], entities: {}});
    referentialDataService.connectToProjectData.and.returnValue(of({customFieldBindings: []} as any));
    service.initializeIteration(mockIterationModel({
      id: 1,
      itpi: [],
      attachmentList: {id: 1, attachments: []},
      customFieldValues: null,
      projectId: 1,
      name: 'iteration-1',
      reference: '',
      description: '',
      hasDatasets: false,
      executionStatusMap: new Map<number, string>()
    } as IterationModel));

    service.toggleTestCaseTreePicker();

    service.componentData$.subscribe(
      state => {
        expect(state.iteration.uiState.openTestCaseTreePicker).toBeTruthy();
        done();
      }
    );
  });
});
