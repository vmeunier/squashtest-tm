import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectLinkCellComponent } from './project-link-cell.component';
import {GridService} from 'sqtm-core';
import {mockGridService, mockPassThroughTranslateService} from '../../../../../../utils/testing-utils/mocks.service';
import {TranslateService} from '@ngx-translate/core';

describe('ProjectLinkCellComponent', () => {
  let component: ProjectLinkCellComponent;
  let fixture: ComponentFixture<ProjectLinkCellComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectLinkCellComponent ],
      providers: [
        { provide: GridService, useValue: mockGridService() },
        { provide: TranslateService, useValue: mockPassThroughTranslateService() },
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectLinkCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
