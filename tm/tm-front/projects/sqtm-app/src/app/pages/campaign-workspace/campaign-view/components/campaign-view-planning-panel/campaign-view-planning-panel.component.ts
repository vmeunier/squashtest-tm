import {ChangeDetectionStrategy, Component, ElementRef, Host, Input, ViewChild} from '@angular/core';
import {CampaignViewComponentData} from '../../container/campaign-view/campaign-view.component';
import {DialogReference, DialogService, EditableDateFieldComponent} from 'sqtm-core';
import {CampaignViewService} from '../../service/campaign-view.service';

@Component({
  selector: 'sqtm-app-campaign-view-planning-panel',
  templateUrl: './campaign-view-planning-panel.component.html',
  styleUrls: ['./campaign-view-planning-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CampaignViewPlanningPanelComponent {

  @ViewChild('actualStartDate')
  actualStartDate: EditableDateFieldComponent;

  @ViewChild('actualEndDate')
  actualEndDate: EditableDateFieldComponent;

  @ViewChild('scheduledStartDate')
  scheduledStartDate: EditableDateFieldComponent;

  @ViewChild('scheduledEndDate')
  scheduledEndDate: EditableDateFieldComponent;

  @Input()
  campaignViewComponentData: CampaignViewComponentData;

  private readonly TWO_COLUMNS_LAYOUT_MIN_SIZE = 600;

  get canEdit(): boolean {
    return this.campaignViewComponentData.permissions.canWrite
      && this.campaignViewComponentData.milestonesAllowModification;
  }

  get isActualStartDateEditable(): boolean {
    return this.canEdit && !this.campaignViewComponentData.campaign.actualStartAuto;
  }

  get isActualEndDateEditable(): boolean {
    return this.canEdit && !this.campaignViewComponentData.campaign.actualEndAuto;
  }

  get isSmallLayout(): boolean {
    return this.hostElement.nativeElement.getBoundingClientRect().width < this.TWO_COLUMNS_LAYOUT_MIN_SIZE;
  }

  constructor(private campaignViewService: CampaignViewService,
              private dialogService: DialogService,
              @Host() private hostElement: ElementRef<HTMLElement>) {
  }

  updateScheduledStartDate(scheduledStartDate: Date, scheduledEndDate: Date) {
    if (this.canUpdateDate(scheduledStartDate, scheduledEndDate)) {
      this.campaignViewService.updateScheduledStartDate(scheduledStartDate).subscribe(
        () => this.scheduledStartDate.value = scheduledStartDate
      );
    } else {
      this.showAlertDateDialog();
      this.scheduledStartDate.cancel();
    }
  }

  updateScheduledEndDate(scheduledEndDate: Date, scheduledStartDate: Date) {
    if (this.canUpdateDate(scheduledStartDate, scheduledEndDate)) {
      this.campaignViewService.updateScheduledEndDate(scheduledEndDate).subscribe(
        () => this.scheduledEndDate.value = scheduledEndDate
      );
    } else {
      this.showAlertDateDialog();
      this.scheduledEndDate.cancel();
    }
  }

  updateActualStartDate(actualStartDate: Date, actualEndDate: Date) {
    if (this.canUpdateDate(actualStartDate, actualEndDate)) {
      this.campaignViewService.updateActualStartDate(actualStartDate).subscribe(
        () => this.actualStartDate.value = actualStartDate
      );
    } else {
      this.showAlertDateDialog();
      this.actualStartDate.cancel();
    }
  }

  updateActualEndDate(actualEndDate: Date, actualStartDate: Date) {
    if (this.canUpdateDate(actualStartDate, actualEndDate)) {
      this.campaignViewService.updateActualEndDate(actualEndDate).subscribe(
        () => this.actualEndDate.value = actualEndDate
      );
    } else {
      this.showAlertDateDialog();
      this.actualEndDate.cancel();
    }
  }

  updateActualStartAuto(startAuto: boolean) {
    this.campaignViewService.updateActualStartAuto(startAuto).subscribe();
  }

  updateActualEndAuto(endAuto: boolean) {
    this.campaignViewService.updateActualEndAuto(endAuto).subscribe();
  }

  showAlertDateDialog(): DialogReference {
    return this.dialogService.openAlert({
      level: 'DANGER',
      messageKey: 'sqtm-core.validation.errors.time-period-not-consistent'
    });
  }

  private canUpdateDate(startDate: Date, endDate: Date) {
    if (startDate != null && endDate != null) {
      const start = new Date(startDate);
      const end = new Date(endDate);
      return start < end;
    } else {
      return true;
    }
  }
}
