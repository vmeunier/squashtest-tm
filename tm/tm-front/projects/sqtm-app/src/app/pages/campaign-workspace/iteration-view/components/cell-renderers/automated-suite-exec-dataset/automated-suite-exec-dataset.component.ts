import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {AbstractCellRendererComponent, ColumnDefinitionBuilder, GridService} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-automated-suite-exec-dataset',
  template: `
    <ng-container *ngIf="columnDisplay && row">
      <div class="flex full-width full-height">
    <span
      style="margin: auto 0;"
      nz-tooltip
      class="sqtm-grid-cell-txt-renderer"
      [sqtmCoreLabelTooltip]="dataSet">
          {{dataSet}}
    </span>
      </div>
    </ng-container>`,
  styleUrls: ['./automated-suite-exec-dataset.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AutomatedSuiteExecDatasetComponent extends AbstractCellRendererComponent implements OnInit {

  constructor(gridService: GridService, cdrRef: ChangeDetectorRef, private translateService: TranslateService) {
    super(gridService, cdrRef);
  }

  ngOnInit(): void {
  }

  get dataSet() {
    if (this.row.data[this.columnDisplay.id] == null) {
      return this.translateService.instant('sqtm-core.campaign-workspace.test-plan.label.dataset.none');
    } else {
      return this.row.data[this.columnDisplay.id];
    }
  }

}

export function automatedSuiteDatasetColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(AutomatedSuiteExecDatasetComponent);
}
