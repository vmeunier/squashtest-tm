import {Injectable} from '@angular/core';
import {
  AttachmentService,
  CustomFieldValueService, EntityViewAttachmentHelperService, EntityViewCustomFieldHelperService,
  EntityViewService, ProjectData,
  ReferentialDataService,
  RestService,
  CampaignPermissions,
  CampaignLibraryModel
} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {
  CampaignLibraryViewState,
  provideInitialCampaignLibraryView
} from '../state/campaign-library-view.state';
import {CampaignLibraryState} from '../state/campaign-library.state';

@Injectable()
export class CampaignLibraryViewService extends EntityViewService<CampaignLibraryState, 'campaignLibrary', CampaignPermissions> {

  constructor(protected restService: RestService,
              protected referentialDataService: ReferentialDataService,
              protected attachmentService: AttachmentService,
              protected translateService: TranslateService,
              protected customFieldValueService: CustomFieldValueService,
              protected attachmentHelper: EntityViewAttachmentHelperService,
              protected customFieldHelper: EntityViewCustomFieldHelperService) {
    super(
      restService,
      referentialDataService,
      attachmentService,
      translateService,
      customFieldValueService,
      attachmentHelper,
      customFieldHelper
    );
  }

  addSimplePermissions(projectData: ProjectData): CampaignPermissions {
    return new CampaignPermissions(projectData);
  }

  getInitialState(): CampaignLibraryViewState {
    return provideInitialCampaignLibraryView();
  }

  load(id: number) {
    this.restService.get<CampaignLibraryModel>(['campaign-library-view', id.toString()])
      .subscribe((campaignLibraryModel: CampaignLibraryModel) => {
        const campaignLibrary = this.initializeCampaignLibraryState(campaignLibraryModel);
        this.initializeEntityState(campaignLibrary);
      });
  }

  private initializeCampaignLibraryState(campaignLibraryModel: CampaignLibraryModel): CampaignLibraryState {
    const attachmentEntityState = this.initializeAttachmentState(campaignLibraryModel.attachmentList.attachments);
    const customFieldValueState = this.initializeCustomFieldValueState(campaignLibraryModel.customFieldValues);
    return {
      ...campaignLibraryModel,
      attachmentList: {id: campaignLibraryModel.attachmentList.id, attachments: attachmentEntityState},
      customFieldValues: customFieldValueState
    };
  }
}
