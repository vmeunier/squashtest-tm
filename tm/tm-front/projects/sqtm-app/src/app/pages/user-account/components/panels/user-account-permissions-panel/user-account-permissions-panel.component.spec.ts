import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {UserAccountPermissionsPanelComponent} from './user-account-permissions-panel.component';
import {UserAccountService} from '../../../services/user-account.service';
import {of} from 'rxjs';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {RestService} from 'sqtm-core';
import {TranslateModule} from '@ngx-translate/core';
import {UserAccountViewState} from '../../../state/user-account.model';
import SpyObj = jasmine.SpyObj;
import createSpyObj = jasmine.createSpyObj;

describe('UserAccountPermissionsPanelComponent', () => {
  let component: UserAccountPermissionsPanelComponent;
  let fixture: ComponentFixture<UserAccountPermissionsPanelComponent>;

  const userAccountService: SpyObj<UserAccountService> = createSpyObj(['load']);
  userAccountService.componentData$ = of({
    userAccount: {
      projectPermissions: [
        {
          permissionGroup: {
            id: 1,
            qualifiedName: 'admin'
          },
          projectName: 'project name',
        }
      ]
    }
  } as UserAccountViewState);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [UserAccountPermissionsPanelComponent],
      providers: [
        {
          provide: UserAccountService,
          useValue: userAccountService,
        },
        {
          provide: RestService,
          useValue: createSpyObj(['get', 'post'])
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserAccountPermissionsPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
