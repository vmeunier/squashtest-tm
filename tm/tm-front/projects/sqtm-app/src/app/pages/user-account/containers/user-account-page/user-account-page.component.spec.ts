import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {UserAccountPageComponent} from './user-account-page.component';
import {RouterTestingModule} from '@angular/router/testing';
import {ReactiveFormsModule} from '@angular/forms';
import {AppTestingUtilsModule} from '../../../../utils/testing-utils/app-testing-utils.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {UserAccountService} from '../../services/user-account.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {EMPTY, of} from 'rxjs';
import {AdminReferentialDataService} from 'sqtm-core';
import {mockPassThroughTranslateService} from '../../../../utils/testing-utils/mocks.service';
import {TranslateService} from '@ngx-translate/core';
import SpyObj = jasmine.SpyObj;

describe('UserAccountPageComponent', () => {
  let component: UserAccountPageComponent;
  let fixture: ComponentFixture<UserAccountPageComponent>;
  const userAccountService: SpyObj<UserAccountService> = jasmine.createSpyObj(['load']);
  userAccountService.componentData$ = EMPTY;

  const adminReferentialService = jasmine.createSpyObj(['refresh']);
  adminReferentialService.refresh.and.returnValue(EMPTY);
  adminReferentialService.authenticatedUser$ = of({
    username: 'username',
    lastName: '',
    firstName: '',
    automationProgrammer: false,
    functionalTester: false,
    projectManager: true,
    userId: 1,
    admin: true,
  });

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, RouterTestingModule, ReactiveFormsModule, HttpClientTestingModule],
      declarations: [UserAccountPageComponent],
      providers: [
        {
          provide: UserAccountService,
          useValue: userAccountService,
        },
        {
          provide: AdminReferentialDataService,
          useValue: adminReferentialService,
        },
        {
          provide: TranslateService,
          useValue: mockPassThroughTranslateService(),
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(UserAccountPageComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
      });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
