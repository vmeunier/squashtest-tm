import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutomationWorkspaceTestCaseViewComponent } from './automation-workspace-test-case-view.component';

describe('AutomationWorkspaceTestCaseViewComponent', () => {
  let component: AutomationWorkspaceTestCaseViewComponent;
  let fixture: ComponentFixture<AutomationWorkspaceTestCaseViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AutomationWorkspaceTestCaseViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutomationWorkspaceTestCaseViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
