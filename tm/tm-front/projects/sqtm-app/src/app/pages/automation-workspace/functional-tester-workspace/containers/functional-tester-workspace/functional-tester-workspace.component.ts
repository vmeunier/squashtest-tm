import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import {Router} from '@angular/router';
import {ReferentialDataService} from 'sqtm-core';
import {FunctionalTesterWorkspaceService} from '../../services/functional-tester-workspace.service';
import {switchMap} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-automation-tester-workspace',
  templateUrl: './functional-tester-workspace.component.html',
  styleUrls: ['./functional-tester-workspace.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {provide: FunctionalTesterWorkspaceService, useClass: FunctionalTesterWorkspaceService}
  ]
})
export class FunctionalTesterWorkspaceComponent implements OnInit {

  constructor(private referentialDataService: ReferentialDataService,
              private automationTesterWorkspaceService: FunctionalTesterWorkspaceService) {
  }

  ngOnInit(): void {
    this.referentialDataService.refresh().pipe(
      switchMap(() => this.automationTesterWorkspaceService.load())
    ).subscribe();
  }
}
