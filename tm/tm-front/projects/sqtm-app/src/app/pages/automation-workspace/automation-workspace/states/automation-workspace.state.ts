import {User} from 'sqtm-core';

export type AutomationWorkspaceState = AutomationWorkspaceDataModel;

export class AutomationWorkspaceDataModel {
  nbTotal: number;
  nbAutomReqToTreat: number;
  nbAssignedAutomReq: number;
  usersWhoModifiedTestCasesAssignView: User[];
  usersWhoModifiedTestCasesTreatmentView: User[];
  usersWhoModifiedTestCasesGlobalView: User[];
  usersAssignedTo: User[];
  loaded: boolean;
}
