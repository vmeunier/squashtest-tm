import {AutomationRequestPermissions, ColumnDisplay, DataRow} from 'sqtm-core';

export function isAutomReqEditable(columnDisplay: ColumnDisplay, row: DataRow): boolean {
  let noLockedMilestones = true;
  if (row.data['tcMilestoneLocked'] != null) {
    noLockedMilestones = row.data['tcMilestoneLocked'] === 0;
  }
  const permissions = row.simplePermissions as AutomationRequestPermissions;
  return columnDisplay.editable && (permissions.canWrite || permissions.canWriteAsAutomation) && noLockedMilestones;
}

export function canEditRows(rows: DataRow[]) {

  return rows.filter(row => {
    let noLockedMilestones = true;
    if (row.data['tcMilestoneLocked'] != null) {
      noLockedMilestones = row.data['tcMilestoneLocked'] === 0;
    }
    const permissions = row.simplePermissions as AutomationRequestPermissions;
    return (permissions.canWrite || permissions.canWriteAsAutomation) && noLockedMilestones;
  }).length > 0;
}

export function filterEditableAutomReq(rows: DataRow[]) {
  return rows.filter(row => {
    let noLockedMilestones = true;
    if (row.data['tcMilestoneLocked'] != null) {
      noLockedMilestones = row.data['tcMilestoneLocked'] === 0;
    }
    const permissions = row.simplePermissions as AutomationRequestPermissions;
    return (permissions.canWrite || permissions.canWriteAsAutomation) && noLockedMilestones;
  });
}
