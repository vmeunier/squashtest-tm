import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FunctionalTesterTestCaseViewComponent } from './functional-tester-test-case-view.component';

describe('FunctionalTesterTestCaseViewComponent', () => {
  let component: FunctionalTesterTestCaseViewComponent;
  let fixture: ComponentFixture<FunctionalTesterTestCaseViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FunctionalTesterTestCaseViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FunctionalTesterTestCaseViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
