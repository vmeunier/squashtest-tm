import {AutomationRequestPermissions, DataRow} from 'sqtm-core';

export function isAutomReqEditable(row: DataRow): boolean {
  let noLockedMilestones = true;
  if (row.data['tcMilestoneLocked'] != null) {
    noLockedMilestones = row.data['tcMilestoneLocked'] === 0;
  }
  const permissions = row.simplePermissions as AutomationRequestPermissions;
  return permissions.canWrite && noLockedMilestones;
}
