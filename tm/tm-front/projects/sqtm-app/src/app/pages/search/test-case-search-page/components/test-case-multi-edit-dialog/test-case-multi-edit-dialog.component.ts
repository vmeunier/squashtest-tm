import {ChangeDetectionStrategy, Component, OnInit, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {
  DialogReference,
  DisplayOption,
  InfoList,
  InfoListItem,
  OptionalSelectFieldComponent,
  ProjectData,
  ProjectDataMap,
  ReferentialDataService,
  RestService,
  TestCaseAutomatable,
  TestCaseStatus,
  TestCaseWeight
} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {Observable} from 'rxjs';
import {TestCaseMultiEditConfiguration} from './test-case-multi-edit.configuration';
import {AbstractMassEditDialog} from '../../abstract-mass-edit-dialog';

@Component({
  selector: 'sqtm-app-test-case-multi-edit-dialog',
  templateUrl: './test-case-multi-edit-dialog.component.html',
  styleUrls: ['./test-case-multi-edit-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TestCaseMultiEditDialogComponent extends AbstractMassEditDialog implements OnInit {

  nature: InfoList;
  type: InfoList;
  projectDataMap$: Observable<ProjectDataMap>;
  autoDisable = true;
  importanceAutoCheck = false;

  data: TestCaseMultiEditConfiguration;

  @ViewChildren(OptionalSelectFieldComponent)
  selectFields: QueryList<OptionalSelectFieldComponent>;

  @ViewChild('importance')
  importance: OptionalSelectFieldComponent;

  constructor(translateService: TranslateService,
              referentialDataService: ReferentialDataService,
              public dialogReference: DialogReference<TestCaseMultiEditConfiguration>,
              restService: RestService) {
    super(translateService, referentialDataService, restService);
  }

  ngOnInit() {
    this.data = this.dialogReference.data;
    this.projectDataMap$ = this.referentialDataService.projectDatas$;
  }

  changeImportanceAuto($event) {
    this.importanceAutoCheck = $event;
  }

  getStatusValues() {
    return this.convertLevelEnumToDisplayOptions(TestCaseStatus);
  }

  getWeightValues() {
    return this.convertLevelEnumToDisplayOptions(TestCaseWeight);
  }

  getAutomatableValues() {
    return this.convertLevelEnumToDisplayOptions(TestCaseAutomatable);
  }

  getNatureValues(projectDataMap: ProjectDataMap): DisplayOption[] {
    const nature = this.getNature(projectDataMap);
    return this.convertInfoListItemsToDisplayOptions(nature.items);
  }

  getTypeValues(projectDataMap: ProjectDataMap): DisplayOption[] {
    const type = this.getType(projectDataMap);
    return this.convertInfoListItemsToDisplayOptions(type.items);
  }

  getDefaultType(projectDataMap: ProjectDataMap) {
    const type = this.getType(projectDataMap);
    return this.getDefaultItem(type.items);
  }

  getDefaultNature(projectDataMap: ProjectDataMap) {
    const nature = this.getNature(projectDataMap);
    return this.getDefaultItem(nature.items);
  }

  importanceAutoDisable($event) {
    return this.autoDisable = !$event;
  }

  private getType(projectDataMap: ProjectDataMap) {
    const projectsData = this.getProjectsData(projectDataMap, this.data.projectIds);
    return projectsData[0].testCaseType;
  }

  private getNature(projectDataMap: ProjectDataMap) {
    const projectsData = this.getProjectsData(projectDataMap, this.data.projectIds);
    return projectsData[0].testCaseNature;
  }

  private getDefaultItem(infoListItem: InfoListItem[]) {
    const systemItems = infoListItem.filter(item => item.system);

    let defaultValue;
    if (systemItems.length > 0) {
      defaultValue = infoListItem[0].id;
    } else {
      defaultValue = infoListItem.find(item => item.default).id;
    }
    return defaultValue;
  }

  cantEditAutomatable(projectDataMap: ProjectDataMap) {
    const projectsData = this.getProjectsData(projectDataMap, this.data.projectIds);
    if (this.getAutomatableProjectsList(projectsData)) {
      return false;
    } else {
      return true;
    }
  }

  getAutomatableProjectsList(projectData: ProjectData[]) {
    return projectData.find(project => project.allowAutomationWorkflow);
  }

  cantEditNature(projectDataMap: ProjectDataMap) {
    const projectsData = this.getProjectsData(projectDataMap, this.data.projectIds);
    const natureList = new Set(projectsData.map(projectData => projectData.testCaseNature.id));
    return natureList.size !== 1;
  }

  cantEditType(projectDataMap: ProjectDataMap) {
    const projectsData = this.getProjectsData(projectDataMap, this.data.projectIds);
    const typeList = new Set(projectsData.map(projectData => projectData.testCaseType.id));
    return typeList.size !== 1;
  }

  cantEditAllFields(projectDataMap: ProjectDataMap) {
    return this.cantEditNature(projectDataMap) && this.cantEditType(projectDataMap);
  }

  getProjectsData(projectDataMap: ProjectDataMap, ids: number[]) {
    const projectsData: ProjectData[] = [];
    ids.forEach(id => projectsData.push(projectDataMap[id]));
    return projectsData;
  }

  confirmation() {

    const checkFields = this.selectFields.filter(field => field.check);
    if (checkFields.length === 0) {
      this.dialogReference.close();
    }
    const result = {};
    checkFields.forEach(checkField => {
      if (checkField.fieldName === 'importance' && this.importanceAutoCheck) {
        result['importanceAuto'] = true;
      }

      result[checkField.fieldName] = checkField.selectedValue;
    });
    result['testCaseIds'] = this.data.testCaseIds;
    this.restService.post(['search/test-case/mass-update'], result).subscribe(
      () => {
        this.dialogReference.result = true;
        this.dialogReference.close();
      }
    );
  }

}
