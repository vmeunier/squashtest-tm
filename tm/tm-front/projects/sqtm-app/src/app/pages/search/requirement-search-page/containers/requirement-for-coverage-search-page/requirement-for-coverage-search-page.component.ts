import {AfterViewInit, ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {searchRequirementGridConfigFactory} from '../requirement-search-grid.builders';
import {
  ChangeCoverageOperationReport,
  createTestCaseCoverageMessageDialogConfiguration,
  DataRow,
  DialogService,
  GridNode,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService,
  shouldShowCoverageMessageDialog,
  TestCaseService,
  UserHistorySearchProvider
} from 'sqtm-core';
import {RequirementSearchViewService} from '../../services/requirement-search-view.service';
import {
  AbstractRequirementSearchComponent,
  REQUIREMENT_RESEARCH_GRID,
  REQUIREMENT_RESEARCH_GRID_CONFIG
} from '../abstract-requirement-search.component';
import {concatMap, map, take, takeUntil} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {BACK_URL_PARAM} from '../../../search-constants';

@Component({
  selector: 'sqtm-app-requirement-for-coverage-search-page',
  templateUrl: './requirement-for-coverage-search-page.component.html',
  styleUrls: ['./requirement-for-coverage-search-page.component.less'],
  providers: [
    {
      provide: REQUIREMENT_RESEARCH_GRID_CONFIG,
      useFactory: searchRequirementGridConfigFactory
    },
    {
      provide: REQUIREMENT_RESEARCH_GRID,
      useFactory: gridServiceFactory,
      deps: [
        RestService,
        REQUIREMENT_RESEARCH_GRID_CONFIG,
        ReferentialDataService
      ]
    }, {
      provide: GridService,
      useExisting: REQUIREMENT_RESEARCH_GRID
    },
    {
      provide: RequirementSearchViewService,
      useClass: RequirementSearchViewService,
      deps: [RestService]
    },
    {
      provide: UserHistorySearchProvider,
      useExisting: RequirementSearchViewService,
    }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequirementForCoverageSearchPageComponent
  extends AbstractRequirementSearchComponent implements OnInit, OnDestroy, AfterViewInit {

  constructor(referentialDataService: ReferentialDataService,
              requirementSearchViewService: RequirementSearchViewService,
              gridService: GridService,
              private testCaseService: TestCaseService,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private dialogService: DialogService) {
    super(referentialDataService, requirementSearchViewService, gridService);
  }

  ngOnInit() {
    super.ngOnInit();
  }

  linkSelection() {
    this.linkFromObservable(this.gridService.selectedRows$);
  }

  linkAll() {
    this.linkFromObservable(this.gridService.gridNodes$.pipe(
      map(nodes => this.getDataRows(nodes))));
  }

  private getDataRows(nodes: GridNode[]) {
    return nodes.map(node => node.dataRow);
  }

  linkFromObservable(obs: Observable<DataRow[]>) {
    obs.pipe(
      take(1),
      map(rows => this.extractRequirementIds(rows)),
      concatMap(requirementIds => this.persistNewCoverages(requirementIds))
    ).subscribe(operationReport => {
      if (shouldShowCoverageMessageDialog(operationReport)) {
        this.showCoverageReport(operationReport);
      } else {
        this.navigateBack();
      }
    });
  }

  navigateBack() {
    const backUrl =  this.activatedRoute.snapshot.queryParamMap.get(BACK_URL_PARAM) || 'test-case-workspace';
    this.router.navigate([backUrl]);
  }

  private showCoverageReport(operationReport: ChangeCoverageOperationReport) {
    this.dialogService.openDialog(createTestCaseCoverageMessageDialogConfiguration(operationReport));
  }

  private persistNewCoverages(requirementIds: number[]): Observable<ChangeCoverageOperationReport> {
    const testCaseId = this.activatedRoute.snapshot.paramMap.get('testCaseId');
    return this.testCaseService.persistCoverages(Number.parseInt(testCaseId, 10), requirementIds);
  }

  private extractRequirementIds(rows: DataRow[]) {
    return rows.map(row => row.data.requirementId);
  }


}
