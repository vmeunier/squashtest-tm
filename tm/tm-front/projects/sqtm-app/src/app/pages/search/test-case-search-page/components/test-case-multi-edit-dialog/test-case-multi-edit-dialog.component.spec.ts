import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {TestCaseMultiEditDialogComponent} from './test-case-multi-edit-dialog.component';
import {EMPTY} from 'rxjs';
import {DialogReference, ReferentialDataService, RestService} from 'sqtm-core';
import {TranslateModule} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('TestCaseMultiEditDialogComponent', () => {
  let component: TestCaseMultiEditDialogComponent;
  let fixture: ComponentFixture<TestCaseMultiEditDialogComponent>;
  const referentialDataServiceMock = jasmine.createSpyObj('referentialDataService', ['refresh', 'connectToProjectData']);
  const overlayReference = jasmine.createSpyObj(['attachments']);

  overlayReference.attachments.and.returnValue(EMPTY);

  const dialogReference: DialogReference = new DialogReference(
    'edit',
    null,
    overlayReference,
    {titleKey: 'titleKey', level: 'INFO', id: 'edit', messageKey: 'message'},
  );

  const restService = {};
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [TestCaseMultiEditDialogComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: DialogReference, useValue: dialogReference
        },
        {
          provide: ReferentialDataService,
          useValue: referentialDataServiceMock
        },
        {provide: RestService, useValue: restService}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestCaseMultiEditDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
