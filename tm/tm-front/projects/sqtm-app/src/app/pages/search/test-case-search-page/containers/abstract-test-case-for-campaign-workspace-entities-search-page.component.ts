import {AbstractTestCaseSearchComponent} from './abstract-test-case-search.component';
import {AfterViewInit, Directive, OnDestroy, OnInit} from '@angular/core';
import {DataRow, GridNode, GridService, ReferentialDataService} from 'sqtm-core';
import {TestCaseSearchViewService} from '../services/test-case-search-view.service';
import {ActivatedRoute, Router} from '@angular/router';
import {concatMap, map, take} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {BACK_URL_PARAM} from '../../search-constants';

@Directive()
// tslint:disable-next-line:directive-class-suffix
export abstract class AbstractTestCaseForCampaignWorkspaceEntitiesSearchPageComponent extends AbstractTestCaseSearchComponent
  implements OnInit, OnDestroy, AfterViewInit {

  protected constructor(referentialDataService: ReferentialDataService,
                        testCaseSearchViewService: TestCaseSearchViewService,
                        gridService: GridService,
                        protected activatedRoute: ActivatedRoute,
                        protected router: Router) {
    super(referentialDataService, testCaseSearchViewService, gridService);
  }

  ngOnInit() {
    super.ngOnInit();
  }

  linkSelection() {
    this.linkFromObservable(this.gridService.selectedRows$);
  }

  linkAll() {
    this.linkFromObservable(this.gridService.gridNodes$.pipe(
      map((nodes: GridNode[]) => this.getDataRows(nodes))));
  }

  protected getDataRows(nodes: GridNode[]) {
    return nodes.map(node => node.dataRow);
  }

  linkFromObservable(obs: Observable<DataRow[]>) {
    obs.pipe(
      take(1),
      map(rows => this.extractTestCaseIds(rows)),
      concatMap(testCaseIds => this.persistTestCasesInEntity(testCaseIds))
    ).subscribe(() => this.navigateBack());
  }

  protected extractTestCaseIds(rows: DataRow[]) {
    return rows.map(row => row.data.id);
  }

  navigateBack() {
    const backUrl = this.activatedRoute.snapshot.queryParamMap.get(BACK_URL_PARAM) || 'campaign-workspace';
    this.router.navigate([backUrl]);
  }

  protected abstract persistTestCasesInEntity(testCaseIds: number []);
}
