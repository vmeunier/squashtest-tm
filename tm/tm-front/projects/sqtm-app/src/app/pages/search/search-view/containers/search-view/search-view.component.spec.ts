import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {SearchViewComponent} from './search-view.component';
import {EMPTY, of} from 'rxjs';
import {GridService} from 'sqtm-core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('SearchViewComponent', () => {
  let component: SearchViewComponent;
  let fixture: ComponentFixture<SearchViewComponent>;
  const tableMock = jasmine.createSpyObj('tableMock', ['load', 'setColumnSorts', 'addFilters', 'inactivateFilter', 'activateFilter']);
  tableMock.selectedRows$ = EMPTY;
  tableMock.openedRowIds$ = EMPTY;
  tableMock.sortedColumns$ = EMPTY;
  tableMock.loaded$ = of(false);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, HttpClientTestingModule, RouterTestingModule],
      declarations: [SearchViewComponent],
      providers: [
        {
          provide: GridService,
          useValue: tableMock
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
