import {AfterViewInit, Directive, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FilterGroup, GridFilter, GridService, ReferentialDataService} from 'sqtm-core';
import {SearchViewComponent} from '../../search-view/containers/search-view/search-view.component';
import {Observable, Subject} from 'rxjs';
import {concatMap, pluck, takeUntil} from 'rxjs/operators';
import {TestCaseSearchViewService} from '../services/test-case-search-view.service';
import {TestCaseSearchGridBuilders} from './test-case-search-grid.builders';


@Directive()
// tslint:disable-next-line:directive-class-suffix
export abstract class AbstractTestCaseSearchComponent implements OnInit, OnDestroy, AfterViewInit {
  filters: GridFilter[];

  filterGroups: FilterGroup[];

  @ViewChild(SearchViewComponent)
  protected searchViewComponent: SearchViewComponent;

  protected unsub$ = new Subject<void>();

  hasSelectedRows$: Observable<boolean>;

  protected constructor(protected referentialDataService: ReferentialDataService,
                        protected testCaseSearchViewService: TestCaseSearchViewService,
                        protected gridService: GridService) {
    this.observeMilestoneFeatureEnabled();
  }

  protected observeMilestoneFeatureEnabled(): void {
    this.referentialDataService.globalConfiguration$.pipe(
      takeUntil(this.unsub$),
      pluck('milestoneFeatureEnabled')
    ).subscribe((milestoneFeatureEnabled) => {
      this.setMilestoneCountColumnVisibility(milestoneFeatureEnabled);
      this.buildFilters(milestoneFeatureEnabled);
    });
  }

  private setMilestoneCountColumnVisibility(milestoneFeatureEnabled: boolean): void {
    this.gridService.setColumnVisibility('milestones', milestoneFeatureEnabled);
  }

  protected buildFilters(milestoneFeatureEnabled: boolean): void {
    const builders = this.getFilterBuilders();
    this.filters = builders.buildFilters();
    this.filterGroups = builders.buildFilterGroups();

    if (!milestoneFeatureEnabled) {
      this.filterGroups = this.filterGroups.filter(group => group.id !== 'milestones');
    }
  }

  protected getFilterBuilders() {
    return new TestCaseSearchGridBuilders();
  }

  ngOnInit(): void {
    this.hasSelectedRows$ = this.gridService.hasSelectedRows$.pipe(
      takeUntil(this.unsub$)
    );
  }

  ngAfterViewInit(): void {
    this.referentialDataService.refresh().pipe(
      concatMap(() => this.testCaseSearchViewService.loadResearchData())
    ).subscribe(() => this.searchViewComponent.initializeResearch());
  }

  ngOnDestroy(): void {
    this.gridService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

}
