import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {TestCaseEditableTextRendererComponent} from './test-case-editable-text-renderer.component';
import {DialogService, GridTestingModule} from 'sqtm-core';
import {TranslateModule} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';

describe('TestCaseEditableTextRendererComponent', () => {
  let component: TestCaseEditableTextRendererComponent;
  let fixture: ComponentFixture<TestCaseEditableTextRendererComponent>;
  const dialogService = jasmine.createSpyObj('dialogService', ['create']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, GridTestingModule, TranslateModule.forRoot()],
      declarations: [TestCaseEditableTextRendererComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {provide: DialogService, useValue: dialogService}
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestCaseEditableTextRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
