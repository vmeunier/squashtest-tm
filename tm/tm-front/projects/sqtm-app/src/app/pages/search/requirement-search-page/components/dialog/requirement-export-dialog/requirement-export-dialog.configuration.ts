import {RequirementCurrentVersionFilterKeys} from 'sqtm-core';

export class RequirementExportDialogConfiguration {
  id: string;
  titleKey: string;
  requirementIds: number[];
  requirementVersionFilter: RequirementCurrentVersionFilterKeys;
}
