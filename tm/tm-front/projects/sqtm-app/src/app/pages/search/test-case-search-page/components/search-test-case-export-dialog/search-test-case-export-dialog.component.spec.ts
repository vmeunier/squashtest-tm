import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {SearchTestCaseExportDialogComponent} from './search-test-case-export-dialog.component';
import {EMPTY} from 'rxjs';
import {DialogReference} from 'sqtm-core';
import {SearchTestCaseExportDialogConfiguration} from './search-test-case-export-dialog-configuration';
import {TranslateModule} from '@ngx-translate/core';
import {ReactiveFormsModule} from '@angular/forms';
import {DatePipe} from '@angular/common';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('SearchTestCaseExportDialogComponent', () => {
  let component: SearchTestCaseExportDialogComponent;
  let fixture: ComponentFixture<SearchTestCaseExportDialogComponent>;
  const overlayReference = jasmine.createSpyObj(['attachments']);

  overlayReference.attachments.and.returnValue(EMPTY);

  const dialogReference: DialogReference = new DialogReference<SearchTestCaseExportDialogConfiguration>(
    'edit',
    null,
    overlayReference,
    {id: '', nodes: []},
  );
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot(), AppTestingUtilsModule, ReactiveFormsModule, HttpClientTestingModule],
      declarations: [SearchTestCaseExportDialogComponent],
      providers: [
        {provide: DialogReference, useValue: dialogReference}, DatePipe
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchTestCaseExportDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
