import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RequirementEditableCellRendererComponent } from './requirement-editable-cell-renderer.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {DialogService, GridTestingModule} from 'sqtm-core';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';
import {TranslateModule} from '@ngx-translate/core';

describe('RequirementEditableCellRendererComponent', () => {
  let component: RequirementEditableCellRendererComponent;
  let fixture: ComponentFixture<RequirementEditableCellRendererComponent>;
  const dialogService = jasmine.createSpyObj('dialogService', ['create']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, GridTestingModule, TranslateModule.forRoot()],
      declarations: [ RequirementEditableCellRendererComponent ],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {provide: DialogService, useValue: dialogService}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequirementEditableCellRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
