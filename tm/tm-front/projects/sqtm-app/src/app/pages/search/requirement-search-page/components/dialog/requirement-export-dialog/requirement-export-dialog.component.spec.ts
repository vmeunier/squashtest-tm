import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {RequirementExportDialogComponent} from './requirement-export-dialog.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {DialogReference} from 'sqtm-core';
import {TranslateModule} from '@ngx-translate/core';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {DatePipe} from '@angular/common';
import {EMPTY} from 'rxjs';
import {RequirementExportDialogConfiguration} from './requirement-export-dialog.configuration';

describe('RequirementExportDialogComponent', () => {
  let component: RequirementExportDialogComponent;
  let fixture: ComponentFixture<RequirementExportDialogComponent>;
  const overlayReference = jasmine.createSpyObj(['attachments']);

  overlayReference.attachments.and.returnValue(EMPTY);

  const dialogReference: DialogReference = new DialogReference<RequirementExportDialogConfiguration>(
    'edit',
    null,
    overlayReference,
    {id: '', requirementVersionFilter: 'ALL', requirementIds: [], titleKey: ''},
  );
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot(), AppTestingUtilsModule, ReactiveFormsModule, HttpClientTestingModule],
      declarations: [RequirementExportDialogComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {provide: DialogReference, useValue: dialogReference}, DatePipe
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequirementExportDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
