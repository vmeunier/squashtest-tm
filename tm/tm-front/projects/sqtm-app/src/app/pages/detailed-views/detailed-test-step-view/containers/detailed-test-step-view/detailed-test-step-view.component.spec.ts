import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import {DetailedTestStepViewComponent} from './detailed-test-step-view.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {OverlayModule} from '@angular/cdk/overlay';
import {TranslateModule} from '@ngx-translate/core';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('DetailedTestStepViewComponent', () => {
  let component: DetailedTestStepViewComponent;
  let fixture: ComponentFixture<DetailedTestStepViewComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        AppTestingUtilsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        OverlayModule,
        TranslateModule.forRoot(),
      ],
      declarations: [ DetailedTestStepViewComponent ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailedTestStepViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
