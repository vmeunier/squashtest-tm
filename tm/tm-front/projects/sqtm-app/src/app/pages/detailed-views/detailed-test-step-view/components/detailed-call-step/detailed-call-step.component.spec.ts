import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {DetailedCallStepComponent} from './detailed-call-step.component';
import {DetailedTestStepViewService} from '../../services/detailed-test-step-view.service';
import {TranslateModule} from '@ngx-translate/core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {OverlayModule} from '@angular/cdk/overlay';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('DetailedCallStepComponent', () => {
  let component: DetailedCallStepComponent;
  let fixture: ComponentFixture<DetailedCallStepComponent>;

  const serviceMock = jasmine.createSpyObj(['changeCurrentStepIndex']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot(), HttpClientTestingModule, AppTestingUtilsModule, OverlayModule],
      declarations: [DetailedCallStepComponent],
      providers: [
        {
          provide: DetailedTestStepViewService,
          useValue: serviceMock
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailedCallStepComponent);
    component = fixture.componentInstance;
    component.callTestStep = {calledTcId: 12} as any;
    component.testCase = {} as any;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
