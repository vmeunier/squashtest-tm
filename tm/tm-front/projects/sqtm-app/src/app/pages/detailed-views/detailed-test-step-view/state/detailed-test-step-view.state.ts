import {
  entitySelector,
  EntityViewState,
  Milestone,
  provideInitialViewState,
  SqtmEntityState,
  TestCaseKindKeys
} from 'sqtm-core';
import {TestStepsState, TestStepState} from '../../../test-case-workspace/test-case-view/state/test-step.state';
import {
  coverageEntitySelectors,
  CoverageState
} from '../../../test-case-workspace/test-case-view/state/requirement-version-coverage.state';
import {TestCaseState, TestCaseViewUiState} from '../../../test-case-workspace/test-case-view/state/test-case.state';
import {createSelector} from '@ngrx/store';
import {ParameterState} from '../../../test-case-workspace/test-case-view/state/parameter.state';
import {DataSetState} from '../../../test-case-workspace/test-case-view/state/dataset.state';
import {DatasetParamValueState} from '../../../test-case-workspace/test-case-view/state/dataset-param-value.state';

export interface DetailedTestStepViewState extends EntityViewState<DetailedTestCaseState, 'testCase'> {
  testCase: DetailedTestCaseState;
}

export function provideInitialDetailedTestStepView(): Readonly<DetailedTestStepViewState> {
  return provideInitialViewState<DetailedTestCaseState, 'testCase'>('testCase');
}

export interface DetailedTestCaseState extends SqtmEntityState {
  name: string;
  reference: string;
  kind: TestCaseKindKeys,
  testSteps: TestStepsState;
  coverages: CoverageState;
  milestones: Milestone[];
  uiState: TestCaseViewUiState;
  lastModifiedOn: string;
  lastModifiedBy: string;
  createdOn: string;
  createdBy: string;
  currentStepIndex: number;
  parameters: ParameterState;
  datasets: DataSetState;
  datasetParamValues: DatasetParamValueState;
}

export const testStepStateSelector = createSelector(entitySelector, (testCase: DetailedTestCaseState) => {
  return testCase.testSteps;
});

export const currentStepIndexSelector = createSelector(entitySelector, (testCase: DetailedTestCaseState) => {
  return testCase.currentStepIndex;
});

export const currentTestStepStateSelector =
  createSelector(testStepStateSelector, currentStepIndexSelector, (testStepSate, currentStepIndex) => {
    const id = testStepSate.ids[currentStepIndex];
    return testStepSate.entities[id];
  });

export const coverageStateSelector = createSelector(entitySelector, (testCase: TestCaseState) => {
  return testCase.coverages;
});

const coveragesSelector = createSelector(coverageStateSelector, coverageEntitySelectors.selectAll);
const directlyVerifiedCoveragesSelector = createSelector(coveragesSelector, (coverages) => coverages.filter(cov => cov.directlyVerified));

export const coveragesWithCurrentStepSelector = createSelector(directlyVerifiedCoveragesSelector, currentTestStepStateSelector,
  (coverages, testStepState: TestStepState) => {
    if (testStepState) {
      return coverages.map(coverage => {
        const linkedToStep = coverage.coverageStepInfos.filter(stepInfo => stepInfo.id === testStepState.id).length > 0;
        return {...coverage, linkedToStep};
      });
    } else {
      return coverages;
    }
});
