import {Injectable} from '@angular/core';
import {createStore, InterWindowCommunicationService, InterWindowMessages, ModifDuringExecService} from 'sqtm-core';
import {
  executionStepPairAdapter,
  ModifDuringExecState,
  provideInitialModifDuringExecState,
  selectCurrentActionStepId
} from '../states/modif-during-exec.state';
import {concatMap, filter, map, take, withLatestFrom} from 'rxjs/operators';
import {select} from '@ngrx/store';
import {MODIF_DURING_EXEC_STEP_VIEW_URL} from '../modif-during-exec-step.constant';
import {Router} from '@angular/router';
import {
  BackToExecutionMessagePayload,
  ExecutionRunnerOpenerService
} from '../../../execution/execution-runner/services/execution-runner-opener.service';

@Injectable()
export class ModifDuringExecViewService {

  private store = createStore<ModifDuringExecState>(provideInitialModifDuringExecState());

  componentData$ = this.store.state$.pipe(
    filter(state => Boolean(state.currentExecutionStepId) && state.executionStepActionTestStepPairs.ids.length > 0)
  );

  currentActionStepId$ = this.componentData$.pipe(
    select(selectCurrentActionStepId)
  );

  constructor(private modifDuringExecService: ModifDuringExecService,
              private router: Router,
              private interWindowCommunicationService: InterWindowCommunicationService,
              private executionRunnerOpenerService: ExecutionRunnerOpenerService) {
  }

  load(executionId: number) {
    this.modifDuringExecService.getModifDuringExecModel(executionId).pipe(
      withLatestFrom(this.store.state$),
      map(([model, state]) => {
        const executionStepActionTestStepPairs =
          executionStepPairAdapter.setAll(model.executionStepActionTestStepPairs, state.executionStepActionTestStepPairs);
        return {...state, executionStepActionTestStepPairs, executionId};
      })
    ).subscribe(state => this.store.commit(state));
  }

  complete() {
    this.store.complete();
  }

  notifyCurrentExecutionStep(executionStepId: number) {
    this.store.state$.pipe(
      take(1),
      map((state: ModifDuringExecState) => {
        const initialStepId = state.initialStepId || executionStepId;
        return {...state, currentExecutionStepId: executionStepId, initialStepId};
      })
    ).subscribe(state => this.store.commit(state));
  }

  navigateToStep(index: number) {
    this.store.state$.pipe(
      take(1),
    ).subscribe(state => {
      const executionStepId = state.executionStepActionTestStepPairs.ids[index];
      const url = `${MODIF_DURING_EXEC_STEP_VIEW_URL}/execution/${state.executionId}/step/${executionStepId}`;
      this.router.navigate([url]);
    });
  }

  navigateBackToExecution() {
    this.store.state$.pipe(
      take(1),
      concatMap(state => this.modifDuringExecService.updateExecutionStepsAfterModif(state.executionId)),
      withLatestFrom(this.store.state$)
    ).subscribe(([index, state]) => {
      this.openExecutionRunner(index, state);
    });
  }

  private openExecutionRunner(index: number, state: ModifDuringExecState) {
    if (index >= 0) {
      this.navigateToModifiedStep(state, index);
    } else {
      const ids = state.executionStepActionTestStepPairs.ids as number[];
      const originalStepIndex = ids.includes(state.initialStepId) ? ids.indexOf(state.initialStepId) : 0;
      this.navigateToModifiedStep(state, originalStepIndex);
    }
  }

  private navigateToModifiedStep(state: ModifDuringExecState, index: number) {
    if (this.interWindowCommunicationService.isAbleToSendMessagesToOpener()) {
      const payload: BackToExecutionMessagePayload = {
        executionId: state.executionId,
        executionStepIndex: index + 1 // + 1 because server begin count at 0
      };
      const message = new InterWindowMessages('MODIFICATION-DURING-EXECUTION-STEP', payload);
      this.interWindowCommunicationService.sendMessage(message);
    } else {
      this.executionRunnerOpenerService.openExecutionAtStep(state.executionId, index + 1);
    }
    window.close();
  }
}
