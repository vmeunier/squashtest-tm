import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {ExecutionStepComponent} from './execution-step.component';
import {TranslateModule} from '@ngx-translate/core';
import {ExecutionPageService} from '../../services/execution-page.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {CampaignPermissions, WorkspaceCommonModule} from 'sqtm-core';
import {NzDropDownModule} from 'ng-zorro-antd/dropdown';
import {ExecutionRunnerService} from '../../../execution-runner/services/execution-runner.service';
import SpyObj = jasmine.SpyObj;
import {ExecutionState, ExecutionStepState} from '../../../states/execution-state';

describe('ExecutionStepComponent', () => {
  let component: ExecutionStepComponent;
  let fixture: ComponentFixture<ExecutionStepComponent>;
  const executionPageServiceMock: SpyObj<ExecutionRunnerService> = jasmine.createSpyObj(['load']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ExecutionStepComponent],
      imports: [AppTestingUtilsModule, WorkspaceCommonModule, NzDropDownModule, TranslateModule.forRoot()],
      providers: [
        {
          provide: ExecutionPageService,
          useValue: executionPageServiceMock
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutionStepComponent);
    component = fixture.componentInstance;
    component.step = {
      executionStatus: 'FAILURE',
      lastExecutedOn: null,
      lastExecutedBy: null
    } as ExecutionStepState;
    component.permissions = {
      canExecute: true,
      canAttach: true,
      canDelete: true,
    } as CampaignPermissions;
    component.issues = [];
    component.execution = {
      kind: 'STANDARD'
    } as ExecutionState;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
