import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExecutionStepIssuesComponent } from './execution-step-issues.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {ExecutionRunnerService} from '../../services/execution-runner.service';
import {EMPTY} from 'rxjs';
import {DialogService, GridService, RestService} from 'sqtm-core';
import {mockDialogService, mockGridService, mockRestService} from '../../../../../utils/testing-utils/mocks.service';
import {ExecutionState, ExecutionStepState} from '../../../states/execution-state';

describe('ExecutionStepIssuesComponent', () => {
  let component: ExecutionStepIssuesComponent;
  let fixture: ComponentFixture<ExecutionStepIssuesComponent>;

  const executionRunnerService = {
    componentData$: EMPTY,
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExecutionStepIssuesComponent ],
      providers: [
        { provide: ExecutionRunnerService, useValue: executionRunnerService },
        { provide: GridService, useValue: mockGridService() },
        { provide: RestService, useValue: mockRestService() },
        { provide: DialogService, useValue: mockDialogService() },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutionStepIssuesComponent);
    component = fixture.componentInstance;
    component.execution = {
      currentStepIndex: 0,
      executionSteps: {
        ids: [1],
        entities: {
          1: {
            id: 1,
          } as ExecutionStepState,
        }
      }
    } as unknown as ExecutionState;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
