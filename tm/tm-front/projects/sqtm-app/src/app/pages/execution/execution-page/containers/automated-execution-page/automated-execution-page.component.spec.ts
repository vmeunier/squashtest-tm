import { ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import { AutomatedExecutionPageComponent } from './automated-execution-page.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {RouterTestingModule} from '@angular/router/testing';
import {TranslateService} from '@ngx-translate/core';
import {ExecutionPageService} from '../../services/execution-page.service';
import {EMPTY} from 'rxjs';
import {DatePipe} from '@angular/common';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {DialogService} from 'sqtm-core';

describe('AutomatedExecutionPageComponent', () => {
  let component: AutomatedExecutionPageComponent;
  let fixture: ComponentFixture<AutomatedExecutionPageComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule, RouterTestingModule],
      declarations: [ AutomatedExecutionPageComponent ],
      providers: [
        {
          provide: TranslateService,
          useValue: jasmine.createSpyObj(['instant']),
        },
        {
          provide: DialogService,
          useValue: jasmine.createSpyObj(['openDeletionConfirm', 'openAlert'])
        },
        {
          provide: ExecutionPageService,
          useValue: {
            componentData$: EMPTY,
            loaded$: EMPTY
          }
        },
        DatePipe
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutomatedExecutionPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
