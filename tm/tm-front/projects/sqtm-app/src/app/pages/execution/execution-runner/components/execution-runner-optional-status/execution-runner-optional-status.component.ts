import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ExecutionStatus, ExecutionStatusKeys} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-execution-runner-optional-status',
  templateUrl: './execution-runner-optional-status.component.html',
  styleUrls: ['./execution-runner-optional-status.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExecutionRunnerOptionalStatusComponent implements OnInit {

  executionStatus = ExecutionStatus;

  @Input()
  disabledExecutionStatus: ExecutionStatusKeys[] = [];

  @Output()
  changeStatus = new EventEmitter<ExecutionStatusKeys>();

  constructor() {
  }

  ngOnInit() {
  }

  handleChangeStatus(id: ExecutionStatusKeys) {
    this.changeStatus.next(id);
  }

  isVisible(statusKey: ExecutionStatusKeys) {
    return !this.disabledExecutionStatus.includes(statusKey);
  }
}
