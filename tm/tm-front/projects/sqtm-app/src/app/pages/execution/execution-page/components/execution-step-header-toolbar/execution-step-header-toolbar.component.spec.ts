import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExecutionStepHeaderToolbarComponent } from './execution-step-header-toolbar.component';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {AttachmentState, CampaignPermissions, WorkspaceCommonModule} from 'sqtm-core';
import {NzDropDownModule} from 'ng-zorro-antd/dropdown';
import {TranslateModule} from '@ngx-translate/core';
import {ExecutionPageService} from '../../services/execution-page.service';
import {DatePipe} from '@angular/common';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {ExecutionRunnerService} from '../../../execution-runner/services/execution-runner.service';
import SpyObj = jasmine.SpyObj;
import {ExecutionStepState} from '../../../states/execution-state';

describe('ExecutionStepHeaderToolbarComponent', () => {
  let component: ExecutionStepHeaderToolbarComponent;
  let fixture: ComponentFixture<ExecutionStepHeaderToolbarComponent>;
  const executionPageServiceMock: SpyObj<ExecutionRunnerService> = jasmine.createSpyObj(['load']);


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExecutionStepHeaderToolbarComponent ],
      imports: [AppTestingUtilsModule, WorkspaceCommonModule, NzDropDownModule, TranslateModule.forRoot()],
      providers: [
        {
          provide: ExecutionPageService,
          useValue: executionPageServiceMock
        },
        DatePipe
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutionStepHeaderToolbarComponent);
    component = fixture.componentInstance;
    component.step = {
      executionStatus: 'FAILURE',
      lastExecutedOn: null,
      lastExecutedBy: null,
      attachmentList: {
        id: -1,
        attachments: {
          ids: []
        } as AttachmentState
      }
    } as ExecutionStepState;
    component.permissions = {
      canExecute: true,
      canAttach: true,
      canDelete: true,
    } as CampaignPermissions;
    component.issuesNb = 0;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
