import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {
  EXECUTION_ISSUE_TABLE,
  EXECUTION_ISSUE_TABLE_CONF,
  executionIssuesTableDefinition,
  ExecutionPageIssuesPanelComponent
} from './execution-page-issues-panel.component';
import {TranslateModule} from '@ngx-translate/core';
import {ExecutionPageService} from '../../../services/execution-page.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {EMPTY} from 'rxjs';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';
import {DialogService, gridServiceFactory, ReferentialDataService, RestService} from 'sqtm-core';

describe('ExecutionPageIssuesPanelComponent', () => {
  let component: ExecutionPageIssuesPanelComponent;
  let fixture: ComponentFixture<ExecutionPageIssuesPanelComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ExecutionPageIssuesPanelComponent ],
      imports: [HttpClientTestingModule, AppTestingUtilsModule, TranslateModule.forRoot()],
      providers: [
        {
          provide: ExecutionPageService,
          useValue: EMPTY
        },
        {
          provide: DialogService,
          useValue: {},
        },
        {
          provide: EXECUTION_ISSUE_TABLE_CONF,
          useFactory: executionIssuesTableDefinition
        },
        {
          provide: EXECUTION_ISSUE_TABLE,
          useFactory: gridServiceFactory,
          deps: [RestService, EXECUTION_ISSUE_TABLE_CONF, ReferentialDataService]
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutionPageIssuesPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
