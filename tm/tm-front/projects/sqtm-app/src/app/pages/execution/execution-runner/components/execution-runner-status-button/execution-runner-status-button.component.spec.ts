import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {ExecutionRunnerStatusButtonComponent} from './execution-runner-status-button.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';

describe('ExecutionRunnerStatusButtonComponent', () => {
  let component: ExecutionRunnerStatusButtonComponent;
  let fixture: ComponentFixture<ExecutionRunnerStatusButtonComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule],
      declarations: [ExecutionRunnerStatusButtonComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutionRunnerStatusButtonComponent);
    component = fixture.componentInstance;
    component.status = 'SUCCESS';
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
