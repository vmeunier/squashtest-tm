import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {ExecutionPrologueBodyComponent} from './execution-prologue-body.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {CampaignPermissions, WorkspaceCommonModule} from 'sqtm-core';
import {ExecutionRunnerService} from '../../services/execution-runner.service';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import SpyObj = jasmine.SpyObj;
import {ExecutionState} from '../../../states/execution-state';

describe('ExecutionPrologueBodyComponent', () => {
  let component: ExecutionPrologueBodyComponent;
  let fixture: ComponentFixture<ExecutionPrologueBodyComponent>;

  const executionRunnerServiceMock: SpyObj<ExecutionRunnerService> = jasmine.createSpyObj(['load']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot(), AppTestingUtilsModule, WorkspaceCommonModule],
      providers: [
        {
          provide: ExecutionRunnerService,
          useValue: executionRunnerServiceMock
        }
      ],
      declarations: [ExecutionPrologueBodyComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutionPrologueBodyComponent);
    component = fixture.componentInstance;
    const executionState: ExecutionState = {
      id: 12,
      executionSteps: {ids: [], entities: {}},
      attachmentList: {id: 1, attachments: {ids: [], entities: {}}}
      // }
    } as unknown as ExecutionState;
    component.execution = executionState;
    component.customFieldData = [];
    component.permissions = allCampaignPermissions;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

const allCampaignPermissions: CampaignPermissions = {
  kind: 'CAMPAIGN_LIBRARY',
  canRead: true,
  canWrite: true,
  canAttach: true,
  canCreate: true,
  canDelete: true,
  canLink: true,
  canExecute: true,
  canExtendedDelete: true,
  canImport: true,
  permissions: [],
} as unknown as CampaignPermissions;
