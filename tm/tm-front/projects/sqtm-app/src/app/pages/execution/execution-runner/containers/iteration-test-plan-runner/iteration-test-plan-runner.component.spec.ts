import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IterationTestPlanRunnerComponent } from './iteration-test-plan-runner.component';
import {ExecutionRunnerNavigationService} from '../../services/execution-runner-navigation.service';
import {SingleExecutionRunnerNavigationService} from '../../services/single-execution-runner-navigation.service';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {RouterTestingModule} from '@angular/router/testing';
import {OverlayModule} from '@angular/cdk/overlay';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('TestPlanRunnerComponent', () => {
  let component: IterationTestPlanRunnerComponent;
  let fixture: ComponentFixture<IterationTestPlanRunnerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, RouterTestingModule, OverlayModule, HttpClientTestingModule],
      declarations: [ IterationTestPlanRunnerComponent ],
      providers: [
        SingleExecutionRunnerNavigationService,
        {
          provide: ExecutionRunnerNavigationService,
          useClass: SingleExecutionRunnerNavigationService
        }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IterationTestPlanRunnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
