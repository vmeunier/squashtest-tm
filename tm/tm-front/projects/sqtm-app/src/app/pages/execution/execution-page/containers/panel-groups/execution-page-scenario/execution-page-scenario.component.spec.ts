import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import { ExecutionPageScenarioComponent } from './execution-page-scenario.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';
import {ExecutionPageService} from '../../../services/execution-page.service';
import {EMPTY} from 'rxjs';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('ExecutionPageScenarioComponent', () => {
  let component: ExecutionPageScenarioComponent;
  let fixture: ComponentFixture<ExecutionPageScenarioComponent>;

  beforeEach(waitForAsync( () => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule],
      declarations: [ ExecutionPageScenarioComponent ],
      providers: [
        {
          provide: ExecutionPageService,
          useValue: {
            componentData$: EMPTY
          }
        },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutionPageScenarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
