import {sqtmAppLogger} from '../../../app-logger';

export const executionPageLogger = sqtmAppLogger.compose('execution-page');
