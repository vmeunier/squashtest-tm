import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleExecutionRunnerComponent } from './single-execution-runner.component';

describe('SingleExecutionRunnerComponent', () => {
  let component: SingleExecutionRunnerComponent;
  let fixture: ComponentFixture<SingleExecutionRunnerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SingleExecutionRunnerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleExecutionRunnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
