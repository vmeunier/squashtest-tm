import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ExecutionPageInformationPanelComponent } from './execution-page-information-panel.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {ExecutionPageService} from '../../../services/execution-page.service';
import {ExecutionRunnerService} from '../../../../execution-runner/services/execution-runner.service';
import SpyObj = jasmine.SpyObj;
import {ExecutionPageComponentData} from '../../../containers/abstract-execution-page.component';

describe('ExecutionPageInformationPanelComponent', () => {
  let component: ExecutionPageInformationPanelComponent;
  let fixture: ComponentFixture<ExecutionPageInformationPanelComponent>;

  const executionPageServiceMock: SpyObj<ExecutionRunnerService> = jasmine.createSpyObj(['load']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ExecutionPageInformationPanelComponent ],
      imports: [TranslateModule.forRoot()],
      providers: [
        {
          provide: ExecutionPageService,
          useValue: executionPageServiceMock
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutionPageInformationPanelComponent);
    component = fixture.componentInstance;
    component.executionPageComponentData = {
      execution: {
        id: 1
      }
    } as ExecutionPageComponentData;
    component.customFieldData = [];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

