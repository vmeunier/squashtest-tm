import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {ExecutionRunnerComponent} from './execution-runner.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {ExecutionRunnerService} from '../../services/execution-runner.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {TranslateModule} from '@ngx-translate/core';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';

const mock = jasmine.createSpyObj<ExecutionRunnerService>('ExecutionRunnerService', ['load']);

describe('ExecutionRunnerComponent', () => {
  let component: ExecutionRunnerComponent;
  let fixture: ComponentFixture<ExecutionRunnerComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule, AppTestingUtilsModule, TranslateModule.forRoot()],
      declarations: [ExecutionRunnerComponent],
      providers: [
        {
          provide: ExecutionRunnerService,
          useValue: mock
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutionRunnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
