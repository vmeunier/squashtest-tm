import {Component, OnInit, ChangeDetectionStrategy, Input} from '@angular/core';
import {ExecutionPageComponentData} from '../../../containers/abstract-execution-page.component';

@Component({
  selector: 'sqtm-app-execution-page-comments-panel',
  templateUrl: './execution-page-comments-panel.component.html',
  styleUrls: ['./execution-page-comments-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExecutionPageCommentsPanelComponent implements OnInit {

  @Input()
  executionPageComponentData: ExecutionPageComponentData;

  constructor() { }

  ngOnInit(): void {
  }

}
