import {Component, ChangeDetectionStrategy, ChangeDetectorRef} from '@angular/core';
import {Subject} from 'rxjs';
import {AbstractDeleteCellRenderer, DialogService, GridService} from 'sqtm-core';
import {ExecutionPageService} from '../../../services/execution-page.service';
import {finalize} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-remove-execution-issue-cell',
  template: `
    <sqtm-core-delete-icon
      [iconName]="getIcon()"
      (delete)="showDeleteConfirm()"></sqtm-core-delete-icon>
  `,
  styleUrls: ['./remove-execution-issue-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RemoveExecutionIssueCellComponent extends AbstractDeleteCellRenderer {

  unsub$ = new Subject<void>();

  constructor(public grid: GridService,
              cdr: ChangeDetectorRef,
              dialogService: DialogService,
              private executionPageService: ExecutionPageService) {
    super(grid, cdr, dialogService);
  }

  getIcon(): string {
    return 'sqtm-core-generic:unlink';
  }

  get isExecutionStep(): boolean {
    return this.row.data['stepOrder'] !== '';
  }

  protected doDelete(): void {
    this.grid.beginAsyncOperation();
    const issueId = this.row.data.issueId;
    this.executionPageService.removeIssues([issueId]).pipe(
      finalize(() => this.grid.completeAsyncOperation())
    ).subscribe(() => this.grid.refreshData());
  }

  protected getTitleKey(): string {
    return 'sqtm-core.campaign-workspace.execution-page.dialog.title.remove-issue.delete-one';
  }

  protected getMessageKey(): string {
    return this.isExecutionStep ?
      'sqtm-core.campaign-workspace.execution-page.dialog.message.remove-issue.delete-one-from-step'
      : 'sqtm-core.campaign-workspace.execution-page.dialog.message.remove-issue.delete-one';
  }
}
