import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RemoveExecutionStepIssueCellComponent } from './remove-execution-step-issue-cell.component';
import {DialogService, GridService, RestService} from 'sqtm-core';
import {mockDialogService, mockGridService, mockRestService} from '../../../../../utils/testing-utils/mocks.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('RemoveExecutionStepIssueCellComponent', () => {
  let component: RemoveExecutionStepIssueCellComponent;
  let fixture: ComponentFixture<RemoveExecutionStepIssueCellComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RemoveExecutionStepIssueCellComponent ],
      providers: [
        { provide: GridService, useValue: mockGridService() },
        { provide: RestService, useValue: mockRestService() },
        { provide: DialogService, useValue: mockDialogService() },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoveExecutionStepIssueCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
