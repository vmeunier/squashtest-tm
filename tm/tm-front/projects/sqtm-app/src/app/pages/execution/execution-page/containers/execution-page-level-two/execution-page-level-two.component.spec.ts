import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {ExecutionPageLevelTwoComponent} from './execution-page-level-two.component';
import {TranslateModule} from '@ngx-translate/core';
import {ActivatedRoute, convertToParamMap} from '@angular/router';
import {EMPTY, of} from 'rxjs';
import {GenericEntityViewService, ReferentialDataService, RestService} from 'sqtm-core';
import {mockRestService} from '../../../../../utils/testing-utils/mocks.service';
import {ExecutionPageService} from '../../services/execution-page.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import createSpyObj = jasmine.createSpyObj;

describe('ExecutionPageLevelTwoComponent', () => {
  let component: ExecutionPageLevelTwoComponent;
  let fixture: ComponentFixture<ExecutionPageLevelTwoComponent>;
  const referentialDataMock = createSpyObj(['refresh']);
  referentialDataMock.refresh.and.returnValue(EMPTY);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [ExecutionPageLevelTwoComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: of(convertToParamMap({
              userId: '123',
            })),
          }
        },
        {
          provide: ReferentialDataService,
          useValue: referentialDataMock,
        },
        {
          provide: GenericEntityViewService,
          useValue: {},
        },
        {
          provide: ExecutionPageService,
          useValue: {},
        },
        {
          provide: RestService,
          useValue: mockRestService(),
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutionPageLevelTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
