import {ComponentFixture, TestBed} from '@angular/core/testing';

import {CoverageExternalLinkComponent} from './coverage-external-link.component';
import {GridTestingModule} from 'sqtm-core';
import {AppTestingUtilsModule} from '../../../../utils/testing-utils/app-testing-utils.module';
import {RouterTestingModule} from '@angular/router/testing';
import {WindowOpenerService} from '../../../../services/window-opener.service';

describe('CoverageExternalLinkComponent', () => {
  let component: CoverageExternalLinkComponent;
  let fixture: ComponentFixture<CoverageExternalLinkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [GridTestingModule, AppTestingUtilsModule, RouterTestingModule],
      providers: [WindowOpenerService],
      declarations: [CoverageExternalLinkComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CoverageExternalLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
