import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ExecutionPageCoveragePanelComponent } from './execution-page-coverage-panel.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('ExecutionPageCoveragePanelComponent', () => {
  let component: ExecutionPageCoveragePanelComponent;
  let fixture: ComponentFixture<ExecutionPageCoveragePanelComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ExecutionPageCoveragePanelComponent ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutionPageCoveragePanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
