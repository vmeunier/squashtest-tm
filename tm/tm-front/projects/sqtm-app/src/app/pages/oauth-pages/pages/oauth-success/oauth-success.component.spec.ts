import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OAuthSuccessComponent } from './oauth-success.component';
import {AppTestingUtilsModule} from '../../../../utils/testing-utils/app-testing-utils.module';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('OAuthSuccessComponent', () => {
  let component: OAuthSuccessComponent;
  let fixture: ComponentFixture<OAuthSuccessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule],
      declarations: [ OAuthSuccessComponent ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OAuthSuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
