import {AuthenticationProtocol, Credentials, SqtmGenericEntityState} from 'sqtm-core';

export interface AdminTestAutomationServerState extends SqtmGenericEntityState {
  id: number;
  name: string;
  baseUrl: string;
  kind: string;
  description: string;
  createdBy: string;
  createdOn: string;
  lastModifiedBy: string;
  lastModifiedOn: string;
  manualSlaveSelection: boolean;
  credentials: Credentials;
  supportedAuthenticationProtocols: AuthenticationProtocol[];
  authProtocol: AuthenticationProtocol;
}
