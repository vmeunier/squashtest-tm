import {GenericEntityViewState, provideInitialGenericViewState} from 'sqtm-core';
import {AdminMilestoneState} from './admin-milestone-state';

export interface AdminMilestoneViewState extends GenericEntityViewState<AdminMilestoneState, 'milestone'> {
  milestone: AdminMilestoneState;
}

export function provideInitialAdminMilestoneView(): Readonly<AdminMilestoneViewState> {
  return provideInitialGenericViewState<AdminMilestoneState, 'milestone'>('milestone');
}
