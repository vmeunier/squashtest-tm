import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ScmServerViewComponent } from './scm-server-view.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import {TranslateService} from '@ngx-translate/core';
import {of} from 'rxjs';
import {ActivatedRoute, convertToParamMap} from '@angular/router';
import {DialogService, GridService} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {ScmServerViewService} from '../../services/scm-server-view.service';

describe('ScmServerViewComponent', () => {
  let component: ScmServerViewComponent;
  let fixture: ComponentFixture<ScmServerViewComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule, AppTestingUtilsModule, NzDropDownModule],
      providers: [
        {
          provide: TranslateService,
          useValue: jasmine.createSpyObj(['instant']),
        },
        {
          provide: ScmServerViewService,
          useValue: {
            componentData$: of({})
          }
        },
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({id: 123}),
            paramMap: of(convertToParamMap({
              toto: 'tutu',
            })),
          }
        },
        {
          provide: DialogService,
          useValue: jasmine.createSpyObj(['openDeletionConfirm', 'openAlert'])
        },
        {
          provide: GridService,
          useValue: jasmine.createSpyObj(['refreshData'])
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [ ScmServerViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScmServerViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
