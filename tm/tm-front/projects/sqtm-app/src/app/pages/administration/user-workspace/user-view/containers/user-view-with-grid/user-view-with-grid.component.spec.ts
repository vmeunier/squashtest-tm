import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { UserViewWithGridComponent } from './user-view-with-grid.component';
import {ActivatedRoute, convertToParamMap} from '@angular/router';
import {of} from 'rxjs';
import {GridService, RestService} from 'sqtm-core';
import {mockGridService, mockRestService} from '../../../../../../utils/testing-utils/mocks.service';
import {TranslateService} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('UserViewWithGridComponent', () => {
  let component: UserViewWithGridComponent;
  let fixture: ComponentFixture<UserViewWithGridComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ UserViewWithGridComponent ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: of(convertToParamMap({
              teamId: '123',
            })),
          }
        },
        {
          provide: GridService,
          useValue: mockGridService(),
        },
        {
          provide: RestService,
          useValue: mockRestService(),
        },
        {
          provide: TranslateService,
          useValue: {}
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserViewWithGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
