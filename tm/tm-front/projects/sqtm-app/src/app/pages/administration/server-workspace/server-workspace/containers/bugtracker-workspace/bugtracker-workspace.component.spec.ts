import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BugtrackerWorkspaceComponent } from './bugtracker-workspace.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';
import {GridService} from 'sqtm-core';

describe('BugtrackerWorkspaceComponent', () => {
  let component: BugtrackerWorkspaceComponent;
  let fixture: ComponentFixture<BugtrackerWorkspaceComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BugtrackerWorkspaceComponent ],
      imports: [HttpClientTestingModule, AppTestingUtilsModule, RouterTestingModule],
      providers: [
        {
          provide: GridService,
          useValue: jasmine.createSpyObj(['complete']),
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BugtrackerWorkspaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
