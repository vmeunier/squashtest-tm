import {ChangeDetectionStrategy, Component, OnInit, QueryList, ViewChildren} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DialogReference, passwordConfirmationValidator, passwordLengthValidator, TextFieldComponent} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-reset-password-dialog',
  templateUrl: './reset-password-dialog.component.html',
  styleUrls: ['./reset-password-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResetPasswordDialogComponent implements OnInit {

  formGroup: FormGroup;

  @ViewChildren(TextFieldComponent)
  textFields: QueryList<TextFieldComponent>;

  constructor(public readonly dialogReference: DialogReference,
              public readonly fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.formGroup = this.fb.group({
      password: this.fb.control('', [Validators.required]),
      confirmPassword: this.fb.control('', [Validators.required])
    }, {
      validators: [
        passwordLengthValidator('password'),
        passwordConfirmationValidator('password', 'confirmPassword'),
      ]
    });
  }

  confirm() {
    if (this.formIsValid()) {
      this.dialogReference.result = this.formGroup.controls['password'].value;
      this.dialogReference.close();
    } else {
      this.showClientSideErrors();
    }
  }

  private formIsValid() {
    return this.formGroup.status === 'VALID';
  }

  private showClientSideErrors() {
    this.textFields.forEach(textField => textField.showClientSideError());
  }

}
