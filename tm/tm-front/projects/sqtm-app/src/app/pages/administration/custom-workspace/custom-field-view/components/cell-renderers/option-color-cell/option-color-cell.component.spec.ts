import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OptionColorCellComponent } from './option-color-cell.component';
import {grid, GridDefinition, GridService, gridServiceFactory, ReferentialDataService, RestService} from 'sqtm-core';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {CustomFieldViewService} from '../../../services/custom-field-view.service';

describe('OptionColorCellComponent', () => {
  let component: OptionColorCellComponent;
  let fixture: ComponentFixture<OptionColorCellComponent>;
  const gridConfig = grid('grid-test').build();

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ OptionColorCellComponent ],
      imports: [AppTestingUtilsModule,  HttpClientTestingModule],
      providers: [
      {
        provide: GridDefinition,
        useValue: gridConfig
      },
      {
        provide: GridService,
        useFactory: gridServiceFactory,
        deps: [RestService, GridDefinition, ReferentialDataService]
      },
      {
        provide: CustomFieldViewService,
        useValue: {}
      },
    ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OptionColorCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
