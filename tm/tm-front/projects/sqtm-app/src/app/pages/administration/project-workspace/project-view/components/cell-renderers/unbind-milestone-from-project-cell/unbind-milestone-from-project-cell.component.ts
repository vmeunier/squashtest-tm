import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {AbstractDeleteCellRenderer, DialogService, GridService, RestService} from 'sqtm-core';
import {Observable} from 'rxjs';
import {AdminProjectViewComponentData} from '../../../containers/project-view/project-view.component';
import {ProjectViewService} from '../../../services/project-view.service';
import {finalize} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-unbind-milestone-from-project-cell',
  template: `
    <sqtm-core-delete-icon [iconName]="getIcon()" (delete)="showDeleteConfirm()"></sqtm-core-delete-icon>
  `,
  styleUrls: ['./unbind-milestone-from-project-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UnbindMilestoneFromProjectCellComponent extends AbstractDeleteCellRenderer {
  private componentData$: Observable<AdminProjectViewComponentData>;


  constructor(public grid: GridService, cdr: ChangeDetectorRef,
              dialogService: DialogService,
              private restService: RestService,
              private projectViewService: ProjectViewService) {
    super(grid, cdr, dialogService);
    this.componentData$ = projectViewService.componentData$;
  }

  getIcon(): string {
    return 'sqtm-core-generic:unlink';
  }

  protected doDelete(): any {
    this.grid.beginAsyncOperation();
    const boundMilestonesId = this.row.data.id;
    this.projectViewService.unbindMilestones([boundMilestonesId]).pipe(
      finalize(() => this.grid.completeAsyncOperation())
    ).subscribe();
  }

  protected getTitleKey(): string {
    return 'sqtm-core.administration-workspace.projects.dialog.title.unbind-milestone-from-project.unbind-one';
  }

  protected getMessageKey(): string {
    return 'sqtm-core.administration-workspace.projects.dialog.message.unbind-milestone-from-project.unbind-one';
  }
}
