import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {ProjectCustomComponent} from './project-custom.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {DialogService} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {ProjectViewService} from '../../../services/project-view.service';
import {EMPTY} from 'rxjs';

describe('ProjectCustomComponent', () => {
  let component: ProjectCustomComponent;
  let fixture: ComponentFixture<ProjectCustomComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule],
      providers: [
        {
          provide: TranslateService,
          useValue: jasmine.createSpyObj('TranslateService', ['instant']),
        },
        {
          provide: DialogService,
          useValue: {},
        },
        {
          provide: ProjectViewService,
          useValue: {
            componentData$: EMPTY,
          }
        }
      ],
      declarations: [ProjectCustomComponent],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectCustomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
