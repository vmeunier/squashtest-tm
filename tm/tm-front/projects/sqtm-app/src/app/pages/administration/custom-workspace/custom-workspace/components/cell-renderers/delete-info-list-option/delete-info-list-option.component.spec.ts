import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DeleteInfoListOptionComponent } from './delete-info-list-option.component';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {OverlayModule} from '@angular/cdk/overlay';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {grid, GridDefinition, GridService, gridServiceFactory, ReferentialDataService, RestService} from 'sqtm-core';
import {DropdownListOptionService} from '../../../services/dropdown-list-option.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('DeleteInfoListOptionCellRendererComponent', () => {
  let component: DeleteInfoListOptionComponent;
  let fixture: ComponentFixture<DeleteInfoListOptionComponent>;
  const gridConfig = grid('grid-test').build();

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteInfoListOptionComponent ],
      imports: [AppTestingUtilsModule, OverlayModule,  HttpClientTestingModule],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig
        }, {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        },
        {
          provide: DropdownListOptionService,
          useValue: {}
        },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteInfoListOptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
