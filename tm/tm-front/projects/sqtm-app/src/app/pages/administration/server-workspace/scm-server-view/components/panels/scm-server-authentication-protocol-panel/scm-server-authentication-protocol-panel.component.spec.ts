import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ScmServerAuthenticationProtocolPanelComponent } from './scm-server-authentication-protocol-panel.component';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('ScmServerAuthenticationProtocolPanelComponent', () => {
  let component: ScmServerAuthenticationProtocolPanelComponent;
  let fixture: ComponentFixture<ScmServerAuthenticationProtocolPanelComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ScmServerAuthenticationProtocolPanelComponent ],
      imports: [AppTestingUtilsModule, FormsModule, ReactiveFormsModule],
      providers: [
      {
        provide: TranslateService,
        useValue: jasmine.createSpyObj(['instant']),
      },
    ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScmServerAuthenticationProtocolPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
