import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SystemMilestoneDeactivationPanelComponent } from './system-milestone-deactivation-panel.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {OverlayModule} from '@angular/cdk/overlay';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {DialogModule, GridTestingModule} from 'sqtm-core';

describe('SystemMilestoneDeactivationPanelComponent', () => {
  let component: SystemMilestoneDeactivationPanelComponent;
  let fixture: ComponentFixture<SystemMilestoneDeactivationPanelComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SystemMilestoneDeactivationPanelComponent ],
      imports: [AppTestingUtilsModule, HttpClientTestingModule, GridTestingModule, DialogModule, OverlayModule],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemMilestoneDeactivationPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
