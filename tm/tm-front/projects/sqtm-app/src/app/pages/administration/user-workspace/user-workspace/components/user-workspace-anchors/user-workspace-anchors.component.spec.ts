import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {UserWorkspaceAnchorsComponent} from './user-workspace-anchors.component';
import {RouterTestingModule} from '@angular/router/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';
import {GridService, WorkspaceWithGridComponent} from 'sqtm-core';

describe('UserWorkspaceAnchorsComponent', () => {
  let component: UserWorkspaceAnchorsComponent;
  let fixture: ComponentFixture<UserWorkspaceAnchorsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [UserWorkspaceAnchorsComponent],
      imports: [RouterTestingModule, AppTestingUtilsModule],
      providers: [
        {
          provide: WorkspaceWithGridComponent,
          useValue: {},
        },
        {
          provide: GridService,
          useValue: {}
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserWorkspaceAnchorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
