import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {AbstractCellRendererComponent, ColumnDefinitionBuilder, GridService} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-custom-field-bound-entity-cell',
  template: `
    <ng-container *ngIf="columnDisplay && row">
      <div class="full-width full-height flex-column">
        <span style="margin: auto 0;" nz-tooltip [sqtmCoreLabelTooltip]="cellText | translate" [ellipsis]="true">
          {{cellText | translate}}
        </span>
      </div>
    </ng-container>`,
  styleUrls: ['./custom-field-bound-entity-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CustomFieldBoundEntityCellComponent extends AbstractCellRendererComponent {

  constructor(public grid: GridService, public cdRef: ChangeDetectorRef) {
    super(grid, cdRef);
  }

  get cellText(): string {
    return 'sqtm-core.entity.custom-field.bound-entity.' + this.row.data[this.columnDisplay.id];
  }
}

export function customFieldBoundEntityColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(CustomFieldBoundEntityCellComponent)
    .withI18nKey('sqtm-core.entity.custom-field.bound-entity.label');
}
