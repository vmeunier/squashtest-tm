import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { EditableWorkingBranchColumnComponent } from './editable-working-branch-column.component';
import {OverlayModule} from '@angular/cdk/overlay';
import {
  DialogService,
  grid,
  GridDefinition,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService,
} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {TranslateModule} from '@ngx-translate/core';
import {ScmServerViewService} from '../../../services/scm-server-view.service';


describe('EditableWorkingBranchColumnComponent', () => {
  let component: EditableWorkingBranchColumnComponent;
  let fixture: ComponentFixture<EditableWorkingBranchColumnComponent>;

  const gridConfig = grid('grid-test').build();
  const restService = {};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ EditableWorkingBranchColumnComponent ],
      imports: [AppTestingUtilsModule, TranslateModule.forRoot(), OverlayModule],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig
        }, {
          provide: RestService,
          useValue: restService
        }, {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        }, {
          provide: DialogService,
          userValue: {}
        }, {
          provide: ScmServerViewService,
          useValue: {},
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditableWorkingBranchColumnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
