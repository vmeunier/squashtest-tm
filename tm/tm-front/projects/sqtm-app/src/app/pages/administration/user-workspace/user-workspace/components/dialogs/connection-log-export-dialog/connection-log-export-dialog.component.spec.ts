import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnectionLogExportDialogComponent } from './connection-log-export-dialog.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {DialogReference, RestService} from 'sqtm-core';
import {mockPassThroughTranslateService, mockRestService} from '../../../../../../../utils/testing-utils/mocks.service';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {TranslateService} from '@ngx-translate/core';

describe('ConnectionLogExportDialogComponent', () => {
  let component: ConnectionLogExportDialogComponent;
  let fixture: ComponentFixture<ConnectionLogExportDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, ReactiveFormsModule],
      declarations: [ ConnectionLogExportDialogComponent ],
      providers: [
        {provide: RestService, useValue: mockRestService()},
        {provide: DialogReference, useValue: jasmine.createSpyObj<DialogReference>(['close'])},
        {provide: TranslateService, useValue: mockPassThroughTranslateService()},
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectionLogExportDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
