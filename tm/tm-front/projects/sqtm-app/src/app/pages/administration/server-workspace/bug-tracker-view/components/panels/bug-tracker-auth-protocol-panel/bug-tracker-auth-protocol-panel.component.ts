import {ChangeDetectionStrategy, Component, Input, ViewChild} from '@angular/core';
import {AuthConfiguration, AuthenticationProtocol} from 'sqtm-core';
import {AdminBugTrackerViewState} from '../../../states/admin-bug-tracker-view-state';
import {BugTrackerViewService} from '../../../services/bug-tracker-view.service';
import {TranslateService} from '@ngx-translate/core';
import {AuthProtocolFormComponent} from '../../../../components/auth-protocol-form/auth-protocol-form.component';

@Component({
  selector: 'sqtm-app-bug-tracker-authentication-protocol-panel',
  templateUrl: './bug-tracker-auth-protocol-panel.component.html',
  styleUrls: ['./bug-tracker-auth-protocol-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BugTrackerAuthProtocolPanelComponent {

  @Input()
  componentData: AdminBugTrackerViewState;

  @ViewChild(AuthProtocolFormComponent)
  authProtocolForm: AuthProtocolFormComponent;

  constructor(public readonly bugTrackerViewService: BugTrackerViewService,
              public readonly translateService: TranslateService) {
  }

  handleConfigurationChange(authConf: AuthConfiguration): void {
    this.bugTrackerViewService.setAuthenticationConfiguration(authConf).subscribe(
      () => this.authProtocolForm.handleServerSuccess(),
      (err) => {
        console.error(err);
        this.authProtocolForm.handleServerError();
      }
    );
  }

  handleProtocolChange(protocol: AuthenticationProtocol): void {
    this.bugTrackerViewService.setAuthenticationProtocol(protocol)
      .subscribe(() => this.authProtocolForm.authProtocol = protocol);
  }
}
