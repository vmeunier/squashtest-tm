import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {adminConnectionLogTableDefinition, ConnectionLogGridComponent} from './connection-log-grid.component';
import {DialogService, gridServiceFactory, GridTestingModule, ReferentialDataService, RestService} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {ADMIN_WS_CONNECTION_LOG_TABLE, ADMIN_WS_CONNECTION_LOG_TABLE_CONFIG} from '../../../user-workspace.constant';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';
import {mockDialogService} from '../../../../../../utils/testing-utils/mocks.service';

describe('ConnectionLogGridComponent', () => {
  let component: ConnectionLogGridComponent;
  let fixture: ComponentFixture<ConnectionLogGridComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ConnectionLogGridComponent],
      imports: [GridTestingModule, AppTestingUtilsModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: ADMIN_WS_CONNECTION_LOG_TABLE_CONFIG,
          useFactory: adminConnectionLogTableDefinition,
        },
        {
          provide: ADMIN_WS_CONNECTION_LOG_TABLE,
          useFactory: gridServiceFactory,
          deps: [RestService, ADMIN_WS_CONNECTION_LOG_TABLE_CONFIG, ReferentialDataService]
        },
        {
          provide: DialogService,
          useValue: mockDialogService(),
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectionLogGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
