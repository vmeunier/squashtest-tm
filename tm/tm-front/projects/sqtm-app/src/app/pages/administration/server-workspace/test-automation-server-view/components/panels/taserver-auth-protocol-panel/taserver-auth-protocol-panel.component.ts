import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {AdminTestAutomationServerViewComponentData} from '../../../containers/test-automation-server-view/test-automation-server-view.component';
import {AuthConfiguration} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-taserver-auth-protocol-panel',
  templateUrl: './taserver-auth-protocol-panel.component.html',
  styleUrls: ['./taserver-auth-protocol-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TAServerAuthProtocolPanelComponent implements OnInit {

  @Input()
  componentData: AdminTestAutomationServerViewComponentData;

  // authConfiguration is only used for OAuth protocol. No TAServer use OAuth credentials so far
  authConfiguration: AuthConfiguration = null;

  constructor() {
  }

  ngOnInit(): void {
  }

}
