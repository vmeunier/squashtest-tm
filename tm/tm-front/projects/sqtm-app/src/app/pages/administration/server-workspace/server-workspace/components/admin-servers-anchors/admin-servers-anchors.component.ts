import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {WorkspaceWithGridComponent} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-administration-servers-anchors',
  templateUrl: './admin-servers-anchors.component.html',
  styleUrls: ['./admin-servers-anchors.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminServersAnchorsComponent implements OnInit {

  constructor(private workspaceWithGrid: WorkspaceWithGridComponent) {
  }

  ngOnInit(): void {
  }

  hideContextualContent() {
    this.workspaceWithGrid.switchToNoRowLayout();
  }
}
