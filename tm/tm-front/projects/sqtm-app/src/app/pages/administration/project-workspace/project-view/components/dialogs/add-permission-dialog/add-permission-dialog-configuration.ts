export interface AddPermissionDialogConfiguration {
  permissionScope: 'users' | 'teams';
  i18nPartyTypeLabel: string;
  i18nPlaceholder: string;
}
