import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {ConfigurePluginDialogComponent} from './configure-plugin-dialog.component';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {DialogReference} from 'sqtm-core';
import {OverlayModule} from '@angular/cdk/overlay';

describe('ConfigurePluginDialogComponent', () => {
  let component: ConfigurePluginDialogComponent;
  let fixture: ComponentFixture<ConfigurePluginDialogComponent>;
  const dialogReference = {data: {}};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, OverlayModule],
      declarations: [ConfigurePluginDialogComponent],
      providers: [
        {
          provide: DialogReference,
          useValue: dialogReference
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigurePluginDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
