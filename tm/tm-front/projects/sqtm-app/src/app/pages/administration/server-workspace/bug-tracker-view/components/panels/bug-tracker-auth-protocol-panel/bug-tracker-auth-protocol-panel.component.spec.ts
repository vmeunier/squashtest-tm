import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {BugTrackerAuthProtocolPanelComponent} from './bug-tracker-auth-protocol-panel.component';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {BugTrackerViewService} from '../../../services/bug-tracker-view.service';
import {AdminBugTrackerState, AuthConfiguration, AuthenticationProtocol, GenericEntityViewService, RestService} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AdminBugTrackerViewState} from '../../../states/admin-bug-tracker-view-state';
import {of, throwError} from 'rxjs';
import {mockRestService} from '../../../../../../../utils/testing-utils/mocks.service';
import {mockAuthProtocolForm} from '../../../../../../../utils/testing-utils/test-component-generator';
import SpyObj = jasmine.SpyObj;

describe('BugTrackerAuthenticationProtocolPanelComponent', () => {
  let component: BugTrackerAuthProtocolPanelComponent;
  let fixture: ComponentFixture<BugTrackerAuthProtocolPanelComponent>;
  const viewService: SpyObj<BugTrackerViewService> = jasmine.createSpyObj([
    'setAuthenticationConfiguration',
    'setAuthenticationProtocol'
  ]);
  const restService = mockRestService();

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, FormsModule, ReactiveFormsModule],
      declarations: [BugTrackerAuthProtocolPanelComponent],
      providers: [
        {
          provide: BugTrackerViewService,
          useValue: viewService,
        },
        {
          provide: GenericEntityViewService,
          useValue: viewService,
        },
        {
          provide: TranslateService,
          useValue: jasmine.createSpyObj(['instant']),
        },
        {
          provide: RestService,
          useValue: restService,
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BugTrackerAuthProtocolPanelComponent);
    component = fixture.componentInstance;
    component.componentData = makeComponentData(makeBugTracker());
    fixture.detectChanges();
    component.componentData = makeComponentData(makeBugTracker());
    component.authProtocolForm = mockAuthProtocolForm();

    viewService.setAuthenticationProtocol.and.returnValue(of({}));
    viewService.setAuthenticationProtocol.calls.reset();
    viewService.setAuthenticationConfiguration.and.returnValue(of({}));
    viewService.setAuthenticationConfiguration.calls.reset();
  });

  it('should create', waitForAsync(() => {
    expect(component).toBeTruthy();
  }));

  it('should handle protocol change', waitForAsync(() => {
    component.handleProtocolChange(AuthenticationProtocol.OAUTH_1A);
    expect(viewService.setAuthenticationProtocol).toHaveBeenCalledWith(AuthenticationProtocol.OAUTH_1A);
  }));

  it('should handle configuration change', waitForAsync(() => {
    const newConf = {} as AuthConfiguration;
    component.handleConfigurationChange(newConf);
    expect(viewService.setAuthenticationConfiguration).toHaveBeenCalledWith(newConf);
  }));

  it('should handle configuration change errors', waitForAsync(() => {
    viewService.setAuthenticationConfiguration.and.returnValue(throwError({}));

    const newConf = {} as AuthConfiguration;
    component.handleConfigurationChange(newConf);
    expect(component.authProtocolForm.handleServerError).toHaveBeenCalled();
  }));
});

function makeComponentData(bugTracker: AdminBugTrackerState): AdminBugTrackerViewState {
  return {
    bugTracker,
  } as AdminBugTrackerViewState;
}

function makeBugTracker(): AdminBugTrackerState {
  return {
    id: 1,
    kind: 'bugTracker',
    name: 'name',
    url: 'http://url.com',
    authPolicy: null,
    authProtocol: AuthenticationProtocol.OAUTH_1A,
    iframeFriendly: false,
    bugTrackerKinds: [],
    authConfiguration: null,
    credentials: null,
    supportedAuthenticationProtocols: [AuthenticationProtocol.OAUTH_1A, AuthenticationProtocol.BASIC_AUTH],
    attachmentList: {id: null, attachments: null},
  };
}
