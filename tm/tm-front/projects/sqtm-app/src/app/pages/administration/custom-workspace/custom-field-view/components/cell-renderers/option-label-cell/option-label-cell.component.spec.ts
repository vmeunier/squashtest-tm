import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OptionLabelCellComponent } from './option-label-cell.component';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {
  DialogService,
  grid,
  GridDefinition,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService
} from 'sqtm-core';
import {CustomFieldViewService} from '../../../services/custom-field-view.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('OptionLabelCellComponent', () => {
  let component: OptionLabelCellComponent;
  let fixture: ComponentFixture<OptionLabelCellComponent>;
  const gridConfig = grid('grid-test').build();

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ OptionLabelCellComponent ],
      imports: [AppTestingUtilsModule,  HttpClientTestingModule],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig
        },
        {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        },
        {
          provide: CustomFieldViewService,
          useValue: {}
        },
        {
          provide: DialogService,
          useValue: {}
        },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OptionLabelCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
