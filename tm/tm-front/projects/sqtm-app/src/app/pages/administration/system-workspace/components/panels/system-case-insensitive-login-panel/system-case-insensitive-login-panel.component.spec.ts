import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SystemCaseInsensitiveLoginPanelComponent } from './system-case-insensitive-login-panel.component';
import {SystemViewService} from '../../../services/system-view.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {EMPTY} from 'rxjs';

describe('SystemCaseInsensitiveLoginPanelComponent', () => {
  let component: SystemCaseInsensitiveLoginPanelComponent;
  let fixture: ComponentFixture<SystemCaseInsensitiveLoginPanelComponent>;

  const systemViewService = jasmine.createSpyObj(['load']);
  systemViewService['componentData$'] = EMPTY;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SystemCaseInsensitiveLoginPanelComponent ],
      providers: [
        {
          provide: SystemViewService,
          useValue: systemViewService,
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemCaseInsensitiveLoginPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
