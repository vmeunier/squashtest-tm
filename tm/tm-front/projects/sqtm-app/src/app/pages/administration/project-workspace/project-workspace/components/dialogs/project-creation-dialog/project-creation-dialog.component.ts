import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {CreationDialogData, DialogReference, DisplayOption, FieldValidationError, RestService} from 'sqtm-core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {AbstractAdministrationCreationDialogDirective} from '../../../../../components/abstract-administration-creation-dialog';
import {of} from 'rxjs';

@Component({
  selector: 'sqtm-app-project-creation-dialog',
  templateUrl: './project-creation-dialog.component.html',
  styleUrls: ['./project-creation-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectCreationDialogComponent extends AbstractAdministrationCreationDialogDirective implements OnInit {

  formGroup: FormGroup;
  serverSideValidationErrors: FieldValidationError[] = [];
  data: CreationDialogData;
  templateOptions: DisplayOption[];
  private readonly noTemplateOptionId = '';

  constructor(private fb: FormBuilder,
              private translateService: TranslateService,
              dialogReference: DialogReference,
              restService: RestService,
              cdr: ChangeDetectorRef) {
    super('projects/new', dialogReference, restService, cdr);
    this.data = this.dialogReference.data;
  }

  get textFieldToFocus(): string {
    return 'name';
  }

  get hasTemplateSelected(): boolean {
    return this.formGroup.controls['template'].value !== this.noTemplateOptionId;
  }

  ngOnInit() {
    this.initializeFormGroup();
    this.initializeTemplateSelectField();

    // Disable fields that depend on 'keepTemplateBinding'
    this.handleKeepTemplateBindingChange(true);
  }


  protected getRequestPayload() {
    return of({
      name: this.getFormControlValue('name'),
      description: this.getFormControlValue('description'),
      label: this.getFormControlValue('label'),
      keepTemplateBinding: this.getFormControlValue('keepTemplateBinding'),
      copyPermissions: this.getFormControlValue('keepPermissions'),
      copyCUF: this.getFormControlValue('keepCustomFields'),
      copyInfolists: this.getFormControlValue('keepInfoLists'),
      copyBugtrackerBinding: this.getFormControlValue('keepBugtracker'),
      copyAutomatedProjects: this.getFormControlValue('keepAutomation'),
      copyMilestone: this.getFormControlValue('keepMilestones'),
      copyAllowTcModifFromExec: this.getFormControlValue('keepAllowTcModificationsFromExecution'),
      copyOptionalExecStatuses: this.getFormControlValue('keepOptionalExecutionStatuses'),
      copyPlugins: this.getFormControlValue('keepPlugins'),
      fromTemplate: this.getFormControlValue('template') !== this.noTemplateOptionId,
      templateId: this.getFormControlValue('template'),
    });
  }

  protected doResetForm(): void {
    this.resetFormControl('name', '');
    this.resetFormControl('description', '');
    this.resetFormControl('label', '');
    this.resetFormControl('template', this.noTemplateOptionId);
    this.resetFormControl('keepTemplateBinding', true);
    this.resetFormControl('keepPermissions', true);
    this.resetFormControl('keepCustomFields', true);
    this.resetFormControl('keepInfoLists', true);
    this.resetFormControl('keepBugtracker', true);
    this.resetFormControl('keepAutomation', true);
    this.resetFormControl('keepMilestones', true);
    this.resetFormControl('keepAllowTcModificationsFromExecution', true);
    this.resetFormControl('keepOptionalExecutionStatuses', true);
    this.resetFormControl('keepPlugins', true);

    // Disable fields that depend on 'keepTemplateBinding'
    this.handleKeepTemplateBindingChange(true);
  }

  public handleKeepTemplateBindingChange(checked: boolean) {
    const fields = [
      'keepCustomFields',
      'keepInfoLists',
      'keepAllowTcModificationsFromExecution',
      'keepOptionalExecutionStatuses',
    ];

    if (checked) {
      fields.forEach(field => {
        this.formGroup.get(field).setValue(true);
        this.formGroup.get(field).disable();
      });
    } else {
      fields.forEach(field => this.formGroup.get(field).enable());
    }
  }

  private initializeFormGroup() {
    this.formGroup = this.fb.group({
      name: this.fb.control('', [
        Validators.required,
        Validators.pattern('(.|\\s)*\\S(.|\\s)*'),
        Validators.maxLength(255)
      ]),
      description: this.fb.control(''),
      label: this.fb.control('', [Validators.maxLength(255)]),
      template: this.fb.control(this.noTemplateOptionId),
      keepTemplateBinding: this.fb.control(true),
      keepPermissions: this.fb.control(true),
      keepCustomFields: this.fb.control(true),
      keepInfoLists: this.fb.control(true),
      keepBugtracker: this.fb.control(true),
      keepAutomation: this.fb.control(true),
      keepMilestones: this.fb.control(true),
      keepAllowTcModificationsFromExecution: this.fb.control(true),
      keepOptionalExecutionStatuses: this.fb.control(true),
      keepPlugins: this.fb.control(true),
    });
  }

  private initializeTemplateSelectField() {
    this.restService.get<{ templates: { id: string, name: string }[] }>(['generic-projects/templates']).subscribe((response) => {
      const options = retrieveTemplatesAsDisplayOptions(response.templates);
      const defaultOption: DisplayOption = this.retrieveDefaultOption();
      this.templateOptions = [defaultOption, ...options];

      if (options.length > 0) {
        this.formGroup.controls['template'].enable();
      }

      this.cdr.detectChanges();
    });
  }

  private retrieveDefaultOption() {
    return {
      id: this.noTemplateOptionId,
      label: this.translateService.instant('sqtm-core.administration-workspace.projects.dialog.message.new-project.no-template'),
    };
  }
}

function retrieveTemplatesAsDisplayOptions(templates: { id: string; name: string }[]) {
  return templates.map((ref) => ({ id: ref.id, label: ref.name }));
}
