import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InfoListContentComponent } from './info-list-content.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {DialogModule, GridService} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {InfoListViewService} from '../../../services/info-list-view.service';
import {EMPTY} from 'rxjs';

describe('InfoListContentComponent', () => {
  let component: InfoListContentComponent;
  let fixture: ComponentFixture<InfoListContentComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoListContentComponent ],
      imports: [HttpClientTestingModule, AppTestingUtilsModule, DialogModule],
      providers: [
        {
          provide: TranslateService,
          useValue: jasmine.createSpyObj('TranslateService', ['instant']),
        },
        {
          provide: GridService,
          useValue: jasmine.createSpyObj(['refreshData'])
        },
        {
          provide: InfoListViewService,
          useValue: {
            componentData$: EMPTY
          }
        },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoListContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
