import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {
  SCM_SERVER_REPOSITORIES_TABLE,
  SCM_SERVER_REPOSITORIES_TABLE_CONF,
  ScmServerRepositoriesPanelComponent,
  scmServerRepositoriesTableDefinition
} from './scm-server-repositories-panel.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {DialogService, gridServiceFactory, ReferentialDataService, RestService} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {ScmServerViewService} from '../../../services/scm-server-view.service';
import {mockRestService} from '../../../../../../../utils/testing-utils/mocks.service';

describe('ScmServerRepositoriesPanelComponent', () => {
  let component: ScmServerRepositoriesPanelComponent;
  let fixture: ComponentFixture<ScmServerRepositoriesPanelComponent>;

  const restService = mockRestService();

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ScmServerRepositoriesPanelComponent],
      imports: [HttpClientTestingModule, AppTestingUtilsModule, TranslateModule.forRoot(), RouterTestingModule],
      providers: [
        {
          provide: RestService,
          useValue: restService
        },
        {
          provide: ScmServerViewService,
          useClass: ScmServerViewService
        },
        {
          provide: DialogService,
          useValue: {},
        },
        {
          provide: SCM_SERVER_REPOSITORIES_TABLE_CONF,
          useFactory: scmServerRepositoriesTableDefinition,
        },
        {
          provide: SCM_SERVER_REPOSITORIES_TABLE,
          useFactory: gridServiceFactory,
          deps: [RestService, SCM_SERVER_REPOSITORIES_TABLE_CONF, ReferentialDataService]
        }

      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScmServerRepositoriesPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
