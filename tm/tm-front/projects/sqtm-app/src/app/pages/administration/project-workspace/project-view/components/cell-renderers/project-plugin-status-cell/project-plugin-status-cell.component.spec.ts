import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ProjectPluginStatusCellComponent } from './project-plugin-status-cell.component';
import {
  grid,
  GridDefinition,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService,
} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';

describe('ProjectPluginStatusCellComponent', () => {
  let component: ProjectPluginStatusCellComponent;
  let fixture: ComponentFixture<ProjectPluginStatusCellComponent>;
  const gridConfig = grid('grid-test').build();
  const restService = {};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectPluginStatusCellComponent ],
      imports: [AppTestingUtilsModule],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig
        },
        {
          provide: RestService,
          useValue: restService
        },
        {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectPluginStatusCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
