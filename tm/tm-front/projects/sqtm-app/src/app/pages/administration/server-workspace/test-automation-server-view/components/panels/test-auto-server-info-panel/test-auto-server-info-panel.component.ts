import {Component, ChangeDetectionStrategy, Input} from '@angular/core';
import {AdminTestAutomationServerViewComponentData} from '../../../containers/test-automation-server-view/test-automation-server-view.component';
import {TestAutomationServerViewService} from '../../../services/test-automation-server-view.service';
import {getTestAutomationServerKindI18nKey, TestAutomationServerKind} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-test-automation-server-information-panel',
  templateUrl: './test-auto-server-info-panel.component.html',
  styleUrls: ['./test-auto-server-info-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TestAutoServerInfoPanelComponent {

  @Input()
  componentData: AdminTestAutomationServerViewComponentData;

  get isManualSlaveSelectionVisible(): boolean {
    return this.componentData.testAutomationServer.kind === TestAutomationServerKind.jenkins;
  }

  constructor(private testAutomationServerViewService: TestAutomationServerViewService) {
  }

  changeManualSlaveSelection(selected: boolean) {
    this.testAutomationServerViewService.changeManualSlaveSelection(selected);
  }

  getServerKind(componentData: AdminTestAutomationServerViewComponentData): string {
    return getTestAutomationServerKindI18nKey(componentData.testAutomationServer.kind as TestAutomationServerKind);
  }
}
