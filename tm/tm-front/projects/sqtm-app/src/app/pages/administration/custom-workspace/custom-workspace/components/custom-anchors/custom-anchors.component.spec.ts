import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {CustomAnchorsComponent} from './custom-anchors.component';
import {RouterTestingModule} from '@angular/router/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';
import {GridService, WorkspaceWithGridComponent} from 'sqtm-core';

describe('CustomAnchorsComponent', () => {
  let component: CustomAnchorsComponent;
  let fixture: ComponentFixture<CustomAnchorsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [CustomAnchorsComponent],
      imports: [RouterTestingModule, AppTestingUtilsModule],
      providers: [
        {
          provide: WorkspaceWithGridComponent,
          useValue: {},
        },
        {
          provide: GridService,
          useValue: {}
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomAnchorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
