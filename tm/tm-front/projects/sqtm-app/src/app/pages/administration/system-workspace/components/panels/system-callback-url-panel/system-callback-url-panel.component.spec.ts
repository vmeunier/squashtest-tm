import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SystemCallbackUrlPanelComponent } from './system-callback-url-panel.component';
import {SystemViewService} from '../../../services/system-view.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {EMPTY} from 'rxjs';
import {TranslateModule} from '@ngx-translate/core';

describe('SystemCallbackUrlPanelComponent', () => {
  let component: SystemCallbackUrlPanelComponent;
  let fixture: ComponentFixture<SystemCallbackUrlPanelComponent>;

  const systemViewService = jasmine.createSpyObj(['load']);
  systemViewService['componentData$'] = EMPTY;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SystemCallbackUrlPanelComponent ],
      imports: [TranslateModule.forRoot()],
      providers: [
        {
          provide: SystemViewService,
          useValue: systemViewService,
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemCallbackUrlPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
