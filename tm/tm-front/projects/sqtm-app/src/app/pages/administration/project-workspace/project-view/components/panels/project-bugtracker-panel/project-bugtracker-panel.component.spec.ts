import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {ProjectBugtrackerPanelComponent} from './project-bugtracker-panel.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RestService} from 'sqtm-core';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {ProjectViewService} from '../../../services/project-view.service';
import {AdminProjectState} from '../../../state/admin-project-state';
import {of} from 'rxjs';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {mockRestService} from '../../../../../../../utils/testing-utils/mocks.service';

describe('ProjectBugtrackerPanelComponent', () => {
  let component: ProjectBugtrackerPanelComponent;
  let fixture: ComponentFixture<ProjectBugtrackerPanelComponent>;

  const restService = mockRestService();

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule, TranslateModule.forRoot(), RouterTestingModule],
      providers: [
        {
          provide: RestService,
          useValue: restService
        },
        {
          provide: ProjectViewService,
          useValue: {
            componentData$: of(null)
          }
        },
      ],
      declarations: [ProjectBugtrackerPanelComponent],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectBugtrackerPanelComponent);
    component = fixture.componentInstance;
    component.adminProjectViewComponentData = {
      project: {id: -1, availableBugtrackers: []} as AdminProjectState,
      type: 'project', uiState: null
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
