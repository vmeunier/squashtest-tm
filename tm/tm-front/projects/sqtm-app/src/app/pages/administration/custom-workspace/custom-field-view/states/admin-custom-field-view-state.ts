import {AdminCustomFieldState} from './admin-custom-field-state';
import {GenericEntityViewState, provideInitialGenericViewState} from 'sqtm-core';

export interface AdminCustomFieldViewState extends GenericEntityViewState<AdminCustomFieldState, 'customField'> {
  customField: AdminCustomFieldState;
}

export function provideInitialAdminCustomFieldView(): Readonly<AdminCustomFieldViewState> {
  return provideInitialGenericViewState<AdminCustomFieldState, 'customField'>('customField');
}
