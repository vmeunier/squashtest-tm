import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { UserCreationDialogComponent } from './user-creation-dialog.component';
import {DialogReference} from 'sqtm-core';
import {ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {CKEditorModule} from 'ckeditor4-angular';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import createSpyObj = jasmine.createSpyObj;
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';

describe('UserCreationDialogComponent', () => {
  let component: UserCreationDialogComponent;
  let fixture: ComponentFixture<UserCreationDialogComponent>;

  const dialogReference = createSpyObj(['close']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, ReactiveFormsModule, TranslateModule.forRoot(), HttpClientTestingModule, CKEditorModule],
      providers: [
        {
          provide: DialogReference,
          useValue: dialogReference
        }],
      declarations: [ UserCreationDialogComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserCreationDialogComponent);
    component = fixture.componentInstance;
    component.data = {
      titleKey: 'sqtm-core.administration-workspace.users.dialog.title.new-user',
      usersGroups: [],
      canManageLocalPassword: true,
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
