import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddCustomFieldOptionDialogComponent } from './add-custom-field-option-dialog.component';
import createSpyObj = jasmine.createSpyObj;
import {
  DialogReference, grid,
  GridDefinition,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService
} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {CustomFieldViewService} from '../../../services/custom-field-view.service';
import {of} from 'rxjs';

describe('AddCustomFieldOptionDialogComponent', () => {
  let component: AddCustomFieldOptionDialogComponent;
  let fixture: ComponentFixture<AddCustomFieldOptionDialogComponent>;
  const dialogReference = createSpyObj(['close']);
  const gridConfig = grid('grid-test').build();

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCustomFieldOptionDialogComponent ],
      imports: [AppTestingUtilsModule, ReactiveFormsModule, TranslateModule.forRoot(), HttpClientTestingModule],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig
        },
        {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        },
        {
          provide: CustomFieldViewService,
          useValue: {
            componentData$: of({})
          }
        },
        {
          provide: DialogReference,
          useValue: dialogReference
        }],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCustomFieldOptionDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
