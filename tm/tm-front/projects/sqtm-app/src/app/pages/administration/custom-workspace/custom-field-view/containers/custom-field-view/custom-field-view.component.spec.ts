import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {CustomFieldViewComponent} from './custom-field-view.component';
import {ActivatedRoute, convertToParamMap} from '@angular/router';
import {of} from 'rxjs';
import {CustomFieldViewService} from '../../services/custom-field-view.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';
import {TranslateService} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import {DialogService, GridService} from 'sqtm-core';

describe('CustomFieldViewComponent', () => {
  let component: CustomFieldViewComponent;
  let fixture: ComponentFixture<CustomFieldViewComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule, AppTestingUtilsModule, NzDropDownModule],
      providers: [
        {
          provide: TranslateService,
          useValue: jasmine.createSpyObj(['instant']),
        },
        {
          provide: CustomFieldViewService,
          useValue: {
            componentData$: of({})
          }
        },
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({id: 123}),
            paramMap: of(convertToParamMap({
              toto: 'tutu',
            })),
          }
        },
        {
          provide: DialogService,
          useValue: jasmine.createSpyObj(['openDeletionConfirm', 'openAlert'])
        },
        {
          provide: GridService,
          useValue: jasmine.createSpyObj(['refreshData'])
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [CustomFieldViewComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomFieldViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
