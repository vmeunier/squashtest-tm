import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {DialogReference, DisplayOption, FieldValidationError, RestService, SelectFieldComponent} from 'sqtm-core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {ProjectViewService} from '../../../services/project-view.service';

@Component({
  selector: 'sqtm-app-associate-template-dialog',
  templateUrl: './associate-template-dialog.component.html',
  styleUrls: ['./associate-template-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AssociateTemplateDialogComponent implements OnInit {

  public templateOptions: DisplayOption[];
  formGroup: FormGroup;
  serverSideValidationErrors: FieldValidationError[] = [];

  @ViewChild(SelectFieldComponent)
  templateSelectField: SelectFieldComponent;

  private readonly noTemplateOptionId: string = '';

  constructor(private dialogReference: DialogReference<any, void>,
              private restService: RestService,
              private cdr: ChangeDetectorRef,
              private fb: FormBuilder,
              private projectViewService: ProjectViewService,
              private translateService: TranslateService) {
  }

  ngOnInit(): void {
    this.initializeFormGroup();
    this.prepareTemplateSelectField();
  }

  private prepareTemplateSelectField() {
    this.restService.get<{ templates: { id: string, name: string }[] }>(['generic-projects/templates']).subscribe((response) => {
      const defaultOption: DisplayOption = this.retrieveDefaultDisplayOption();
      const options = retrieveTemplatesAsDisplayOptions(response.templates);

      this.templateOptions = [defaultOption, ...options];

      if (options.length > 0) {
        this.templateSelectField.disabled = false;
      }

      this.formGroup.controls['template'].setValue(this.noTemplateOptionId);

      this.cdr.detectChanges();
    });
  }

  private retrieveDefaultDisplayOption() {
    return {
      id: this.noTemplateOptionId,
      label: this.translateService.instant('sqtm-core.administration-workspace.projects.dialog.message.associate-template-default-option'),
    };
  }

  private initializeFormGroup() {
    this.formGroup = this.fb.group({
      template: this.fb.control(null, [
        Validators.required,
      ]),
    });
  }

  confirm() {
    if (! this.formGroup.valid) {
      this.templateSelectField.showClientSideError();
      return;
    }
    const selectedTemplateId = this.formGroup.controls['template'].value;
    this.projectViewService.associateWithTemplate(selectedTemplateId);
    this.dialogReference.close();
  }
}

function retrieveTemplatesAsDisplayOptions(templates: { id: string; name: string }[]) {
  return templates.map((ref) => ({ id: ref.id, label: ref.name }));
}
