import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TestAutomationServerContentComponent } from './test-automation-server-content.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {DialogModule, GridService} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TestAutomationServerViewService} from '../../../services/test-automation-server-view.service';
import {EMPTY} from 'rxjs';
import createSpyObj = jasmine.createSpyObj;

describe('TestAutomationServerContentComponent', () => {
  let component: TestAutomationServerContentComponent;
  let fixture: ComponentFixture<TestAutomationServerContentComponent>;

  const viewService = createSpyObj(['complete', 'load']);
  viewService['componentData$'] = EMPTY;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TestAutomationServerContentComponent ],
      imports: [HttpClientTestingModule, AppTestingUtilsModule, DialogModule],
      providers: [
        {
          provide: TranslateService,
          useValue: jasmine.createSpyObj('TranslateService', ['instant']),
        },
        {
          provide: GridService,
          useValue: jasmine.createSpyObj(['refreshData'])
        },
        {
          provide: TestAutomationServerViewService,
          useValue: viewService,
        },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestAutomationServerContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
