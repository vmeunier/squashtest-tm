import {TestBed} from '@angular/core/testing';

import {TestAutomationServerViewService} from './test-automation-server-view.service';
import {
  AdminTestAutomationServer,
  AttachmentService,
  AuthenticationProtocol,
  EntityViewAttachmentHelperService,
  RestService,
  TestAutomationServerKind
} from 'sqtm-core';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {TranslateService} from '@ngx-translate/core';
import {of} from 'rxjs';
import {take} from 'rxjs/operators';
import {mockRestService} from '../../../../../utils/testing-utils/mocks.service';
import createSpyObj = jasmine.createSpyObj;

describe('TestAutomationServerViewService', () => {
  let service: TestAutomationServerViewService;
  const restService = mockRestService();

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule],
      providers: [
        {
          provide: RestService,
          useValue: restService,
        },
        {
          provide: TranslateService,
          useValue: createSpyObj(['instant']),
        },
        {
          provide: TestAutomationServerViewService,
          useClass: TestAutomationServerViewService,
          deps: [
            RestService,
            AttachmentService,
            TranslateService,
            EntityViewAttachmentHelperService,
          ]
        },
      ]
    });
    service = TestBed.inject(TestAutomationServerViewService);
  });

  it('should load a test automation server', (done) => {
    restService.get.and.returnValue(of(getInitialModel()));

    service.componentData$.subscribe(data => {
      expect(data.testAutomationServer.id).toEqual(1);
      done();
    });

    expect(service).toBeTruthy();

    service.load(1);
  });

  it('should change manual slave selection', async (done) => {

    const testAutomationServer = getInitialModel();
    restService.get.and.returnValue(of(testAutomationServer));
    service.load(1);

    const changeManualSlaveSelectionResponse = {
      ...testAutomationServer,
      manualSlaveSelection: true
    };
    restService.post.and.returnValue(of(changeManualSlaveSelectionResponse));

    service.changeManualSlaveSelection(true);
    service.componentData$.pipe(
      take(1)
    ).subscribe(componentData => {
      expect(componentData.testAutomationServer.manualSlaveSelection).toEqual(changeManualSlaveSelectionResponse.manualSlaveSelection);
      done();
    });
  });

  function getInitialModel(): AdminTestAutomationServer {
    return {
      id: 1,
      name: 'milestone 1',
      kind: TestAutomationServerKind.jenkins,
      baseUrl: 'http://127.0.0.01:8080',
      description: '',
      createdBy: '',
      createdOn: null,
      lastModifiedBy: null,
      lastModifiedOn: null,
      manualSlaveSelection: true,
      credentials: null,
      authProtocol: AuthenticationProtocol.BASIC_AUTH,
      supportedAuthenticationProtocols: [AuthenticationProtocol.BASIC_AUTH]
    };
  }
});
