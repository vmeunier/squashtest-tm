import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {USER_TEAMS_TABLE, USER_TEAMS_TABLE_CONF, UserTeamsPanelComponent, userTeamsTableDefinition} from './user-teams-panel.component';
import {DialogService, gridServiceFactory, ReferentialDataService, RestService} from 'sqtm-core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {UserViewService} from '../../../services/user-view.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {EMPTY} from 'rxjs';
import {mockRestService} from '../../../../../../../utils/testing-utils/mocks.service';

describe('UserTeamsPanelComponent', () => {
  let component: UserTeamsPanelComponent;
  let fixture: ComponentFixture<UserTeamsPanelComponent>;

  const restService = mockRestService();

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule, TranslateModule.forRoot(), RouterTestingModule],
      declarations: [UserTeamsPanelComponent],
      providers: [
        {
          provide: RestService,
          useValue: restService
        },
        {
          provide: UserViewService,
          useValue: {
            componentData$: EMPTY
          }
        },
        {
          provide: DialogService,
          useValue: {},
        },
        {
          provide: USER_TEAMS_TABLE_CONF,
          useFactory: userTeamsTableDefinition,
        },
        {
          provide: USER_TEAMS_TABLE,
          useFactory: gridServiceFactory,
          deps: [RestService, USER_TEAMS_TABLE_CONF, ReferentialDataService]
        }

      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserTeamsPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
