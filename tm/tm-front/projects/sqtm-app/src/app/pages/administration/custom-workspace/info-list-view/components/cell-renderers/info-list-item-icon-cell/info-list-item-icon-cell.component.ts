import {ChangeDetectionStrategy, ChangeDetectorRef, Component, ViewChild} from '@angular/core';
import {AbstractCellRendererComponent, ColumnDefinitionBuilder, GridService, IconPickerFieldComponent} from 'sqtm-core';
import {InfoListViewService} from '../../../services/info-list-view.service';
import {catchError} from 'rxjs/operators';
import {of} from 'rxjs';
import {getInfoListIcons, INFO_LIST_ICON_NAMESPACE} from '../../../../info-list-icons.constant';

@Component({
  selector: 'sqtm-app-info-list-item-icon-cell',
  template: `
    <ng-container *ngIf="columnDisplay && row">
      <div class="full-width full-height flex-column" style="cursor: pointer">
        <sqtm-core-icon-picker-field
            class="m-auto"
            [icons]="infoListIcons"
            [icon]="iconName"
            (iconChanged)="handleIconChange($event)">
        </sqtm-core-icon-picker-field>
      </div>
    </ng-container>`,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InfoListItemIconCellComponent extends AbstractCellRendererComponent {

  @ViewChild(IconPickerFieldComponent)
  iconField: IconPickerFieldComponent;

  infoListIcons: string[];

  get hasIcon(): boolean {
    const iconName = this.row.data[this.columnDisplay.id];
    return Boolean(iconName) && iconName.length > 0 && iconName !== 'noicon';
  }

  get iconName(): string {
    return this.hasIcon ? INFO_LIST_ICON_NAMESPACE + this.row.data[this.columnDisplay.id] : '';
  }

  constructor(public grid: GridService,
              public cdRef: ChangeDetectorRef,
              public infoListViewService: InfoListViewService) {
    super(grid, cdRef);
    this.infoListIcons = getInfoListIcons();
  }

  handleIconChange(newIcon: string): void {
    const previousIcon = this.row.data.iconName;
    this.infoListViewService.changeItemIcon(this.row.data.id, newIcon).pipe(
      catchError((err) => {
        this.iconField.selectedIcon = previousIcon;
        return of(err);
      })
    ).subscribe();
  }
}

export function infoListItemIconColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(InfoListItemIconCellComponent);
}
