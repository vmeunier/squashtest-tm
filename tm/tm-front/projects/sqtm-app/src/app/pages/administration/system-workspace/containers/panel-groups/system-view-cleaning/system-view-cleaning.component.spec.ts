import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SystemViewCleaningComponent } from './system-view-cleaning.component';
import {Overlay} from '@angular/cdk/overlay';
import {SystemViewService} from '../../../services/system-view.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {EMPTY} from 'rxjs';

describe('SystemViewCleaningComponent', () => {
  let component: SystemViewCleaningComponent;
  let fixture: ComponentFixture<SystemViewCleaningComponent>;

  const systemViewService = jasmine.createSpyObj(['load']);
  systemViewService['componentData$'] = EMPTY;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SystemViewCleaningComponent ],
      providers : [
        {provide: Overlay, useClass: Overlay},
        {provide: SystemViewService, useValue: systemViewService}
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemViewCleaningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
