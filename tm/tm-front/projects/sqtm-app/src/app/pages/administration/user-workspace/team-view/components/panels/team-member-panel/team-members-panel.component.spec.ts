import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {
  TEAM_MEMBERS_TABLE,
  TEAM_MEMBERS_TABLE_CONF,
  TeamMembersPanelComponent,
  teamMembersTableDefinition
} from './team-members-panel.component';
import {DialogService, gridServiceFactory, ReferentialDataService, RestService} from 'sqtm-core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TeamViewService} from '../../../services/team-view.service';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {EMPTY} from 'rxjs';
import {mockRestService} from '../../../../../../../utils/testing-utils/mocks.service';

describe('TeamMembersPanelComponent', () => {
  let component: TeamMembersPanelComponent;
  let fixture: ComponentFixture<TeamMembersPanelComponent>;

  const restService = mockRestService();


  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule, TranslateModule.forRoot(), RouterTestingModule],
      declarations: [TeamMembersPanelComponent],
      providers: [
        {
          provide: RestService,
          useValue: restService
        },
        {
          provide: TeamViewService,
          useValue: {
            componentData$: EMPTY
          }
        },
        {
          provide: DialogService,
          useValue: {},
        },
        {
          provide: TEAM_MEMBERS_TABLE_CONF,
          useFactory: teamMembersTableDefinition,
        },
        {
          provide: TEAM_MEMBERS_TABLE,
          useFactory: gridServiceFactory,
          deps: [RestService, TEAM_MEMBERS_TABLE_CONF, ReferentialDataService]
        }

      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamMembersPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
