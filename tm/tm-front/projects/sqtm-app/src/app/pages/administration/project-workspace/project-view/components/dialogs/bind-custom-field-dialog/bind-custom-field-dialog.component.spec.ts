import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {BindCustomFieldDialogComponent} from './bind-custom-field-dialog.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DialogReference, RestService} from 'sqtm-core';
import {TranslateModule} from '@ngx-translate/core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import { NzSelectModule } from 'ng-zorro-antd/select';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {ProjectViewService} from '../../../services/project-view.service';
import {mockRestService} from '../../../../../../../utils/testing-utils/mocks.service';


describe('BindCustomFieldDialogComponent', () => {
  let component: BindCustomFieldDialogComponent;
  let fixture: ComponentFixture<BindCustomFieldDialogComponent>;
  const restService = mockRestService();

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule,
        FormsModule,
        AppTestingUtilsModule,
        TranslateModule.forRoot(),
        HttpClientTestingModule,
        NzSelectModule,
        BrowserAnimationsModule],
      declarations: [BindCustomFieldDialogComponent],
      providers: [
        {
          provide: RestService,
          useValue: restService
        },
        {
          provide: DialogReference,
          useValue: {data: {customFields: []}},
        },
        {
          provide: ProjectViewService,
          useValue: {}
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BindCustomFieldDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
