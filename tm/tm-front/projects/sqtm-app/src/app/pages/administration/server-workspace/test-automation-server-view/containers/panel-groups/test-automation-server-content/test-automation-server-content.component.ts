import {Component, OnInit, ChangeDetectionStrategy, OnDestroy} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {TestAutomationServerViewService} from '../../../services/test-automation-server-view.service';
import {AdminTestAutomationServerViewState} from '../../../states/admin-test-automation-server-view-state';

@Component({
  selector: 'sqtm-app-test-automation-server-content',
  templateUrl: './test-automation-server-content.component.html',
  styleUrls: ['./test-automation-server-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TestAutomationServerContentComponent implements OnInit, OnDestroy  {

  componentData$: Observable<AdminTestAutomationServerViewState>;

  unsub$ = new Subject<void>();

  constructor(public testAutomationServerViewService: TestAutomationServerViewService) {
    this.componentData$ = testAutomationServerViewService.componentData$.pipe(takeUntil(this.unsub$));
  }

  ngOnInit(): void {
    this.componentData$ = this.testAutomationServerViewService.componentData$.pipe(
      takeUntil(this.unsub$)
    );
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

}
