import {ChangeDetectionStrategy, Component, Input, ViewChild} from '@angular/core';
import {AuthenticationProtocol, Credentials, isBasicAuthCredentials, ThirdPartyCredentialsFormComponent} from 'sqtm-core';
import {AdminScmServerViewState} from '../../../states/admin-scm-server-view-state';
import {ScmServerViewService} from '../../../services/scm-server-view.service';

@Component({
  selector: 'sqtm-app-scm-server-authentication-policy-panel',
  templateUrl: './scm-server-authentication-policy-panel.component.html',
  styleUrls: ['./scm-server-authentication-policy-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ScmServerAuthenticationPolicyPanelComponent {

  readonly authenticationProtocol = AuthenticationProtocol.BASIC_AUTH;

  @Input()
  componentData: AdminScmServerViewState;

  @ViewChild(ThirdPartyCredentialsFormComponent)
  credentialsForm: ThirdPartyCredentialsFormComponent;
  credentialsStatusMessage = '';
  statusIcon: 'INFO' = null;

  constructor(public readonly scmServerViewService: ScmServerViewService) {
  }

  sendCredentialsForm(credentials: Credentials) {
    this.clearMessage();

    if (isBasicAuthCredentials(credentials)) {
      this.scmServerViewService.setCredentials(credentials.username, credentials.password)
        .subscribe(() => this.handleCredentialsSaveSuccess());
    } else {
      throw new Error('Only basic auth is handled for SCM servers.');
    }
  }

  private clearMessage(): void {
    this.credentialsStatusMessage = null;
    this.statusIcon = null;
  }

  private handleCredentialsSaveSuccess(): void {
    this.credentialsForm.formGroup.markAsPristine();
    this.credentialsForm.endAsync();

    this.credentialsStatusMessage = 'sqtm-core.entity.scm-server.credentials.save-success';
    this.statusIcon = 'INFO';
  }
}
