import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {
  INFO_LIST_ITEMS_TABLE,
  INFO_LIST_ITEMS_TABLE_CONF,
  InfoListItemsPanelComponent,
  infoListItemsTableDefinition
} from './info-list-items-panel.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {GridService, gridServiceFactory, ReferentialDataService, RestService} from 'sqtm-core';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {RouterTestingModule} from '@angular/router/testing';
import {OverlayModule} from '@angular/cdk/overlay';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {InfoListViewService} from '../../../services/info-list-view.service';
import {EMPTY} from 'rxjs';

describe('InfoListItemsPanelComponent', () => {
  let component: InfoListItemsPanelComponent;
  let fixture: ComponentFixture<InfoListItemsPanelComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule, TranslateModule.forRoot(), RouterTestingModule, OverlayModule],
      declarations: [InfoListItemsPanelComponent],
      providers: [
        {
          provide: RestService,
          useValue: jasmine.createSpyObj(['get', 'post']),
        },
        {
          provide: TranslateService,
          useValue: jasmine.createSpyObj(['instant']),
        },
        {
          provide: INFO_LIST_ITEMS_TABLE_CONF,
          useFactory: infoListItemsTableDefinition,
        },
        {
          provide: INFO_LIST_ITEMS_TABLE,
          useFactory: gridServiceFactory,
          deps: [RestService, INFO_LIST_ITEMS_TABLE_CONF, ReferentialDataService]
        },
        {
          provide: GridService,
          useValue: INFO_LIST_ITEMS_TABLE,
        },
        {
          provide: InfoListViewService,
          useValue: {
            componentData$: EMPTY
          }
        },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoListItemsPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
