import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InfoListInformationPanelComponent } from './info-list-information-panel.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {OverlayModule} from '@angular/cdk/overlay';
import {DialogService, RestService} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {InfoListViewService} from '../../../services/info-list-view.service';
import {EMPTY} from 'rxjs';

describe('InfoListInformationPanelComponent', () => {
  let component: InfoListInformationPanelComponent;
  let fixture: ComponentFixture<InfoListInformationPanelComponent>;
  const restService = {};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoListInformationPanelComponent ],
      imports: [HttpClientTestingModule, AppTestingUtilsModule, OverlayModule],
      providers: [
        {
          provide: RestService,
          useValue: restService
        },
        {
          provide: TranslateService,
          useValue: jasmine.createSpyObj(['instant']),
        },
        {
          provide: DialogService,
          userValue: {}
        },
        {
          provide: InfoListViewService,
          useValue: {
            componentData$: EMPTY
          }
        },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoListInformationPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
