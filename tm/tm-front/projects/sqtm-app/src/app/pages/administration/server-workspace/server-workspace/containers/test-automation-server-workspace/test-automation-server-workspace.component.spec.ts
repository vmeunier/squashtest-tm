import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TestAutomationServerWorkspaceComponent } from './test-automation-server-workspace.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';
import {GridService} from 'sqtm-core';

describe('TestAutomationServerWorkspaceComponent', () => {
  let component: TestAutomationServerWorkspaceComponent;
  let fixture: ComponentFixture<TestAutomationServerWorkspaceComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TestAutomationServerWorkspaceComponent ],
      imports: [HttpClientTestingModule, AppTestingUtilsModule, RouterTestingModule],
      providers: [
        {
          provide: GridService,
          useValue: jasmine.createSpyObj(['complete']),
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestAutomationServerWorkspaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
