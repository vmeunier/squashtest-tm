import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DeleteDropdownListOptionComponent } from './delete-dropdown-list-option.component';
import {
  grid,
  GridDefinition,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService,
} from 'sqtm-core';
import {OverlayModule} from '@angular/cdk/overlay';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {DropdownListOptionService} from '../../../services/dropdown-list-option.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';

describe('DeleteDropDownListOptionCellRendererComponent', () => {
  let component: DeleteDropdownListOptionComponent;
  let fixture: ComponentFixture<DeleteDropdownListOptionComponent>;
  const gridConfig = grid('grid-test').build();

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteDropdownListOptionComponent ],
      imports: [AppTestingUtilsModule, OverlayModule,  HttpClientTestingModule],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig
        }, {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        },
        {
          provide: DropdownListOptionService,
          useValue: {}
        },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteDropdownListOptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
