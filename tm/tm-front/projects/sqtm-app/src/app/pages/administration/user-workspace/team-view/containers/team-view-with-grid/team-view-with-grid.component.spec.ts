import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TeamViewWithGridComponent } from './team-view-with-grid.component';
import {ActivatedRoute, convertToParamMap} from '@angular/router';
import {of} from 'rxjs';
import {GridService, RestService} from 'sqtm-core';
import {mockGridService, mockRestService} from '../../../../../../utils/testing-utils/mocks.service';
import {TranslateService} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('TeamViewWithGridComponent', () => {
  let component: TeamViewWithGridComponent;
  let fixture: ComponentFixture<TeamViewWithGridComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamViewWithGridComponent ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: of(convertToParamMap({
              teamId: '123',
            })),
          }
        },
        {
          provide: GridService,
          useValue: mockGridService(),
        },
        {
          provide: RestService,
          useValue: mockRestService(),
        },
        {
          provide: TranslateService,
          useValue: {}
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamViewWithGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
