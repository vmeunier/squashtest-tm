import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BindProjectsToMilestoneDialogComponent } from './bind-projects-to-milestone-dialog.component';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {DialogReference} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('BindProjectsToMilestoneDialogComponent', () => {
  let component: BindProjectsToMilestoneDialogComponent;
  let fixture: ComponentFixture<BindProjectsToMilestoneDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BindProjectsToMilestoneDialogComponent ],
      imports: [AppTestingUtilsModule, ReactiveFormsModule, TranslateModule.forRoot(), HttpClientTestingModule],
      providers: [
        {
          provide: DialogReference,
          useValue: jasmine.createSpyObj(['close'])
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BindProjectsToMilestoneDialogComponent);
    component = fixture.componentInstance;
    component.data = {
      bindableProjects: []
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
