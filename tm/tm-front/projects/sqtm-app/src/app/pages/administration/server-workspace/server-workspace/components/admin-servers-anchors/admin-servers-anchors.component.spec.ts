import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {AdminServersAnchorsComponent} from './admin-servers-anchors.component';
import {RouterTestingModule} from '@angular/router/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';
import {GridService, WorkspaceWithGridComponent} from 'sqtm-core';

describe('AdministrationServersAnchorsComponent', () => {
  let component: AdminServersAnchorsComponent;
  let fixture: ComponentFixture<AdminServersAnchorsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AdminServersAnchorsComponent],
      imports: [RouterTestingModule, AppTestingUtilsModule],
      providers: [
        {
          provide: WorkspaceWithGridComponent,
          useValue: {},
        },
        {
          provide: GridService,
          useValue: {}
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminServersAnchorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
