import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SystemViewInformationComponent } from './system-view-information.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {SystemViewService} from '../../../services/system-view.service';
import {EMPTY} from 'rxjs';

describe('SystemViewInformationComponent', () => {
  let component: SystemViewInformationComponent;
  let fixture: ComponentFixture<SystemViewInformationComponent>;

  const systemViewService = jasmine.createSpyObj(['load']);
  systemViewService['componentData$'] = EMPTY;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SystemViewInformationComponent ],
      providers: [
        {
          provide: SystemViewService,
          useValue: systemViewService,
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemViewInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
