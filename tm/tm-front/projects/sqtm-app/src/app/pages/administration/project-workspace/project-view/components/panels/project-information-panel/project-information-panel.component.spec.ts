import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ProjectInformationPanelComponent} from './project-information-panel.component';
import {RestService} from 'sqtm-core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {DatePipe} from '@angular/common';
import {ProjectViewService} from '../../../services/project-view.service';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {mockRestService} from '../../../../../../../utils/testing-utils/mocks.service';

describe('ProjectInformationPanelComponent', () => {
  let component: ProjectInformationPanelComponent;
  let fixture: ComponentFixture<ProjectInformationPanelComponent>;

  const restService = mockRestService();

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule, TranslateModule.forRoot(), RouterTestingModule],
      providers: [
        {
          provide: DatePipe,
          useValue: jasmine.createSpyObj(['transform']),
        },
        {
          provide: RestService,
          useValue: restService
        },
        {
          provide: ProjectViewService,
          useClass: ProjectViewService
        },
      ],
      declarations: [ProjectInformationPanelComponent],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectInformationPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
