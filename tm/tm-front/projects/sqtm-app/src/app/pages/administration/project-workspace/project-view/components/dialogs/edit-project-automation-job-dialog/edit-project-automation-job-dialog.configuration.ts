export interface EditProjectAutomationJobDialogConfiguration {
  remoteName: string;
  canRunBdd: boolean;
  taProjectId: number;
  label: string;
  executionEnvironment: string;
}
