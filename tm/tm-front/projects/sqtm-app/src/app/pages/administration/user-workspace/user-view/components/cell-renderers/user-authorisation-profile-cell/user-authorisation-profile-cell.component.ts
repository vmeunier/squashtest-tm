import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnInit,
  TemplateRef,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {
  AbstractListCellRendererComponent,
  AclGroup,
  ActionErrorDisplayService,
  AdminReferentialDataService,
  ColumnDefinitionBuilder,
  getAclGroupI18nKey,
  GridService,
  LicenseInformationMessageProvider,
  ListPanelItem,
  PermissionGroup,
  RestService
} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {Overlay} from '@angular/cdk/overlay';
import {UserViewService} from '../../../services/user-view.service';
import {finalize, map, takeUntil} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-user-auhtorisation-profile-cell',
  template: `
    <ng-container *ngIf="columnDisplay && row">
      <div class="full-width full-height container"
           [class.container-interactive]="canEdit()"
           [class.__hover_pointer]="canEdit()"
           (click)="showProfileList()">
        <span #profileName
              class="text-ellipsis"
              style="margin: auto 0;"
              nz-tooltip
              [sqtmCoreLabelTooltip]="cellText">
          {{cellText}}
        </span>
        <ng-template #templatePortalContent>
          <sqtm-core-list-panel
              [selectedItem]="row.data[columnDisplay.id]"
              (itemSelectionChanged)="change($event)"
              [items]="panelItems">
          </sqtm-core-list-panel>
        </ng-template>
      </div>
    </ng-container>`,
  styleUrls: ['./user-authorisation-profile-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserAuthorisationProfileCellComponent extends AbstractListCellRendererComponent implements OnInit {

  constructor(public readonly grid: GridService,
              public readonly cdRef: ChangeDetectorRef,
              public readonly translateService: TranslateService,
              public readonly overlay: Overlay,
              public readonly vcr: ViewContainerRef,
              public readonly restService: RestService,
              public readonly userViewService: UserViewService,
              public readonly actionErrorDisplayService: ActionErrorDisplayService,
              public readonly adminReferentialDataService: AdminReferentialDataService) {

    super(grid, cdRef, overlay, vcr, translateService, restService, actionErrorDisplayService);
    this.panelItems = this.getPanelItems();
  }

  get cellText(): string {
    if (this.panelItems == null) {
      return '';
    }

    const permissionGroup = this.row.data['permissionGroup'] as PermissionGroup;
    const groupName = permissionGroup.qualifiedName;

    const items = this.panelItems.filter(item => item.id === groupName);

    if (items.length === 0) {
      return '';
    }

    return items[0].label;
  }

  @ViewChild('templatePortalContent', {read: TemplateRef})
  templatePortalContent: TemplateRef<any>;

  @ViewChild('profileName', {read: ElementRef})
  profileName: ElementRef;

  panelItems: ListPanelItem[] = [];

  private _licenseAllowsUserCreation: boolean;

  ngOnInit() {
    this.initialisePanelItems();
    this.initialiseLicenseLock();
  }

  change(newValue: any) {
    this.grid.beginAsyncOperation();
    this.userViewService.setUserAuthorisation([this.row.data['projectId']], newValue).pipe(
      finalize(() => this.grid.completeAsyncOperation())
    ).subscribe();
    this.close();
  }

  canEdit(): boolean {
    return this._licenseAllowsUserCreation;
  }

  showProfileList() {
    if (this.canEdit()) {
      this.showList(
        this.profileName,
        this.templatePortalContent,
        [
          {originX: 'start', overlayX: 'start', originY: 'bottom', overlayY: 'top', offsetX: -10, offsetY: 6},
          {originX: 'start', overlayX: 'start', originY: 'top', overlayY: 'bottom', offsetX: -10, offsetY: -6},
        ]
      );
    }
  }

  private initialiseLicenseLock(): void {
    this.adminReferentialDataService.licenseInformation$.pipe(
      takeUntil(this.unsub$),
      map(licenseInfo => new LicenseInformationMessageProvider(licenseInfo, this.translateService))
    ).subscribe(messageHelper => {
      this._licenseAllowsUserCreation = messageHelper.licenseInformation.allowCreateUsers;
      this.cdRef.detectChanges();
    });
  }

  private initialisePanelItems(): void {
    this.panelItems = this.getPanelItems();
  }

  private getPanelItems() {
    const panelItems = Object.keys(AclGroup)
      .map(key => AclGroup[key])
      .map((groupName) => {
        return {
          label: this.translateService.instant(getAclGroupI18nKey(groupName)),
          id: groupName,
        };
      });

    // Sort options by locale label
    panelItems.sort((a, b) => {
      return a.label.localeCompare(b.label);
    });

    return panelItems;
  }
}

export function userAuthorisationProfileColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(UserAuthorisationProfileCellComponent)
    .withI18nKey('sqtm-core.administration-workspace.views.project.permissions.profile.label');
}
