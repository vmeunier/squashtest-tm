import {Component, OnInit, ChangeDetectionStrategy, Input} from '@angular/core';
import {AdminScmServerViewState} from '../../../states/admin-scm-server-view-state';
import {AuthenticationProtocol, Option} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-scm-server-authentication-protocol-panel',
  templateUrl: './scm-server-authentication-protocol-panel.component.html',
  styleUrls: ['./scm-server-authentication-protocol-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ScmServerAuthenticationProtocolPanelComponent implements OnInit {

  @Input()
  componentData: AdminScmServerViewState;

  private readonly authProtocolOptions: Option[];

  constructor(public readonly translateService: TranslateService) {
    this.authProtocolOptions = [
      {
        value: AuthenticationProtocol.BASIC_AUTH,
        label: translateService.instant('sqtm-core.entity.server.auth-protocol.basic-auth.label'),
      }
    ];
  }

  ngOnInit(): void {
  }

}
