import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {
  USER_AUTHORISATIONS_TABLE,
  USER_AUTHORISATIONS_TABLE_CONF,
  UserAuthorisationsPanelComponent,
  userAuthorisationsTableDefinition
} from './user-authorisations-panel.component';
import {DialogService, gridServiceFactory, ReferentialDataService, RestService} from 'sqtm-core';
import {UserViewService} from '../../../services/user-view.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {EMPTY} from 'rxjs';
import {mockRestService} from '../../../../../../../utils/testing-utils/mocks.service';

describe('UserAuthorisationsPanelComponent', () => {
  let component: UserAuthorisationsPanelComponent;
  let fixture: ComponentFixture<UserAuthorisationsPanelComponent>;

  const restService = mockRestService();

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule, TranslateModule.forRoot(), RouterTestingModule],
      declarations: [UserAuthorisationsPanelComponent],
      providers: [
        {
          provide: RestService,
          useValue: restService
        },
        {
          provide: UserViewService,
          useValue: {
            componentData$: EMPTY
          }
        },
        {
          provide: DialogService,
          useValue: {},
        },
        {
          provide: USER_AUTHORISATIONS_TABLE_CONF,
          useFactory: userAuthorisationsTableDefinition,
        },
        {
          provide: USER_AUTHORISATIONS_TABLE,
          useFactory: gridServiceFactory,
          deps: [RestService, USER_AUTHORISATIONS_TABLE_CONF, ReferentialDataService]
        }

      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserAuthorisationsPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
