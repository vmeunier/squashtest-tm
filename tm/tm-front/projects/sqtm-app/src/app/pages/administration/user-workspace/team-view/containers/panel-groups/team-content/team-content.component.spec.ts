import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TeamContentComponent } from './team-content.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {DialogModule} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {TeamViewService} from '../../../services/team-view.service';
import {EMPTY} from 'rxjs';

describe('TeamContentComponent', () => {
  let component: TeamContentComponent;
  let fixture: ComponentFixture<TeamContentComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule, DialogModule],
      providers: [
        {
          provide: TranslateService,
          useValue: jasmine.createSpyObj('TranslateService', ['instant']),
        },
        {
          provide: TeamViewService,
          useValue: {
            componentData$: EMPTY
          }
        },
      ],
      declarations: [ TeamContentComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
