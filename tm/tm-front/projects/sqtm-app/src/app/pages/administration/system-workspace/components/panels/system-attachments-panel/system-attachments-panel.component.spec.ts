import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SystemAttachmentsPanelComponent } from './system-attachments-panel.component';
import {EMPTY} from 'rxjs';
import {SystemViewService} from '../../../services/system-view.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('SystemAttachmentsPanelComponent', () => {
  let component: SystemAttachmentsPanelComponent;
  let fixture: ComponentFixture<SystemAttachmentsPanelComponent>;

  const systemViewService = jasmine.createSpyObj(['load']);
  systemViewService['componentData$'] = EMPTY;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SystemAttachmentsPanelComponent ],
      providers: [
        {
          provide: SystemViewService,
          useValue: systemViewService,
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemAttachmentsPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
