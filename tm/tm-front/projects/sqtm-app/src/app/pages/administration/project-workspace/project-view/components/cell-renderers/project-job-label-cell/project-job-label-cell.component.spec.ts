import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {ProjectJobLabelCellComponent} from './project-job-label-cell.component';
import {DialogService, grid, GridDefinition, GridService, gridServiceFactory, ReferentialDataService, RestService} from 'sqtm-core';
import {ProjectViewService} from '../../../services/project-view.service';

describe('ProjectJobLabelCellComponent', () => {
  let component: ProjectJobLabelCellComponent;
  let fixture: ComponentFixture<ProjectJobLabelCellComponent>;

  const gridConfig = grid('grid-test').build();
  const restService = {};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ProjectJobLabelCellComponent],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig
        }, {
          provide: RestService,
          useValue: restService
        }, {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        }, {
          provide: ProjectViewService,
          useValue: {},
        }, {
          provide: DialogService,
          useValue: {}
        },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectJobLabelCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
