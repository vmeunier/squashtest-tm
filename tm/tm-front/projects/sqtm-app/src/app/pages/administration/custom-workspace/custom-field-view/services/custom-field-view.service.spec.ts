import {TestBed} from '@angular/core/testing';

import {CustomFieldViewService} from './custom-field-view.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {CustomField, RestService} from 'sqtm-core';
import {of} from 'rxjs';
import {AdminCustomFieldState} from '../states/admin-custom-field-state';
import {take, withLatestFrom} from 'rxjs/operators';
import {mockRestService} from '../../../../../utils/testing-utils/mocks.service';

describe('CustomFieldViewService', () => {

  const restService = mockRestService();

  let service: CustomFieldViewService;

  beforeEach(async (done) => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule, TranslateModule.forRoot(), RouterTestingModule],
      providers: [
        {
          provide: RestService,
          useValue: restService
        },
        {
          provide: CustomFieldViewService,
          useClass: CustomFieldViewService
        }
      ]
    });

    restService.get.and.returnValue(of(getDropdownListInitialState()));
    service = TestBed.get(CustomFieldViewService);
    service.load(1);
    done();
  });

  it('should load initial data', async (done) => {
    service.componentData$.subscribe(data => {
      expect(data.customField.id).toEqual(1);
      done();
    });
  });

  it('should change custom field default value', (done) => {
    service = TestBed.get(CustomFieldViewService);
    const cuf = getDropdownListInitialState();
    cuf.defaultValue = 'default1';

    restService.get.and.returnValue(of(cuf));
    service.load(1);
    service.changeDefaultValue('default2').subscribe(() => {
      service.componentData$.pipe(
        take(1)
      ).subscribe(data => {
        expect(data.customField.defaultValue).toEqual('default2');
        done();
      });
    });
  });

  it('should add option', (done) => {
    service = TestBed.get(CustomFieldViewService);
    const cuf = getDropdownListInitialState();
    cuf.options = [];
    const option = {label: 'option1', code: 'code1', colour: 'black'};
    const serverResponse: CustomField = {
      code: 'code1',
      id: 1,
      inputType: undefined,
      label: 'cuf1',
      name: 'cuf1',
      optional: false,
      options: [{cfId: 1, label: 'option1', code: 'code1', colour: 'black', position: 0}],
    };

    restService.get.and.returnValue(of(cuf));
    service.load(1);
    restService.post.and.returnValue(of(serverResponse));
    service.addOption(option).subscribe(() => {
      service.componentData$.pipe(
        take(1)
      ).subscribe(data => {
        expect(data.customField.options.length).toEqual(1);
        done();
      });
    });
  });

  it('should delete option', (done) => {
    service = TestBed.get(CustomFieldViewService);
    const cuf = getDropdownListInitialState();

    restService.get.and.returnValue(of(cuf));
    service.load(1);
    service.deleteOptions(['option1']).subscribe(() => {
      service.componentData$.pipe(
        take(1)
      ).subscribe(data => {
        expect(data.customField.options.length).toEqual(1);
        expect(data.customField.options[0].label).toEqual('option2');
        done();
      });
    });
  });

  it('should change option label', (done) => {
    service = TestBed.get(CustomFieldViewService);
    const cuf = getDropdownListInitialState();

    restService.get.and.returnValue(of(cuf));
    service.load(1);
    service.changeOptionLabel('option1', 'option10').subscribe(() => {
      service.componentData$.pipe(
        take(1)
      ).subscribe(data => {
        expect(data.customField.options[0].label).toEqual('option10');
        done();
      });
    });
  });

  it('should change option code', (done) => {
    service = TestBed.get(CustomFieldViewService);
    const cuf = getDropdownListInitialState();

    restService.get.and.returnValue(of(cuf));
    service.load(1);
    service.changeOptionCode('option1', 'code2').subscribe(() => {
      service.componentData$.pipe(
        take(1)
      ).subscribe(data => {
        expect(data.customField.options[0].code).toEqual('code2');
        done();
      });
    });
  });

  it('should change option color', (done) => {
    service = TestBed.get(CustomFieldViewService);
    const cuf = getDropdownListInitialState();

    restService.get.and.returnValue(of(cuf));
    service.load(1);
    service.changeOptionColor('option1', 'blue');
    service.componentData$.pipe(
      take(1)
    ).subscribe(data => {
      expect(data.customField.options[0].colour).toEqual('blue');
      done();
    });
  });

  it('change dropdown list option positions', (done) => {
    service = TestBed.get(CustomFieldViewService);
    const cuf = getDropdownListInitialState();
    cuf.options = [
      {cfId: 1, label: 'label1', code: 'code1', colour: null, position: 0},
      {cfId: 1, label: 'label2', code: 'code2', colour: null, position: 1},
      {cfId: 1, label: 'label3', code: 'code3', colour: null, position: 2},
      {cfId: 1, label: 'label4', code: 'code4', colour: null, position: 3}
    ];

    restService.get.and.returnValue(of(cuf));
    service.load(1);

    restService.post.and.returnValue(of({
      ...cuf,
      options: [
        {...cuf.options[1], position: 0},
        {...cuf.options[3], position: 1},
        {...cuf.options[0], position: 2},
        {...cuf.options[2], position: 3},
      ]
    }));

    service.changeOptionsPosition(['label1', 'label3'], 1).pipe(
      withLatestFrom(service.componentData$),
    ).subscribe(([, data]) => {
      expect(data.customField.options.map((opt) => opt.label))
        .toEqual(['label2', 'label4', 'label1', 'label3']);
      done();
    });
  });

  it('should set custom field optional', (done) => {
    service = TestBed.get(CustomFieldViewService);
    const cuf = getDropdownListInitialState();
    restService.get.and.returnValue(of(cuf));
    service.load(1);

    restService.post.and.returnValue(of({
      ...cuf,
      optional: true,
    }));

    service.toggleOptional(true).pipe(
      withLatestFrom(service.componentData$),
    ).subscribe(([, data]) => {
      expect(data.customField.optional).toBeTruthy();
      done();
    });
  });
});

function getDropdownListInitialState(): AdminCustomFieldState {
  return {
    code: 'code1',
    id: 1,
    inputType: undefined,
    label: 'cuf1',
    name: 'cuf1',
    optional: false,
    options: [
      {cfId: 1, label: 'option1', code: 'code1', colour: 'black', position: 0},
      {cfId: 1, label: 'option2', code: 'code2', colour: 'white', position: 1}
    ],
    attachmentList: {
      attachments: null,
      id: null,
    }
  };
}
