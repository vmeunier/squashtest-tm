import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SystemPluginsPanelComponent } from './system-plugins-panel.component';
import {SystemViewState} from '../../../states/system-view.state';

describe('SystemPluginsPanelComponent', () => {
  let component: SystemPluginsPanelComponent;
  let fixture: ComponentFixture<SystemPluginsPanelComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SystemPluginsPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemPluginsPanelComponent);
    component = fixture.componentInstance;
    component.componentData = {
      statistics: {
        databaseSize: 0, executionsNumber: 0, iterationsNumber: 0, campaignsNumber: 0,
        testCasesNumber: 0, requirementsNumber: 0, usersNumber: 0, projectsNumber: 0,
        campaignIndexingDate: null, requirementIndexingDate: null, testcaseIndexingDate: null,
      },
      appVersion: 'SquashTM version',
      plugins: [],
    } as SystemViewState;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
