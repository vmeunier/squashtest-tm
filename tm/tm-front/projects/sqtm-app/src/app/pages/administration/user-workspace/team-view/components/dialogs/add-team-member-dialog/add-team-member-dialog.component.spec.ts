import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddTeamMemberDialogComponent } from './add-team-member-dialog.component';
import {
  DialogReference,
  grid,
  GridDefinition,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService
} from 'sqtm-core';
import {ReactiveFormsModule} from '@angular/forms';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {TeamViewService} from '../../../services/team-view.service';
import {of} from 'rxjs';
import {AdminTeamState} from '../../../states/admin-team-state';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {mockRestService} from '../../../../../../../utils/testing-utils/mocks.service';

describe('AddTeamMemberDialogComponent', () => {
  let component: AddTeamMemberDialogComponent;
  let fixture: ComponentFixture<AddTeamMemberDialogComponent>;
  const restService = mockRestService();
  const gridConfig = grid('grid-test').build();

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddTeamMemberDialogComponent ],
      imports: [AppTestingUtilsModule, ReactiveFormsModule, TranslateModule.forRoot()],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig
        },
        {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        },
        {
          provide: RestService,
          useValue: restService
        },
        {
          provide: TeamViewService,
          useValue: {
            componentData$: of({team: {id: -1} as AdminTeamState, type: 'team', uiState: null}),
          }
        },
        {
          provide: DialogReference,
          useValue: jasmine.createSpyObj(['close'])
        },
        TranslateService
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    restService.get.and.returnValue(of([]));
    fixture = TestBed.createComponent(AddTeamMemberDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
