import {CustomFieldOption, InputType, SqtmGenericEntityState} from 'sqtm-core';

export interface AdminCustomFieldState extends SqtmGenericEntityState {
  id: number;
  code: string;
  label: string;
  name: string;
  defaultValue?: string;
  largeDefaultValue?: string;
  numericDefaultValue?: number;
  inputType: InputType;
  optional: boolean;
  options: CustomFieldOption[];
}
