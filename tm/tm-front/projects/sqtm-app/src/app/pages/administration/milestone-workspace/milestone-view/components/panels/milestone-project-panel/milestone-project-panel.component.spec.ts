import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {
  MILESTONE_PROJECTS_TABLE,
  MILESTONE_PROJECTS_TABLE_CONF,
  MilestoneProjectPanelComponent,
  milestoneProjectsTableDefinition
} from './milestone-project-panel.component';
import {DialogService, gridServiceFactory, ReferentialDataService, RestService} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {MilestoneViewService} from '../../../services/milestone-view.service';
import {mockRestService} from '../../../../../../../utils/testing-utils/mocks.service';

describe('MilestoneProjectPanelComponent', () => {
  let component: MilestoneProjectPanelComponent;
  let fixture: ComponentFixture<MilestoneProjectPanelComponent>;

  const restService = mockRestService();

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule, TranslateModule.forRoot(), RouterTestingModule],
      declarations: [MilestoneProjectPanelComponent],
      providers: [
        {
          provide: RestService,
          useValue: restService
        },
        {
          provide: MilestoneViewService,
          useClass: MilestoneViewService
        },
        {
          provide: DialogService,
          useValue: {},
        },
        {
          provide: MILESTONE_PROJECTS_TABLE_CONF,
          useFactory: milestoneProjectsTableDefinition,
        },
        {
          provide: MILESTONE_PROJECTS_TABLE,
          useFactory: gridServiceFactory,
          deps: [RestService, MILESTONE_PROJECTS_TABLE_CONF, ReferentialDataService]
        }

      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MilestoneProjectPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
