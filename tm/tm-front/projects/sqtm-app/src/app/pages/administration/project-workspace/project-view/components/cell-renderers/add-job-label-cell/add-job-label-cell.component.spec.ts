import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {AddJobLabelCellComponent} from './add-job-label-cell.component';
import {
  DataRow,
  DialogService,
  grid,
  GridDefinition,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService,
} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';

describe('AddJobLabelCellComponent', () => {
  let component: AddJobLabelCellComponent;
  let fixture: ComponentFixture<AddJobLabelCellComponent>;

  const gridConfig = grid('grid-test').build();
  const restService = {};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule],
      declarations: [AddJobLabelCellComponent],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig
        }, {
          provide: RestService,
          useValue: restService
        }, {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        }, {
          provide: DialogService,
          userValue: {}
        }, {
          provide: DialogService,
          userValue: {}
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddJobLabelCellComponent);
    component = fixture.componentInstance;
    component.row = {
      data: {}
    } as DataRow;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
