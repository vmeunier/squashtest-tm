import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SystemStackTraceManagementPanelComponent } from './system-stack-trace-management-panel.component';
import {SystemViewService} from '../../../services/system-view.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {EMPTY} from 'rxjs';

describe('SystemStackTraceManagementPanelComponent', () => {
  let component: SystemStackTraceManagementPanelComponent;
  let fixture: ComponentFixture<SystemStackTraceManagementPanelComponent>;

  const systemViewService = jasmine.createSpyObj(['load']);
  systemViewService['componentData$'] = EMPTY;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SystemStackTraceManagementPanelComponent ],
      providers: [
        {
          provide: SystemViewService,
          useValue: systemViewService,
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemStackTraceManagementPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
