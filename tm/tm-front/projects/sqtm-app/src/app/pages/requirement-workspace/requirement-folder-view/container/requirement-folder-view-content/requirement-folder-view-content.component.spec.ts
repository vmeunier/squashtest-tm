import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {RequirementFolderViewContentComponent} from './requirement-folder-view-content.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {EMPTY} from 'rxjs';
import {RequirementFolderViewService} from '../../services/requirement-folder-view.service';
import SpyObj = jasmine.SpyObj;
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('RequirementFolderViewContentComponent', () => {
  let component: RequirementFolderViewContentComponent;
  let fixture: ComponentFixture<RequirementFolderViewContentComponent>;

  const requirementFolderViewService: SpyObj<RequirementFolderViewService> = jasmine.createSpyObj('requirementFolderViewService', ['load']);
  requirementFolderViewService.componentData$ = EMPTY;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, HttpClientTestingModule],
      declarations: [RequirementFolderViewContentComponent],
      providers: [
        {
          provide: RequirementFolderViewService,
          useValue: requirementFolderViewService
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequirementFolderViewContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
