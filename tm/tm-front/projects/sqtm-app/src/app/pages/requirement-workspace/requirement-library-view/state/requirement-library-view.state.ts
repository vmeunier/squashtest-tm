import {EntityViewState, provideInitialViewState} from 'sqtm-core';
import {RequirementLibraryState} from './requirement-library.state';

export interface RequirementLibraryViewState extends EntityViewState<RequirementLibraryState, 'requirementLibrary'> {
  requirementLibrary: RequirementLibraryState;
}

export function provideInitialRequirementLibraryView(): Readonly<RequirementLibraryViewState> {
  return provideInitialViewState<RequirementLibraryState, 'requirementLibrary'>('requirementLibrary');
}
