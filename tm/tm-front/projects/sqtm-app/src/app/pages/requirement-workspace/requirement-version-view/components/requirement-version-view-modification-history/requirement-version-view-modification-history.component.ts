import {AfterViewInit, ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {
  dateTimeColumn,
  Extendable,
  grid,
  GridDefinition,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService,
  StyleDefinitionBuilder,
  textColumn
} from 'sqtm-core';
import {
  RVW_MODIFICATION_HISTORY_TABLE,
  RVW_MODIFICATION_HISTORY_TABLE_CONF
} from '../../requirement-version-view.constant';
import {take} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {RequirementVersionViewService} from '../../services/requirement-version-view.service';
import {
  eventColumn,
  ModificationHistoryEventComponent
} from '../cell-renderers/modification-history-event/modification-history-event.component';
import {
  ModificationHistoryChangedValueComponent,
  valueColumn
} from '../cell-renderers/modification-history-changed-value/modification-history-changed-value.component';

@Component({
  selector: 'sqtm-app-requirement-version-view-modification-history',
  template: `
    <ng-container>
      <div class="flex-column full-height full-width p-r-15" style="overflow-y: hidden;">
        <div class="history-title">
          {{'sqtm-core.requirement-workspace.title.modification-history' | translate}}
        </div>
        <div class="history-container full-width full-height p-15">
          <sqtm-core-grid></sqtm-core-grid>
        </div>
      </div>
    </ng-container>
  `,
  styleUrls: ['./requirement-version-view-modification-history.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: RVW_MODIFICATION_HISTORY_TABLE_CONF,
      useFactory: rvwModificationHistoryTableDefinition
    },
    {
      provide: RVW_MODIFICATION_HISTORY_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, RVW_MODIFICATION_HISTORY_TABLE_CONF, ReferentialDataService]
    },
    {
      provide: GridService,
      useExisting: RVW_MODIFICATION_HISTORY_TABLE
    }
  ]
})
export class RequirementVersionViewModificationHistoryComponent implements OnInit, OnDestroy, AfterViewInit {
  unsub$ = new Subject<void>();

  constructor(private gridService: GridService,
              private requirementVersionViewService: RequirementVersionViewService) {
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.requirementVersionViewService.componentData$.pipe(
      take(1)
    ).subscribe(componentData => {
      this.gridService.setServerUrl([`requirement-version/${componentData.requirementVersion.id}/history`]);
    });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
    this.gridService.complete();
  }

}

export function rvwModificationHistoryTableDefinition(): GridDefinition {
  return grid('req-version-modification-history')
    .withColumns([
      dateTimeColumn('date')
        .changeWidthCalculationStrategy(new Extendable(100))
        .withI18nKey('sqtm-core.entity.requirement.requirement-version.modification-history.date'),
      textColumn('user')
        .withI18nKey('sqtm-core.entity.user.label.singular'),
      eventColumn(ModificationHistoryEventComponent)
        .withI18nKey('sqtm-core.entity.requirement.requirement-version.modification-history.event'),
      valueColumn(ModificationHistoryChangedValueComponent, 'oldValue')
        .withI18nKey('sqtm-core.entity.requirement.requirement-version.modification-history.old-value'),
      valueColumn(ModificationHistoryChangedValueComponent, 'newValue')
        .withI18nKey('sqtm-core.entity.requirement.requirement-version.modification-history.new-value')
    ]).server()
    .disableRightToolBar()
    .withStyle(new StyleDefinitionBuilder().showLines())
    .withRowHeight(35)
    .build();
}
