import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {
  ImportRequirementState,
  ImportRequirementSteps,
  initialImportRequirementState,
  XlsReport
} from '../state/import-requirement.state';
import {concatMap, map, take, tap, withLatestFrom} from 'rxjs/operators';
import {createStore, ProjectData, ReferentialDataService, RestService, Store} from 'sqtm-core';

@Injectable({
  providedIn: 'root'
})
export class ImportRequirementService {

  store: Store<ImportRequirementState>;

  state$: Observable<ImportRequirementState>;

  constructor(private restService: RestService, private referentialData: ReferentialDataService) {
    const initialState = initialImportRequirementState();
    this.store = createStore(initialState);
    this.state$ = this.store.state$;
    this.initializeComponentData();
  }

  private initializeComponentData() {
    this.state$.pipe(
      take(1),
      withLatestFrom(this.referentialData.projectsManaged$),
      map(([state, projects]: [ImportRequirementState, ProjectData[]]) => {
        const componentData: ImportRequirementState = {
          ...state,
          projects,
          selectedProject: projects[0].id
        };
        return componentData;
      }),
      tap(state => this.store.commit(state))
    ).subscribe();
  }

  restoreState() {
    this.state$.pipe(
      take(1),
      map(state => initialImportRequirementState())
    ).subscribe(state => this.store.commit(state));
  }

  saveFile(file: File) {
    this.state$.pipe(
      take(1),
      map((state: ImportRequirementState) => {
        return {...state, file};
      })
    ).subscribe(state => this.store.commit(state));
  }

  simulateImport(file: File) {
    this.state$.pipe(
      take(1),
      map((state: ImportRequirementState) => this.changeCurrentStep(state, 'SIMULATION_REPORT')),
      map((state: ImportRequirementState) => this.beginAsync(state)),
      concatMap((state: ImportRequirementState) => this.doXlsImport(state, file, true)),
      map((state: ImportRequirementState) => this.endAsync(state))
    ).subscribe(state => this.store.commit(state));
  }

  goToConfirmationImportPage() {
    this.state$.pipe(
      take(1),
      map((state: ImportRequirementState) => this.changeCurrentStep(state, 'CONFIRMATION')),
    ).subscribe(state => this.store.commit(state));
  }

  private changeCurrentStep(state: ImportRequirementState, step: ImportRequirementSteps) {
    const newState = {...state, currentStep: step};
    this.store.commit(newState);
    return newState;
  }

  private beginAsync(state: ImportRequirementState) {
    const newState = {...state, loadingData: true};
    this.store.commit(newState);
    return state;
  }

  private endAsync(state: ImportRequirementState) {
    const newState = {...state, loadingData: false};
    this.store.commit(newState);
    return state;
  }

  private doXlsImport(state: ImportRequirementState, file: File, simulation?: boolean) {
    const formData = new FormData();
    formData.append('archive', file, file.name);
    if (simulation) {
      formData.append('dry-run', 'true');
    }
    return this.restService.post<XlsReport>([`requirement/importer/xls`], formData).pipe(
      map((res: XlsReport) => {
        const importFailed = (res.importFormatFailure != null);
        if (simulation) {
          return {...state, simulationReport: res, importFailed};
        }
        return {...state, xlsReport: res, importFailed};
      })
    );
  }

  getDownLoadReportUrl(reportUrl: string) {
    return `${this.restService.backendRootUrl}${reportUrl}`;
  }

  importRequirementXls(): Observable<any> {
    return this.state$.pipe(
      take(1),
      map((state: ImportRequirementState) => this.changeCurrentStep(state, 'CONFIRMATION_REPORT')),
      map((state: ImportRequirementState) => this.beginAsync(state)),
      concatMap((state: ImportRequirementState) => this.doXlsImport(state, state.file)),
      map((state: ImportRequirementState) => this.endAsync(state)),
      tap(state => this.store.commit(state))
    );
  }
}
