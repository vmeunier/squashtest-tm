import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {RequirementVersionStatsBundle} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-requirement-version-view-rate-panel',
  templateUrl: './requirement-version-view-rate-panel.component.html',
  styleUrls: ['./requirement-version-view-rate-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequirementVersionViewRatePanelComponent implements OnInit {

  @Input()
  stats: RequirementVersionStatsBundle;

  constructor() {
  }

  ngOnInit(): void {
  }

}
