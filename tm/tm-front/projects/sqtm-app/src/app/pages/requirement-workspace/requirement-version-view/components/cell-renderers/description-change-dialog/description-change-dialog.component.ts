import {ChangeDetectionStrategy, Component, HostListener, OnInit} from '@angular/core';
import {DialogReference, KeyNames} from 'sqtm-core';
// tslint:disable-next-line:max-line-length
import {DescriptionChangeDialogConfiguration} from './description-change-dialog-configuration';

@Component({
  selector: 'sqtm-app-alert-requirement-version-description-change-dialog',
  templateUrl: './description-change-dialog.component.html',
  styleUrls: ['./description-change-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DescriptionChangeDialogComponent implements OnInit {
  configuration: DescriptionChangeDialogConfiguration;

  constructor(private dialogReference: DialogReference<DescriptionChangeDialogConfiguration, boolean>) {
    this.configuration = dialogReference.data;
  }

  ngOnInit(): void {
  }

  @HostListener('window:keyup', ['$event'])
  handleKeyUp(event: KeyboardEvent) {
    if (event.key === KeyNames.ENTER || event.key === KeyNames.ESCAPE) {
      this.dialogReference.close();
    }
  }
}
