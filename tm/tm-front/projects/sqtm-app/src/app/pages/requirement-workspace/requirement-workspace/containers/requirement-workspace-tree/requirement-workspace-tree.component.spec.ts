import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {RequirementWorkspaceTreeComponent} from './requirement-workspace-tree.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {gridServiceFactory, ReferentialDataService, RestService} from 'sqtm-core';
import {REQ_WS_TREE, REQ_WS_TREE_CONFIG} from '../../requirement-workspace.constant';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {reqTreeConfigFactory} from '../requirement-workspace/requirement-workspace.component';
import {RouterTestingModule} from '@angular/router/testing';
import {OverlayModule} from '@angular/cdk/overlay';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';

describe('RequirementWorkspaceTreeComponent', () => {
  let component: RequirementWorkspaceTreeComponent;
  let fixture: ComponentFixture<RequirementWorkspaceTreeComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [RequirementWorkspaceTreeComponent],
      imports: [AppTestingUtilsModule, HttpClientTestingModule, RouterTestingModule, OverlayModule, NzDropDownModule],
      providers: [
        {provide: REQ_WS_TREE_CONFIG, useFactory: reqTreeConfigFactory},
        {
          provide: REQ_WS_TREE,
          useFactory: gridServiceFactory,
          deps: [RestService, REQ_WS_TREE_CONFIG, ReferentialDataService]
        },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequirementWorkspaceTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
