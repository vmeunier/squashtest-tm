import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {ImportLog} from '../../../../test-case-workspace/import-test-case/state/import-test-case.state';
import {ImportRequirementService} from '../../services/import-requirement.service';
import {ImportRequirementState} from '../../state/import-requirement.state';

@Component({
  selector: 'sqtm-app-import-report',
  templateUrl: './import-report.component.html',
  styleUrls: ['./import-report.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImportReportComponent implements OnInit {

  @Input()
  componentData: ImportRequirementState;

  constructor(private importRequirementService: ImportRequirementService) {
  }

  ngOnInit(): void {
  }

  getFileName(templateOk: ImportLog) {
    return this.importRequirementService.getDownLoadReportUrl(templateOk.reportUrl);
  }
}
