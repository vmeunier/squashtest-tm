import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {RequirementLibraryViewContentComponent} from './requirement-library-view-content.component';
import {EMPTY} from 'rxjs';
import {RequirementLibraryViewService} from '../../service/requirement-library-view.service';
import SpyObj = jasmine.SpyObj;
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('RequirementLibraryViewContentComponent', () => {
  let component: RequirementLibraryViewContentComponent;
  let fixture: ComponentFixture<RequirementLibraryViewContentComponent>;
  const requirementLibraryViewService: SpyObj<RequirementLibraryViewService> =
    jasmine.createSpyObj('requirementLibraryViewService', ['load']);
  requirementLibraryViewService.componentData$ = EMPTY;
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, HttpClientTestingModule],
      declarations: [ RequirementLibraryViewContentComponent ],
      providers: [{provide: RequirementLibraryViewService, useValue: requirementLibraryViewService}]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequirementLibraryViewContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
