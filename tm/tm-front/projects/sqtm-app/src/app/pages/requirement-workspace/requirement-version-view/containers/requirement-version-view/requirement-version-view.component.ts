import {ChangeDetectionStrategy, Component, ContentChild, ElementRef, Renderer2, ViewContainerRef} from '@angular/core';
import {RequirementVersionViewService} from '../../services/requirement-version-view.service';
import {
  DialogService,
  DragAndDropService,
  EntityViewComponentData,
  ReferentialDataService,
  RequirementPermissions
} from 'sqtm-core';
import {RequirementVersionState} from '../../state/requirement-version-view.state';
import {TranslateService} from '@ngx-translate/core';
import {AbstractRequirementVersionView} from '../abstract-requirement-version-view';

@Component({
  selector: 'sqtm-app-requirement-version-view',
  templateUrl: './requirement-version-view.component.html',
  styleUrls: ['./requirement-version-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequirementVersionViewComponent extends AbstractRequirementVersionView {

  @ContentChild('entityProjection')
  entityProjection: ElementRef;

  constructor(requirementViewService: RequirementVersionViewService,
              translateService: TranslateService,
              dndService: DragAndDropService,
              renderer: Renderer2,
              referentialDataService: ReferentialDataService,
              dialogService: DialogService,
              vcr: ViewContainerRef) {
    super(requirementViewService, translateService, dndService, renderer, referentialDataService, dialogService, vcr);
  }
}

export interface RequirementVersionViewComponentData
  extends EntityViewComponentData<RequirementVersionState, 'requirementVersion', RequirementPermissions> {
}


