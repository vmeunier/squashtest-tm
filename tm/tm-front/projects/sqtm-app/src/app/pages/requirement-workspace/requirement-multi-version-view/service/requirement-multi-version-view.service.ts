import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {RequirementMultiVersionViewState} from '../state/requirement-multi-version-view.state';
import {createStore, Store} from 'sqtm-core';
import {map, take} from 'rxjs/operators';

@Injectable()
export class RequirementMultiVersionViewService {

  store: Store<RequirementMultiVersionViewState>;

  public componentData$: Observable<RequirementMultiVersionViewState>;

  constructor() {

    this.store = createStore({requirementId: null, requirementVersionId: null});

    this.componentData$ = this.store.state$;
  }

  load(id: number) {
    this.store.state$.pipe(
      take(1),
      map(state => {
        return {...state, requirementId: id};
      })
    ).subscribe(state => this.store.commit(state));
  }
}
