import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, Type} from '@angular/core';
import {AbstractCellRendererComponent, ColumnDefinitionBuilder, Extendable, GridService} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-modification-history',
  template: `
    <ng-container *ngIf="row">
      <div class="full-width full-height flex-column">
        <span
          style="margin: auto 0;"
          class="txt-ellipsis"
          nz-tooltip [sqtmCoreLabelTooltip]="displayString">{{displayString}}</span>
      </div>
    </ng-container>
  `,
  styleUrls: ['./modification-history-event.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModificationHistoryEventComponent extends AbstractCellRendererComponent implements OnInit {

  constructor(public grid: GridService,
              public cdRef: ChangeDetectorRef,
              private translator: TranslateService) {
    super(grid, cdRef);
  }

  ngOnInit(): void {
  }

  get displayString(): string {
    const event = this.row.data['event'];
    if (event == null) {
      return this.displaySynReqEvent();
    }
    const modificationPrefix = this.translator
      .instant('sqtm-core.entity.requirement.requirement-version.modification-history.action.modification');
    if (['category', 'criticality', 'name', 'reference', 'status', 'description'].includes(event)) {
      const target = this.translator.instant('sqtm-core.entity.requirement.requirement-version.modification-history.element.' + event);
      return modificationPrefix + ' ' + target;
    }
    return event;
  }

  private displaySynReqEvent() {
    if (this.row.data['syncReqCreationSource'] != null) {
      return this.translator.instant('sqtm-core.entity.requirement.requirement-version.modification-history.action.sync-creation');
    }
    if (this.row.data['syncReqUpdateSource'] != null) {
      return this.translator.instant('sqtm-core.entity.requirement.requirement-version.modification-history.action.sync-modification');
    }
    return '';
  }
}

export function eventColumn(renderer: Type<any>, id = 'event', label = 'event'): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(renderer)
    .withLabel(label)
    .changeWidthCalculationStrategy(new Extendable(100));
}
