import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {RequirementVersionViewContentComponent} from './requirement-version-view-content.component';
import {WorkspaceCommonModule} from 'sqtm-core';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {EMPTY} from 'rxjs';
import {RequirementVersionViewService} from '../../services/requirement-version-view.service';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('RequirementViewContentComponent', () => {
  let component: RequirementVersionViewContentComponent;
  let fixture: ComponentFixture<RequirementVersionViewContentComponent>;

  let requirementVersionViewService = jasmine.createSpyObj('requirementVersionViewService', ['load']);
  requirementVersionViewService = {...requirementVersionViewService, componentData$: EMPTY};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, TranslateModule.forRoot(), RouterTestingModule, WorkspaceCommonModule],
      declarations: [RequirementVersionViewContentComponent],
      providers: [{provide: RequirementVersionViewService, useValue: requirementVersionViewService}],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequirementVersionViewContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
