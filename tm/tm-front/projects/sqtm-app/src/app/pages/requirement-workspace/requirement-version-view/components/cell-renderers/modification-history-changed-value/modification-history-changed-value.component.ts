import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, Type} from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  DialogConfiguration,
  DialogService,
  Extendable,
  GridService,
  SystemRequirementCodeEnum
} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
// tslint:disable-next-line:max-line-length
import {DescriptionChangeDialogComponent} from '../description-change-dialog/description-change-dialog.component';

@Component({
  selector: 'sqtm-app-modification-history-changed-value',
  template: `
    <ng-container *ngIf="row">
      <div class="full-width full-height flex-row">
        <span
          style="margin: auto 0;"
          nz-tooltip
          [sqtmCoreLabelTooltip]="displayString"
          class="txt-ellipsis">{{displayString}}</span>
        <a style="margin: auto 0;"
           *ngIf="this.row.data['event']==='description'"
           (click)="displayShowChangeDialog()">{{displayUrl}}</a>
        <a style="margin: auto 0;"
           *ngIf="this.row.data['event']==null"
           target="_blank"
           [href]="displaySyncLink()">{{displaySyncUrl}}</a>
      </div>
    </ng-container>
  `,
  styleUrls: ['./modification-history-changed-value.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModificationHistoryChangedValueComponent extends AbstractCellRendererComponent implements OnInit {

  constructor(public grid: GridService,
              public cdr: ChangeDetectorRef,
              private translator: TranslateService,
              protected dialogService: DialogService) {
    super(grid, cdr);
  }

  ngOnInit(): void {
  }

  get displayString(): string {
    const event = this.row.data['event'];
    let target = this.row.data[this.columnDisplay.id];
    target = this.updateCategoryEventTarget(event, target);
    if (['criticality', 'status'].includes(event)) {
      return this.translator.instant(`sqtm-core.entity.requirement.${event}.${target}`);
    }
    if (event === 'category') {
      return Object.values(SystemRequirementCodeEnum).includes(target) ?
        this.translator.instant(`sqtm-core.entity.requirement.category.${target}`)
        : target;
    }
    if (['name', 'reference'].includes(event)) {
      return target;
    }
    return '';
  }

  get displaySyncUrl() {
    const event = this.row.data['event'];
    if (event == null) {
      return this.displaySynchronizedRequirementInfo(this.columnDisplay.id.toString());
    }
    return '';
  }

  get displayUrl() {
    const event = this.row.data['event'];
    if ('description' === event) {
      return this.getShowChangesLink(this.columnDisplay.id.toString());
    }
    return '';
  }

  private displaySynchronizedRequirementInfo(columnName: string) {
    if ('oldValue' === columnName) {
      return this.translator.instant('sqtm-core.entity.requirement.requirement-version.modification-history.sync-source');
    } else {
      return '';
    }
  }

  private getShowChangesLink(columnName: string) {
    if ('oldValue' === columnName) {
      return this.translator.instant('sqtm-core.entity.requirement.requirement-version.modification-history.show-change');
    } else {
      return '';
    }
  }

  private updateCategoryEventTarget(event, target) {
    if ('category' === event) {
      target = target.replace('requirement.category.', '');
    }
    return target;
  }

  displayShowChangeDialog() {
    const event = this.row.data['event'];
    if (event != null) {
      const dialogConfiguration: DialogConfiguration = {
        id: 'alert-requirement-version-description-changes',
        component: DescriptionChangeDialogComponent,
        width: 550,
        data: {
          titleKey: 'sqtm-core.entity.requirement.requirement-version.modification-history.detail',
          messageKey: 'sqtm-core.entity.requirement.requirement-version.modification-history.detail-warning',
          oldValue: this.row.data['oldValue'],
          newValue: this.row.data['newValue']
        }
      };
      this.dialogService.openDialog(dialogConfiguration);
    }
  }

  displaySyncLink() {
    const syncReqCreationSource = this.row.data['syncReqCreationSource'];
    if (syncReqCreationSource != null) {
      return syncReqCreationSource;
    }
    return this.row.data['syncReqUpdateSource'];
  }
}

export function valueColumn(renderer: Type<any>, id: string, label = 'value'): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(renderer)
    .withLabel(label)
    .changeWidthCalculationStrategy(new Extendable(150));
}
