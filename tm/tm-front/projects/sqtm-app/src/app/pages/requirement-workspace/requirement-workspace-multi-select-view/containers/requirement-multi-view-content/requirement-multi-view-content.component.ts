import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {RequirementMultiViewState} from '../../state/requirement-multi-view.state';
import {Observable} from 'rxjs';
import {RequirementMultiSelectionService} from '../../services/requirement-multi-selection.service';
import {CustomDashboardBinding, FavoriteDashboardValue, GridService, ReferentialDataService} from 'sqtm-core';
import {filter, switchMap, take} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-requirement-multi-view-content',
  templateUrl: './requirement-multi-view-content.component.html',
  styleUrls: ['./requirement-multi-view-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequirementMultiViewContentComponent implements OnInit {

  componentData$: Observable<RequirementMultiViewState>;

  constructor(public referentialDataService: ReferentialDataService, private viewService: RequirementMultiSelectionService, private tree: GridService) {
  }

  ngOnInit(): void {
    this.componentData$ = this.viewService.componentData$;
  }

  getStatisticsScope(componentData: RequirementMultiViewState) {
    return componentData.scope;
  }

  refreshStats($event: MouseEvent) {
    this.tree.selectedRows$.pipe(
      take(1),
      filter(rows => rows.length > 1),
    ).subscribe(rows => {
      this.viewService.init(rows);
    });
    $event.stopPropagation();
  }

  displayFavoriteDashboard($event: MouseEvent) {
    $event.stopPropagation();
    this.changeDashboardToDisplay('dashboard');
  }

  displayDefaultDashboard($event: MouseEvent) {
    $event.stopPropagation();
    this.changeDashboardToDisplay('default');
  }

  private changeDashboardToDisplay(preferenceValue: FavoriteDashboardValue) {
    this.viewService.changeDashboardToDisplay(preferenceValue)
      .pipe(
        take(1),
        switchMap(() => this.tree.selectedRows$),
        filter(rows => rows.length > 1),
      ).subscribe(rows => {
      this.viewService.init(rows);
    });
  }

  getChartBindings(dashboard: any) {
    return [...dashboard.chartBindings, ...dashboard.reportBindings] as CustomDashboardBinding[];
  }
}
