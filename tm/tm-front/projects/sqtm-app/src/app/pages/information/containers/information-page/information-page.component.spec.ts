import {ComponentFixture, TestBed} from '@angular/core/testing';

import {InformationPageComponent} from './information-page.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AdminReferentialDataService, DialogService, SquashPlatformNavigationService} from 'sqtm-core';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {EMPTY} from 'rxjs';

describe('InformationPageComponent', () => {
  let component: InformationPageComponent;
  let fixture: ComponentFixture<InformationPageComponent>;

  beforeEach(async () => {
    const adminReferentialDataService = jasmine.createSpyObj<AdminReferentialDataService>(['refresh']);
    adminReferentialDataService.refresh.and.returnValue(EMPTY);
    adminReferentialDataService.licenseInformation$ = EMPTY;

    await TestBed.configureTestingModule({
      declarations: [InformationPageComponent],
      providers: [
        {provide: AdminReferentialDataService, useValue: adminReferentialDataService},
        {provide: ActivatedRoute, useValue: {}},
        {provide: SquashPlatformNavigationService, useValue: {}},
        {provide: TranslateService, useValue: {}},
        {provide: DialogService, useValue: {}},
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InformationPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
