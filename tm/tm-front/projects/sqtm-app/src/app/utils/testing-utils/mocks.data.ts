import {
  BindableEntity,
  GlobalConfiguration,
  IterationModel,
  ProjectData,
  ProjectPermissions,
  WorkspaceTypeForPlugins
} from 'sqtm-core';

export function mockIterationModel(customData?: Partial<IterationModel>): IterationModel {
  const defaultModel: IterationModel = {
    id: 1,
    actualEndAuto: false,
    actualEndDate: undefined,
    actualStartAuto: false,
    actualStartDate: undefined,
    attachmentList: undefined,
    createdBy: '',
    createdOn: undefined,
    customFieldValues: [],
    description: '',
    hasDatasets: false,
    iterationStatus: '',
    itpi: [],
    lastModifiedBy: '',
    lastModifiedOn: undefined,
    name: '',
    projectId: 0,
    reference: '',
    scheduledEndDate: undefined,
    scheduledStartDate: undefined,
    testPlanStatistics: undefined,
    uuid: '',
    nbIssues: 0,
    milestones: [],
    users: [],
    testSuites: [],
    executionStatusMap: null,
    iterationStatisticsBundle: null,
    nbAutomatedSuites: 0,
    shouldShowFavoriteDashboard: false,
    canShowFavoriteDashboard: false,
    favoriteDashboardId: 0,
    dashboard: null,
    nbTestPlanItems: 0
  };

  return {
    ...defaultModel,
    ...(customData || {})
  };
}

export function mockGlobalConfiguration(customData?: Partial<GlobalConfiguration>): GlobalConfiguration {
  const defaultData: GlobalConfiguration = {
    uploadFileExtensionWhitelist: [],
    milestoneFeatureEnabled: false,
    uploadFileSizeLimit: 0,
  };

  return {
    ...defaultData,
    ...(customData || {})
  };
}

export function mockProjectData(customData?: Partial<ProjectData>): ProjectData {
  const defaultData: ProjectData = {
    id: 1,
    name: 'name',
    uri: 'uri',
    label: 'label',
    customFieldBinding: {
      [BindableEntity.CAMPAIGN]: [],
      [BindableEntity.EXECUTION_STEP]: [],
      [BindableEntity.EXECUTION]: [],
      [BindableEntity.CUSTOM_REPORT_FOLDER]: [],
      [BindableEntity.CAMPAIGN_FOLDER]: [],
      [BindableEntity.ITERATION]: [],
      [BindableEntity.REQUIREMENT_FOLDER]: [],
      [BindableEntity.REQUIREMENT_VERSION]: [],
      [BindableEntity.TEST_CASE]: [],
      [BindableEntity.TEST_STEP]: [],
      [BindableEntity.TEST_SUITE]: [],
      [BindableEntity.TESTCASE_FOLDER]: [],
    },
    permissions: {} as ProjectPermissions,
    bugTracker: null,
    milestones: [],
    taServer: null,
    automationWorkflowType: '',
    disabledExecutionStatus: [],
    bddScriptLanguage: '',
    keywords: [],
    testCaseNature: null,
    testCaseType: null,
    requirementCategory: null,
    allowAutomationWorkflow: false,
    allowTcModifDuringExec: false,
    activatedPlugins: {
      [WorkspaceTypeForPlugins.CAMPAIGN_WORKSPACE]: [],
      [WorkspaceTypeForPlugins.TEST_CASE_WORKSPACE]: [],
      [WorkspaceTypeForPlugins.REQUIREMENT_WORKSPACE]: [],
    }
  };

  return {
    ...defaultData,
    ...(customData || {})
  };
}
