/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.exception.execution;

import org.squashtest.tm.core.foundation.exception.ActionException;

/**
 * Indicates we try to modify an execution step witch has some steps we can't read.
 * It's tricky be cause this view is an aggregation of test cases in case of call test case.
 * And business want a custom error message so...
 *
 */
public class ModifDuringExecMissingReadPermissionException extends ActionException {

	/**
	 *
	 */
	private static final String MISSING_READ_PERMISSION = "sqtm-core.error.execution-step.no-read-permission";

	private static final long serialVersionUID = -8184446070355509532L;


	public ModifDuringExecMissingReadPermissionException() {
		super("No read permissions on at least one test case");
	}

	@Override
	public String getI18nKey() {
		return MISSING_READ_PERMISSION;
	}

}
