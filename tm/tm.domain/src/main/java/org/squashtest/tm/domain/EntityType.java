/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain;

import org.apache.commons.lang3.StringUtils;
import org.squashtest.tm.domain.actionword.ActionWordLibrary;
import org.squashtest.tm.domain.attachment.Attachment;
import org.squashtest.tm.domain.attachment.AttachmentList;
import org.squashtest.tm.domain.bdd.ActionWord;
import org.squashtest.tm.domain.bugtracker.Issue;
import org.squashtest.tm.domain.campaign.*;
import org.squashtest.tm.domain.chart.ChartDefinition;
import org.squashtest.tm.domain.customreport.CustomReportCustomExport;
import org.squashtest.tm.domain.customreport.CustomReportDashboard;
import org.squashtest.tm.domain.customreport.CustomReportFolder;
import org.squashtest.tm.domain.customreport.CustomReportLibrary;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStep;
import org.squashtest.tm.domain.infolist.InfoListItem;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.report.ReportDefinition;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.domain.requirement.RequirementFolder;
import org.squashtest.tm.domain.requirement.RequirementLibrary;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.domain.testautomation.AutomatedExecutionExtender;
import org.squashtest.tm.domain.testautomation.AutomatedTest;
import org.squashtest.tm.domain.testcase.*;
import org.squashtest.tm.domain.tf.automationrequest.AutomationRequest;
import org.squashtest.tm.domain.users.User;

import java.util.EnumSet;
import java.util.Optional;

public enum EntityType {

	// @formatter:off
	PROJECT(Project.class),
	TEST_CASE_LIBRARY(TestCaseLibrary.class),
	TEST_CASE_FOLDER(TestCaseFolder.class),
	TEST_CASE(TestCase.class),
	TEST_CASE_STEP(TestStep.class),
	REQUIREMENT_LIBRARY(RequirementLibrary.class),
	REQUIREMENT_FOLDER(RequirementFolder.class),
	REQUIREMENT(Requirement.class),
	REQUIREMENT_VERSION(RequirementVersion.class),
	CAMPAIGN_LIBRARY(CampaignLibrary.class),
	CAMPAIGN_FOLDER(CampaignFolder.class),
	CAMPAIGN(Campaign.class),
	ITERATION(Iteration.class),
	EXECUTION(Execution.class),
	TEST_SUITE(TestSuite.class),
	EXECUTION_STEP(ExecutionStep.class),
	TEST_STEP(TestStep.class),
	ISSUE(Issue.class),
	ITEM_TEST_PLAN(IterationTestPlanItem.class),
	INFO_LIST_ITEM(InfoListItem.class),
	USER(User.class),
	MILESTONE(Milestone.class),
	AUTOMATED_TEST(AutomatedTest.class),
	AUTOMATED_EXECUTION_EXTENDER(AutomatedExecutionExtender.class),
	ATTACHMENT(Attachment.class),
	ATTACHMENT_LIST(AttachmentList.class),
	AUTOMATION_REQUEST(AutomationRequest.class),
	DATASET(Dataset.class),
	PARAMETER(Parameter.class),
	CUSTOM_REPORT_LIBRARY(CustomReportLibrary.class),
	CUSTOM_REPORT_FOLDER(CustomReportFolder.class),
	CUSTOM_REPORT_DASHBOARD(CustomReportDashboard.class),
	CHART_DEFINITION(ChartDefinition.class),
	REPORT_DEFINITION(ReportDefinition.class),
	CUSTOM_REPORT_CUSTOM_EXPORT(CustomReportCustomExport.class),
	ACTION_WORD_LIBRARY(ActionWordLibrary.class),
	ACTION_WORD(ActionWord.class);

	// @formatter:on


	EntityType(Class clazz) {
		this.clazz = clazz;
	}

	private Class clazz;

	public Class getEntityClass() {
		return clazz;
	}

	public String getSimpleClassName() {
		return clazz.getSimpleName();
	}

	public static EntityType fromSimpleName(String simpleClassName) {
		if (StringUtils.isBlank(simpleClassName)) {
			throw new IllegalArgumentException("Simple Name can't be empty");
		}

		EnumSet<EntityType> entityTypes = EnumSet.allOf(EntityType.class);

		Optional<EntityType> type = entityTypes.stream()
			.filter(entityType -> entityType.getSimpleClassName().equals(simpleClassName))
			.findFirst();

		if (type.isPresent()) {
			return type.get();
		}

		throw new IllegalArgumentException("No Entity Type for simple class name : " + simpleClassName);
	}
}
