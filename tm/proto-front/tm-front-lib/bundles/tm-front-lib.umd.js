/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/common'), require('@angular/core')) :
    typeof define === 'function' && define.amd ? define('tm-front-lib', ['exports', '@angular/common', '@angular/core'], factory) :
    (factory((global['tm-front-lib'] = {}),global.ng.common,global.ng.core));
}(this, (function (exports,common,core) { 'use strict';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var ComponentAComponent = /** @class */ (function () {
        function ComponentAComponent() {
        }
        /**
         * @return {?}
         */
        ComponentAComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
            };
        ComponentAComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'sqtm-component-a',
                        template: "\n    <p>\n      component-a works!\n    </p>\n  ",
                        changeDetection: core.ChangeDetectionStrategy.OnPush,
                        styles: [""]
                    }] }
        ];
        /** @nocollapse */
        ComponentAComponent.ctorParameters = function () { return []; };
        return ComponentAComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var ExportedModule = /** @class */ (function () {
        function ExportedModule() {
        }
        ExportedModule.decorators = [
            { type: core.NgModule, args: [{
                        declarations: [ComponentAComponent],
                        imports: [
                            common.CommonModule
                        ],
                        exports: [ComponentAComponent]
                    },] }
        ];
        return ExportedModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    exports.ExportedModule = ExportedModule;
    exports.ComponentAComponent = ComponentAComponent;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=tm-front-lib.umd.js.map