/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
export { ExportedModule } from './exported.module';
export { ComponentAComponent } from './component/component-a/component-a.component';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290Ijoibmc6Ly90bS1mcm9udC1saWIvIiwic291cmNlcyI6WyJhcHAvdWkvZXhwb3J0ZWQvaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxjQUFjLEVBQUMsTUFBTSxtQkFBbUIsQ0FBQztBQUNqRCxPQUFPLEVBQUMsbUJBQW1CLEVBQUMsTUFBTSwrQ0FBK0MsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCB7RXhwb3J0ZWRNb2R1bGV9IGZyb20gJy4vZXhwb3J0ZWQubW9kdWxlJztcclxuZXhwb3J0IHtDb21wb25lbnRBQ29tcG9uZW50fSBmcm9tICcuL2NvbXBvbmVudC9jb21wb25lbnQtYS9jb21wb25lbnQtYS5jb21wb25lbnQnO1xyXG4iXX0=