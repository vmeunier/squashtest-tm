/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
import { CommonModule } from '@angular/common';
import { Component, ChangeDetectionStrategy, NgModule } from '@angular/core';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var ComponentAComponent = /** @class */ (function () {
    function ComponentAComponent() {
    }
    /**
     * @return {?}
     */
    ComponentAComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    ComponentAComponent.decorators = [
        { type: Component, args: [{
                    selector: 'sqtm-component-a',
                    template: "\n    <p>\n      component-a works!\n    </p>\n  ",
                    changeDetection: ChangeDetectionStrategy.OnPush,
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    ComponentAComponent.ctorParameters = function () { return []; };
    return ComponentAComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var ExportedModule = /** @class */ (function () {
    function ExportedModule() {
    }
    ExportedModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [ComponentAComponent],
                    imports: [
                        CommonModule
                    ],
                    exports: [ComponentAComponent]
                },] }
    ];
    return ExportedModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { ExportedModule, ComponentAComponent };

//# sourceMappingURL=tm-front-lib.js.map