/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
// export {BugTracker, BugTrackerFacade} from './app/features/global/bugtrackers';
// export {GridModule, GridComponent} from './app/ui/grid';
export { ExportedModule, ComponentAComponent } from './app/ui/exported/index';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHVibGljX2FwaS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3RtLWZyb250LWxpYi8iLCJzb3VyY2VzIjpbInB1YmxpY19hcGkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBRUEsT0FBTyxFQUFDLGNBQWMsRUFBRSxtQkFBbUIsRUFBQyxNQUFNLHlCQUF5QixDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiLy8gZXhwb3J0IHtCdWdUcmFja2VyLCBCdWdUcmFja2VyRmFjYWRlfSBmcm9tICcuL2FwcC9mZWF0dXJlcy9nbG9iYWwvYnVndHJhY2tlcnMnO1xyXG4vLyBleHBvcnQge0dyaWRNb2R1bGUsIEdyaWRDb21wb25lbnR9IGZyb20gJy4vYXBwL3VpL2dyaWQnO1xyXG5leHBvcnQge0V4cG9ydGVkTW9kdWxlLCBDb21wb25lbnRBQ29tcG9uZW50fSBmcm9tICcuL2FwcC91aS9leHBvcnRlZC9pbmRleCc7XHJcbiJdfQ==