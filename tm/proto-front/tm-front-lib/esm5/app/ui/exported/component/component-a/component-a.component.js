/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ChangeDetectionStrategy } from '@angular/core';
var ComponentAComponent = /** @class */ (function () {
    function ComponentAComponent() {
    }
    /**
     * @return {?}
     */
    ComponentAComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    ComponentAComponent.decorators = [
        { type: Component, args: [{
                    selector: 'sqtm-component-a',
                    template: "\n    <p>\n      component-a works!\n    </p>\n  ",
                    changeDetection: ChangeDetectionStrategy.OnPush,
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    ComponentAComponent.ctorParameters = function () { return []; };
    return ComponentAComponent;
}());
export { ComponentAComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LWEuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vdG0tZnJvbnQtbGliLyIsInNvdXJjZXMiOlsiYXBwL3VpL2V4cG9ydGVkL2NvbXBvbmVudC9jb21wb25lbnQtYS9jb21wb25lbnQtYS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsdUJBQXVCLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFM0U7SUFZRTtJQUFnQixDQUFDOzs7O0lBRWpCLHNDQUFROzs7SUFBUjtJQUNBLENBQUM7O2dCQWZGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsa0JBQWtCO29CQUM1QixRQUFRLEVBQUUsbURBSVQ7b0JBRUQsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07O2lCQUNoRDs7OztJQVFELDBCQUFDO0NBQUEsQUFqQkQsSUFpQkM7U0FQWSxtQkFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnc3F0bS1jb21wb25lbnQtYScsXG4gIHRlbXBsYXRlOiBgXG4gICAgPHA+XG4gICAgICBjb21wb25lbnQtYSB3b3JrcyFcbiAgICA8L3A+XG4gIGAsXG4gIHN0eWxlVXJsczogWycuL2NvbXBvbmVudC1hLmNvbXBvbmVudC5jc3MnXSxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2hcbn0pXG5leHBvcnQgY2xhc3MgQ29tcG9uZW50QUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgY29uc3RydWN0b3IoKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcbiAgfVxuXG59XG4iXX0=