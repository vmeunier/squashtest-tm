# TmFront

This project hold the front end angular app for Squash TM

## Structure of app directory

**app/root** => This folder is not a ngModule. It just contains a few store configuration, mainly store-router
binding.

**app/common** => This folder is not a ngModule 
  it contains all of our commons model, reducers... You don't have to import this as an angular module,
  it's just a plain bunch of typescript modules that are shared across application
  
**app/core** => This folder is just an NgModule referencing all global features, to avoid to overflow app module

**app/features** => This folder contains several NgModule, divided in two categories.

  ***app/features/global*** => Each module inside is responsible of a global feature. 
  By global feature i mean a feature in NgRx, so in fact each module is responsible for a slice of state. 
  These slices are root one, ie they are at first level in store. They mainly concern referential data like bugtrackers, infolists ...
  
  ***app/features/*** => All other folder are modules that can be combined in pages module as a slice of state.
  These features do not contains ui element but are local state to a page not to the root state. (ie they are at second level in state hierachy)
  
**app/pages** => Each module inside this folder represent a root page in the app. All the pages are lazy loaded modules.

**app/ui** => Each module inside this folder represent a reusable ui element. The more complex are also NgRx features. 
In that case, the page using the ui element is responsible for creating reducers and facades using the providers functions inside each complex ui module

## Plugin system

Squash TM has a pluggable architecture. A Plugin is a jar file, put in a prefined folder server side. When the backend (ie Spring Boot) start, it scan the plugin folder and extend the classpath with it's content.
Plugin are added at runtime.  
The core of Squash Tm doesn't know the plugin at compile time, it's just consume them at runtime. Some of these plugin have jsp and/or thymeleaf pages (ie front-ends), that must be migrated to the new Angular front-end.

So our angular platform must be able to :
1. Load plugins that are  unknown at compile time. So webpack doesn't known them...
2. Share components/services/module from the core, so any plugin can use them. For exemple a plugin must be able to display the nav bar and use the grid module...

We have two use case for our front end plugin :
1. Be able to inject component in existing page, in pre defined extension points.
2. Be able to extends the routes in Angular Router, so plugins can add new pages to application. These pages and the associated routes are configured at runtime. No list of routes or plugin or module exist when the core is built by angular-cli.

During prototyping several technical difficulties have been identified and first solution found. These solution can of course evolve.

For a plugin that must declare new routes :

1. How can the webpack packaged platform can load the plugin and import the plugin.  
    * Plugin are bundled with ng-packager.
    * At runtime the routes of plugins are read from server and added to the routes of Squash TM as lazy loaded modules.
    * To load these module, Squash TM override the class : SystemJsNgModuleLoader. This custom module loader is used just for plugin, all other modules are loaded by the original class.
    * Our custom module loader is located at tm-front\src\app\features\global\plugin\services\sqtm-module-loader.service.ts   
     A dedicated version of SystemJS is loaded by the core. All js module are aliased as new module to this SystemJS config.
    * The plugin javascript module is loaded at runtime by SystemJS.
    
2. How the plugin is included to angular platefom
    * The javascript module is given to the Angular compiler to compile it to an angular module. The NgFactory is returned and the module is loaded by the router as any other module...
    * In AOT mode, a second compiler is created into the app because the JIT compiler is not included in AOT builds. We can't just overide the existing compiler because angular loader use the main compiler to kwnon if it's JIT or AOT

3. How the plugin can use core components/services/elements from the core.
    * The core of the app can also be bundled as a library that export accessible module and component for the plugin. This lib can be installed by npm/yarn in the plugin code.
    * The core lib is added as peer dep to plugin, and is not included in plugin bundle.
    * When the angular compiler resolve the dependancies of the plugin NgModule, by parsing NgModule metadata, it will resolve the dep from the core.
    * **This trick doesn't work in AOT mode**, as the JIT compiler doesn't known the core NgModules as they are precompiled. So the resolution of plugin NgModule metadata fail...
    * In fact, it doesn't work in aot because the angular CLI is removing all decorators from @angular/core as soon as AOT is activated. So our module metadata are stripped from the code and the compiler can't retrieve them. See : [https://github.com/angular/angular-cli/issues/9306](https://github.com/angular/angular-cli/issues/9306)
      * We can hack this optimization by re-exporting the decorators from angular core as our own decorators. So the decorators are not stripped by the @ng-tools of the angular-cli. It probably led to the JIT compiler recompiling the module, witch will probably make two version of the module exist. If providers are preserved, we don't care...
      * We could make our own build process for the app using webpack or rollup. However it will require a huge amount of work...
      * We can accept to have just JIT and pay the performance hit. Several users have asked for a way to preserve these decorator, if one day the angular-cli team accept this request we will be able to make the switch to AOT without having to pollute our code with custom decorators.
      * We can fork the cli and just remove this build step : [https://github.com/angular/angular-cli/blob/a47f024b37858a93f022a5060411a41c2fb36414/packages/%40ngtools/webpack/src/transformers/remove_decorators.ts#L33-L37](https://github.com/angular/angular-cli/blob/a47f024b37858a93f022a5060411a41c2fb36414/packages/%40ngtools/webpack/src/transformers/remove_decorators.ts#L33-L37).  
      The transformer is called at this line : [https://github.com/angular/angular-cli/blob/5f73a75193bc1217cbfb7f2240355679517cc8d0/packages/%40ngtools/webpack/src/angular_compiler_plugin.ts#L682](https://github.com/angular/angular-cli/blob/5f73a75193bc1217cbfb7f2240355679517cc8d0/packages/%40ngtools/webpack/src/angular_compiler_plugin.ts#L682)

For a plugin that must inject components :

1. The process is nearly the same but instead of lazy load the module, we just eagerly load him. After compilation the first component factory is resolved and used to instantiate the component. This instantiated component is thus injected in the page. 
