///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {Directive, Inject, Input, OnDestroy, OnInit} from '@angular/core';
import {ThemeFacade} from '@features/global/theme/store/theme.facade';
import {DOCUMENT} from '@angular/common';
import {
  WorkspaceCssProperties,
  Theme,
  Workspaces,
  WorkspaceTheme,
  CssProperties,
  GlobalTheme
} from '@features/global/theme/store/theme.model';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Directive({
  selector: '[sqtmTheme]'
})
export class ThemeDirective implements OnInit, OnDestroy {

  @Input('sqtmTheme')
  workspaceKey: string;

  private unsub$ = new Subject();

  constructor(private themeFacade: ThemeFacade, @Inject(DOCUMENT) private document: any) {

  }

  ngOnInit(): void {
    this.themeFacade.selectedTheme$
      .pipe(takeUntil(this.unsub$))
      .subscribe(
        (theme: Theme) => {
          this.applyTheme(theme);
        }
      );
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  private applyTheme(theme: Theme) {
    this.applyGlobalTheme(theme);
    if (this.workspaceKey) {
      this.applyWorkspaceTheme(theme);
    }
  }

  private applyGlobalTheme(theme: Theme) {
    const globalTheme = theme['global-theme'];
    this.updateCssVariables(globalTheme);
    this.extractWorkspaceColors(theme);
  }

  private applyWorkspaceTheme(theme: Theme) {
    const workspaceTheme = theme[this.workspaceKey];
    if (workspaceTheme) {
      this.updateCssVariables(workspaceTheme);
    } else {
      throw new Error(`No key ${this.workspaceKey} defined in theme ${theme.id}`);
    }
  }

  private updateCssVariables(properties) {
    for (const key of Object.keys(properties)) {
      const cssVar = `--${key}`;
      this.document.body.style.setProperty(cssVar, properties[key]);
    }
  }

  private extractWorkspaceColors(theme: Theme) {
    const properties: CssProperties = {};
    for (const key of Object.keys(Workspaces)) {
      const workspaceTheme: WorkspaceTheme = theme[key];
      const colorKey = `${key}-main-color`;
      properties[colorKey] = workspaceTheme['main-color'];
    }
    this.updateCssVariables(properties);
  }
}
