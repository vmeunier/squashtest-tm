///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {Observable} from 'rxjs';
import {GridRequest, GridResponse} from '../store/model/data-row.model';
import {RestService} from '../../../common/services/rest.service';
import {Injectable} from '@angular/core';

@Injectable({providedIn: 'root'})
export class GridService {

  constructor(private restService: RestService) {
  }

  getData(url: string [], gridRequest: GridRequest): Observable<GridResponse> {
    return this.restService.get(url, {gridRequest: 1, ...gridRequest});
  }

  getInitialRowsFromServer(url: string []): Observable<GridResponse> {
    return this.restService.get(url);
  }

  getChildrenFromServer(url: string [], id: string): Observable<GridResponse> {
    return this.restService.get([...url, id, 'content']);
  }
}
