///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {ComponentAction} from './../../../../common/store/factories/actions/actions-type';
import {PaginationState} from '../reducers/state/pagination.state';
import {GridConfigState} from '../reducers/state/config.state';
import {ColumnsDefinitionState} from '@ui/grid/store/reducers/state/columns-definition.state';

export enum PaginationActionTypes {
  NextPage = '[Grid][Pagination] Next Page',
  PreviousPage = '[Grid][Pagination] Previous Page',
  ChangePageSize = '[Grid][Pagination] Change Page Size',
  RefreshDataFromServer = '[Grid][Pagination] Refresh Data From Server',
}

export class NextPage implements ComponentAction {
  readonly type = PaginationActionTypes.NextPage;

  constructor(public pagePrefix: string, public componentId: string) {
  }
}

export class PreviousPage implements ComponentAction {
  readonly type = PaginationActionTypes.PreviousPage;

  constructor(public pagePrefix: string, public componentId: string) {
  }
}

export class ChangePageSize implements ComponentAction {
  readonly type = PaginationActionTypes.ChangePageSize;

  constructor(public pagePrefix: string, public componentId: string, public payload: { pageSize: number }) {
  }
}

export class RefreshDataFromServer implements ComponentAction {
  readonly type = PaginationActionTypes.RefreshDataFromServer;

  constructor(public pagePrefix: string,
              public componentId: string,
              public payload: { pagination: PaginationState, config: GridConfigState, columnDefinitions: ColumnsDefinitionState }) {
  }
}

export type PaginationActions =
  NextPage
  | PreviousPage
  | ChangePageSize
  | RefreshDataFromServer;
