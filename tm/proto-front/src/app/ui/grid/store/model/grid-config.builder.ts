///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {GridDataProvider, GridType} from '../reducers/state/config.state';
import {ColumnDefinitionBuilder} from '@ui/grid/store/model/column-definition.builder';
import {GridConfig, PaginationConfig} from '@ui/grid/store/model/grid-config.model';

export function grid() {
  return new GridConfigBuilder(GridType.GRID).defaultPagination();
}

export function treeGrid() {
  return new GridConfigBuilder(GridType.TREE).withPagination(pagination().inactive());
}

export function dataTable() {
  return new GridConfigBuilder(GridType.TABLE).defaultPagination();
}

export function pagination() {
  return new PaginationConfigBuilder();
}

export class GridConfigBuilder {
  private rowHeight = 25;
  private dataProvider: GridDataProvider = GridDataProvider.CLIENT;
  private serverRootUrl ?: string [];
  private columnDefinitionBuilders: ColumnDefinitionBuilder[] = [];
  private paginationBuilder: PaginationConfigBuilder;

  constructor(private gridType: GridType) {
  }

  withRowHeight(rowHeight: number): GridConfigBuilder {
    this.rowHeight = rowHeight;
    return this;
  }

  withColumns(columnDefinitionBuilders: ColumnDefinitionBuilder[]): GridConfigBuilder {
    this.columnDefinitionBuilders.push(...columnDefinitionBuilders);
    return this;
  }

  server(serverRootUrl: string[]): GridConfigBuilder {
    this.serverRootUrl = serverRootUrl;
    this.dataProvider = GridDataProvider.SERVER;
    return this;
  }

  defaultPagination(): GridConfigBuilder {
    this.paginationBuilder = new PaginationConfigBuilder();
    return this;
  }

  withPagination(paginationBuilder: PaginationConfigBuilder): GridConfigBuilder {
    this.paginationBuilder = paginationBuilder;
    return this;
  }

  build(): GridConfig {
    const columnDefinitions = this.columnDefinitionBuilders.map(builder => builder.build());
    return {
      rowHeight: this.rowHeight,
      dataProvider: this.dataProvider,
      serverRootUrl: this.serverRootUrl,
      gridType: this.gridType,
      columnDefinitions: columnDefinitions,
      pagination: this.paginationBuilder.build()
    };
  }

}

export class PaginationConfigBuilder {
  private active = true;
  private pageSize = 25;
  private showAll = false;
  private allowedPageSizes: number[] = [10, 25, 50, 100];

  inactive(): PaginationConfigBuilder {
    this.active = false;
    return this;
  }

  build(): PaginationConfig {
    return {...this as object as PaginationConfig};
  }
}
