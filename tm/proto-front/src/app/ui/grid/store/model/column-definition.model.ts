///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {CellRenderers} from '../../interfaces/cell-renderer';

export class ColumnDefinition {
  id: string;
  i18nKey?: string;
  width: number;
  sortable?: Boolean = true;
  sort?: ColumnSort = ColumnSort.NO_SORT;
  sortIndex?: number;
  cellRenderer?: CellRenderers;
}

export enum ColumnSort {
  'NO_SORT' = 'NO_SORT', ASC = 'ASC', DESC = 'DESC'
}

export namespace ColumnSort {
  export function toggleSortState(columnSort: ColumnSort): ColumnSort {
    if (!columnSort) {
      return ColumnSort.ASC;
    }
    switch (columnSort) {
      case ColumnSort.NO_SORT :
        return ColumnSort.ASC;
      case ColumnSort.ASC:
        return ColumnSort.DESC;
      case ColumnSort.DESC:
        return ColumnSort.NO_SORT;
    }
  }

  export function sortMultiplier(columnSort: ColumnSort): number {
    if (columnSort === ColumnSort.ASC) {
      return 1;
    } else if (columnSort === ColumnSort.DESC) {
      return -1;
    }
    throw new Error(`No multiplier for sort : ${columnSort}`);
  }
}
