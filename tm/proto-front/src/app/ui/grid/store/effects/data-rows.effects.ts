///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {
  DataRowActionTypes,
  GetChildrenFromServer,
  GetChildrenFromServerSuccess,
  GetInitialRowsFromServer,
  LoadDataRows
} from '../actions';
import {map, switchMap} from 'rxjs/operators';
import {GridService} from '../../services/grid.service';
import {GridResponse} from '../model/data-row.model';


@Injectable()
export class DataRowsEffects {

  constructor(private actions$: Actions, private gridService: GridService) {
  }

  @Effect()
  getInitialRowsFromServer$ = this.actions$.pipe(
    ofType(DataRowActionTypes.GetInitialRowsFromServer),
    switchMap((action: GetInitialRowsFromServer) => {
      return this.gridService.getInitialRowsFromServer(action.payload.config.serverRootUrl).pipe(
        map((gridResponse: GridResponse) => {
          gridResponse.dataRows = gridResponse.dataRows.map(dataRow => {
            return {
              ...dataRow,
              ...dataRow['data']
            };
          });
          return new LoadDataRows(action.pagePrefix, action.componentId, {
            dataRows: gridResponse.dataRows,
            count: gridResponse.dataRows.length
          });
        })
      );
    })
  );

  @Effect()
  getChildrenFromServer$ = this.actions$.pipe(
    ofType(DataRowActionTypes.GetChildrenFromServer),
    switchMap((action: GetChildrenFromServer) => {
      return this.gridService.getChildrenFromServer(action.payload.config.serverRootUrl, action.payload.id).pipe(
        map((gridResponse: GridResponse) => {
          gridResponse.dataRows = gridResponse.dataRows.map(dataRow => {
            return {
              ...dataRow,
              parentRowId: action.payload.id,
              ...dataRow['data']
            };
          });
          return new GetChildrenFromServerSuccess(action.pagePrefix, action.componentId, {
            id: action.payload.id,
            children: gridResponse.dataRows
          });
        })
      );
    })
  );

}
