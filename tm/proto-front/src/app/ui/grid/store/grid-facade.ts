///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {ComponentFacade} from '../../../common/store/facades/component.facade';
import * as fromConfigSelectors from './selectors/grid-config.selectors';
import * as fromPaginationSelectors from './selectors/grid-pagination.selectors';
import {createSelector, select, Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {RowDisplay} from './model/row-display.model';
import {DataRow} from './model/data-row.model';
import {ChangePageSize, CloseDataRow, LoadDataRows, NextPage, OpenDataRow, PreviousPage, ToggleRowSelection} from './actions';
import {GridConfig} from './model/grid-config.model';
import {GridDisplay} from './model/grid-display.model';
import {gridInitialState, GridState} from './reducers/state/grid.state';
import {PaginationDisplay} from './model/pagination-display.model';
import {GridConfigState} from './reducers/state/config.state';
import {ToggleSort} from '@ui/grid/store/actions/column.actions';

export abstract class GridFacade extends ComponentFacade {

  // public selectors
  // public nodes$: Observable<NodeRowModel[]>;
  public rows$: Observable<RowDisplay[]>;
  public gridDisplay$: Observable<GridDisplay>;
  public pagination$: Observable<PaginationDisplay>;
  public config$: Observable<GridConfigState>;

  // full grid state. For internal use only as for consumer it's better to separate and transform state.
  protected grid$: Observable<GridState>;

  constructor(pageUrl: string, componentId: string, config: GridConfig, store: Store<GridState>) {
    super(pageUrl, componentId, store);

    this.grid$ = this.store.pipe(
      select(createSelector(this.componentStateSelector))
    );

    this.gridDisplay$ = this.store.pipe(
      select(createSelector(this.componentStateSelector, fromConfigSelectors.gridDisplaySelector))
    );

    this.pagination$ = this.store.pipe(
      select(createSelector(this.componentStateSelector, fromPaginationSelectors.paginationDisplaySelector))
    );

    this.config$ = this.store.pipe(
      select(createSelector(this.componentStateSelector, fromConfigSelectors.configSelector))
    );

  }

  public loadData(dataRows: DataRow[], count: number) {
    this.store.dispatch(new LoadDataRows(this.featureName, this.componentId, {dataRows, count}));
  }

  public paginateNextPage() {
    this.store.dispatch(new NextPage(this.featureName, this.componentId));
  }

  public paginatePreviousPage() {
    this.store.dispatch(new PreviousPage(this.featureName, this.componentId));
  }

  public changePaginationSize(pageSize: number) {
    this.store.dispatch(new ChangePageSize(this.featureName, this.componentId, {pageSize}));
  }

  public openRow (id: string) {
    this.store.dispatch(new OpenDataRow(this.featureName, this.componentId, {id}));
  }

  public closeRow(id: string) {
    this.store.dispatch(new CloseDataRow(this.featureName, this.componentId, {id}));
  }

  public toggleRowSelection (id: string) {
    this.store.dispatch(new ToggleRowSelection(this.featureName, this.componentId, {id}));
  }

  public toggleColumnSort (id: string) {
    this.store.dispatch(new ToggleSort(this.featureName, this.componentId, {id}));
  }

  get initialState() {
    return gridInitialState;
  }
}
