///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {sortRowDisplay} from './utils.selector';
import {ColumnSort} from '../model/column-definition.model';
import {RowDisplay} from '../model/row-display.model';

describe('Client Row Display Selector', () => {

  describe('Single Sorts on std columns', () => {
    it('should sort on one simple string column ASC', () => {
      const result = sortRowDisplay(getRows(), [{id: 'name', width: 0, sortIndex: 0, sort: ColumnSort.ASC}]);

      expect(result.map(row => row.id)).toEqual(['row-1', 'row-4', 'row-2', 'row-3']);
    });

    it('should sort on one simple string column DESC', () => {
      const result = sortRowDisplay(getRows(), [{id: 'name', width: 0, sortIndex: 0, sort: ColumnSort.DESC}]);

      expect(result.map(row => row.id)).toEqual(['row-3', 'row-2', 'row-4', 'row-1']);
    });

    it('should sort on one simple number column ASC', () => {
      const result = sortRowDisplay(getRows(), [
        {id: 'hitPoints', width: 0, sortIndex: 0, sort: ColumnSort.ASC},
      ]);

      expect(result.map(row => row.id)).toEqual(['row-4', 'row-2', 'row-1', 'row-3']);
    });

    it('should sort on one simple number column DESC', () => {
      const result = sortRowDisplay(getRows(), [
        {id: 'hitPoints', width: 0, sortIndex: 0, sort: ColumnSort.DESC},
      ]);

      expect(result.map(row => row.id)).toEqual(['row-3', 'row-1', 'row-2', 'row-4']);
    });

    function getRows(): RowDisplay[] {
      return [
        {id: 'row-1', data: {id: 'row-1', name: 'asgore', hitPoints: 500}},
        {id: 'row-2', data: {id: 'row-2', name: 'papyrus', hitPoints: 120}},
        {id: 'row-3', data: {id: 'row-3', name: 'undyne', hitPoints: 4000}},
        {id: 'row-4', data: {id: 'row-4', name: 'flowy', hitPoints: 9}},
      ];
    }
  });


  describe('should sort on two columns', () => {

    it('should sort on two column ASC', () => {
      const columns = [
        {id: 'name', width: 0, sortIndex: 0, sort: ColumnSort.ASC},
        {id: 'hitPoints', width: 0, sortIndex: 1, sort: ColumnSort.ASC},
      ];

      const result = sortRowDisplay(getRows(), columns);

      expect(result.map(row => row.id)).toEqual(['row-1', 'row-5', 'row-8', 'row-4', 'row-2', 'row-6', 'row-3', 'row-7']);
    });

    it('should sort on three column heterogeneous', () => {
      const columns = [
        {id: 'name', width: 0, sortIndex: 0, sort: ColumnSort.ASC},
        {id: 'hitPoints', width: 0, sortIndex: 1, sort: ColumnSort.DESC},
        {id: 'kind', width: 0, sortIndex: 2, sort: ColumnSort.ASC},
      ];

      const result = sortRowDisplay(getRows(), columns);

      expect(result.map(row => row.id)).toEqual(['row-1', 'row-4', 'row-8', 'row-5', 'row-2', 'row-6', 'row-7', 'row-3']);
    });


    function getRows(): RowDisplay[] {
      return [
        {id: 'row-1', data: {id: 'row-1', name: 'asgore', hitPoints: 500, kind: 'king'}},
        {id: 'row-2', data: {id: 'row-2', name: 'papyrus', hitPoints: 120, kind: 'skeleton'}},
        {id: 'row-3', data: {id: 'row-3', name: 'undyne', hitPoints: 400, kind: 'guard'}},
        {id: 'row-4', data: {id: 'row-4', name: 'flowy', hitPoints: 999999999, kind: 'monster'}},
        {id: 'row-5', data: {id: 'row-5', name: 'flowy', hitPoints: 9, kind: 'flower'}},
        {id: 'row-6', data: {id: 'row-6', name: 'sans', hitPoints: 12000, kind: 'skeleton'}},
        {id: 'row-7', data: {id: 'row-7', name: 'undyne', hitPoints: 4000, kind: 'true-hero'}},
        {id: 'row-8', data: {id: 'row-8', name: 'flowy', hitPoints: 9, kind: 'flawy'}},
      ];
    }
  });

  describe('should handle various unexpected values', () => {

    it('should handle various null, undefined and empty string', () => {
      const columns = [
        {id: 'name', width: 0, sortIndex: 0, sort: ColumnSort.ASC}
      ];

      const result = sortRowDisplay(getRows(), columns);

      expect(result.map(row => row.id)).toEqual(['row-4', 'row-5', 'row-2', 'row-1', 'row-3']);
    });

    it('should handle various null, undefined and NaN numbers', () => {
      const columns = [
        {id: 'hitPoints', width: 0, sortIndex: 0, sort: ColumnSort.DESC}
      ];

      const result = sortRowDisplay(getRows(), columns);

      expect(result.map(row => row.id)).toEqual(['row-4', 'row-2', 'row-5', 'row-1', 'row-3']);
    });

    function getRows(): RowDisplay[] {
      return [
        {id: 'row-1', data: {id: 'row-1', name: 'asgore', hitPoints: null, kind: 'king'}},
        {id: 'row-2', data: {id: 'row-2', name: '', hitPoints: 120, kind: 'skeleton'}},
        {id: 'row-3', data: {id: 'row-3', name: 'undyne', hitPoints: undefined, kind: 'guard'}},
        {id: 'row-4', data: {id: 'row-4', name: null, hitPoints: 999999999, kind: 'monster'}},
        {id: 'row-5', data: {id: 'row-5', name: undefined, hitPoints: -1 * ('toto' as any), kind: 'monster'}},
      ];
    }
  });

});

