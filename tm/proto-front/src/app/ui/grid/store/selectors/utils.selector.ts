///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {ColumnDefinition, ColumnSort} from '../model/column-definition.model';
import {RowDisplay} from '../model/row-display.model';
import {DataRowOpenState} from '../model/data-row.model';
import sortMultiplier = ColumnSort.sortMultiplier;

export function findSortedColumns(columnDefinitions: ColumnDefinition[]) {
  return columnDefinitions
    .filter(columnDef => columnDef.sort !== ColumnSort.NO_SORT)
    .sort((colA, colB) => colA.sortIndex - colB.sortIndex);
}

export function sortRowDisplay(rowDisplays: RowDisplay[], sortedColumns: ColumnDefinition[]) {
  return rowDisplays.sort((rowA, rowB) => {
    let result = 0;
    const sortedColumnsStack = [...sortedColumns].reverse();
    while (result === 0 && sortedColumnsStack.length > 0) {
      const sort = sortedColumnsStack.pop();
      const valueA = rowA.data[sort.id];
      const valueB = rowB.data[sort.id];
      // == on null will be true for undefined and null
      // DO NOT change to === or undefined values will not be filtered
      const valueAisNullish = valueA == null;
      const valueBisNullish = valueB == null;
      let viciousValues = false;

      // handling nullish values
      if (valueAisNullish && valueBisNullish) {
        viciousValues = true;
      } else if (valueAisNullish && !valueBisNullish) {
        viciousValues = true;
        result = -1 * sortMultiplier(sort.sort);
      } else if (!valueAisNullish && valueBisNullish) {
        viciousValues = true;
        result = sortMultiplier(sort.sort);
      }

      // if we have no nullish values let's compare
      if (!viciousValues) {
        if (typeof valueA === 'string' || typeof valueB === 'string') {
          result = doCompareStrings(valueA, valueB, sort);
        } else if (typeof valueA === 'number' || typeof valueB === 'number') {
          result = doCompareNumbers(valueA, valueB, sort);
        } else {
          console.log(`unable to sort type, valueA ${typeof valueA} valueB ${typeof valueB}.
            Please provide a custom sorter in your column definition`);
        }
      }
    }
    return result;
  });
}

export function doCompareStrings(valueA: string, valueB: string, sort: ColumnDefinition) {
  return ((valueA as string).localeCompare(valueB) * sortMultiplier(sort.sort));
}

export function doCompareNumbers(valueA: number, valueB: number, sort: ColumnDefinition) {
  const valueAisNan = isNaN(valueA);
  const valueBisNan = isNaN(valueB);
  if (valueAisNan && valueBisNan) {
    return 0;
  }

  if (!valueAisNan && valueBisNan) {
    return sortMultiplier(sort.sort);
  }

  if (valueAisNan && !valueBisNan) {
    return -1 * sortMultiplier(sort.sort);
  }

  return (valueA - valueB) * sortMultiplier(sort.sort);
}

export function flatTree(rowDisplays: RowDisplay[], rowDisplayDict) {
  const stack: RowDisplay[] = rowDisplays.filter(nodeRow => !Boolean(nodeRow.parentRowId)).reverse();
  const tree: RowDisplay[] = [];
  let index = 0;

  while (stack.length) {
    const rowDisplay = stack.pop();
    rowDisplay.index = index;
    index++;
    tree.push(rowDisplay);
    if (rowDisplay.children && rowDisplay.state === DataRowOpenState.open) {
      for (let i = rowDisplay.children.length - 1; i >= 0; i--) {
        const id = rowDisplay.children[i];
        const child: RowDisplay = rowDisplayDict[id];
        child.depth = rowDisplay.depth + 1;
        stack.push(child);
      }
    }
  }

  return tree;
}
