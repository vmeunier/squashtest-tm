///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {
  ConfigActions,
  ConfigActionTypes,
  DataRowActions,
  DataRowActionTypes,
  GridActions,
  PaginationActions,
  PaginationActionTypes
} from '../actions';
import {reduceConfigAction} from './config.reducer';
import {reduceDataRowAction} from './data-row.reducer';
import {gridInitialState, GridState} from './state/grid.state';
import {reducePaginationAction} from './pagination.reducer';
import {ColumnActions, ColumnActionTypes} from '@ui/grid/store/actions/column.actions';
import {reduceColumnAction} from '@ui/grid/store/reducers/column.reducer';


export function reducer(
  state = gridInitialState,
  action: GridActions
): GridState {
  const isDataRowAction = Object.values(DataRowActionTypes).includes(action.type);
  const isConfigAction = Object.values(ConfigActionTypes).includes(action.type);
  const isPaginationAction = Object.values(PaginationActionTypes).includes(action.type);
  const isColumnAction = Object.values(ColumnActionTypes).includes(action.type);

  if (isDataRowAction) {
    return reduceDataRowAction(state, action as DataRowActions);
  } else if (isColumnAction) {
    return reduceColumnAction(state, action as ColumnActions);
  } else if (isConfigAction) {
    return reduceConfigAction(state, action as ConfigActions);
  } else if (isPaginationAction) {
    return reducePaginationAction(state, action as PaginationActions);
  }
  return state;
}
