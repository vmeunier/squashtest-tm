///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {getDescendants, getAncestors} from './data-row.reducer';
import {Dictionary} from '@ngrx/entity';
import {DataRow, DataRowOpenState, DataRowState} from '@ui/grid/store/model/data-row.model';
import {reducer} from '@ui/grid/store/reducers/grid.reducer';
import {gridInitialState} from '@ui/grid/store/reducers/state/grid.state';

describe('DataRow Reducer', () => {
  describe('unknown action', () => {
    it('should return the initial state', () => {
      const action = {} as any;

      const result = reducer(gridInitialState, action);

      expect(result).toBe(gridInitialState);
    });
  });

  // describe('[TreeNode] Toggle TreeNode', () => {
  //   it('should add a node', () => {
  //     const action: ToggleTreeNode = new ToggleTreeNode('', '', {id: 'TestCase-12'});
  //
  //     const entities = getEntities();
  //     const result = reducer({...initialState, entities}, action);
  //
  //     expect(result.entities).toBe(entities);
  //     expect(result.ids).toBe(initialState.ids);
  //     expect(result.selectedNodes).toEqual(['TestCase-12']);
  //
  //   });
  //
  //   it('should add a node in existing selection', () => {
  //     const action: ToggleTreeNode = new ToggleTreeNode('', '', {id: 'TestCase-8'});
  //
  //     const entities = getEntities();
  //     const result = reducer({...initialState, entities, selectedNodes: ['TestCaseLibrary-2', 'TestCase-9']}, action);
  //
  //     expect(result.entities).toBe(entities);
  //     expect(result.ids).toBe(initialState.ids);
  //     expect(result.selectedNodes).toEqual(['TestCaseLibrary-2', 'TestCase-9', 'TestCase-8']);
  //
  //   });
  //
  //   it('should remove a node in selection', () => {
  //     const action: ToggleTreeNode = new ToggleTreeNode('', '', {id: 'TestCase-1'});
  //
  //     const result = reducer({
  //       ...initialState,
  //       selectedNodes: ['TestCaseLibrary-1', 'TestCaseLibrary-2', 'TestCase-1', 'TestCase-2', 'TestCaseFolder-2']
  //     }, action);
  //
  //     expect(result.entities).toBe(initialState.entities);
  //     expect(result.ids).toBe(initialState.ids);
  //     expect(result.selectedNodes).toEqual(['TestCaseLibrary-1', 'TestCaseLibrary-2', 'TestCase-2', 'TestCaseFolder-2']);
  //
  //   });
  //
  //   it('should prevent node selection if an ancestor library is selected', () => {
  //     const action: ToggleTreeNode = new ToggleTreeNode('', '', {id: 'TestCase-12'});
  //
  //     const result = reducer({
  //       ...initialState,
  //       entities: getEntities(),
  //       selectedNodes: ['TestCaseLibrary-1', 'TestCaseLibrary-2']
  //     }, action);
  //
  //     expect(result.entities).toEqual(getEntities());
  //     expect(result.ids).toBe(initialState.ids);
  //     expect(result.selectedNodes).toEqual(['TestCaseLibrary-1', 'TestCaseLibrary-2']);
  //
  //   });
  //
  //   it('should prevent node selection if an ancestor folder is selected', () => {
  //     const action: ToggleTreeNode = new ToggleTreeNode('', '', {id: 'TestCase-9'});
  //
  //     const result = reducer({
  //       ...initialState,
  //       entities: getEntities(),
  //       selectedNodes: ['TestCaseFolder-3', 'TestCaseLibrary-2']
  //     }, action);
  //
  //     expect(result.entities).toEqual(getEntities());
  //     expect(result.ids).toBe(initialState.ids);
  //     expect(result.selectedNodes).toEqual(['TestCaseFolder-3', 'TestCaseLibrary-2']);
  //
  //   });
  //
  // });
  //
  // describe('[TreeNode] Select TreeNode', () => {
  //   it('should select node', () => {
  //     const action: SelectTreeNode = new SelectTreeNode('', '', {id: 'TestCase-1'});
  //
  //     const result = reducer(initialState, action);
  //
  //     expect(result.entities).toBe(initialState.entities);
  //     expect(result.ids).toBe(initialState.ids);
  //     expect(result.selectedNodes).toEqual(['TestCase-1']);
  //
  //
  //   });
  //
  //
  //   it('should clear selection and select node', () => {
  //     const action: SelectTreeNode = new SelectTreeNode('', '', {id: 'TestCase-1'});
  //
  //     const result = reducer({
  //       ...initialState,
  //       selectedNodes: ['TestCaseLibrary-1', 'TestCaseLibrary-2', 'TestCase-1', 'TestCase-2', 'TestCaseFolder-2']
  //     }, action);
  //
  //     expect(result.entities).toBe(initialState.entities);
  //     expect(result.ids).toBe(initialState.ids);
  //     expect(result.selectedNodes).toEqual(['TestCase-1']);
  //
  //
  //   });
  //
  //   it('should be idempotent', () => {
  //     const action: SelectTreeNode = new SelectTreeNode('', '', {id: 'TestCase-1'});
  //
  //     const result = reducer({
  //       ...initialState,
  //       selectedNodes: ['TestCase-1']
  //     }, action);
  //
  //     expect(result.entities).toBe(initialState.entities);
  //     expect(result.ids).toBe(initialState.ids);
  //     expect(result.selectedNodes).toEqual(['TestCase-1']);
  //
  //
  //   });
  // });
});

/**
 * Tests of getDescendant method
 */
describe('DataRow Get Descendant', () => {
  it('should return empty array', () => {
    const dataRow = {
      id: 'TestCaseLibrary-1',
      state: DataRowOpenState.closed,
      children: []
    };
    const entities: Dictionary<DataRow> = {
      'TestCaseLibrary-1': dataRow
    };

    const ancestorId = 'TestCaseLibrary-1';
    const result = getDescendants(entities, [dataRow]);
    expect(result).toEqual([]);
    expect(entities).toBe(entities);
  });

  it('should return first level children', () => {

    const entities = getEntities();

    let result = getDescendants(entities, [entities['TestCaseFolder-7']]);
    expect(result).toEqual(['TestCase-9']);
    expect(entities).toBe(entities);

    result = getDescendants(entities, [entities['TestCaseLibrary-2']]);
    expect(result).toEqual(['TestCase-10', 'TestCaseFolder-11']);

  });

  it('should return deep nested children of libraries', () => {

    const entities = getEntities();

    let result = getDescendants(entities, [entities['TestCaseLibrary-1']]);
    expect(result.sort()).toEqual(
      ['TestCase-12', 'TestCaseFolder-1', 'TestCaseFolder-2', 'TestCaseFolder-3', 'TestCase-4',
        'TestCase-5', 'TestCaseFolder-6', 'TestCaseFolder-7', 'TestCase-8', 'TestCase-9'].sort());
    expect(entities).toBe(entities);

    result = getDescendants(entities, [entities['TestCaseLibrary-2']]);
    expect(result.sort()).toEqual(
      ['TestCase-10', 'TestCaseFolder-11'].sort());
    expect(entities).toBe(entities);

  });

  it('should return children of nested folders', () => {

    const entities = getEntities();

    let result = getDescendants(entities, [entities['TestCaseFolder-7']]);
    expect(result.sort()).toEqual(
      ['TestCase-9'].sort());

    result = getDescendants(entities, [entities['TestCaseFolder-3']]);
    expect(result.sort()).toEqual(
      ['TestCaseFolder-7', 'TestCase-8', 'TestCase-9'].sort());

    result = getDescendants(entities, [entities['TestCase-9']]);
    expect(result.sort()).toEqual(
      [].sort());
  });

  it('should return children of multiple nested folders', () => {

    const entities = getEntities();

    let result = getDescendants(entities, [entities['TestCaseFolder-7'], entities['TestCaseFolder-3']]);
    expect(result.sort()).toEqual(
      ['TestCaseFolder-7', 'TestCase-8', 'TestCase-9'].sort());

    result = getDescendants(entities, [entities['TestCaseLibrary-1'], entities['TestCaseLibrary-2']]);
    expect(result.sort()).toEqual(
      ['TestCase-12', 'TestCaseFolder-1', 'TestCaseFolder-2', 'TestCaseFolder-3',
        'TestCase-4', 'TestCase-5', 'TestCaseFolder-6', 'TestCaseFolder-7',
        'TestCase-8', 'TestCase-9', 'TestCase-10', 'TestCaseFolder-11'].sort());
  });


});

describe('TreeNode Get Ancestors', () => {
  it('should return empty array for libraries', (done) => {

    const entities = getEntities();

    let result = getAncestors(entities, entities['TestCaseLibrary-1']);
    expect(result.sort()).toEqual(
      []);

    result = getAncestors(entities, entities['TestCaseLibrary-2']);
    expect(result.sort()).toEqual(
      []);
    done();
  });

  it('should return single value arrays for first level nodes', (done) => {

    const entities = getEntities();

    let result = getAncestors(entities, entities['TestCase-12']);
    expect(result).toEqual(
      ['TestCaseLibrary-1']);

    result = getAncestors(entities, entities['TestCaseFolder-11']);
    expect(result).toEqual(
      ['TestCaseLibrary-2']);
    done();
  });

  it('should return array for deep nodes', (done) => {

    const entities = getEntities();

    let result = getAncestors(entities, entities['TestCase-9']);
    expect(result).toEqual(
      ['TestCaseFolder-7', 'TestCaseFolder-3', 'TestCaseFolder-1', 'TestCaseLibrary-1']);

    result = getAncestors(entities, entities['TestCaseFolder-3']);
    expect(result).toEqual(
      ['TestCaseFolder-1', 'TestCaseLibrary-1']);
    done();
  });
});

function getEntities(): Dictionary<DataRow> {
  const tcl1 = {
    id: 'TestCaseLibrary-1',
    name: 'Project-1',
    state: DataRowOpenState.closed,
    children: ['TestCase-12', 'TestCaseFolder-1', 'TestCaseFolder-2'],
    projectId: 1
  };

  const tcl2 = {
    id: 'TestCaseLibrary-2',
    state: DataRowOpenState.closed,
    children: ['TestCase-10', 'TestCaseFolder-11'],
  };

  const tcf1 = {
    id: 'TestCaseFolder-1',
    state: DataRowOpenState.open,
    parentRowId: 'TestCaseLibrary-1',
    children: ['TestCaseFolder-3', 'TestCase-4', 'TestCase-5', 'TestCaseFolder-6'],
  };

  const tcf2 = {
    id: 'TestCaseFolder-2',
    state: DataRowOpenState.closed,
    parentRowId: 'TestCaseLibrary-1',
    children: [],
  };

  const tcf3 = {
    id: 'TestCaseFolder-3',
    state: DataRowOpenState.open,
    parentRowId: 'TestCaseFolder-1',
    children: ['TestCaseFolder-7', 'TestCase-8'],
  };

  const tcf6 = {
    id: 'TestCaseFolder-6',
    state: DataRowOpenState.closed,
    parentRowId: 'TestCaseFolder-1',
    children: [],
  };

  const tcf7 = {
    id: 'TestCaseFolder-7',
    state: DataRowOpenState.closed,
    parentRowId: 'TestCaseFolder-3',
    children: ['TestCase-9'],
  };

  const tcf10 = {
    id: 'TestCaseFolder-11',
    state: DataRowOpenState.closed,
    parentRowId: 'TestCaseLibrary-2',
    children: [],
  };

  const tc12 = {
    id: 'TestCase-12',
    state: DataRowOpenState.leaf,
    parentRowId: 'TestCaseLibrary-1',
    children: [],
  };

  const tc4 = {
    id: 'TestCase-4',
    state: DataRowOpenState.leaf,
    parentRowId: 'TestCaseFolder-1',
    children: [],
  };

  const tc5 = {
    id: 'TestCase-5',
    state: DataRowOpenState.leaf,
    parentRowId: 'TestCaseFolder-1',
    children: [],
  };

  const tc8 = {
    id: 'TestCase-8',
    state: DataRowOpenState.leaf,
    parentRowId: 'TestCaseFolder-3',
    children: [],
  };

  const tc9 = {
    id: 'TestCase-9',
    state: DataRowOpenState.leaf,
    parentRowId: 'TestCaseFolder-7',
    children: [],
  };

  const tc10 = {
    id: 'TestCase-10',
    name: 'TestCase 10',
    state: DataRowOpenState.leaf,
    parentRowId: 'TestCaseLibrary-2',
    children: [],
  };

  return [tcl1, tcf1, tcf2, tcf3, tcf6, tcf7, tc12, tc4, tc5, tcl2, tc8, tc9, tc10, tcf10]
    .reduce((accumulation: Dictionary<DataRow>, currentNode: DataRow) => {
      accumulation[currentNode.id] = currentNode;
      return accumulation;
    }, {});
}
