///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {ConfigActions, ConfigActionTypes} from '@ui/grid/store/actions/config.actions';
import {GridConfigState} from './state/config.state';
import {gridInitialState, GridState} from './state/grid.state';
import {columnDefinitionAdapter} from './state/columns-definition.state';
import {PaginationState} from './state/pagination.state';


export function reduceConfigAction(state: GridState = gridInitialState, action: ConfigActions) {
  switch (action.type) {
    case ConfigActionTypes.LoadConfig : {
      const partialConfig: Partial<GridConfigState> = {...action.payload.config};
      const {columnDefinitions, pagination} = action.payload.config;

      let partialPagination: Partial<PaginationState> = {};
      if (pagination) {
        partialPagination = {...pagination};
      }

      return {
        ...state,
        columnDefinitions: columnDefinitionAdapter.addAll(columnDefinitions, state.columnDefinitions),
        config: {...state.config, ...partialConfig},
        pagination: {...state.pagination, ...partialPagination}
      };
    }
    default: {
      return state;
    }
  }
}
