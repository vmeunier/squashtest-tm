///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {DataRowActions, DataRowActionTypes, ToggleRowSelection} from '../actions';
import {dataRowAdapter, DataRowState} from './state/data-row.state';
import {gridInitialState, GridState} from './state/grid.state';
import {DataRow, DataRowOpenState} from '../model/data-row.model';
import {Dictionary, UpdateStr} from '@ngrx/entity/src/models';


export function reduceDataRowAction(
  state = gridInitialState,
  action: DataRowActions): GridState {
  switch (action.type) {
    case DataRowActionTypes.LoadDataRows : {
      return {
        ...state,
        dataRowState: dataRowAdapter.addAll(action.payload.dataRows, state.dataRowState),
        pagination: {...state.pagination, count: action.payload.count}
      };
    }
    case DataRowActionTypes.AddDataRows : {
      return {
        ...state,
        dataRowState: dataRowAdapter.addMany(action.payload.dataRows, state.dataRowState),
      };
    }
    case DataRowActionTypes.UpdateDataRow : {
      return {
        ...state,
        dataRowState: dataRowAdapter.updateOne(action.payload.dataRow, state.dataRowState),
      };
    }
    case DataRowActionTypes.OpenDataRow : {
      const dataRow = state.dataRowState.entities[action.payload.id];
      return {
        ...state,
        dataRowState: dataRowAdapter.updateOne({
          id: dataRow.id,
          changes: {state: DataRowOpenState.open}
        } as UpdateStr<DataRow>, state.dataRowState)
      };
    }
    case DataRowActionTypes.CloseDataRow : {
      const dataRow = state.dataRowState.entities[action.payload.id];
      return {
        ...state,
        dataRowState: dataRowAdapter.updateOne({
          id: dataRow.id,
          changes: {state: DataRowOpenState.closed}
        } as UpdateStr<DataRow>, state.dataRowState)
      };
    }
    case DataRowActionTypes.GetChildrenFromServerSuccess: {
      const childrenIds = action.payload.children.map(child => child.id);
      let dataRowState = dataRowAdapter.updateOne({
        id: action.payload.id,
        changes: {state: DataRowOpenState.open, children: childrenIds}
      } as UpdateStr<DataRow>, state.dataRowState);
      dataRowState = dataRowAdapter.addMany(action.payload.children, dataRowState);
      return {...state, dataRowState};
    }
    case DataRowActionTypes.ToggleRowSelection: {
      return doToggleRowSelection(state, action);
    }
    default: {
      return state;
    }
  }
}

function doToggleRowSelection(state: GridState, action: ToggleRowSelection): GridState {
  const selectedRows = Object.values(state.dataRowState.entities)
    .filter(dataRow => dataRow.selected)
    .map(value => value.id);

  const id = action.payload.id;
  let dataRowState = state.dataRowState;

  // if node was previously selected, we just toggle it
  if (selectedRows.includes(id)) {
    // selectedRows = selectedRows.filter(value => value !== id);
    dataRowState = dataRowAdapter.updateOne({
      id,
      changes: {selected: false}
    } as UpdateStr<DataRow>, state.dataRowState);
    // else :
    // if we have already selected an ancestor of the selected node we must ignore the selection and do nothing.
    // else deselect all previously selected children and sub-children and select the given node.
  } else {
    const selectedRow = state.dataRowState.entities[id];
    const ancestors = getAncestors(state.dataRowState.entities, selectedRow);
    const intersection = selectedRows.filter(nodeId => ancestors.includes(nodeId));
    // if the selection is not forbidden
    // ie if no ancestor of selected node was previously selected
    if (intersection.length === 0) {
      const descendantIds = getDescendants(state.dataRowState.entities, [selectedRow]);
      // unselect all descendant;
      const changes = descendantIds.map(descendantId => {
        return {
          id: descendantId,
          changes: {selected: false}
        };
      });
      changes.push({id, changes: {selected: true}});
      dataRowState = dataRowAdapter.updateMany(changes, dataRowState);
    }
  }
  return {...state, dataRowState};
}

export function getDescendants(entities: Dictionary<DataRow>, ancestors: DataRow[]): any[] {
  const descendantIds = new Set<string>([]);
  let childrenIds;
  let nextLayerIds = [];
  ancestors.forEach(value => nextLayerIds.push(...value.children));
  do {
    childrenIds = [...nextLayerIds];
    nextLayerIds = [];
    for (const childrenId of childrenIds) {
      descendantIds.add(childrenId);
      const node = entities[childrenId];
      nextLayerIds.push(...node.children);
    }
  } while (nextLayerIds.length > 0);

  return [...descendantIds];
}

export function getAncestors(entities: Dictionary<DataRow>, descendant: DataRow): any[] {
  const ancestorIds = new Set<string>([]);
  let parentId: string = descendant.parentRowId;
  while (parentId) {
    ancestorIds.add(parentId);
    const entity = entities[parentId];
    if (entity) {
      parentId = entity.parentRowId;
    } else {
      parentId = null;
    }
  }

  return [...ancestorIds];
}


