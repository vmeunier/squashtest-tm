///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {GridState} from '@ui/grid/store/reducers/state/grid.state';
import {ColumnActions, ColumnActionTypes} from '@ui/grid/store/actions/column.actions';
import {columnDefinitionAdapter} from '@ui/grid/store/reducers/state/columns-definition.state';
import {ColumnDefinition, ColumnSort} from '@ui/grid/store/model/column-definition.model';
import {UpdateStr} from '@ngrx/entity/src/models';

export function reduceColumnAction(state: GridState, action: ColumnActions): GridState {
  switch (action.type) {
    case ColumnActionTypes.ToggleSort: {
      return handleTogglePagination(action, state);
    }
    default:
      return state;
  }
}

function handleTogglePagination(action: ColumnActions, state: GridState) {
  const id = action.payload.id;
  const columnDefinition = state.columnDefinitions.entities[id];
  const sort = ColumnSort.toggleSortState(columnDefinition.sort);
  const changes: UpdateStr<ColumnDefinition>[] = [];
  // now we change the index of sorts if needed
  // first retrieve all sorted column in array
  const sortedColumnsIds: string[] =
    Object.values(state.columnDefinitions.entities)
      .filter(columnDef => columnDef.sort !== ColumnSort.NO_SORT)
      .sort((colA, colB) => colA.sortIndex - colB.sortIndex)
      .map(columnDef => columnDef.id);

  if (sort === ColumnSort.NO_SORT) {
    // we must strip the now unsorted column from sorted column and subtract 1 to index of all following sorted columns.
    const index = sortedColumnsIds.indexOf(id);
    sortedColumnsIds.splice(index, 1);
    changes.push(...sortedColumnsIds.map((columnId: string, i: number) => {
      return {
        id: columnId, changes: {sortIndex: i}
      };
    }));
    changes.push({id, changes: {sort, sortIndex: 0}});
  } else if (sort === ColumnSort.ASC) {
    changes.push({id, changes: {sort, sortIndex: sortedColumnsIds.length}});
  } else {
    changes.push({id, changes: {sort}});
  }

  const columnDefinitions = columnDefinitionAdapter.updateMany(changes, state.columnDefinitions);
  return {...state, pagination: {...state.pagination, page: 0}, columnDefinitions};
}
