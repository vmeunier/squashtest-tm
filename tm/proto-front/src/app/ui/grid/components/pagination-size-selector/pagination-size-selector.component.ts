///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'sqtm-pagination-size-selector',
  template: `
    <select (change)="handleChange($event.target.value)">
      <option *ngFor="let size of allowedPageSizes" [selected]="size === pageSize">{{size}}</option>
    </select>
  `,
  styleUrls: ['./pagination-size-selector.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PaginationSizeSelectorComponent implements OnInit {

  @Input()
  pageSize: number;

  @Input()
  allowedPageSizes: number [];

  @Output()
  changeSize = new EventEmitter<number>();

  constructor() {
  }

  ngOnInit() {  }

  handleChange(value: any) {
    this.changeSize.emit(Number.parseInt(value, 10));
  }
}
