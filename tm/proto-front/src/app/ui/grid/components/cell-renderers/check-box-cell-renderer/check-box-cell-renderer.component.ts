///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {Component, OnInit, ChangeDetectionStrategy, Input, Inject} from '@angular/core';
import {ColumnDisplay} from '@ui/grid/store/model/column-display.model';
import {RowDisplay} from '@ui/grid/store/model/row-display.model';
import {GridDisplay} from '@ui/grid/store/model/grid-display.model';
import {GRID_FACADE} from '@ui/grid/token';
import {GridFacade} from '@ui/grid/store/grid-facade';
import {GridType} from '@ui/grid/store/reducers/state/config.state';
import {CellRenderer} from '@ui/grid/interfaces/cell-renderer';

@Component({
  selector: 'sqtm-check-box-cell-renderer',
  template: `
    <div [ngClass]="getCellClass()" [ngStyle]="getCellStyle(columnDisplay)">
      <input type="checkbox" [checked]="row.data.selected" (click)="toggle()">
    </div>
  `,
  styleUrls: ['./check-box-cell-renderer.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CheckBoxCellRendererComponent implements OnInit, CellRenderer {

  @Input()
  columnDisplay: ColumnDisplay;

  @Input()
  row: RowDisplay;

  @Input()
  gridDisplay: GridDisplay;

  constructor(@Inject(GRID_FACADE) public grid: GridFacade) {
  }

  ngOnInit() {
  }

  getCellClass() {
    if (this.gridDisplay.gridType === GridType.TABLE) {
      return ['sqtm-table-cell'];
    }
    return ['sqtm-grid-cell'];
  }

  getCellStyle(columnDisplay: ColumnDisplay) {
    if (this.gridDisplay.gridType === GridType.TABLE) {
      return {
        width: `${columnDisplay.width}px`,
      };
    }
    return {
      width: `${columnDisplay.width}px`,
      left: `${columnDisplay.left}px`
    };
  }

  /**
   * The tricks here is to prevent the dom to naturally toggle the box we return false in the handler
   * Witch is equivalent to prevent default.
   * So the state is afterward set by the store. If we do not do that, the update from the store is eat by the event handler
   * and the checkbox is not properly updated even if the state is correct in store, and the @Input value have changed.
   */
  toggle() {
    setTimeout(_ => {
      // by emitting the action in a time out, we allow the native event handler to complete, and the state update is not lost.
      this.grid.toggleRowSelection(this.row.id);
    });
    // Returning false, so the checkbox state in the DOM not modified.
    // It will be modified by the store selecor once the action is reduced by reducers and selectors emit the new node value.
    return false;
  }
}
