///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {ChangeDetectionStrategy, Component, Inject, Input, OnInit} from '@angular/core';
import {ColumnDisplay} from '../../../store/model/column-display.model';
import {RowDisplay} from '../../../store/model/row-display.model';
import {CellRenderer} from '../../../interfaces/cell-renderer';
import {DataRowOpenState} from '../../../store/model/data-row.model';
import {GridFacade} from '../../../store/grid-facade';
import {GRID_FACADE} from '../../../token';
import {GridDisplay} from '../../../store/model/grid-display.model';

@Component({
  selector: 'sqtm-tree-hierarchy-cell-renderer',
  template: `
    <div class="sqtm-grid-cell" [ngStyle]="getCellStyle(columnDisplay)">
      <div class="sqtm-grid-cell-txt">
        {{row.data[columnDisplay.id]}}
      </div>
      <div class="sqtm-grid-cell-icon" (click)="toggleRow($event)">
        <fa-icon [icon]="iconName()" *ngIf="row.state != 'leaf'"></fa-icon>
      </div>
    </div>
  `,
  styleUrls: ['./tree-hierarchy-cell-renderer.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TreeHierarchyCellRendererComponent implements OnInit, CellRenderer {

  @Input()
  columnDisplay: ColumnDisplay;

  @Input()
  row: RowDisplay;

  @Input()
  gridDisplay: GridDisplay;

  constructor(@Inject(GRID_FACADE) public grid: GridFacade) {
  }

  ngOnInit() {
  }

  getCellStyle(columnDisplay: ColumnDisplay) {
    const padding = this.row.depth ? this.row.depth * 10 : 0;
    return {
      width: `${columnDisplay.width}px`,
      'padding-left': `${padding}px`,
      left: `${columnDisplay.left}px`
    };
  }

  iconName() {
    if (this.row.state === DataRowOpenState.closed) {
      return 'angle-right';
    } else {
      return 'angle-down';
    }
  }

  toggleRow($event) {
    if (this.row.state === DataRowOpenState.closed) {
      this.grid.openRow(this.row.id);
    } else if (this.row.state === DataRowOpenState.open) {
      this.grid.closeRow(this.row.id);
    }
  }
}
