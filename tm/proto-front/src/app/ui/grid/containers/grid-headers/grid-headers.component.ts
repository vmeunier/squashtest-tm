///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {ChangeDetectionStrategy, Component, Inject, Input, OnInit} from '@angular/core';
import {GRID_FACADE} from '@ui/grid/token';
import {GridFacade} from '@ui/grid/store/grid-facade';
import {Observable} from 'rxjs';
import {GridDisplay} from '@ui/grid/store/model/grid-display.model';
import {ColumnDisplay} from '@ui/grid/store/model/column-display.model';
import {ColumnSort} from '@ui/grid/store/model/column-definition.model';

@Component({
  selector: 'sqtm-grid-headers',
  template: `
    <!-- headers contains all columns headers and if time to do it, headers groups -->
    <div class="sqtm-grid-headers" *ngIf="gridDisplay$ | async as gridDisplay">
      <!-- left pinned columns headers to implements later -->
      <div></div>
      <!-- viewport of the headers -->
      <div class="sqtm-grid-header-viewport sqtm-grid-std-layout">
        <div class="sqtm-grid-std-layout" style="height: 25px;">
          <div class="sqtm-grid-header-row" [ngStyle]="getRowHeaderStyle(gridDisplay.rowHeight)">
            <ng-container *ngFor="let columnDisplay of gridDisplay.columnDisplays">
              <div class="sqtm-grid-cell" [ngStyle]="getCellStyle(columnDisplay)" (click)="handleClick(columnDisplay)">
                <b *ngIf="columnDisplay.i18nKey; else displayId">{{columnDisplay.i18nKey | translate}}</b>
                <ng-template #displayId>
                  <b>No 18nKey for : {{columnDisplay.id}}</b>
                </ng-template>
                <fa-icon icon="arrow-up" size="xs" *ngIf="columnDisplay.sort === sorts.ASC"></fa-icon>
                <fa-icon icon="arrow-down" size="xs" *ngIf="columnDisplay.sort === sorts.DESC"></fa-icon>
              </div>
            </ng-container>
          </div>
        </div>
      </div>
      <!-- right pinned columns headers to implements later -->
      <div></div>
    </div>
  `,
  styleUrls: ['./grid-headers.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GridHeadersComponent implements OnInit {

  @Input()
  viewPortScrollX: number;

  // observables from store
  gridDisplay$: Observable<GridDisplay>;

  sorts = ColumnSort;

  constructor(@Inject(GRID_FACADE) public grid: GridFacade) {
    this.gridDisplay$ = this.grid.gridDisplay$;
  }

  ngOnInit() {
  }

  getCellStyle(columnDisplay: ColumnDisplay) {
    return {
      width: `${columnDisplay.width}px`,
      left: `${columnDisplay.left}px`
    };
  }

  getRowHeaderStyle(rowHeight: number) {
    return {
      left: `${this.viewPortScrollX * -1}px`,
      height: `${rowHeight}px`
    };
  }

  handleClick(columndisplay: ColumnDisplay) {
    if (columndisplay.sortable) {
      this.grid.toggleColumnSort(columndisplay.id);
    }
  }

}
