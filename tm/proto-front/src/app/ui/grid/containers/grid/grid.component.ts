///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {AfterViewInit, ChangeDetectionStrategy, Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {GRID_FACADE} from '../../token';
import {GridFacade} from '../../store/grid-facade';
import {BehaviorSubject, combineLatest, Observable} from 'rxjs';
import {ColumnDisplay} from '../../store/model/column-display.model';
import {RowDisplay} from '../../store/model/row-display.model';
import {PaginationDisplay} from '../../store/model/pagination-display.model';
import {GridDisplay} from '../../store/model/grid-display.model';
import {GridType} from '../../store/reducers/state/config.state';
import {GridViewportDirective} from '../../directives/grid-viewport.directive';
import {map} from 'rxjs/operators';

@Component({
  selector: 'sqtm-grid',
  template: `
    <div class="sqtm-grid">
      <!-- root wrapper contain the grid itself plus fix header/footer like pagination.-->
      <!--flex is column so footer will be nicely placed on foot -->
      <div class="sqtm-grid-root-wrapper sqtm-grid-std-layout" *ngIf="gridDisplay$ | async as gridDisplay">
        <!-- root panel contains the grid core and lateral panels. flex is row -->
        <div class="sqtm-grid-root">
          <!-- core panel contains the headers and the grid itself-->
          <!-- flex is column -->
          <div class="sqtm-grid-core sqtm-grid-std-layout">
            <!-- headers contains all columns headers and if time to do it, headers groups -->
            <sqtm-grid-headers [viewPortScrollX]="viewPortScrollX"></sqtm-grid-headers>
            <!-- body of the grid. flex row as we can have pinned columns -->
            <div class="sqtm-grid-body sqtm-grid-std-layout">
              <!-- left pinned columns to implements later -->
              <div></div>
              <!-- wrapper of the row viewport -->
              <!-- overflow hidden so the grid will no grow indefinitely -->
              <div class="sqtm-grid-row-viewport-wrapper sqtm-grid-std-layout">
                <!-- viewport containing the rows -->
                <!-- overflow scroll so the scrollbar will be inner to that tag -->
                <!-- view port is not resizeable all the content will be overflowed -->
                <div sqtmGridViewport class="sqtm-grid-row-viewport" (scroll)="viewportScroll($event)">
                  <!-- row container that will be resized with row content -->
                  <!-- position is relative so rows can use absolute placement -->

                  <div *ngIf="(rowSlice$ | async) as rowSlice" [ngClass]="getRowContainerClass(gridDisplay)"
                       [ngStyle]="getRowContainerStyle(rowSlice.totalLines, gridDisplay)">

                    <ng-container *ngFor="let row of rowSlice.rows; trackBy: trackByFn">
                      <sqtm-grid-row [gridDisplay]="gridDisplay" [index]="row.index" [row]="row"></sqtm-grid-row>
                    </ng-container>
                  </div>
                </div>
              </div>
              <!-- right pinned columns to implements later -->
              <div></div>
              <!-- this one will be full size viewport for absolute placement of other stuff -->
              <!-- will be used for pinned lines, internal line stuff and so on -->
              <!-- it's like a second layer to place elements above rows in main grid -->
              <div class="sqtm-grid-row-viewport-wrapper-full-width">

              </div>
            </div>
          </div>
          <!-- lateral panel 1 will probably be used one day -->
          <div class="sqtm-grid-side-panel sqtm-grid-std-layout">HERE IS THE LATERAL PANEL</div>
        </div>
        <ng-container *ngIf="pagination$ | async as pagination">
          <ng-container *ngIf="pagination.active">
            <div class="sqtm-grid-footer">
              <span *ngIf="pagination.page > 0" (click)="handlePrevious()">PREVIOUS</span> -
              <span *ngIf="pagination.page < pagination.maxPage" (click)="handleNext()">NEXT</span>
              <sqtm-pagination-size-selector
                [allowedPageSizes]="pagination.allowedPageSizes"
                [pageSize]="pagination.pageSize"
                (changeSize)="handlePaginationSizeChange($event)">
              </sqtm-pagination-size-selector>
            </div>
          </ng-container>
        </ng-container>
      </div>
    </div>
    <!--<div>{{viewPortScrollX}}</div>-->
    <!--<div>{{pagination$ | async | json}}</div>-->
    <!--<div>{{gridDisplay$ | async | json}}</div>-->
    <!--<div>row slice {{rowSlice$ | async | json}}</div>-->
    <!--<div>rows before slice {{grid.rows$ | async | json}}</div>-->
    <!--<div>Ui scroll {{uiScroll$ | async | json}}</div>-->
  `,
  styleUrls: ['./grid.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GridComponent implements OnInit, AfterViewInit {

  // observables from store
  gridDisplay$: Observable<GridDisplay>;
  rowSlice$: Observable<RowSlice>;
  pagination$: Observable<PaginationDisplay>;

  // local state
  viewPortScrollX = 0;

  @ViewChild(GridViewportDirective, {read: ElementRef})
  viewport;

  uiScroll$: BehaviorSubject<[number, number]>;

  initialViewportSize = 0;

  constructor(@Inject(GRID_FACADE) public grid: GridFacade) {
  }

  ngOnInit() {
    this.uiScroll$ = new BehaviorSubject<[number, number]>([0, 0]);
    this.gridDisplay$ = this.grid.gridDisplay$;
    this.rowSlice$ = combineLatest(this.uiScroll$, this.grid.rows$, this.grid.gridDisplay$).pipe(
      map(([[viewportHeight, viewportScrollTop], rows, gridDisplay]) => {
        if (gridDisplay.gridType === GridType.TABLE) {
          return {totalLines: rows.length, rows};
        }
        const lineMin = Math.max((viewportScrollTop / gridDisplay.rowHeight) - 30, 0);
        const lineMax = Math.min(((viewportScrollTop + viewportHeight) / gridDisplay.rowHeight) + 30, rows.length);
        return {
          totalLines: rows.length,
          rows: rows.slice(lineMin, lineMax)
        };
      }),
    );
    this.pagination$ = this.grid.pagination$;
  }

  getCellStyle(columnDisplay: ColumnDisplay) {
    return {
      width: `${columnDisplay.width}px`,
      left: `${columnDisplay.left}px`
    };
  }

  viewportScroll($event: Event) {
    this.viewPortScrollX = $event.srcElement.scrollLeft;
    this.emitScroll();
  }

  handlePrevious() {
    this.grid.paginatePreviousPage();
  }

  handleNext() {
    this.grid.paginateNextPage();
  }

  handlePaginationSizeChange(size: number) {
    this.grid.changePaginationSize(size);
  }

  trackByFn(index: number, node: RowDisplay) {
    return node.id;
  }

  getRowContainerClass(gridDisplay: GridDisplay) {
    const cssClasses = ['sqtm-grid-std-layout'];
    if (gridDisplay.gridType === GridType.TABLE) {
      cssClasses.push('sqtm-table-row-container');
    }
    return cssClasses;
  }

  getRowContainerStyle(totalRow: number, gridDisplay: GridDisplay) {
    const computedSize = totalRow * gridDisplay.rowHeight;
    return {
      height: `${Math.max(this.initialViewportSize, computedSize)}px`
    };
  }

  ngAfterViewInit(): void {
    this.initialViewportSize = this.viewport.nativeElement.clientHeight;
    this.emitScroll();
  }

  emitScroll(): void {
    // console.log('emit scroll ', [this.viewport.nativeElement.clientHeight, this.viewport.nativeElement.scrollTop]);
    this.uiScroll$.next([this.viewport.nativeElement.clientHeight, this.viewport.nativeElement.scrollTop]);
  }

}

interface RowSlice {
  totalLines: number;
  rows: RowDisplay[];
}
