///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter} from '@angular/core';
import {TreeNode, TreeNodeUtils} from '@tree/store';
import isDraggable = TreeNodeUtils.isDraggable;

@Component({
  selector: 'sqtm-tree-node-display',
  template: `
    <div [draggable]="draggable" (dragstart)="handleDragStart($event)" (click)="handleClick($event)" (dblclick)="handleDblClick($event)">
      <div class="tree-node">
        <span>{{treeNode.name}}</span>
      </div>
    </div>
  `,
  styleUrls: ['./tree-node-display.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TreeNodeDisplayComponent implements OnInit {

  @Input()
  treeNode: TreeNode;

  @Output()
  clickNode = new EventEmitter();

  @Output()
  clickNodeCtrl = new EventEmitter();

  @Output()
  dblClickNode = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  handleClick($event: PointerEvent) {
    if ($event.ctrlKey) {
      this.clickNodeCtrl.emit();
    } else {
      this.clickNode.emit();
    }
  }

  handleDblClick($event) {
    this.dblClickNode.emit();
  }

  handleDragStart($event: DragEvent) {
    console.log('statr drag');
    // $event.dataTransfer.effectAllowed = 'all';
    $event.dataTransfer.setData('text', 'ABC');
  }

  get draggable(): boolean {
    return this.treeNode && isDraggable(this.treeNode);
  }


}
