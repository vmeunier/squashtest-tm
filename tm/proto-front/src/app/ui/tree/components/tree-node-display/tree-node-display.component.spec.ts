///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TreeNodeDisplayComponent} from './tree-node-display.component';
import {NodeState, NodeType} from '@tree/store';

describe('TreeNodeDisplayComponent', () => {
  let component: TreeNodeDisplayComponent;
  let fixture: ComponentFixture<TreeNodeDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TreeNodeDisplayComponent]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(TreeNodeDisplayComponent);
        component = fixture.componentInstance;
        component.treeNode = {
          nodeId: 'TestCaseLibrary-1',
          id: 1,
          type: NodeType.TestCaseLibrary,
          name: 'Project-1',
          state: NodeState.closed,
          children: [],
          projectId: 1
        };
        fixture.detectChanges();
        }
      );
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
