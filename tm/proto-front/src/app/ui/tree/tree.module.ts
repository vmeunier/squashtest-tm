///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import * as fromStore from './store';
import {EffectsModule} from '@ngrx/effects';
import {TreeComponent} from '@tree/containers/tree/tree.component';
import {TreeNodeComponent} from '@tree/containers/node/tree-node.component';
import {NodeListComponent} from '@tree/containers/node-list/node-list.component';
import {TreeService} from '@tree/service/tree.service';
import { TreeNodeDisplayComponent } from './components/tree-node-display/tree-node-display.component';
import { TreeNodeDraggableDirective } from './directive/tree-node-draggable.directive';

@NgModule({
  imports: [
    CommonModule,
    EffectsModule.forFeature(fromStore.effects)
  ],
  declarations: [TreeComponent, TreeNodeComponent, NodeListComponent, TreeNodeDisplayComponent, TreeNodeDraggableDirective],
  exports: [TreeComponent],
  providers: [TreeService]
})
export class TreeModule { }
