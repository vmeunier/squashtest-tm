import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TreeNodeComponent} from './tree-node.component';
import {TreeNodeDisplayComponent} from '@tree/components/tree-node-display/tree-node-display.component';
import {NodeListComponent} from '@tree/containers/node-list/node-list.component';
import {instance, mock, when} from 'ts-mockito';
import {TreeFacade} from '@tree/store/tree.facade';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TREE_FACADE} from '@tree/token';
import {of} from 'rxjs';
import {NodeState, NodeType} from '@tree/store';

describe('NodeComponent', () => {
  let component: TreeNodeComponent;
  let fixture: ComponentFixture<TreeNodeComponent>;

  const treeFacade = mock(TreeFacade);

  beforeEach(done => {
    when(treeFacade.getNode('TestCaseLibrary-1')).thenReturn(of({
        nodeId: 'TestCaseLibrary-1',
        id: 1,
        type: NodeType.TestCaseLibrary,
        name: 'Project-1',
        state: NodeState.closed,
        children: [],
        projectId: 1
      }
    ));
    done();
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TreeNodeComponent, TreeNodeDisplayComponent, NodeListComponent],
      providers: [
        {
          provide: TREE_FACADE,
          useFactory: () => instance(treeFacade)
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreeNodeComponent);
    component = fixture.componentInstance;
    component.nodeId = 'TestCaseLibrary-1';
    fixture.detectChanges();
  });

  it('should create', () => {
    console.log('testing mocking', treeFacade.getNode('TestCaseLibrary-1'));
    expect(component).toBeTruthy();
  });
});
