import {ChangeDetectionStrategy, Component, Inject, Input, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {TreeFacade} from '@tree/store/tree.facade';
import {TREE_FACADE} from '@tree/token';
import {TreeNode, TreeNodeUtils} from '@tree/store';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'sqtm-tree-node',
  template: `
    <div class="sqtm-tree-node" *ngIf="treeNode$ | async as treeNode">
      <sqtm-tree-node-display [treeNode]="treeNode" (clickNode)="selectNode()" (clickNodeCtrl)="toggleNode()"
                              (dblClickNode)="openNode()"></sqtm-tree-node-display>
      <sqtm-node-list [nodeIds]="treeNode.children"></sqtm-node-list>
    </div>
  `,
  styleUrls: ['./tree-node.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TreeNodeComponent implements OnInit, OnDestroy {

  @Input()
  nodeId: string;

  @Input()
  index: number;

  /**
   * Observable<TreeNode> from store.
   * There is two subscriptions on this component :
   * One with async pipe in the template
   * One inside the component itself to avoid passing the node inside each event handler
   */
  treeNode$: Observable<TreeNode>;

  /**
   * This TreeNode is updated by the the manual sub to the store.
   */
  private treeNode: TreeNode;

  /**
   * This subject controls all subscription for this component.
   * Used with takeUntil as pipe on all input observables
   * It emit only one time in OnDestroy callback, and thus all subscription are canceled
   */
  private unsub$ = new Subject();

  constructor(@Inject(TREE_FACADE) private tree: TreeFacade) {
  }

  ngOnInit() {
    this.treeNode$ = this.tree.getNode(this.nodeId).pipe(
      takeUntil(this.unsub$)
    );
    // manual sub
    this.treeNode$.subscribe(
      value => this.treeNode = value
    );
  }

  toggleNode() {
    this.tree.toggleNodeInSelection(this.nodeId);
  }

  selectNode() {
    this.tree.selectNode(this.nodeId);
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  openNode() {
    if (TreeNodeUtils.couldBeOpen(this.treeNode)) {
      this.tree.openNode(this.nodeId);
    }
  }
}
