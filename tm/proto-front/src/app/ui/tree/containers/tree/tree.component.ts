///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {ChangeDetectionStrategy, Component, Inject, Input, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {TreeNode} from '../../store';
import {map, take} from 'rxjs/operators';
import {TreeFacade} from '@tree/store/tree.facade';
import {TREE_FACADE} from '@tree/token';

@Component({
  selector: 'sqtm-tree',
  template: `
    <div class="sqtm-tree">
      <sqtm-node-list [nodeIds]="libraryIds$ | async"></sqtm-node-list>
    </div>
  `,
  styleUrls: ['./tree.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TreeComponent implements OnInit {

  libraryIds$: Observable<string[]>;

  constructor(@Inject(TREE_FACADE) private tree: TreeFacade) {

  }

  ngOnInit() {
    this.libraryIds$ = this.tree.rootNode$.pipe(
      map(nodes => nodes.map(node => node.nodeId)),
      take(1)
    );
  }

}
