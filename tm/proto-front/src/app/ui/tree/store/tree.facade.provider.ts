///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {Store} from '@ngrx/store';
import {initialState, reducer, TreeState} from './reducers';
import {TreeFacade} from './tree.facade';
import {TreeNodeActions} from './actions';
import {createComponentReducer} from '@common/store/factories/reducers/reducers-factory';
import {TreeType} from './tree-types';

/**
 * Return a function that will be called by angular DI system.
 * To use it, you need to add the provided service to your provider module :
 * @NgModule(
 *  providers : [
 *  ...
 *  {
 *    provide : TreeFacade,
      useFactory: treeFacadeProvider(<YOUR_PAGE_ROOT_URL>, <THE_ID_OF_YOUR_TREE_COMPONENT>),
      deps: [Store] //Don't forget to provide a deps to the ngrx store, as angular need it to properly inject it into the service
 *  }
 *  ...
 *  ]
 * }
 *
 * Note : If you have several trees inside your page, you should use DI token to pass the facade corresponding to your tree component.
 *
 * @param rootUrl the URL of the page, witch should be unique across all app.
 * Take care of nested url that will make poor reducer keys !
 * If needed you can provide any value here but be aware that it will be used as feature key inside Store feature module.
 * So this identifier needs to be consistent with your modules and unique across all app to prevent clashing.
 * The generated facade will bind it's actions, selectors to the rootUrl and the componentId provided.
 * @param componentId the component id.
 * @param treeType the type of tree (ie test case, requirement...)
 * @returns A factory function that angular will call to instantiate the facade.
 */
export function treeFacadeProvider(rootUrl: string, componentId: string, treeType: TreeType) {
  return (store: Store<TreeState>) => {
    return new TreeFacade(rootUrl, componentId, treeType, store);
  };
}

export function createTreeReducer(rootUrl: string, componentId: string): (state: TreeState, action: TreeNodeActions) => TreeState {
  return createComponentReducer<TreeState, TreeNodeActions>(rootUrl, componentId, reducer, initialState);
}
