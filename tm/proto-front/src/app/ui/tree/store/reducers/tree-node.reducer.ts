///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {createEntityAdapter, Dictionary, EntityAdapter, EntityState} from '@ngrx/entity';
import {TreeNode} from '../tree-node.model';
import {ToggleTreeNode, TreeNodeActions, TreeNodeActionTypes} from '../actions';

export interface TreeState extends EntityState<TreeNode> {
  // additional entities state properties
  selectedNodes: string[];
}

export const adapter: EntityAdapter<TreeNode> = createEntityAdapter<TreeNode>({
  selectId: (treeNode: TreeNode) => treeNode.nodeId
});

export const initialState: TreeState = adapter.getInitialState({
  // additional entity state properties
  selectedNodes: []
});


export function reducer(
  state = initialState,
  action: TreeNodeActions
): TreeState {
  switch (action.type) {
    case TreeNodeActionTypes.AddTreeNode: {
      return adapter.addOne(action.payload.treeNode, state);
    }

    case TreeNodeActionTypes.UpsertTreeNode: {
      return adapter.upsertOne(action.payload.treeNode, state);
    }

    case TreeNodeActionTypes.AddTreeNodes: {
      return adapter.addMany(action.payload.treeNodes, state);
    }

    case TreeNodeActionTypes.UpsertTreeNodes: {
      return adapter.upsertMany(action.payload.treeNodes, state);
    }

    case TreeNodeActionTypes.UpdateTreeNode: {
      return adapter.updateOne(action.payload.treeNode, state);
    }

    case TreeNodeActionTypes.UpdateTreeNodes: {
      return adapter.updateMany(action.payload.treeNodes, state);
    }

    case TreeNodeActionTypes.DeleteTreeNode: {
      return adapter.removeOne(action.payload.id, state);
    }

    case TreeNodeActionTypes.DeleteTreeNodes: {
      return adapter.removeMany(action.payload.ids, state);
    }

    case TreeNodeActionTypes.LoadTreeNodes: {
      return adapter.addAll(action.payload.treeNodes, state);
    }

    case TreeNodeActionTypes.ClearTreeNodes: {
      return adapter.removeAll(state);
    }

    case TreeNodeActionTypes.ToggleTreeNode: {
      return doToggleTreeNode(state, action);
    }

    case TreeNodeActionTypes.SelectTreeNode: {
      return {...state, selectedNodes: [action.payload.id]};
    }

    default: {
      return state;
    }
  }
}

function doToggleTreeNode(state, action: ToggleTreeNode) {
  let selectedNodes = [...state.selectedNodes];
  const id = action.payload.id;
  // if node was previously selected, we just toggle it
  if (selectedNodes.includes(id)) {
    selectedNodes = selectedNodes.filter(value => value !== id);
    // else :
    // if we have already selected an ancestor of the selected node we must ignore the selection and do nothing.
    // else deselect all previously selected children and sub-children and select the given node.
  } else {
    const selectedNode = state.entities[id];
    const ancestors = getAncestors(state.entities, selectedNode);
    const intersection = [...state.selectedNodes].filter(nodeId => ancestors.includes(nodeId));
    // if the selection is not forbidden
    // ie if no ancestor of selected node was previously selected
    if (intersection.length === 0) {
      const descendantIds = getDescendants(state.entities, [selectedNode]);
      // filter all previously selected nodes that are descendant of the toggled node
      selectedNodes = selectedNodes.filter(nodeId => !descendantIds.includes(nodeId));
      selectedNodes.push(id);
    }
  }
  return {...state, selectedNodes};
}

export function getDescendants(entities: Dictionary<TreeNode>, ancestors: TreeNode[]): string[] {
  const descendantIds = new Set<string>([]);
  let childrenIds;
  let nextLayerIds = [];
  ancestors.forEach(value => nextLayerIds.push(...value.children));
  do {
    childrenIds = [...nextLayerIds];
    nextLayerIds = [];
    for (const childrenId of childrenIds) {
      descendantIds.add(childrenId);
      const node = entities[childrenId];
      nextLayerIds.push(...node.children);
    }
  } while (nextLayerIds.length > 0);

  return [...descendantIds];
}

export function getAncestors(entities: Dictionary<TreeNode>, descendant: TreeNode): string[] {
  const ancestorIds = new Set<string>([]);
  let parentId: string = descendant.parentNodeId;
  while (parentId) {
    ancestorIds.add(parentId);
    const entity = entities[parentId];
    parentId = entity.parentNodeId;
  }

  return [...ancestorIds];
}

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors();
