///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {reducer, initialState, getDescendants, getAncestors} from './tree-node.reducer';
import {NodeState, NodeType, SelectTreeNode, ToggleTreeNode, TreeNode} from '@tree/store';
import {Dictionary} from '@ngrx/entity';

describe('TreeNode Reducer', () => {
  describe('unknown action', () => {
    it('should return the initial state', () => {
      const action = {} as any;

      const result = reducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });

  describe('[TreeNode] Toggle TreeNode', () => {
    it('should add a node', () => {
      const action: ToggleTreeNode = new ToggleTreeNode('', '', {id: 'TestCase-12'});

      const entities = getEntities();
      const result = reducer({...initialState, entities}, action);

      expect(result.entities).toBe(entities);
      expect(result.ids).toBe(initialState.ids);
      expect(result.selectedNodes).toEqual(['TestCase-12']);

    });

    it('should add a node in existing selection', () => {
      const action: ToggleTreeNode = new ToggleTreeNode('', '', {id: 'TestCase-8'});

      const entities = getEntities();
      const result = reducer({...initialState, entities, selectedNodes: ['TestCaseLibrary-2', 'TestCase-9']}, action);

      expect(result.entities).toBe(entities);
      expect(result.ids).toBe(initialState.ids);
      expect(result.selectedNodes).toEqual(['TestCaseLibrary-2', 'TestCase-9', 'TestCase-8']);

    });

    it('should remove a node in selection', () => {
      const action: ToggleTreeNode = new ToggleTreeNode('', '', {id: 'TestCase-1'});

      const result = reducer({
        ...initialState,
        selectedNodes: ['TestCaseLibrary-1', 'TestCaseLibrary-2', 'TestCase-1', 'TestCase-2', 'TestCaseFolder-2']
      }, action);

      expect(result.entities).toBe(initialState.entities);
      expect(result.ids).toBe(initialState.ids);
      expect(result.selectedNodes).toEqual(['TestCaseLibrary-1', 'TestCaseLibrary-2', 'TestCase-2', 'TestCaseFolder-2']);

    });

    it('should prevent node selection if an ancestor library is selected', () => {
      const action: ToggleTreeNode = new ToggleTreeNode('', '', {id: 'TestCase-12'});

      const result = reducer({
        ...initialState,
        entities: getEntities(),
        selectedNodes: ['TestCaseLibrary-1', 'TestCaseLibrary-2']
      }, action);

      expect(result.entities).toEqual(getEntities());
      expect(result.ids).toBe(initialState.ids);
      expect(result.selectedNodes).toEqual(['TestCaseLibrary-1', 'TestCaseLibrary-2']);

    });

    it('should prevent node selection if an ancestor folder is selected', () => {
      const action: ToggleTreeNode = new ToggleTreeNode('', '', {id: 'TestCase-9'});

      const result = reducer({
        ...initialState,
        entities: getEntities(),
        selectedNodes: ['TestCaseFolder-3', 'TestCaseLibrary-2']
      }, action);

      expect(result.entities).toEqual(getEntities());
      expect(result.ids).toBe(initialState.ids);
      expect(result.selectedNodes).toEqual(['TestCaseFolder-3', 'TestCaseLibrary-2']);

    });

  });

  describe('[TreeNode] Select TreeNode', () => {
    it('should select node', () => {
      const action: SelectTreeNode = new SelectTreeNode('', '', {id: 'TestCase-1'});

      const result = reducer(initialState, action);

      expect(result.entities).toBe(initialState.entities);
      expect(result.ids).toBe(initialState.ids);
      expect(result.selectedNodes).toEqual(['TestCase-1']);


    });


    it('should clear selection and select node', () => {
      const action: SelectTreeNode = new SelectTreeNode('', '', {id: 'TestCase-1'});

      const result = reducer({
        ...initialState,
        selectedNodes: ['TestCaseLibrary-1', 'TestCaseLibrary-2', 'TestCase-1', 'TestCase-2', 'TestCaseFolder-2']
      }, action);

      expect(result.entities).toBe(initialState.entities);
      expect(result.ids).toBe(initialState.ids);
      expect(result.selectedNodes).toEqual(['TestCase-1']);


    });

    it('should be idempotent', () => {
      const action: SelectTreeNode = new SelectTreeNode('', '', {id: 'TestCase-1'});

      const result = reducer({
        ...initialState,
        selectedNodes: ['TestCase-1']
      }, action);

      expect(result.entities).toBe(initialState.entities);
      expect(result.ids).toBe(initialState.ids);
      expect(result.selectedNodes).toEqual(['TestCase-1']);


    });
  });
});

/**
 * Tests of getDescendant method
 */
describe('TreeNode Get Descendant', () => {
  it('should return empty array', () => {
    const node = {
      nodeId: 'TestCaseLibrary-1',
      id: 1,
      type: NodeType.TestCaseLibrary,
      name: 'Project-1',
      state: NodeState.closed,
      children: [],
      projectId: 1
    };
    const entities: Dictionary<TreeNode> = {
      'TestCaseLibrary-1': node
    };

    const ancestorId = 'TestCaseLibrary-1';
    const result = getDescendants(entities, [node]);
    expect(result).toEqual([]);
    expect(entities).toBe(entities);
  });

  it('should return first level children', () => {
    const tcl1 = {
      nodeId: 'TestCaseLibrary-1',
      id: 1,
      type: NodeType.TestCaseLibrary,
      name: 'Project-1',
      state: NodeState.closed,
      children: ['TestCase-1', 'TestCaseFolder-1', 'TestCaseFolder-2'],
      projectId: 1
    };

    const tcl2 = {
      nodeId: 'TestCaseLibrary-2',
      id: 1,
      type: NodeType.TestCaseLibrary,
      name: 'Project-1',
      state: NodeState.closed,
      children: ['TestCase-10', 'TestCaseFolder-10'],
      projectId: 2
    };

    const tcf1 = {
      nodeId: 'TestCaseFolder-1',
      id: 1,
      type: NodeType.TestCaseFolder,
      name: 'Folder 1',
      state: NodeState.closed,
      children: [],
      projectId: 1
    };

    const tcf2 = {
      nodeId: 'TestCaseFolder-2',
      id: 2,
      type: NodeType.TestCaseFolder,
      name: 'Folder 2',
      state: NodeState.closed,
      children: [],
      projectId: 1
    };

    const tcf10 = {
      nodeId: 'TestCaseFolder-10',
      id: 10,
      type: NodeType.TestCaseFolder,
      name: 'Folder 10',
      state: NodeState.closed,
      children: [],
      projectId: 2
    };

    const tc1 = {
      nodeId: 'TestCase-1',
      id: 1,
      type: NodeType.TestCase,
      name: 'TestCase 1',
      state: NodeState.leaf,
      children: [],
      projectId: 1
    };

    const tc10 = {
      nodeId: 'TestCase-10',
      id: 10,
      type: NodeType.TestCase,
      name: 'TestCase 10',
      state: NodeState.leaf,
      children: [],
      projectId: 2
    };

    const entities: Dictionary<TreeNode> =
      [tcl1, tcf1, tcf2, tc1, tcl2, tc10, tcf10].reduce((accumulation: Dictionary<TreeNode>, currentNode: TreeNode) => {
        accumulation[currentNode.nodeId] = currentNode;
        return accumulation;
      }, {});

    let result = getDescendants(entities, [tcl1]);
    expect(result).toEqual(['TestCase-1', 'TestCaseFolder-1', 'TestCaseFolder-2']);
    expect(entities).toBe(entities);

    result = getDescendants(entities, [tcl2]);
    expect(result).toEqual(['TestCase-10', 'TestCaseFolder-10']);

  });

  it('should return deep nested children of libraries', () => {

    const entities = getEntities();

    let result = getDescendants(entities, [entities['TestCaseLibrary-1']]);
    expect(result.sort()).toEqual(
      ['TestCase-12', 'TestCaseFolder-1', 'TestCaseFolder-2', 'TestCaseFolder-3', 'TestCase-4',
        'TestCase-5', 'TestCaseFolder-6', 'TestCaseFolder-7', 'TestCase-8', 'TestCase-9'].sort());
    expect(entities).toBe(entities);

    result = getDescendants(entities, [entities['TestCaseLibrary-2']]);
    expect(result.sort()).toEqual(
      ['TestCase-10', 'TestCaseFolder-11'].sort());
    expect(entities).toBe(entities);

  });

  it('should return children of nested folders', () => {

    const entities = getEntities();

    let result = getDescendants(entities, [entities['TestCaseFolder-7']]);
    expect(result.sort()).toEqual(
      ['TestCase-9'].sort());

    result = getDescendants(entities, [entities['TestCaseFolder-3']]);
    expect(result.sort()).toEqual(
      ['TestCaseFolder-7', 'TestCase-8', 'TestCase-9'].sort());

    result = getDescendants(entities, [entities['TestCase-9']]);
    expect(result.sort()).toEqual(
      [].sort());
  });

  it('should return children of multiple nested folders', () => {

    const entities = getEntities();

    let result = getDescendants(entities, [entities['TestCaseFolder-7'], entities['TestCaseFolder-3']]);
    expect(result.sort()).toEqual(
      ['TestCaseFolder-7', 'TestCase-8', 'TestCase-9'].sort());

    result = getDescendants(entities, [entities['TestCaseLibrary-1'], entities['TestCaseLibrary-2']]);
    expect(result.sort()).toEqual(
      ['TestCase-12', 'TestCaseFolder-1', 'TestCaseFolder-2', 'TestCaseFolder-3',
        'TestCase-4', 'TestCase-5', 'TestCaseFolder-6', 'TestCaseFolder-7',
        'TestCase-8', 'TestCase-9', 'TestCase-10', 'TestCaseFolder-11'].sort());
  });


});

describe('TreeNode Get Ancestors', () => {
  it('should return empty array for libraries', () => {

    const entities = getEntities();

    let result = getAncestors(entities, entities['TestCaseLibrary-1']);
    expect(result.sort()).toEqual(
      []);

    result = getAncestors(entities, entities['TestCaseLibrary-2']);
    expect(result.sort()).toEqual(
      []);
  });

  it('should return single value arrays for first level nodes', () => {

    const entities = getEntities();

    let result = getAncestors(entities, entities['TestCase-12']);
    expect(result).toEqual(
      ['TestCaseLibrary-1']);

    result = getAncestors(entities, entities['TestCaseFolder-11']);
    expect(result).toEqual(
      ['TestCaseLibrary-2']);
  });

  it('should return array for deep nodes', () => {

    const entities = getEntities();

    let result = getAncestors(entities, entities['TestCase-9']);
    expect(result).toEqual(
      ['TestCaseFolder-7', 'TestCaseFolder-3', 'TestCaseFolder-1', 'TestCaseLibrary-1']);

    result = getAncestors(entities, entities['TestCaseFolder-3']);
    expect(result).toEqual(
      ['TestCaseFolder-1', 'TestCaseLibrary-1']);
  });
});

function getEntities(): Dictionary<TreeNode> {
  const tcl1 = {
    nodeId: 'TestCaseLibrary-1',
    id: 1,
    type: NodeType.TestCaseLibrary,
    name: 'Project-1',
    state: NodeState.closed,
    children: ['TestCase-12', 'TestCaseFolder-1', 'TestCaseFolder-2'],
    projectId: 1
  };

  const tcl2 = {
    nodeId: 'TestCaseLibrary-2',
    id: 1,
    type: NodeType.TestCaseLibrary,
    name: 'Project-1',
    state: NodeState.closed,
    children: ['TestCase-10', 'TestCaseFolder-11'],
    projectId: 2
  };

  const tcf1 = {
    nodeId: 'TestCaseFolder-1',
    id: 1,
    type: NodeType.TestCaseFolder,
    name: 'Folder 1',
    state: NodeState.open,
    parentNodeId: 'TestCaseLibrary-1',
    children: ['TestCaseFolder-3', 'TestCase-4', 'TestCase-5', 'TestCaseFolder-6'],
    projectId: 1
  };

  const tcf2 = {
    nodeId: 'TestCaseFolder-2',
    id: 2,
    type: NodeType.TestCaseFolder,
    name: 'Folder 2',
    state: NodeState.closed,
    parentNodeId: 'TestCaseLibrary-1',
    children: [],
    projectId: 1
  };

  const tcf3 = {
    nodeId: 'TestCaseFolder-3',
    id: 3,
    type: NodeType.TestCaseFolder,
    name: 'Folder 3',
    state: NodeState.open,
    parentNodeId: 'TestCaseFolder-1',
    children: ['TestCaseFolder-7', 'TestCase-8'],
    projectId: 1
  };

  const tcf6 = {
    nodeId: 'TestCaseFolder-6',
    id: 6,
    type: NodeType.TestCaseFolder,
    name: 'Folder 6',
    state: NodeState.closed,
    parentNodeId: 'TestCaseFolder-1',
    children: [],
    projectId: 1
  };

  const tcf7 = {
    nodeId: 'TestCaseFolder-7',
    id: 7,
    type: NodeType.TestCaseFolder,
    name: 'Folder 7',
    state: NodeState.closed,
    parentNodeId: 'TestCaseFolder-3',
    children: ['TestCase-9'],
    projectId: 1
  };

  const tcf10 = {
    nodeId: 'TestCaseFolder-11',
    id: 11,
    type: NodeType.TestCaseFolder,
    name: 'Folder 11',
    state: NodeState.closed,
    parentNodeId: 'TestCaseLibrary-2',
    children: [],
    projectId: 2
  };

  const tc12 = {
    nodeId: 'TestCase-12',
    id: 1,
    type: NodeType.TestCase,
    name: 'TestCase 12',
    state: NodeState.leaf,
    parentNodeId: 'TestCaseLibrary-1',
    children: [],
    projectId: 1
  };

  const tc4 = {
    nodeId: 'TestCase-4',
    id: 4,
    type: NodeType.TestCase,
    name: 'TestCase 4',
    state: NodeState.leaf,
    parentNodeId: 'TestCaseFolder-1',
    children: [],
    projectId: 1
  };

  const tc5 = {
    nodeId: 'TestCase-5',
    id: 5,
    type: NodeType.TestCase,
    name: 'TestCase 5',
    state: NodeState.leaf,
    parentNodeId: 'TestCaseFolder-1',
    children: [],
    projectId: 1
  };

  const tc8 = {
    nodeId: 'TestCase-8',
    id: 8,
    type: NodeType.TestCase,
    name: 'TestCase 8',
    state: NodeState.leaf,
    parentNodeId: 'TestCaseFolder-3',
    children: [],
    projectId: 1
  };

  const tc9 = {
    nodeId: 'TestCase-9',
    id: 9,
    type: NodeType.TestCase,
    name: 'TestCase 9',
    state: NodeState.leaf,
    parentNodeId: 'TestCaseFolder-7',
    children: [],
    projectId: 1
  };

  const tc10 = {
    nodeId: 'TestCase-10',
    id: 10,
    type: NodeType.TestCase,
    name: 'TestCase 10',
    state: NodeState.leaf,
    parentNodeId: 'TestCaseLibrary-2',
    children: [],
    projectId: 2
  };

  return [tcl1, tcf1, tcf2, tcf3, tcf6, tcf7, tc12, tc4, tc5, tcl2, tc8, tc9, tc10, tcf10]
    .reduce((accumulation: Dictionary<TreeNode>, currentNode: TreeNode) => {
      accumulation[currentNode.nodeId] = currentNode;
      return accumulation;
    }, {});
}
