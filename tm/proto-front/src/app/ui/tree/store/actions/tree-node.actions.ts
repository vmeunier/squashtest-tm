///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {Action} from '@ngrx/store';
import {Update} from '@ngrx/entity';
import {TreeNode} from '../tree-node.model';
import {ComponentAction} from '@common/store/factories/actions/actions-type';
import {TreeType} from '../tree-types';

export enum TreeNodeActionTypes {
  LoadTreeNodes = '[TreeNode] Load TreeNodes',
  AddTreeNode = '[TreeNode] Add TreeNode',
  UpsertTreeNode = '[TreeNode] Upsert TreeNode',
  AddTreeNodes = '[TreeNode] Add TreeNodes',
  UpsertTreeNodes = '[TreeNode] Upsert TreeNodes',
  UpdateTreeNode = '[TreeNode] Update TreeNode',
  UpdateTreeNodes = '[TreeNode] Update TreeNodes',
  DeleteTreeNode = '[TreeNode] Delete TreeNode',
  DeleteTreeNodes = '[TreeNode] Delete TreeNodes',
  ClearTreeNodes = '[TreeNode] Clear TreeNodes',

  OpenTreeNode = '[TreeNode] Open TreeNode',
  OpenTreeNodeSuccess = '[TreeNode] Open TreeNode Success',
  OpenTreeNodeFailure = '[TreeNode] Open TreeNode Failure',

  ToggleTreeNode = '[TreeNode] Toggle TreeNode',
  SelectTreeNode = '[TreeNode] Select TreeNode'

}

export class LoadTreeNodes implements ComponentAction {
  readonly type = TreeNodeActionTypes.LoadTreeNodes;

  constructor(public pagePrefix: string, public componentId: string, public payload: { treeNodes: TreeNode[] }) {
  }
}

export class AddTreeNode implements ComponentAction {
  readonly type = TreeNodeActionTypes.AddTreeNode;

  constructor(public pagePrefix: string, public componentId: string, public payload: { treeNode: TreeNode }) {
  }
}

export class UpsertTreeNode implements ComponentAction {
  readonly type = TreeNodeActionTypes.UpsertTreeNode;

  constructor(public pagePrefix: string, public componentId: string, public payload: { treeNode: TreeNode }) {
  }
}

export class AddTreeNodes implements ComponentAction {
  readonly type = TreeNodeActionTypes.AddTreeNodes;

  constructor(public pagePrefix: string, public componentId: string, public payload: { treeNodes: TreeNode[] }) {
  }
}

export class UpsertTreeNodes implements ComponentAction {
  readonly type = TreeNodeActionTypes.UpsertTreeNodes;

  constructor(public pagePrefix: string, public componentId: string, public payload: { treeNodes: TreeNode[] }) {
  }
}

export class UpdateTreeNode implements ComponentAction {
  readonly type = TreeNodeActionTypes.UpdateTreeNode;

  constructor(public pagePrefix: string, public componentId: string, public payload: { treeNode: Update<TreeNode> }) {
  }
}

export class UpdateTreeNodes implements ComponentAction {
  readonly type = TreeNodeActionTypes.UpdateTreeNodes;

  constructor(public pagePrefix: string, public componentId: string, public payload: { treeNodes: Update<TreeNode>[] }) {
  }
}

export class DeleteTreeNode implements ComponentAction {
  readonly type = TreeNodeActionTypes.DeleteTreeNode;

  constructor(public pagePrefix: string, public componentId: string, public payload: { id: string }) {
  }
}

export class DeleteTreeNodes implements Action {
  readonly type = TreeNodeActionTypes.DeleteTreeNodes;

  constructor(public pagePrefix: string, public componentId: string, public payload: { ids: string[] }) {
  }
}

export class ClearTreeNodes implements ComponentAction {
  readonly type = TreeNodeActionTypes.ClearTreeNodes;

  constructor(public pagePrefix: string, public componentId: string) {
  }
}

export class OpenTreeNode implements ComponentAction {
  readonly type = TreeNodeActionTypes.OpenTreeNode;

  constructor(public pagePrefix: string, public componentId: string, public treeType: TreeType, public payload: { id: string }) {
  }
}

export class ToggleTreeNode implements ComponentAction {
  readonly type = TreeNodeActionTypes.ToggleTreeNode;

  constructor(public pagePrefix: string, public componentId: string, public payload: { id: string }) {
  }
}

export class SelectTreeNode implements ComponentAction {
  readonly type = TreeNodeActionTypes.SelectTreeNode;

  constructor(public pagePrefix: string, public componentId: string, public payload: { id: string }) {
  }
}


export type TreeNodeActions =
  LoadTreeNodes
  | AddTreeNode
  | UpsertTreeNode
  | AddTreeNodes
  | UpsertTreeNodes
  | UpdateTreeNode
  | UpdateTreeNodes
  | DeleteTreeNode
  | DeleteTreeNodes
  | ClearTreeNodes
  | OpenTreeNode
  | SelectTreeNode
  | ToggleTreeNode;
