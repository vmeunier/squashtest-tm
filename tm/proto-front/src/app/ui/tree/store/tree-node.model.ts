///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

export interface TreeNode {
  nodeId: string;
  id: number;
  projectId: number;
  name: string;
  type: NodeType;
  state: NodeState;
  parentNodeId?: string;
  children: string[];
}

export enum NodeState {
  open = 'open',
  closed = 'closed',
  leaf = 'leaf'
}

export enum NodeType {
  TestCaseLibrary = 'TestCaseLibrary',
  TestCaseFolder = 'TestCaseFolder',
  TestCase = 'TestCase',
  RequirementLibrary = 'RequirementLibrary',
  RequirementFolder = 'RequirementFolder',
  Requirement = 'Requirement',
  CampaignLibrary = 'CampaignLibrary',
  CampaignFolder = 'CampaignFolder',
  Campaign = 'Campaign',
  Iteration = 'Iteration'
}

export namespace NodeType {
  export function isLibrary(nodeType: NodeType): boolean {
    switch (nodeType) {
      case NodeType.TestCaseLibrary :
      case NodeType.RequirementLibrary :
      case NodeType.CampaignLibrary :
        return true;
      default :
        return false;
    }
  }
}

export namespace TreeNodeUtils {
  import isLibrary = NodeType.isLibrary;

  export function couldBeOpen(treeNode: TreeNode) {
    return treeNode.state === NodeState.closed;
  }
  export function isDraggable(treeNode: TreeNode) {
    return !isLibrary(treeNode.type);
  }
}
