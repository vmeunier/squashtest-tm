///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {createSelector} from '@ngrx/store';
import {selectAll, selectEntities, TreeState} from '../reducers/tree-node.reducer';
import {TreeNode} from '../';
import {NodeType} from '../tree-node.model';
import {Dictionary} from '@ngrx/entity';

/**
 * This one is expensive, should be only call at tree init, we don't want to filter gigantic collection of nodes too many times...
 * or maybe we should cache the libs in state
 */
export const selectLibraries = createSelector(selectAll, (nodes: TreeNode[]) => nodes.filter((node: TreeNode) => {
  return NodeType.isLibrary(node.type);
}));

/**
 * This one is a factory of selectors, it return a brand new selector each time it's called
 * @param nodeId the nodeId
 */
export const selectNode = (nodeId: string) => createSelector(selectEntities, (nodes) => nodes[nodeId]);

export const getSelectedNodeIds = createSelector((state: TreeState) => {
  console.log('selecting state ', state);
  if (state) {
   return state.selectedNodes;
  }
  return [];
});

// export const getSelectedNodes = createSelector(selectEntities, getSelectedNodeIds,
//   (entities: Dictionary<TreeNode>, nodeIds: string[]) => {
//     return nodeIds.map(id => entities[id]);
//   });

