///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {TestBed, inject} from '@angular/core/testing';
import {provideMockActions} from '@ngrx/effects/testing';
import {Observable} from 'rxjs';

import {TreeNodeEffects} from './tree-node.effects';
import {instance, mock} from 'ts-mockito';
import {TreeService} from '@tree/service/tree.service';

describe('TreeNodeEffects', () => {
  let actions$: Observable<any>;
  let effects: TreeNodeEffects;

  const treeService = mock(TreeService);

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        TreeNodeEffects,
        {
          provide: TreeService,
          useValue: instance(treeService)
        },
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get(TreeNodeEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
