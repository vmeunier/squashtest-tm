///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {AddTreeNodes, OpenTreeNode, UpdateTreeNode} from '../actions';
import {TreeNodeActionTypes} from '../actions/tree-node.actions';
import {concatMap, filter, switchMap, tap} from 'rxjs/internal/operators';
import {NodeState} from '../tree-node.model';
import {TreeService} from '@tree/service/tree.service';


@Injectable()
export class TreeNodeEffects {

  constructor(private actions$: Actions, private service: TreeService) {
  }

  @Effect()
  openNode$ = this.actions$.pipe(
    ofType(TreeNodeActionTypes.OpenTreeNode),
    switchMap((action: OpenTreeNode) => {
      return this.service.loadNodeContent(action.payload.id, action.treeType).pipe(
        concatMap((children) => {
          return [
            new AddTreeNodes(action.pagePrefix, action.componentId, {treeNodes: children}),
            new UpdateTreeNode(action.pagePrefix, action.componentId, {
              treeNode:
                {
                  id: action.payload.id,
                  changes: {state: NodeState.open, children: children.map(child => child.nodeId)}
                }
            })
          ];
        })
      );
    })
  );
}
