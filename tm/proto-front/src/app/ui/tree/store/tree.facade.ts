///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {TreeState} from './reducers';
import {createSelector, select, Store} from '@ngrx/store';

import {Observable} from 'rxjs';
import {TreeNode} from './tree-node.model';
import {getSelectedNodeIds, selectLibraries, selectNode} from './selectors';
import {LoadTreeNodes, OpenTreeNode, SelectTreeNode, ToggleTreeNode} from './actions';
import {ComponentFacade} from '@common/store/facades/component.facade';
import {TreeType} from './tree-types';
import * as fromTreeReducer from './reducers';

export class TreeFacade extends ComponentFacade {

  // public selectors
  public rootNode$: Observable<TreeNode[]>;

  public selectedNodeIds$: Observable<string[]>;

  private treeType: TreeType;

  constructor(pageUrl: string, componentId: string, treeType: TreeType, store: Store<TreeState>) {
    super(pageUrl, componentId, store);
    this.treeType = treeType;
    this.rootNode$ = this.store.pipe(
      select(createSelector(this.componentStateSelector, selectLibraries))
    );
    this.selectedNodeIds$ = this.store.pipe(
      select(createSelector(this.componentStateSelector, getSelectedNodeIds))
    );
  }

  loadNodes(treeNodes: TreeNode[]) {
    this.store.dispatch(new LoadTreeNodes(this.featureName, this.componentId, {treeNodes}));
  }

  getNode(nodeId: string): Observable<TreeNode> {
    return this.store.pipe(
      select(createSelector(this.componentStateSelector, selectNode(nodeId)))
    );
  }

  openNode(nodeId: string): void {
    this.store.dispatch(
      new OpenTreeNode(this.featureName, this.componentId, this.treeType, {id: nodeId})
    );
  }

  selectNode(id: string): void {
    this.store.dispatch(
      new SelectTreeNode(this.featureName, this.componentId, {id})
    );
  }

  toggleNodeInSelection(id: string) {
    this.store.dispatch(
      new ToggleTreeNode(this.featureName, this.componentId, {id})
    );
  }

  get initialState() {
    return fromTreeReducer.initialState;
  }

}
