///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {AfterContentInit, AfterViewInit, Directive, ElementRef, HostListener, OnInit, Renderer2} from '@angular/core';

@Directive({
  selector: '[sqtmTreeNodeDraggable]'
})
export class TreeNodeDraggableDirective implements OnInit {

  private nativeElement;

  constructor(private el: ElementRef, private renderer: Renderer2) {
    this.nativeElement = this.el.nativeElement;
  }

  @HostListener('dragstart', ['$event'])
  onDragStart($event) {
    // const dragImg = $event.target.querySelector('.ghost-tree-node');
    console.log('start drag');
    $event.dataTransfer.effectAllowed = 'move';
    $event.dataTransfer.setData('text', '123');
    // $event.dataTransfer.setDragImage(dragImg, 0, 0);
    // $event.preventDefault();
    // $event.stopPropagation();
  }

  ngOnInit(): void {
    this.renderer.setAttribute(this.nativeElement, 'draggable', 'true');
  }
}
