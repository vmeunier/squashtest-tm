///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {AfterContentInit, ChangeDetectionStrategy, Component, ContentChild, ElementRef, Renderer2} from '@angular/core';
import {WorkspaceSplitPanelFacade} from '../../store/workspace-split-panel.facade';
import {take} from 'rxjs/operators';

@Component({
  selector: 'sqtm-split',
  template: `
    <div>
      <div class="split-wrapper">
        <ng-content select=".split-left-panel"></ng-content>
        <div class="gutter" sqtmSplitDrag (resize)="handleResize($event)" (endResize)="handleEndResize($event)"></div>
        <ng-content select=".split-right-panel"></ng-content>
      </div>
    </div>
  `,
  styleUrls: ['./split.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SplitComponent implements AfterContentInit {


  @ContentChild('splitLeftPanel', {read: ElementRef})
  leftPanel;

  @ContentChild('splitRightPanel', {read: ElementRef})
  rightPanel;

  constructor(private facade: WorkspaceSplitPanelFacade, private renderer: Renderer2) {
  }

  ngAfterContentInit(): void {
    this.facade.leftPanelWidth$.pipe(
      take(1)
    ).subscribe(width => {
      this.setLeftPanelWidth(width);
    });
  }

  handleResize($event: number) {
    this.setLeftPanelWidth($event);
  }

  handleEndResize($event: number) {
    this.setLeftPanelWidth($event);
    this.storeWidth($event);
  }

  private setLeftPanelWidth(width: number) {
    if (width < 200) {
      width = 200;
    }
    if (this.leftPanel) {
      this.renderer.setStyle(this.leftPanel.nativeElement, 'width', `${width}px`);
    }
  }

  private storeWidth(width: number) {
    if (width < 200) {
      width = 200;
    }
    this.facade.changeWidth(width);
  }

}
