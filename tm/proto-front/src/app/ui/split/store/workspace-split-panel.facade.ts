///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {PageFacade} from '@common/store/facades/page.facade';
import {createSelector, select, Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {visibilitySelector, widthSelector} from './workspace-split-panel.selectors';
import {ResizeLeftPanel, ToggleLeftPanel} from './workspace-split-panel.actions';
import {SplitPanelSlice} from './workspace-split-panel.model';
import * as fromReducers from './workspace-split-panel.reducer';

export class WorkspaceSplitPanelFacade extends PageFacade {

  // public selectors
  public leftPanelWidth$: Observable<number>;
  public leftPanelVisible$: Observable<boolean>;

  // private selector to the WorkspaceSplitPanel State Slice
  private splitPanelStateSelector;

  constructor(pageUrl: string, store: Store<any>) {
    super(pageUrl, store);

    this.splitPanelStateSelector = createSelector(this.featureSelector, (state: SplitPanelSlice) => state.splitPanel);

    // init the selectors
    this.leftPanelWidth$ = this.store.pipe(
      select(createSelector(this.splitPanelStateSelector, widthSelector))
    );
    this.leftPanelVisible$ = this.store.pipe(
      select(createSelector(this.splitPanelStateSelector, visibilitySelector))
    );
  }

  public changeWidth(width: number) {
    this.store.dispatch(new ResizeLeftPanel(this.featureName, {width}));
  }

  public toggleLeftPanel() {
    this.store.dispatch(new ToggleLeftPanel(this.featureName));
  }

  get initialState() {
    return fromReducers.initialState;
  }
}
