///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {AfterViewInit, Directive, ElementRef, EventEmitter, HostListener, NgZone, OnInit, Output, Renderer2} from '@angular/core';

@Directive({
  selector: '[sqtmSplitDrag]'
})
export class SplitDragDirective implements OnInit {

  private resizing: boolean;

  @Output()
  resize = new EventEmitter<number>();

  @Output()
  endResize = new EventEmitter<number>();


  constructor(private el: ElementRef, private zone: NgZone, private renderer: Renderer2) {

  }

  ngOnInit(): void {
    this.zone.runOutsideAngular(() => {
      this.el.nativeElement.addEventListener(
        'mousedown', this.dragEventHandler.bind(this)
      );

      this.renderer.listen('document', 'mousemove', this.moveEventHandler.bind(this));
      this.renderer.listen('document', 'mouseup', this.dropEventHandler.bind(this));
    });
  }

  private dragEventHandler($event: PointerEvent) {
    this.resizing = true;
  }

  private dropEventHandler($event: PointerEvent) {
    if (this.resizing) {
      this.resizing = false;
      this.endResize.emit($event.clientX);
    }
  }

  private moveEventHandler($event: PointerEvent) {
    if (this.resizing) {
      this.resize.emit($event.clientX);
    }
  }


}
