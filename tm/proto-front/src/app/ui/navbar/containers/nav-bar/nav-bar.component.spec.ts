///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NavBarComponent} from './nav-bar.component';
import {RouterTestingModule} from '@angular/router/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {mock} from 'ts-mockito';
import {BugTrackerFacade} from '@features/global/bugtrackers/bug-tracker.facade';
import {AuthenticationFacade} from '@features/global/authentication/authentication.facade';
import {of} from 'rxjs';
import {UserRoles} from '@features/global/authentication/authenticated-user.model';
import {StoreModule} from '@ngrx/store';
import {ThemeFacade} from '@features/global/theme/store/theme.facade';
import {Themes} from '@features/global/theme/themes';

describe('NavBarComponent', () => {
  let component: NavBarComponent;
  let fixture: ComponentFixture<NavBarComponent>;

  const bugtrackerFacade = mock(BugTrackerFacade);
  const authenticationFacade = mock(AuthenticationFacade);
  const themeFacade = mock(ThemeFacade);
  authenticationFacade.authenticatedUser$ = of({login: 'admin', roles: [UserRoles.ROLE_ADMIN, UserRoles.ROLE_USER]});
  authenticationFacade.shouldAccessToAdmin$ = of(true);
  themeFacade.themes$ = of(Themes);
  themeFacade.selectedTheme$ = of(Themes[0]);

  bugtrackerFacade.bugTrackers$ = of([]);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, StoreModule.forRoot({})],
      declarations: [NavBarComponent],
      providers: [
        {
          provide: BugTrackerFacade,
          useFactory: () => bugtrackerFacade
        },
        {
          provide: AuthenticationFacade,
          useFactory: () => authenticationFacade
        }
        ,
        {
          provide: ThemeFacade,
          useFactory: () => themeFacade
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
