///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {BugTrackerFacade} from '@features/global/bugtrackers/bug-tracker.facade';
import {Observable} from 'rxjs';
import {BugTracker} from '@features/global/bugtrackers/bug-tracker.model';
import {AuthenticationFacade} from '@features/global/authentication/authentication.facade';
import {ThemeFacade} from '@features/global/theme/store/theme.facade';
import {Theme} from '@features/global/theme/store/theme.model';

@Component({
  selector: 'sqtm-nav-bar',
  template: `
    <nav role="navigation" class="flex-row-no-wrap nav-bar-container">

      <div class="flex-row-no-wrap icon-container">
        <sqtm-workspace-link [workspaceName]="'requirement-workspace'"></sqtm-workspace-link>
        <sqtm-workspace-link [workspaceName]="'test-case-workspace'"></sqtm-workspace-link>
        <sqtm-workspace-link [workspaceName]="'campaign-workspace'"></sqtm-workspace-link>
        <sqtm-workspace-link [workspaceName]="'custom-report-workspace'"></sqtm-workspace-link>
      </div>

      <div class="flex-row-no-wrap icon-container">
        <img class="squash-logo" src="/assets/img/nav-bar/logo-squash.png"/>
      </div>

      <div class="flex-row-no-wrap icon-container">
        <sqtm-dropdown-nav-button>
          <img class="bug-tracker-icon" src="/assets/img/nav-bar/palette.png"/>
          <a class="bug-tracker-link" *ngFor="let theme of (themes$ | async)">
            <div (click)="selectTheme(theme.id)">{{theme.id}}</div>
          </a>
        </sqtm-dropdown-nav-button>
        <sqtm-nav-button [rootUrl]="'grid-show-case'"></sqtm-nav-button>
        <sqtm-nav-button [rootUrl]="'grid-rxjs-show-case'"></sqtm-nav-button>
        <sqtm-nav-button [rootUrl]="'plugin-show-case'"></sqtm-nav-button>
      </div>

      <div class="flex-row-no-wrap icon-container">
        <sqtm-dropdown-nav-button>
          <img class="bug-tracker-icon" src="/assets/img/nav-bar/bug-tracker.png"/>

          <div class="bug-tracker-link" *ngFor="let bugtracker of (bugTrackers$ | async)">
            <a [href]="bugtracker.url">{{bugtracker.name}}</a>
          </div>
        </sqtm-dropdown-nav-button>
        <sqtm-workspace-link [workspaceName]="'home-workspace'"></sqtm-workspace-link>
        <sqtm-nav-button [rootUrl]="'administration'" *ngIf="shouldAccessToAdmin$ | async"></sqtm-nav-button>
        <sqtm-dropdown-nav-button>
          <img class="bug-tracker-icon" src="/assets/img/nav-bar/three-points.png"/>
          <div class="bug-tracker-link">
            <a>Logout</a>
          </div>
          <div class="bug-tracker-link">
            <a>Mon compte</a>
          </div>
        </sqtm-dropdown-nav-button>
      </div>

    </nav>
  `,
  styleUrls: ['./nav-bar.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavBarComponent implements OnInit {

  bugTrackers$: Observable<BugTracker[]>;
  shouldAccessToAdmin$: Observable<boolean>;
  themes$: Observable<Theme[]>;

  constructor(private bugTrackerFacade: BugTrackerFacade, private authFacade: AuthenticationFacade, private themeFacade: ThemeFacade) {
  }

  ngOnInit() {
    this.bugTrackers$ = this.bugTrackerFacade.bugTrackers$;
    this.shouldAccessToAdmin$ = this.authFacade.shouldAccessToAdmin$;
    this.themes$ = this.themeFacade.themes$;
  }

  selectTheme(id: string) {
    this.themeFacade.changeTheme(id);
  }
}
