///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'sqtm-workspace-link',
  template: `
    <div [routerLink]="routerPath" [ngClass]="color" class="nav-link-container">
      <img [src]="iconPath"/>
    </div>
  `,
  styleUrls: ['./workspace-link.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorkspaceLinkComponent implements OnInit {

  @Input()
  workspaceName: string;

  constructor() {
  }

  ngOnInit() {
  }

  get iconPath() {
    return `assets/img/nav-bar/${this.workspaceName}.png`;
  }

  get routerPath() {
    return `/${this.workspaceName}`;
  }

  get color() {
    return `${this.workspaceName}-main-color`;
  }
}
