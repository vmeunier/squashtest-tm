///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {RowDisplay} from '../store/model/row-display.model';
import {ColumnDisplay} from '../store/model/column-display.model';
import {GridDisplay} from '../store/model/grid-display.model';
import {GridRxjsService} from '@ui/grid_rxjs/services/grid-rxjs.service';

export interface CellRenderer {
  columnDisplay: ColumnDisplay;
  row: RowDisplay;
  grid: GridRxjsService;
  gridDisplay: GridDisplay;
}

export enum CellRenderers {
  'TEXT' = 'TEXT', 'TREE' = 'TREE', 'SELECT_ROW' = 'SELECT_ROW'
}
