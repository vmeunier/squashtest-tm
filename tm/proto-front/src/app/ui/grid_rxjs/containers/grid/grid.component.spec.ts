///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {GridComponent} from './grid.component';
import {instance, mock} from 'ts-mockito';
import {GridFacade} from '@ui/grid/store/grid-facade';
import {of} from 'rxjs';
import {GRID_FACADE} from '@ui/grid/token';
import {GridType} from '@ui/grid/store/reducers/state/config.state';
import {TranslateModule} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {GridViewportDirective} from '@ui/grid/directives/grid-viewport.directive';

describe('GridComponent', () => {
  let component: GridComponent;
  let fixture: ComponentFixture<GridComponent>;

  const gridFacade = instance(mock(GridFacade));

  gridFacade.rows$ = of([
    {
      id: '1',
      data: {id: 1}
    }
  ]);
  gridFacade.gridDisplay$ = of(
    {
      totalWidth: 500,
      rowHeight: 25,
      gridType: GridType.GRID,
      columnDisplays: [
        {
          id: 'column1',
          width: 125,
          left: 0
        }
      ]
    }
  );

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule],
      declarations: [GridComponent, GridViewportDirective],
      providers: [
        {
          provide: GRID_FACADE,
          useFactory: () => gridFacade
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(GridComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
      });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
