///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {GridHeadersComponent} from './grid-headers.component';
import {TranslateModule} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {instance, mock} from 'ts-mockito';
import {GridFacade} from '@ui/grid/store/grid-facade';
import {GRID_FACADE} from '@ui/grid/token';

describe('GridHeadersComponent', () => {
  let component: GridHeadersComponent;
  let fixture: ComponentFixture<GridHeadersComponent>;


  beforeEach(async(() => {
    const gridFacade = instance(mock(GridFacade));
    TestBed.configureTestingModule({
      imports: [TranslateModule],
      declarations: [GridHeadersComponent],
      providers: [
        {
          provide: GRID_FACADE,
          useFactory: () => gridFacade
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(GridHeadersComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
      });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
