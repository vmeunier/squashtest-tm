///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {ChangeDetectionStrategy, Component, Inject, Input, OnInit} from '@angular/core';
import {RowDisplay} from '../../store/model/row-display.model';
import {ColumnDisplay} from '../../store/model/column-display.model';
import {GRID_SERVICE} from '../../token';
import {CellRenderers} from '../../interfaces/cell-renderer';
import {GridDisplay} from '../../store/model/grid-display.model';
import {GridRxjsService} from '@ui/grid_rxjs/services/grid-rxjs.service';

// It seems that we have to make this ugly switch because change detection is implemented by compiler.
// So we cannot dynamically generate a component by type and having a change detection running in AOT mode,
// because the compiler make static analysis of the template
// see https://blog.angularindepth.com/here-is-what-you-need-to-know-about-dynamic-components-in-angular-ac1e96167f9e
@Component({
  selector: 'sqtm-grid-cell',
  template: `
    <ng-container [ngSwitch]="columnDisplay.cellRenderer">
      <sqtm-check-box-cell-renderer
        *ngSwitchCase="cellRenderers.SELECT_ROW"
        [columnDisplay]="columnDisplay"
        [gridDisplay]="gridDisplay"
        [row]="row">
      </sqtm-check-box-cell-renderer>

      <sqtm-tree-hierarchy-cell-renderer
        *ngSwitchCase="cellRenderers.TREE"
        [columnDisplay]="columnDisplay"
        [gridDisplay]="gridDisplay"
        [row]="row">
      </sqtm-tree-hierarchy-cell-renderer>

      <sqtm-text-cell-renderer
        *ngSwitchDefault
        [columnDisplay]="columnDisplay"
        [gridDisplay]="gridDisplay"
        [row]="row">
      </sqtm-text-cell-renderer>
    </ng-container>
  `,
  styleUrls: ['./grid-cell.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GridCellComponent implements OnInit {

  @Input()
  columnDisplay: ColumnDisplay;

  @Input()
  gridDisplay: GridDisplay;

  @Input()
  index: number;

  @Input()
  row: RowDisplay;

  cellRenderers = CellRenderers;

  constructor(@Inject(GRID_SERVICE) public grid: GridRxjsService) {
  }

  ngOnInit() {
  }

}
