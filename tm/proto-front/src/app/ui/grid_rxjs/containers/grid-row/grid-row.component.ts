///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {ChangeDetectionStrategy, Component, Inject, Input, OnInit} from '@angular/core';
import {GridDisplay} from '../../store/model/grid-display.model';
import {RowDisplay} from '../../store/model/row-display.model';
import {GRID_SERVICE} from '../../token';
import {GridType} from '../../store/reducers/state/config.state';
import {ColumnDisplay} from '../../store/model/column-display.model';
import {GridRxjsService} from '@ui/grid_rxjs/services/grid-rxjs.service';

@Component({
  selector: 'sqtm-grid-row',
  template: `
    <div class="sqtm-grid-row" [ngStyle]="getRowStyle(index, gridDisplay.rowHeight)">
      <ng-container *ngFor="let columnDisplay of gridDisplay.columnDisplays; trackBy: trackByFn">
        <sqtm-grid-cell [row]="row" [columnDisplay]="columnDisplay" [gridDisplay]="gridDisplay"></sqtm-grid-cell>
      </ng-container>
    </div>
  `,
  styleUrls: ['./grid-row.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GridRowComponent implements OnInit {

  @Input()
  gridDisplay: GridDisplay;

  @Input()
  index: number;

  @Input()
  row: RowDisplay;


  constructor(@Inject(GRID_SERVICE) public grid: GridRxjsService) {
  }

  ngOnInit() {
  }


  getRowStyle(index: number, rowHeight: number) {
    if (this.gridDisplay.gridType === GridType.TABLE) {
      return {
        position: 'relative',
        width: `${this.gridDisplay.totalWidth}px`,
        'flex-grow': '1'
      };
    }
    return {
      position: 'absolute',
      height: `${rowHeight}px`,
      transform: `translateY(${index * rowHeight}px)`
    };
  }

  trackByFn(index: number, columnDisplay: ColumnDisplay) {
    return columnDisplay.id;
  }


}
