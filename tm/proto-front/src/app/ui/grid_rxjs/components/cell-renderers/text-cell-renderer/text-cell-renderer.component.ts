///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {ChangeDetectionStrategy, Component, Inject, Input, OnInit} from '@angular/core';
import {ColumnDisplay} from '../../../store/model/column-display.model';
import {RowDisplay} from '../../../store/model/row-display.model';
import {GRID_SERVICE} from '../../../token';
import {GridDisplay} from '../../../store/model/grid-display.model';
import {GridType} from '../../../store/reducers/state/config.state';
import {GridRxjsService} from '@ui/grid_rxjs/services/grid-rxjs.service';
import {CellRenderer} from '@ui/grid_rxjs/interfaces/cell-renderer';

@Component({
  selector: 'sqtm-text-cell-renderer',
  template: `
    <div [ngClass]="getCellClass()" [ngStyle]="getCellStyle(columnDisplay)">
      {{row.data[columnDisplay.id]}}
    </div>
  `,
  styleUrls: ['./text-cell-renderer.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TextCellRendererComponent implements OnInit, CellRenderer {

  @Input()
  columnDisplay: ColumnDisplay;

  @Input()
  row: RowDisplay;

  @Input()
  gridDisplay: GridDisplay;

  constructor(@Inject(GRID_SERVICE) public grid: GridRxjsService) {
  }

  ngOnInit() {
  }

  getCellClass() {
    if (this.gridDisplay.gridType === GridType.TABLE) {
      return ['sqtm-table-cell'];
    }
    return ['sqtm-grid-cell'];
  }

  getCellStyle(columnDisplay: ColumnDisplay) {
    if (this.gridDisplay.gridType === GridType.TABLE) {
      return {
        width: `${columnDisplay.width}px`,
      };
    }
    return {
      width: `${columnDisplay.width}px`,
      left: `${columnDisplay.left}px`
    };
  }


}
