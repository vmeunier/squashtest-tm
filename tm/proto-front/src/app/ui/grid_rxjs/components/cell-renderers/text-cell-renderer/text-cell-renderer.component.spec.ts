///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TextCellRendererComponent} from './text-cell-renderer.component';
import {GRID_FACADE} from '@ui/grid/token';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {instance, mock} from 'ts-mockito';
import {GridFacade} from '@ui/grid/store/grid-facade';
import {GridType} from '@ui/grid/store/reducers/state/config.state';

describe('TextCellRendererComponent', () => {
  let component: TextCellRendererComponent;
  let fixture: ComponentFixture<TextCellRendererComponent>;

  const gridFacade = instance(mock(GridFacade));

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TextCellRendererComponent],
      providers: [
        {
          provide: GRID_FACADE,
          useFactory: () => gridFacade
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(TextCellRendererComponent);
        component = fixture.componentInstance;
        component.gridDisplay = {rowHeight: 25, gridType: GridType.GRID, totalWidth: 0, columnDisplays: []};
        component.columnDisplay = {id: 'column-1', width: 200, left: 200};
        component.row = {id: 'tcln-1', data: {id: 'tcln-1'}};
        fixture.detectChanges();
      });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
