///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {CellRenderers} from '@ui/grid/interfaces/cell-renderer';
import {ColumnDefinition, ColumnSort} from '@ui/grid/store/model/column-definition.model';

export function column(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id);
}

export function selectRowColumn(): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder('select-row').withWidth(40).unsortable().withRenderer(CellRenderers.SELECT_ROW);
}

export class ColumnDefinitionBuilder {

  private width: number;
  private i18nKey: string;
  private sortable: Boolean = true;
  private sort: ColumnSort = ColumnSort.NO_SORT;
  private cellRenderer: CellRenderers = CellRenderers.TEXT;
  private iconName: string;

  constructor(private id: string) {
  }

  static column(id: string) {
    return new ColumnDefinitionBuilder(id);
  }

  withWidth(width: number): ColumnDefinitionBuilder {
    this.width = width;
    return this;
  }

  withI18nKey(i18nKey: string): ColumnDefinitionBuilder {
    this.i18nKey = i18nKey;
    return this;
  }

  withIconname(iconName: string): ColumnDefinitionBuilder {
    this.iconName = iconName;
    return this;
  }

  unsortable() {
    this.sortable = false;
    return this;
  }

  withRenderer(cellRenderer: CellRenderers) {
    this.cellRenderer = cellRenderer;
    return this;
  }

  build(): ColumnDefinition {
    return {...this as object as ColumnDefinition};
  }
}
