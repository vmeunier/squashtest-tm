///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

export class DataRow {
  id: string | number;
  children?: string[];
  parentRowId?: string;
  state?: DataRowOpenState = DataRowOpenState.closed;
  selected?: boolean;
  [K: string]: any;
}

export interface GridResponse {
  count?: number;
  idAttribute?: string;
  dataRows: DataRow[];
}

export interface GridRequest {
  page: number;
  size: number;
  sort?: any[];
}

export enum DataRowOpenState {
  open = 'open',
  closed = 'closed',
  leaf = 'leaf',
}

export namespace DataRowState {
  export function getToggleState(state: DataRowOpenState): DataRowOpenState {
    if (state === DataRowOpenState.open) {
      return DataRowOpenState.closed;
    }
    if (state === DataRowOpenState.closed) {
      return DataRowOpenState.open;
    }
    return DataRowOpenState.leaf;
  }
}
