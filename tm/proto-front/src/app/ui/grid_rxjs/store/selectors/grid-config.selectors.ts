///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {createSelector} from '@ngrx/store';
import {ColumnDefinition} from '../model/column-definition.model';
import {ColumnDisplay} from '../model/column-display.model';
import {GridState} from '../reducers/state/grid.state';
import {columnDefinitionAdapter} from '../reducers/state/columns-definition.state';

export const configSelector = createSelector((state: GridState) => state.config);

const columnDefinitionProjection = (state: GridState) => state.columnDefinitions;

export const columnDefinitionSelectors = columnDefinitionAdapter.getSelectors(columnDefinitionProjection);

export const gridDisplaySelector = createSelector(columnDefinitionSelectors.selectAll, configSelector, (columnDefinitions, config) => {
  const columns = columnDefinitions.reduce<AccGridDisplay>
  ((gridDisplay: AccGridDisplay, columnDefinition: ColumnDefinition) => {
    const previousWidth = gridDisplay.totalWidth;
    const columnDisplay: ColumnDisplay = {...columnDefinition, left: previousWidth};
    gridDisplay.columnDisplays.push(columnDisplay);
    gridDisplay.totalWidth = previousWidth + columnDefinition.width;
    return gridDisplay;
  }, {totalWidth: 0, columnDisplays: []});

  return {...columns, rowHeight: config.rowHeight, gridType: config.gridType};
});

interface AccGridDisplay {
  columnDisplays: ColumnDisplay[];
  totalWidth: number;
}

