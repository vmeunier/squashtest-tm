///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {createSelector} from '@ngrx/store';
import {columnDefinitionSelectors, configSelector} from './grid-config.selectors';
import {dataRowSelectors} from './data-row.selector';
import {RowDisplay} from '../model/row-display.model';
import {paginationSelector} from './grid-pagination.selectors';
import {PaginationState} from '../reducers/state/pagination.state';
import {findSortedColumns, flatTree, sortRowDisplay} from './utils.selector';

// This selector is responsible for selecting data according to filters and sorts and convert to diplayRow.
export const clientRowDisplayMapSelector = createSelector(dataRowSelectors.selectAll,
  (dataRows) => {
    return dataRows.reduce((rowsDisplay, dataRow) => {
      const strId = dataRow.id.toString();
      rowsDisplay[strId] = {...dataRow, data: dataRow, id: strId, depth: 0};
      return rowsDisplay;
    }, {});
  });

// this selector is responsible for  :
//    apply pagination client side.


//    create a tree if tree grid or grouping rows data.
export const rowDisplaySelector =
  createSelector(configSelector, paginationSelector, columnDefinitionSelectors.selectAll, clientRowDisplayMapSelector,
    (config, pagination, columnDefinitions, rowDisplayDict) => {
      let rowDisplays: RowDisplay[] = Object.values(rowDisplayDict);
      const sortedColumns = findSortedColumns(columnDefinitions);
      rowDisplays = sortRowDisplay(rowDisplays, sortedColumns);

      if (pagination.active) {
        return extractRowDsiplay(rowDisplays, pagination).map((row, index) => {
          row.index = index;
          return row;
        });
      } else {
        return flatTree(rowDisplays, rowDisplayDict);
      }
    }
  );

/**
 * Method responsible for extracting paginated data when pagination is done client side.
 * @param rowDisplays all rows
 * @param pagination pagination parameters
 */
function extractRowDsiplay(rowDisplays: RowDisplay[], pagination: PaginationState): RowDisplay[] {
  const minIndex = pagination.page * pagination.size;
  const maxIndex = (pagination.page + 1) * pagination.size;
  return rowDisplays.slice(minIndex, maxIndex);
}


