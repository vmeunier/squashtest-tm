///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {createSelector} from '@ngrx/store';
import {configSelector} from './grid-config.selectors';
import {dataRowSelectors} from './data-row.selector';
import {RowDisplay} from '../model/row-display.model';
import {paginationSelector} from './grid-pagination.selectors';
import {DataRowOpenState} from '../model/data-row.model';

// This selector is responsible for convert dataRows to row display.
export const rowDisplayMapSelector = createSelector(dataRowSelectors.selectEntities, (dataRows) => {
  return Object.values(dataRows).reduce((rowsDisplay, dataRow) => {
    const strId = dataRow.id.toString();
    rowsDisplay[strId] = {...dataRow, data: dataRow, id: strId, depth: 0};
    return rowsDisplay;
  }, {});
});


// this selector is responsible for  :
//    create a tree if tree grid or grouping rows data.
export const rowDisplaySelector =
  createSelector(configSelector, paginationSelector, rowDisplayMapSelector, (config, pagination, rowDisplayDict) => {
      const rowDisplays: RowDisplay[] = Object.values(rowDisplayDict);
      const stack: RowDisplay[] = rowDisplays.filter(row => !Boolean(row.parentRowId)).reverse();
      const tree: RowDisplay[] = [];
      let index = 0;

      while (stack.length) {
        const rowDisplay = stack.pop();
        rowDisplay.index = index;
        index++;
        tree.push(rowDisplay);
        if (rowDisplay.children && rowDisplay.state === DataRowOpenState.open) {
          for (let i = rowDisplay.children.length - 1; i >= 0; i--) {
            const id = rowDisplay.children[i];
            const child: RowDisplay = rowDisplayDict[id];
            child.depth = rowDisplay.depth + 1;
            stack.push(child);
          }
        }
      }

      return tree;
    }
  );

