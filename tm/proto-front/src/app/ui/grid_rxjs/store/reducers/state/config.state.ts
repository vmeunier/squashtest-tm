///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

export interface GridConfigState {
  rowHeight: number;
  dataProvider: GridDataProvider;
  serverRootUrl?: string[];
  gridType: GridType;
}

export enum GridDataProvider {
  SERVER = 'SERVER', CLIENT = 'CLIENT'
}

// Enum representing the different flavor of grids
// GRID is the standard grid with fixed row height and all functionality activated
// TREE is a grid with fixed row height designed for representation of hierarchical data. Some functionality are not possible in this mode
//  because they enter in conflict with hierarchy : Pagination, Row grouping...
// TABLE is a grid with variable row height. Useful when having to treat with rich text... However some functionality are disabled in
//  this mode : Pinned columns...
export enum GridType {
  GRID = 'GRID', TREE = 'TREE', TABLE = 'TABLE'
}
