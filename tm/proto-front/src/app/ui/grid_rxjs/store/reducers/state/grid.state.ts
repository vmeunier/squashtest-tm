///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {GridConfigState, GridDataProvider, GridType} from './config.state';
import {dataRowAdapter, DataRowState} from './data-row.state';
import {PaginationState} from './pagination.state';
import {columnDefinitionAdapter, ColumnsDefinitionState} from './columns-definition.state';

export interface GridState {
  config: GridConfigState;
  columnDefinitions: ColumnsDefinitionState;
  dataRowState: DataRowState;
  pagination: PaginationState;
}


export const gridInitialState: GridState = {
  config: {
    rowHeight: 25,
    dataProvider: GridDataProvider.CLIENT,
    gridType: GridType.GRID,
    serverRootUrl: []
  },
  columnDefinitions: columnDefinitionAdapter.getInitialState(),
  dataRowState: dataRowAdapter.getInitialState(),
  pagination: {
    active: false,
    page: 0,
    size: 25,
    showAll: false,
    count: 0,
    allowedPageSizes: [10, 25, 50, 75, 100]
  }
};
