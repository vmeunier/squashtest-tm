///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {LoadDataRows, PaginationActionTypes, RefreshDataFromServer} from '../actions';
import {map, switchMap} from 'rxjs/operators';
import {GridRxjsService} from '../../services/grid-rxjs.service';
import {GridResponse} from '../../store/model/data-row.model';
import {findSortedColumns} from '@ui/grid/store/selectors/utils.selector';


// @Injectable()
export class PaginationEffects {

  // constructor(private actions$: Actions, private gridService: GridRxjsService) {
  // }
  //
  // @Effect()
  // refreshDataFromServer$ = this.actions$.pipe(
  //   ofType(PaginationActionTypes.RefreshDataFromServer),
  //   switchMap((action: RefreshDataFromServer) => {
  //     const config = action.payload.config;
  //     const pagination = action.payload.pagination;
  //     const sort = findSortedColumns(Object.values(action.payload.columnDefinitions.entities));
  //
  //     return this.gridService.getData(config.serverRootUrl, {page: pagination.page, size: pagination.pageSize, sort}).pipe(
  //       map((gridResponse: GridResponse) => {
  //         gridResponse.idAttribute = 'TCLN_ID';
  //         if (gridResponse.idAttribute) {
  //           gridResponse._dataRows = gridResponse.dataRows.map(dataRow => {
  //             return {
  //               id: dataRow[gridResponse.idAttribute],
  //               ...dataRow
  //             };
  //           });
  //         }
  //         return new LoadDataRows(action.pagePrefix, action.componentId, {dataRows: gridResponse.dataRows, count: gridResponse.count});
  //       })
  //     );
  //   })
  // );
}
