///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {Action} from '@ngrx/store';
import {Update} from '@ngrx/entity';
import {ComponentAction} from '../../../../common/store/factories/actions/actions-type';
import {DataRow} from '../model/data-row.model';
import {GridConfigState} from '../reducers/state/config.state';

export enum DataRowActionTypes {
  LoadDataRows = '[Grid][DataRow] Load DataRows',
  AddDataRow = '[Grid][DataRow] Add DataRow',
  UpsertDataRow = '[Grid][DataRow] Upsert DataRow',
  AddDataRows = '[Grid][DataRow] Add DataRows',
  UpsertDataRows = '[Grid][DataRow] Upsert DataRows',
  UpdateDataRow = '[Grid][DataRow] Update DataRow',
  UpdateDataRows = '[Grid][DataRow] Update DataRows',
  DeleteDataRow = '[Grid][DataRow] Delete DataRow',
  DeleteDataRows = '[Grid][DataRow] Delete DataRows',
  ClearDataRows = '[Grid][DataRow] Clear DataRows',
  OpenDataRow = '[Grid][DataRow] Open DataRow',
  CloseDataRow = '[Grid][DataRow] Close DataRow',
  GetInitialRowsFromServer = '[Grid][DataRow] Get Initial Rows From Server',
  GetChildrenFromServer = '[Grid][DataRow] Get Children From Server',
  GetChildrenFromServerSuccess = '[Grid][DataRow] Get Children From Server Success',
  ToggleRowSelection = '[Grid][DataRow] Toggle Row Selection',
}

export class LoadDataRows implements ComponentAction {
  readonly type = DataRowActionTypes.LoadDataRows;

  constructor(public pagePrefix: string, public componentId: string, public payload: { dataRows: DataRow[], count: number }) {
  }
}

export class AddDataRow implements ComponentAction {
  readonly type = DataRowActionTypes.AddDataRow;

  constructor(public pagePrefix: string, public componentId: string, public payload: { dataRow: DataRow }) {
  }
}

export class UpsertDataRow implements ComponentAction {
  readonly type = DataRowActionTypes.UpsertDataRow;

  constructor(public pagePrefix: string, public componentId: string, public payload: { dataRow: DataRow }) {
  }
}

export class AddDataRows implements ComponentAction {
  readonly type = DataRowActionTypes.AddDataRows;

  constructor(public pagePrefix: string, public componentId: string, public payload: { dataRows: DataRow[] }) {
  }
}

export class UpsertDataRows implements ComponentAction {
  readonly type = DataRowActionTypes.UpsertDataRows;

  constructor(public pagePrefix: string, public componentId: string, public payload: { dataRows: DataRow[] }) {
  }
}

export class UpdateDataRow implements ComponentAction {
  readonly type = DataRowActionTypes.UpdateDataRow;

  constructor(public pagePrefix: string, public componentId: string, public payload: { dataRow: Update<DataRow> }) {
  }
}

export class UpdateDataRows implements ComponentAction {
  readonly type = DataRowActionTypes.UpdateDataRows;

  constructor(public pagePrefix: string, public componentId: string, public payload: { dataRows: Update<DataRow>[] }) {
  }
}

export class DeleteDataRow implements ComponentAction {
  readonly type = DataRowActionTypes.DeleteDataRow;

  constructor(public pagePrefix: string, public componentId: string, public payload: { id: string }) {
  }
}

export class DeleteDataRows implements Action {
  readonly type = DataRowActionTypes.DeleteDataRows;

  constructor(public pagePrefix: string, public componentId: string, public payload: { ids: string[] }) {
  }
}

export class ClearDataRows implements ComponentAction {
  readonly type = DataRowActionTypes.ClearDataRows;

  constructor(public pagePrefix: string, public componentId: string) {
  }
}

export class OpenDataRow implements ComponentAction {
  readonly type = DataRowActionTypes.OpenDataRow;

  constructor(public pagePrefix: string, public componentId: string, public payload: { id: string }) {
  }
}

export class CloseDataRow implements ComponentAction {
  readonly type = DataRowActionTypes.CloseDataRow;

  constructor(public pagePrefix: string, public componentId: string, public payload: { id: string }) {
  }
}

export class GetInitialRowsFromServer implements ComponentAction {
  readonly type = DataRowActionTypes.GetInitialRowsFromServer;

  constructor(public pagePrefix: string,
              public componentId: string,
              public payload: { config: GridConfigState }) {
  }
}

export class GetChildrenFromServer implements ComponentAction {
  readonly type = DataRowActionTypes.GetChildrenFromServer;

  constructor(public pagePrefix: string,
              public componentId: string,
              public payload: { config: GridConfigState, id: string }) {
  }
}

export class GetChildrenFromServerSuccess implements ComponentAction {
  readonly type = DataRowActionTypes.GetChildrenFromServerSuccess;

  constructor(public pagePrefix: string,
              public componentId: string,
              public payload: { id: string, children: DataRow[] }) {
  }
}

export class ToggleRowSelection implements ComponentAction {
  readonly type = DataRowActionTypes.ToggleRowSelection;

  constructor(public pagePrefix: string,
              public componentId: string,
              public payload: { id: string}) {
  }
}

export type DataRowActions =
  LoadDataRows
  | AddDataRow
  | UpsertDataRow
  | AddDataRows
  | UpsertDataRows
  | UpdateDataRow
  | UpdateDataRows
  | DeleteDataRow
  | DeleteDataRows
  | ClearDataRows
  | OpenDataRow
  | CloseDataRow
  | GetInitialRowsFromServer
  | GetChildrenFromServer
  | GetChildrenFromServerSuccess
  | ToggleRowSelection;
