///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {RowDisplayProvider} from './row-display-provider';
import {BehaviorSubject, combineLatest, Observable} from 'rxjs';
import {DataRowState} from '@ui/grid_rxjs/services/grid-rxjs.service';
import {PaginationState} from '@ui/grid_rxjs/store/reducers/state/pagination.state';
import {RowDisplay} from '@ui/grid_rxjs/store/model/row-display.model';
import {map} from 'rxjs/operators';

export class ClientGridRowDisplayProvider implements RowDisplayProvider {

  public createRow$(
    dataRowsState: BehaviorSubject<DataRowState>,
    paginationState: BehaviorSubject<PaginationState>): Observable<RowDisplay[]> {
    return combineLatest(dataRowsState, paginationState).pipe(
      map(([dataRowState, pagination]) => {
        const ids = this.paginateRows(dataRowState.ids as string[], pagination);
        return ids.map((id, index) => {
          const dataRow = dataRowState.entities[id];
          return {...dataRow, id: dataRow.id.toString(), data: dataRow, depth: 0, index};
        });
      })
    );
  }

  private paginateRows(ids: string[], pagination: PaginationState): string[] {
    if (pagination.active) {
      const minIndex = pagination.page * pagination.size;
      const maxIndex = (pagination.page + 1) * pagination.size;
      return ids.slice(minIndex, maxIndex);
    }
    return ids;
  }

}
