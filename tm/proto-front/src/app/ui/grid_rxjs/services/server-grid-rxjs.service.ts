///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {Injectable} from '@angular/core';
import {GridRxjsService} from './grid-rxjs.service';
import {GridConfig} from '../store/model/grid-config.model';
import {RestService} from '@common/services/rest.service';
import {GridRequest, GridResponse} from '../store/model/data-row.model';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';

@Injectable()
export class ServerGridRxjsService extends GridRxjsService {

  constructor(config: GridConfig, private restService: RestService) {
    super(config);
  }

  refreshData() {
    this.getData().pipe(
      tap((gridResponse: GridResponse) => {
        const dataRowState = this.dataRowAdapter.addAll(gridResponse.dataRows, this._dataRows.getValue());
        this._dataRows.next(dataRowState);
        this._pagination.next({...this._pagination.getValue(), count: gridResponse.count});
      })
    );
  }

  private getData(): Observable<GridResponse> {
    const config = this._config.getValue();
    const pagination = this._pagination.getValue();
    const gridRequest: GridRequest = {
      ...pagination, sort: []
    };
    return this.restService.get(config.serverRootUrl, {gridRequest: 1, ...gridRequest});
  }
}


