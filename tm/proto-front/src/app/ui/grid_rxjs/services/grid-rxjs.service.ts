///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {BehaviorSubject, combineLatest, Observable} from 'rxjs';
import {DataRow} from '../store/model/data-row.model';
import {GridConfigState, GridDataProvider, GridType} from '../store/reducers/state/config.state';
import {ColumnDefinition} from '../store/model/column-definition.model';
import {PaginationState} from '../store/reducers/state/pagination.state';
import {RowDisplay} from '../store/model/row-display.model';
import {GridDisplay} from '../store/model/grid-display.model';
import {PaginationDisplay} from '../store/model/pagination-display.model';
import {GridConfig} from '../store/model/grid-config.model';
import {map} from 'rxjs/operators';
import {ColumnDisplay} from '../store/model/column-display.model';
import {createEntityAdapter, EntityState} from '@ngrx/entity';
import {ClientGridRowDisplayProvider} from './client-grid-row-display-provider';
import {GridState} from '../store/reducers/state/grid.state';

// @Injectable()
export abstract class GridRxjsService {

  protected dataRowAdapter = createEntityAdapter<DataRow>();
  protected columnDefinitionAdapter = createEntityAdapter<ColumnDefinition>();

  // // Behavior subjects containing state
  // // Should we have only one big BehaviorSubject or several like we have here ?
  protected _dataRows = new BehaviorSubject<DataRowState>({ids: [], entities: {}});
  protected _config = new BehaviorSubject<GridConfigState>({
    rowHeight: 25,
    serverRootUrl: [],
    gridType: GridType.GRID,
    dataProvider: GridDataProvider.CLIENT
  });
  protected _columnDefinitions = new BehaviorSubject<ColumnDefinition[]>([]);
  protected _pagination = new BehaviorSubject<PaginationState>({
    size: 25,
    page: 0,
    allowedPageSizes: [10, 25, 50, 100],
    count: 0,
    active: true,
    showAll: false
  });

  // protected state = new BehaviorSubject<GridState>({
  //   dataRowState: {ids: [], entities: {}},
  //   config: {
  //     rowHeight: 25,
  //     serverRootUrl: [],
  //     gridType: GridType.GRID,
  //     dataProvider: GridDataProvider.CLIENT
  //   },
  //   columnDefinitions: {ids: [], entities: {}},
  //   pagination: {
  //     size: 25,
  //     page: 0,
  //     allowedPageSizes: [10, 25, 50, 100],
  //     count: 0,
  //     active: true,
  //     showAll: false
  //   }
  // });

  // Output observables
  public rows$: Observable<RowDisplay[]>;
  public gridDisplay$: Observable<GridDisplay>;
  public pagination$: Observable<PaginationDisplay>;
  public config$: Observable<GridConfigState>;

  constructor(config: GridConfig) {
    // push initial config into config behavior subject
    
    // this.state.next({...this.state.getValue(), config: ...config});

    // plumbing behavior subjects into observables
    // it's a little bit like ngrx selectors, we combine the subjects into output observables
    this.config$ = this._config;
    this.configureRowsOutput();
    this.gridDisplay$ = this.createGridDisplay$();
    this.pagination$ = this.createPagination$();
  }

  private configureRowsOutput() {
    const config = this._config.getValue();
    let rowsProvider;
    switch (config.dataProvider) {
      case GridDataProvider.CLIENT:
        rowsProvider = new ClientGridRowDisplayProvider();
        break;
      default:
        throw new Error(`Unknown data provider ${config.dataProvider}`);
    }
    this.rows$ = rowsProvider.createRow$(this._dataRows, this._pagination);
  }

// public methods to manipulate state
  public loadData(dataRows: DataRow[], count: number) {
    this._dataRows.next(this.dataRowAdapter.addAll(dataRows, this._dataRows.getValue()));
    this._pagination.next({...this._pagination.value, count});
  }

  public paginateNextPage() {
    const pagination = {...this._pagination.value};
    if (pagination.page < this.getMaxPage(pagination)) {
      pagination.page++;
      this._pagination.next(pagination);
    }
  }

  public paginatePreviousPage() {
    const pagination = {...this._pagination.value};
    if (pagination.page > 0) {
      pagination.page--;
      this._pagination.next(pagination);
    }
  }

  public changePaginationSize(pageSize: number) {
    const pagination = {...this._pagination.value};
    pagination.size = pageSize;
    pagination.page = 0;
    this._pagination.next(pagination);
  }

  public openRow(id: string) {
  }

  public closeRow(id: string) {
  }

  public toggleRowSelection(id: string) {
  }

  public toggleColumnSort(id: string) {
  }

  // creators of output observables, aka sorts of selectors
  // we will compute outputs from combination of our behavior subjects
  private createPagination$() {
    return this._pagination.pipe(
      map(pagination => {
        const maxPage = this.getMaxPage(pagination);
        return {
          ...pagination,
          maxPage
        };
      })
    );
  }

  private getMaxPage(pagination) {
    const modulo = pagination.count % pagination.pageSize;
    let maxPage = Math.floor(pagination.count / pagination.pageSize) - 1;
    if (modulo > 0) {
      maxPage++;
    }
    return maxPage;
  }

  private createGridDisplay$(): Observable<GridDisplay> {
    return combineLatest(this._columnDefinitions, this._config).pipe(
      map(([columnDefinitions, config]) => {
        const columnDisplays = columnDefinitions.reduce<AccGridDisplay>
        ((gridDisplay: AccGridDisplay, columnDefinition: ColumnDefinition) => {
          const previousWidth = gridDisplay.totalWidth;
          const columnDisplay: ColumnDisplay = {...columnDefinition, left: previousWidth};
          gridDisplay.columnDisplays.push(columnDisplay);
          gridDisplay.totalWidth = previousWidth + columnDefinition.width;
          return gridDisplay;
        }, {totalWidth: 0, columnDisplays: []});
        return {
          rowHeight: config.rowHeight,
          totalWidth: columnDisplays.totalWidth,
          gridType: config.gridType,
          columnDisplays: columnDisplays.columnDisplays
        };
      })
    );
  }



}

interface AccGridDisplay {
  columnDisplays: ColumnDisplay[];
  totalWidth: number;
}

export interface DataRowState extends EntityState<DataRow> {
}


