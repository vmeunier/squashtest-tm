///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {BehaviorSubject, Observable} from 'rxjs';
import {DataRowState} from './grid-rxjs.service';
import {PaginationState} from '../store/reducers/state/pagination.state';
import {RowDisplay} from '../store/model/row-display.model';

/**
 * A grid row provider is responsible for creating "selectors" for the different flavors of grid.
 * Mainly it will take the state of the Grid represented by the behavior subjects and provide an output observable to feed the grid
 * component.
 */
export interface RowDisplayProvider {
  createRow$: (dataRows: BehaviorSubject<DataRowState>, pagination: BehaviorSubject<PaginationState>) => Observable<RowDisplay[]>;
}
