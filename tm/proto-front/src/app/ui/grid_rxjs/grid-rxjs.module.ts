///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GridComponent} from './containers/grid/grid.component';
import {EffectsModule} from '@ngrx/effects';
import * as fromEffects from './store/effects';
import {PaginationSizeSelectorComponent} from './components/pagination-size-selector/pagination-size-selector.component';
import {GridRowComponent} from './containers/grid-row/grid-row.component';
import {CellHostDirective} from './directives/cell-host.directive';
import {GridCellComponent} from './containers/grid-cell/grid-cell.component';
import {TextCellRendererComponent} from './components/cell-renderers/text-cell-renderer/text-cell-renderer.component';
// tslint:disable-next-line:max-line-length
import {TreeHierarchyCellRendererComponent} from './components/cell-renderers/tree-hierarchy-cell-renderer/tree-hierarchy-cell-renderer.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {faAngleDown, faAngleRight, faArrowDown, faArrowUp} from '@fortawesome/free-solid-svg-icons';
import {library} from '@fortawesome/fontawesome-svg-core';
import {GridViewportDirective} from './directives/grid-viewport.directive';
import {TranslateModule} from '@ngx-translate/core';
import {CheckBoxCellRendererComponent} from './components/cell-renderers/check-box-cell-renderer/check-box-cell-renderer.component';
import {GridHeadersComponent} from './containers/grid-headers/grid-headers.component';

@NgModule({
  declarations: [GridComponent, PaginationSizeSelectorComponent, GridRowComponent, CellHostDirective, GridCellComponent,
    TextCellRendererComponent, TreeHierarchyCellRendererComponent, GridViewportDirective, CheckBoxCellRendererComponent,
    GridHeadersComponent],
  imports: [
    CommonModule,
    FontAwesomeModule,
    TranslateModule,
    EffectsModule.forFeature(fromEffects.effects)
  ],
  exports: [GridComponent]
})
export class GridRxjsModule {

  constructor() {
    library.add(faAngleRight);
    library.add(faAngleDown);
    library.add(faArrowUp);
    library.add(faArrowDown);
  }
}
