///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {Injectable} from '@angular/core';
import {createFeatureSelector, createSelector, select, Store} from '@ngrx/store';
import {SpinnerState} from './reducers/spinner.reducer';
import {getShowSpinner} from './selectors';
import {SPINNER_FEATURE} from './spinner.model';
import {HideSpinner, ShowSpinner} from './actions';

@Injectable({providedIn: 'root'})
export class SpinnerFacade {

  // public selectors
  public showSpinner$;

  private spinnerStateSelector;

  constructor(private store: Store<SpinnerState>) {
    this.spinnerStateSelector = createFeatureSelector(SPINNER_FEATURE);
    this.showSpinner$ = this.store.pipe(
      select(createSelector(this.spinnerStateSelector, getShowSpinner))
    );
  }

  showSpinner(): void {
    this.store.dispatch(new ShowSpinner());
  }

  hideSpinner(): void {
    this.store.dispatch(new HideSpinner());
  }


}
