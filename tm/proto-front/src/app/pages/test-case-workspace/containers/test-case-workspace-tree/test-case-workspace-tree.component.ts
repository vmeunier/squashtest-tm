///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {ChangeDetectionStrategy, Component, forwardRef, OnInit} from '@angular/core';
import {MAIN_TREE_FACADE} from '../../test-case-workspace.constants';
import {TREE_FACADE} from '@tree/token';

@Component({
  selector: 'sqtm-test-case-workspace-tree',
  template: `
    <sqtm-tree></sqtm-tree>
  `,
  styleUrls: ['./test-case-workspace-tree.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: TREE_FACADE,
      useExisting: forwardRef(() => MAIN_TREE_FACADE)
    }
  ]
})
export class TestCaseWorkspaceTreeComponent {

  constructor() {
  }
}
