///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TestCaseWorkspaceComponent} from './test-case-workspace.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {instance, mock} from 'ts-mockito';
import {GridFacade} from '@ui/grid/store/grid-facade';
import {TreeFacade} from '@tree/store/tree.facade';
import {of} from 'rxjs';
import {NodeState, NodeType} from '@tree/store';
import {MAIN_TREE_FACADE, SECOND_TREE_FACADE} from '@pages/test-case-workspace/test-case-workspace.constants';

describe('TestCaseWorkspaceComponent', () => {
  let component: TestCaseWorkspaceComponent;
  let fixture: ComponentFixture<TestCaseWorkspaceComponent>;

  const treeFacade = instance(mock(TreeFacade));

  beforeEach(done => {
    treeFacade.rootNode$ = of([
      {
        nodeId: 'TestCaseLibrary-1',
        id: 1,
        type: NodeType.TestCaseLibrary,
        name: 'Project-1',
        state: NodeState.closed,
        children: [],
        projectId: 1
      }
    ]);
    done();
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TestCaseWorkspaceComponent],
      providers: [
        {
          provide: MAIN_TREE_FACADE,
          useFactory: () => treeFacade
        },
        {
          provide: SECOND_TREE_FACADE,
          useFactory: () => treeFacade
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestCaseWorkspaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
