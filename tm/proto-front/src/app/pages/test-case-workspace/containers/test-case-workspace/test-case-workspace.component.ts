///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {ChangeDetectionStrategy, Component, Inject, OnInit} from '@angular/core';
import {MAIN_TREE_FACADE, SECOND_TREE_FACADE} from '@pages/test-case-workspace/test-case-workspace.constants';

@Component({
  selector: 'sqtm-test-case-workspace',
  template: `
    <div [sqtmTheme]="'test-case-workspace'" >
      <sqtm-nav-bar></sqtm-nav-bar>
      <div>Selected nodes in tree1 : {{treeMain.selectedNodeIds$ | async}}</div>
      <div>Selected nodes in tree2 : {{secondTree.selectedNodeIds$ | async}}</div>
      <sqtm-split>
        <div class="split-left-panel" #splitLeftPanel>
          <sqtm-test-case-workspace-tree></sqtm-test-case-workspace-tree>
        </div>
        <div class="split-right-panel" #splitRightPanel>
          <sqtm-second-tree></sqtm-second-tree>
        </div>
      </sqtm-split>
    </div>
  `,
  styleUrls: ['./test-case-workspace.component.css'],
  providers: [],
  changeDetection: ChangeDetectionStrategy.OnPush

})
export class TestCaseWorkspaceComponent implements OnInit {

  constructor(@Inject(MAIN_TREE_FACADE) public treeMain, @Inject(SECOND_TREE_FACADE) public secondTree) {
  }

  ngOnInit() {
  }

}
