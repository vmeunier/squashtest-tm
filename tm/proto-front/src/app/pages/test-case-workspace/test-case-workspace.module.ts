///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {TestCaseWorkspaceComponent} from './containers/test-case-workspace/test-case-workspace.component';
import {Store} from '@ngrx/store';
import * as fromStore from './store';
import {Actions, EffectsModule} from '@ngrx/effects';
import {TestCaseWorkspaceService} from './services/test-case-workspace.service';
import {PAGE_URL} from '@common/tokens/tokens';
import {MAIN_TREE_FACADE, SECOND_TREE_FACADE, TEST_CASE_WORKSPACE_ROOT_URL} from './test-case-workspace.constants';
import {MAIN_TREE_ID} from './containers/test-case-workspace-tree/test-case-workspace-tree.constant';
import {TreeModule} from '@tree/tree.module';
import {TestCaseWorkspaceEffects} from './store/effects';
import {treeFacadeProvider} from '@tree/store';
import {TestCaseWorkspaceTreeComponent} from './containers/test-case-workspace-tree/test-case-workspace-tree.component';
import {LoadingGuard} from '@loading/router/loading.guard';
import {LoadingFacade} from '@loading/store/loading.facade';
import {loadingFacadeProvider} from '@loading/store/loading.facade.provider';
import * as splitPanelProvider from '@ui/split/store/workspace-split-panel.provider';
import {SpinnerFacade} from '@ui/spinner/store/spinner.facade';
import {SECOND_TREE_ID, SecondTreeComponent} from './containers/second-tree/second-tree.component';
import {NavBarModule} from '@ui/navbar/nav-bar.module';
import {SplitModule} from '@ui/split/split.module';
import {WorkspaceSplitPanelFacade} from '@ui/split/store/workspace-split-panel.facade';
import {TestCaseTree} from '@tree/store/tree-types';
import {StoreConfigurerService} from '@pages/test-case-workspace/services/store-configurer.service';
import {ThemeUiModule} from '@ui/theme-ui/theme-ui.module';

export const routes: Routes = [
  {
    path: '',
    component: TestCaseWorkspaceComponent,
    canActivate: [LoadingGuard]
  }
];

@NgModule({
  imports: [
    CommonModule,
    TreeModule,
    NavBarModule,
    SplitModule,
    ThemeUiModule,
    RouterModule.forChild(routes),
    EffectsModule.forFeature(fromStore.effects)
  ],
  declarations: [TestCaseWorkspaceComponent, TestCaseWorkspaceTreeComponent, SecondTreeComponent],
  exports: [TestCaseWorkspaceComponent],
  providers: [
    {
      provide: PAGE_URL,
      useValue: TEST_CASE_WORKSPACE_ROOT_URL
    },
    // This StoreConfigurerService is needed in AOT because it seems that we have a race beetween :
    // The instantiation of the LoadingGuard, the StoreModule.forChild() and the facades.
    // As the Loading Guard must have access to Store with this module reducer initialized and facades,
    // i used a dedicated service to enforce instantiation order.
    {
      provide: StoreConfigurerService,
      useClass: StoreConfigurerService
    }, {
      provide: MAIN_TREE_FACADE,
      useFactory: treeFacadeProvider(TEST_CASE_WORKSPACE_ROOT_URL, MAIN_TREE_ID, new TestCaseTree),
      deps: [Store, StoreConfigurerService]
    }, {
      provide: SECOND_TREE_FACADE,
      useFactory: treeFacadeProvider(TEST_CASE_WORKSPACE_ROOT_URL, SECOND_TREE_ID, new TestCaseTree),
      deps: [Store, StoreConfigurerService]
    },
    // clearly indicating that the LoadingGuard must be initiated AFTER the reducers for this module have been initialized.
    {
      provide: LoadingGuard,
      useClass: LoadingGuard,
      deps: [LoadingFacade, SpinnerFacade, StoreConfigurerService]
    }, {
      provide: TestCaseWorkspaceEffects,
      useClass: TestCaseWorkspaceEffects,
      deps: [Actions, TestCaseWorkspaceService, PAGE_URL, MAIN_TREE_FACADE, SECOND_TREE_FACADE]
    }, {
      provide: LoadingFacade,
      useFactory: loadingFacadeProvider(TEST_CASE_WORKSPACE_ROOT_URL),
      deps: [Store, StoreConfigurerService]
    }, {
      provide: WorkspaceSplitPanelFacade,
      useFactory: splitPanelProvider.provideFacade(TEST_CASE_WORKSPACE_ROOT_URL),
      deps: [Store, StoreConfigurerService]
    },
    TestCaseWorkspaceService
  ]
})
export class TestCaseWorkspaceModule {
}
