///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {Actions, Effect} from '@ngrx/effects';
import * as fromTestCaseWorkspaceActions from '../actions';
import {TestCaseWorkspaceService} from '../../services/test-case-workspace.service';
import {TestCaseWorkspacePayload} from '../../model/page/test-case-workspace-payload';
import {TreeFacade} from '@tree/store/tree.facade';
import {createLoadPageDataEffect} from '@loading/store/effects';
import {Inject} from '@angular/core';
import {PAGE_URL} from '@common/tokens/tokens';
import {MAIN_TREE_FACADE, SECOND_TREE_FACADE} from '@pages/test-case-workspace/test-case-workspace.constants';


export class TestCaseWorkspaceEffects {

  constructor(private actions$: Actions, private testCaseWorkspaceService: TestCaseWorkspaceService, @Inject(PAGE_URL) private pagePrefix: string,
              @Inject(MAIN_TREE_FACADE) private tree: TreeFacade, @Inject(SECOND_TREE_FACADE) private secondFacade: TreeFacade) {
  }

  @Effect()
  loadHomeWorkspace$ = createLoadPageDataEffect<TestCaseWorkspacePayload>(
    this.actions$,
    this.pagePrefix,
    (action) => {
      return this.testCaseWorkspaceService.loadWorkspace();
    },
    (testCaseWorkspacePayload: TestCaseWorkspacePayload) => {
      // dispatching loadNode by using facade
      this.tree.loadNodes(testCaseWorkspacePayload.treeNodes);
      this.secondFacade.loadNodes(testCaseWorkspacePayload.treeNodes);
      return [
        new fromTestCaseWorkspaceActions.LoadTestCaseWorkspaceSuccess(testCaseWorkspacePayload.workspaceData)
      ];
    });

}
