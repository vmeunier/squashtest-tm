///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {ActionReducer, combineReducers, createFeatureSelector, MetaReducer} from '@ngrx/store';
import {environment} from '@env/environment';
import * as fromTestCaseWorkspaceReducer from './test-case-workspace.reducer';
import {TEST_CASE_WORKSPACE_ROOT_URL} from '../../test-case-workspace.constants';
import {MAIN_TREE_ID} from '../../containers/test-case-workspace-tree/test-case-workspace-tree.constant';
import * as fromTreeStore from '@tree/store';
import {TreeState} from '@tree/store';
import * as fromSplitPanelStore from '@ui/split/store/workspace-split-panel.provider';
import {LoadingStateSlice} from '@loading/store/loading.model';
import {createLoadingReducer} from '@loading/store/loading.facade.provider';
import {SECOND_TREE_ID} from '../../containers/second-tree/second-tree.component';
import {SplitPanelSlice} from '@ui/split/store/workspace-split-panel.model';

export const STORE_FEATURE_NAME = 'testCaseWorkspace';

export interface TestCaseWorkspaceState extends LoadingStateSlice, SplitPanelSlice {
  testCaseWorkspaceInternalState: fromTestCaseWorkspaceReducer.TestCaseWorkspaceInternalState;
  [MAIN_TREE_ID]: TreeState;
  [SECOND_TREE_ID]: TreeState;
}

export const reducers: ActionReducer<TestCaseWorkspaceState> = combineReducers( {
  testCaseWorkspaceInternalState: fromTestCaseWorkspaceReducer.reducer,
  pageLoad: createLoadingReducer(TEST_CASE_WORKSPACE_ROOT_URL),
  splitPanel: fromSplitPanelStore.provideReducer(TEST_CASE_WORKSPACE_ROOT_URL),
  [MAIN_TREE_ID]: fromTreeStore.createTreeReducer(TEST_CASE_WORKSPACE_ROOT_URL, MAIN_TREE_ID),
  [SECOND_TREE_ID]: fromTreeStore.createTreeReducer(TEST_CASE_WORKSPACE_ROOT_URL, SECOND_TREE_ID)
});


export const metaReducers: MetaReducer<TestCaseWorkspaceState>[] = !environment.production ? [] : [];

export const getTestCaseWorkspaceState = createFeatureSelector<TestCaseWorkspaceState>(STORE_FEATURE_NAME);
