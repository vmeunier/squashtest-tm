///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RequirementWorkspaceComponent} from './containers/requirement-workspace/requirement-workspace.component';
import {RouterModule, Routes} from '@angular/router';
import {MAIN_TREE_FACADE, TEST_CASE_WORKSPACE_ROOT_URL} from '@pages/test-case-workspace/test-case-workspace.constants';
import {treeFacadeProvider} from '@tree/store';
import {MAIN_TREE_ID} from '@pages/test-case-workspace/containers/test-case-workspace-tree/test-case-workspace-tree.constant';
import {TestCaseTree} from '@tree/store/tree-types';
import {Store} from '@ngrx/store';
import {NavBarModule} from '@ui/navbar/nav-bar.module';
import {ThemeUiModule} from '@ui/theme-ui/theme-ui.module';

export const routes: Routes = [
  {
    path: '',
    component: RequirementWorkspaceComponent,
    canActivate: []
  }
];

@NgModule({
  declarations: [RequirementWorkspaceComponent],
  imports: [
    CommonModule,
    NavBarModule,
    ThemeUiModule,
    RouterModule.forChild(routes)
  ]
})
export class RequirementWorkspaceModule { }
