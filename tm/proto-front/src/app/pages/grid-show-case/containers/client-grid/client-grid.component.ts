///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {Component, OnInit, ChangeDetectionStrategy, forwardRef, Inject} from '@angular/core';
import {CLIENT_GRID_FACADE} from '@pages/grid-show-case/grid-show-case.constants';
import {mockData} from '@pages/grid-show-case/data/grid-mock-data';
import {GridFacade} from '@ui/grid/store/grid-facade';
import {GRID_FACADE} from '@ui/grid/token';

@Component({
  selector: 'sqtm-client-grid',
  template: `
    <div style="height: 100%">
      <sqtm-nav-bar></sqtm-nav-bar>
      <div style="height: 90%; padding: 20px;">
        <sqtm-grid></sqtm-grid>
      </div>
    </div>
  `,
  styleUrls: ['./client-grid.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: GRID_FACADE,
      useExisting: forwardRef(() => CLIENT_GRID_FACADE)
    }
  ]
})
export class ClientGridComponent implements OnInit {

  constructor(@Inject(CLIENT_GRID_FACADE) private grid: GridFacade) {
  }

  ngOnInit() {
    this.grid.loadData(mockData, mockData.length);
  }

}
