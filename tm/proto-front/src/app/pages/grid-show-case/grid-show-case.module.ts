///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NavBarModule} from '@ui/navbar/nav-bar.module';
import {RouterModule, Routes} from '@angular/router';
import {GridShowCaseComponent} from './containers/grid-show-case/grid-show-case.component';
import {PAGE_URL} from '@common/tokens/tokens';
import {
  CLIENT_GRID_FACADE,
  CLIENT_GRID_ID,
  CLIENT_TABLE_FACADE,
  CLIENT_TABLE_ID,
  GRID_SHOW_CASE_ROOT_URL,
  SERVER_GRID_FACADE,
  SERVER_GRID_ID,
  TREE_CLIENT_GRID_FACADE,
  TREE_CLIENT_GRID_ID,
  TREE_SERVER_GRID_FACADE,
  TREE_SERVER_GRID_ID
} from '@pages/grid-show-case/grid-show-case.constants';
import {StoreConfigurerService} from '@pages/grid-show-case/services/store-configurer.service';
import {Store} from '@ngrx/store';
import {gridFacadeProvider} from '@ui/grid/store/grid-facade.provider';
import {GridModule} from '@ui/grid/grid.module';
import {GridConfig} from '@ui/grid/store/model/grid-config.model';
import {ClientGridComponent} from './containers/client-grid/client-grid.component';
import {ServerGridComponent} from './containers/server-grid/server-grid.component';
import {GridDataProvider, GridType} from '@ui/grid/store/reducers/state/config.state';
import {TreeClientGridComponent} from './containers/tree-client-grid/tree-client-grid.component';
import {TreeServerGridComponent} from './containers/tree-server-grid/tree-server-grid.component';
import {CellRenderers} from '@ui/grid/interfaces/cell-renderer';
import {ClientTableComponent} from './containers/client-table/client-table.component';
import {column, selectRowColumn} from '@ui/grid/store/model/column-definition.builder';
import {dataTable, grid, treeGrid} from '@ui/grid/store/model/grid-config.builder';

export const routes: Routes = [
  {
    path: '',
    component: GridShowCaseComponent
  }, {
    path: 'client',
    component: ClientGridComponent
  }, {
    path: 'server',
    component: ServerGridComponent
  }, {
    path: 'tree-client',
    component: TreeClientGridComponent
  },
  {
    path: 'tree-server',
    component: TreeServerGridComponent
  }, {
    path: 'table-client',
    component: ClientTableComponent
  },
];

export const clientGridConfig: GridConfig =
  grid().withColumns([
    column('id').withWidth(50),
    column('label').withWidth(300),
    column('createdOn').withWidth(90),
    column('createdBy').withWidth(100),
    column('column3').withWidth(150),
    column('column4').withWidth(200),
    column('column5').withWidth(75),
    column('column6').withWidth(900)
  ])
    .build();

export const treeClientGridConfig: GridConfig =
  treeGrid()
    .withColumns([
      selectRowColumn(),
      column('id').withWidth(200).withRenderer(CellRenderers.TREE),
      column('label').withWidth(300)
    ])
    .build();

export const serverGridConfig: GridConfig =
  grid()
    .server(['test-case-data'])
    .withColumns([
      column('id').withWidth(200),
      column('NAME').withWidth(300)
    ])
    .build();

export const treeServerGridConfig: GridConfig =
  treeGrid()
    .server(['test-case-tree'])
    .withColumns([
      selectRowColumn(),
      column('id').withWidth(200).withRenderer(CellRenderers.TREE),
      column('NAME').withWidth(500),
      column('REFERENCE').withWidth(300),
      column('IMPORTANCE').withI18nKey('test-case.importance').withWidth(200),
      column('TC_STATUS').withWidth(200),
      column('TC_KIND').withWidth(200),
    ])
    .build();

export const clientTableConfig: GridConfig =
  dataTable()
    .withColumns([
      column('id').withWidth(400),
      column('longText').withWidth(1400),
      column('text').withWidth(200),
      column('otherText').withWidth(100)
  ]).build();

@NgModule({
  declarations: [GridShowCaseComponent, ClientGridComponent, ServerGridComponent,
    TreeClientGridComponent, TreeServerGridComponent, ClientTableComponent],
  imports: [
    CommonModule,
    NavBarModule,
    GridModule,
    RouterModule.forChild(routes)
  ],
  providers: [
    {
      provide: PAGE_URL,
      useValue: GRID_SHOW_CASE_ROOT_URL
    }, {
      provide: StoreConfigurerService,
      useClass: StoreConfigurerService
    }, {
      provide: CLIENT_GRID_FACADE,
      useFactory: gridFacadeProvider(GRID_SHOW_CASE_ROOT_URL, CLIENT_GRID_ID, clientGridConfig),
      deps: [Store, StoreConfigurerService]
    }, {
      provide: TREE_CLIENT_GRID_FACADE,
      useFactory: gridFacadeProvider(GRID_SHOW_CASE_ROOT_URL, TREE_CLIENT_GRID_ID, treeClientGridConfig),
      deps: [Store, StoreConfigurerService]
    },
    {
      provide: SERVER_GRID_FACADE,
      useFactory: gridFacadeProvider(GRID_SHOW_CASE_ROOT_URL, SERVER_GRID_ID, serverGridConfig),
      deps: [Store, StoreConfigurerService]
    },
    {
      provide: TREE_SERVER_GRID_FACADE,
      useFactory: gridFacadeProvider(GRID_SHOW_CASE_ROOT_URL, TREE_SERVER_GRID_ID, treeServerGridConfig),
      deps: [Store, StoreConfigurerService]
    }, {
      provide: CLIENT_TABLE_FACADE,
      useFactory: gridFacadeProvider(GRID_SHOW_CASE_ROOT_URL, CLIENT_TABLE_ID, clientTableConfig),
      deps: [Store, StoreConfigurerService]
    },
  ]
})
export class GridShowCaseModule {
}
