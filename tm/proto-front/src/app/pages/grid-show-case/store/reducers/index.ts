///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {ActionReducer, combineReducers, createFeatureSelector, MetaReducer} from '@ngrx/store';
import {environment} from '@env/environment';
import {
  GRID_SHOW_CASE_ROOT_URL,
  CLIENT_GRID_ID,
  SERVER_GRID_ID,
  TREE_CLIENT_GRID_ID,
  TREE_SERVER_GRID_ID, CLIENT_TABLE_ID
} from '@pages/grid-show-case/grid-show-case.constants';
import {createGridReducer} from '@ui/grid/store/grid-facade.provider';
import {GridState} from '@ui/grid/store/reducers/state/grid.state';

export const STORE_FEATURE_NAME = 'gridShowCase';

export interface GridShowCaseState {
  [CLIENT_GRID_ID]: GridState;
  [TREE_CLIENT_GRID_ID]: GridState;
  [SERVER_GRID_ID]: GridState;
  [TREE_SERVER_GRID_ID]: GridState;
  [CLIENT_TABLE_ID]: GridState;
}

export const reducers: ActionReducer<GridShowCaseState> = combineReducers({
  [CLIENT_GRID_ID]: createGridReducer(GRID_SHOW_CASE_ROOT_URL, CLIENT_GRID_ID),
  [TREE_CLIENT_GRID_ID]: createGridReducer(GRID_SHOW_CASE_ROOT_URL, TREE_CLIENT_GRID_ID),
  [SERVER_GRID_ID]: createGridReducer(GRID_SHOW_CASE_ROOT_URL, SERVER_GRID_ID),
  [TREE_SERVER_GRID_ID]: createGridReducer(GRID_SHOW_CASE_ROOT_URL, TREE_SERVER_GRID_ID),
  [CLIENT_TABLE_ID]: createGridReducer(GRID_SHOW_CASE_ROOT_URL, CLIENT_TABLE_ID),
});


export const metaReducers: MetaReducer<GridShowCaseState>[] = !environment.production ? [] : [];

