///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {DataRow, DataRowOpenState} from '@ui/grid/store/model/data-row.model';

export const treeMockData: DataRow[] = [
  {
    id: 'Test-Case-Library-1',
    parentRowId: null,
    label: 'Project - 1',
    children: ['Test-Case-Folder-1', 'Test-Case-Folder-2'],
    state: DataRowOpenState.closed
  }, {
    id: 'Test-Case-Folder-1',
    parentRowId: 'Test-Case-Library-1',
    label: 'Dossier - 1',
    children: ['Test-Case-3', 'Test-Case-4'],
    state: DataRowOpenState.closed
  }, {
    id: 'Test-Case-Folder-2',
    parentRowId: 'Test-Case-Library-1',
    label: 'Dossier - 2',
    children: [],
    state: DataRowOpenState.leaf
  }, {
    id: 'Test-Case-3',
    parentRowId: 'Test-Case-Folder-1',
    label: 'Test case 1',
    children: [],
    state: DataRowOpenState.leaf
  }, {
    id: 'Test-Case-4',
    parentRowId: 'Test-Case-Folder-1',
    label: 'Test case 2',
    children: [],
    state: DataRowOpenState.leaf
  }, {
    id: 'Test-Case-Folder-3',
    parentRowId: 'Test-Case-Library-1',
    label: 'Dossier - 3',
    children: [],
    state: DataRowOpenState.leaf
  },
  {
    id: 'Test-Case-Library-2',
    parentRowId: null,
    label: 'Project - 2',
    children: [],
    state: DataRowOpenState.closed
  },
  {
    id: 'Test-Case-Library-3',
    parentRowId: null,
    label: 'Project - 3',
    children: [],
    state: DataRowOpenState.closed
  }
];
