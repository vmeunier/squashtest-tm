///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {TreeFacade} from '@tree/store/tree.facade';
import {InjectionToken} from '@angular/core';

export const GRID_SHOW_CASE_ROOT_URL = 'grid-show-case';

export const CLIENT_GRID_FACADE = new InjectionToken<TreeFacade>('DI token for the client side grid of grid show case');
export const TREE_CLIENT_GRID_FACADE = new InjectionToken<TreeFacade>('DI token for the client side tree-grid of grid show case');
export const SERVER_GRID_FACADE = new InjectionToken<TreeFacade>('DI token for the server side grid of grid show case');
export const TREE_SERVER_GRID_FACADE = new InjectionToken<TreeFacade>('DI token for the server side tree-grid of grid show case');
export const CLIENT_TABLE_FACADE = new InjectionToken<TreeFacade>('DI token for the client side table of grid show case');

export const CLIENT_GRID_ID = 'clientGrid';
export const TREE_CLIENT_GRID_ID = 'treeClientGrid';
export const SERVER_GRID_ID = 'serverGrid';
export const TREE_SERVER_GRID_ID = 'treeServerGrid';
export const CLIENT_TABLE_ID = 'clientTable';
