///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {HomeWorkspaceState, selectWelcomeMessage} from '../../store';
import {Observable} from 'rxjs';

@Component({
  selector: 'sqtm-home-workspace-page',
  template: `
    <div [sqtmTheme]="'home-workspace'">
      <sqtm-nav-bar></sqtm-nav-bar>
      <div>
        <sqtm-welcome-message [welcomeMessage]="welcomeMessage$ | async"></sqtm-welcome-message>
      </div>
    </div>
  `,
  styleUrls: ['./home-workspace-page.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeWorkspacePageComponent implements OnInit {

  welcomeMessage$: Observable<string>;

  constructor(private store: Store<HomeWorkspaceState>) {
  }

  ngOnInit() {
    this.welcomeMessage$ = this.store.pipe(
      select(selectWelcomeMessage)
    );
  }

}
