///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomeWorkspacePageComponent} from './containers/home-workspace-page/home-workspace-page.component';
import {RouterModule, Routes} from '@angular/router';
import {Store} from '@ngrx/store';
import * as fromStore from './store';
import {EffectsModule} from '@ngrx/effects';
import {WelcomeMessageComponent} from './components/welcome-message/welcome-message.component';
import {HomeWorkspaceService} from './services/home-workspace.service';
import {PAGE_URL} from '@common/tokens/tokens';
import {HOME_WORKSPACE_ROOT_URL} from './home-workspace-constants';
import {LoadingGuard} from '@loading/router/loading.guard';
import {LoadingFacade} from '@loading/store/loading.facade';
import {loadingFacadeProvider} from '@loading/store/loading.facade.provider';
import {SpinnerFacade} from '@ui/spinner/store/spinner.facade';
import {NavBarModule} from '@ui/navbar/nav-bar.module';
import {StoreConfigurerService} from '@pages/home-workspace/services/store-configurer.service';
import {ThemeUiModule} from '@ui/theme-ui/theme-ui.module';

export const routes: Routes = [
  {
    path: '',
    component: HomeWorkspacePageComponent,
    canActivate: [LoadingGuard]
  }
];

@NgModule({
  imports: [
    CommonModule,
    EffectsModule.forFeature(fromStore.effects),
    RouterModule.forChild(routes),
    NavBarModule,
    ThemeUiModule
  ],
  declarations: [HomeWorkspacePageComponent, WelcomeMessageComponent],
  providers: [
    {
      provide: PAGE_URL,
      useValue: HOME_WORKSPACE_ROOT_URL
    }, {
      provide: StoreConfigurerService,
      useClass: StoreConfigurerService
    }, {
      provide: LoadingGuard,
      useClass: LoadingGuard,
      deps: [LoadingFacade, SpinnerFacade]
    }, {
      provide: LoadingFacade,
      useFactory: loadingFacadeProvider(HOME_WORKSPACE_ROOT_URL),
      // dep on StoreConfigurerService, so we are sure that even with AOT compilation
      // the store will have reducers initialized before facade is created
      deps: [Store, StoreConfigurerService]
    },
    HomeWorkspaceService]
})
export class HomeWorkspaceModule {
}
