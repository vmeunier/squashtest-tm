///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {ActionReducer, combineReducers, createFeatureSelector} from '@ngrx/store';
import * as fromHomeWorkspaceReducer from './home-workspace.reducer';
import {HOME_WORKSPACE_ROOT_URL} from '../../home-workspace-constants';
import {createLoadingReducer} from '@loading/store/loading.facade.provider';
import camelcase from 'camelcase';

export const STORE_FEATURE_NAME = camelcase(HOME_WORKSPACE_ROOT_URL);

export interface HomeWorkspaceState {
  homeWorkspaceInternalState: fromHomeWorkspaceReducer.HomeWorkspaceInternalState;
}

export const reducers: ActionReducer<HomeWorkspaceState> = combineReducers({
  homeWorkspaceInternalState: fromHomeWorkspaceReducer.reducer,
  pageLoad: createLoadingReducer(HOME_WORKSPACE_ROOT_URL)
});

export const getHomeWorkspaceState = createFeatureSelector<HomeWorkspaceState>(STORE_FEATURE_NAME);
