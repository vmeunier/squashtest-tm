///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {Inject, Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import * as fromHomeWorkspaceActions from '../actions';
import {HomeWorkspaceService} from '../../services/home-workspace.service';
import {HomeWorkspacePayload} from '../../model/page/home-workspace-payload';
import {createLoadPageDataEffect} from '@loading/store/effects';
import {PAGE_URL} from '@common/tokens/tokens';


@Injectable()
export class HomeWorkspaceEffects {

  constructor(private actions$: Actions, private homeWorkspaceService: HomeWorkspaceService, @Inject(PAGE_URL) private rootUrl: string) {

  }

  @Effect()
  loadHomeWorkspace$ = createLoadPageDataEffect<HomeWorkspacePayload>(this.actions$,
    this.rootUrl,
    (action) => {
      return this.homeWorkspaceService.loadWorkspace();
    },
    (response) => {
      return [new fromHomeWorkspaceActions.LoadHomeWorkspaceSuccess(response.workspaceData)];
    });
}
