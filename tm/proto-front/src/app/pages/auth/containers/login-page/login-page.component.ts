///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {AuthenticationFacade} from '@features/global/authentication/authentication.facade';

@Component({
  selector: 'sqtm-login-page',
  template: `
    <form [formGroup]="form">
      <sqtm-login-form [parent]="form" (submit)="handleLogin()"></sqtm-login-form>
    </form>
  `,
  styleUrls: ['./login-page.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginPageComponent implements OnInit {

  form = this.fb.group({
    login: '',
    password: ''
  });

  constructor(private fb: FormBuilder, private authFacade : AuthenticationFacade) { }

  ngOnInit() {
  }

  handleLogin() {
    this.authFacade.login(this.form.get('login').value, this.form.get('password').value);
  }
}
