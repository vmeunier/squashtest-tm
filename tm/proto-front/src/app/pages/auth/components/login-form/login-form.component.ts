///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'sqtm-login-form',
  template: `
   <div [formGroup]="parent">
     <label>
       {{'label.login'| translate}}
       <input type="text" formControlName="login">
     </label>
     <label>
       {{'label.password'| translate}}
       <input type="password" formControlName="password">
     </label>
     <button type="button" (click)="clickSubmit()">LOGIN</button>
   </div>
  `,
  styleUrls: ['./login-form.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginFormComponent implements OnInit {

  @Input()
  parent: FormGroup;

  @Output()
  submit = new EventEmitter<any>();

  constructor() {
  }

  ngOnInit() {
  }

  clickSubmit(){
    this.submit.emit();
  }

}
