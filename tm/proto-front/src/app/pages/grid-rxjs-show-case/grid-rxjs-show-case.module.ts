///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GridRxjsShowCaseComponent} from './grid-rxjs-show-case/grid-rxjs-show-case.component';
import {RouterModule, Routes} from '@angular/router';
import {GridRxjsModule} from '@ui/grid_rxjs/grid-rxjs.module';
import {CLIENT_GRID_SERVICE} from '@pages/grid-rxjs-show-case/grid-rxjs-show-case.constants';
import {provideClientGridService} from '@ui/grid_rxjs/grid.service.provider';
import {GridConfig} from '@ui/grid_rxjs/store/model/grid-config.model';
import {grid} from '@ui/grid_rxjs/store/model/grid-config.builder';
import {column} from '@ui/grid_rxjs/store/model/column-definition.builder';
import {GridRxjsClientSideComponent} from './grid-rxjs-client-side/grid-rxjs-client-side.component';
import { GridRxjsServerSideComponent } from './grid-rxjs-server-side/grid-rxjs-server-side.component';


export const routes: Routes = [
  {
    path: '',
    component: GridRxjsShowCaseComponent
  },
  {
    path: 'client',
    component: GridRxjsClientSideComponent
  },
  {
    path: 'server',
    component: GridRxjsServerSideComponent
  }
];

export const clientGridConfig: GridConfig =
  grid().withColumns([
    column('id').withWidth(50),
    column('label').withWidth(300),
    column('createdOn').withWidth(90),
    column('createdBy').withWidth(100),
    column('column3').withWidth(150),
    column('column4').withWidth(200),
    column('column5').withWidth(75),
    column('column6').withWidth(900)
  ]).build();


@NgModule({
  declarations: [GridRxjsShowCaseComponent, GridRxjsClientSideComponent, GridRxjsServerSideComponent],
  imports: [
    CommonModule,
    GridRxjsModule,
    RouterModule.forChild(routes)
  ],
  providers: [
    {provide: CLIENT_GRID_SERVICE, useFactory: provideClientGridService(clientGridConfig)}
  ]

})
export class GridRxjsShowCaseModule {
}
