///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {ChangeDetectionStrategy, Component, forwardRef, Inject, OnInit} from '@angular/core';
import {GRID_SERVICE} from '@ui/grid_rxjs/token';
import {CLIENT_GRID_SERVICE} from '@pages/grid-rxjs-show-case/grid-rxjs-show-case.constants';
import {mockData} from '@pages/grid-show-case/data/grid-mock-data';
import {GridRxjsService} from '@ui/grid_rxjs/services/grid-rxjs.service';

@Component({
  selector: 'sqtm-grid-rxjs-client-side',
  template: `
    <div style="height: 90%; padding: 20px;">
      <sqtm-grid></sqtm-grid>
    </div>
  `,
  styleUrls: ['./grid-rxjs-client-side.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: GRID_SERVICE, useExisting: forwardRef(() => CLIENT_GRID_SERVICE)
    }
  ]
})
export class GridRxjsClientSideComponent implements OnInit {

  constructor(@Inject(CLIENT_GRID_SERVICE) private grid: GridRxjsService) {
  }

  ngOnInit() {
    this.grid.loadData(mockData, mockData.length);
  }

}
