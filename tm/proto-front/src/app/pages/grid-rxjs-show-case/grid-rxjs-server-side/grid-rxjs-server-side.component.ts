///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {ChangeDetectionStrategy, Component, Inject, OnInit} from '@angular/core';
import {GRID_SERVICE} from '@ui/grid_rxjs/token';
import {provideClientGridService, provideServerGridService} from '@ui/grid_rxjs/grid.service.provider';
import {GridConfig} from '@ui/grid_rxjs/store/model/grid-config.model';
import {grid} from '@ui/grid_rxjs/store/model/grid-config.builder';
import {column} from '@ui/grid_rxjs/store/model/column-definition.builder';
import {GridRxjsService} from '@ui/grid_rxjs/services/grid-rxjs.service';
import {RestService} from '@common/services/rest.service';
import {ServerGridRxjsService} from '@ui/grid_rxjs/services/server-grid-rxjs.service';


export const serverGridConfig: GridConfig =
  grid()
    .server(['test-case-data'])
    .withColumns([
      column('id').withWidth(200),
      column('NAME').withWidth(300)
    ])
    .build();

@Component({
  selector: 'sqtm-grid-rxjs-server-side',
  template: `
    <div style="height: 90%; padding: 20px;">
      <sqtm-grid></sqtm-grid>
    </div>
  `,
  styleUrls: ['./grid-rxjs-server-side.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: GRID_SERVICE, useFactory: provideServerGridService(serverGridConfig), deps: [RestService]
    }
  ]
})
export class GridRxjsServerSideComponent implements OnInit {

  constructor(@Inject(GRID_SERVICE) private gridService: ServerGridRxjsService) {
  }

  ngOnInit() {
    // little tricks to force data load from server
    // in a true page you should provide the initial data at page load
    this.gridService.refreshData();
  }

}
