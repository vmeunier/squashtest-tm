///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PluginShowCaseComponent} from './containers/plugin-show-case/plugin-show-case.component';
import {RouterModule, Routes} from '@angular/router';
import {NavBarModule} from '@ui/navbar/nav-bar.module';
import {ContentInjectionComponent} from './containers/content-injection/content-injection.component';
import {PageInjectionComponent} from './containers/page-injection/page-injection.component';

export const routes: Routes = [
  {
    path: '',
    component: PluginShowCaseComponent
  },
  {
    path: 'content-injection',
    component: ContentInjectionComponent
  },
  {
    path: 'page-injection',
    component: PageInjectionComponent
  }
];

@NgModule({
  declarations: [PluginShowCaseComponent, ContentInjectionComponent, PageInjectionComponent],
  imports: [
    CommonModule,
    NavBarModule,
    RouterModule.forChild(routes)
  ]
})
export class PluginShowCaseModule {
}
