///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {PluginLoaderService} from '@features/global/plugin/services/plugin-loader.service';
import {Route, Router} from '@angular/router';
import {BehaviorSubject} from 'rxjs';

@Component({
  selector: 'sqtm-page-injection',
  template: `
    <a class="nav-link" routerLinkActive="active" [routerLink]="['/','plugin-a','comp-a']">page-inside-plugin</a>
  `,
  styleUrls: ['./page-injection.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PageInjectionComponent implements OnInit {


  existingRoutes: BehaviorSubject<Route[]>;

  constructor(private pluginLoaderService: PluginLoaderService, private router: Router) {
    this.existingRoutes = new BehaviorSubject<Route[]>([]);
  }

  ngOnInit(): void {
    this.router.config.unshift({
      path: 'plugin-a',
      loadChildren:
        'org/squashtest/tm/plugin/project/plugin-new-frontend-lib-a/bundles/plugin-new-frontend-lib-a#PluginNewFrontendLibAModule',
      // loadChildren: 'org/squashtest/tm/plugin/project/plugin-new-frontend-lib-a/esm2015/lib/plugin-new-frontend-lib-a.module.ngfactory.js#PluginNewFrontendLibAModuleNgFactory'
    });

  }

}
