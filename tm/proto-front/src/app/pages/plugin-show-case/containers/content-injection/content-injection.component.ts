///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {Component, OnInit, ChangeDetectionStrategy, ViewContainerRef, ViewChild} from '@angular/core';
import {PluginLoaderService} from '@features/global/plugin/services/plugin-loader.service';
import {PluginModuleReference} from '@features/global/plugin/store/model/PluginModuleReference';

@Component({
  selector: 'sqtm-content-injection',
  template: `
    <div style="height: 100%">
      <sqtm-nav-bar></sqtm-nav-bar>
      <div>
        <div>CONTENT FROM CORE</div>
        <div #pluginExtensionPoint></div>
        <div>CONTENT FROM CORE</div>
      </div>
    </div>
  `,
  styleUrls: ['./content-injection.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ContentInjectionComponent implements OnInit {

  @ViewChild('pluginExtensionPoint', {read: ViewContainerRef})
  pluginExtensionPoint: ViewContainerRef;

  constructor(private pluginLoaderService: PluginLoaderService) {

  }

  ngOnInit(): void {
    const pluginModuleReference = new PluginModuleReference('PluginAModule', 'org/squashtest/tm/plugin/plugin-a-module.js');
    this.pluginLoaderService.loadPluginModule(pluginModuleReference).then(
      componentReference => {
        console.log (`loading component ref ${componentReference}`);
        this.pluginExtensionPoint.clear();
        this.pluginExtensionPoint.insert(componentReference.hostView);
      });
  }

}
