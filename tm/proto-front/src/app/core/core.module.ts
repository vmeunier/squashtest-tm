///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {NgModule, Optional, SkipSelf} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SpinnerModule} from '@ui/spinner/spinner.module';
import {BugTrackersModule} from '@features/global/bugtrackers/bug-trackers.module';
import {AuthenticationModule} from '@features/global/authentication/authentication.module';
import {ThemeModule} from '@features/global/theme/theme.module';

@NgModule({
  imports: [
    CommonModule,
    SpinnerModule,
    BugTrackersModule,
    AuthenticationModule,
    ThemeModule
  ],
  declarations: [],
  exports: [
    SpinnerModule,
    BugTrackersModule
  ]
})
export class CoreModule {

  constructor(@Optional() @SkipSelf() core: CoreModule) {
    if (core) {
      throw new Error('This module should be loaded only one time, in App Module');
    }
  }
}
