///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {BrowserModule} from '@angular/platform-browser';
import {Compiler, COMPILER_OPTIONS, CompilerFactory, NgModule, NgModuleFactoryLoader} from '@angular/core';

import {AppComponent} from './app.component';
import {RouterModule, Routes} from '@angular/router';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {StoreModule} from '@ngrx/store';
import {CustomSerializer, effects, metaReducers, reducers} from '@root/store';
import {RouterStateSerializer, StoreRouterConnectingModule} from '@ngrx/router-store';
import {environment} from '@env/environment';
import {EffectsModule} from '@ngrx/effects';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {BACKEND_ROOT_URL, JIT_COMPILER} from '@common/tokens/tokens';
import {CoreModule} from './core/core.module';
import {TEST_CASE_WORKSPACE_ROOT_URL} from '@pages/test-case-workspace/test-case-workspace.constants';
import {JitCompilerFactory} from '@angular/platform-browser-dynamic';
import {SqtmModuleLoaderService} from '@features/global/plugin/services/sqtm-module-loader.service';

export const routes: Routes = [
  { path: '',
    redirectTo: '/home-workspace',
    pathMatch: 'full'
  },
  {path: 'login', loadChildren: './pages/auth/auth.module#AuthModule'},
  {path: 'home-workspace', loadChildren: './pages/home-workspace/home-workspace.module#HomeWorkspaceModule'},
  {path: TEST_CASE_WORKSPACE_ROOT_URL, loadChildren: './pages/test-case-workspace/test-case-workspace.module#TestCaseWorkspaceModule'},
  {path: 'requirement-workspace', loadChildren: './pages/requirement-workspace/requirement-workspace.module#RequirementWorkspaceModule'},
  {path: 'grid-show-case', loadChildren: './pages/grid-show-case/grid-show-case.module#GridShowCaseModule'},
  {path: 'grid-rxjs-show-case', loadChildren: './pages/grid-rxjs-show-case/grid-rxjs-show-case.module#GridRxjsShowCaseModule'},
  {path: 'plugin-show-case', loadChildren: './pages/plugin-show-case/plugin-show-case.module#PluginShowCaseModule'},
  {path: '**', loadChildren: './pages/not-found/not-found.module#NotFoundModule'}
];

/**
 * Function used to instantiate a JIT compiler, required in AOT mode as compiler is not available by default.
 * Squash TM needs runtime compilation for the plugin system.
 * @param compilerFactory provided with implementation class JitCompilerFactory
 */
export function createCompiler(compilerFactory: CompilerFactory) {
  return compilerFactory.createCompiler();
}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/translations_', '.json');
}

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    RouterModule.forRoot(routes),
    StoreModule.forRoot(reducers, {metaReducers}),
    StoreRouterConnectingModule,
    EffectsModule.forRoot(effects),
    CoreModule,
    environment.production ? [] : StoreDevtoolsModule.instrument()
  ],
  providers: [
    {
      provide: RouterStateSerializer,
      useClass: CustomSerializer
    }, {
      provide: BACKEND_ROOT_URL,
      useValue: 'backend'
    }, {
      provide: NgModuleFactoryLoader,
      useClass: SqtmModuleLoaderService,
      deps: [Compiler, JIT_COMPILER]
    },
    {provide: COMPILER_OPTIONS, useValue: {}, multi: true},
    {provide: CompilerFactory, useClass: JitCompilerFactory, deps: [COMPILER_OPTIONS]},
    {provide: JIT_COMPILER, useFactory: createCompiler, deps: [CompilerFactory]}
  ],
  declarations: [AppComponent],
  bootstrap: [AppComponent],
  exports: [RouterModule]
})
export class AppModule {
}
