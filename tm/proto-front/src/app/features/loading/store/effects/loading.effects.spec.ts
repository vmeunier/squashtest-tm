///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {TestBed} from '@angular/core/testing';
import {provideMockActions} from '@ngrx/effects/testing';
import {Observable, of} from 'rxjs';
import {HomeWorkspaceEffects} from '@pages/home-workspace/store';
import {HomeWorkspaceService} from '@pages/home-workspace/services/home-workspace.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {BACKEND_ROOT_URL, PAGE_URL} from '@common/tokens/tokens';
import {HOME_WORKSPACE_ROOT_URL} from '@pages/home-workspace/home-workspace-constants';


describe('LoadingEffects', () => {
  const actions$: Observable<any> = null;
  let effects: HomeWorkspaceEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        HomeWorkspaceService,
        HomeWorkspaceEffects,
        {provide: PAGE_URL, useValue: HOME_WORKSPACE_ROOT_URL},
        {provide: BACKEND_ROOT_URL, useValue: 'backend'},
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get(HomeWorkspaceEffects);
    const  homeWorkspaceService = TestBed.get(HomeWorkspaceService);
    spyOn(homeWorkspaceService, 'loadWorkspace').and.returnValue(of({}));
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
