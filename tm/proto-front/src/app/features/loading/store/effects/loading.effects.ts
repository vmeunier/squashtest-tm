///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {Actions, ofType} from '@ngrx/effects';
import {Action} from '@ngrx/store';
import {Observable, of} from 'rxjs';
import {catchError, filter, switchMap, tap} from 'rxjs/operators';
import * as fromLoadPageActions from '../actions';
import {HttpErrorResponse} from '@angular/common/http';
import {NavigateTo} from '@root/store';
import camelCase from 'camelcase';
import {hasBugTrackersData} from '@features/global/bugtrackers/bug-tracker.model';
import {LoadBugTrackers} from '@features/global/bugtrackers/bug-tracker.actions';
import {hasAuthenticatedUserData} from '@features/global/authentication/authenticated-user.model';
import {LoadAuthenticatedUser} from '@features/global/authentication/authenticated-user.actions';

import {LoadThemes} from '@features/global/theme/store/theme.actions';
import {Themes} from '@features/global/theme/themes';

export function createLoadPageDataEffect<R>
(actions$: Actions, rootUrl: string, actionHandler?: (action) => Observable<R>, responseHandler?: (reponse: R) => Action[]) {

  const pagePrefix = camelCase(rootUrl);
  return actions$.pipe(
    ofType(fromLoadPageActions.LoadPageDataActionTypes.LoadPageData),
    filter((action: fromLoadPageActions.LoadPageData) => action.pagePrefix === pagePrefix),
    switchMap((action) => {
      return actionHandler(action).pipe(
        switchMap((response: R) => {
          const automaticLoadActions = [];

          if (hasBugTrackersData(response)) {
            automaticLoadActions.push(new LoadBugTrackers({bugTrackers: response.bugTrackers}));
          }

          if (hasAuthenticatedUserData(response)) {
            automaticLoadActions.push(new LoadAuthenticatedUser({user: response.authenticatedUser}));
          }

          const actions: Action[] = responseHandler(response);
          return [...automaticLoadActions, ...actions, new fromLoadPageActions.LoadPageDataSuccess(pagePrefix)];
        }),
        catchError((err: HttpErrorResponse) => {
          switch (err.status) {
            case 401 : {
              return of(
                new fromLoadPageActions.LoadPageDataFailure(pagePrefix),
                new NavigateTo({path: ['/login']})
              );
            }
          }
          return of(new fromLoadPageActions.LoadPageDataFailure(pagePrefix));
        })
      );
    })
  );
}
