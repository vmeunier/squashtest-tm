///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {Observable} from 'rxjs';
import {LoadingStatus} from './loading-status';
import {createSelector, select, Store} from '@ngrx/store';
import {selectLoadingStatus} from './selectors/loading.selector';
import {PageFacade} from '@common/store/facades/page.facade';
import {LoadPageDataInit} from './actions';
import {LoadPageData} from './actions';
import * as fromReducer from './reducers';

export class LoadingFacade extends PageFacade {

  // public selectors
  public loadingStatus$: Observable<LoadingStatus>;

  private loadingStateSelector;

  constructor(pageUrl: string, store: Store<any>) {
    super(pageUrl, store);
    console.log('instantiation of loading facade, logging this', this);
    this.loadingStateSelector = createSelector(this.featureSelector, state => state.pageLoad);

    // public selectors init
    this.loadingStatus$ = this.store.pipe(
      select(createSelector(this.loadingStateSelector, selectLoadingStatus))
    );
  }

  public initPageDataLoading(): void {
    this.store.dispatch(new LoadPageDataInit(this.featureName));
  }

  public loadPageData(): void {
    this.store.dispatch(new LoadPageData(this.featureName));
  }

  get initialState() {
    return fromReducer.initialState;
  }

}
