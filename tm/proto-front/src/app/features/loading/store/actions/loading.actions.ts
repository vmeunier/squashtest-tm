///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {PageAction} from '@common/store/factories/actions/actions-type';

export enum LoadPageDataActionTypes {
  LoadPageData = '[LoadPageData] Load Page Data',
  LoadPageDataSuccess = '[LoadPageData] Load Page Data Success',
  LoadPageDataFailure = '[LoadPageData] Load Page Data Failure',
  LoadPageDataInit = '[LoadPageData] Load Page Data Init'
}


export class LoadPageData implements PageAction {
  readonly type = LoadPageDataActionTypes.LoadPageData;
  constructor(public pagePrefix: string) {
  }
}

export class LoadPageDataInit implements PageAction {
  readonly type = LoadPageDataActionTypes.LoadPageDataInit;
  constructor(public pagePrefix: string) {
  }
}

export class LoadPageDataSuccess implements PageAction {
  readonly type = LoadPageDataActionTypes.LoadPageDataSuccess;
  constructor(public pagePrefix: string) {
  }
}

export class LoadPageDataFailure implements PageAction {
  readonly type = LoadPageDataActionTypes.LoadPageDataFailure;
  constructor(public pagePrefix: string) {
  }
}

export type LoadPageDataActions =
| LoadPageData
| LoadPageDataInit
| LoadPageDataSuccess
| LoadPageDataFailure

