///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {LoadingStatus} from '../loading-status';
import {LoadPageDataActions, LoadPageDataActionTypes} from '../actions';
import {createPageReducer} from '@common/store/factories/reducers/reducers-factory';

export interface LoadingPageState {
  loadingStatus: LoadingStatus;
}

export const initialState: LoadingPageState = {
  loadingStatus: LoadingStatus.INIT
};

export function createLoadPageDataReducer(pagePrefix: string) {
  function reducer(state = initialState, action: LoadPageDataActions): LoadingPageState {
    switch (action.type) {
      case LoadPageDataActionTypes.LoadPageDataInit: {
        return initialState;
      }

      case LoadPageDataActionTypes.LoadPageData: {
        return {
          ...state,
          loadingStatus: LoadingStatus.LOADING
        };
      }

      case LoadPageDataActionTypes.LoadPageDataSuccess: {
        return {
          ...state,
          loadingStatus: LoadingStatus.SUCCESS
        };
      }

      case LoadPageDataActionTypes.LoadPageDataFailure: {
        return {
          ...state,
          loadingStatus: LoadingStatus.FAILURE
        };
      }

      default:
        return state;
    }
  }

  return createPageReducer(pagePrefix, reducer, initialState);
}
