///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {LoadingStatus} from '@loading/store/loading-status';
import {LoadingFacade} from '@loading/store/loading.facade';
import {SpinnerFacade} from '@ui/spinner/store/spinner.facade';
import {Observable, of} from 'rxjs';
import {catchError, filter, map, switchMap, take, tap} from 'rxjs/operators';


/**
 * This function handle the loading process of a root screen in Squash TM app.
 * It will :
 * Show the global spinner.
 * Dispatch an init action if the state of the module is not INIT, using pagePrefix.
 * Dispatch a load action using pagePrefix. In Squash TM we always reload data during navigation, so the load is performed each time.
 * It would be a good idea to cache all project configuration but it require some work server side also.
 * Wait for the loading status to be SUCCESS or FAILURE.
 * Hide the global spinner.
 *
 * @returns The guard as an Observable<boolean> as required by angular.
 * Note that angular will wait to the observable to complete and use last value.
 * So this guard use take(1) to unsub.
 */
export function createLoadingGuard<T>(loadingFacade: LoadingFacade, spinnerFacade: SpinnerFacade)
  : Observable<boolean> {

  return loadingFacade.loadingStatus$
    .pipe(
      // take only the first element and unsubscribe to prevent infinite loop
      // as we dispatch multiple action that update this status attribute
      take(1),
      tap(() => spinnerFacade.showSpinner()), // show global spinner
      tap((loadingStatus: LoadingStatus) => { // if status is not INIT dispatch an init action
        if (loadingStatus !== LoadingStatus.INIT) {
          loadingFacade.initPageDataLoading();
        }
      }),
      switchMap(() => { // switch map to a brand new observable of the status
        return loadingFacade.loadingStatus$.pipe(
          // wait for the status to be INIT (ie wait for the init action to be handled by reducers)
          filter((loadingStatus: LoadingStatus) => loadingStatus === LoadingStatus.INIT),
          take(1),
          // take the first value (witch is INIT of course) and unsubscribe from the select.
          // No further updates will go trough this stream
          switchMap(() => { // switch map to a brand new observable of the status
            return loadingFacade.loadingStatus$.pipe(
              tap((loadingStatus: LoadingStatus) => {
                // If status is INIT we dispatch a load action that will pass it to LOADED.
                if (loadingStatus === LoadingStatus.INIT) {
                  loadingFacade.loadPageData();
                }
              }),
              // we are always piped to fromStore.getLoadingStatus stream we will received all values
              // so we wait for status to be SUCCESS or FAILURE to proceed further in the stream
              filter((loadingStatus: LoadingStatus) => {
                  return loadingStatus === LoadingStatus.SUCCESS || loadingStatus === LoadingStatus.FAILURE;
                }
              ),
              // at this stage status is either SUCCESS or FAILURE and the http request have been solved.
              // We take the first value, that will complete the inner observable, and thus allow Angular to resolve the guard
              take(1),
              tap(() => spinnerFacade.hideSpinner()),
              // now we convert loading status in boolean that honor CanActivate contract
              map((loadingStatus: LoadingStatus) => loadingStatus === LoadingStatus.SUCCESS),
              catchError(err => of(false))
            );
          })
        );
      })
    );
}
