///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {createLoadingGuard} from './loading-initial-data';
import {LoadingFacade} from '@loading/store/loading.facade';
import {SpinnerFacade} from '@ui/spinner/store/spinner.facade';

/**
 * Guard responsible of a scoped LoadingGuard driven by a slice of ngrx state. It use a facade that must be provided by the same module.
 * Ie you must create a LoadingFacade and provide it to the same module that is using this guard.
 */
@Injectable()
export class LoadingGuard implements CanActivate {

  constructor(private loadingFacade: LoadingFacade, private spinnerFacade: SpinnerFacade) {
    console.log('instantiation of loading guard, logging this', this);
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return createLoadingGuard(this.loadingFacade, this.spinnerFacade);
  }
}
