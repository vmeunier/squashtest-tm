///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {EntityState, EntityAdapter, createEntityAdapter} from '@ngrx/entity';
import {BugTracker} from './bug-tracker.model';
import {BugTrackerActions, BugTrackerActionTypes} from './bug-tracker.actions';

export interface BugTrackerState extends EntityState<BugTracker> {
  // additional entities state properties
}

export const adapter: EntityAdapter<BugTracker> = createEntityAdapter<BugTracker>();

export const initialState: BugTrackerState = adapter.getInitialState({
  // additional entity state properties
});

export function reducer(
  state = initialState,
  action: BugTrackerActions
): BugTrackerState {
  switch (action.type) {
    case BugTrackerActionTypes.AddBugTracker: {
      return adapter.addOne(action.payload.bugTracker, state);
    }

    case BugTrackerActionTypes.UpsertBugTracker: {
      return adapter.upsertOne(action.payload.bugTracker, state);
    }

    case BugTrackerActionTypes.AddBugTrackers: {
      return adapter.addMany(action.payload.bugTrackers, state);
    }

    case BugTrackerActionTypes.UpsertBugTrackers: {
      return adapter.upsertMany(action.payload.bugTrackers, state);
    }

    case BugTrackerActionTypes.UpdateBugTracker: {
      return adapter.updateOne(action.payload.bugTracker, state);
    }

    case BugTrackerActionTypes.UpdateBugTrackers: {
      return adapter.updateMany(action.payload.bugTrackers, state);
    }

    case BugTrackerActionTypes.DeleteBugTracker: {
      return adapter.removeOne(action.payload.id, state);
    }

    case BugTrackerActionTypes.DeleteBugTrackers: {
      return adapter.removeMany(action.payload.ids, state);
    }

    case BugTrackerActionTypes.LoadBugTrackers: {
      return adapter.addAll(action.payload.bugTrackers, state);
    }

    case BugTrackerActionTypes.ClearBugTrackers: {
      return adapter.removeAll(state);
    }

    default: {
      return state;
    }
  }
}

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors();
