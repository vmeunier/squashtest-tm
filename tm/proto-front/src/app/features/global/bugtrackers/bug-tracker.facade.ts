///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {Injectable} from '@angular/core';
import {createFeatureSelector, createSelector, select, Store} from '@ngrx/store';
import {BugTrackerState} from './bug-tracker.reducer';
import {FEATURE_NAME} from './bug-trackers.module';
import {Observable} from 'rxjs';
import {BugTracker} from './bug-tracker.model';
import {LoadBugTrackers} from './bug-tracker.actions';
import * as reducer from './bug-tracker.reducer';

@Injectable({providedIn: 'root'})
export class BugTrackerFacade {

  // public selectors
  public bugTrackers$: Observable<BugTracker[]>;

  private readonly featureSelector;

  constructor(private store: Store<BugTrackerState>) {
    this.featureSelector = createFeatureSelector(FEATURE_NAME);
    this.bugTrackers$ = this.store.pipe(
      select(createSelector(this.featureSelector, reducer.selectAll))
    );
  }

  loadBugTrackers(bugTrackers: BugTracker[]) {
    this.store.dispatch(new LoadBugTrackers({bugTrackers}))
  }

}
