///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { BugTracker } from './bug-tracker.model';

export enum BugTrackerActionTypes {
  LoadBugTrackers = '[BugTracker] Load BugTrackers',
  AddBugTracker = '[BugTracker] Add BugTracker',
  UpsertBugTracker = '[BugTracker] Upsert BugTracker',
  AddBugTrackers = '[BugTracker] Add BugTrackers',
  UpsertBugTrackers = '[BugTracker] Upsert BugTrackers',
  UpdateBugTracker = '[BugTracker] Update BugTracker',
  UpdateBugTrackers = '[BugTracker] Update BugTrackers',
  DeleteBugTracker = '[BugTracker] Delete BugTracker',
  DeleteBugTrackers = '[BugTracker] Delete BugTrackers',
  ClearBugTrackers = '[BugTracker] Clear BugTrackers'
}

export class LoadBugTrackers implements Action {
  readonly type = BugTrackerActionTypes.LoadBugTrackers;

  constructor(public payload: { bugTrackers: BugTracker[] }) {}
}

export class AddBugTracker implements Action {
  readonly type = BugTrackerActionTypes.AddBugTracker;

  constructor(public payload: { bugTracker: BugTracker }) {}
}

export class UpsertBugTracker implements Action {
  readonly type = BugTrackerActionTypes.UpsertBugTracker;

  constructor(public payload: { bugTracker: BugTracker }) {}
}

export class AddBugTrackers implements Action {
  readonly type = BugTrackerActionTypes.AddBugTrackers;

  constructor(public payload: { bugTrackers: BugTracker[] }) {}
}

export class UpsertBugTrackers implements Action {
  readonly type = BugTrackerActionTypes.UpsertBugTrackers;

  constructor(public payload: { bugTrackers: BugTracker[] }) {}
}

export class UpdateBugTracker implements Action {
  readonly type = BugTrackerActionTypes.UpdateBugTracker;

  constructor(public payload: { bugTracker: Update<BugTracker> }) {}
}

export class UpdateBugTrackers implements Action {
  readonly type = BugTrackerActionTypes.UpdateBugTrackers;

  constructor(public payload: { bugTrackers: Update<BugTracker>[] }) {}
}

export class DeleteBugTracker implements Action {
  readonly type = BugTrackerActionTypes.DeleteBugTracker;

  constructor(public payload: { id: string }) {}
}

export class DeleteBugTrackers implements Action {
  readonly type = BugTrackerActionTypes.DeleteBugTrackers;

  constructor(public payload: { ids: string[] }) {}
}

export class ClearBugTrackers implements Action {
  readonly type = BugTrackerActionTypes.ClearBugTrackers;
}

export type BugTrackerActions =
 LoadBugTrackers
 | AddBugTracker
 | UpsertBugTracker
 | AddBugTrackers
 | UpsertBugTrackers
 | UpdateBugTracker
 | UpdateBugTrackers
 | DeleteBugTracker
 | DeleteBugTrackers
 | ClearBugTrackers;
