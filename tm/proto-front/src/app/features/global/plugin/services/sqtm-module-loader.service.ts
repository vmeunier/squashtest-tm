///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import * as AngularCore from '@angular/core';
import {Compiler, Injectable, NgModuleFactory, SystemJsNgModuleLoader, SystemJsNgModuleLoaderConfig} from '@angular/core';
import * as AngularCommon from '@angular/common';
import * as AngularRouter from '@angular/router';
import * as AngularHttp from '@angular/common/http';
import * as NgxTranslateCore from '@ngx-translate/core';
import * as NgxHttpLoader from '@ngx-translate/http-loader';
import * as NgRxStore from '@ngrx/store';
import * as TmFront from '../../../../../public_api';

declare var SystemJS;
const _SEPARATOR = '#';

// provided by app module
// see AppModule provider section for information about injection of two compiler.
@Injectable()
export class SqtmModuleLoaderService extends SystemJsNgModuleLoader {

  // Compiler of the platform.
  // In dev mode it's a fully capable JIT compiler.
  // However in AOT it's just a dummy.
  private _compilerRef: Compiler;

  // A second compiler, used only in prod mode
  // it's a fully capable JIT compiler.
  private _jitCompiler: Compiler;

  constructor(_compiler: Compiler, _jitCompiler: Compiler, config?: SystemJsNgModuleLoaderConfig) {
    super(_compiler, config);
    this._compilerRef = _compiler;
    this._jitCompiler = _jitCompiler;
    SystemJS.set('@angular/core', SystemJS.newModule(AngularCore));
    SystemJS.set('@angular/common', SystemJS.newModule(AngularCommon));
    SystemJS.set('@angular/router', SystemJS.newModule(AngularRouter));
    SystemJS.set('@angular/common/http', SystemJS.newModule(AngularHttp));
    SystemJS.set('@ngx-translate/core', SystemJS.newModule(NgxTranslateCore));
    SystemJS.set('@ngx-translate/http-loader', SystemJS.newModule(NgxHttpLoader));
    SystemJS.set('@ngrx/store', SystemJS.newModule(NgRxStore));
    SystemJS.set('tm-front-lib', SystemJS.newModule(TmFront));
  }

  load(path: string): Promise<NgModuleFactory<any>> {
    console.log('Try to load module with custom loader : ', path);
    console.log('SystemJS is initialized  : ', SystemJS);
    // All our plugin java package begin with org/squashtest/tm/plugin
    // We use the same convention here to make distinction between plugin module and standard modules.
    if (path.startsWith('org/squashtest/tm/plugin')) {
      console.log('This module seems to be a plugin, it should be loaded with plugin loader');
      const offlineMode = this._compilerRef instanceof Compiler;
      if (offlineMode) {
        console.log('We are in AOT compilation, load and compile plugin with JIT compiler.');
        return this.loadPluginAndCompile(path, this._jitCompiler);
      } else {
        console.log('We are in JIT compilation, load and compile plugin with the platform compiler.');
        return this.loadPluginAndCompile(path, this._compilerRef);
      }
    }
    // delegate to Angular SystemJsNgModuleLoaderConfig for all modules that are not plugins.
    return super.load(path);
  }

  private loadPluginAndCompile(path: string, compiler: Compiler): Promise<NgModuleFactory<any>> {
    let [moduleName, exportName] = path.split(_SEPARATOR);
    if (exportName === undefined) {
      exportName = 'default';
    }

    // we infer that all plugin will be build as umd modules.
    moduleName = moduleName + '.umd.js';

    return SystemJS.load(moduleName)
      .then((module: any) => module[exportName])
      .then((type: any) => {
        return compiler.compileModuleAsync(type).then(ngModuleFactory => {
          console.log(ngModuleFactory);
          return ngModuleFactory;
        });
      });
  }
}
