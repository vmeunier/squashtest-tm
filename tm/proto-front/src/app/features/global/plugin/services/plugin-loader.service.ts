///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import * as AngularCore from '@angular/core';
import {
  Compiler,
  ComponentFactory, ComponentFactoryResolver,
  ComponentRef,
  Inject,
  Injectable,
  Injector,
  NgModuleFactory,
  NgModuleFactoryLoader,
  NgModuleRef, Optional
} from '@angular/core';
import * as AngularCommon from '@angular/common';
import * as AngularRouter from '@angular/router';
import {Route, Router, RouterModule} from '@angular/router';
import {PluginModuleReference} from '@features/global/plugin/store/model/PluginModuleReference';
import {JIT_COMPILER} from '@common/tokens/tokens';

declare var SystemJS: any;

@Injectable({
  providedIn: 'root'
})
export class PluginLoaderService {

  constructor(@Optional() private compiler: Compiler, private _injector: Injector) {
  }

  loadPluginModule(pluginModuleReference: PluginModuleReference): Promise<ComponentRef<any>> {
    console.log(`try to load ${JSON.stringify(pluginModuleReference)}`);
    // Redefine the import for system js only. So our plugin UMD native import are not modified.
    // Our dynamically loaded modules will be linked to the modules already loaded by webpack in squash tm core by aliasing
    // All the module from squash tm core that must be loaded by plugins must be declared and aliased here
    // It's like a list of provided modules...
    SystemJS.set('@angular/core', SystemJS.newModule(AngularCore));
    SystemJS.set('@angular/common', SystemJS.newModule(AngularCommon));
    SystemJS.set('@angular/router', SystemJS.newModule(AngularRouter));

    // Now we load the plugin and then we compile it and give back a Promise<ComponentRef<any>>
    // The ComponentRef is a complete component ready to be inserted dynamically in another component
    //   by using a ViewContainerRef inside the component typescript file.
    return SystemJS.load(pluginModuleReference.modulePath)
    // .then( module => module[pluginModuleReference.moduleId])
      .then(module => this.compilePluginModule(module, pluginModuleReference));
  }

  compilePluginModule(ngModule, pluginModuleReference: PluginModuleReference): Promise<ComponentRef<any>> {
    console.log(ngModule);
    return this.compiler.compileModuleAndAllComponentsAsync(ngModule[pluginModuleReference.moduleId]).then(compiled => {
      console.log(compiled);
      const moduleReference: NgModuleRef<any> = compiled.ngModuleFactory.create(this._injector);
      const factory: ComponentFactory<any> = compiled.componentFactories[0];
      const cmp: ComponentRef<any> = factory.create(this._injector, [], null, moduleReference); // component ref
      console.log(cmp);
      return cmp;
    });
  }
}
