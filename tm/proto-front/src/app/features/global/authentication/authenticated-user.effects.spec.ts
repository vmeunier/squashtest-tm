///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {TestBed, inject} from '@angular/core/testing';
import {provideMockActions} from '@ngrx/effects/testing';
import {Observable} from 'rxjs';

import {AuthenticatedUserEffects} from './authenticated-user.effects';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {BACKEND_ROOT_URL} from '@common/tokens/tokens';

describe('AuthenticatedUserEffects', () => {
  let actions$: Observable<any>;
  let effects: AuthenticatedUserEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        AuthenticatedUserEffects,
        provideMockActions(() => actions$),
        {
          provide : BACKEND_ROOT_URL,
          useValue: 'backend'
        }
      ]
    });

    effects = TestBed.get(AuthenticatedUserEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
