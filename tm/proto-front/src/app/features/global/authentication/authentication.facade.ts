///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {Injectable} from '@angular/core';
import {createFeatureSelector, createSelector, select, Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {AuthenticatedUser} from './authenticated-user.model';
import {AuthenticationState} from './index';
import {AUTH_FEATURE_NAME} from './authentication.module';
import {selectAdminPageAccess, selectAuthenticatedUser} from '@features/global/authentication/authenticated-user.selectors';
import {LoadAuthenticatedUser, Login} from '@features/global/authentication/authenticated-user.actions';

@Injectable({providedIn: 'root'})
export class AuthenticationFacade {

  // public selectors
  public authenticatedUser$: Observable<AuthenticatedUser>;
  public shouldAccessToAdmin$: Observable<boolean>;

  private readonly featureSelector;

  constructor(private store: Store<AuthenticationState>) {
    this.featureSelector = createFeatureSelector(AUTH_FEATURE_NAME);
    this.authenticatedUser$ = this.store.pipe(
      select(createSelector(this.featureSelector, selectAuthenticatedUser))
    );
    this.shouldAccessToAdmin$ = this.store.pipe(
      select(createSelector(this.featureSelector, selectAdminPageAccess))
    )
  }

  login(username, password){
    const payload = {username, password};
    this.store.dispatch(new Login(payload));
  }

}
