///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import * as fromRootAction from '@root/store/actions';
import {map, switchMap} from 'rxjs/internal/operators';
import {
  AuthenticatedUserActionTypes,
  Login,
  LoginFailure,
  LoginSuccess
} from '@features/global/authentication/authenticated-user.actions';
import {LoginService} from './login.service';


@Injectable()
export class AuthenticatedUserEffects {

  constructor(private actions$: Actions, private loginService : LoginService) {}

  @Effect()
  login$ = this.actions$.pipe(
    ofType(AuthenticatedUserActionTypes.Login),
    switchMap((action: Login) => {
      return this.loginService.login(action.payload.username, action.payload.password).pipe(
        map(value => {
          if (value.authenticated) {
            return new LoginSuccess();
          }
          return new LoginFailure();
        })
      );
    })
  );

  @Effect()
  loginSuccess$ = this.actions$.pipe(
    ofType(AuthenticatedUserActionTypes.LoginSuccess),
    map(action => new fromRootAction.NavigateTo({path: ['/home-workspace']}))
  );
}
