///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {Action} from '@ngrx/store';
import {AuthenticatedUser} from '@features/global/authentication/authenticated-user.model';

export enum AuthenticatedUserActionTypes {
  LoadAuthenticatedUser = '[AuthenticatedUser] Load AuthenticatedUser',
  Login = '[AuthenticatedUser] Login',
  LoginSuccess = '[AuthenticatedUser] Login Success',
  LoginFailure = '[AuthenticatedUser] Login Failure',
}

export class LoadAuthenticatedUser implements Action {
  readonly type = AuthenticatedUserActionTypes.LoadAuthenticatedUser;

  constructor(public payload: { user: AuthenticatedUser }) {
  }
}

export class Login implements Action {
  readonly type = AuthenticatedUserActionTypes.Login;

  constructor(public payload: { username: string, password: string }) {
  }
}

export class LoginSuccess implements Action {
  readonly type = AuthenticatedUserActionTypes.LoginSuccess;
}

export class LoginFailure implements Action {
  readonly type = AuthenticatedUserActionTypes.LoginFailure;
}

export type AuthenticatedUserActions = LoadAuthenticatedUser | Login | LoginSuccess | LoginFailure;
