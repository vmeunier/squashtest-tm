///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {Theme} from '@features/global/theme/store/theme.model';

export const Themes: Theme[] = [
  {
    'id': 'default',
    'global-theme' : {
      'nav-main-color': '#191D35',
      'nav-height': '40px',
      'nav-dropdown-hover-color': '#bdc8e6',
      'nav-dropdown-background-color': '#f2f2f2'
    },
    'home-workspace' : {
      'main-color': '#c8c2c5'
    },
    'test-case-workspace' : {
      'main-color': '#6EAC2C'
    },
    'requirement-workspace' : {
      'main-color': '#0078AE'
    },
    'campaign-workspace' : {
      'main-color': '#5E287B'
    },
    'custom-report-workspace' : {
      'main-color': '#750021'
    }
  },
  {
    'id': 'halloween',
    'global-theme' : {
      'nav-main-color': '#191D35',
      'nav-height': '30px',
      'nav-dropdown-hover-color': '#bdc8e6',
      'nav-dropdown-background-color': '#f2f2f2'
    },
    'test-case-workspace' : {
      'main-color': '#8b79ac'
    },
    'requirement-workspace' : {
      'main-color': '#ff8400'
    },
    'campaign-workspace' : {
      'main-color': '#002f7b'
    },
    'custom-report-workspace' : {
      'main-color': '#1f1f21'
    },
    'home-workspace' : {
      'main-color': '#c8c2c5'
    },
  }
];
