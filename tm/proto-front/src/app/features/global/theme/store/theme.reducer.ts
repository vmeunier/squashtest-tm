///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {createEntityAdapter, EntityAdapter, EntityState} from '@ngrx/entity';
import {Theme} from './theme.model';
import {ThemeActions, ThemeActionTypes} from './theme.actions';
import {Themes} from '@features/global/theme/themes';

export interface State extends EntityState<Theme> {
  selectedTheme: string;
}

export const adapter: EntityAdapter<Theme> = createEntityAdapter<Theme>();

export function getInitialState(): State {
  const state = adapter.getInitialState({
    selectedTheme: 'default'
  });

  return adapter.addAll(Themes, state);
}

export function reducer(
  state = getInitialState(),
  action: ThemeActions
): State {
  switch (action.type) {
    case ThemeActionTypes.AddTheme: {
      return adapter.addOne(action.payload.theme, state);
    }

    case ThemeActionTypes.UpsertTheme: {
      return adapter.upsertOne(action.payload.theme, state);
    }

    case ThemeActionTypes.AddThemes: {
      return adapter.addMany(action.payload.themes, state);
    }

    case ThemeActionTypes.UpsertThemes: {
      return adapter.upsertMany(action.payload.themes, state);
    }

    case ThemeActionTypes.UpdateTheme: {
      return adapter.updateOne(action.payload.theme, state);
    }

    case ThemeActionTypes.UpdateThemes: {
      return adapter.updateMany(action.payload.themes, state);
    }

    case ThemeActionTypes.DeleteTheme: {
      return adapter.removeOne(action.payload.id, state);
    }

    case ThemeActionTypes.DeleteThemes: {
      return adapter.removeMany(action.payload.ids, state);
    }

    case ThemeActionTypes.LoadThemes: {
      return adapter.addAll(action.payload.themes, state);
    }

    case ThemeActionTypes.ClearThemes: {
      return adapter.removeAll(state);
    }

    case ThemeActionTypes.SelectTheme: {
      return {...state, selectedTheme: action.payload.id};
    }

    default: {
      return state;
    }
  }
}

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors();
