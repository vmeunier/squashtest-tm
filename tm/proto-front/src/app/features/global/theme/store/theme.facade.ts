///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {Injectable} from '@angular/core';
import {createFeatureSelector, createSelector, select, Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {Theme} from '@features/global/theme/store/theme.model';
import {FEATURE_NAME} from '@features/global/theme/theme.module';
import {selectedTheme} from '@features/global/theme/store/theme.selector';
import * as fromReducers from '@features/global/theme/store/theme.reducer';
import {SelectTheme} from '@features/global/theme/store/theme.actions';

@Injectable({
  providedIn: 'root'
})
export class ThemeFacade {

  // public selectors
  public selectedTheme$: Observable<Theme>;
  public themes$: Observable<Theme[]>;

  // private selectors
  private readonly featureSelector;

  constructor(private store: Store<any>) {
    this.featureSelector = createFeatureSelector(FEATURE_NAME);
    this.selectedTheme$ = this.store.pipe(
      select(createSelector(this.featureSelector, selectedTheme))
    );
    this.themes$ = this.store.pipe(
      select(createSelector(this.featureSelector, fromReducers.selectAll))
    );
  }

  changeTheme(id: string) {
    this.store.dispatch(new SelectTheme({id}));
  }

}
