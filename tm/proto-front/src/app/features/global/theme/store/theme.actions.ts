///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {Action} from '@ngrx/store';
import {Update} from '@ngrx/entity';
import {Theme} from './theme.model';
import {Themes} from '@features/global/theme/themes';

export enum ThemeActionTypes {
  SelectTheme = '[Theme] Select Themes',
  LoadThemes = '[Theme] Load Themes',
  AddTheme = '[Theme] Add Theme',
  UpsertTheme = '[Theme] Upsert Theme',
  AddThemes = '[Theme] Add Themes',
  UpsertThemes = '[Theme] Upsert Themes',
  UpdateTheme = '[Theme] Update Theme',
  UpdateThemes = '[Theme] Update Themes',
  DeleteTheme = '[Theme] Delete Theme',
  DeleteThemes = '[Theme] Delete Themes',
  ClearThemes = '[Theme] Clear Themes'
}

export class LoadThemes implements Action {
  readonly type = ThemeActionTypes.LoadThemes;

  constructor(public payload: { themes: Theme[] }) {
  }
}

export class AddTheme implements Action {
  readonly type = ThemeActionTypes.AddTheme;

  constructor(public payload: { theme: Theme }) {
  }
}

export class UpsertTheme implements Action {
  readonly type = ThemeActionTypes.UpsertTheme;

  constructor(public payload: { theme: Theme }) {
  }
}

export class AddThemes implements Action {
  readonly type = ThemeActionTypes.AddThemes;

  constructor(public payload: { themes: Theme[] }) {
  }
}

export class UpsertThemes implements Action {
  readonly type = ThemeActionTypes.UpsertThemes;

  constructor(public payload: { themes: Theme[] }) {
  }
}

export class UpdateTheme implements Action {
  readonly type = ThemeActionTypes.UpdateTheme;

  constructor(public payload: { theme: Update<Theme> }) {
  }
}

export class UpdateThemes implements Action {
  readonly type = ThemeActionTypes.UpdateThemes;

  constructor(public payload: { themes: Update<Theme>[] }) {
  }
}

export class DeleteTheme implements Action {
  readonly type = ThemeActionTypes.DeleteTheme;

  constructor(public payload: { id: string }) {
  }
}

export class DeleteThemes implements Action {
  readonly type = ThemeActionTypes.DeleteThemes;

  constructor(public payload: { ids: string[] }) {
  }
}

export class ClearThemes implements Action {
  readonly type = ThemeActionTypes.ClearThemes;
}

export class SelectTheme implements Action {
  readonly type = ThemeActionTypes.SelectTheme;

  constructor(public payload: { id: string }) {
  }
}

export type ThemeActions =
  LoadThemes
  | AddTheme
  | UpsertTheme
  | AddThemes
  | UpsertThemes
  | UpdateTheme
  | UpdateThemes
  | DeleteTheme
  | DeleteThemes
  | ClearThemes
  | SelectTheme;
