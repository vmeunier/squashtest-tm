///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {Compiler, InjectionToken} from '@angular/core';

export const BACKEND_ROOT_URL = new InjectionToken<string>('prefix for all requests to backend');
export const PAGE_URL = new InjectionToken<string>(`page url for the router, the feature module, the facades...
Should be unique across all app, it WILL be used as key of the ngrx feature module`);

export const JIT_COMPILER = new InjectionToken<Compiler>('An injection token for providing a JIT compiler in AOT mode');
