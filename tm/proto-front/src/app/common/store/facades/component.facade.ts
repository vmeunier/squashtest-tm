///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {PageFacade} from './page.facade';
import {createSelector, Selector, Store} from '@ngrx/store';
import camelcase from 'camelcase';

export abstract class ComponentFacade extends PageFacade {

  protected componentId: string;

  protected componentStateSelector: Selector<any, any>;

  constructor(pageUrl: string, componentId: string, store: Store<any>) {
    super(pageUrl, store);
    this.componentId = camelcase(componentId);
    this.componentStateSelector = createSelector(this.featureSelector, (state: any) => {
      if (!!state) {
        return state[this.componentId];
      }
      return this.initialState;
    });
  }
}
