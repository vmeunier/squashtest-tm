///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {ComponentAction, PageAction} from '@common/store/factories/actions/actions-type';
import camelcase from 'camelcase';

/**
 * Helper function used to create a reducer prefixed by a page id. So actions will be filtered by page id, allowing to reuse the same
 * reducers across all application, without any clash.
 * @param {string} rootUrl an unique identifier for the page.
 * @param {(state: S, action: A) => S} reducer the non prefixed reducer.
 * @param {S} initialState initial state for the reducer.
 * @returns {(state: S, action: A) => S} a reducer that accept action prefixed by @param {string} pagePrefix.
 */
export function createPageReducer<S, A extends PageAction>(rootUrl: string, reducer: (state: S, action: A) => S, initialState: S) {
  const pagePrefix = camelcase(rootUrl);
  return (state: S = initialState, action: A) => {
    switch (action.pagePrefix) {
      case pagePrefix :
        return reducer(state, action);
      default :
        return state;
    }
  };
}

export function createComponentReducer<S, A extends ComponentAction>(rootUrl: string, componentId: string, reducer: (state: S, action: A) => S, initialState: S) {
  const pagePrefix = camelcase(rootUrl);
  const fullPrefix = pagePrefix + componentId;

  return (state: S = initialState, action: A) => {
    switch (action.pagePrefix + action.componentId) {
      case fullPrefix :
        return reducer(state, action);
      default :
        return state;
    }
  };
}
