///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {Inject, Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {BACKEND_ROOT_URL} from '../tokens/tokens';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private http: HttpClient, @Inject(BACKEND_ROOT_URL) private backendRootUrl: string) {

  }

  get<T>(parts: string[], params?: { [param: string]: any }): Observable<T> {
    const options = {};
    if (params) {
      let headers = new HttpHeaders();
      headers = headers.set('Content-Type', 'application/x-www-form-urlencoded');
      const paramKeys = Object.keys(params);
      let httpParams = new HttpParams();
      console.log(paramKeys);
      paramKeys.forEach((paramKey) => {
        const httpParamValue = params[paramKey];
        console.log('param value ', httpParamValue);
        if (typeof httpParamValue === 'string') {
          httpParams = httpParams.append(paramKey, httpParamValue);
        } else {
          console.log('append param ', httpParamValue);
          httpParams = httpParams.append(paramKey, httpParamValue.toString());
        }
      });
      options['params'] = httpParams;
      options['headers'] = headers;
    }

    const url = '/' + this.backendRootUrl + '/' + parts.join('/');
    console.log(url, options);
    return this.http.get<T>(url, options);
  }
}
