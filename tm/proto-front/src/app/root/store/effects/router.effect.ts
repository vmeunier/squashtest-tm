///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Location} from '@angular/common';

import {Actions, Effect, ofType} from '@ngrx/effects';

import * as routerActions from '../actions/router.actions';
import {map, tap} from 'rxjs/operators';


@Injectable()
export class RouterEffect {

  constructor(private actions$: Actions,
              private router: Router,
              private location: Location
  ) {
  }

  @Effect({dispatch: false})
  navigate$ = this.actions$
    .pipe(
      ofType(routerActions.NAVIGATE_TO),
      map((action: routerActions.NavigateTo) => action.payload)
      , tap(({path, query : queryParams, extras}) => {
        this.router.navigate(path, {queryParams, ...extras});
      })
    );

  navigateBack$ = this.actions$
    .pipe(
      ofType(routerActions.NAVIGATE_BACK),
      tap(x => this.location.back())
    );

  navigateForward$ = this.actions$
    .pipe(
      ofType(routerActions.NAVIGATE_FORWARD),
      tap(x => this.location.forward())
    );
}
