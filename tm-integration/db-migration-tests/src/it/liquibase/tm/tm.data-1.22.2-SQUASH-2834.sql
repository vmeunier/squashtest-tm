INSERT INTO THIRD_PARTY_SERVER (SERVER_ID, NAME, URL, AUTH_POLICY, AUTH_PROTOCOL) VALUES
(-425, 'Github-MyUser', 'https://github.com/myuser', 'APP_LEVEL', 'BASIC_AUTH');

INSERT INTO SCM_SERVER (SERVER_ID, KIND) VALUES
(-425, 'git');

INSERT INTO SCM_REPOSITORY (SCM_REPOSITORY_ID, SERVER_ID, NAME, REPOSITORY_PATH, WORKING_BRANCH) VALUES
(-677, -425, 'my_repository', 'home/test/repo1', 'main'),
(-678, -425, 'my_repository', 'home/test/repo2', 'develop');
