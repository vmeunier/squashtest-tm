/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display


import org.squashtest.it.basespecs.DbunitDaoSpecification
import org.squashtest.tm.service.internal.display.dto.AttachmentDto
import org.squashtest.tm.service.internal.repository.display.AttachmentDisplayDao
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject
import java.text.SimpleDateFormat

@UnitilsSupport
@DataSet
class AttachmentDisplayDaoIT extends DbunitDaoSpecification {

	@Inject
	AttachmentDisplayDao attachmentDisplayDao;

	def "findAttachmentListById(long): [Nominal] Should retrieve the requested AttachmentList with some Attachments"() {
		given:
		long requestedId = -8

		def attachmentOne = new AttachmentDto(id: -1, name: "attachment_1", size: 5206)
		def attachmentTwo = new AttachmentDto(id: -2, name: "attachment_2", size: 1244)
		def attachmentThree = new AttachmentDto(id: -3, name: "attachment_3", size: 18977)

		def expectedAttachments = [attachmentOne, attachmentTwo, attachmentThree]

		when:
		def resultAttachmentList = attachmentDisplayDao.findAttachmentListById(requestedId)

		then:
		resultAttachmentList != null
		resultAttachmentList.getId() == requestedId
		resultAttachmentList.getAttachments() != null
		resultAttachmentList.getAttachments().size() == expectedAttachments.size()
		def attachmentsById = resultAttachmentList.getAttachments().sort { it.id }

		attachmentsById.get(0).id == -3L
		attachmentsById.get(0).name == "attachment_3"
		attachmentsById.get(0).size == 18977
		attachmentsById.get(0).addedOn == new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse("2019-06-02 10:37:57")

		attachmentsById.get(1).id == -2L
		attachmentsById.get(1).name == "attachment_2"
		attachmentsById.get(1).size == 1244
		attachmentsById.get(1).addedOn == new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse("2019-06-02 10:02:00")

		attachmentsById.get(2).id == -1L
		attachmentsById.get(2).name == "attachment_1"
		attachmentsById.get(2).size == 5206
		attachmentsById.get(2).addedOn == new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse("2019-06-01 16:51:02")
	}

	def "findAttachmentListById(long): [Empty] Should retrieve the requested AttachmentList with no Attachments"() {
		given:
		long requestedId = -5

		when:
		def resultAttachmentList = attachmentDisplayDao.findAttachmentListById(requestedId)

		then:
		resultAttachmentList != null
		resultAttachmentList.getId() == requestedId
		resultAttachmentList.getAttachments() != null
		resultAttachmentList.getAttachments().size() == 0
	}

	def "findAttachmentListById(long): [Null] Should return throw because the AttachmentList does not exist"() {
		given:
		long nonExistentId = -404

		when:
		def resultAttachmentList = attachmentDisplayDao.findAttachmentListById(nonExistentId)

		then:
		thrown IllegalArgumentException
	}

	def "findAttachmentListByIds : Should find several attachment lists"() {
		given:
		Set<Long> ids = [-5L, -8L, -9L]

		when:
		def attachmentLists = attachmentDisplayDao.findAttachmentListByIds(ids).sort {it.id}

		then:
		attachmentLists.size() == 3

		def attachmentList9 = attachmentLists.get(0)
		attachmentList9.id == -9L
		def attachments = attachmentList9.attachments.sort{it.id}
		attachments.size() == 2
		attachments.get(0).id == -5L
		attachments.get(0).name == "attachment_5"
		attachments.get(0).size == 632

	}

}
