/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display

import org.spockframework.util.NotThreadSafe
import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.NodeReference
import org.squashtest.tm.domain.NodeReferences
import org.unitils.dbunit.annotation.DataSet
import spock.lang.Unroll
import spock.unitils.UnitilsSupport

import javax.inject.Inject

import static org.squashtest.tm.domain.NodeType.*

@UnitilsSupport
@Transactional
@NotThreadSafe
@DataSet("TreeBrowserDaoIT.xml")
class SingleHierarchyTreeBrowserDaoIT extends DbunitServiceSpecification {

	@Inject
	SingleHierarchyTreeBrowserDao treeBrowser

	def "should find children reference of a custom report library"() {
		when:
		def ids = new HashSet<NodeReference>()
		def reference = new NodeReference(CUSTOM_REPORT_LIBRARY, -1L)
		ids.add(reference)

		def children = treeBrowser.findChildrenReference(ids)

		then:
		children.size() == 3
		children.get(reference).get(0).id == -4L
		children.get(reference).get(0).nodeType == CUSTOM_REPORT_FOLDER
		children.get(reference).get(1).id == -5L
		children.get(reference).get(1).nodeType == CUSTOM_REPORT_FOLDER
		children.get(reference).get(2).id == -6L
		children.get(reference).get(2).nodeType == CHART_DEFINITION
	}

	def "should find children reference of a action word library"() {
		when:
		def ids = new HashSet<NodeReference>()
		def reference = new NodeReference(ACTION_WORD_LIBRARY, -1L)
		ids.add(reference)

		def children = treeBrowser.findChildrenReference(ids)

		then:
		children.size() == 2
		children.get(reference).get(0).id == -4L
		children.get(reference).get(0).nodeType == ACTION_WORD
		children.get(reference).get(1).id == -6L
		children.get(reference).get(1).nodeType == ACTION_WORD
	}

	@Unroll("#descendantIds should have ancestors : #expectedAncestors")
	def "should find ancestor reference of various workspace nodes"() {
		when:
		def ancestors = treeBrowser.findAncestors(new NodeReferences(descendantIds))

		then:
		ancestors == expectedAncestors as Set

		where:
		descendantIds                                   | expectedAncestors
		[new NodeReference(CUSTOM_REPORT_LIBRARY, -1L)] | []
		[new NodeReference(CHART_DEFINITION, -8L)]      | [new NodeReference(CUSTOM_REPORT_FOLDER, -5L),
														   new NodeReference(CUSTOM_REPORT_LIBRARY, -1L)]
		[new NodeReference(ACTION_WORD_LIBRARY, -1L)]   | []
		[new NodeReference(ACTION_WORD, -4L)]           | [new NodeReference(ACTION_WORD_LIBRARY, -1L)]
	}
}
