/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display

import com.google.common.collect.ListMultimap
import org.squashtest.it.basespecs.DbunitDaoSpecification
import org.squashtest.tm.domain.denormalizedfield.DenormalizedFieldHolderType
import org.squashtest.tm.service.internal.display.dto.DenormalizedCustomFieldValueDto
import org.unitils.dbunit.annotation.DataSet
import spock.lang.Unroll
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
class DenormalizedCustomFieldValueDisplayDaoIT extends DbunitDaoSpecification {

	@Inject
	DenormalizedCustomFieldValueDisplayDao dao;

	@DataSet
	@Unroll
	def "Should find denormalized custom field values"() {
		given:

		when:
		ListMultimap<Long, DenormalizedCustomFieldValueDto> customFieldValues = dao.findDenormalizedCustomFieldValues(entityType, ids)

		then:
		for (id in ids) {
			def customFieldValuesForEntity = customFieldValues.get(id).sort { it.id }
			def expectedValuesForEntity = expectedValues.get(id).sort { it.id }
			customFieldValuesForEntity.size() == expectedValuesForEntity.size()
			for (int i = 0; i < expectedValuesForEntity.size(); i++) {
				def customFieldValueDto = customFieldValuesForEntity.get(i)
				def expectedValue = expectedValuesForEntity.get(i)
				assert customFieldValueDto.getId() == expectedValue.id
				assert customFieldValueDto.getDenormalizedFieldHolderId() == expectedValue.denormalizedFieldHolderId
				assert customFieldValueDto.getValue() == expectedValue.value
				assert customFieldValueDto.getFieldType() == expectedValue.fieldType
				assert customFieldValueDto.getLabel() == expectedValue.label
				assert customFieldValueDto.getInputType() == expectedValue.inputType
			}
		}

		where:
		entityType                            | ids             || expectedValues
		DenormalizedFieldHolderType.EXECUTION | null            || []
		DenormalizedFieldHolderType.EXECUTION | []              || []
		DenormalizedFieldHolderType.EXECUTION | [-113L]         || [(-113L): []]
		DenormalizedFieldHolderType.EXECUTION | [-1L]           || [(-1L): [["id": -1L, "label": "CUF_1", inputType: "PLAIN_TEXT", "denormalizedFieldHolderId": -1L, "value": "value_1", "fieldType": "CF"], ["id": -2L, "label": "CUF_2", inputType: "DROPDOWN_LIST", "denormalizedFieldHolderId": -1L, "value": "OPTION_1", "fieldType": "SSF"]]]
		DenormalizedFieldHolderType.EXECUTION | [-1L, -2L, -3L] || [
			(-1L): [["id": -1L, "label": "CUF_1", "denormalizedFieldHolderId": -1L, "value": "value_1", "fieldType": "CF", inputType: "PLAIN_TEXT"], ["id": -2L, "label": "CUF_2", "denormalizedFieldHolderId": -1L, inputType: "DROPDOWN_LIST", "value": "OPTION_1", "fieldType": "SSF"]],
			(-2L): [["id": -12L, "denormalizedFieldHolderId": -2L, label: "CUF_3", "value": "false", inputType: "PLAIN_TEXT", "fieldType": "CF"], ["id": -11L, "denormalizedFieldHolderId": -2L, label: "CUF_1", inputType: "PLAIN_TEXT", "value": "value_2", "fieldType": "CF"]],
			(-3L): [["id": -13L, "denormalizedFieldHolderId": -3L, inputType: "TAG", label: "CUF_TAG", "value": "tag0|toto|ahaha|value0", "fieldType": "MFV",], ["id": -14L, "denormalizedFieldHolderId": -3L, inputType: "PLAIN_TEXT", label: "CUF_1", "value": "hello", "fieldType": "CF"]],
		]
	}
}
