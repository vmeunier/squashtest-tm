/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.infolist

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.service.internal.display.grid.GridRequest
import org.squashtest.tm.service.internal.display.grid.GridSort
import org.unitils.dbunit.annotation.DataSet
import spock.lang.Unroll
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@Transactional
@DataSet
class InfoListDisplayServiceIT extends DbunitServiceSpecification {

	@Inject
	private InfoListDisplayService infoListDisplayService

	def "should fetch a custom fields grid"() {
		given:
		def gridRequest = new GridRequest()
		gridRequest.size = 25

		when:
		def gridResponse = this.infoListDisplayService.findAll(gridRequest)

		then:
		gridResponse.count == 3
		def rows = gridResponse.dataRows

		// Rows are sorted by label ASC

		def first = rows.get(0)
		first.id == "-1"
		first.data.get("infoListId") == -1
		first.data.get("label") == "label1"
		first.data.get("description") == "descr1"
		first.data.get("code") == "c1"
		first.data.get("defaultValue") == "default"
		first.data.get("projectCount") == 1

		def second = rows.get(1)
		second.id == "-2"
		second.data.get("defaultValue") == "itemC"
		second.data.get("projectCount") == 3

		def third = rows.get(2)
		third.id == "-3"
		third.data.get("defaultValue") == "itemD"
		third.data.get("projectCount") == 0
	}

	@Unroll("should allow sorting on #column")
	def "should allow sorting"() {
		when:
		def gridRequest = new GridRequest()
		gridRequest.sort.add(new GridSort(column, GridSort.SortDirection.ASC))
		gridRequest.setSize(25)
		this.infoListDisplayService.findAll(gridRequest)

		then:
		noExceptionThrown()

		where:
		column << ["label",
				   "description",
				   "code",
				   "defaultValue"]
	}
}
