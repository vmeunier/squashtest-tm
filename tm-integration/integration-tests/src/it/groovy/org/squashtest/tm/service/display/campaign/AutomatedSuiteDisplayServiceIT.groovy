/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.campaign

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.service.internal.display.grid.GridRequest
import org.squashtest.tm.service.internal.display.grid.GridSort
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@DataSet
@UnitilsSupport
@Transactional
class AutomatedSuiteDisplayServiceIT extends DbunitServiceSpecification {

	@Inject
	AutomatedSuiteDisplayService service

	def "should fetch automated suite by iteration id"() {
		given:
		def gridRequest = new GridRequest()
		gridRequest.page = 0
		gridRequest.size = 25
		gridRequest.sort.add(new GridSort("createdOn", GridSort.SortDirection.DESC))

		when:
		def gridResponse = service.findAutomatedSuitesByIterationId(-11L, gridRequest)

		then:
		gridResponse.count == 2
		def firstAutomatedSuite = gridResponse.dataRows.get(0)
		firstAutomatedSuite.data.get('createdBy') == "User-1"
		firstAutomatedSuite.data.get('suiteId') == "456"
		firstAutomatedSuite.data.get('hasExecution') == true
		firstAutomatedSuite.data.get('hasResultUrl') == false
		firstAutomatedSuite.data.get('executionExtenderHasAttachment') == false
		firstAutomatedSuite.data.get('automatedSuiteHasAttachment') == false
		def secondAutomatedSuite = gridResponse.dataRows.get(1)
		secondAutomatedSuite.data.get('createdBy') == "User-1"
		secondAutomatedSuite.data.get('suiteId') == "123"
		secondAutomatedSuite.data.get('hasExecution') == true
		secondAutomatedSuite.data.get('hasResultUrl') == false
		secondAutomatedSuite.data.get('executionExtenderHasAttachment') == false
		secondAutomatedSuite.data.get('automatedSuiteHasAttachment') == false
	}

	def "should fetch automated suite by suite id"() {
		given:
		def gridRequest = new GridRequest()
		gridRequest.page = 0
		gridRequest.size = 25
		gridRequest.sort.add(new GridSort("createdOn", GridSort.SortDirection.DESC))

		when:
		def gridResponse = service.findAutomatedSuitesBySuiteId(-21L, gridRequest)

		then:
		gridResponse.count == 1
		def firstAutomatedSuite = gridResponse.dataRows.get(0)
		firstAutomatedSuite.data.get('createdBy') == "User-1"
		firstAutomatedSuite.data.get('suiteId') == "123"
		firstAutomatedSuite.data.get('hasExecution') == true
		firstAutomatedSuite.data.get('hasResultUrl') == false
		firstAutomatedSuite.data.get('executionExtenderHasAttachment') == false
		firstAutomatedSuite.data.get('automatedSuiteHasAttachment') == false
	}

	def "should fetch execution by automated suite id"() {
		given:
		def gridRequest = new GridRequest()
		gridRequest.page = 0
		gridRequest.size = 25
		gridRequest.sort.add(new GridSort("lastExecutedOn", GridSort.SortDirection.DESC))

		when:
		def gridResponse = service.findExecutionByAutomatedSuiteID("123", gridRequest)

		then:
		gridResponse.count == 3
		def firstExecution = gridResponse.dataRows.get(0)
		firstExecution.data.get('name') == "ITPI202"
		def secondExecution = gridResponse.dataRows.get(1)
		secondExecution.data.get('name') == "ITPI203"
		def thirdExecution = gridResponse.dataRows.get(2)
		thirdExecution.data.get('name') == "ITPI201"
	}
}
