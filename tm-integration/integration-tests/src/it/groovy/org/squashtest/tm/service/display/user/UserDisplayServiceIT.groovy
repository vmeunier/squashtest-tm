/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.user

import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.context.SecurityContextImpl
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.ContextHierarchy
import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.it.config.EnabledAclSpecConfig
import org.squashtest.it.stub.security.StubAuthentication
import org.squashtest.tm.domain.NamedReference
import org.squashtest.tm.service.internal.display.dto.UserAdminViewTeamDto
import org.squashtest.tm.service.internal.display.grid.GridRequest
import org.squashtest.tm.service.internal.display.grid.GridSort
import org.unitils.dbunit.annotation.DataSet
import spock.lang.Unroll
import spock.unitils.UnitilsSupport

import javax.inject.Inject
import java.text.SimpleDateFormat

@UnitilsSupport
@Transactional
@DataSet
@ContextHierarchy([
	// enabling the ACL management that was disabled in DbunitServiceSpecification
	@ContextConfiguration(name = "aclcontext", classes = [EnabledAclSpecConfig], inheritLocations = false)
])
class UserDisplayServiceIT extends DbunitServiceSpecification {

	@Inject
	private UserDisplayService userDisplayService


	def "should find current detailed user"() {
		given:
		SecurityContextHolder.setContext(new SecurityContextImpl(new StubAuthentication("login0")))

		when:
		def detailedUserDto = userDisplayService.findCurrentUser()

		then:
		detailedUserDto.userId == -10L
		detailedUserDto.username == "login0"
		detailedUserDto.firstName == "Mister"
		detailedUserDto.lastName == "Pink"
		detailedUserDto.getPartyIds().sort() == [-102L, -101L, -10L]

	}

	def "should fetch users"() {
		given:
		def gridRequest = new GridRequest()
		gridRequest.size = 25
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd")

		when:
		def gridResponse = this.userDisplayService.findAll(gridRequest)

		then:
		gridResponse.count == 6
		def rows = gridResponse.dataRows
		rows.get(0).data.userGroup == "squashtest.authz.group.tm.User"
		rows.get(0).data.createdBy == "dbu"
		rows.get(0).data.login == "login0"
		rows.get(0).data.firstName == "Mister"
		rows.get(0).data.lastName == "Pink"
		rows.get(0).data.createdBy == "dbu"
		rows.get(0).data.email == "mpink@rdogs.com"
		dateFormat.parse((String) rows.get(0).data.createdOn) == dateFormat.parse("2020-09-15")
		rows.get(0).data.teamCount == 2
		rows.get(0).data.habilitationCount == 3

		rows.get(1).data.userGroup == "squashtest.authz.group.core.Admin"
		rows.get(1).data.createdBy == "dbu"
		rows.get(1).data.login == "login1"
		rows.get(1).data.firstName == "Mister"
		rows.get(1).data.lastName == "Yellow"
		rows.get(1).data.createdBy == "dbu"
		rows.get(1).data.email == "myellow@rdogs.com"
		dateFormat.parse((String) rows.get(1).data.createdOn) == dateFormat.parse("2020-09-15")
		rows.get(1).data.teamCount == 1
		rows.get(1).data.habilitationCount == 1

		rows.get(2).data.userGroup == "squashtest.authz.group.tm.User"
		rows.get(2).data.createdBy == "dbu"
		rows.get(2).data.login == "login2"
		rows.get(2).data.firstName == "Mister"
		rows.get(2).data.lastName == "Black"
		rows.get(2).data.createdBy == "dbu"
		rows.get(2).data.email == ""
		dateFormat.parse((String) rows.get(2).data.createdOn) == dateFormat.parse("2020-09-15")
		rows.get(2).data.teamCount == 0
		rows.get(2).data.habilitationCount == 0

		rows.get(3).data.userGroup == "squashtest.authz.group.tm.User"
		rows.get(3).data.login == "login3"

		rows.get(4).data.userGroup == "squashtest.authz.group.tm.TestAutomationServer"
		rows.get(4).data.login == "login4"
	}

	def "should fetch users sorted by group"() {
		given:
		def gridRequest = new GridRequest()
		gridRequest.sort.add(new GridSort("userGroup", GridSort.SortDirection.ASC))
		gridRequest.size = 25

		when:
		def gridResponse = this.userDisplayService.findAll(gridRequest)

		then:
		gridResponse.count == 6
		def rows = gridResponse.dataRows
		rows.get(0).data.userGroup == "squashtest.authz.group.core.Admin"
		rows.get(1).data.userGroup == "squashtest.authz.group.core.Admin"
		rows.get(2).data.userGroup == "squashtest.authz.group.tm.TestAutomationServer"
		rows.get(3).data.userGroup == "squashtest.authz.group.tm.User"
		rows.get(4).data.userGroup == "squashtest.authz.group.tm.User"
		rows.get(5).data.userGroup == "squashtest.authz.group.tm.User"
	}

	@Unroll("should allow sorting on #column")
	def "should allow sorting"() {
		when:
		def gridRequest = new GridRequest()
		gridRequest.sort.add(new GridSort(column, GridSort.SortDirection.ASC))
		gridRequest.setSize(25)
		this.userDisplayService.findAll(gridRequest)

		then:
		noExceptionThrown()

		where:
		column << ["active",
				   "login",
				   "userGroup",
				   "firstName",
				   "lastName",
				   "email",
				   "createdOn",
				   "createdBy",
				   "habilitationCount",
				   "teamCount",
				   "lastConnectedOn"]
	}

	def "should fetch user view dto for a given user id"() {

		when:
		def userViewDto = userDisplayService.getUserView(-10L)

		then:

		userViewDto.login == "login0"
		userViewDto.firstName == "Mister"
		userViewDto.lastName == "Pink"
		userViewDto.createdBy == "dbu"
		userViewDto.active
		userViewDto.email == "mpink@rdogs.com"
		List<UserAdminViewTeamDto> teams = userViewDto.teams
		teams.size() == 2
		teams.sort{t1,t2 -> t1.name <=> t2.name}
		teams.get(0).name == "team 1"
	}

	def "should fetch teams from a given user id"() {
		when:
		def teams = userDisplayService.getAssociatedTeams(-10L)

		then:

		teams.sort{t1,t2 -> t1.name <=> t2.name}
		teams.get(0).name == "team 1"
	}


	def "should find all users with access to project"(){
		given:
		def projectIds = [-1L, -404L]
		when:
		List<NamedReference> users = userDisplayService.getUsersWhoCanAccessProjects(projectIds)
		then:
		users.size() == 3
	}
}
