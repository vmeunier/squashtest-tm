/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.backend.tests.administration;

import io.restassured.http.Cookie;
import io.restassured.path.json.JsonPath;
import org.junit.jupiter.api.Test;
import org.squashtest.tm.backend.tests.basetest.DatabaseTest;
import org.squashtest.tm.backend.tests.basetest.Users;
import org.squashtest.tm.service.internal.display.grid.GridRequest;

import java.util.Arrays;
import java.util.List;

import static io.restassured.RestAssured.given;
import static io.restassured.path.json.JsonPath.from;
import static org.junit.Assert.assertEquals;

public class InfoListTest extends DatabaseTest {

	@Test
	public void shouldShowAllUserInfoLists() {
		Cookie session = login(Users.ADMIN);

		GridRequest gridRequest = new GridRequest();
		gridRequest.setSize(10);
		gridRequest.setPage(0);

		String response = given()
			.cookie(session)
			.body(gridRequest)
			.contentType("application/json")
			.when()
			.post("backend/info-lists")
			.then()
			.statusCode(200)
			.extract().response().asString();

		JsonPath jsonPath = from(response);
		List<Object> dataRows = jsonPath.getList("dataRows");

		// There are 3 info_list in database but only 2 are returned because the third one is invalid (it doesn't have
		// a default info_list_item), which is something the front-end should be preventing.
		assertEquals(2, dataRows.size());

		assertEquals(-100L, jsonPath.getLong("dataRows[0].data.infoListId"));
		assertEquals("My list", jsonPath.getString("dataRows[0].data.label"));
		assertEquals("Description", jsonPath.getString("dataRows[0].data.description"));
		assertEquals("LIST01", jsonPath.getString("dataRows[0].data.code"));
		assertEquals("Item01", jsonPath.getString("dataRows[0].data.defaultValue"));
		assertEquals(1, jsonPath.getInt("dataRows[0].data.projectCount"));

		assertEquals(-10L, jsonPath.getLong("dataRows[1].data.infoListId"));
		assertEquals("My list2", jsonPath.getString("dataRows[1].data.label"));
		assertEquals("Item03", jsonPath.getString("dataRows[1].data.defaultValue"));
		assertEquals(1, jsonPath.getInt("dataRows[1].data.projectCount"));
	}

	@Test
	public void shouldDeleteLists() {
		Cookie session = login(Users.ADMIN);

		given()
			.cookie(session)
			.contentType("application/json")
		.when()
			.delete("backend/info-lists/-10,-100")
		.then()
			.statusCode(200);

		GridRequest gridRequest = new GridRequest();
		gridRequest.setSize(10);
		gridRequest.setPage(0);

		String response = given()
			.cookie(session)
			.body(gridRequest)
			.contentType("application/json")
			.when()
			.post("backend/info-lists")
			.then()
			.statusCode(200)
			.extract().response().asString();

		JsonPath jsonPath = from(response);
		List<Object> dataRows = jsonPath.getList("dataRows");
		assertEquals(0, dataRows.size());
	}

	@Override
	protected List<String> getSetupDatasetPath() {
		return Arrays.asList(
			"core-config/core-config.xml",
			"administration/info-list-data-test.xml");
	}
}
