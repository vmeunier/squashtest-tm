/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
	Would allow to load as first class citizen in the main WorkflowScript

	A nice idea, alas wouldn't work on all versions of Jenkins.

	Unused in prod for now

	usage :

node {

	checkout scm

	include = load "${workspace}/pipeline-v1/libs/include.groovy"

		include(["ensureRevision"]).from("gitmodule").into(this)
		include(["extractPomVersion"]).from("mvnmodule").into(this)
		include(["sideDocker", "runAsSide", "containerInfo", "cleanContainer"]).from("dockermodule").into(this)
		include(["eval", "execute", "failsafe", "rm"]).from("shellmodule").into(this)
		include(["isRelease", "addReleaseInfo"]).from("stepmodule").into(this)

}

 */



def include(memberlist){

	def script = this

	return [
		"from" : {
			modulename ->
				// loads the module
				// then attach directly to the main Script object the requested member of the module
				def module = script.load "${script.workspace}/pipeline-v1/libs/${modulename}.groovy"
				memberlist.each{ mbr -> script."$mbr" = module.&(mbr) }
		}
	]
}

return this.&include
