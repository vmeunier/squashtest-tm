/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
	This module exports utilities related to the use of the shell, with respect to he underlying OS (windows or linux)

	Be mindful that some of those command require to be executed within a 'script{ }' block when called from
	within the pipeline because we need to execute some arbitrary Groovy code. This disinction will be mentionned
	in the documentation of each method.

 */


/**
 * This one plainly executes the command using the appropriate shell.
 *
 *
 * Requires 'script{}' block : no
 *
 * @param command
 * @return
 */
def execute(command){
	if (isUnix()){
		sh command
	}
	else{
		bat command
	}
}


/**
 * This one executes the command and returns the first line of the stdout if present, or "" if not
 *
 * Requires 'script{}' block : yes
 *
 * @param command
 * @return
 */
def eval(command){
	def args = [ returnStdout: true, script: command ]

	def output = (isUnix()) ? sh(args) : bat(args)

	def split = output.split('\n')

	return (split.length > 0) ? split[0].trim() : ""
}


/**
 * This one executes the command and will always "succeed" (ie the pipeline won't crash if the command failed).
 * The actual exit status code of the command is returned for further inspection.
 *
 * Requires 'script{}' block : yes
 *
 * @param command
 * @return
 */
def failsafe(command){
	def args = [returnStatus: true, script: command]
	return (isUnix()) ? sh(args) : bat(args)
}


/**
 * Force removes the files or directories (given as strings).
 *
 * It is so because according to https://support.cloudbees.com/hc/en-us/articles/230922508-Pipeline-Files-manipulation
 * it is best to delegate to the OS.
 *
 * Requires 'script{}' block : no
 *
 * @param files
 */
def rm(files){
	if (isUnix()){
		sh "rm -f ${files}"
	}
	else{
		bat "del /f ${files}"
	}
}


return this
