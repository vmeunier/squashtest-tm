#! /bin/bash
#
#     This file is part of the Squashtest platform.
#     Copyright (C) Henix, henix.fr
#
#     See the NOTICE file distributed with this work for additional
#     information regarding copyright ownership.
#
#     This is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Lesser General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     this software is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Lesser General Public License for more details.
#
#     You should have received a copy of the GNU Lesser General Public License
#     along with this software.  If not, see <http://www.gnu.org/licenses/>.
#


#########################################################################
# Installs Node via NVM, then Yarn via NPM
#
# ------
# Arguments :
#       $1 : node version WITHOUT THE 'v' prefix
#       $2 : yarn version WITHOUT THE 'v' prefix
#
#
# Also expect in the environment:
#	NVM_DIR : the installation directory for NVM
#
# ------
# Credits : inspired by
#       https://stackoverflow.com/questions/25899912/how-to-install-nvm-in-docker
#
#
#
# All instructions were reviewed before being included.
#
##########################################################################


cd /tmp

NODE_VERSION="$1"
YARN_VERSION="$2"


# Neutralize dbconf interactive questions
DEBIAN_FRONTEND=noninteractive

# Install nvm with node and npm
curl https://raw.githubusercontent.com/creationix/nvm/v0.33.0/install.sh | bash
. $NVM_DIR/nvm.sh
source ~/.bashrc
nvm install  $NODE_VERSION
nvm alias default $NODE_VERSION
nvm use default

# install yarn
npm install yarn -g
yarn set version $YARN_VERSION


