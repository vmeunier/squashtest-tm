WHAT IS THIS
-----------------------------

This docker image packages various utilities that are required to run the tests of the Angular frontend.
It embeds the following utilities :
- NVM,
- NPM,
- Node,
- Yarn,
- Cypress,
- Chrome

Aside for the version of NVM (harcoded) and that of Chrome (see MAINTENANCE), the version for these tools
can be modified by either specifying it as build argument (for Node and Yarn) or in package.json (for Cypress
and other dependencies). These tools are installed as system dependencies.

Also, the node_modules are provisionned according to package.json, thus the Jenkins test step won't need to
run `yarn install --frozen-lockfile` again. These node_modules are installed into `/root`.

Please note that the user `root` is the only one defined, and owns everything (obviously). It is so because Jenkins wouldn't allow us to run our images with the user we define in the Dockerfile, so please don't shoot me.

Base image is Debian 10.


USAGE
------------------------------

* Run the image (ie instantiate a container) :
```
docker run -d --name testbox squash-tm/frontend-testbox:latest
```

* Copy your folder `tm/tm-front` (including the folder `dist` if required)
into the `root` directory :

```
docker cp <path/tm-front> testbox:/home/frontest
docker exec -u root textbox chown -R frontest:frontest /root/tm-front
```


* Jump into the container as `root` (default user) :
```
docker exec -it testbox /bin/bash
```

* Inside the container move to `/root/tm-front` and link/move the `node_modules`

```
cd ~/tm-front
ln -s /root/node_modules ./node_modules
```


* Run the unit tests for Chrome :

```
yarn test-core --browsers ChromeHeadlessNoSandbox
```

(Note the usage of a dedicated Karma Launcher, see karma.conf.js)

* Notes :

*Note for human users* : 
you can also clone the repo, build with Maven etc instead of `docker cp` your sources. However no 
particular tooling was set up for these usages as the primary usage of 
this image is within Jenkins, so you'll have to `apt-get install` things yourself.

*Note for usage in Jenkins* :
Jenkins will run it as root and will mount the workspace of the host machine using `-w`. 


ENVIRONMENT
---------------------

All the tools are installed systemwide while the node_modules belongs to 
user `root`. In particular : 

`NVM_DIR` : The environment variable pointing to the installation directory of nvm. Everything is installed in subdirectories of this one.

`NODE_HOME` : Specifically the installation dir of Node (a subfolder of NVM_DIR) 

`NODE_PATH` : This variable points to the base `node_modules` which lies in NVM install dir, which pretty much contains only modules of npm and yarn. **Not to be confused** with the TM-specific `node_modules` in `/root`. 

`PATH` : contains the path to all useful binaries.


MAINTENANCE
-----------------
This section explains how to keep this image up-to-date with respect to 
the tooling, and also to the project `tm-front`. This requires occasional manual operations. 

#### Node and Yarn

Their versions are defined by the build variables `NODE_VERSION` and `YARN_VERSION`. They should be kept as close as possible to those defined in the `pom.xml` of `tm-front`. You thus might either change their default value directly in the Dockerfile, or change them at buildtime using the Docker Build flag `--build-args` when regenerating the image.

#### Cypress and other user node_modules

The Docker Build context requires `package.json` and `yarn.lock` in order to provision the user node_modules. When these change in the sources, the files should be copied here afterward. The image will then be regenerated on the fly at the next Jenkins build. 

*Note* : These files have to be physically present in the context when `docker build` is run, otherwise Docker cannot know for sure whether the image requires a new build. This could lead to unnecessary image build, or on the contrary lead to no re-build while it should. So the responsibility know lies on the shoulder of the developper.

#### Chrome

Google publishes only one version of google-chrome-stable at a time. The version of Chrome packaged with the image is then fixed when the image is built. 

To circumvent that limitation one can do one of the following : 
* add/remove a space or a new line, or any modification of the companion script `install-chrome.sh` in order to trigger a new build, or 
* upgrade google-chrome-stable at runtime.

Waiting for a better solution of couse.

#### Branching `tm-front` and versionning the image

The Jenkins build for the branch `front-end` assumes the image is `squash-tm/frontend-testbox:latest`. However if different images are required for different builds (maintenance versions, feature branches etc) the dev that branches the project must also choose a new tag for the image (instead of `latest`) and modify the `Jenkinsfile` accordingly, otherwise unstable Jenkins build and/or waste of CPU time and disk will happen.


