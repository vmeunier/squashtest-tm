#! /bin/bash
#
#     This file is part of the Squashtest platform.
#     Copyright (C) Henix, henix.fr
#
#     See the NOTICE file distributed with this work for additional
#     information regarding copyright ownership.
#
#     This is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Lesser General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     this software is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Lesser General Public License for more details.
#
#     You should have received a copy of the GNU Lesser General Public License
#     along with this software.  If not, see <http://www.gnu.org/licenses/>.
#


#################################################
# UNUSED FOR NOW
#################################################
#  Installs Chrome at the specified version
#
#
 --------------
# Credits : inspired by
#* https://github.com/cypress-io/cypress-docker-images/blob/master/browsers/node10.16.0-chrome76/Dockerfile
#* A former docker image written E. Degenetais & J.Thebault, that help fixing the Chrome version
#
#################################################

cd /tmp


CHROME_MAJOR_VERSION="$1"

# Declare the Google repos
apt-get update && apt-get install -y gnupg2
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
echo "deb http://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google.list
apt-get update


# Install Chrome
apt-get install -y dbus-x11 google-chrome-stable



###############################################

# "fake" dbus address to prevent errors
# https://github.com/SeleniumHQ/docker-selenium/issues/87
# ENV DBUS_SESSION_BUS_ADDRESS=/dev/null

# Add zip utility - it comes in very handy
# RUN apt-get update && apt-get install -y zip
